#include <QFileInfo>
#pragma warning(push)
#pragma warning(disable : 4268)
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#pragma warning(pop)

#include "TCMaskReader.h"

namespace TC::IO {
    struct TCMaskReader::Impl {
        QString path;

        auto GetFullName(H5::Group& group, const QString& name) {
            QString fullname = name;

            H5::Group subGroup = group;
            while (1) {
                try {
                    subGroup = group.openGroup(fullname.toStdString());
                    if (subGroup.getNumObjs() == 0) 
                        break;

                    auto type = subGroup.getObjTypeByIdx(0);
                    if (type != H5G_GROUP) break;

                    auto firstChild = QString::fromStdString(subGroup.getObjnameByIdx(0));
                    fullname = QString("%1/%2").arg(fullname).arg(firstChild);
                    subGroup.close();
                } catch (...) {
                    break;
                }
            }

            return fullname;
        }

        auto OpenBlobGroup(H5::H5File& file, const QString dataID)->H5::Group {
            H5::Group group;
            try {                
                group = file.openGroup(QString("Blobs/%1").arg(dataID).toStdString());
            } catch (...) {
            }

            return group;
        }

        auto OpenResolution(H5::H5File& file,int index)->float {
            double res{0.0};
            std::string attrName;
            if(index == 0 ) attrName = "ResX";
            else if(index == 1) attrName = "ResY";
            else attrName = "ResZ";
            try {
                auto attr = file.openAttribute(attrName);
                attr.read(H5::PredType::NATIVE_DOUBLE,&res);
                attr.close();
            }catch(...) {
                
            }
            return res;
        }

        auto OpenFullSize(H5::H5File& file,int index)->int32_t {
            int32_t size{0};
            std::string attrName;
            if (index == 0) attrName = "SizeX";
            else if (index == 1) attrName = "SizeY";
            else attrName = "SizeZ";
            try {                
                auto attr =  file.openAttribute(attrName);
                attr.read(H5::PredType::NATIVE_INT32, &size);
                attr.close();
            }catch(...) {
                
            }

            return size;
        }

        auto OpenBlobDataSet(H5::Group& group, int timeIndex)->H5::DataSet {
            const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            H5::DataSet dataSet;
            try {
                dataSet = group.openDataSet(name.toStdString());
            }
            catch (...) {
            }

            return dataSet;
        }        
        auto GetBlobCount(H5::DataSet data)->uint32_t {
            uint32_t count = 0;
            if (data.attrExists("Count")) {
                auto attr = data.openAttribute("Count");
                attr.read(H5::PredType::NATIVE_INT32, &count);
                attr.close();
            }
            else {
                hsize_t dims[1]{ 1 };
                auto space = H5::DataSpace(1, dims);
                auto attr = data.createAttribute("Count", H5::PredType::NATIVE_INT32, space);

                int32_t ddata[1]{ 0 };
                attr.write(H5::PredType::NATIVE_INT32, ddata);
                attr.close();
            }
            return count;
        }

        auto OpenMaskGroup(H5::H5File& file, const QString dataID, int timeIndex)->H5::Group {
            const QString name = QString("Masks/%1/%2").arg(dataID).arg(timeIndex, 6, 10, QLatin1Char('0'));

            H5::Group group;
            try {
                group = file.openGroup(name.toStdString());
            } catch (...) {
            }

            return group;
        }

        auto OpenMaskDataSet(H5::Group& group, int blobIndex)->H5::DataSet {
            const QString name = QString("%1").arg(blobIndex, 6, 10, QLatin1Char('0'));
            H5::DataSet dataSet;
            try {
                dataSet = group.openDataSet(name.toStdString());
            } catch (...) {
            }

            return dataSet;
        }
    };

    TCMaskReader::TCMaskReader(const QString& path) :d{ new Impl } {
        d->path = path;
    }

    TCMaskReader::~TCMaskReader() {
    }

    auto TCMaskReader::Exist() const -> bool {

        return QFileInfo::exists(d->path);
        /*HDF5MutexLocker lock(HDF5Mutex::GetInstance());

        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
        } catch (...) {
            return false;
        }

        return true;*/
    }

    auto TCMaskReader::GetDataGroups() const -> QStringList {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        QStringList groups;

        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);

            auto blobGroup = file.openGroup("Blobs");
            auto objs = blobGroup.getNumObjs();
            for (int idx = 0; idx < objs; idx++) {
                auto groupName = QString::fromStdString(blobGroup.getObjnameByIdx(idx));
                groupName = d->GetFullName(blobGroup, groupName);
                groups.append(groupName);
            }
            blobGroup.close();
            file.close();
        } catch (...) {
        }

        return groups;
    }

    auto TCMaskReader::GetTimeIndexCount(const QString& dataID) const -> int32_t {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t count = 0;

        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto group = d->OpenBlobGroup(file, dataID);
            count = group.getNumObjs();
            group.close();
            file.close();
        } catch (...) {
        }

        return count;
    }
    auto TCMaskReader::GetMaskResolution() const -> std::tuple<double, double, double> {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        double resX,resY,resZ;
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            resX = d->OpenResolution(file,0);
            resY = d->OpenResolution(file,1);
            resZ = d->OpenResolution(file,2);
            file.close();
        }catch(...) {
            
        }
        return std::make_tuple(resX, resY, resZ);
    }

    auto TCMaskReader::GetMaskSize() const -> std::tuple<int, int, int> {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t sizeX, sizeY, sizeZ;

        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            sizeX = d->OpenFullSize(file, 0);
            sizeY = d->OpenFullSize(file, 1);
            sizeZ = d->OpenFullSize(file, 2);
            file.close();
        }catch(...) {
            
        }
        return std::make_tuple(sizeX, sizeY, sizeZ);
    }

    auto TCMaskReader::GetBlobCount(const QString& dataID, int timeIndex) const -> int32_t {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t count = 0;

        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto group = d->OpenBlobGroup(file, dataID);
            auto dataSet = d->OpenBlobDataSet(group, timeIndex);
            count = d->GetBlobCount(dataSet);
            dataSet.close();
            group.close();
            file.close();
        } catch (...) {
        }

        return count;
    }

    auto TCMaskReader::ReadBlob(const QString& dataID, int timeIndex,
        int blobIndex) const -> std::tuple<int, BoundingBox, int> {
        int blobKey = -1;
        BoundingBox bbox;
        int code = 0;

        const auto blobs = GetBlobCount(dataID, timeIndex);
        if (blobIndex >= blobs) return std::make_tuple(blobKey, bbox, code);

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto group = d->OpenBlobGroup(file, dataID);
            auto dataSet = d->OpenBlobDataSet(group, timeIndex);

            auto fileSpace = dataSet.getSpace();
            hsize_t size[2]{ 1, 8 };
            hsize_t offset[2]{ static_cast<hsize_t>(blobIndex), 0 };
            fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);

            auto memSpace = H5::DataSpace(2, size);

            int data[8];
            dataSet.read(data, H5::PredType::NATIVE_INT32, memSpace, fileSpace);

            blobKey = data[0];
            bbox.SetOffset(data[1], data[2], data[3]);
            bbox.SetSize(data[4], data[5], data[6]);
            code = data[7];
            
            dataSet.close();
            group.close();
            file.close();
        } catch (...) {
        }

        return std::make_tuple(blobKey, bbox, code);
    }
    auto TCMaskReader::GetNameList(const QString& dataID) const -> QStringList {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        auto result = QStringList();
        H5::StrType strdatatype(H5::PredType::C_S1, 256);
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto group = d->OpenBlobGroup(file, dataID);
            {
                auto exist = true;
                auto cnt = 0;                
                while(exist) {
                    auto attr_name = "organ_name" + std::to_string(cnt);
                    if(group.attrExists(attr_name)) {
                        H5std_string strreadbuf("");
                        auto attr = group.openAttribute(attr_name);
                        attr.read(strdatatype, strreadbuf);
                        result.push_back(QString(strreadbuf.c_str()));
                        attr.close();
                    }else {
                        exist = false;
                    }
                    cnt++;
                }
            }
            group.close();
            file.close();
        }catch(H5::Exception& ) {
            
        }
        return result;
    }
    auto TCMaskReader::ReadMask(const QString& dataID, int timeIndex, int blobIndex) const -> std::shared_ptr<uint8_t[]> {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto group = d->OpenMaskGroup(file, dataID, timeIndex);
            auto dataSet = d->OpenMaskDataSet(group, blobIndex);

            hsize_t dims[3];
            auto fileSpace = dataSet.getSpace();
            fileSpace.getSimpleExtentDims(dims);
            const hsize_t elements = dims[0] * dims[1] * dims[2];

            auto dataSpace = H5::DataSpace(3, dims);

            std::shared_ptr<uint8_t[]> data{ new uint8_t[elements] };
            dataSet.read(data.get(), H5::PredType::NATIVE_UINT8, dataSpace);

            dataSet.close();
            group.close();
            file.close();
            return data;
        } catch (H5::Exception& ) {
            
        }

        return nullptr;
    }
}
