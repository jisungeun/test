#include <QFileInfo>
#include <QDir>
#include "TCUMaskReader.h"
#include "TCUMaskWriter.h"
#include "TCMaskWriterPort.h"

namespace TC::IO {
    struct TCMaskWriterPort::Impl {
        
    };
    TCMaskWriterPort::TCMaskWriterPort() : d{ new Impl } {
        
    }
    TCMaskWriterPort::~TCMaskWriterPort() {
        
    }
    auto TCMaskWriterPort::Modify(TCMask::Pointer data, const QString& path, const QString& organName,const QString& modalityName, int nameIdx) -> bool {
        if(false == QFileInfo::exists(path)) {
            return false;
        }
        auto verReader = TCUMaskReader(path);
        auto version = verReader.GetVersion();
        if(version.at(0) != "2") {
            QFile::remove(path);
            QDir dirPath(path.chopped(4));
            if(dirPath.exists()) {
                dirPath.removeRecursively();
            }
            auto newPath = path.chopped(15) + ".msk"; // remove _MultiLabel.msk or _MultiLayer.msk
            return Write(data, newPath, organName, modalityName, nameIdx);
        }
        auto time_idx = data->GetTimeStep();
        TC::IO::TCUMaskWriter writer(path, time_idx);
        const auto indexes = data->GetBlobIndexes();
        writer.ClearBlobCount(modalityName, organName, time_idx);
        writer.ClearWhole(modalityName, organName, time_idx);
        auto names = data->GetLayerNames();
        for (auto i = 0; i < indexes.size(); i++) {
            if (data->GetLayerNames()[i] != organName) {
                continue;
            }
            auto index = indexes[i];

            auto blob = data->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);
            if (!writer.WriteBlob(modalityName, organName, time_idx, index, bbox, code)) return false;

            auto maskVolume = blob.GetMaskVolume();
            if (!writer.WriteMask(modalityName, organName, time_idx, index, bbox, maskVolume)) return false;
        }
        return true;
    }
    auto TCMaskWriterPort::WriteIntAttrb(TCMask::Pointer data, const QString& path, const QString& modalityName, const QString& organName, const QString& attrName, int value) -> bool {
        auto time_idx = data->GetTimeStep();
        TCUMaskWriter writer(path, time_idx);
        return writer.WriteIntAttrb(modalityName, organName, attrName, value);
    }

    auto TCMaskWriterPort::Write(TCMask::Pointer data, const QString& path,const QString& modalityName, const QString& organName, int nameIdx) -> bool {
        auto time_idx = data->GetTimeStep();
        TC::IO::TCUMaskWriter writer(path, time_idx);
        const auto indexes = data->GetBlobIndexes();
        QString type = "Binary";
        auto typeFromMask = data->GetTypeText();
        if (typeFromMask.isEmpty()) {
            if (data->GetType()._to_integral() == MaskTypeEnum::MultiLabel) {
                type = "Label";
            }
        }
        else {
            type = typeFromMask;
        }
        if (type == "Label") {
            writer.SetNumberOfBlob(indexes.count());
        }
        else {
            writer.SetNumberOfBlob(1);
        }
        writer.ClearBlobCount(modalityName, organName, time_idx);
        writer.ClearWhole(modalityName, organName, time_idx);        

        writer.WriteName(modalityName, nameIdx, organName, type);
        if (type == "Label") {
            for (auto i = 0; i < indexes.count(); i++) {
                auto index = indexes[i];

                auto blob = data->GetBlob(index);
                auto inBox = blob.GetBoundingBox();
                auto code = blob.GetCode();

                TC::IO::BoundingBox bbox;
                bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
                bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                    std::get<4>(inBox) - std::get<1>(inBox) + 1,
                    std::get<5>(inBox) - std::get<2>(inBox) + 1);
                if (!writer.WriteBlob(modalityName, organName, time_idx, index, bbox, code)) {
                    return false;
                }

                auto maskVolume = blob.GetMaskVolume();
                if (!writer.WriteMask(modalityName, organName, time_idx, index, bbox, maskVolume)) {
                    return false;
                }
            }            
        }
        else {
            for (auto i = 0; i < indexes.count(); i++) {
                if (data->GetLayerNames()[i] != organName) {
                    continue;
                }
                auto index = indexes[i];

                auto blob = data->GetBlob(index);
                auto inBox = blob.GetBoundingBox();
                auto code = blob.GetCode();

                TC::IO::BoundingBox bbox;
                bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
                bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                    std::get<4>(inBox) - std::get<1>(inBox) + 1,
                    std::get<5>(inBox) - std::get<2>(inBox) + 1);
                if (!writer.WriteBlob(modalityName, organName, time_idx, 0, bbox, code)) {
                    return false;
                }

                auto maskVolume = blob.GetMaskVolume();
                if (!writer.WriteMask(modalityName, organName, time_idx, 0, bbox, maskVolume)) {
                    return false;
                }
            }
        }
        const auto size = data->GetSize();
        writer.WriteSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
        const auto res = data->GetResolution();
        writer.WriteResolution(std::get<0>(res), std::get<1>(res), std::get<2>(res));
        writer.WriteVersion(2, 0, 0);
        return true;
    }
}