#include <QFileInfo>
#include <QDir>
#pragma warning(push)
#pragma warning(disable : 4268)
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#pragma warning(pop)

#include "TCUMaskReader.h"

namespace TC::IO {
    struct TCUMaskReader::Impl {
        QString path;
        auto OpenBlobGroup(const QString& dataID,const QString &name)->QString {
            QString group = path.chopped(4) + "/Blobs/" + dataID + "/" +name;
            return group;
        }
        auto OpenVersion(H5::H5File& file)->QString {
            QString version = QString();
            try {
                H5std_string strreadbuf("");
                auto attr = file.openAttribute("Version");
                H5::StrType strdatatype(H5::PredType::C_S1, 256);
                attr.read(strdatatype, strreadbuf);
                version = QString(strreadbuf.c_str());
                attr.close();
                strdatatype.close();
            }
            catch (...) {

            }
            return version;
        }
        auto OpenResolution(H5::H5File& file, int index)->float {
            double res{ 0.0 };
            std::string attrName;
            if (index == 0) attrName = "ResX";
            else if (index == 1) attrName = "ResY";
            else attrName = "ResZ";
            try {
                auto attr = file.openAttribute(attrName);
                attr.read(H5::PredType::NATIVE_DOUBLE, &res);
                attr.close();
            }
            catch (...) {

            }
            return res;
        }
        auto OpenFullSize(H5::H5File& file, int index)->int32_t {
            int32_t size{ 0 };
            std::string attrName;
            if (index == 0) attrName = "SizeX";
            else if (index == 1) attrName = "SizeY";
            else attrName = "SizeZ";
            try {
                auto attr = file.openAttribute(attrName);
                attr.read(H5::PredType::NATIVE_INT32, &size);
                attr.close();
            }
            catch (...) {

            }

            return size;
        }
        auto OpenBlobDataSet(H5::H5File& file)->H5::DataSet {
            H5::DataSet dataSet;

            try {
                dataSet = file.openDataSet("BlobData");                
            }
            catch (...) {

            }

            return dataSet;
        }
        auto GetBlobCount(H5::DataSet& data)->int32_t {
            int32_t count = 0;
            if (data.attrExists("Count")) {
                auto attr = data.openAttribute("Count");
                attr.read(H5::PredType::NATIVE_INT32, &count);
                attr.close();
            }          
            return count;
        }
        auto GetIntAttrb(H5::DataSet& data,const QString& attrName)->int32_t {
            int32_t value = -1;
            if(data.attrExists(attrName.toStdString())) {
                auto attr = data.openAttribute(attrName.toStdString());
                attr.read(H5::PredType::NATIVE_INT32, &value);
                attr.close();
            }
            return value;
        }
    };

    TCUMaskReader::TCUMaskReader(const QString& path) : d{ new Impl } {
        d->path = path;
    }
    TCUMaskReader::~TCUMaskReader() {

    }
    auto TCUMaskReader::Exist() const -> bool {
        return QFileInfo::exists(d->path);
    }
    auto TCUMaskReader::isValid() const -> bool {
        if (false == QFileInfo::exists(d->path)) {
            return false;
        }
        QDir dir(d->path.chopped(4));
        if (false == dir.exists(d->path.chopped(4))) {
            return false;
        }
        return true;
    }
    auto TCUMaskReader::GetTimeIndexCount(const QString& dataID,const QString& name) const -> int32_t {
        int32_t count = 0;
        try {
            auto group = d->OpenBlobGroup(dataID,name);
            const QDir dir(group);
            count = static_cast<int32_t>(dir.count());
        }
        catch (...) {
        }
        return count;
    }
    auto TCUMaskReader::GetVersion() const -> QString {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        QString version = QString();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            version = d->OpenVersion(file);
            file.close();
        }
        catch (H5::Exception&) {

        }
        return version;
    }
    auto TCUMaskReader::GetMaskResolution() const -> std::tuple<double, double, double> {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        double resX, resY, resZ;
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            resX = d->OpenResolution(file, 0);
            resY = d->OpenResolution(file, 1);
            resZ = d->OpenResolution(file, 2);
            file.close();
        }
        catch (...) {

        }
        return std::make_tuple(resX, resY, resZ);
    }
    auto TCUMaskReader::GetMaskSize() const -> std::tuple<int, int, int> {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t sizeX, sizeY, sizeZ;

        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            sizeX = d->OpenFullSize(file, 0);
            sizeY = d->OpenFullSize(file, 1);
            sizeZ = d->OpenFullSize(file, 2);
            file.close();
        }
        catch (...) {

        }
        return std::make_tuple(sizeX, sizeY, sizeZ);
    }
    auto TCUMaskReader::ReadIntAttrb(const QString& dataID, const QString& name, const QString& attrName) -> int {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t value = -1;
        try {
            auto group = d->OpenBlobGroup(dataID, name);
            const QString timeStamp = QString("%1").arg(0, 6, 10, QLatin1Char('0'));
            auto dsetPath = group + "/" + timeStamp;
            QFileInfo filePath(dsetPath);
            if (filePath.exists()) {
                H5::H5File file(dsetPath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
                auto dataSet = file.openDataSet("BlobData");
                value = d->GetIntAttrb(dataSet,attrName);
                dataSet.close();
                file.close();
            }
        }
        catch (H5::Exception& e) {
            std::cout << e.getCDetailMsg() << std::endl;
        }

        return value;
    }
    auto TCUMaskReader::GetBlobCount(const QString& dataID,const QString&name, int timeIndex) const -> int32_t {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t count = 0;
        try {
            auto group = d->OpenBlobGroup(dataID,name);
            const QString timeStamp = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            auto dsetPath = group + "/" + timeStamp;            
            QFileInfo filePath(dsetPath);
            if (filePath.exists()) {
                H5::H5File file(dsetPath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
                auto dataSet = file.openDataSet("BlobData");
                count = d->GetBlobCount(dataSet);
                dataSet.close();
                file.close();
            }
        }
        catch (H5::Exception& e) {
            std::cout << e.getCDetailMsg() << std::endl;
        }

        return count;
    }
    auto TCUMaskReader::ReadBlob(const QString& dataID,const QString& name, int timeIndex, int blobIndex) const -> std::tuple<int, BoundingBox, int> {
        int blobKey = -1;
        BoundingBox bbox;
        int code = 0;

        const auto blobs = GetBlobCount(dataID, name, timeIndex);
        if (blobIndex >= blobs) {
            return std::make_tuple(blobKey, bbox, code);
        }

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            auto group = d->OpenBlobGroup(dataID,name);
            const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            auto dsetPath = group + "/" + name;
            QFileInfo filePath(dsetPath);
            if (filePath.exists()) {
                H5::H5File file(dsetPath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
                auto dataSet = file.openDataSet("BlobData");

                auto fileSpace = dataSet.getSpace();
                hsize_t size[2]{ 1, 8 };
                hsize_t offset[2]{ static_cast<hsize_t>(blobIndex), 0 };
                fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);

                auto memSpace = H5::DataSpace(2, size);

                int data[8];
                dataSet.read(data, H5::PredType::NATIVE_INT32, memSpace, fileSpace);

                blobKey = data[0];
                bbox.SetOffset(data[1], data[2], data[3]);
                bbox.SetSize(data[4], data[5], data[6]);
                code = data[7];

                memSpace.close();
                fileSpace.close();
                dataSet.close();
                file.close();
            }
        }
        catch (...) {
        }

        return std::make_tuple(blobKey, bbox, code);
    }
    auto TCUMaskReader::GetTypeList(const QString& dataID) const -> QStringList {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        auto result = QStringList();
        H5::StrType strdatatype(H5::PredType::C_S1, 256);
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            {
                auto group = file.openGroup(dataID.toStdString());
                auto exist = true;
                auto cnt = 0;
                while (exist) {
                    auto attr_name = "type" + std::to_string(cnt);
                    if (group.attrExists(attr_name)) {
                        H5std_string strreadbuf("");
                        auto attr = group.openAttribute(attr_name);
                        attr.read(strdatatype, strreadbuf);
                        result.push_back(QString(strreadbuf.c_str()));
                        attr.close();
                    }
                    else {
                        exist = false;
                    }
                    cnt++;
                }
                group.close();
            }
            file.close();
        }
        catch (H5::Exception&) {

        }
        strdatatype.close();
        return result;
    }
    auto TCUMaskReader::GetNameList(const QString& dataID) const -> QStringList {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        auto result = QStringList();
        H5::StrType strdatatype(H5::PredType::C_S1, 256);
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            {
                auto group = file.openGroup(dataID.toStdString());
                auto exist = true;
                auto cnt = 0;
                while (exist) {
                    auto attr_name = "organ_name" + std::to_string(cnt);
                    if (group.attrExists(attr_name)) {
                        H5std_string strreadbuf("");
                        auto attr = group.openAttribute(attr_name);
                        attr.read(strdatatype, strreadbuf);
                        result.push_back(QString(strreadbuf.c_str()));
                        attr.close();
                    }
                    else {
                        exist = false;
                    }
                    cnt++;
                }
                group.close();
            }
            file.close();
        }
        catch (H5::Exception&) {

        }
        strdatatype.close();
        return result;
    }
    auto TCUMaskReader::ReadMask(const QString& dataID,const QString& name, int timeIndex, int blobIndex) const -> std::shared_ptr<uint8_t[]> {
        const QString tmp_path = QString("/Masks/%1/%2/%3").arg(dataID).arg(name).arg(timeIndex, 6, 10, QLatin1Char('0'));
        auto folder_path = d->path.chopped(4) + tmp_path;
        const QString blob_name = QString("%1").arg(blobIndex, 6, 10, QLatin1Char('0'));
        auto file_path = folder_path + "/" + blob_name;
        if (!QFileInfo::exists(file_path)) {
            return nullptr;
        }
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(file_path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto dataSet = file.openDataSet("MaskData");
            hsize_t dims[3];
            auto fileSpace = dataSet.getSpace();
            fileSpace.getSimpleExtentDims(dims);
            const hsize_t elements = dims[0] * dims[1] * dims[2];

            auto dataSpace = H5::DataSpace(3, dims);

            std::shared_ptr<uint8_t[]> data{ new uint8_t[elements] };
            dataSet.read(data.get(), H5::PredType::NATIVE_UINT8, dataSpace);

            dataSpace.close();
            fileSpace.close();
            dataSet.close();
            file.close();

            return data;
        }
        catch (H5::Exception&) {
            return nullptr;
        }
    }
}