#include <QFileInfo>
#include <QDir>
#pragma warning(push)
#pragma warning(disable : 4268)
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#pragma warning(pop)

#include "TCHMaskReader.h"

namespace TC::IO {
    struct TCHMaskReader::Impl {
        QString path;
        auto OpenBlobGroup(const QString dataID)->QString {
            QString group =  path.chopped(4) + "/Blobs/" + dataID;
            return group;
        }
        auto OpenVersion(H5::H5File& file)->QString {
            QString version = QString();
            try {
                H5std_string strreadbuf("");
                auto attr = file.openAttribute("Version");
                H5::StrType strdatatype(H5::PredType::C_S1, 256);
                attr.read(strdatatype, strreadbuf);
                version = QString(strreadbuf.c_str());
                attr.close();
                strdatatype.close();
            }catch(...) {
                
            }
            return version;
        }
        auto OpenResolution(H5::H5File& file, int index)->float {
            double res{0.0};
            std::string attrName;
            if (index == 0) attrName = "ResX";
            else if (index == 1) attrName = "ResY";
            else attrName = "ResZ";
            try {
                auto attr = file.openAttribute(attrName);
                attr.read(H5::PredType::NATIVE_DOUBLE, &res);
                attr.close();                
            }
            catch (...) {

            }
            return res;
        }
        auto OpenFullSize(H5::H5File& file, int index)->int32_t {
            int32_t size{0};
            std::string attrName;
            if (index == 0) attrName = "SizeX";
            else if (index == 1) attrName = "SizeY";
            else attrName = "SizeZ";
            try {
                auto attr = file.openAttribute(attrName);
                attr.read(H5::PredType::NATIVE_INT32, &size);
                attr.close();
            }
            catch (...) {

            }

            return size;
        }
        auto OpenBlobDataSet(H5::H5File& file, int timeIndex)->H5::DataSet {
            //const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            //auto dsetPath = group + "/" + name;
            H5::DataSet dataSet;            

            try {                
                dataSet = file.openDataSet("BlobData");
                //dataSet.close();
                //file.close();
            }
            catch (...) {
                
            }            

            return dataSet;
        }
        auto GetBlobCount(H5::DataSet& data)->int32_t {
            int32_t count = 0;
            if (data.attrExists("Count")) {
                auto attr = data.openAttribute("Count");
                attr.read(H5::PredType::NATIVE_INT32, &count);
                attr.close();
            }
            else {
                hsize_t dims[1]{ 1 };
                auto space = H5::DataSpace(1, dims);
                auto attr = data.createAttribute("Count", H5::PredType::NATIVE_INT32, space);

                int32_t ddata[1]{ 0 };
                attr.write(H5::PredType::NATIVE_INT32, ddata);
                space.close();
                attr.close();                
            }
            return count;
        }
    };

    TCHMaskReader::TCHMaskReader(const QString& path) : d{new Impl} {
        d->path = path;
    }
    TCHMaskReader::~TCHMaskReader() {
        
    }
    auto TCHMaskReader::Exist() const -> bool {
        return QFileInfo::exists(d->path);
    }
    auto TCHMaskReader::isValid() const -> bool {        
        if(!QFileInfo::exists(d->path)) {
            return false;
        }        
        return true;
    }
    auto TCHMaskReader::GetTimeIndexCount(const QString& dataID) const -> int32_t {        
        int32_t count = 0;
        try {            
            auto group = d->OpenBlobGroup(dataID);
            QDir dir(group);
            count = dir.count();            
        }
        catch (...) {
        }
        return count;
    }
    auto TCHMaskReader::GetVersion() const -> QString {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        QString version = QString();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            version = d->OpenVersion(file);
            file.close();
        }catch(H5::Exception&) {
            
        }
        return version;
    }
    auto TCHMaskReader::GetMaskResolution() const -> std::tuple<double, double, double> {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        double resX, resY, resZ;
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            resX = d->OpenResolution(file, 0);
            resY = d->OpenResolution(file, 1);
            resZ = d->OpenResolution(file, 2);
            file.close();
        }
        catch (...) {

        }
        return std::make_tuple(resX, resY, resZ);
    }
    auto TCHMaskReader::GetMaskSize() const -> std::tuple<int, int, int> {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t sizeX, sizeY, sizeZ;

        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            sizeX = d->OpenFullSize(file, 0);
            sizeY = d->OpenFullSize(file, 1);
            sizeZ = d->OpenFullSize(file, 2);
            file.close();
        }
        catch (...) {

        }
        return std::make_tuple(sizeX, sizeY, sizeZ);
    }
    auto TCHMaskReader::GetBlobCount(const QString& dataID, int timeIndex) const -> int32_t {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        int32_t count = 0;
        try {
            auto group = d->OpenBlobGroup(dataID);
            const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            auto dsetPath = group + "/" + name;
            QFileInfo filePath(dsetPath);
            if (filePath.exists()) {
                H5::H5File file(dsetPath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
                auto dataSet = file.openDataSet("BlobData");
                count = d->GetBlobCount(dataSet);
                dataSet.close();
                file.close();
            }
        }
        catch (H5::Exception& e) {
            std::cout << e.getCDetailMsg() << std::endl;
        }

        return count;
    }
    auto TCHMaskReader::ReadBlob(const QString& dataID, int timeIndex, int blobIndex) const -> std::tuple<int, BoundingBox, int> {
        int blobKey = -1;
        BoundingBox bbox;
        int code = 0;

        const auto blobs = GetBlobCount(dataID, timeIndex);
        if (blobIndex >= blobs) {            
            return std::make_tuple(blobKey, bbox, code);
        }

        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {            
            auto group = d->OpenBlobGroup(dataID);
            const QString name = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            auto dsetPath = group + "/" + name;
            QFileInfo filePath(dsetPath);
            if (filePath.exists()) {
                H5::H5File file(dsetPath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
                auto dataSet = file.openDataSet("BlobData");

                auto fileSpace = dataSet.getSpace();
                hsize_t size[2]{ 1, 8 };
                hsize_t offset[2]{ static_cast<hsize_t>(blobIndex), 0 };
                fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);

                auto memSpace = H5::DataSpace(2, size);

                int data[8];
                dataSet.read(data, H5::PredType::NATIVE_INT32, memSpace, fileSpace);

                blobKey = data[0];
                bbox.SetOffset(data[1], data[2], data[3]);
                bbox.SetSize(data[4], data[5], data[6]);
                code = data[7];

                memSpace.close();
                fileSpace.close();
                dataSet.close();
                file.close();
            }
        }
        catch (...) {
        }

        return std::make_tuple(blobKey, bbox, code);
    }
    auto TCHMaskReader::GetNameList(const QString& dataID) const -> QStringList {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        auto result = QStringList();
        H5::StrType strdatatype(H5::PredType::C_S1, 256);
        try {            
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            {
                auto group = file.openGroup(dataID.toStdString());
                auto exist = true;
                auto cnt = 0;
                while (exist) {
                    auto attr_name = "organ_name" + std::to_string(cnt);
                    if (group.attrExists(attr_name)) {
                        H5std_string strreadbuf("");
                        auto attr = group.openAttribute(attr_name);
                        attr.read(strdatatype, strreadbuf);
                        result.push_back(QString(strreadbuf.c_str()));
                        attr.close();
                    }
                    else {
                        exist = false;
                    }
                    cnt++;
                }
                group.close();
            }            
            file.close();            
        }        
        catch (H5::Exception& ) {

        }
        strdatatype.close();
        return result;
    }
    auto TCHMaskReader::ReadMask(const QString& dataID, int timeIndex, int blobIndex) const -> std::shared_ptr<uint8_t[]> {
        const QString name = QString("/Masks/%1/%2").arg(dataID).arg(timeIndex, 6, 10, QLatin1Char('0'));
        auto folder_path = d->path.chopped(4) + name;
        const QString blob_name = QString("%1").arg(blobIndex, 6, 10, QLatin1Char('0'));
        auto file_path = folder_path + "/" + blob_name;        
        if(!QFileInfo::exists(file_path)) {             
            return nullptr;
        }
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(file_path.toLocal8Bit().constData(), H5F_ACC_RDONLY); 
            auto dataSet = file.openDataSet("MaskData");
            hsize_t dims[3];
            auto fileSpace = dataSet.getSpace();
            fileSpace.getSimpleExtentDims(dims);
            const hsize_t elements = dims[0] * dims[1] * dims[2];

            auto dataSpace = H5::DataSpace(3, dims);

            std::shared_ptr<uint8_t[]> data{ new uint8_t[elements] };
            dataSet.read(data.get(), H5::PredType::NATIVE_UINT8, dataSpace);

            dataSpace.close();
            fileSpace.close();
            dataSet.close();            
            file.close();

            return data;
        }catch(H5::Exception& ) {
            return nullptr;
        }        
    }

}