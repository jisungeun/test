#include <QFileInfo>

#include "TCMaskReader.h"
#include "TCHMaskReader.h"
#include "TCUMaskReader.h"

#include "TCMaskReaderPort.h"

namespace TC::IO {
	struct TCMaskReaderPort::Impl {

	};
	TCMaskReaderPort::TCMaskReaderPort() : d{ new Impl } {

	}
	TCMaskReaderPort::~TCMaskReaderPort() {

	}
	auto TCMaskReaderPort::Read(const QString& path, const QString& modalityName, const QString& organName, int time_idx) -> TCMask::Pointer {
		auto version = GetVersion(path);
		if (std::get<0>(version) < 0) {
			return nullptr;
		}
		if (std::get<0>(version) < 1) {
			return ReadOld(path, time_idx, organName);
		}
		if (std::get<0>(version) < 2) {
			return Read100(path, time_idx, organName);
		}
		auto reader = TCUMaskReader(path);		
		TCMask::Pointer maskData(new TCMask());		
		maskData->SetTimeStep(time_idx);
		const auto blobs = reader.GetBlobCount(modalityName, organName, time_idx);
		if (blobs < 1) {
			return nullptr;
		}
		auto nameList = reader.GetNameList(modalityName);
		auto typeList = reader.GetTypeList(modalityName);
		auto nameKey = -1;
		for (auto i = 0; i < nameList.count(); i++) {
			if (nameList[i] == organName) {
				nameKey = i;
				break;
			}
		}
		if (nameKey < 0) {
			return nullptr;
		}
		maskData->SetType(MaskTypeEnum::MultiLabel);
		maskData->SetTypeText(typeList[nameKey]);
		
		for (int idx = 0; idx < blobs; idx++) {
			auto data = reader.ReadBlob(modalityName, organName, time_idx, idx);
			auto index = std::get<0>(data);
			auto bbox = std::get<1>(data);
			auto code = std::get<2>(data);
			auto offset = bbox.GetOffset();
			auto size = bbox.GetSize();
			MaskBlob blob;
			blob.SetBoundingBox(offset.x0, offset.y0, offset.z0, offset.x0 + size.d0 - 1, offset.y0 + size.d1 - 1, offset.z0 + size.d2 - 1);
			blob.SetCode(code);

		    maskData->AppendBlob(index, blob,organName);
			auto maskVolume = reader.ReadMask(modalityName, organName, time_idx, index);

		    maskData->AppendMaskVolume(index, maskVolume);
		}
		const auto size = reader.GetMaskSize();
		maskData->SetSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
		const auto res = reader.GetMaskResolution();
		double ress[3] = { std::get<0>(res), std::get<1>(res), std::get<2>(res) };
		maskData->SetResolution(ress);
		maskData->SetTimeStep(time_idx);
		return maskData;

	}
	auto TCMaskReaderPort::ReadIntAttrb(const QString& path, const QString& modalityName, const QString& organName, const QString& attrbName) -> int {
		TCUMaskReader reader(path);
		return reader.ReadIntAttrb(modalityName, organName, attrbName);
    }
	auto TCMaskReaderPort::GetVersion(const QString& path)const -> std::tuple<int, int, int> {
		auto reader = TCUMaskReader(path);
		QString versionText{ "0.0.0" };
		if (false == reader.Exist()) {
			versionText = QString("-1.-1.-1");
		}
		else {
			auto version = reader.GetVersion();
			if (false == version.isEmpty()) {
				versionText = version;
			}
		}
		auto sp = versionText.split(".");
		return std::make_tuple(sp[0].toInt(), sp[1].toInt(), sp[2].toInt());
	}
	auto TCMaskReaderPort::GetMaskList(const QString& path, const QString& modalityName) ->QList<MaskMeta> {
		auto version = GetVersion(path);
		if (std::get<0>(version) < 0) {
			//not exist
			return QList<MaskMeta>();
		}
		if (std::get<0>(version) < 1) {
			//old reader
			QList<MaskMeta> metaList;
			auto layerReader = TCMaskReader(path);
			auto layerNames = layerReader.GetNameList(modalityName);
			for (auto layer : layerNames) {
				MaskMeta meta;
				meta.name = layer;
				if (path.contains("_MultiLayer")) {
					meta.type = MaskTypeEnum::MultiLayer;
				}
				else {
					meta.type = MaskTypeEnum::MultiLabel;
				}
				metaList.append(meta);
			}
			return metaList;
		}
		if (std::get<0>(version) < 2) {
			//1.0.0 reader
			QList<MaskMeta> metaList;
			auto layerReader = TCHMaskReader(path);
			auto layerNames = layerReader.GetNameList(modalityName);
			for (auto layer : layerNames) {
				MaskMeta meta;
				meta.name = layer;
				if (path.contains("_MultiLayer")) {
					meta.type = MaskTypeEnum::MultiLayer;
				}
				else {
					meta.type = MaskTypeEnum::MultiLabel;
				}
				metaList.append(meta);
			}
			return metaList;
		}
		//2.0.0 reader
		auto reader = TCUMaskReader(path);
		auto nameList = reader.GetNameList(modalityName);
		auto typeList = reader.GetTypeList(modalityName);
		if (nameList.count() != typeList.count()) {
			return QList<MaskMeta>();
		}
		auto metaList = QList<MaskMeta>();
		for (auto i = 0; i < nameList.count(); i++) {			
			MaskMeta meta;
			meta.name = nameList[i];
			if (typeList[i] == "Binary") {
				meta.type = MaskTypeEnum::MultiLayer;
			}else {
				meta.type = MaskTypeEnum::MultiLabel;
			}
			metaList.append(meta);
		}
		return metaList;
	}
	auto TCMaskReaderPort::ReadOld(const QString& path, int time_idx, const QString& organName)  -> TCMask::Pointer {
		auto reader = TC::IO::TCMaskReader(path);
		if (!reader.Exist()) return nullptr;
				
		TCMask::Pointer maskData(new TCMask());
		maskData->SetTimeStep(time_idx);		
		const auto blobs = reader.GetBlobCount("HT", time_idx);
		if (blobs < 1) {
			return nullptr;
		}
		auto nameList = reader.GetNameList("HT");
		maskData->SetType(MaskTypeEnum::MultiLabel);
		if (organName == "cellInst") {			
			maskData->SetTypeText("Label");
		}
		else {		
			maskData->SetTypeText("Binary");
		}
		for (int idx = 0; idx < blobs; idx++) {
			if(nameList[idx] != organName) {
				continue;
			}
			auto data = reader.ReadBlob("HT", time_idx, idx);
			auto index = std::get<0>(data);
			auto bbox = std::get<1>(data);
			auto code = std::get<2>(data);
			auto offset = bbox.GetOffset();
			auto size = bbox.GetSize();
			MaskBlob blob;
			blob.SetBoundingBox(offset.x0, offset.y0, offset.z0, offset.x0 + size.d0 - 1, offset.y0 + size.d1 - 1, offset.z0 + size.d2 - 1);
			blob.SetCode(code);
			
			maskData->AppendBlob(index, blob,organName);

			auto maskVolume = reader.ReadMask("HT", time_idx, index);
			
			maskData->AppendMaskVolume(index, maskVolume);
		}
		const auto size = reader.GetMaskSize();
		maskData->SetSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
		const auto res = reader.GetMaskResolution();
		double ress[3] = { std::get<0>(res), std::get<1>(res), std::get<2>(res) };
		maskData->SetResolution(ress);
		maskData->SetTimeStep(time_idx);
		return maskData;
	}
	auto TCMaskReaderPort::Read100(const QString& path, int time_idx, const QString& organName)  -> TCMask::Pointer {
		auto reader = TC::IO::TCHMaskReader(path);
				
		TCMask::Pointer maskData(new TCMask());
		maskData->SetTimeStep(time_idx);
		const auto blobs = reader.GetBlobCount("HT", time_idx);
		if (blobs < 1) {
			return nullptr;
		}
		auto nameList = reader.GetNameList("HT");
		maskData->SetType(MaskTypeEnum::MultiLabel);
		if (organName == "cellInst") {			
			maskData->SetTypeText("Label");
		}
		else {			
			maskData->SetTypeText("Binary");
		}
		for (int idx = 0; idx < blobs; idx++) {
			if(nameList[idx] != organName) {
				continue;
			}
			auto data = reader.ReadBlob("HT", time_idx, idx);
			auto index = std::get<0>(data);
			auto bbox = std::get<1>(data);
			auto code = std::get<2>(data);
			auto offset = bbox.GetOffset();
			auto size = bbox.GetSize();
			MaskBlob blob;
			blob.SetBoundingBox(offset.x0, offset.y0, offset.z0, offset.x0 + size.d0 - 1, offset.y0 + size.d1 - 1, offset.z0 + size.d2 - 1);
			blob.SetCode(code);

		    maskData->AppendBlob(index, blob,organName);

		    auto maskVolume = reader.ReadMask("HT", time_idx, index);
			
		    maskData->AppendMaskVolume(index, maskVolume);			
		}
		const auto size = reader.GetMaskSize();
		maskData->SetSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
		const auto res = reader.GetMaskResolution();
		double ress[3] = { std::get<0>(res), std::get<1>(res), std::get<2>(res) };
		maskData->SetResolution(ress);
		maskData->SetTimeStep(time_idx);
		return maskData;
	}
}