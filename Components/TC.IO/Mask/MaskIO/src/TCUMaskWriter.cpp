#pragma warning(push)
#pragma warning(disable : 4268)
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#pragma warning(pop)

#include "TCUMaskWriter.h"

#include <QMap>
#include <QDir>

namespace TC::IO {
	struct TCUMaskWriter::Impl {
		QString path;
        int time_steps{ 1 };		
		const uint32_t columns{ 8 };
        uint32_t blobNum{ 1 };
        auto WriteResolution(H5::H5File& file, double res, int index)->void {
            double value[1]{ res };
            std::string attrName;
            if (index == 0) attrName = "ResX";
            else if (index == 1) attrName = "ResY";
            else if (index == 2) attrName = "ResZ";

            H5::Attribute attr;
            try {
                if (file.attrExists(attrName)) {
                    attr = file.openAttribute(attrName);
                }
                else {
                    hsize_t dims[1]{ 1 };
                    auto space = H5::DataSpace(1, dims);
                    attr = file.createAttribute(attrName, H5::PredType::NATIVE_DOUBLE, space);
                    space.close();
                }
            }
            catch (H5::Exception&) {

            }
            attr.write(H5::PredType::NATIVE_DOUBLE, value);
            attr.close();
        }
        auto WriteFullSize(H5::H5File& file, int32_t size, int index)->void {
            int32_t value[1]{ size };
            std::string attrName;
            if (index == 0) attrName = "SizeX";
            else if (index == 1) attrName = "SizeY";
            else if (index == 2) attrName = "SizeZ";
            H5::Attribute attr;
            try {
                if (file.attrExists(attrName)) {
                    attr = file.openAttribute(attrName);
                }
                else {
                    hsize_t dims[1]{ 1 };
                    auto space = H5::DataSpace(1, dims);
                    attr = file.createAttribute(attrName, H5::PredType::NATIVE_INT32, space);
                    space.close();
                }
            }
            catch (H5::Exception&) {
            }
            attr.write(H5::PredType::NATIVE_INT32, value);
            attr.close();
        }
        auto OpenGroup(H5::H5File& file, const QString strName)->H5::Group {
            H5::Group group;
            try {
                if (file.exists(strName.toStdString())) {
                    group = file.openGroup(strName.toStdString());
                }
                else {
                    group = file.createGroup(strName.toStdString());
                }
            }
            catch (H5::Exception&) {

            }
            return group;
        }
        auto WriteVersionText(H5::H5File& file, int major, int minor, int patch)->void {
            QString versionText = QString("%1.%2.%3").arg(major).arg(minor).arg(patch);
            H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters
            if (file.attrExists("Version")) {
                auto attr = file.openAttribute("Version");
                attr.write(strdatatype, versionText.toStdString());
                attr.close();
            }
            else {
                H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
                auto attr = file.createAttribute("Version", strdatatype, attr_dataspace);
                attr.write(strdatatype, versionText.toStdString());
                attr_dataspace.close();
                attr.close();
            }
            strdatatype.close();
        }
        auto OpenGroup(H5::Group& parentGroup, const QString strName)->H5::Group {
            H5::Group group;
            try {
                if (parentGroup.exists(strName.toStdString())) {
                    group = parentGroup.openGroup(strName.toStdString());
                }
                else {
                    group = parentGroup.createGroup(strName.toStdString());
                }
            }
            catch (H5::Exception&) {
            }
            return group;
        }
        auto OpenOrCraeteMaskDataSet(H5::H5File& file, const QString name, hsize_t dims[3])->H5::DataSet {
            H5::DataSet dataSet;
            if (file.exists(name.toStdString())) {
                dataSet = file.openDataSet(name.toStdString());
            }
            else {
                auto space = H5::DataSpace(3, dims);
                H5::DSetCreatPropList cparams;
                int32_t fillValue = 0;
                cparams.setFillValue(H5::PredType::NATIVE_UINT8, &fillValue);

                dataSet = file.createDataSet(name.toStdString(), H5::PredType::NATIVE_UINT8, space, cparams);
                cparams.close();
                space.close();
            }
            return dataSet;
        }        
        auto OpenOrCreateBlobDataSet(H5::H5File& file, const QString name)->H5::DataSet {
            H5::DataSet dataSet;
            try {
                if (file.exists(name.toStdString())) {
                    dataSet = file.openDataSet(name.toStdString());
                }
                else {                    
                    hsize_t dims[2]{ blobNum, columns };
                    //hsize_t max_dims[2]{ H5S_UNLIMITED, columns };                    
                    //auto space = H5::DataSpace(2, dims, max_dims);
                    auto space = H5::DataSpace(2, dims);

                    H5::DSetCreatPropList cparams;
                    int32_t fillValue = 0;
                    cparams.setFillValue(H5::PredType::NATIVE_INT32, &fillValue);                    
                    dataSet = file.createDataSet(name.toStdString(), H5::PredType::NATIVE_INT32, space, cparams);
                    cparams.close();
                    space.close();
                }
            }
            catch (H5::Exception&) {
            }

            return dataSet;
        }
        auto SetName(H5::Group group, int nameIdx, const QString& name) ->bool {
            auto numAttr = group.getNumAttrs();            
            //std::string attrName = "organ_name" + std::to_string(nameIdx);
            H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters
            for(auto i=0;i<numAttr/2;i++) {
                std::string prevAttr = "organ_name" + std::to_string(i);
                if(group.attrExists(prevAttr)) {
                    auto attr = group.openAttribute(prevAttr);
                    H5std_string strreadbuf("");
                    attr.read(strdatatype,strreadbuf);
                    attr.close();
                    if(QString(strreadbuf.c_str()) == name) {
                        strdatatype.close();
                        return false;
                    }
                }
            }
            std::string attrName = "organ_name" + std::to_string(numAttr / 2);            
            if (group.attrExists(attrName)) {
                auto attr = group.openAttribute(attrName);
                attr.write(strdatatype, name.toStdString());
                attr.close();
            }
            else {                
                H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
                // Create new string datatype for attribute                
                auto attr = group.createAttribute(attrName, strdatatype, attr_dataspace);
                attr.write(strdatatype, name.toStdString());
                attr_dataspace.close();
                attr.close();
            }
            strdatatype.close();
            return true;
        }
        auto SetType(H5::Group group,int typeIdx,const QString& type)->void {            
            auto numAttr = group.getNumAttrs();
            //std::string attrName = "type" + std::to_string(typeIdx);
            std::string attrName = "type" + std::to_string(numAttr / 2);
            H5::StrType strdatatype(H5::PredType::C_S1, 256); // of length 256 characters            
            if (group.attrExists(attrName)) {
                auto attr = group.openAttribute(attrName);
                attr.write(strdatatype, type.toStdString());
                attr.close();
            }
            else {
                H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
                // Create new string datatype for attribute                
                auto attr = group.createAttribute(attrName, strdatatype, attr_dataspace);
                attr.write(strdatatype, type.toStdString());
                attr_dataspace.close();
                attr.close();
            }
            strdatatype.close();
        }
        auto GetBlobCount(H5::DataSet data)->uint32_t {
            uint32_t count = 0;
            if (data.attrExists("Count")) {
                auto attr = data.openAttribute("Count");
                attr.read(H5::PredType::NATIVE_INT32, &count);
                attr.close();
            }
            else {
                hsize_t dims[1]{ 1 };
                auto space = H5::DataSpace(1, dims);
                auto attr = data.createAttribute("Count", H5::PredType::NATIVE_INT32, space);

                int32_t ddata[1]{ 0 };
                attr.write(H5::PredType::NATIVE_INT32, ddata);
                space.close();
                attr.close();
            }
            return count;
        }
        auto SetBlobCount(H5::DataSet dataset, int count)->void {
            int32_t data[1]{ count };
            auto attr = dataset.openAttribute("Count");
            attr.write(H5::PredType::NATIVE_INT32, data);
            attr.close();
        }
	};
    TCUMaskWriter::TCUMaskWriter(const QString& path,int time_steps) : d{ new Impl } {
        d->path = path;
        d->time_steps = time_steps;
        QFileInfo info(d->path);
        auto dir = info.dir();
        if(false == dir.exists()) {
            dir.mkpath(info.dir().path());
        }
        if (false == info.exists()) {
            HDF5MutexLocker lock(HDF5Mutex::GetInstance());
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_CREAT | H5F_ACC_RDWR);
            file.close();
        }
    }
    TCUMaskWriter::~TCUMaskWriter() {
        
    }
    auto TCUMaskWriter::SetNumberOfBlob(const int& cnt) -> void {
        d->blobNum = cnt;
    }

    auto TCUMaskWriter::WriteResolution(double resX, double resY, double resZ) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            d->WriteResolution(file, resX, 0);
            d->WriteResolution(file, resY, 1);
            d->WriteResolution(file, resZ, 2);
            file.close();
        }
        catch (H5::Exception&) {
            return false;
        }
        return true;
    }
    auto TCUMaskWriter::WriteSize(int32_t sizeX, int32_t sizeY, int32_t sizeZ) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            d->WriteFullSize(file, sizeX, 0);
            d->WriteFullSize(file, sizeY, 1);
            d->WriteFullSize(file, sizeZ, 2);
            file.close();
        }
        catch (H5::Exception&) {
            return false;
        }
        return true;
    }
    auto TCUMaskWriter::WriteVersion(int major, int minor, int patch) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            d->WriteVersionText(file, major, minor, patch);
            file.close();
        }
        catch (H5::Exception&) {
            return false;
        }
        return true;
    }
    auto TCUMaskWriter::WriteName(const QString& dataID, int nameKey, const QString& name,const QString& type) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            H5::H5File file(d->path.toLocal8Bit().constData(), H5F_ACC_RDWR);
            auto group = d->OpenGroup(file, dataID);
            if (d->SetName(group, nameKey, name)) {
                d->SetType(group, nameKey, type);
            }
            group.close();
            file.close();
        }
        catch (H5::Exception&) {
            return false;
        }
        return true;
    }
    auto TCUMaskWriter::GetBlobCount(const QString& dataID, const QString& name, int timeIndex) -> int32_t {
        int32_t count = 0;
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            auto blobPath = d->path.chopped(4);
            blobPath += "/Blobs/" + dataID +"/"+name;
            const QString timeStamp = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            blobPath += "/" + timeStamp;
            if (!QFileInfo::exists(blobPath)) {
                return count;
            }
            H5::H5File file(blobPath.toLocal8Bit().constData(), H5F_ACC_RDONLY);
            auto dataSet = file.openDataSet("BlobData");
            count = d->GetBlobCount(dataSet);

            dataSet.close();
            file.close();
        }
        catch (H5::Exception&) {
        }
        return count;
    }
    auto TCUMaskWriter::WriteIntAttrb(const QString& dataID, const QString& name, const QString& attrName, int value) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            auto blobPath = d->path.chopped(4);
            blobPath += "/Blobs/" + dataID + "/" + name;
            const QString timeStamp = QString("%1").arg(0, 6, 10, QLatin1Char('0'));
            blobPath += "/" + timeStamp;
            if(!QFileInfo::exists(blobPath)) {
                return false;
            }
            H5::H5File file(blobPath.toLocal8Bit().constData(), H5F_ACC_RDWR);
            auto dataSet = file.openDataSet("BlobData");
            int32_t data[1]{ value };
            if (dataSet.attrExists(attrName.toLocal8Bit().constData())) {
                auto attr = dataSet.openAttribute(attrName.toLocal8Bit().constData());
                attr.write(H5::PredType::NATIVE_INT32, data);
                attr.close();
            }
            else {
                hsize_t dims[1]{ 1 };
                auto space = H5::DataSpace(1, dims);                
                auto attr = dataSet.createAttribute(attrName.toLocal8Bit().constData(), H5::PredType::NATIVE_INT32, space);
                attr.write(H5::PredType::NATIVE_INT32, data);
                space.close();
                attr.close();
            }
            dataSet.close();
            file.close();
        }catch(H5::Exception& e) {            
            std::cout << e.getCDetailMsg() << std::endl;
            return false;
        }
        return true;
    }
    auto TCUMaskWriter::ClearBlobCount(const QString& dataId, const QString& name, int timeIndex) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            auto blobPath = d->path.chopped(4);
            blobPath += ("/Blobs/" + dataId + "/" + name);
            QDir dir(blobPath);
            if (!dir.exists()) {
                dir.mkpath(blobPath);
            }
            const QString timeStamp = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            blobPath += ("/" + timeStamp);
            if (QFileInfo::exists(blobPath)) {
                QFile::remove(blobPath);
            }
            H5::H5File file(blobPath.toLocal8Bit().constData(), H5F_ACC_CREAT | H5F_ACC_RDWR);
            // file.openDataSet("BlobData");
            auto dataSet = d->OpenOrCreateBlobDataSet(file, "BlobData");
            int32_t data[1]{ 0 };
            if (dataSet.attrExists("Count")) {
                auto attr = dataSet.openAttribute("Count");
                attr.write(H5::PredType::NATIVE_INT32, data);
                attr.close();
            }
            else {
                hsize_t dims[1]{ 1 };
                auto space = H5::DataSpace(1, dims);
                auto attr = dataSet.createAttribute("Count", H5::PredType::NATIVE_INT32, space);
                attr.write(H5::PredType::NATIVE_INT32, data);
                space.close();
                attr.close();
            }
            dataSet.close();
            file.close();
        }
        catch (H5::Exception&) {
            return false;
        }
        return true;
    }
    auto TCUMaskWriter::WriteBlob(const QString& dataID, const QString& name, int timeIndex, int blobKey, BoundingBox& bBox, int code) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            auto blobPath = d->path.chopped(4);
            blobPath += ("/Blobs/" + dataID);
            blobPath += ("/" + name);

            const QString timeStamp = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            blobPath += ("/" + timeStamp);
            H5::H5File file(blobPath.toLocal8Bit().constData(), H5F_ACC_RDWR);
            auto dataSet = d->OpenOrCreateBlobDataSet(file, "BlobData");
            auto blobs = d->GetBlobCount(dataSet);            
            auto fileSpace = dataSet.getSpace();
                        
            hsize_t size[2]{ 1, d->columns };
            hsize_t offset[2]{ blobs, 0 };
            fileSpace.selectHyperslab(H5S_SELECT_SET, size, offset);            
            auto memSpace = H5::DataSpace(2, size);

            auto bBoxOffset = bBox.GetOffset();
            auto bBoxSize = bBox.GetSize();
            int data[8]{ blobKey, bBoxOffset.x0, bBoxOffset.y0, bBoxOffset.z0,
                static_cast<int>(bBoxSize.d0),
                static_cast<int>(bBoxSize.d1),
                static_cast<int>(bBoxSize.d2),
                code };
            dataSet.write(data, H5::PredType::NATIVE_INT32, memSpace, fileSpace);
            d->SetBlobCount(dataSet, blobs + 1);
            memSpace.close();
            fileSpace.close();
            dataSet.close();
            file.close();
        }
        catch (H5::Exception& e) {
            std::cout << e.getDetailMsg().c_str() << std::endl;
            return false;
        }
        return true;
    }
    auto TCUMaskWriter::WriteMask(const QString& dataID, const QString& name, int timeIndex, int blobKey, BoundingBox& bBox, std::shared_ptr<uint8_t[]> mask) -> bool {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        H5::Exception::dontPrint();
        try {
            auto maskPath = d->path.chopped(4);
            maskPath += ("/Masks/" + dataID + "/" + name);
            const QString timeStamp = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            maskPath += ("/" + timeStamp);
            QDir dir(maskPath);
            if (!dir.exists()) {
                dir.mkpath(maskPath);
            }
            const QString blob_name = QString("%1").arg(blobKey, 6, 10, QLatin1Char('0'));
            maskPath += ("/" + blob_name);
            if (QFileInfo::exists(maskPath)) {
                //remove existing mask data if exist
                QFile::remove(maskPath);
            }
            H5::H5File file(maskPath.toLocal8Bit().constData(), H5F_ACC_CREAT | H5F_ACC_RDWR);
            {
                const auto bBoxSize = bBox.GetSize();

                hsize_t size[3]{ bBoxSize.d2, bBoxSize.d1, bBoxSize.d0 };
                auto dataSet = d->OpenOrCraeteMaskDataSet(file, "MaskData", size);
                auto dataSpace = H5::DataSpace(3, size);

                hsize_t sss[3];
                dataSet.getSpace().getSimpleExtentDims(sss);
                dataSet.write(mask.get(), H5::PredType::NATIVE_UINT8, dataSpace);
                dataSpace.close();
                dataSet.close();
            }
            file.close();
        }
        catch (H5::Exception&) {
            return false;
        }
        return true;
    }
    auto TCUMaskWriter::ClearWhole(const QString& dataId, const QString& name, int timeIndex) -> bool {
        try {
            auto maskPath = d->path.chopped(4);
            maskPath += "/Masks/" + dataId + "/" + name;
            const QString timeStamp = QString("%1").arg(timeIndex, 6, 10, QLatin1Char('0'));
            maskPath += "/" + timeStamp;
            QDir dir(maskPath);
            if (dir.exists()) {
                //remove every containing masks
                dir.setNameFilters(QStringList() << "*.*");
                dir.setFilter(QDir::Files);
                foreach(QString dirFile, dir.entryList())
                {
                    dir.remove(dirFile);
                }
            }
        }
        catch (H5::Exception&) {
            return false;
        }
        return true;
    }    
}
