#pragma once

#include <memory>
#include <tuple>

#include <QStringList>

#include "VolumeData.h"
#include "BoundingBox.h"
#include "TCMaskIOExport.h"

namespace TC::IO {
    /**
     * \brief Mask Data를 파일로 부터 읽어 들이기
     */
    class TCMaskIO_API TCMaskReader {
    public:
        explicit TCMaskReader(const QString& path = QString());
        virtual ~TCMaskReader();

        /**
         * \brief HDF5 포맷 파일 존재하는지 여부 확인
         * \return true/false
         */
        auto Exist() const->bool;

        /**
          * \brief Mask resolution 정보
          * \return Mask voxel resolution
          */        
        auto GetMaskResolution() const->std::tuple<double, double, double>;
        /**
          * \brief Mask 크기 정보
          * \return Mask 전체 Boundary size
          */        
        auto GetMaskSize() const->std::tuple<int, int, int>;
        /**
         * \brief 데이타 그룹 목록
         * \return 데이타 그룹 목록
         */         
        auto GetDataGroups() const->QStringList;
        /**
         * \brief 특정 데이타 그룹의 Time index 개수
         * \param dataID 데이타 그룹
         * \return time index 개수
         */
        auto GetTimeIndexCount(const QString& dataID) const->int32_t;
        /**
         * \brief 특정 데이타의 blob 개수
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \return blob 개수
         */
        auto GetBlobCount(const QString& dataID, int timeIndex) const->int32_t;

        /**
         * \brief 특정 Blob 정보
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \param blobIndex blob index
         * \return tuple로 blob key와 blob을 싸고 있는 bounding box 그리고 code
         */
        auto ReadBlob(const QString& dataID, int timeIndex, int blobIndex) const->std::tuple<int, BoundingBox, int>;
        /**
         * \brief Mask 이미지
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \param blobIndex blob index
         * \return 
         */
        auto ReadMask(const QString& dataID, int timeIndex, int blobIndex) const->std::shared_ptr<uint8_t[]>;        
        /**
         * \brief Mask 이름 목록
         * \param dataID 데이타 그룹         
         * \return
         */
        auto GetNameList(const QString& dataID)const->QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}