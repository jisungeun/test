#pragma once

#include <memory>

#include <QString>

#include "VolumeData.h"
#include "BoundingBox.h"
#include "TCMaskIOExport.h"

namespace TC::IO {
    class TCMaskIO_API TCUMaskWriter {
    public:
        explicit TCUMaskWriter(const QString& path,int time_steps);
        virtual ~TCUMaskWriter();

        auto WriteResolution(double resX, double resY, double resZ)->bool;
        auto WriteSize(int32_t sizeX, int32_t sizeY, int32_t sizeZ)->bool;        
        auto WriteVersion(int major, int minor, int patch)->bool;
        auto WriteName(const QString& dataID, int nameKey, const QString& name,const QString& type)->bool;        
        auto WriteBlob(const QString& dataID, const QString& name, int timeIndex, int blobKey, BoundingBox& bBox, int code)->bool;
        auto WriteMask(const QString& dataID, const QString& name, int timeIndex, int blobKey, BoundingBox& bBox, std::shared_ptr<uint8_t[]> mask)->bool;                
        auto GetBlobCount(const QString& dataID,const QString& name, int timeIndex)->int32_t;
        auto ClearBlobCount(const QString& dataId,const QString& name, int timeIndex)->bool;
        auto ClearWhole(const QString& dataId,const QString& name, int timeIndex)->bool;
        auto SetNumberOfBlob(const int& cnt)->void;

        //Custom Attributes
        auto WriteIntAttrb(const QString& dataID, const QString& name, const QString& attrName, int value)->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}