#pragma once

#include <memory>
#include <tuple>

#include <QStringList>


#include "VolumeData.h"
#include "BoundingBox.h"
#include "TCMaskIOExport.h"

namespace TC::IO {
    class TCMaskIO_API TCHMaskReader {
    public:
        explicit TCHMaskReader(const QString& path = QString());
        virtual ~TCHMaskReader();

        auto Exist()const->bool;
        auto isValid()const->bool;

        auto GetMaskResolution()const->std::tuple<double, double, double>;
        auto GetMaskSize()const->std::tuple<int, int, int>;
        auto GetTimeIndexCount(const QString& dataID)const->int32_t;
        auto GetBlobCount(const QString& dataID, int timeIndex)const->int32_t;
        auto ReadBlob(const QString& dataID, int timeIndex, int blobIndex)const->std::tuple<int, BoundingBox, int>;
        auto ReadMask(const QString& dataID, int timeIndex, int blobIndex)const->std::shared_ptr<uint8_t[]>;
        auto GetNameList(const QString& dataID)const->QStringList;
        auto GetVersion()const->QString;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}