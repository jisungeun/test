#pragma once

#include "TCMask.h"

#include "TCMaskIOExport.h"

namespace TC::IO {
    class TCMaskIO_API TCMaskWriterPort {
    public:
        explicit TCMaskWriterPort();
        virtual ~TCMaskWriterPort();

        auto Modify(TCMask::Pointer data, const QString& path,const QString& modalityName, const QString& organName, int nameIdx)->bool;
        auto Write(TCMask::Pointer data, const QString& path,const QString& modalityName, const QString& organName, int nameIdx)->bool;
        auto WriteIntAttrb(TCMask::Pointer data,const QString& path,const QString& modalityName,const QString& organName, const QString& attrName, int value)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}