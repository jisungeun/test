#pragma once

#include <memory>

#include <QString>

#include "VolumeData.h"
#include "BoundingBox.h"
#include "TCMaskIOExport.h"

namespace TC::IO {
    class TCMaskIO_API TCHMaskWriter {
    public:
        explicit TCHMaskWriter(const QString& path, uint32_t maxCounts = 100);
        virtual ~TCHMaskWriter();

        auto WriteResolution(double resX, double resY, double resZ)->bool;
        auto WriteSize(int32_t sizeX, int32_t sizeY, int32_t sizeZ)->bool;
        auto WriteName(const QString& dataID, int nameKey, const QString& name = QString())->bool;
        auto WriteVersion(int major, int minor, int patch)->bool;
        auto WriteBlob(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox, int code)->bool;
        auto WriteMask(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox, std::shared_ptr<uint8_t> mask)->bool;

        auto GetBlobCount(const QString& dataID, int timeIndex)->int32_t;
        auto ClearBlobCount(const QString& dataId, int timeIndex)->bool;
        auto ClearWhole(const QString& dataId, int timeIndex)->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}