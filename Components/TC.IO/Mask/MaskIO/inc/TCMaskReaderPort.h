#pragma once

#include <TCMask.h>
#include <memory>
#include <tuple>

#include <QStringList>

#include "TCMaskIOExport.h"

namespace TC::IO {		
	class TCMaskIO_API TCMaskReaderPort {		
	public:		
		explicit TCMaskReaderPort();
		virtual ~TCMaskReaderPort();

		auto Read(const QString& path, const QString& modalityName, const QString& organName, int time_idx)->TCMask::Pointer;
		auto ReadIntAttrb(const QString& path, const QString& modalityName, const QString& organName, const QString& attrbName)->int;
		auto GetVersion(const QString& path)const ->std::tuple<int,int,int>;//major, minor, patch
		auto GetMaskList(const QString& path,const QString& modalityName) ->QList<MaskMeta>;
		
	protected:		
		auto ReadOld(const QString& path, int time_idx, const QString& organName)->TCMask::Pointer;		
		auto Read100(const QString& path, int time_idx, const QString& organName)->TCMask::Pointer;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}