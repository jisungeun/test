#pragma once

#include <memory>

#include <QString>

#include "VolumeData.h"
#include "BoundingBox.h"
#include "TCMaskIOExport.h"

namespace TC::IO {
    /**
     * \brief Mask Data 를 기록하는 파일
     */
    class TCMaskIO_API TCMaskWriter {
    public:
        explicit TCMaskWriter(const QString& path, uint32_t maxCounts=100);
        virtual ~TCMaskWriter();

        /**
         * \brief Mask의 resolution을 기록
         * \param X axis resolution
         * \param Y axis resolution
         * \param Z axis resolution
         * \return true/false
         */
        auto WriteResolution(double resX,double resY,double resZ)->bool;
        /**
         * \brief Mask의 전체 크기를 기록
         * \param X axis size
         * \param Y axis size
         * \param Z axis size, for 2D size is 1         
         * \return true/false
         */
        auto WriteSize(int32_t sizeX,int32_t sizeY,int32_t sizeZ)->bool;
        /**
         * \brief Blob 이름 정보를 기록
         * \param dataID 데이타 그룹
         * \param nameKey order of name
         * \param name name of blob
         * \return true/false
         */
        auto WriteName(const QString& dataID, int nameKey, const QString& name = QString())->bool;
        /**
         * \brief Blob 정보를 기록
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \param blobKey key for blob
         * \param bBox bounding box
         * \param code code
         * \return true/false
         */
        auto WriteBlob(const QString& dataID,int timeIndex, int blobKey, BoundingBox& bBox, int code)->bool;        
        /**
         * \brief Blob 정보를 기록
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \param blobKey key for blob
         * \param bBox bounding box
         * \param code code
         * \return true/false
         */        
        auto UpdateBlob(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox, int code)->bool;
        /**
         * \brief Mask volume writer
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \param blobKey key for blob
         * \param bBox bounding box
         * \param mask 3D mask data         
         * \return true/false
         */
        auto WriteMask(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox, std::shared_ptr<uint8_t> mask)->bool;
        /**
         * \brief Mask volume updater
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \param blobKey key for blob
         * \param bBox bounding box
         * \param mask 3D mask data
         * \return true/false
         */
        auto UpdateMask(const QString& dataID, int timeIndex, int blobKey, BoundingBox& bBox, std::shared_ptr<uint8_t> mask)->bool;
        /**
         * \brief Blob 개수 구하기
         * \param dataID 데이타 그룹
         * \param timeIndex time index
         * \return blob 개수
         */        
        auto GetBlobCount(const QString& dataID, int timeIndex)->int32_t;
        /**
         *\brief BlobCount 초기화
         *\param dataID 데이터 그룹
         *\param timeIndex time index
         *\return true/false
         */
        auto ClearBlobCount(const QString& dataID, int timeIndex)-> bool;

        auto ClearWhole(const QString& dataID, int timeIndex)->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}