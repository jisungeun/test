#include "TcfLdmConverter.h"

#include "H5Cpp.h"
#include "TcfLdmGroupAttributeCopier.h"
#include "Hdf5DataSetPathScanner.h"
#include "Hdf5Utilities.h"
#include "TcfDataSetLdmConverter.h"

namespace TC::IO::LdmConverting {
    struct TcfLdmConverter::Impl {
        Impl() = default;
        ~Impl() = default;
        std::string inputTcfPath{};
        std::string outputTcfPath{};
        Dimension tileUnitDimensionHt{ 256, 256, 128 };
        Dimension tileUnitDimensionFl{ 256, 256, 128 };
        Dimension tileUnitDimensionBf{ 256, 256 };
        Dimension tileUnitDimensionPhase{ 256, 256 };

        auto WriteIsLdmAttribute(const H5::H5File& outputTcf)->void;
    };

    auto TcfLdmConverter::Impl::WriteIsLdmAttribute(const H5::H5File& outputTcf) -> void {
        auto dataGroup = outputTcf.openGroup("Data");
        HDF5Utilities::CreateAndWriteAttributeInt64(dataGroup, "isLDM", 1);

        dataGroup.close();
    }

    TcfLdmConverter::TcfLdmConverter()
        : d(new Impl()) {
    }

    TcfLdmConverter::~TcfLdmConverter() = default;

    auto TcfLdmConverter::SetInOutTcfPaths(const QString& inputTcfPath, const QString& outputTcfPath) -> void {
        d->inputTcfPath = std::string(inputTcfPath.toLocal8Bit().constData());
        d->outputTcfPath = std::string(outputTcfPath.toLocal8Bit().constData());
    }

    auto TcfLdmConverter::Write() -> void {        
        auto inputTcf = H5::H5File(d->inputTcfPath, H5F_ACC_RDONLY);
        auto outputTcf = H5::H5File(d->outputTcfPath, H5F_ACC_TRUNC);

        LdmWriting::TcfLdmGroupAttributeCopier::Copy(inputTcf,outputTcf);
        HDF5Utilities::Hdf5DataSetPathScanner hdf5DataSetPathScanner;
        const auto dataSetPathList = hdf5DataSetPathScanner.Scan(inputTcf);

        for (const auto& dataSetPath : dataSetPathList) {
            Convert(inputTcf, outputTcf, dataSetPath);
        }

        d->WriteIsLdmAttribute(outputTcf);

        inputTcf.close();
        outputTcf.close();
    }

    auto TcfLdmConverter::SetHtTileSize(const uint32_t& tileSizeX, const uint32_t& tileSizeY,
        const uint32_t& tileSizeZ) -> void {
        d->tileUnitDimensionHt = Dimension(tileSizeX, tileSizeY, tileSizeZ);
    }

    auto TcfLdmConverter::SetFlTileSize(const uint32_t& tileSizeX, const uint32_t& tileSizeY,
        const uint32_t& tileSizeZ) -> void {
        d->tileUnitDimensionFl = Dimension(tileSizeX, tileSizeY, tileSizeZ);
    }

    auto TcfLdmConverter::SetBfTileSize(const uint32_t& tileSizeX, const uint32_t& tileSizeY) -> void {
        d->tileUnitDimensionBf = Dimension(tileSizeX, tileSizeY);
    }

    auto TcfLdmConverter::SetPhaseTileSize(const uint32_t& tileSizeX, const uint32_t& tileSizeY) -> void {
        d->tileUnitDimensionPhase = Dimension(tileSizeX, tileSizeY);
    }

    auto TcfLdmConverter::Convert(const H5::H5File& inputTcf, const H5::H5File& outputTcf,
        const QString& dataSetPath) -> void {        
        const auto groupPath = GetGroupPath(dataSetPath);
        const auto dataSetName = GetDataSetName(dataSetPath);

        if (groupPath.contains("View")) {
            return;
        }

        auto inputPositionGroup = inputTcf.openGroup(ToStdString(groupPath));
        auto inputDataSet = inputPositionGroup.openDataSet(ToStdString(dataSetName));

        auto outputPositionGroup = outputTcf.openGroup(ToStdString(groupPath));

        const auto dataSetIsHt = dataSetPath.contains("/Data/3D/");
        const auto dataSetIsHtMip = dataSetPath.contains("/Data/2DMIP/");
        const auto dataSetIsFl = dataSetPath.contains("/Data/3DFL/");
        const auto dataSetIsFlMip = dataSetPath.contains("/Data/2DFLMIP/");
        const auto dataSetIsBf = dataSetPath.contains("/Data/BF/");
        const auto dataSetIsPhase = dataSetPath.contains("/Data/2D/");

        TcfDataSetLdmConverter tcfDataSetLdmConverter;
        LdmWriting::LdmProperty ldmProperty;
        if(dataSetIsHt) {
            const auto tileDimension = d->tileUnitDimensionHt;
            ldmProperty = tcfDataSetLdmConverter.Convert3D(inputDataSet, outputPositionGroup, tileDimension);
        } else if(dataSetIsHtMip) {
            const auto tileDimension = Dimension(d->tileUnitDimensionHt.X(), d->tileUnitDimensionHt.Y());
            ldmProperty = tcfDataSetLdmConverter.Convert2D(inputDataSet, outputPositionGroup, tileDimension);
        } else if (dataSetIsFl) {
            const auto tileDimension = d->tileUnitDimensionFl;
            ldmProperty = tcfDataSetLdmConverter.Convert3D(inputDataSet, outputPositionGroup, tileDimension);
        } else if (dataSetIsFlMip) {
            const auto tileDimension = Dimension(d->tileUnitDimensionFl.X(), d->tileUnitDimensionFl.Y());
            ldmProperty = tcfDataSetLdmConverter.Convert2D(inputDataSet, outputPositionGroup, tileDimension);
        } else if (dataSetIsBf) {
            const auto tileDimension = d->tileUnitDimensionBf;
            ldmProperty = tcfDataSetLdmConverter.Convert2DStack(inputDataSet, outputPositionGroup, tileDimension);
        } else if (dataSetIsPhase) {
            const auto tileDimension = d->tileUnitDimensionPhase;
            ldmProperty = tcfDataSetLdmConverter.Convert2DStack(inputDataSet, outputPositionGroup, tileDimension);
        }

        auto outputConvertedGroup = outputPositionGroup.openGroup(ToStdString(dataSetName));
        HDF5Utilities::CopyAllAttributes(inputDataSet, outputConvertedGroup);
        WriteLdmAttributes(outputConvertedGroup, ldmProperty);

        outputPositionGroup.close();
        outputConvertedGroup.close();
        inputDataSet.close();
        inputPositionGroup.close();
    }

    auto TcfLdmConverter::GetGroupPath(const QString& dataSetPath) -> QString {
        return dataSetPath.left(dataSetPath.lastIndexOf("/"));
    }

    auto TcfLdmConverter::GetDataSetName(const QString& dataSetPath) -> QString {
        return dataSetPath.right(dataSetPath.size() - dataSetPath.lastIndexOf("/") - 1);
    }

    auto TcfLdmConverter::ToStdString(const QString& qString) -> std::string {
        return std::string(qString.toLocal8Bit().constData());
    }

    auto TcfLdmConverter::WriteLdmAttributes(const H5::Group& group, const LdmWriting::LdmProperty& ldmProperty) -> void {
        HDF5Utilities::CreateAndWriteAttributeInt64(group, "NumberOfTiles", static_cast<int64_t>(ldmProperty.GetNumberOfTiles()));

        const auto tileSize = ldmProperty.GetTileSize();
        const auto tileSizeIs2D = (tileSize.GetDimensionality() == 2);
        const auto tileSizeIs3D = (tileSize.GetDimensionality() == 3);
        if (tileSizeIs2D) {
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "TileSizeX", static_cast<int64_t>(tileSize.X()));
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "TileSizeY", static_cast<int64_t>(tileSize.Y()));
        } else if (tileSizeIs3D) {
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "TileSizeX", static_cast<int64_t>(tileSize.X()));
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "TileSizeY", static_cast<int64_t>(tileSize.Y()));
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "TileSizeZ", static_cast<int64_t>(tileSize.Z()));
        }

        const auto dataDimension = ldmProperty.GetDataDimension();
        const auto dataDimensionIs2D = (dataDimension.GetDimensionality() == 2);
        const auto dataDimensionIs3D = (dataDimension.GetDimensionality() == 3);
        if (dataDimensionIs2D) {
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "DataSizeX", static_cast<int64_t>(dataDimension.X()));
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "DataSizeY", static_cast<int64_t>(dataDimension.Y()));
        } else if (dataDimensionIs3D) {
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "DataSizeX", static_cast<int64_t>(dataDimension.X()));
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "DataSizeY", static_cast<int64_t>(dataDimension.Y()));
            HDF5Utilities::CreateAndWriteAttributeInt64(group, "DataSizeZ", static_cast<int64_t>(dataDimension.Z()));
        }
    }
}
