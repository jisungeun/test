#include "TcfDataSetLdmConverter.h"

#include "Hdf5DataSetTileWriter.h"
#include "LdmWritingAlgorithm.h"
#include "Octree3DConfigurator.h"
#include "StorageOnePickSampler.h"
#include "Hdf5Utilities.h"
#include "Octree2DConfigurator.h"
#include "Octree2DStackConfigurator.h"
#include "Hdf5DataSetHybridSamplerReader.h"

namespace TC::IO::LdmConverting {
    struct LdmConverting::TcfDataSetLdmConverter::Impl {
        Impl() = default;
        ~Impl() = default;

    };

    TcfDataSetLdmConverter::TcfDataSetLdmConverter()
        : d(new Impl()) {
    }

    TcfDataSetLdmConverter::~TcfDataSetLdmConverter() = default;

    auto TcfDataSetLdmConverter::Convert3D(H5::DataSet& srcDataSet, H5::Group& destPositionGroup,        
        const Dimension& tileDimension) -> LdmWriting::LdmProperty {        
        CheckTileDimensionality(tileDimension, 3);

        const auto nameOfDataSet = GetNameFromObjectPath(srcDataSet.getObjName());
        auto destDataSetGroup = destPositionGroup.createGroup(nameOfDataSet);
        const auto dataSetDimension = HDF5Utilities::GetDimension(srcDataSet);

        auto octree3DConfigurator = new LdmCore::Octree3DConfigurator(dataSetDimension, tileDimension);
        auto hdf5DataSetTileWriter = new LdmWriting::Hdf5DataSetTileWriter(destDataSetGroup);

        auto storageOnePickSampler = new LdmWriting::StorageOnePickSampler;

        auto hdf5DataSetHybridSamplerReader = LdmReading::ISamplerReader::Pointer(
                new LdmReading::Hdf5DataSetHybridSamplerReader(srcDataSet, destDataSetGroup));
        storageOnePickSampler->SetSamplerReader(hdf5DataSetHybridSamplerReader);

        const auto ldmProperty = Convert(octree3DConfigurator, hdf5DataSetTileWriter, storageOnePickSampler);

        destDataSetGroup.close();
        return ldmProperty;
    }

    auto TcfDataSetLdmConverter::Convert2D(H5::DataSet& srcDataSet, H5::Group& destPositionGroup,
        const Dimension& tileDimension) -> LdmWriting::LdmProperty {        
        CheckTileDimensionality(tileDimension, 2);

        const auto nameOfDataSet = GetNameFromObjectPath(srcDataSet.getObjName());
        auto destDataSetGroup = destPositionGroup.createGroup(nameOfDataSet);
        const auto dataSetDimension = HDF5Utilities::GetDimension(srcDataSet);

        auto octree2DConfigurator = new LdmCore::Octree2DConfigurator(dataSetDimension, tileDimension);
        auto hdf5DataSetTileWriter = new LdmWriting::Hdf5DataSetTileWriter(destDataSetGroup);

        auto storageOnePickSampler = new LdmWriting::StorageOnePickSampler;

        auto hdf5DataSetHybridSamplerReader = LdmReading::ISamplerReader::Pointer(
            new LdmReading::Hdf5DataSetHybridSamplerReader(srcDataSet, destDataSetGroup));
        storageOnePickSampler->SetSamplerReader(hdf5DataSetHybridSamplerReader);

        const auto ldmProperty = Convert(octree2DConfigurator, hdf5DataSetTileWriter, storageOnePickSampler);
        return ldmProperty;
    }

    auto TcfDataSetLdmConverter::Convert2DStack(H5::DataSet& srcDataSet, H5::Group& destPositionGroup,
        const Dimension& tileDimension) -> LdmWriting::LdmProperty {        
        CheckTileDimensionality(tileDimension, 2);

        const auto nameOfDataSet = GetNameFromObjectPath(srcDataSet.getObjName());
        auto destDataSetGroup = destPositionGroup.createGroup(nameOfDataSet);
        const auto dataSetDimension = HDF5Utilities::GetDimension(srcDataSet);

        auto octree2DStackConfigurator = new LdmCore::Octree2DStackConfigurator(dataSetDimension, tileDimension);
        auto hdf5DataSetTileWriter = new LdmWriting::Hdf5DataSetTileWriter(destDataSetGroup);

        auto storageOnePickSampler = new LdmWriting::StorageOnePickSampler;

        auto hdf5DataSetHybridSamplerReader = LdmReading::ISamplerReader::Pointer(
            new LdmReading::Hdf5DataSetHybridSamplerReader(srcDataSet, destDataSetGroup));
        storageOnePickSampler->SetSamplerReader(hdf5DataSetHybridSamplerReader);

        const auto ldmProperty = Convert(octree2DStackConfigurator, hdf5DataSetTileWriter, storageOnePickSampler);
        return ldmProperty;
    }

    auto TcfDataSetLdmConverter::CheckTileDimensionality(const Dimension& tileDimension,
        const int32_t& answerDimensionality) -> bool {
        const auto tileDimensionIsInvalid = 
            (tileDimension.GetDimensionality() != static_cast<size_t>(answerDimensionality));
        return tileDimensionIsInvalid;
    }

    auto TcfDataSetLdmConverter::Convert(LdmCore::ILdmConfigurator* ldmConfigurator,
        LdmWriting::ITileWriter* tileWriter, LdmWriting::IDataSampler* dataSampler) -> LdmWriting::LdmProperty {
        LdmCore::ILdmConfigurator::Pointer ldmConfiguratorPointer(ldmConfigurator);
        LdmWriting::ITileWriter::Pointer tileWriterPointer(tileWriter);
        LdmWriting::IDataSampler::Pointer dataSamplerPointer(dataSampler);
        LdmWriting::LdmWritingAlgorithm ldmWritingAlgorithm(ldmConfiguratorPointer, tileWriterPointer, dataSamplerPointer);

        auto ldmPropertyPointer = new LdmWriting::LdmProperty;
        LdmWriting::LdmProperty::Pointer ldmProperty(ldmPropertyPointer);
        ldmWritingAlgorithm.Write(ldmProperty);

        return *ldmPropertyPointer;
    }

    auto TcfDataSetLdmConverter::GetNameFromObjectPath(const std::string& objectPath) -> std::string {
        const auto path = QString::fromStdString(objectPath);
        const auto pathSplitList = path.split("/");

        return pathSplitList.last().toStdString();
    }
}
