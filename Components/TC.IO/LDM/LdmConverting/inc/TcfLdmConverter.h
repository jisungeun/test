#pragma once
#include <memory>
#include <QString>

#include "TCLdmConvertingExport.h"
#pragma warning(push)
#pragma warning(disable:4268)
#include "H5Cpp.h"
#pragma warning(pop)
#include "LdmProperty.h"

namespace TC::IO::LdmConverting {
    class TCLdmConverting_API TcfLdmConverter final{
    public:
        TcfLdmConverter();
        ~TcfLdmConverter();
        auto SetInOutTcfPaths(const QString& inputTcfPath, const QString& outputTcfPath)->void;
        auto Write() -> void;

        auto SetHtTileSize(const uint32_t& tileSizeX, const uint32_t& tileSizeY, const uint32_t& tileSizeZ)->void;
        auto SetFlTileSize(const uint32_t& tileSizeX, const uint32_t& tileSizeY, const uint32_t& tileSizeZ)->void;
        auto SetBfTileSize(const uint32_t& tileSizeX, const uint32_t& tileSizeY)->void;
        auto SetPhaseTileSize(const uint32_t& tileSizeX, const uint32_t& tileSizeY)->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto Convert(const H5::H5File& inputTcf, const H5::H5File& outputTcf, const QString& dataSetPath)->void;
        static auto GetGroupPath(const QString& dataSetPath)->QString;
        static auto GetDataSetName(const QString& dataSetPath)->QString;
        static auto ToStdString(const QString& qString)->std::string;
        static auto WriteLdmAttributes(const H5::Group& group, const LdmWriting::LdmProperty& ldmProperty)->void;
    };
}
