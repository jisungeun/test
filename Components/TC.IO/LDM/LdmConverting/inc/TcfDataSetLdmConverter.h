#pragma once
#include <memory>


#include "TCLdmConvertingExport.h"
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include "Dimension.h"
#include "LdmProperty.h"
#include "ILdmConfigurator.h"
#include "ITileWriter.h"
#include "IDataSampler.h"

namespace TC::IO::LdmConverting {
    class TCLdmConverting_API TcfDataSetLdmConverter {
    public:
        TcfDataSetLdmConverter();
        ~TcfDataSetLdmConverter();

        auto Convert3D(H5::DataSet& srcDataSet, H5::Group& destPositionGroup,
            const Dimension& tileDimension)-> LdmWriting::LdmProperty;
        auto Convert2D(H5::DataSet& srcDataSet, H5::Group& destPositionGroup,
            const Dimension& tileDimension)-> LdmWriting::LdmProperty;
        auto Convert2DStack(H5::DataSet& srcDataSet, H5::Group& destPositionGroup,
            const Dimension& tileDimension)-> LdmWriting::LdmProperty;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto CheckTileDimensionality(const Dimension& tileDimension, 
            const int32_t& answerDimensionality)->bool;
        static auto Convert(LdmCore::ILdmConfigurator* ldmConfigurator, LdmWriting::ITileWriter* tileWriter, LdmWriting::IDataSampler* dataSampler)
            -> LdmWriting::LdmProperty;
        static auto GetNameFromObjectPath(const std::string& objectPath)->std::string;
    };
}
