#include <catch2/catch.hpp>
#include <QFile>

#include "H5Cpp.h"

#include "TcfLdmConverter.h"

#include "MemoryCheckUtility.h"
using namespace TC::IO::LdmConverting;
using namespace TC::IO;

namespace TcfLdmConverterTest {
    auto CreateAttribute(H5::H5Object& object, const std::string& name)->void {
        auto attribute = object.createAttribute(name, H5::PredType::NATIVE_INT64, H5S_SCALAR);
        const int64_t attributeValue = 1;
        attribute.write(H5::PredType::NATIVE_INT64, &attributeValue);
        attribute.close();
    }

    auto CheckAttribute(H5::H5Object& object, const std::string& name)->bool {
        auto attribute = object.openAttribute(name);
        int64_t attributeValue{};
        attribute.read(H5::PredType::NATIVE_INT64, &attributeValue);
        attribute.close();
        return (attributeValue == 1);
    }

    auto CheckAttribute(H5::H5Object& object, const std::string& groupPath, const std::string& name)->bool {
        auto group = object.openGroup(groupPath);
        auto attribute = group.openAttribute(name);
        int64_t attributeValue{};
        attribute.read(H5::PredType::NATIVE_INT64, &attributeValue);
        attribute.close();
        group.close();
        return (attributeValue == 1);
    }

    auto CheckAttributeExistence(H5::H5Object& object, const std::string& groupPath, const std::string& name)->bool {
        auto group = object.openGroup(groupPath);
        const auto attributeExists = group.attrExists(name);
        group.close();
        return attributeExists;
    }

    auto GenerateDummyTCF()->QString {
        if (QFile::exists("dummyTcf.h5")) return "dummyTcf.h5";

        H5::H5File dummyTcf("dummyTcf.h5", H5F_ACC_TRUNC);
        CreateAttribute(dummyTcf, "topAttribute");

        auto groupData = dummyTcf.createGroup("Data");
        {
            auto group3D = groupData.createGroup("3D");
            {
                CreateAttribute(group3D, "group3DAttribute");

                const hsize_t rank = 3;
                const hsize_t dims[rank] = { 210,200,200 };
                H5::DataSpace dataSpace3D(rank, dims);
                auto dataSet000000 = group3D.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpace3D);
                auto dataSet000001 = group3D.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpace3D);
                dataSpace3D.close();

                CreateAttribute(dataSet000000, "000000Attribute");
                CreateAttribute(dataSet000001, "000001Attribute");

                dataSet000000.close();
                dataSet000001.close();
            }
            group3D.close();

            auto group2DMIP = groupData.createGroup("2DMIP");
            {
                CreateAttribute(group2DMIP, "group2DMIPAttribute");

                const hsize_t rank = 2;
                const hsize_t dims[rank] = { 200,200 };
                H5::DataSpace dataSpace2DMIP(rank, dims);
                auto dataSet000000 = group2DMIP.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpace2DMIP);
                auto dataSet000001 = group2DMIP.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpace2DMIP);
                dataSpace2DMIP.close();

                CreateAttribute(dataSet000000, "000000Attribute");
                CreateAttribute(dataSet000001, "000001Attribute");

                dataSet000000.close();
                dataSet000001.close();
            }
            group2DMIP.close();

            auto group3DFL = groupData.createGroup("3DFL");
            {
                CreateAttribute(group3DFL, "group3DFLAttribute");

                auto groupCH0 = group3DFL.createGroup("CH0");
                {
                    CreateAttribute(groupCH0, "groupCH0Attribute");

                    const hsize_t rank = 3;
                    const hsize_t dims[rank] = { 120, 150, 150 };
                    H5::DataSpace dataSpaceCH0(rank, dims);
                    auto dataSet000000 = groupCH0.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH0);
                    auto dataSet000001 = groupCH0.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH0);
                    dataSpaceCH0.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }
                groupCH0.close();

                auto groupCH1 = group3DFL.createGroup("CH1");
                {
                    CreateAttribute(groupCH1, "groupCH1Attribute");

                    const hsize_t rank = 3;
                    const hsize_t dims[rank] = { 120, 150, 150 };
                    H5::DataSpace dataSpaceCH1(rank, dims);
                    auto dataSet000000 = groupCH1.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH1);
                    auto dataSet000001 = groupCH1.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH1);
                    dataSpaceCH1.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }
                groupCH1.close();

                auto groupCH2 = group3DFL.createGroup("CH2");
                {
                    CreateAttribute(groupCH2, "groupCH2Attribute");

                    const hsize_t rank = 3;
                    const hsize_t dims[rank] = { 120, 150, 150 };
                    H5::DataSpace dataSpaceCH2(rank, dims);
                    auto dataSet000000 = groupCH2.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH2);
                    auto dataSet000001 = groupCH2.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH2);
                    dataSpaceCH2.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }
                groupCH2.close();


            }
            group3DFL.close();

            auto group2DFLMIP = groupData.createGroup("2DFLMIP");
            {
                CreateAttribute(group2DFLMIP, "group2DFLMIPAttribute");
 
                auto groupCH0 = group2DFLMIP.createGroup("CH0");
                {
                    CreateAttribute(groupCH0, "groupCH0Attribute");

                    const hsize_t rank = 2;
                    const hsize_t dims[rank] = { 150, 150 };
                    H5::DataSpace dataSpaceCH0(rank, dims);
                    auto dataSet000000 =
                        groupCH0.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH0);
                    auto dataSet000001 =
                        groupCH0.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH0);
                    groupCH0.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }

                auto groupCH1 = group2DFLMIP.createGroup("CH1");
                {
                    CreateAttribute(groupCH1, "groupCH1Attribute");

                    const hsize_t rank = 2;
                    const hsize_t dims[rank] = { 150, 150 };
                    H5::DataSpace dataSpaceCH1(rank, dims);
                    auto dataSet000000 =
                        groupCH1.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH1);
                    auto dataSet000001 =
                        groupCH1.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH1);
                    dataSpaceCH1.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }

                auto groupCH2 = group2DFLMIP.createGroup("CH2");
                {
                    CreateAttribute(groupCH2, "groupCH2Attribute");

                    const hsize_t rank = 2;
                    const hsize_t dims[rank] = { 150, 150 };
                    H5::DataSpace dataSpaceCH2(rank, dims);
                    auto dataSet000000 =
                        groupCH2.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpaceCH2);
                    auto dataSet000001 =
                        groupCH2.createDataSet("000001", H5::PredType::NATIVE_UINT16, dataSpaceCH2);
                    dataSpaceCH2.close();

                    CreateAttribute(dataSet000000, "000000Attribute");
                    CreateAttribute(dataSet000001, "000001Attribute");

                    dataSet000000.close();
                    dataSet000001.close();
                }
            }
            group2DFLMIP.close();

            auto group2D = groupData.createGroup("2D");
            {
                CreateAttribute(group2D, "group2DAttribute");

                const hsize_t rank = 3;
                const hsize_t dims[rank] = { 60, 100, 100 };
                H5::DataSpace dataSpace2D(rank, dims);
                auto dataSet000000 = group2D.createDataSet("000000", H5::PredType::NATIVE_FLOAT, dataSpace2D);
                auto dataSet000001 = group2D.createDataSet("000001", H5::PredType::NATIVE_FLOAT, dataSpace2D);
                dataSpace2D.close();

                CreateAttribute(dataSet000000, "000000Attribute");
                CreateAttribute(dataSet000001, "000001Attribute");

                dataSet000000.close();
                dataSet000001.close();
            }
            group2D.close();

            auto groupBF = groupData.createGroup("BF");
            {
                CreateAttribute(groupBF, "groupBFAttribute");

                const hsize_t rank = 3;
                const hsize_t dims[rank] = { 3, 210, 210};
                H5::DataSpace dataSpaceBF(rank, dims);
                auto dataSet000000 = groupBF.createDataSet("000000", H5::PredType::NATIVE_UINT8, dataSpaceBF);
                auto dataSet000001 = groupBF.createDataSet("000001", H5::PredType::NATIVE_UINT8, dataSpaceBF);
                dataSpaceBF.close();

                CreateAttribute(dataSet000000, "000000Attribute");
                CreateAttribute(dataSet000001, "000001Attribute");

                dataSet000000.close();
                dataSet000001.close();
            }
            groupBF.close();
        }
        groupData.close();

        auto groupInfo = dummyTcf.createGroup("Info");
        {
            auto groupAnnotation = groupInfo.createGroup("Annotation");
            {
                CreateAttribute(groupAnnotation, "groupAnnotationAttribute");
            }
            groupAnnotation.close();

            auto groupDevice = groupInfo.createGroup("Device");
            {
                CreateAttribute(groupDevice, "groupDeviceAttribute");
            }
            groupDevice.close();

            auto groupImaging = groupInfo.createGroup("Imaging");
            {
                CreateAttribute(groupImaging, "groupImagingAttribute");
            }
            groupImaging.close();

            auto groupTile = groupInfo.createGroup("Tile");
            {
                CreateAttribute(groupTile, "groupTileAttribute");
            }
            groupTile.close();
        }
        groupInfo.close();

        auto groupView = dummyTcf.createGroup("View");
        {
            auto level01Group = groupView.createGroup("Level01");
            level01Group.close();
        }
        groupView.close();

        dummyTcf.close();

        return "dummyTcf.h5";
    }


    TEST_CASE("TcfLdmConverter") {
        const auto inputTcfPath = GenerateDummyTCF();
        const QString outputFilePath = "dummyLdm.TCF";
        SECTION("Write()") {
            TcfLdmConverter tcfLdmConverter;
            tcfLdmConverter.SetInOutTcfPaths(inputTcfPath, outputFilePath);
            tcfLdmConverter.Write();

            H5::H5File outputFile(outputFilePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

            CHECK(CheckAttribute(outputFile, "topAttribute"));

            CHECK(outputFile.exists("/Data"));
            CHECK(CheckAttributeExistence(outputFile, "/Data", "isLDM"));
            CHECK(CheckAttribute(outputFile, "/Data", "isLDM"));

            CHECK(outputFile.exists("/Data/2D"));
            CHECK(CheckAttribute(outputFile, "/Data/2D", "group2DAttribute"));

            CHECK(outputFile.exists("/Data/2D/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/2D/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2D/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2D/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2D/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/2D/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/2D/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2D/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2D/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2D/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/2DFLMIP"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP", "group2DFLMIPAttribute"));

            CHECK(outputFile.exists("/Data/2DFLMIP/CH0"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH0", "groupCH0Attribute"));
            CHECK(outputFile.exists("/Data/2DFLMIP/CH0/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH0/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH0/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH0/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH0/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/2DFLMIP/CH0/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH0/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH0/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH0/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH0/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/2DFLMIP/CH1"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH1", "groupCH1Attribute"));
            CHECK(outputFile.exists("/Data/2DFLMIP/CH1/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH1/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH1/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH1/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH1/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/2DFLMIP/CH1/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH1/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH1/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH1/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH1/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/2DFLMIP/CH2"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH2", "groupCH2Attribute"));
            CHECK(outputFile.exists("/Data/2DFLMIP/CH2/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH2/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH2/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH2/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH2/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/2DFLMIP/CH2/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/2DFLMIP/CH2/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH2/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH2/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DFLMIP/CH2/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/2DMIP"));
            CHECK(CheckAttribute(outputFile, "/Data/2DMIP", "group2DMIPAttribute"));
            CHECK(outputFile.exists("/Data/2DMIP/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/2DMIP/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DMIP/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DMIP/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DMIP/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/2DMIP/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/2DMIP/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DMIP/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DMIP/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/2DMIP/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/3D"));
            CHECK(CheckAttribute(outputFile, "/Data/3D", "group3DAttribute"));
            CHECK(outputFile.exists("/Data/3D/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/3D/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3D/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3D/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3D/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/3D/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/3D/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3D/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3D/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3D/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/3DFL"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL", "group3DFLAttribute"));

            CHECK(outputFile.exists("/Data/3DFL/CH0"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH0", "groupCH0Attribute"));
            CHECK(outputFile.exists("/Data/3DFL/CH0/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH0/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH0/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH0/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH0/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/3DFL/CH0/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH0/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH0/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH0/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH0/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/3DFL/CH1"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH1", "groupCH1Attribute"));
            CHECK(outputFile.exists("/Data/3DFL/CH1/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH1/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH1/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH1/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH1/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/3DFL/CH1/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH1/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH1/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH1/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH1/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/3DFL/CH2"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH2", "groupCH2Attribute"));
            CHECK(outputFile.exists("/Data/3DFL/CH2/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH2/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH2/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH2/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH2/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/3DFL/CH2/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/3DFL/CH2/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH2/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH2/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/3DFL/CH2/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Data/BF"));
            CHECK(CheckAttribute(outputFile, "/Data/BF", "groupBFAttribute"));
            CHECK(outputFile.exists("/Data/BF/000000"));
            CHECK(CheckAttribute(outputFile, "/Data/BF/000000", "000000Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/BF/000000", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/BF/000000", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/BF/000000", "TileSizeX"));
            CHECK(outputFile.exists("/Data/BF/000001"));
            CHECK(CheckAttribute(outputFile, "/Data/BF/000001", "000001Attribute"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/BF/000001", "NumberOfTiles"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/BF/000001", "TileSizeX"));
            CHECK(CheckAttributeExistence(outputFile, "/Data/BF/000001", "TileSizeX"));

            CHECK(outputFile.exists("/Info"));
            CHECK(outputFile.exists("/Info/Annotation"));
            CHECK(CheckAttribute(outputFile, "/Info/Annotation", "groupAnnotationAttribute"));
            CHECK(outputFile.exists("/Info/Device"));
            CHECK(CheckAttribute(outputFile, "/Info/Device", "groupDeviceAttribute"));
            CHECK(outputFile.exists("/Info/Imaging"));
            CHECK(CheckAttribute(outputFile, "/Info/Imaging", "groupImagingAttribute"));
            CHECK(outputFile.exists("/Info/Tile"));
            CHECK(CheckAttribute(outputFile, "/Info/Tile", "groupTileAttribute"));

            CHECK(!outputFile.exists("/View"));

            outputFile.close();
        }
    }
}
