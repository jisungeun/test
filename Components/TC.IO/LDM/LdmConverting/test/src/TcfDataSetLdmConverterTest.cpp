#include <catch2/catch.hpp>
#include "TcfDataSetLdmConverter.h"
#include "H5Cpp.h"
using namespace TC::IO::LdmConverting;
using namespace TC::IO;

namespace TcfDataSetLdmConverterTest {
    auto ReadDimensionFromDataSet(H5::DataSet& dataSet)->Dimension {
        auto dataSpace = dataSet.getSpace();
        const auto dimensionality = dataSpace.getSimpleExtentNdims();

        hsize_t dataSpaceDimensions[3] = { 0,0,0 };
        dataSpace.getSimpleExtentDims(dataSpaceDimensions);
        dataSpace.close();

        const auto dataIs2D = (dimensionality == 2);
        const auto dataIs3D = (dimensionality == 3);

        Dimension dimension;
        if (dataIs2D) {
            const auto dimensionX = static_cast<hsize_t>(dataSpaceDimensions[1]);
            const auto dimensionY = static_cast<hsize_t>(dataSpaceDimensions[0]);
            dimension = Dimension(dimensionX, dimensionY);
        } else if (dataIs3D) {
            const auto dimensionX = static_cast<hsize_t>(dataSpaceDimensions[2]);
            const auto dimensionY = static_cast<hsize_t>(dataSpaceDimensions[1]);
            const auto dimensionZ = static_cast<hsize_t>(dataSpaceDimensions[0]);
            dimension = Dimension(dimensionX, dimensionY, dimensionZ);
        }
        return dimension;
    }

    TEST_CASE("TcfDataSetLdmConverter") {
        H5::H5File file("TcfDataSetLdmConverterTest.h5", H5F_ACC_TRUNC);
        auto outputGroup = file.createGroup("outputGroup");

        const auto rank3D = 3;
        const hsize_t dims3D[rank3D] = { 128,128,128 };
        auto dataSpace3D = H5::DataSpace(rank3D, dims3D);
        auto dataSet3D = file.createDataSet("dataSet3D", H5::PredType::NATIVE_UINT16, dataSpace3D);
        dataSpace3D.close();

        const auto rank2D = 2;
        const hsize_t dims2D[rank2D] = { 128,128 };
        auto dataSpace2D = H5::DataSpace(rank2D, dims2D);
        auto dataSet2D = file.createDataSet("dataSet2D", H5::PredType::NATIVE_UINT16, dataSpace2D);
        dataSpace2D.close();

        const auto rank2DStack = 3;
        const hsize_t dims2DStack[rank2DStack] = { 128,128,128 };
        auto dataSpace2DStack = H5::DataSpace(rank2DStack, dims2DStack);
        auto dataSet2DStack = file.createDataSet("dataSet2DStack", H5::PredType::NATIVE_UINT16, dataSpace2DStack);
        dataSpace2DStack.close();

        SECTION("Convert3D()") {
            const Dimension tileDimension(64, 64, 64);
            TcfDataSetLdmConverter tcfDataSetLdmConverter;
            const auto ldmProperty = tcfDataSetLdmConverter.Convert3D(dataSet3D, outputGroup, tileDimension);

            CHECK(ldmProperty.GetNumberOfTiles() == 9);

            const auto tileSize = ldmProperty.GetTileSize();
            CHECK(tileSize.GetDimensionality() == 3);
            CHECK(tileSize.X() == 64);
            CHECK(tileSize.Y() == 64);
            CHECK(tileSize.Z() == 64);

            auto convertedGroup = outputGroup.openGroup("dataSet3D");

            const auto numberOfDataSet = convertedGroup.getNumObjs();
            CHECK(numberOfDataSet == 9);

            for (auto i = 0; i < numberOfDataSet; ++i) {
                auto dataSet = convertedGroup.openDataSet(convertedGroup.getObjnameByIdx(i));
                const auto dimension = ReadDimensionFromDataSet(dataSet);

                CHECK(dimension.GetDimensionality() == 3);
                CHECK(dimension.X() == 64);
                CHECK(dimension.Y() == 64);
                CHECK(dimension.Z() == 64);
                dataSet.close();
            }
            convertedGroup.close();
        }

        SECTION("Convert2D()") {
            const Dimension tileDimension(64, 64);
            TcfDataSetLdmConverter tcfDataSetLdmConverter;
            const auto ldmProperty = tcfDataSetLdmConverter.Convert2D(dataSet2D, outputGroup, tileDimension);

            CHECK(ldmProperty.GetNumberOfTiles() == 5);

            const auto tileSize = ldmProperty.GetTileSize();
            CHECK(tileSize.GetDimensionality() == 2);
            CHECK(tileSize.X() == 64);
            CHECK(tileSize.Y() == 64);

            auto convertedGroup = outputGroup.openGroup("dataSet2D");

            const auto numberOfDataSet = convertedGroup.getNumObjs();
            CHECK(numberOfDataSet == 5);

            for (auto i = 0; i < numberOfDataSet; ++i) {
                auto dataSet = convertedGroup.openDataSet(convertedGroup.getObjnameByIdx(i));
                const auto dimension = ReadDimensionFromDataSet(dataSet);

                CHECK(dimension.GetDimensionality() == 2);
                CHECK(dimension.X() == 64);
                CHECK(dimension.Y() == 64);
                dataSet.close();
            }
            convertedGroup.close();
        }

        SECTION("Convert2DStack()") {
            const Dimension tileDimension(64, 64);
            TcfDataSetLdmConverter tcfDataSetLdmConverter;
            const auto ldmProperty = tcfDataSetLdmConverter.Convert2DStack(dataSet2DStack, outputGroup, tileDimension);

            CHECK(ldmProperty.GetNumberOfTiles() == 5);

            const auto tileSize = ldmProperty.GetTileSize();
            CHECK(tileSize.GetDimensionality() == 3);
            CHECK(tileSize.X() == 64);
            CHECK(tileSize.Y() == 64);
            CHECK(tileSize.Z() == 128);

            auto convertedGroup = outputGroup.openGroup("dataSet2DStack");

            const auto numberOfDataSet = convertedGroup.getNumObjs();
            CHECK(numberOfDataSet == 5);

            for (auto i = 0; i < numberOfDataSet; ++i) {
                auto dataSet = convertedGroup.openDataSet(convertedGroup.getObjnameByIdx(i));
                const auto dimension = ReadDimensionFromDataSet(dataSet);

                CHECK(dimension.GetDimensionality() == 3);
                CHECK(dimension.X() == 64);
                CHECK(dimension.Y() == 64);
                CHECK(dimension.Z() == 128);
                dataSet.close();
            }
            convertedGroup.close();
        }

        outputGroup.close();
        file.close();
    }
}
