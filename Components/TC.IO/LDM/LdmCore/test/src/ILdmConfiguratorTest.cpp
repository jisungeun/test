#include <catch2/catch.hpp>

#include <ILdmConfigurator.h>

using namespace TC::IO::LdmCore;
using namespace TC::IO;

namespace ILdmConfiguratorTest {

    class DummyLdmConfigurator final : public ILdmConfigurator {
    public:
        DummyLdmConfigurator() :ILdmConfigurator() {}
        ~DummyLdmConfigurator() {}

        auto Configure() -> LdmConfiguration override {
            LdmConfiguration ldmConfiguration;
            ldmConfiguration.SetTileUnitDimension(Dimension(1, 2, 3));
            ldmConfiguration.AddLdmTile(1, std::make_shared<LdmTile>());
            return ldmConfiguration;
        }
    };

    TEST_CASE("ILdmConfiguratorTest") {
        SECTION("Configurate()") {
            DummyLdmConfigurator dummyLdmConfigurator;

            const Dimension dataDimension;
            const Dimension tileUnitDimension;

            auto ldmConfiguration = dummyLdmConfigurator.Configure();

            CHECK(ldmConfiguration.GetTileUnitDimension() == Dimension(1, 2, 3));
            CHECK(ldmConfiguration.GetNumberOfTiles() == 1);
        }
    }
}