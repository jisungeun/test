#include <catch2/catch.hpp>

#include "LdmTile.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO;

namespace LdmTileTest {
    TEST_CASE("LdmTileTest") {
        SECTION("LdmTile()") {
            LdmTile ldmTile;
            CHECK(ldmTile.GetTileIndex() == -1);
            CHECK(ldmTile.GetSamplingLevel() == -1);

            const auto dataRange = ldmTile.GetDataRange();
            CHECK(dataRange.IsEmpty() == true);

            const auto tileDataDimension = ldmTile.GetTileDataDimension();
            CHECK(tileDataDimension.GetDimensionality() == 0);

            const auto tileUnitDimension = ldmTile.GetTileUnitDimension();
            CHECK(tileUnitDimension.GetDimensionality() == 0);

            const auto parentTilePointer = ldmTile.GetParentTilePointer();
            CHECK(parentTilePointer == nullptr);

            const auto childrenTilePointerList = ldmTile.GetChildrenPointerList();
            CHECK(childrenTilePointerList.isEmpty() == true);
        }

        SECTION("SetTileIndex()/GetTileIndex()") {
            LdmTile ldmTile;
            ldmTile.SetTileIndex(1);
            CHECK(ldmTile.GetTileIndex() == 1);
        }

        SECTION("SetSamplingLevel()/GetSamplingLevel()") {
            LdmTile ldmTile;
            ldmTile.SetSamplingLevel(1);
            CHECK(ldmTile.GetSamplingLevel() == 1);
        }

        SECTION("SetDataRange()/GetDataRange()") {
            DataRange dataRange(Point(1, 1, 1), Count(1, 1, 1));
            LdmTile ldmTile;
            ldmTile.SetDataRange(dataRange);

            const auto dataRangeFromTile = ldmTile.GetDataRange();
            const auto offset = dataRangeFromTile.GetOffsetPoint();
            const auto count = dataRangeFromTile.GetCount();
            CHECK(offset.GetDimensionality() == 3);
            CHECK(offset.X() == 1);
            CHECK(offset.Y() == 1);
            CHECK(offset.Z() == 1);

            CHECK(count.GetDimensionality() == 3);
            CHECK(count.X() == 1);
            CHECK(count.Y() == 1);
            CHECK(count.Z() == 1);
        }

        SECTION("SetTileDataDimension()/GetTileDataDimension()") {
            Dimension dimension(1, 1, 1);
            LdmTile ldmTile;
            ldmTile.SetTileDataDimension(dimension);

            const auto tileDataDimension = ldmTile.GetTileDataDimension();
            CHECK(tileDataDimension.GetDimensionality() == 3);
            CHECK(tileDataDimension.X() == 1);
            CHECK(tileDataDimension.Y() == 1);
            CHECK(tileDataDimension.Z() == 1);
        }

        SECTION("SetTileUnitDimension()/GetTileUnitDimension()") {
            Dimension dimension(1, 1, 1);
            LdmTile ldmTile;
            ldmTile.SetTileUnitDimension(dimension);

            const auto tileUnitDimension = ldmTile.GetTileUnitDimension();
            CHECK(tileUnitDimension.GetDimensionality() == 3);
            CHECK(tileUnitDimension.X() == 1);
            CHECK(tileUnitDimension.Y() == 1);
            CHECK(tileUnitDimension.Z() == 1);
        }

        SECTION("SetParentTilePointer()/GetParentTilePointer()") {
            LdmTile ldmTile;
            LdmTile::Pointer parentTile(new LdmTile);
            ldmTile.SetParentTilePointer(parentTile);

            CHECK(ldmTile.GetParentTilePointer().get() == parentTile.get());
        }

        SECTION("AddChildTilePointer()/GetChildrenPointerList()") {
            LdmTile ldmTile;
            LdmTile::Pointer childTile1(new LdmTile);
            LdmTile::Pointer childTile2(new LdmTile);

            ldmTile.AddChildTilePointer(childTile1);
            ldmTile.AddChildTilePointer(childTile2);

            const auto childrenTileList = ldmTile.GetChildrenPointerList();
            CHECK(childrenTileList.count() == 2);
            CHECK(childrenTileList.at(0).get() == childTile1.get());
            CHECK(childrenTileList.at(1).get() == childTile2.get());
        }
    }
}
