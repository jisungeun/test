#include <catch2/catch.hpp>

#include "LdmConfiguration.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO;

namespace LdmConfigurationTest {
    TEST_CASE("LdmConfigurationTest") {
        SECTION("LdmConfiguration()") {
            LdmConfiguration ldmConfiguration;
            const auto tileUnitDimension = ldmConfiguration.GetTileUnitDimension();

            CHECK(tileUnitDimension.GetDimensionality() == 0);
            CHECK(tileUnitDimension.X() == 0);
            CHECK(tileUnitDimension.Y() == 0);
            CHECK(tileUnitDimension.Z() == 0);

            const auto numberOfTiles = ldmConfiguration.GetNumberOfTiles();
            CHECK(numberOfTiles == 0);
        }

        SECTION("LdmConfiguration(LdmCofiguration)") {
            LdmConfiguration srcLdmConfiguration;

            const LdmTile::Pointer ldmTilePointer1(new LdmTile);
            srcLdmConfiguration.AddLdmTile(0, ldmTilePointer1);
            srcLdmConfiguration.SetTileUnitDimension(Dimension(1, 2, 3));

            auto destLdmConfiguration(srcLdmConfiguration);
            const auto numberOfTiles = destLdmConfiguration.GetNumberOfTiles();
            const auto tileUnitDimension = destLdmConfiguration.GetTileUnitDimension();

            CHECK(tileUnitDimension.GetDimensionality() == 3);
            CHECK(tileUnitDimension.X() == 1);
            CHECK(tileUnitDimension.Y() == 2);
            CHECK(tileUnitDimension.Z() == 3);

            CHECK(numberOfTiles == 1);
        }


        SECTION("SetTileUnitDimension()/GetTileUnitDimension()") {
            LdmConfiguration ldmConfiguration;

            const Dimension dimension(1, 2, 3);
            ldmConfiguration.SetTileUnitDimension(dimension);

            const auto tileUnitDimension = ldmConfiguration.GetTileUnitDimension();

            CHECK(tileUnitDimension.GetDimensionality() == 3);
            CHECK(tileUnitDimension.X() == 1);
            CHECK(tileUnitDimension.Y() == 2);
            CHECK(tileUnitDimension.Z() == 3);
        }

        SECTION("AddLdmTile()/GetNumberOfTiles()") {
            LdmConfiguration ldmConfiguration;

            const LdmTile::Pointer ldmTilePointer1(new LdmTile);
            const LdmTile::Pointer ldmTilePointer2(new LdmTile);

            ldmConfiguration.AddLdmTile(0, ldmTilePointer1);
            ldmConfiguration.AddLdmTile(1, ldmTilePointer2);

            const auto numberOfTiles = ldmConfiguration.GetNumberOfTiles();

            CHECK(numberOfTiles == 2);
        }

        SECTION("AddLdmTile()/GetLdmTilePointerByTileIndex()") {
            LdmConfiguration ldmConfiguration;

            const LdmTile::Pointer ldmTilePointer1(new LdmTile);
            const LdmTile::Pointer ldmTilePointer2(new LdmTile);

            const auto tileIndex1 = 1;
            const auto tileIndex2 = 3;

            ldmConfiguration.AddLdmTile(tileIndex1, ldmTilePointer1);
            ldmConfiguration.AddLdmTile(tileIndex2, ldmTilePointer2);

            const auto ldmTileFromConfiguration1 = 
                ldmConfiguration.GetLdmTilePointerByTileIndex(tileIndex1);
            const auto ldmTileFromConfiguration2 = 
                ldmConfiguration.GetLdmTilePointerByTileIndex(tileIndex2);

            CHECK(ldmTileFromConfiguration1.get() == ldmTilePointer1.get());
            CHECK(ldmTileFromConfiguration2.get() == ldmTilePointer2.get());
        }
    }
}
