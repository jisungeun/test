#include <catch2/catch.hpp>

#include "Octree3DConfigurator.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO;

namespace Octree3DConfiguratorTest {
    TEST_CASE("Octree3DConfigurator", "Cube") {
        SECTION("Tile Cube BestFit data(256, 256, 256), tile(256, 256, 256)") {
            Dimension dataDimension(256, 256, 256);
            Dimension tileDimension(256, 256, 256);

            Octree3DConfigurator octree3DConfigurator(dataDimension, tileDimension);
            const auto configuration = octree3DConfigurator.Configure();

            CHECK(configuration.GetNumberOfTiles() == 1);
            CHECK(configuration.GetTileUnitDimension() == tileDimension);

            const auto tileIndex = 0;
            const auto ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(256, 256, 256)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 256));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer().get() == nullptr);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());
        }

        SECTION("Tile Cube BestFit data(256, 256, 256), tile(128, 128, 128)") {
            Dimension dataDimension(256, 256, 256);
            Dimension tileDimension(128, 128, 128);

            Octree3DConfigurator octree3DConfigurator(dataDimension, tileDimension);
            const auto configuration = octree3DConfigurator.Configure();

            CHECK(configuration.GetNumberOfTiles() == 9);
            CHECK(configuration.GetTileUnitDimension() == tileDimension);

            auto tileIndex = 0;
            auto ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 1);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(256, 256, 256)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 256));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer().get() == nullptr);
            CHECK(ldmTile->GetChildrenPointerList().size() == 8);

            tileIndex = 2;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(128, 0, 0), Count(128, 128, 128)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(128, 128, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());

            tileIndex = 5;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 128), Count(128, 128, 128)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(128, 128, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());
        }

        SECTION("Tile Cube BestFit data(512, 512, 512), tile(128, 128, 128)") {
            Dimension dataDimension(512, 512, 512);
            Dimension tileDimension(128, 128, 128);

            Octree3DConfigurator octree3DConfigurator(dataDimension, tileDimension);
            const auto configuration = octree3DConfigurator.Configure();

            CHECK(configuration.GetNumberOfTiles() == 73);
            CHECK(configuration.GetTileUnitDimension() == tileDimension);

            auto tileIndex = 0;
            auto ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 2);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(512, 512, 512)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(512, 512, 512));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer().get() == nullptr);
            CHECK(ldmTile->GetChildrenPointerList().size() == 8);

            tileIndex = 3;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 1);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 256, 0), Count(256, 256, 256)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 256));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().size() == 8);

            tileIndex = 5;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 1);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 256), Count(256, 256, 256)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 256));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().size() == 8);

            tileIndex = 9;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(128, 128, 128)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(128, 128, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 1);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());

            tileIndex = 21;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(256, 0, 128), Count(128, 128, 128)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(128, 128, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 2);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());
        }

        SECTION("Tile Cuboid data(256, 256, 256), tile(256, 256, 128)") {
            Dimension dataDimension(256, 256, 256);
            Dimension tileDimension(256, 256, 128);

            Octree3DConfigurator octree3DConfigurator(dataDimension, tileDimension);
            const auto configuration = octree3DConfigurator.Configure();

            CHECK(configuration.GetNumberOfTiles() == 3);
            CHECK(configuration.GetTileUnitDimension() == tileDimension);

            auto tileIndex = 0;
            auto ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 1);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(256, 256, 256)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(512, 512, 256));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer().get() == nullptr);
            CHECK(ldmTile->GetChildrenPointerList().size() == 2);

            tileIndex = 1;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(256, 256, 128)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());

            tileIndex = 2;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 128), Count(256, 256, 128)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());
        }

        SECTION("Tile Cuboid data(256, 256, 256), tile(256, 256, 64)") {
            Dimension dataDimension(256, 256, 256);
            Dimension tileDimension(256, 256, 64);

            Octree3DConfigurator octree3DConfigurator(dataDimension, tileDimension);
            const auto configuration = octree3DConfigurator.Configure();

            CHECK(configuration.GetNumberOfTiles() == 7);
            CHECK(configuration.GetTileUnitDimension() == tileDimension);

            auto tileIndex = 0;
            auto ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 2);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(256, 256, 256)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(1024, 1024, 256));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer().get() == nullptr);
            CHECK(ldmTile->GetChildrenPointerList().size() == 2);

            tileIndex = 1;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 1);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(256, 256, 128)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(512, 512, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().size() == 2);

            tileIndex = 5;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 128), Count(256, 256, 64)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 64));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 2);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());
        }

        SECTION("Tile Cuboid data(500, 500, 210), tile(256, 256, 128)") {
            Dimension dataDimension(500, 500, 210);
            Dimension tileDimension(256, 256, 128);

            Octree3DConfigurator octree3DConfigurator(dataDimension, tileDimension);
            const auto configuration = octree3DConfigurator.Configure();

            CHECK(configuration.GetNumberOfTiles() == 9);
            CHECK(configuration.GetTileUnitDimension() == tileDimension);

            auto tileIndex = 0;
            auto ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 1);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(500, 500, 210)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(512, 512, 256));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer().get() == nullptr);
            CHECK(ldmTile->GetChildrenPointerList().size() == 8);

            tileIndex = 2;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(256, 0, 0), Count(244, 256, 128)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());

            tileIndex = 8;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(256, 256, 128), Count(244, 244, 82)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 128));
            CHECK(ldmTile->GetTileUnitDimension() == tileDimension);
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());
        }
    }
}
