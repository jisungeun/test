#include <catch2/catch.hpp>

#include "LdmCoreUtilities.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO;

namespace UtilitiesTest {

    TEST_CASE("Utilities") {
        SECTION("GetTileName()") {
            SECTION("index 0, total number 10") {
                const auto tileName = TileName(0, 10);
                CHECK(tileName == "TILE_0");
            }

            SECTION("index 0, total number 100") {
                const auto tileName = TileName(0, 100);
                CHECK(tileName == "TILE_00");
            }

            SECTION("index 0, total number 1000") {
                const auto tileName = TileName(0, 1000);
                CHECK(tileName == "TILE_000");
            }

            SECTION("index 9, total number 10") {
                const auto tileName = TileName(9, 10);
                CHECK(tileName == "TILE_9");
            }

            SECTION("index 50, total number 100") {
                const auto tileName = TileName(50, 100);
                CHECK(tileName == "TILE_50");
            }

            SECTION("index 1, total number 1000") {
                const auto tileName = TileName(1, 1000);
                CHECK(tileName == "TILE_001");
            }
        }

        SECTION("GetSampledStep()") {
            SECTION("level 0") {
                CHECK(GetSampledStep(0) == 1);
            }

            SECTION("level 1") {
                CHECK(GetSampledStep(1) == 2);
            }

            SECTION("level 2") {
                CHECK(GetSampledStep(2) == 4);
            }

            SECTION("level 3") {
                CHECK(GetSampledStep(3) == 8);
            }

            SECTION("level 4") {
                CHECK(GetSampledStep(4) == 16);
            }

            SECTION("level 5") {
                CHECK(GetSampledStep(5) == 32);
            }
        }

        SECTION("GetSampledLength()") {
            SECTION("Length 100, level 0") {
                CHECK(GetSampledLength(100, 0) == 100);
            }

            SECTION("Length 100, level 1") {
                CHECK(GetSampledLength(100, 1) == 50);
            }

            SECTION("Length 100, level 2") {
                CHECK(GetSampledLength(100, 2) == 25);
            }

            SECTION("Length 100, level 3") {
                CHECK(GetSampledLength(100, 3) == 13);
            }

            SECTION("Length 100, level 4") {
                CHECK(GetSampledLength(100, 4) == 7);
            }
        }

        SECTION("CalculateSampledDimension()") {
            SECTION("2D") {
                SECTION("DataRange 0~99 0~99") {
                    const DataRange dataRange{ Point{0,0}, Point{99,99} };
                    SECTION("level 0") {
                        const auto resultDimension = CalculateSampledDimension(dataRange, 0);
                        CHECK(resultDimension == Dimension(100, 100));
                    }

                    SECTION("level 1") {
                        const auto resultDimension = CalculateSampledDimension(dataRange, 1);
                        CHECK(resultDimension == Dimension(50, 50));
                    }

                    SECTION("level 2") {
                        const auto resultDimension = CalculateSampledDimension(dataRange, 2);
                        CHECK(resultDimension == Dimension(25, 25));
                    }
                }
            }

            SECTION("3D") {
                SECTION("DataRange 0~99 0~99 0~99") {
                    const DataRange dataRange{ Point{0,0,0}, Point{99,99,99} };
                    SECTION("level 0") {
                        const auto resultDimension = CalculateSampledDimension(dataRange, 0);
                        CHECK(resultDimension == Dimension(100, 100, 100));
                    }

                    SECTION("level 1") {
                        const auto resultDimension = CalculateSampledDimension(dataRange, 1);
                        CHECK(resultDimension == Dimension(50, 50, 50));
                    }

                    SECTION("level 2") {
                        const auto resultDimension = CalculateSampledDimension(dataRange, 2);
                        CHECK(resultDimension == Dimension(25, 25, 25));
                    }
                }
            }
        }

        SECTION("CalculateSampledDimension2DStack()") {
            SECTION("DataRange 0~99 0~99 0~9") {
                const DataRange dataRange{ Point{0,0,0}, Point{99,99,9} };
                SECTION("level 0") {
                    const auto resultDimension = CalculateSampledDimension2DStack(dataRange, 0);
                    CHECK(resultDimension == Dimension(100, 100, 10));
                }

                SECTION("level 1") {
                    const auto resultDimension = CalculateSampledDimension2DStack(dataRange, 1);
                    CHECK(resultDimension == Dimension(50, 50, 10));
                }

                SECTION("level 2") {
                    const auto resultDimension = CalculateSampledDimension2DStack(dataRange, 2);
                    CHECK(resultDimension == Dimension(25, 25, 10));
                }
            }
        }

        SECTION("CalculateMaxSamplingLevel()") {
            SECTION("Level 0") {
                constexpr uint32_t dataLength = 128;
                constexpr uint32_t tileLength = 128;

                CHECK(CalculateMaxSamplingLevel(dataLength, tileLength) == 0);
            }

            SECTION("Level 1") {
                constexpr uint32_t dataLength = 128;
                constexpr uint32_t tileLength = 64;

                CHECK(CalculateMaxSamplingLevel(dataLength, tileLength) == 1);
            }

            SECTION("Level 2") {
                constexpr uint32_t dataLength = 128;
                constexpr uint32_t tileLength = 32;

                CHECK(CalculateMaxSamplingLevel(dataLength, tileLength) == 2);
            }
        }
    }
}
