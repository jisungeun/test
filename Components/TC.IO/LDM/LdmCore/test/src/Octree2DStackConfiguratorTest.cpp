#include <catch2/catch.hpp>

#include "Octree2DStackConfigurator.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO;

namespace Octree2DStackConfiguratorTest {
    TEST_CASE("Octree2DStackConfigurator") {
        SECTION("Tile Cube BestFit data(256, 256, 250), tile(256, 256)") {
            Dimension dataDimension(256, 256, 250);
            Dimension tileDimension(256, 256);

            Octree2DStackConfigurator octree2DConfigurator(dataDimension, tileDimension);
            const auto configuration = octree2DConfigurator.Configure();

            CHECK(configuration.GetNumberOfTiles() == 1);
            CHECK(configuration.GetTileUnitDimension() == Dimension(256, 256, 250));

            const auto tileIndex = 0;
            const auto ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(256, 256, 250)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 250));
            CHECK(ldmTile->GetTileUnitDimension() == Dimension(256, 256, 250));
            CHECK(ldmTile->GetParentTilePointer().get() == nullptr);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());
        }

        SECTION("Tile Cube BestFit data(256, 256, 130), tile(128, 128)") {
            Dimension dataDimension(256, 256, 130);
            Dimension tileDimension(128, 128);

            Octree2DStackConfigurator octree2DConfigurator(dataDimension, tileDimension);
            const auto configuration = octree2DConfigurator.Configure();

            CHECK(configuration.GetNumberOfTiles() == 5);
            CHECK(configuration.GetTileUnitDimension() == Dimension(128, 128, 130));

            auto tileIndex = 0;
            auto ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 1);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(0, 0, 0), Count(256, 256, 130)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(256, 256, 130));
            CHECK(ldmTile->GetTileUnitDimension() == Dimension(128,128,130));
            CHECK(ldmTile->GetParentTilePointer().get() == nullptr);
            CHECK(ldmTile->GetChildrenPointerList().size() == 4);

            tileIndex = 2;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(128, 0, 0), Count(128, 128, 130)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(128, 128, 130));
            CHECK(ldmTile->GetTileUnitDimension() == Dimension(128, 128, 130));
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());

            tileIndex = 4;
            ldmTile = configuration.GetLdmTilePointerByTileIndex(tileIndex);
            CHECK(ldmTile->GetTileIndex() == tileIndex);
            CHECK(ldmTile->GetSamplingLevel() == 0);
            CHECK(ldmTile->GetDataRange() == DataRange(Point(128, 128, 0), Count(128, 128, 130)));
            CHECK(ldmTile->GetTileDataDimension() == Dimension(128, 128, 130));
            CHECK(ldmTile->GetTileUnitDimension() == Dimension(128, 128, 130));
            CHECK(ldmTile->GetParentTilePointer()->GetTileIndex() == 0);
            CHECK(ldmTile->GetChildrenPointerList().isEmpty());
        }

    }
}
