#include <catch2/catch.hpp>

#include "LdmMetaInfo.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO;

namespace LdmMetaInfoTest {
    TEST_CASE("LdmMetaInfo") {
        SECTION("LdmMetaInfo()") {
            LdmMetaInfo ldmMetaInfo;
            CHECK(ldmMetaInfo.GetNumberOfTiles() == 0);
            CHECK(ldmMetaInfo.GetNumberOfSamplingLevels() == 0);

            const auto dataDimension = ldmMetaInfo.GetDataDimension();
            const auto dataDimensionIsInitial = (dataDimension == Dimension{});
            CHECK(dataDimensionIsInitial);

            const auto tileUnitDimension = ldmMetaInfo.GetTileUnitDimension();
            const auto tileUnitDimensionIsInitial = (tileUnitDimension == Dimension{});
            CHECK(tileUnitDimensionIsInitial);
        }

        SECTION("LdmMetaInfo(LdmMetaInfo)") {
            LdmMetaInfo srcLdmMetaInfo;
            srcLdmMetaInfo.SetNumberOfTiles(1);
            const auto destLdmMetaInfo = srcLdmMetaInfo;
            CHECK(destLdmMetaInfo.GetNumberOfTiles() == 1);
        }

        SECTION("operator=") {
            LdmMetaInfo srcLdmMetaInfo;
            srcLdmMetaInfo.SetNumberOfTiles(1);

            LdmMetaInfo destLdmMetaInfo;
            destLdmMetaInfo = srcLdmMetaInfo;
            CHECK(destLdmMetaInfo.GetNumberOfTiles() == 1);
        }

        SECTION("SetDataDimension()/GetDataDimension()") {
            LdmMetaInfo ldmMetaInfo;
            ldmMetaInfo.SetDataDimension(Dimension(1, 2, 3));
            const auto dataDimension = ldmMetaInfo.GetDataDimension();

            const auto dataDimensionIsSame = (dataDimension == Dimension(1, 2, 3));
            CHECK(dataDimensionIsSame);
        }

        SECTION("SetTileUnitDimension()/GetTileUnitDimension()") {
            LdmMetaInfo ldmMetaInfo;
            ldmMetaInfo.SetTileUnitDimension(Dimension(1, 2, 3));
            const auto tileUnitDimension = ldmMetaInfo.GetTileUnitDimension();

            const auto tileUnitDimensionIsSame = (tileUnitDimension == Dimension(1, 2, 3));
            CHECK(tileUnitDimensionIsSame);
        }

        SECTION("SetNumberOfTiles()/GetNumberOfTiles()") {
            LdmMetaInfo ldmMetaInfo;
            ldmMetaInfo.SetNumberOfTiles(1);
            CHECK(ldmMetaInfo.GetNumberOfTiles() == 1);
        }

        SECTION("SetNumberOfSamplingLevels()/GetNumberOfSamplingLevels()") {
            LdmMetaInfo ldmMetaInfo;
            ldmMetaInfo.SetNumberOfSamplingLevels(1);
            CHECK(ldmMetaInfo.GetNumberOfSamplingLevels() == 1);
        }
    }

}

