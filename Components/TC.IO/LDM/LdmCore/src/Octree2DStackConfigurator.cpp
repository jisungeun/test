#include "Octree2DStackConfigurator.h"

#include <complex>

namespace TC::IO::LdmCore {
    struct Octree2DStackConfigurator::Impl {
        Impl() = default;
        ~Impl() = default;

        Dimension dataDimension{};
        Dimension tileDimension{};
        int32_t maximumSamplingLevel{ -1 };
        Dimension paddingDimension{};
        LdmConfiguration ldmConfiguration{};
    };

    Octree2DStackConfigurator::Octree2DStackConfigurator(const Dimension& dataDimension, 
        const Dimension& tileDimension)
        : d(new Impl()) {
        d->dataDimension = dataDimension;
        d->tileDimension = Dimension(tileDimension.X(),tileDimension.Y(),dataDimension.Z());
        d->ldmConfiguration = GenerateSkeletonLdmConfiguration(d->tileDimension);
    }

    Octree2DStackConfigurator::~Octree2DStackConfigurator() = default;

    auto Octree2DStackConfigurator::GenerateSkeletonLdmConfiguration(const Dimension& dimension) const
        ->LdmConfiguration {
        LdmConfiguration skeletonLdmConfiguration;
        skeletonLdmConfiguration.SetTileUnitDimension(d->tileDimension);
        return skeletonLdmConfiguration;
    }

    auto Octree2DStackConfigurator::Configure() -> LdmConfiguration {
        d->maximumSamplingLevel = CalculateSamplingLevel();
        d->paddingDimension = CalculatePaddingDimension(d->maximumSamplingLevel);

        auto tileIndex = 0;
        d->ldmConfiguration.AddLdmTile(tileIndex, GenerateInitialLdmTile());
        tileIndex++;

        for (auto parentSamplingLevel = d->maximumSamplingLevel; parentSamplingLevel > 0; --parentSamplingLevel) {
            auto parentTiles = GetLdmTileBySamplingLevel(parentSamplingLevel);

            for (auto& parentTile : parentTiles) {
                for (auto i = 0; i < OctreeOrder2dXy::_size(); ++i) {
                    const auto octreeOrder = OctreeOrder2dXy::_from_index(i);
                    const auto childDataRange = CalChildDataRange(parentTile, octreeOrder);

                    if (!childDataRange.IsEmpty()) {
                        const auto childTile = GenerateChildLdmTile(tileIndex, parentTile, childDataRange);
                        parentTile->AddChildTilePointer(childTile);
                        d->ldmConfiguration.AddLdmTile(tileIndex, childTile);
                        tileIndex++;
                    }
                }
            }
        }

        return d->ldmConfiguration;
    }

    auto Octree2DStackConfigurator::CalculateSamplingLevel() const -> uint32_t {
        const auto dataLengthX = static_cast<uint32_t>(d->dataDimension.X());
        const auto dataLengthY = static_cast<uint32_t>(d->dataDimension.Y());

        const auto tileLengthX = static_cast<uint32_t>(d->tileDimension.X());
        const auto tileLengthY = static_cast<uint32_t>(d->tileDimension.Y());

        const auto samplingLevelX = CalculateSamplingLevel(dataLengthX, tileLengthX);
        const auto samplingLevelY = CalculateSamplingLevel(dataLengthY, tileLengthY);
        const auto maximumSamplingLevel = std::max(samplingLevelX, samplingLevelY);
        return maximumSamplingLevel;
    }

    auto Octree2DStackConfigurator::CalculateSamplingLevel(const uint32_t& dataLength,
        const uint32_t& tileLength) const -> uint32_t {
        const auto dataSizeExponent = std::log2(static_cast<double>(dataLength));
        const auto tileSizeExponent = std::log2(static_cast<double>(tileLength));
        const auto samplingLevel = static_cast<int32_t>(std::ceil(dataSizeExponent - tileSizeExponent));
        return std::max(samplingLevel, 0);
    }

    auto Octree2DStackConfigurator::CalculatePaddingDimension(const uint32_t& maximumSamplingLevel) const
        -> Dimension {
        const auto paddingLengthX = d->tileDimension.X() * static_cast<size_t>(std::pow(2, maximumSamplingLevel));
        const auto paddingLengthY = d->tileDimension.Y() * static_cast<size_t>(std::pow(2, maximumSamplingLevel));
        const auto paddingLengthZ = d->dataDimension.Z();
        return Dimension(paddingLengthX, paddingLengthY, paddingLengthZ);
    }

    auto Octree2DStackConfigurator::GenerateInitialLdmTile() const ->LdmTile::Pointer {
        LdmTile::Pointer initialTile(new LdmTile);
        initialTile->SetTileIndex(0);
        initialTile->SetSamplingLevel(d->maximumSamplingLevel);
        initialTile->SetDataRange(DataRange(Point(0, 0, 0), Count(d->dataDimension)));
        initialTile->SetTileDataDimension(d->paddingDimension);
        initialTile->SetTileUnitDimension(d->tileDimension);
        return initialTile;
    }

    auto Octree2DStackConfigurator::GetLdmTileBySamplingLevel(const int32_t& samplingLevel) const
        -> QList<LdmTile::Pointer> {
        const auto numberOfTiles = d->ldmConfiguration.GetNumberOfTiles();
        QList<LdmTile::Pointer> matchedLdmTile;

        for (uint32_t i = 0; i < numberOfTiles; ++i) {
            const auto ldmTile = d->ldmConfiguration.GetLdmTilePointerByTileIndex(i);
            const auto samplingLevelMatched = (ldmTile->GetSamplingLevel() == samplingLevel);

            if (samplingLevelMatched) {
                matchedLdmTile.push_back(ldmTile);
            }
        }
        return matchedLdmTile;
    }

    auto Octree2DStackConfigurator::CalChildDataRange(const LdmTile::Pointer& parentTile,
        const OctreeOrder2dXy& order) -> DataRange {
        const auto childOffsetPoint = CalChildOffsetPoint(parentTile, order);
        const auto dataOverlap = IsDataOverlapped(parentTile, childOffsetPoint);
        DataRange childDataRange;
        if (dataOverlap) {
            const auto childTileDataDimension = CalculateChildTileDataDimension(parentTile->GetTileDataDimension());

            const auto parentDataLastPoint = parentTile->GetDataRange().GetLastPoint();
            const auto childTileDataLastPoint = childOffsetPoint + Count(childTileDataDimension);

            const auto childDataLastPoint = Point::Min(parentDataLastPoint, childTileDataLastPoint);

            childDataRange = DataRange(childOffsetPoint, childDataLastPoint);
        }

        return childDataRange;
    }

    auto Octree2DStackConfigurator::CalChildOffsetPoint(const LdmTile::Pointer& parentTile,
        const OctreeOrder2dXy& order) -> Point {
        const auto parentOffsetPoint = parentTile->GetDataRange().GetOffsetPoint();
        const auto childTileDataDimension = CalculateChildTileDataDimension(parentTile->GetTileDataDimension());
        Point childOffsetPoint{};

        switch (order) {
        case OctreeOrder2dXy::NN:
            childOffsetPoint = parentOffsetPoint;
            break;
        case OctreeOrder2dXy::NP:
            childOffsetPoint = Point(parentOffsetPoint.X(),
                                             parentOffsetPoint.Y() + childTileDataDimension.Y(),
                                             0);
            break;
        case OctreeOrder2dXy::PN:
            childOffsetPoint = Point(parentOffsetPoint.X() + childTileDataDimension.X(),
                                             parentOffsetPoint.Y(),
                                             0);
            break;
        case OctreeOrder2dXy::PP:
            childOffsetPoint = Point(parentOffsetPoint.X() + childTileDataDimension.X(),
                                             parentOffsetPoint.Y() + childTileDataDimension.Y(),
                                             0);
            break;
        default:
            break;
        }
        return childOffsetPoint;
    }

    auto Octree2DStackConfigurator::CalculateChildTileDataDimension(const Dimension& parentTileDataDimension)
        -> Dimension {
        const auto childTileDataDimensionX = parentTileDataDimension.X() / 2;
        const auto childTileDataDimensionY = parentTileDataDimension.Y() / 2;
        const auto childTileDataDimensionZ = parentTileDataDimension.Z();

        const auto childTileDataDimension = 
            Dimension(childTileDataDimensionX, childTileDataDimensionY, childTileDataDimensionZ);
        return childTileDataDimension;
    }

    auto Octree2DStackConfigurator::IsDataOverlapped(const LdmTile::Pointer& parentTile,
        const Point& childOffsetPoint) -> bool {
        const auto parentDataLastPoint = parentTile->GetDataRange().GetLastPoint();
        const auto inBoundaryX = childOffsetPoint.X() <= parentDataLastPoint.X();
        const auto inBoundaryY = childOffsetPoint.Y() <= parentDataLastPoint.Y();

        return inBoundaryX && inBoundaryY;
    }

    auto Octree2DStackConfigurator::GenerateChildLdmTile(const int32_t& tileIndex, const LdmTile::Pointer& parentTile,
        const DataRange& childDataRange) const -> LdmTile::Pointer {
        const auto parentTileDataDimension = parentTile->GetTileDataDimension();
        const auto childTileDataDimension = CalculateChildTileDataDimension(parentTileDataDimension);

        LdmTile::Pointer childTile(new LdmTile);
        childTile->SetTileIndex(tileIndex);
        childTile->SetTileUnitDimension(d->tileDimension);
        childTile->SetTileDataDimension(childTileDataDimension);
        childTile->SetDataRange(childDataRange);
        childTile->SetSamplingLevel(parentTile->GetSamplingLevel() - 1);
        childTile->SetParentTilePointer(parentTile);

        return childTile;
    }
}
