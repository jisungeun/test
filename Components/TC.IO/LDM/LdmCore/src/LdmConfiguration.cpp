#include "LdmConfiguration.h"

#include <QMap>

namespace TC::IO::LdmCore {
    struct LdmConfiguration::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        Dimension tileUnitDimension{};
        QMap<int32_t, LdmTile::Pointer> tileMap{};
    };

    LdmConfiguration::LdmConfiguration()
        : d(new Impl()) {
    }

    LdmConfiguration::LdmConfiguration(const LdmConfiguration& other)
        : d(new Impl(*other.d)) {
    }

    LdmConfiguration::~LdmConfiguration() = default;

    auto LdmConfiguration::operator=(const LdmConfiguration& other) -> LdmConfiguration& {
        *d = *(other.d);
        return *this;
    }

    auto LdmConfiguration::SetTileUnitDimension(const Dimension& tileUnitDimension) -> void {
        d->tileUnitDimension = tileUnitDimension;
    }

    auto LdmConfiguration::AddLdmTile(const int32_t& tileIndex, const LdmTile::Pointer& ldmTile) -> void {
        d->tileMap[tileIndex] = ldmTile;
    }

    auto LdmConfiguration::GetTileUnitDimension() const -> Dimension {
        return d->tileUnitDimension;
    }

    auto LdmConfiguration::GetNumberOfTiles() const -> uint32_t {
        return static_cast<uint32_t>(d->tileMap.count());
    }

    auto LdmConfiguration::GetLdmTilePointerByTileIndex(const int32_t& tileIndex) const -> LdmTile::Pointer {
        if (d->tileMap.find(tileIndex) == d->tileMap.end()) {
            return LdmTile::Pointer{ nullptr };
        } else {
            return d->tileMap[tileIndex];
        }
    }
}
