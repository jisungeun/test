#include "LdmCoreUtilities.h"

#include <QString>
#include <complex>

namespace TC::IO::LdmCore {
    auto TileName(const int32_t& tileIndex, const int32_t& numberOfTiles) -> std::string {
        const auto digitNumber = static_cast<uint32_t>(std::ceil(std::log10(numberOfTiles)));
        const auto tileName = QString("TILE_%1").arg(tileIndex, digitNumber, 10, QChar('0'));

        return std::string(tileName.toLocal8Bit().constData());
    }

    auto GetSampledStep(const int32_t& samplingLevel) -> size_t {
        return std::pow<int32_t>(static_cast<int32_t>(2), static_cast<int32_t>(samplingLevel));
    }

    auto GetSampledLength(const size_t& originalLength, const int32_t& samplingLevel) -> size_t {
        const auto samplingStep = static_cast<float>(GetSampledStep(samplingLevel));
        const auto sampledLength = std::ceil(static_cast<float>(originalLength) / samplingStep);
        return static_cast<size_t>(sampledLength);
    }

    auto CalculateSampledDimension(const Dimension& dimension, const int32_t& samplingLevel) {
        const auto lengthX = dimension.X();
        const auto lengthY = dimension.Y();

        const auto sampledLengthX = GetSampledLength(lengthX, samplingLevel);
        const auto sampledLengthY = GetSampledLength(lengthY, samplingLevel);
        if (dimension.GetDimensionality() == 2) {
            return Dimension(sampledLengthX, sampledLengthY);
        }

        const auto lengthZ = dimension.Z();
        const auto sampledLengthZ = GetSampledLength(lengthZ, samplingLevel);

        return Dimension(sampledLengthX, sampledLengthY, sampledLengthZ);
    }

    auto CalculateSampledDimension(const DataRange& dataRange, const int32_t& samplingLevel)
        -> Dimension {

        const auto lengthX = dataRange.GetCount().X();
        const auto lengthY = dataRange.GetCount().Y();
        const auto lengthZ = dataRange.GetCount().Z();

        if (dataRange.GetDimensionality() == 2) {
            const Dimension dimension{ lengthX,lengthY };
            return CalculateSampledDimension(dimension, samplingLevel);
        }

        const Dimension dimension{ lengthX,lengthY,lengthZ };

        return CalculateSampledDimension(dimension, samplingLevel);
    }

    auto CalculateSampledDimension2DStack(const DataRange& dataRange, const int32_t& samplingLevel)
        -> Dimension {
        const auto lengthX = dataRange.GetCount().X();
        const auto lengthY = dataRange.GetCount().Y();
        const auto lengthZ = dataRange.GetCount().Z();

        const auto sampledLengthX = GetSampledLength(lengthX, samplingLevel);
        const auto sampledLengthY = GetSampledLength(lengthY, samplingLevel);
        const auto sampledLengthZ = lengthZ;
        return Dimension(sampledLengthX, sampledLengthY, sampledLengthZ);
    }

    auto CalculateMaxSamplingLevel(const uint32_t& dataLength, const uint32_t& tileLength)->uint32_t {
        const auto dataSizeExponent = std::log2(static_cast<double>(dataLength));
        const auto tileSizeExponent = std::log2(static_cast<double>(tileLength));
        const auto samplingLevel = static_cast<int32_t>(std::ceil(dataSizeExponent - tileSizeExponent));
        return std::max(samplingLevel, 0);
    }
}
