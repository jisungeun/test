#include "Octree3DConfigurator.h"

#include <complex>

namespace TC::IO::LdmCore {
    struct Octree3DConfigurator::Impl {
        Impl() = default;
        ~Impl() = default;
        Dimension dataDimension{};
        Dimension tileDimension{};
        int32_t maximumSamplingLevel{ -1 };
        Dimension paddingDimension{};
        LdmConfiguration ldmConfiguration{};
    };

    Octree3DConfigurator::Octree3DConfigurator(const Dimension& dataDimension, const Dimension& tileDimension)
        : d(new Impl()) {
        d->dataDimension = dataDimension;
        d->tileDimension = tileDimension;
        d->ldmConfiguration = GenerateSkeletonLdmConfiguration(d->tileDimension);
    }

    Octree3DConfigurator::~Octree3DConfigurator() = default;

    auto Octree3DConfigurator::GenerateSkeletonLdmConfiguration(const Dimension& dimension) const ->LdmConfiguration {
        LdmConfiguration skeletonLdmConfiguration;
        skeletonLdmConfiguration.SetTileUnitDimension(d->tileDimension);
        return skeletonLdmConfiguration;
    }

    auto Octree3DConfigurator::Configure() -> LdmConfiguration {
        d->maximumSamplingLevel = CalculateSamplingLevel();
        d->paddingDimension = CalculatePaddingDimension(d->maximumSamplingLevel);

        auto tileIndex = 0;
        d->ldmConfiguration.AddLdmTile(tileIndex, GenerateInitialLdmTile());
        tileIndex++;

        for (auto parentSamplingLevel = d->maximumSamplingLevel; parentSamplingLevel > 0; --parentSamplingLevel) {
            auto parentTiles = GetLdmTileBySamplingLevel(parentSamplingLevel);

            for (auto& parentTile : parentTiles) {
                for (auto i = 0; i < OctreeOrder3dXyz::_size(); ++i) {
                    const auto octreeOrder = OctreeOrder3dXyz::_from_index(i);
                    const auto childDataRange = CalChildDataRange(parentTile, octreeOrder);

                    if (!childDataRange.IsEmpty()) {
                        const auto childTile = GenerateChildLdmTile(tileIndex, parentTile, childDataRange);
                        parentTile->AddChildTilePointer(childTile);
                        d->ldmConfiguration.AddLdmTile(tileIndex, childTile);
                        tileIndex++;
                    }
                }
            }
        }

        return d->ldmConfiguration;
    }

    auto Octree3DConfigurator::CalculateSamplingLevel() const -> uint32_t {
        const auto dataLengthX = static_cast<uint32_t>(d->dataDimension.X());
        const auto dataLengthY = static_cast<uint32_t>(d->dataDimension.Y());
        const auto dataLengthZ = static_cast<uint32_t>(d->dataDimension.Z());

        const auto tileLengthX = static_cast<uint32_t>(d->tileDimension.X());
        const auto tileLengthY = static_cast<uint32_t>(d->tileDimension.Y());
        const auto tileLengthZ = static_cast<uint32_t>(d->tileDimension.Z());

        const auto samplingLevelX = CalculateSamplingLevel(dataLengthX, tileLengthX);
        const auto samplingLevelY = CalculateSamplingLevel(dataLengthY, tileLengthY);
        const auto samplingLevelZ = CalculateSamplingLevel(dataLengthZ, tileLengthZ);
        const auto maximumSamplingLevel = std::max(std::max(samplingLevelX, samplingLevelY), samplingLevelZ);
        return maximumSamplingLevel;
    }

    auto Octree3DConfigurator::CalculateSamplingLevel(const uint32_t& dataLength,
        const uint32_t& tileLength) const -> uint32_t {
        const auto dataSizeExponent = std::log2(static_cast<double>(dataLength));
        const auto tileSizeExponent = std::log2(static_cast<double>(tileLength));
        const auto samplingLevel = static_cast<int32_t>(std::ceil(dataSizeExponent - tileSizeExponent));
        return std::max(samplingLevel, 0);
    }

    auto Octree3DConfigurator::CalculatePaddingDimension(const uint32_t& maximumSamplingLevel) const -> Dimension {
        const auto paddingLengthX = d->tileDimension.X() * static_cast<size_t>(std::pow(2, maximumSamplingLevel));
        const auto paddingLengthY = d->tileDimension.Y() * static_cast<size_t>(std::pow(2, maximumSamplingLevel));
        const auto paddingLengthZ = d->tileDimension.Z() * static_cast<size_t>(std::pow(2, maximumSamplingLevel));
        return Dimension(paddingLengthX, paddingLengthY, paddingLengthZ);
    }

    auto Octree3DConfigurator::GenerateInitialLdmTile() const ->LdmTile::Pointer{
        LdmTile::Pointer initialTile(new LdmTile);
        initialTile->SetTileIndex(0);
        initialTile->SetSamplingLevel(d->maximumSamplingLevel);
        initialTile->SetDataRange(DataRange(Point(0, 0, 0), Count(d->dataDimension)));
        initialTile->SetTileDataDimension(d->paddingDimension);
        initialTile->SetTileUnitDimension(d->tileDimension);
        return initialTile;
    }

    auto Octree3DConfigurator::GetLdmTileBySamplingLevel(const int32_t& samplingLevel) const
        -> QList<LdmTile::Pointer> {
        const auto numberOfTiles = d->ldmConfiguration.GetNumberOfTiles();
        QList<LdmTile::Pointer> matchedLdmTile;

        for (uint32_t i = 0; i < numberOfTiles; ++i) {
            const auto ldmTile = d->ldmConfiguration.GetLdmTilePointerByTileIndex(i);
            const auto samplingLevelMatched = (ldmTile->GetSamplingLevel() == samplingLevel);

            if (samplingLevelMatched) {
                matchedLdmTile.push_back(ldmTile);
            }
        }
        return matchedLdmTile;
    }

    auto Octree3DConfigurator::CalChildDataRange(const LdmTile::Pointer& parentTile,
        const OctreeOrder3dXyz& order) -> DataRange {
        const auto childOffsetPoint = CalChildOffsetPoint(parentTile, order);
        const auto dataOverlap = IsDataOverlapped(parentTile, childOffsetPoint);
        DataRange childDataRange;
        if (dataOverlap) {
            const auto childTileDataDimension = CalculateChildTileDataDimension(parentTile->GetTileDataDimension());

            const auto parentDataLastPoint = parentTile->GetDataRange().GetLastPoint();
            const auto childTileDataLastPoint = childOffsetPoint + Count(childTileDataDimension);

            const auto childDataLastPoint = Point::Min(parentDataLastPoint, childTileDataLastPoint);

            childDataRange = DataRange(childOffsetPoint, childDataLastPoint);
        }

        return childDataRange;
    }

    auto Octree3DConfigurator::CalChildOffsetPoint(const LdmTile::Pointer& parentTile,
        const OctreeOrder3dXyz& order) -> Point {
        const auto parentOffsetPoint = parentTile->GetDataRange().GetOffsetPoint();
        const auto childTileDataDimension = CalculateChildTileDataDimension(parentTile->GetTileDataDimension());
        Point childOffsetPoint{};

        switch (order) {
        case OctreeOrder3dXyz::NNN:
            childOffsetPoint = parentOffsetPoint;
            break;
        case OctreeOrder3dXyz::NNP:
            childOffsetPoint = Point(parentOffsetPoint.X(),
                                             parentOffsetPoint.Y(),
                                             parentOffsetPoint.Z() + childTileDataDimension.Z());
            break;
        case OctreeOrder3dXyz::NPN:
            childOffsetPoint = Point(parentOffsetPoint.X(),
                                             parentOffsetPoint.Y() + childTileDataDimension.Y(),
                                             parentOffsetPoint.Z());
            break;
        case OctreeOrder3dXyz::NPP:
            childOffsetPoint = Point(parentOffsetPoint.X(),
                                             parentOffsetPoint.Y() + childTileDataDimension.Y(),
                                             parentOffsetPoint.Z() + childTileDataDimension.Z());
            break;
        case OctreeOrder3dXyz::PNN:
            childOffsetPoint = Point(parentOffsetPoint.X() + childTileDataDimension.X(),
                                             parentOffsetPoint.Y(),
                                             parentOffsetPoint.Z());
            break;
        case OctreeOrder3dXyz::PNP:
            childOffsetPoint = Point(parentOffsetPoint.X() + childTileDataDimension.X(),
                                             parentOffsetPoint.Y(),
                                             parentOffsetPoint.Z() + childTileDataDimension.Z());
            break;
        case OctreeOrder3dXyz::PPN:
            childOffsetPoint = Point(parentOffsetPoint.X() + childTileDataDimension.X(),
                                             parentOffsetPoint.Y() + childTileDataDimension.Y(),
                                             parentOffsetPoint.Z());
            break;
        case OctreeOrder3dXyz::PPP:
            childOffsetPoint = Point(parentOffsetPoint.X() + childTileDataDimension.X(),
                                             parentOffsetPoint.Y() + childTileDataDimension.Y(),
                                             parentOffsetPoint.Z() + childTileDataDimension.Z());
            break;
        default:
            break;
        }
        return childOffsetPoint;
    }

    auto Octree3DConfigurator::CalculateChildTileDataDimension(const Dimension& parentTileDataDimension)
        -> Dimension {
        const auto childTileDataDimensionX = parentTileDataDimension.X() / 2;
        const auto childTileDataDimensionY = parentTileDataDimension.Y() / 2;
        const auto childTileDataDimensionZ = parentTileDataDimension.Z() / 2;

        const auto childTileDataDimension = 
            Dimension(childTileDataDimensionX, childTileDataDimensionY, childTileDataDimensionZ);
        return childTileDataDimension;
    }

    auto Octree3DConfigurator::IsDataOverlapped(const LdmTile::Pointer& parentTile,
        const Point& childOffsetPoint) -> bool {
        const auto parentDataLastPoint = parentTile->GetDataRange().GetLastPoint();
        const auto inBoundaryX = childOffsetPoint.X() <= parentDataLastPoint.X();
        const auto inBoundaryY = childOffsetPoint.Y() <= parentDataLastPoint.Y();
        const auto inBoundaryZ = childOffsetPoint.Z() <= parentDataLastPoint.Z();

        return inBoundaryX && inBoundaryY && inBoundaryZ;
    }

    auto Octree3DConfigurator::GenerateChildLdmTile(const int32_t& tileIndex, const LdmTile::Pointer& parentTile,
        const DataRange& childDataRange) const -> LdmTile::Pointer {
        const auto parentTileDataDimension = parentTile->GetTileDataDimension();
        const auto childTileDataDimension = CalculateChildTileDataDimension(parentTileDataDimension);

        LdmTile::Pointer childTile(new LdmTile);
        childTile->SetTileIndex(tileIndex);
        childTile->SetTileUnitDimension(d->tileDimension);
        childTile->SetTileDataDimension(childTileDataDimension);
        childTile->SetDataRange(childDataRange);
        childTile->SetSamplingLevel(parentTile->GetSamplingLevel() - 1);
        childTile->SetParentTilePointer(parentTile);

        return childTile;
    }
}
