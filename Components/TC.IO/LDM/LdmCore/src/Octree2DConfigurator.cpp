#include "Octree2DConfigurator.h"

#include "LdmCoreUtilities.h"
#include <complex>

namespace TC::IO::LdmCore {
    struct Octree2DConfigurator::Impl {
        Impl() = default;
        ~Impl() = default;
        Dimension dataDimension{};
        Dimension tileDimension{};
        int32_t maximumSamplingLevel{ -1 };
        Dimension paddingDimension{};
        LdmConfiguration ldmConfiguration{};
    };

    Octree2DConfigurator::Octree2DConfigurator(const Dimension& dataDimension, const Dimension& tileDimension)
        : d(new Impl()) {
        d->dataDimension = dataDimension;
        d->tileDimension = tileDimension;
        d->ldmConfiguration = GenerateSkeletonLdmConfiguration(d->tileDimension);
    }

    Octree2DConfigurator::~Octree2DConfigurator() = default;

    auto Octree2DConfigurator::GenerateSkeletonLdmConfiguration(const Dimension& dimension) const ->LdmConfiguration {
        LdmConfiguration skeletonLdmConfiguration;
        skeletonLdmConfiguration.SetTileUnitDimension(d->tileDimension);
        return skeletonLdmConfiguration;
    }

    auto Octree2DConfigurator::CalculateMaximumSamplingLevel() const -> uint32_t {
        const auto dataLengthX = static_cast<uint32_t>(d->dataDimension.X());
        const auto dataLengthY = static_cast<uint32_t>(d->dataDimension.Y());

        const auto tileLengthX = static_cast<uint32_t>(d->tileDimension.X());
        const auto tileLengthY = static_cast<uint32_t>(d->tileDimension.Y());

        const auto samplingLevelX = CalculateMaxSamplingLevel(dataLengthX, tileLengthX);
        const auto samplingLevelY = CalculateMaxSamplingLevel(dataLengthY, tileLengthY);
        const auto maximumSamplingLevel = std::max(samplingLevelX, samplingLevelY);
        return maximumSamplingLevel;
    }

    auto Octree2DConfigurator::Configure() -> LdmConfiguration {
        d->maximumSamplingLevel = CalculateMaximumSamplingLevel();
        d->paddingDimension = CalculatePaddingDimension(d->maximumSamplingLevel);

        auto tileIndex = 0;
        d->ldmConfiguration.AddLdmTile(tileIndex, GenerateInitialLdmTile());
        tileIndex++;

        for (auto parentSamplingLevel = d->maximumSamplingLevel; parentSamplingLevel > 0; --parentSamplingLevel) {
            auto parentTiles = GetLdmTileBySamplingLevel(parentSamplingLevel);

            for (auto& parentTile : parentTiles) {
                for (auto i = 0; i < OctreeOrder2dXy::_size(); ++i) {
                    const auto octreeOrder = OctreeOrder2dXy::_from_index(i);
                    const auto childDataRange = CalChildDataRange(parentTile, octreeOrder);

                    if (!childDataRange.IsEmpty()) {
                        const auto childTile = GenerateChildLdmTile(tileIndex, parentTile, childDataRange);
                        parentTile->AddChildTilePointer(childTile);
                        d->ldmConfiguration.AddLdmTile(tileIndex, childTile);
                        tileIndex++;
                    }
                }
            }
        }

        return d->ldmConfiguration;
    }

    auto Octree2DConfigurator::CalculatePaddingDimension(const uint32_t& maximumSamplingLevel) const -> Dimension {
        const auto paddingLengthX = d->tileDimension.X() * static_cast<size_t>(std::pow(2, maximumSamplingLevel));
        const auto paddingLengthY = d->tileDimension.Y() * static_cast<size_t>(std::pow(2, maximumSamplingLevel));
        return Dimension(paddingLengthX, paddingLengthY);
    }

    auto Octree2DConfigurator::GenerateInitialLdmTile() const ->LdmTile::Pointer {
        LdmTile::Pointer initialTile(new LdmTile);
        initialTile->SetTileIndex(0);
        initialTile->SetSamplingLevel(d->maximumSamplingLevel);
        initialTile->SetDataRange(DataRange(Point(0, 0), Count(d->dataDimension)));
        initialTile->SetTileDataDimension(d->paddingDimension);
        initialTile->SetTileUnitDimension(d->tileDimension);
        return initialTile;
    }

    auto Octree2DConfigurator::GetLdmTileBySamplingLevel(const int32_t& samplingLevel) const
        -> QList<LdmTile::Pointer> {
        const auto numberOfTiles = d->ldmConfiguration.GetNumberOfTiles();
        QList<LdmTile::Pointer> matchedLdmTile;

        for (uint32_t i = 0; i < numberOfTiles; ++i) {
            const auto ldmTile = d->ldmConfiguration.GetLdmTilePointerByTileIndex(i);
            const auto samplingLevelMatched = (ldmTile->GetSamplingLevel() == samplingLevel);

            if (samplingLevelMatched) {
                matchedLdmTile.push_back(ldmTile);
            }
        }
        return matchedLdmTile;
    }

    auto Octree2DConfigurator::CalChildDataRange(const LdmTile::Pointer& parentTile,
        const OctreeOrder2dXy& order) -> DataRange {
        const auto childOffsetPoint = CalChildOffsetPoint(parentTile, order);
        const auto dataOverlap = IsDataOverlapped(parentTile, childOffsetPoint);
        DataRange childDataRange;
        if (dataOverlap) {
            const auto childTileDataDimension = CalculateChildTileDataDimension(parentTile->GetTileDataDimension());

            const auto parentDataLastPoint = parentTile->GetDataRange().GetLastPoint();
            const auto childTileDataLastPoint = childOffsetPoint + Count(childTileDataDimension);

            const auto childDataLastPoint = Point::Min(parentDataLastPoint, childTileDataLastPoint);

            childDataRange = DataRange(childOffsetPoint, childDataLastPoint);
        }

        return childDataRange;
    }

    auto Octree2DConfigurator::CalChildOffsetPoint(const LdmTile::Pointer& parentTile,
        const OctreeOrder2dXy& order) -> Point {
        const auto parentOffsetPoint = parentTile->GetDataRange().GetOffsetPoint();
        const auto childTileDataDimension = CalculateChildTileDataDimension(parentTile->GetTileDataDimension());
        Point childOffsetPoint{};

        switch (order) {
        case OctreeOrder2dXy::NN:
            childOffsetPoint = parentOffsetPoint;
            break;
        case OctreeOrder2dXy::NP:
            childOffsetPoint = Point(parentOffsetPoint.X(),
                                             parentOffsetPoint.Y() + childTileDataDimension.Y());
            break;
        case OctreeOrder2dXy::PN:
            childOffsetPoint = Point(parentOffsetPoint.X() + childTileDataDimension.X(),
                                             parentOffsetPoint.Y());
            break;
        case OctreeOrder2dXy::PP:
            childOffsetPoint = Point(parentOffsetPoint.X() + childTileDataDimension.X(),
                                             parentOffsetPoint.Y() + childTileDataDimension.Y());
            break;
        default:
            break;
        }
        return childOffsetPoint;
    }

    auto Octree2DConfigurator::CalculateChildTileDataDimension(const Dimension& parentTileDataDimension)
        -> Dimension {
        const auto childTileDataDimensionX = parentTileDataDimension.X() / 2;
        const auto childTileDataDimensionY = parentTileDataDimension.Y() / 2;

        const auto childTileDataDimension = Dimension(childTileDataDimensionX, childTileDataDimensionY);
        return childTileDataDimension;
    }

    auto Octree2DConfigurator::IsDataOverlapped(const LdmTile::Pointer& parentTile,
        const Point& childOffsetPoint) -> bool {
        const auto parentDataLastPoint = parentTile->GetDataRange().GetLastPoint();
        const auto inBoundaryX = childOffsetPoint.X() <= parentDataLastPoint.X();
        const auto inBoundaryY = childOffsetPoint.Y() <= parentDataLastPoint.Y();

        return inBoundaryX && inBoundaryY;
    }

    auto Octree2DConfigurator::GenerateChildLdmTile(const int32_t& tileIndex, const LdmTile::Pointer& parentTile,
        const DataRange& childDataRange) const -> LdmTile::Pointer {
        const auto parentTileDataDimension = parentTile->GetTileDataDimension();
        const auto childTileDataDimension = CalculateChildTileDataDimension(parentTileDataDimension);

        LdmTile::Pointer childTile(new LdmTile);
        childTile->SetTileIndex(tileIndex);
        childTile->SetTileUnitDimension(d->tileDimension);
        childTile->SetTileDataDimension(childTileDataDimension);
        childTile->SetDataRange(childDataRange);
        childTile->SetSamplingLevel(parentTile->GetSamplingLevel() - 1);
        childTile->SetParentTilePointer(parentTile);

        return childTile;
    }
}
