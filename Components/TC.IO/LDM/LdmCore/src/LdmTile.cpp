#include "LdmTile.h"
#include <QList>

namespace TC::IO::LdmCore {
    struct LdmTile::Impl {
        Impl() = default;
        ~Impl() = default;

        int32_t tileIndex{ -1 };
        int32_t samplingLevel{ -1 };
        DataRange dataRange{};
        Dimension tileDataDimension{};
        Dimension tileUnitDimension{};

        Pointer parentTilePointer{nullptr};
        QList<Pointer> childrenTilePointers{};
    };

    LdmTile::LdmTile()
        : d(new Impl()) {
    }

    LdmTile::~LdmTile() = default;

    auto LdmTile::SetTileIndex(const int32_t& tileIndex) -> void {
        d->tileIndex = tileIndex;
    }

    auto LdmTile::SetSamplingLevel(const int32_t& samplingLevel) -> void {
        d->samplingLevel = samplingLevel;
    }

    auto LdmTile::SetDataRange(const DataRange& dataRange) -> void {
        d->dataRange = dataRange;
    }

    auto LdmTile::SetTileDataDimension(const Dimension& tileDataDimension) -> void {
        d->tileDataDimension = tileDataDimension;
    }

    auto LdmTile::SetTileUnitDimension(const Dimension& tileUnitDimension) -> void {
        d->tileUnitDimension = tileUnitDimension;
    }

    auto LdmTile::SetParentTilePointer(const Pointer& ldmTilePointer) -> void {
        d->parentTilePointer = ldmTilePointer;
    }

    auto LdmTile::AddChildTilePointer(const Pointer& childLdmTilePointer) -> void {
        d->childrenTilePointers.push_back(childLdmTilePointer);
    }

    auto LdmTile::GetTileIndex() const -> int32_t {
        return d->tileIndex;
    }

    auto LdmTile::GetSamplingLevel() const -> int32_t {
        return d->samplingLevel;
    }

    auto LdmTile::GetDataRange() const -> DataRange {
        return d->dataRange;
    }

    auto LdmTile::GetTileDataDimension() const -> Dimension {
        return d->tileDataDimension;
    }

    auto LdmTile::GetTileUnitDimension() const -> Dimension {
        return d->tileUnitDimension;
    }

    auto LdmTile::GetParentTilePointer() const -> Pointer {
        return d->parentTilePointer;
    }

    auto LdmTile::GetChildrenPointerList() const -> QList<Pointer> {
        return d->childrenTilePointers;
    }
}
