#include "LdmMetaInfo.h"

namespace TC::IO::LdmCore {
    struct LdmMetaInfo::Impl {
        Impl() = default;
        ~Impl() = default;

        Dimension dataDimension{};
        Dimension tileUnitDimension{};
        uint32_t numberOfTiles{};
        uint32_t numberOfSamplingLevels{};
    };

    LdmMetaInfo::LdmMetaInfo()
        : d(new Impl()) {
    }

    LdmMetaInfo::LdmMetaInfo(const LdmMetaInfo& other)
        : d(new Impl(*other.d)){
    }

    LdmMetaInfo::~LdmMetaInfo() = default;

    auto LdmMetaInfo::operator=(const LdmMetaInfo& other) -> LdmMetaInfo& {
        *d = *(other.d);
        return *this;
    }

    auto LdmMetaInfo::GetDataDimension() const -> Dimension {
        return d->dataDimension;
    }

    auto LdmMetaInfo::GetTileUnitDimension() const -> Dimension {
        return d->tileUnitDimension;
    }

    auto LdmMetaInfo::GetNumberOfTiles() const -> uint32_t {
        return d->numberOfTiles;
    }

    auto LdmMetaInfo::GetNumberOfSamplingLevels() const -> uint32_t {
        return d->numberOfSamplingLevels;
    }

    auto LdmMetaInfo::SetDataDimension(const Dimension& dataDimension) -> void {
        d->dataDimension = dataDimension;
    }

    auto LdmMetaInfo::SetTileUnitDimension(const Dimension& tileUnitDimension) -> void {
        d->tileUnitDimension = tileUnitDimension;
    }

    auto LdmMetaInfo::SetNumberOfTiles(const uint32_t& numberOfTiles) -> void {
        d->numberOfTiles = numberOfTiles;
    }

    auto LdmMetaInfo::SetNumberOfSamplingLevels(const uint32_t& numberOfSamplingLevels) -> void {
        d->numberOfSamplingLevels = numberOfSamplingLevels;
    }
}
