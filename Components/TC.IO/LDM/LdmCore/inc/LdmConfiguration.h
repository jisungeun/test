#pragma once
#include <memory>

#include "TCLdmCoreExport.h"

#include "Dimension.h"
#include "LdmTile.h"

namespace TC::IO::LdmCore {
    class TCLdmCore_API LdmConfiguration {
    public:
        LdmConfiguration();
        LdmConfiguration(const LdmConfiguration& other);
        ~LdmConfiguration();
        auto operator=(const LdmConfiguration& other)->LdmConfiguration&;

        auto SetTileUnitDimension(const Dimension& tileUnitDimension)->void;
        auto AddLdmTile(const int32_t& tileIndex, const LdmTile::Pointer& ldmTile)->void;

        auto GetTileUnitDimension() const ->Dimension;
        auto GetNumberOfTiles() const ->uint32_t;
        auto GetLdmTilePointerByTileIndex(const int32_t& tileIndex) const ->LdmTile::Pointer;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
