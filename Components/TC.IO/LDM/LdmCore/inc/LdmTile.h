#pragma once
#include <memory>
#include <QList>

#include "TCLdmCoreExport.h"

#include "DataRange.h"
#include "Dimension.h"

namespace TC::IO::LdmCore {
    class TCLdmCore_API LdmTile {
    public:
        typedef LdmTile Self;
        typedef std::shared_ptr<Self> Pointer;

        LdmTile();
        ~LdmTile();

        auto SetTileIndex(const int32_t& tileIndex)->void;
        auto SetSamplingLevel(const int32_t& samplingLevel)->void;
        auto SetDataRange(const DataRange& dataRange)->void;
        auto SetTileDataDimension(const Dimension& tileDataDimension)->void;
        auto SetTileUnitDimension(const Dimension& tileUnitDimension)->void;
        auto SetParentTilePointer(const Pointer& ldmTilePointer)->void;
        auto AddChildTilePointer(const Pointer& childLdmTilePointer)->void;

        auto GetTileIndex() const ->int32_t;
        auto GetSamplingLevel() const ->int32_t;
        auto GetDataRange() const -> DataRange;
        auto GetTileDataDimension() const -> Dimension;
        auto GetTileUnitDimension() const -> Dimension;
        auto GetParentTilePointer() const ->Pointer;
        auto GetChildrenPointerList() const ->QList<LdmTile::Pointer>;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
