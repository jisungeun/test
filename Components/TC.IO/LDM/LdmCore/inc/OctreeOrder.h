#pragma once
#include <enum.h>

namespace TC::IO::LdmCore {
    BETTER_ENUM(OctreeOrder3dXyz, uint8_t, NNN, PNN, NPN, PPN, NNP, PNP, NPP, PPP)
    BETTER_ENUM(OctreeOrder2dXy, uint8_t, NN, PN, NP, PP)
}
