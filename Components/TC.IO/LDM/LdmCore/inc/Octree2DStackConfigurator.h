#pragma once
#pragma warning (disable : 4100)

#include <memory>
#include <enum.h>

#include "ILdmConfigurator.h"
#include "TCLdmCoreExport.h"
#include "OctreeOrder.h"

namespace TC::IO::LdmCore {
    class TCLdmCore_API Octree2DStackConfigurator final : public ILdmConfigurator{
    public:
        Octree2DStackConfigurator(const Dimension& dataDimension, const Dimension& tileDimension);
        ~Octree2DStackConfigurator();

        auto Configure()->LdmConfiguration override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto GenerateSkeletonLdmConfiguration(const Dimension& dimension) const->LdmConfiguration;
        auto CalculateSamplingLevel() const->uint32_t;
        auto CalculateSamplingLevel(const uint32_t& dataLength, const uint32_t& tileLength) const->uint32_t;
        auto CalculatePaddingDimension(const uint32_t& maximumSamplingLevel) const-> Dimension;

        auto GenerateInitialLdmTile() const->LdmTile::Pointer;
        auto GetLdmTileBySamplingLevel(const int32_t& samplingLevel) const->QList<LdmTile::Pointer>;

        auto CalChildDataRange(const LdmTile::Pointer& parentTile, const OctreeOrder2dXy& order)-> DataRange;
        static auto CalChildOffsetPoint(const LdmTile::Pointer& parentTile, const OctreeOrder2dXy& order)-> Point;
        static auto CalculateChildTileDataDimension(const Dimension& parentTileDataDimension)-> Dimension;
        static auto IsDataOverlapped(const LdmTile::Pointer& parentTile, const Point& childOffsetPoint)-> bool;

        auto GenerateChildLdmTile(const int32_t& tileIndex, const LdmTile::Pointer& parentTile,
            const DataRange& childDataRange)const->LdmTile::Pointer;
    };

}
