#pragma once
#include <memory>

#include "TCLdmCoreExport.h"
#include "Dimension.h"

namespace TC::IO::LdmCore {
    class TCLdmCore_API LdmMetaInfo {
    public:
        LdmMetaInfo();
        LdmMetaInfo(const LdmMetaInfo& other);
        ~LdmMetaInfo();

        auto operator=(const LdmMetaInfo& other)->LdmMetaInfo&;

        auto GetDataDimension() const -> Dimension;
        auto GetTileUnitDimension() const -> Dimension;
        auto GetNumberOfTiles() const ->uint32_t;
        auto GetNumberOfSamplingLevels() const ->uint32_t;

        auto SetDataDimension(const Dimension& dataDimension)->void;
        auto SetTileUnitDimension(const Dimension& tileUnitDimension)->void;
        auto SetNumberOfTiles(const uint32_t& numberOfTiles)->void;
        auto SetNumberOfSamplingLevels(const uint32_t& numberOfSamplingLevels)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
