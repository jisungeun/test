#pragma once
#include <memory>

#include "LdmConfiguration.h"

#include "TCLdmCoreExport.h"

namespace TC::IO::LdmCore {
    class TCLdmCore_API ILdmConfigurator {
    public:
        typedef ILdmConfigurator Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ILdmConfigurator() = default;
        virtual auto Configure()->LdmConfiguration = 0;
    };
}
