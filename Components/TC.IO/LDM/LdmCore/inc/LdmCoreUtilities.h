#pragma once
#include <string>

#include "TCLdmCoreExport.h"
#include "Dimension.h"
#include "DataRange.h"

namespace TC::IO::LdmCore {
    auto TCLdmCore_API TileName(const int32_t& tileIndex, const int32_t& numberOfTiles)->std::string;

    auto TCLdmCore_API GetSampledStep(const int32_t& samplingLevel)->size_t;
    auto TCLdmCore_API GetSampledLength(const size_t& originalLength, const int32_t& samplingLevel)->size_t;
    auto TCLdmCore_API CalculateSampledDimension(const DataRange& dataRange, const int32_t& samplingLevel)->Dimension;
    auto TCLdmCore_API CalculateSampledDimension2DStack(const DataRange& dataRange, const int32_t& samplingLevel)->Dimension;

    auto TCLdmCore_API CalculateMaxSamplingLevel(const uint32_t& dataLength, const uint32_t& tileLength)->uint32_t;
}
