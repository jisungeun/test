#include <catch2/catch.hpp>

#include "IMemoryDataSampler.h"

using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace IMemoryDataSamplerTest {
    auto memoryChunkPointer = new MemoryChunk;

    class DummyMemoryDataSampler : public IMemoryDataSampler {
    public:
        DummyMemoryDataSampler() = default;
        ~DummyMemoryDataSampler() = default;

        auto Sample(const LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer override {
            return MemoryChunk::Pointer(memoryChunkPointer);
        }
        auto SetSourceData(const MemoryChunk::Pointer& sourceData) -> void override {
            sourceDataIsSet = true;
        }

        auto IsSourceDataSet()->bool {
            return sourceDataIsSet;
        }
    private:
        bool sourceDataIsSet = false;
    };

    TEST_CASE("IMemoryDataSampler") {
        SECTION("Sample()") {
            DummyMemoryDataSampler dummyMemoryDataSampler;
            const auto sampledMemoryChunk = dummyMemoryDataSampler.Sample(LdmConfiguration{}, 0);
            CHECK(sampledMemoryChunk.get() == memoryChunkPointer);
        }
        SECTION("SetSourceData()") {
            DummyMemoryDataSampler dummyMemoryDataSampler;
            dummyMemoryDataSampler.SetSourceData(nullptr);
            CHECK(dummyMemoryDataSampler.IsSourceDataSet());
        }
    }
}
