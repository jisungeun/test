#include <catch2/catch.hpp>

#include "Hdf5DataSetTileWriter.h"
#include "H5Cpp.h"
#include "Octree2DConfigurator.h"
#include "Octree3DConfigurator.h"
using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace Hdf5DataSetTileWriterTest {
    auto CompareData(const uint32_t* data1, const uint32_t* data2, const uint32_t& numberOfElements) ->bool {
        auto dataSame = true;
        for (uint32_t i = 0; i < numberOfElements; ++i) {
            if (data1[i] != data2[i]) {
                dataSame = false;
                break;
            }
        }
        return dataSame;
    }

    TEST_CASE("Hdf5DataSetTileWriter") {
        H5::H5File file("Hdf5DataSetTileWriterTest.h5", H5F_ACC_TRUNC);

        SECTION("WriteTile 3D") {
            const auto tileUnitDimension3D = Dimension(8, 8, 8);

            Octree3DConfigurator octree3DConfigurator(Dimension(16, 16, 16), tileUnitDimension3D);
            const auto ldmConfiguration3D = octree3DConfigurator.Configure();

            MemoryChunk::Pointer memoryChunk(new MemoryChunk);

            const auto numberOfElements = tileUnitDimension3D.GetNumberOfElements();

            std::shared_ptr<uint32_t[]> data(new uint32_t[numberOfElements]());

            for (size_t i = 0; i < numberOfElements; ++i) {
                data.get()[i] = static_cast<uint32_t>(i);
            }

            memoryChunk->SetData(data, tileUnitDimension3D);

            Hdf5DataSetTileWriter hdf5DataSetTileWriter(file);
            hdf5DataSetTileWriter.WriteTile(ldmConfiguration3D, 3, memoryChunk);

            std::shared_ptr<uint32_t[]> readingData(new uint32_t[numberOfElements]());

            auto dataSet = file.openDataSet("TILE_3");
            dataSet.read(readingData.get(), H5::PredType::NATIVE_UINT32);
            dataSet.close();

            CHECK(CompareData(data.get(), readingData.get(), static_cast<uint32_t>(numberOfElements)));
        }

        SECTION("WriteTile 2D") {
            const auto tileUnitDimension2D = Dimension(8, 8);

            Octree2DConfigurator octree2DConfigurator(Dimension(16, 16), tileUnitDimension2D);
            const auto ldmConfiguration2D = octree2DConfigurator.Configure();

            MemoryChunk::Pointer memoryChunk(new MemoryChunk);

            const auto numberOfElements = tileUnitDimension2D.GetNumberOfElements();

            std::shared_ptr<uint32_t[]> data(new uint32_t[numberOfElements]());

            for (size_t i = 0; i < numberOfElements; ++i) {
                data.get()[i] = static_cast<uint32_t>(i);
            }

            memoryChunk->SetData(data, tileUnitDimension2D);

            Hdf5DataSetTileWriter hdf5DataSetTileWriter(file);
            hdf5DataSetTileWriter.WriteTile(ldmConfiguration2D, 2, memoryChunk);

            std::shared_ptr<uint32_t[]> readingData(new uint32_t[numberOfElements]());

            auto dataSet = file.openDataSet("TILE_2");
            dataSet.read(readingData.get(), H5::PredType::NATIVE_UINT32);
            dataSet.close();

            CHECK(CompareData(data.get(), readingData.get(), static_cast<uint32_t>(numberOfElements)));
        }

        file.close();
    }
}
