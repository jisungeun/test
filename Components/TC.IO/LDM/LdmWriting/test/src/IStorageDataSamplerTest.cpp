#include <catch2/catch.hpp>

#include "IStorageDataSampler.h"

using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace IStorageDataSamplerTest {
    class DummySamplerReader final : public LdmReading::ISamplerReader {
    public:
        DummySamplerReader() = default;
        ~DummySamplerReader() = default;

        auto Read(const LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer override {
            return MemoryChunk::Pointer{ nullptr };
        }
    };

    class DummyStorageDataSampler final : public IStorageDataSampler {
    public:
        DummyStorageDataSampler() = default;
        ~DummyStorageDataSampler() = default;

        auto SetSamplerReader(const LdmReading::ISamplerReader::Pointer& samplerReader) -> void override {
            this->samplerReader = samplerReader;
        }

        auto Sample(const LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer override {
            return MemoryChunk::Pointer{ nullptr };
        }

        auto GetSamplerReader()-> LdmReading::ISamplerReader::Pointer {
            return samplerReader;
        }

    private:
        LdmReading::ISamplerReader::Pointer samplerReader{nullptr};
    };

    TEST_CASE("IStorageDataSampler") {
        DummyStorageDataSampler dummyStorageDataSampler;
        auto dummySamplerReader = new DummySamplerReader;

        SECTION("SetSamplerReader()") {
            dummyStorageDataSampler.SetSamplerReader(DummySamplerReader::Pointer(dummySamplerReader));
            CHECK(dummyStorageDataSampler.GetSamplerReader().get() == dummySamplerReader);
        }
        SECTION("Sample()") {
            CHECK(dummyStorageDataSampler.Sample(LdmConfiguration{}, 0).get() == nullptr);
        }
    }
}