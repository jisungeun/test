#include <catch2/catch.hpp>

#include "ILdmInfoOutputPort.h"

using namespace TC::IO::LdmWriting;
using namespace TC::IO::LdmCore;

namespace ILdmInfoOutputPortTest {
    class DummyLdmInfoOutputPort final : public ILdmInfoOutputPort {
    public:
        DummyLdmInfoOutputPort() = default;
        ~DummyLdmInfoOutputPort() = default;

        auto SetLdmConfiguration(const LdmConfiguration& ldmConfiguration) -> void override {
            configurationIsSet = true;
        }
        auto IsConfigurationSet() const ->bool {
            return configurationIsSet;
        }
    private:
        bool configurationIsSet{ false };
    };

    TEST_CASE("ILdmInfoOutputPort") {
        SECTION("SetLdmConfiguration()") {
            DummyLdmInfoOutputPort dummyLdmInfoOutputPort;
            dummyLdmInfoOutputPort.SetLdmConfiguration(LdmConfiguration{});
            CHECK(dummyLdmInfoOutputPort.IsConfigurationSet());
        }
    }
}
