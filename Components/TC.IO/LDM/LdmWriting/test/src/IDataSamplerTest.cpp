#include <catch2/catch.hpp>

#include "IDataSampler.h"

using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace IDataSamplerTets {
    auto data = std::shared_ptr<int16_t[]>(new int16_t[24]);

    class DummyDataSampler final : public IDataSampler {
    public:
        DummyDataSampler() = default;
        ~DummyDataSampler() = default;

        auto Sample(const LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer override {
            MemoryChunk::Pointer result(new MemoryChunk);
            Dimension dimension(2, 3, 4);

            for (auto i = 0; i < dimension.GetNumberOfElements(); ++i) {
                data.get()[i] = i;
            }

            result->SetData(data, dimension);
            return result;
        }
    };

    TEST_CASE("IDataSampler") {
        SECTION("Sample()") {
            DummyDataSampler dummyDataSampler;

            LdmTile::Pointer ldmTile{ nullptr };

            const auto memoryChunk = dummyDataSampler.Sample(LdmConfiguration{}, 0);
            CHECK(memoryChunk->GetDimension() == Dimension(2,3,4));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::Int16Type);
            const auto resultData = std::any_cast<std::shared_ptr<int16_t[]>>(memoryChunk->GetData()).get();

            auto sameData = true;
            for (auto i = 0; i < memoryChunk->GetDimension().GetNumberOfElements(); ++i) {
                if (resultData[i] != data.get()[i]) {
                    sameData = false;
                    break;
                }
            }

            CHECK(sameData == true);
        }
    }
}