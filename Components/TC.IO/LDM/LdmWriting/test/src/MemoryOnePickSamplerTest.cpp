#include <catch2/catch.hpp>
#include "MemoryOnePickSampler.h"
#include <iostream>
using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace MemoryOnePickSamplerTest {
    auto GenerateRawDataUInt32(const Dimension& rawDataDimension) ->std::shared_ptr<uint32_t[]>{
        const auto sizeX = rawDataDimension.X();
        const auto sizeY = rawDataDimension.Y();
        const auto sizeZ = rawDataDimension.Z();

        const auto numberOfElements = sizeX * sizeY * sizeZ;
        std::shared_ptr<uint32_t[]> rawData(new uint32_t[numberOfElements]);
        for (auto k = 0; k < sizeZ; ++k) {
            for (auto j = 0; j < sizeY; ++j) {
                for (auto i = 0; i < sizeX; ++i) {
                    const auto index = i + j * sizeX + k * (sizeX * sizeY);

                    rawData.get()[index] = i + j * 1000 + k * 1000 * 1000;
                }
            }
        }
        return rawData;
    }

    auto GenerateAnswerDataUInt32(const Dimension& ansDataDimension, const Dimension& samplingSteps,
        const Point& offsetPoint) ->std::shared_ptr<uint32_t[]> {
        const auto sizeX = ansDataDimension.X();
        const auto sizeY = ansDataDimension.Y();
        const auto sizeZ = ansDataDimension.Z();

        const auto offsetX = offsetPoint.X();
        const auto offsetY = offsetPoint.Y();
        const auto offsetZ = offsetPoint.Z();

        const auto samplingStepX = samplingSteps.X();
        const auto samplingStepY = samplingSteps.Y();
        const auto samplingStepZ = samplingSteps.Z();

        const auto numberOfElements = sizeX * sizeY * sizeZ;
        std::shared_ptr<uint32_t[]> rawData(new uint32_t[numberOfElements]);
        for (auto k = 0; k < sizeZ; ++k) {
            for (auto j = 0; j < sizeY; ++j) {
                for (auto i = 0; i < sizeX; ++i) {
                    const auto index = i + j * sizeX + k * (sizeX * sizeY);

                    const auto answerX = (i * samplingStepX + offsetX);
                    const auto answerY = (j * samplingStepY + offsetY);
                    const auto answerZ = (k * samplingStepZ + offsetZ);

                    rawData.get()[index] = static_cast<uint32_t>(answerX + answerY * 1000 + answerZ * 1000 * 1000);
                }
            }
        }
        return rawData;
    }

    auto Compare(const uint32_t* sampleData, const uint32_t* answerData, const Dimension& dataDimension) {
        const auto numberOfElements = dataDimension.GetNumberOfElements();

        auto dataIsSame = true;

        for (auto i = 0; i < numberOfElements; ++i) {
            const auto sampleDataValue = sampleData[i];
            const auto answerDataValue = answerData[i];
            if (sampleDataValue != answerDataValue) {
                dataIsSame = false;
                break;
            }
        }

        return dataIsSame;
    }

    TEST_CASE("MemoryOnePickSamplerTest") {
        const Dimension rawDataDimension(100, 100, 100);

        const MemoryChunk::Pointer sourceData(new MemoryChunk);
        const auto rawData = GenerateRawDataUInt32(rawDataDimension);
        sourceData->SetData(rawData, rawDataDimension);

        MemoryOnePickSampler memoryOnePickSampler;

        LdmConfiguration ldmConfiguration;
        LdmTile::Pointer ldmTilePointer(new LdmTile);
        ldmConfiguration.AddLdmTile(0, ldmTilePointer);

        SECTION("Sample uint32_t offset(10, 11) count(10, 10) sampling(1, 1)") {
            const Point offsetPoint(10, 11);
            const Count count(10, 10);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(10, 10);
            const auto samplingSteps = Dimension(1, 1);
            const auto tileDataDimension = Dimension(10, 10);


            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryOnePickSampler.SetSourceData(sourceData);
            const auto sampledData = memoryOnePickSampler.Sample(ldmConfiguration,0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            const auto& answerDataDimension = tileUnitDimension;
            const auto answerData = GenerateAnswerDataUInt32(answerDataDimension, samplingSteps, offsetPoint);

            CHECK(Compare(sampleRawData.get(), answerData.get(), answerDataDimension));
        }

        SECTION("Sample uint32_t offset(50, 30) count(15, 14) sampling(1, 1)") {
            const Point offsetPoint(50, 30);
            const Count count(15, 14);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(15, 14);
            const auto samplingSteps = Dimension(1, 1);
            const auto tileDataDimension = Dimension(15, 14);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryOnePickSampler.SetSourceData(sourceData);
            const auto sampledData = memoryOnePickSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            const auto& answerDataDimension = tileUnitDimension;
            const auto answerData = GenerateAnswerDataUInt32(answerDataDimension, samplingSteps, offsetPoint);

            CHECK(Compare(sampleRawData.get(), answerData.get(), answerDataDimension));
        }

        SECTION("Sample uint32_t offset(50, 30, 20) count(15, 14, 13) sampling(2, 2, 2)") {
            const Point offsetPoint(50, 30);
            const Count count(30, 28);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(15, 14);
            const auto samplingSteps = Dimension(2, 2);
            const auto tileDataDimension = Dimension(30, 28);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryOnePickSampler.SetSourceData(sourceData);
            const auto sampledData = memoryOnePickSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            const auto& answerDataDimension = tileUnitDimension;
            const auto answerData = GenerateAnswerDataUInt32(answerDataDimension, samplingSteps, offsetPoint);

            CHECK(Compare(sampleRawData.get(), answerData.get(), answerDataDimension));
        }

        SECTION("Sample uint32_t offset(10, 11, 12) count(10, 10, 10) sampling(1, 1, 1)") {
            const Point offsetPoint(10, 11, 12);
            const Count count(10, 10, 10);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(10, 10, 10);
            const auto samplingSteps = Dimension(1, 1, 1);
            const auto tileDataDimension = Dimension(10, 10, 10);
            

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryOnePickSampler.SetSourceData(sourceData);
            const auto sampledData = memoryOnePickSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            const auto& answerDataDimension = tileUnitDimension;
            const auto answerData = GenerateAnswerDataUInt32(answerDataDimension, samplingSteps, offsetPoint);

            CHECK(Compare(sampleRawData.get(), answerData.get(), answerDataDimension));
        }

        SECTION("Sample uint32_t offset(50, 30, 20) count(15, 14, 13) sampling(1, 1, 1)") {
            const Point offsetPoint(50, 30, 20);
            const Count count(15, 14, 13);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(15, 14, 13);
            const auto samplingSteps = Dimension(1, 1, 1);
            const auto tileDataDimension = Dimension(15, 14, 13);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryOnePickSampler.SetSourceData(sourceData);
            const auto sampledData = memoryOnePickSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            const auto& answerDataDimension = tileUnitDimension;
            const auto answerData = GenerateAnswerDataUInt32(answerDataDimension, samplingSteps, offsetPoint);

            CHECK(Compare(sampleRawData.get(), answerData.get(), answerDataDimension));
        }

        SECTION("Sample uint32_t offset(50, 30, 20) count(15, 14, 13) sampling(2, 2, 2)") {
            const Point offsetPoint(50, 30, 20);
            const Count count(30, 28, 26);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(15, 14, 13);
            const auto samplingSteps = Dimension(2, 2, 2);
            const auto tileDataDimension = Dimension(30, 28, 26);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryOnePickSampler.SetSourceData(sourceData);
            const auto sampledData = memoryOnePickSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            const auto& answerDataDimension = tileUnitDimension;
            const auto answerData = GenerateAnswerDataUInt32(answerDataDimension, samplingSteps, offsetPoint);

            CHECK(Compare(sampleRawData.get(), answerData.get(), answerDataDimension));
        }
    }
}
