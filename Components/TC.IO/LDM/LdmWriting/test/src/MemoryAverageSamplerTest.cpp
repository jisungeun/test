#include <catch2/catch.hpp>
#include "MemoryAverageSampler.h"
#include <iostream>

using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace MemoryAverageSamplerTest {

    uint32_t randRawData2D[100] = {
    0, 14, 55, 98, 79, 46, 100, 83, 43, 5, 62, 69, 18, 40, 5, 85, 80, 37, 40, 27, 10, 61, 40, 14, 84, 84, 41, 75,
    21, 80, 25, 3, 16, 69, 46, 36, 47, 62, 95, 30, 1, 32, 16, 57, 45, 30, 85, 7, 14, 7, 57, 33, 33, 97, 27, 83, 27,
    62, 20, 10, 80, 93, 59, 21, 72, 72, 95, 19, 95, 57, 80, 33, 77, 59, 26, 10, 90, 27, 23, 50, 83, 99, 20, 0, 85,
    73, 3, 41, 74, 80, 88, 17, 61, 89, 59, 79, 44, 27, 67, 1};

    uint32_t randRawData3D[1000] = {
    0, 14, 55, 98, 79, 46, 100, 83, 43, 5, 62, 69, 18, 40, 5, 85, 80, 37, 40, 27, 10, 61, 40, 14, 84, 84, 41, 75,
    21, 80, 25, 3, 16, 69, 46, 36, 47, 62, 95, 30, 1, 32, 16, 57, 45, 30, 85, 7, 14, 7, 57, 33, 33, 97, 27, 83, 27,
    62, 20, 10, 80, 93, 59, 21, 72, 72, 95, 19, 95, 57, 80, 33, 77, 59, 26, 10, 90, 27, 23, 50, 83, 99, 20, 0, 85,
    73, 3, 41, 74, 80, 88, 17, 61, 89, 59, 79, 44, 27, 67, 1, 19, 44, 80, 89, 76, 32, 27, 76, 77, 9, 13, 51, 37,
    35, 20, 32, 40, 94, 30, 94, 43, 100, 72, 7, 1, 17, 66, 2, 35, 58, 66, 52, 22, 26, 1, 5, 79, 83, 97, 99, 58, 99,
    74, 74, 16, 82, 82, 43, 72, 75, 40, 8, 46, 77, 87, 39, 86, 89, 34, 23, 72, 62, 7, 73, 12, 41, 33, 13, 21, 91,
    27, 66, 6, 84, 27, 76, 45, 41, 92, 86, 89, 69, 40, 59, 46, 4, 90, 48, 64, 55, 90, 33, 53, 96, 93, 25, 62, 41,
    22, 43, 41, 0, 41, 35, 62, 4, 67, 96, 47, 71, 28, 10, 56, 84, 63, 20, 80, 44, 19, 14, 8, 97, 49, 69, 26, 18,
    14, 94, 98, 67, 40, 11, 51, 39, 80, 64, 8, 87, 18, 89, 10, 47, 62, 94, 6, 33, 68, 45, 4, 47, 20, 42, 77, 34,
    60, 99, 83, 47, 77, 80, 21, 9, 23, 26, 92, 64, 28, 46, 76, 43, 85, 95, 24, 16, 9, 57, 63, 71, 52, 2, 91, 6, 59,
    98, 81, 76, 66, 49, 12, 82, 36, 22, 56, 88, 60, 18, 84, 38, 83, 32, 34, 18, 78, 19, 92, 30, 29, 53, 87, 23, 14,
    4, 6, 78, 48, 92, 22, 98, 84, 58, 66, 14, 21, 68, 1, 10, 26, 86, 79, 47, 56, 97, 69, 22, 47, 95, 16, 84, 30,
    33, 65, 92, 38, 14, 58, 30, 25, 44, 20, 98, 84, 14, 53, 92, 60, 22, 21, 1, 15, 73, 46, 97, 97, 52, 97, 43, 13,
    53, 67, 98, 76, 73, 2, 33, 26, 42, 68, 76, 94, 88, 41, 27, 4, 56, 9, 18, 29, 75, 53, 68, 76, 19, 95, 87, 14,
    31, 75, 94, 42, 55,92, 81, 1, 82, 52, 66, 52, 64, 38, 66, 26, 57, 97, 82, 65, 9, 62, 63, 36, 38, 52, 55, 37,
    91, 57, 19, 54, 56, 84, 3, 15, 55, 67, 6, 40, 12, 3, 93, 11, 52, 49, 57, 44, 49, 75, 92, 16, 8, 4, 7, 65, 13,
    83, 86, 30, 60, 87, 59, 29, 50, 42, 71, 87, 95, 95, 4, 79, 9, 16, 78, 38, 8, 24, 50, 22, 47, 5, 68, 95, 95, 45,
    80, 95, 77, 1, 4, 22, 78, 75, 48, 29, 60, 99, 40, 38, 10, 57, 14, 22, 53, 47, 42, 98, 68, 64, 28, 16, 29, 7, 8,
    1, 68, 72, 8, 29, 33, 21, 19, 92, 89, 18, 79, 33, 45, 30, 97, 36, 98, 46, 46, 73, 47, 19, 93, 95, 67, 68, 97,
    37, 33, 18, 5, 91, 72, 55, 21, 48, 24, 72, 60, 77, 84, 82, 18, 92, 8, 88, 61, 12, 74, 75, 59, 6, 38, 44, 92,
    43, 27, 30, 98, 41, 38, 55, 33, 59, 37, 96, 5, 41, 55, 60, 95, 16, 84, 79, 41, 31, 81, 79, 48, 87, 99, 44, 57,
    78, 64, 1, 97, 99, 22, 88, 58, 59, 98, 34, 24, 30, 7, 28, 82, 25, 93, 51, 57, 87, 31, 86, 81, 60, 40, 95, 17,
    83, 67, 74, 25, 97, 56, 41, 66, 13, 24, 44, 52, 70, 48, 79, 82, 97, 47, 48, 73, 19, 80, 59, 86, 13, 84, 81, 75,
    25, 50, 24, 55, 40, 9, 18, 7, 35, 16, 51, 36, 1, 83, 73, 18, 77, 25, 81, 58, 38, 20, 21, 10, 48, 49, 75, 74,
    69, 18, 36, 71, 6, 74, 89, 99, 16, 98, 28, 93, 11, 50, 33, 13, 80, 63, 0, 34, 74, 47,67, 12, 16, 48, 81, 36,
    66, 31, 1, 58, 56, 4, 21, 10, 83, 98, 86, 49, 86, 83, 73, 49, 24, 72, 15, 25, 67, 2, 63, 84, 90, 87, 16, 54,
    45, 86, 73, 59, 31, 84, 94, 30, 57, 35, 88, 18, 48, 78, 27, 0, 25, 15, 30, 47, 77, 47, 94, 92, 87, 78, 30, 31,
    88, 48, 13, 49, 89, 90, 80, 30, 54, 35, 53, 79, 78, 19, 33, 53, 10, 3, 75, 15, 62, 94, 88, 18, 96, 75, 73, 8,
    2, 57, 75, 24, 52, 79, 9, 46, 8, 66,87, 49, 33, 5, 88, 53, 76, 7, 46, 67, 58, 55, 84, 80, 32, 17, 72, 53, 9,
    27, 6, 38, 55, 86, 5, 56, 63, 73, 58, 79, 41, 57, 85, 10, 3, 21, 22, 82, 97, 72, 58, 40, 57, 0, 81, 28, 46, 62,
    7, 36, 44, 35, 1, 62, 14, 69, 62, 83, 67, 24, 77, 83, 70, 5, 91, 9, 13, 44, 93, 27, 90, 76, 40, 84, 48, 93, 27,
    41, 66, 59, 42, 59, 90, 28, 93, 46, 46, 7, 39, 40, 24, 73, 80, 36, 52, 75, 26, 41, 97, 70, 26, 24, 45, 72, 85,
    55, 87, 39, 93, 16, 57, 31, 46, 82, 32, 95, 44, 53, 34, 44, 41, 43, 3, 77, 71, 8, 89, 75, 69, 75, 82, 33, 90,
    33, 88, 98, 1, 46, 84, 84, 40, 18, 17, 20, 93, 13, 98, 50, 51, 70, 5, 12, 63, 79, 72, 93, 70, 44, 95, 44, 45,
    68, 88, 4, 12, 10, 61, 36, 2, 21, 57, 70, 10, 97, 42, 58, 69, 70, 93, 35, 2, 2, 8, 98, 73, 10, 93, 20, 65, 71,
    55, 38, 70, 17, 1, 90, 62, 90, 10, 67 };



    auto GenerateRaw2DDataUInt32() ->std::shared_ptr<uint32_t[]> {
        std::shared_ptr<uint32_t[]> rawData(new uint32_t[100]);
        std::memcpy(rawData.get(), randRawData2D, sizeof(uint32_t) * 100);
        return rawData;
    }

    auto GenerateRaw3DDataUInt32() ->std::shared_ptr<uint32_t[]> {
        std::shared_ptr<uint32_t[]> rawData(new uint32_t[1000]);
        std::memcpy(rawData.get(), randRawData3D, sizeof(uint32_t) * 1000);
        return rawData;
    }

    auto Compare(const uint32_t* sampleData, const uint32_t* answerData, const Dimension& dataDimension) {
        const auto numberOfElements = dataDimension.GetNumberOfElements();
        auto dataIsSame = true;

        for (auto i = 0; i < numberOfElements; ++i) {
            const auto sampleDataValue = sampleData[i];
            const auto answerDataValue = answerData[i];
            if (sampleDataValue != answerDataValue) {
                dataIsSame = false;
                break;
            }
        }

        return dataIsSame;
    }

    TEST_CASE("MemoryAverageSamplerTest 2D") {
        const Dimension rawDataDimension(10, 10);

        const MemoryChunk::Pointer sourceData(new MemoryChunk);
        const auto rawData = GenerateRaw2DDataUInt32();
        sourceData->SetData(rawData, rawDataDimension);

        MemoryAverageSampler memoryAverageSampler;
        LdmConfiguration ldmConfiguration;
        LdmTile::Pointer ldmTilePointer(new LdmTile);
        ldmConfiguration.AddLdmTile(0, ldmTilePointer);

        SECTION("Sample uint32_t offset(1, 2) count(4, 2)") {
            const Point offsetPoint(1, 2);
            const Count count(4, 2);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(4, 2);
            const auto tileDataDimension = Dimension(4, 2);


            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryAverageSampler.SetSourceData(sourceData);
            const auto sampledData = memoryAverageSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            uint32_t answer[8] = { 61,40,14,84,3,16,69,46 };


            CHECK(Compare(sampleRawData.get(), answer, tileUnitDimension));
        }

        SECTION("Sample uint32_t offset(3, 3) count(4, 4) sampling(2, 2)") {
            const Point offsetPoint(3, 3);
            const Count count(4, 4);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(2, 2);
            const auto samplingSteps = Dimension(2, 2);
            const auto tileDataDimension = Dimension(4, 4);


            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryAverageSampler.SetSourceData(sourceData);
            const auto sampledData = memoryAverageSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            uint32_t answer[4] = { 54,50,54,69 };
            CHECK(Compare(sampleRawData.get(), answer, tileUnitDimension));
        }

        SECTION("Sample uint32_t offset(9, 8) count(1, 2) sampling(2, 2)") {
            const Point offsetPoint(9, 8);
            const Count count(1, 2);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(2, 2);
            const auto tileDataDimension = Dimension(4, 4);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryAverageSampler.SetSourceData(sourceData);
            const auto sampledData = memoryAverageSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            uint32_t answer[4] = { 41,0,0,0 };
            CHECK(Compare(sampleRawData.get(), answer, tileUnitDimension));
        }
    }

    TEST_CASE("MemoryAverageSamplerTest 3D") {
        const Dimension rawDataDimension(10, 10, 10);

        const MemoryChunk::Pointer sourceData(new MemoryChunk);
        const auto rawData = GenerateRaw3DDataUInt32();
        sourceData->SetData(rawData, rawDataDimension);

        MemoryAverageSampler memoryAverageSampler;
        LdmConfiguration ldmConfiguration;
        LdmTile::Pointer ldmTilePointer(new LdmTile);
        ldmConfiguration.AddLdmTile(0, ldmTilePointer);

        SECTION("Sample uint32_t offset(1, 2, 3) count(2, 2, 2)") {
            const Point offsetPoint(1, 2, 3);
            const Count count(2, 2, 2);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(2, 2, 2);
            const auto samplingSteps = Dimension(1, 1, 1);
            const auto tileDataDimension = Dimension(2, 2, 2);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryAverageSampler.SetSourceData(sourceData);
            const auto sampledData = memoryAverageSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            uint32_t answer[8] = { 14,21,97,69,55,37,55,67 };

            CHECK(Compare(sampleRawData.get(), answer, tileUnitDimension));
        }

        SECTION("Sample uint32_t offset(1, 2, 3) count(1, 1, 1)") {
            const Point offsetPoint(1, 2, 3);
            const Count count(1, 1, 1);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(2, 2, 2);
            const auto tileDataDimension = Dimension(2, 2, 2);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryAverageSampler.SetSourceData(sourceData);
            const auto sampledData = memoryAverageSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            uint32_t answer[8] = { 14,0,0,0,0,0,0,0 };

            CHECK(Compare(sampleRawData.get(), answer, tileUnitDimension));
        }

        SECTION("Sample uint32_t offset(1, 2, 3) count(4, 4, 4)") {
            const Point offsetPoint(1, 2, 3);
            const Count count(4, 4, 4);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(2, 2, 2);
            const auto tileDataDimension = Dimension(4, 4, 4);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryAverageSampler.SetSourceData(sourceData);
            const auto sampledData = memoryAverageSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            uint32_t answer[8] = { 52,42,49,58,43,66,54,59 };

            CHECK(Compare(sampleRawData.get(), answer, tileUnitDimension));
        }

        SECTION("Sample uint32_t offset(0, 0, 0) count(4, 4, 2)") {
            const Point offsetPoint(0, 0, 0);
            const Count count(4, 4, 2);
            const DataRange dataRange(offsetPoint, count);

            const auto tileUnitDimension = Dimension(2, 2, 2);
            const auto tileDataDimension = Dimension(4, 4, 2);

            ldmTilePointer->SetTileUnitDimension(tileUnitDimension);
            ldmTilePointer->SetDataRange(dataRange);
            ldmTilePointer->SetTileDataDimension(tileDataDimension);

            memoryAverageSampler.SetSourceData(sourceData);
            const auto sampledData = memoryAverageSampler.Sample(ldmConfiguration, 0);

            const auto sampleRawData = std::any_cast<std::shared_ptr<uint32_t[]>>(sampledData->GetData());
            uint32_t answer[8] = { 36,53,25,35,32,60,65,32 };

            CHECK(Compare(sampleRawData.get(), answer, tileUnitDimension));
        }

    }
}
