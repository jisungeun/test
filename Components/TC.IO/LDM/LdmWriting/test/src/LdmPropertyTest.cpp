#include <catch2/catch.hpp>

#include "LdmProperty.h"
#include "Octree2DConfigurator.h"

using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace LdmPropertyTest{
    TEST_CASE("LdmProperty") {
        Octree2DConfigurator octree2DConfigurator(Dimension(8, 8), Dimension(8, 8));
        const auto ldmConfiguration = octree2DConfigurator.Configure();

        SECTION("LdmProperty()") {
            LdmProperty ldmProperty;
            CHECK(ldmProperty.GetNumberOfTiles() == 0);

            const auto tileSizeCheck = ldmProperty.GetTileSize() == Count{};
            CHECK(tileSizeCheck);
        }

        SECTION("opertator =") {
            LdmProperty srcLdmProperty;
            srcLdmProperty.SetLdmConfiguration(ldmConfiguration);
            LdmProperty destLdmProperty;
            destLdmProperty = srcLdmProperty;

            CHECK(destLdmProperty.GetNumberOfTiles() == 1);
            const auto tileSizeCheck = destLdmProperty.GetTileSize() == Count(8, 8);
            CHECK(tileSizeCheck);
        }

        SECTION("SetNumberOfTiles()/GetNumberOfTiles()") {
            LdmProperty ldmProperty;
            CHECK(ldmProperty.GetNumberOfTiles() == 0);
            ldmProperty.SetLdmConfiguration(ldmConfiguration);
            CHECK(ldmProperty.GetNumberOfTiles() == 1);
        }

        SECTION("SetTileSize()/GetTileSize()") {
            LdmProperty ldmProperty;
            const auto initialTileSizeCheck = ldmProperty.GetTileSize() == Count{};
            CHECK(initialTileSizeCheck);

            ldmProperty.SetLdmConfiguration(ldmConfiguration);
            const auto tileSizeCheck = ldmProperty.GetTileSize() == Count(8, 8);
            CHECK(tileSizeCheck);
        }
    }
}