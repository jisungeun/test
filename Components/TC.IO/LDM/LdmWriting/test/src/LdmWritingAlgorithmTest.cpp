#include <catch2/catch.hpp>

#include "LdmWritingAlgorithm.h"

using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace LdmWritingAlgorithmTest {
    class DummyLdmConfigurator final : public ILdmConfigurator {
    public:
        DummyLdmConfigurator() = default;
        ~DummyLdmConfigurator() = default;
        auto Configure() -> LdmConfiguration override {
            numberOfCall++;

            LdmConfiguration ldmConfiguration;

            const Dimension tileUnitDimension{ 128,128,64 };
            ldmConfiguration.SetTileUnitDimension(tileUnitDimension);

            const LdmTile::Pointer ldmTilePointer1(new LdmTile);
            const LdmTile::Pointer ldmTilePointer2(new LdmTile);
            const LdmTile::Pointer ldmTilePointer3(new LdmTile);
            const LdmTile::Pointer ldmTilePointer4(new LdmTile);
            const LdmTile::Pointer ldmTilePointer5(new LdmTile);

            ldmConfiguration.AddLdmTile(0, ldmTilePointer1);
            ldmConfiguration.AddLdmTile(1, ldmTilePointer2);
            ldmConfiguration.AddLdmTile(2, ldmTilePointer3);
            ldmConfiguration.AddLdmTile(3, ldmTilePointer4);
            ldmConfiguration.AddLdmTile(4, ldmTilePointer5);

            return ldmConfiguration;
        }

        auto GetNumberOfCall()->int32_t {
            return numberOfCall;
        }
    private:
        int32_t numberOfCall{ 0 };
    };

    class DummyTileWriter final : public ITileWriter {
    public:
        DummyTileWriter() = default;
        ~DummyTileWriter() = default;
        auto GetNumberOfCall()->int32_t {
            return numberOfCall;
        }

        auto WriteTile(const LdmConfiguration& ldmConfiguration, const int32_t& ldmTile,
            const MemoryChunk::Pointer& ldmTileMemoryChunkPointer) -> void override {
            numberOfCall++;
        }
    private:
        int32_t numberOfCall{ 0 };
    };

    class DummyDataSampler final : public IDataSampler {
    public:
        DummyDataSampler() = default;
        ~DummyDataSampler() = default;
        auto Sample(const LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer override {
            numberOfCall++;
            return MemoryChunk::Pointer{ nullptr };
        }

        auto GetNumberOfCall()->int32_t {
            return numberOfCall;
        }
    private:
        int32_t numberOfCall{ 0 };
    };

    class DummyLdmInfoOutputPort final : public ILdmInfoOutputPort {
    public:
        DummyLdmInfoOutputPort() = default;
        ~DummyLdmInfoOutputPort() = default;

        auto SetLdmConfiguration(const LdmConfiguration& ldmConfiguration) -> void override {
            configurationIsSet = true;
        }
        auto IsConfigurationSet() const ->bool {
            return configurationIsSet;
        }
    private:
        bool configurationIsSet{ false };
    };

    TEST_CASE("LdmWritingAlgorithmTest") {

        const auto dummyLdmConfigurator = new DummyLdmConfigurator;
        const auto dummyTileWriter = new DummyTileWriter;
        const auto dummyDataSampler = new DummyDataSampler;
        const auto dummyLdmInfoOutputPort = new DummyLdmInfoOutputPort;


        const DummyLdmConfigurator::Pointer dummyLdmConfiguratorPointer(dummyLdmConfigurator);
        const DummyTileWriter::Pointer dummyTileWriterPointer(dummyTileWriter);
        const DummyDataSampler::Pointer dummyDataSamplerPointer(dummyDataSampler);

        const DummyLdmInfoOutputPort::Pointer dummyLdmInfoOutputPortPointer(dummyLdmInfoOutputPort);

        const LdmWritingAlgorithm ldmWritingAlgorithm(dummyLdmConfiguratorPointer, dummyTileWriterPointer, 
                                                      dummyDataSamplerPointer);

        SECTION("Write()") {
            ldmWritingAlgorithm.Write(dummyLdmInfoOutputPortPointer);

            CHECK(dummyLdmConfigurator->GetNumberOfCall() == 1);
            CHECK(dummyTileWriter->GetNumberOfCall() == 5);
            CHECK(dummyDataSampler->GetNumberOfCall() == 5);
            CHECK(dummyLdmInfoOutputPort->IsConfigurationSet());
        }
    }
}

