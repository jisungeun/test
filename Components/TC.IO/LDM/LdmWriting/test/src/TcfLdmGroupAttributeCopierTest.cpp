#include <catch2/catch.hpp>

#include "TcfLdmGroupAttributeCopier.h"
#include "H5Cpp.h"

using namespace TC::IO::LdmWriting;
using namespace TC::IO;

namespace TcfLdmGroupAttributeCopierTest {
    TEST_CASE("TcfLdmGroupAttributeCopier") {
        H5::H5File file("TcfLdmGroupAttributeCopierTest.h5", H5F_ACC_TRUNC);
        auto attribute1 = file.createAttribute("attribute1", H5::PredType::NATIVE_UINT16, H5::DataSpace(H5S_SCALAR));
        const uint16_t attribute1Value = 1;
        attribute1.write(H5::PredType::NATIVE_UINT16, &attribute1Value);

        auto group1 = file.createGroup("group1");

        const uint32_t attribute2Value = 2;
        auto attribute2 = group1.createAttribute("attribute2", H5::PredType::NATIVE_UINT32, H5::DataSpace(H5S_SCALAR));
        attribute2.write(H5::PredType::NATIVE_UINT32, &attribute2Value);

        auto group2 = group1.createGroup("group2");

        const float attribute3Value = 3;
        auto attribute3 = group2.createAttribute("attribute3", H5::PredType::NATIVE_FLOAT, H5::DataSpace(H5S_SCALAR));
        attribute3.write(H5::PredType::NATIVE_FLOAT, &attribute3Value);

        auto group3 = file.createGroup("group3");

        hsize_t dims[2] = { 1,1 };
        H5::DataSpace dataSpace(2, dims);
        auto dataSet1 = file.createDataSet("dataSet1", H5::PredType::NATIVE_INT32, dataSpace);


        attribute1.close();
        group1.close();
        attribute2.close();
        group2.close();
        attribute3.close();
        group3.close();

        dataSpace.close();
        dataSet1.close();

        SECTION("Copy") {
            H5::H5File fileOut("TcfLdmGroupAttributeCopierOutTest.h5", H5F_ACC_TRUNC);
            TcfLdmGroupAttributeCopier::Copy(file, fileOut);

            uint16_t outAttribute1Value{};
            uint32_t outAttribute2Value{};
            float outAttribute3Value{};

            CHECK(fileOut.attrExists("attribute1"));
            auto outAttribute1 = fileOut.openAttribute("attribute1");
            outAttribute1.read(H5::PredType::NATIVE_UINT16, &outAttribute1Value);
            CHECK(outAttribute1Value == 1);

            CHECK(fileOut.exists("group1"));
            auto outGroup1 = fileOut.openGroup("group1");

            CHECK(outGroup1.attrExists("attribute2"));
            auto outAttribute2 = outGroup1.openAttribute("attribute2");
            outAttribute2.read(H5::PredType::NATIVE_UINT32, &outAttribute2Value);
            CHECK(outAttribute2Value == 2);

            CHECK(outGroup1.exists("group2"));
            auto outGroup2 = outGroup1.openGroup("group2");

            CHECK(outGroup2.attrExists("attribute3"));
            auto outAttribute3 = outGroup2.openAttribute("attribute3");
            outAttribute3.read(H5::PredType::NATIVE_FLOAT, &outAttribute3Value);
            CHECK(outAttribute3Value == 3);

            CHECK(fileOut.exists("group3"));
            CHECK(fileOut.exists("dataSet1") == false);

            outAttribute1.close();
            outGroup1.close();
            outAttribute2.close();
            outGroup2.close();
            outAttribute3.close();

            fileOut.close();
        }
        file.close();
    }
}
