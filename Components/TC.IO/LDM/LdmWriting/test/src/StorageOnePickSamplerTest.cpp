#include <catch2/catch.hpp>

#include "StorageOnePickSampler.h"
#include "Hdf5DataSetHybridSamplerReader.h"
#include "Octree2DConfigurator.h"
#include "Octree2DStackConfigurator.h"
#include "Octree3DConfigurator.h"

using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace StorageOnePickSamplerTest {
    const uint32_t tile3D0[8 * 8 * 8] =
    { 39, 83, 33, 84, 81, 37, 56, 71, 82, 26, 60, 63, 72, 32, 4, 79, 60, 68, 18, 26, 67, 78, 76, 33, 78, 79, 9, 90, 91,
        21, 66, 91, 99, 68, 31, 77, 82, 96, 60, 58, 79, 65, 23, 56, 55, 93, 23, 29, 85, 24, 91, 90, 2, 37, 71, 27, 49,
        48, 94, 37, 9, 34, 72, 89, 35, 20, 24, 11, 60, 65, 76, 49, 67, 41, 23, 65, 25, 92, 84, 23, 30, 85, 36, 15, 65,
        33, 75, 34, 68, 43, 92, 69, 6, 81, 94, 55, 39, 93, 50, 63, 94, 79, 92, 74, 39, 37, 80, 4, 9, 12, 22, 41, 21,
        70, 35, 80, 33, 16, 51, 51, 72, 59, 46, 99, 65, 41, 79, 13, 43, 50, 35, 94, 67, 25, 4, 100, 8, 4, 23, 97, 47,
        93, 44, 31, 57, 56, 76, 38, 94, 48, 33, 81, 83, 5, 23, 98, 59, 88, 29, 46, 35, 87, 45, 12, 23, 78, 50, 59, 78,
        86, 26, 76, 68, 2, 18, 48, 71, 34, 35, 58, 32, 77, 93, 55, 59, 97, 90, 62, 75, 69, 94, 18, 7, 19, 82, 1, 8, 21,
        30, 84, 2, 74, 11, 14, 31, 22, 24, 68, 97, 72, 1, 48, 95, 98, 83, 17, 73, 19, 28, 58, 85, 38, 44, 24, 75, 43,
        94, 29, 17, 64, 51, 43, 3, 5, 67, 44, 54, 5, 19, 3, 12, 66, 39, 40, 29, 23, 15, 27, 26, 78, 86, 14, 90, 87, 91,
        98, 60, 63, 65, 31, 44, 80, 9, 80, 32, 25, 22, 32, 33, 56, 24, 93, 83, 84, 12, 91, 9, 2, 90, 47, 12, 84, 4, 45,
        81, 79, 22, 31, 25, 45, 17, 75, 40, 27, 48, 44, 94, 39, 51, 19, 66, 82, 91, 31, 66, 96, 87, 8, 57, 69, 19, 28,
        75, 1, 77, 21, 0, 60, 45, 14, 34, 30, 70, 52, 16, 40, 44, 25, 84, 48, 61, 71, 99, 60, 3, 73, 39, 1, 4, 26, 45,
        91, 68, 34, 8, 17, 20, 11, 55, 13, 11, 8, 87, 30, 33, 55, 26, 4, 20, 30, 44, 74, 64, 22, 88, 40, 53, 30, 17,
        58, 22, 31, 10, 60, 63, 2, 0, 70, 8, 97, 82, 92, 37, 18, 35, 71, 88, 94, 3, 1, 85, 60, 77, 24, 7, 43, 17, 100,
        69, 2, 29, 59, 34, 61, 30, 28, 96, 39, 82, 94, 18, 97, 96, 3, 99, 6, 17, 90, 25, 36, 78, 15, 53, 69, 86, 61,
        73, 66, 67, 48, 91, 52, 93, 79, 29, 31, 84, 89, 48, 8, 19, 84, 24, 89, 42, 28, 89, 41, 76, 69, 34, 22, 98, 97,
        80, 85, 100, 60, 43, 91, 29, 42, 5, 49, 27, 51, 79, 37, 59, 5, 40, 95, 79, 78, 14, 8, 57, 79, 97, 80, 13, 69,
        61, 3, 62, 63, 41, 13, 4, 32, 93, 63, 56, 51, 33, 24, 45, 83, 64, 94, 28, 57, 64, 13, 56, 66, 5, 32, 15, 31,
        86, 53 };

    const uint32_t tile3D1[8 * 8 * 8] =
    { 19, 69, 43, 51, 73, 0, 0, 0, 83, 65, 4, 33, 11, 0, 0, 0, 49, 62, 23, 34, 16, 0, 0, 0, 59, 95, 67, 95, 30, 0, 0,
        0, 4, 18, 87, 75, 42, 0, 0, 0, 56, 47, 34, 33, 53, 0, 0, 0, 63, 84, 3, 63, 28, 0, 0, 0, 50, 50, 91, 42, 83, 0,
        0, 0, 89, 94, 24, 63, 4, 0, 0, 0, 59, 51, 28, 33, 26, 0, 0, 0, 94, 28, 90, 31, 19, 0, 0, 0, 62, 23, 70, 65, 45,
        0, 0, 0, 25, 95, 85, 43, 77, 0, 0, 0, 10, 69, 38, 10, 63, 0, 0, 0, 55, 71, 63, 24, 31, 0, 0, 0, 44, 66, 3, 97,
        29, 0, 0, 0, 83, 29, 6, 27, 33, 0, 0, 0, 100, 13, 12, 17, 90, 0, 0, 0, 97, 53, 63, 56, 0, 0, 0, 0, 75, 82, 37,
        20, 48, 0, 0, 0, 20, 30, 32, 49, 15, 0, 0, 0, 4, 79, 80, 73, 31, 0, 0, 0, 19, 44, 30, 95, 17, 0, 0, 0, 58, 55,
        61, 76, 59, 0, 0, 0, 69, 20, 47, 11, 91, 0, 0, 0, 20, 60, 16, 73, 9, 0, 0, 0, 6, 8, 16, 35, 52, 0, 0, 0, 62,
        54, 27, 58, 55, 0, 0, 0, 69, 85, 76, 30, 69, 0, 0, 0, 30, 4, 61, 71, 30, 0, 0, 0, 57, 33, 97, 36, 60, 0, 0, 0,
        32, 71, 77, 72, 20, 0, 0, 0, 16, 53, 46, 83, 95, 0, 0, 0, 72, 53, 52, 17, 100, 0, 0, 0, 67, 63, 34, 95, 78, 0,
        0, 0, 36, 65, 33, 72, 99, 0, 0, 0, 9, 19, 99, 13, 49, 0, 0, 0, 1, 80, 47, 85, 13, 0, 0, 0, 37, 20, 75, 93, 46,
        0, 0, 0, 79, 41, 67, 56, 74, 0, 0, 0, 21, 94, 29, 99, 25, 0, 0, 0, 96, 57, 48, 36, 78, 0, 0, 0, 41, 94, 13, 56,
        81, 0, 0, 0, 23, 60, 23, 93, 50, 0, 0, 0, 82, 99, 18, 26, 92, 0, 0, 0, 36, 76, 67, 83, 10, 0, 0, 0, 50, 19, 33,
        18, 14, 0, 0, 0, 75, 52, 79, 88, 86, 0, 0, 0, 73, 99, 46, 33, 50, 0, 0, 0, 61, 96, 17, 9, 43, 0, 0, 0, 90, 34,
        79, 4, 71, 0, 0, 0, 25, 16, 89, 76, 56, 0, 0, 0, 22, 42, 14, 32, 26, 0, 0, 0, 24, 55, 94, 52, 8, 0, 0, 0, 81,
        3, 0, 95, 59, 0, 0, 0, 70, 87, 44, 67, 67, 0, 0, 0, 50, 68, 51, 42, 65, 0, 0, 0, 29, 90, 75, 46, 85, 0, 0, 0,
        18, 35, 42, 31, 10, 0, 0, 0, 96, 10, 74, 23, 59, 0, 0, 0, 76, 79, 69, 12, 58, 0, 0, 0, 95, 47, 96, 13, 52, 0,
        0, 0, 67, 69, 31, 95, 45, 0, 0, 0, 19, 68, 34, 49, 97, 0, 0, 0 };

    const uint32_t tile3D2[8 * 8 * 8] =
    { 87, 94, 3, 34, 51, 69, 34, 37, 33, 24, 59, 62, 95, 72, 3, 15, 21, 21, 4, 9, 84, 80, 84, 95, 49, 27, 42, 38, 94,
        59, 77, 65, 3, 78, 52, 19, 97, 10, 51, 98, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 82, 10, 95, 44, 50, 69, 52, 21, 93, 41, 13, 34, 53, 66, 62, 29, 51, 92, 7, 5, 38, 68, 56, 45, 19, 6, 71,
        23, 77, 1, 73, 24, 60, 38, 93, 58, 28, 13, 6, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 38, 95, 1, 49, 100, 87, 59, 59, 39, 99, 13, 22, 27, 58, 0, 15, 43, 16, 39, 14, 93, 63, 90, 45, 42,
        0, 84, 64, 70, 11, 68, 54, 22, 25, 21, 80, 10, 21, 7, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 89, 86, 67, 95, 8, 46, 99, 31, 97, 79, 61, 36, 79, 0, 39, 57, 99, 43, 44, 84, 17, 46, 83,
        61, 35, 35, 96, 4, 72, 82, 95, 90, 29, 6, 82, 42, 23, 87, 89, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 40, 13, 56, 65, 39, 62, 17, 5, 49, 6, 66, 32, 7, 59, 49, 41, 62, 67, 94, 27,
        100, 67, 9, 73, 7, 48, 4, 46, 66, 27, 93, 14, 14, 50, 16, 62, 23, 42, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 80, 98, 31, 43, 29, 61, 86, 37, 85, 21, 95, 92, 55, 85, 66, 38, 12, 98,
        41, 53, 78, 70, 86, 44, 66, 61, 1, 87, 33, 39, 27, 52, 75, 25, 31, 35, 10, 57, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 56, 14, 30, 70, 78, 2, 43, 55, 0, 22, 30, 48, 17, 13, 66, 1,
        25, 82, 91, 82, 73, 74, 22, 83, 43, 38, 49, 44, 25, 39, 8, 91, 43, 67, 21, 86, 8, 39, 47, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 59, 70, 75, 73, 75, 79, 6, 14, 2, 26, 29, 4, 34, 54, 47,
        68, 84, 69, 12, 80, 88, 74, 98, 35, 11, 36, 51, 41, 33, 74, 68, 51, 84, 54, 39, 83, 80, 44, 87, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    const uint32_t tile3D3[8 * 8 * 8] =
    { 6, 76, 47, 58, 79, 0, 0, 0, 52, 18, 91, 96, 27, 0, 0, 0, 5, 69, 33, 62, 62, 0, 0, 0, 84, 91, 69, 15, 10, 0, 0, 0,
        59, 16, 2, 84, 67, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 69, 78, 92,
        70, 8, 0, 0, 0, 76, 58, 47, 66, 17, 0, 0, 0, 40, 50, 52, 33, 72, 0, 0, 0, 57, 88, 77, 2, 9, 0, 0, 0, 21, 39,
        34, 57, 92, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 99, 46, 8, 66,
        0, 0, 0, 36, 69, 29, 94, 35, 0, 0, 0, 64, 97, 83, 97, 5, 0, 0, 0, 89, 10, 57, 61, 5, 0, 0, 0, 25, 90, 7, 96, 6,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 81, 86, 57, 100, 66, 0, 0, 0,
        37, 50, 45, 8, 70, 0, 0, 0, 70, 99, 29, 47, 46, 0, 0, 0, 63, 43, 75, 9, 16, 0, 0, 0, 96, 27, 9, 73, 42, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 45, 24, 80, 40, 67, 0, 0, 0, 10, 27,
        27, 44, 87, 0, 0, 0, 81, 1, 28, 70, 76, 0, 0, 0, 66, 93, 72, 26, 62, 0, 0, 0, 45, 52, 18, 3, 27, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 64, 55, 28, 2, 0, 0, 0, 39, 87, 23, 85,
        73, 0, 0, 0, 10, 60, 69, 81, 82, 0, 0, 0, 27, 100, 5, 19, 12, 0, 0, 0, 79, 13, 57, 98, 49, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 88, 89, 1, 47, 59, 0, 0, 0, 71, 50, 23, 65, 53, 0,
        0, 0, 66, 21, 71, 67, 47, 0, 0, 0, 44, 72, 69, 23, 32, 0, 0, 0, 56, 70, 27, 80, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 42, 39, 75, 18, 0, 0, 0, 6, 66, 3, 31, 56, 0, 0, 0,
        4, 63, 63, 1, 63, 0, 0, 0, 28, 53, 57, 62, 3, 0, 0, 0, 17, 19, 37, 36, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    const uint32_t tile3D4[8 * 8 * 8] =
    { 84, 31, 1, 14, 74, 53, 25, 46, 40, 64, 30, 3, 10, 93, 68, 49, 44, 56, 27, 81, 97, 62, 76, 43, 8, 66, 59, 5, 74,
        69, 59, 89, 58, 12, 40, 67, 1, 23, 82, 87, 87, 67, 19, 46, 68, 21, 93, 63, 21, 68, 39, 80, 49, 4, 64, 29, 9,
        93, 15, 36, 3, 66, 2, 29, 20, 49, 57, 6, 76, 39, 32, 43, 53, 56, 1, 76, 28, 84, 28, 63, 13, 83, 89, 46, 95, 75,
        82, 59, 75, 20, 83, 5, 41, 58, 34, 23, 31, 53, 78, 84, 22, 16, 87, 78, 19, 81, 79, 16, 63, 53, 27, 29, 98, 36,
        37, 12, 1, 46, 78, 92, 71, 55, 15, 27, 4, 38, 61, 30, 94, 36, 88, 19, 89, 35, 57, 61, 13, 77, 59, 27, 60, 76,
        85, 54, 26, 78, 30, 7, 69, 36, 98, 86, 38, 3, 17, 33, 39, 56, 79, 64, 99, 5, 34, 5, 22, 89, 35, 27, 34, 32, 7,
        10, 13, 84, 50, 11, 90, 80, 65, 7, 17, 24, 26, 6, 24, 63, 24, 90, 94, 44, 51, 62, 52, 8, 10, 47, 65, 27, 81, 3,
        96, 24, 28, 11, 39, 27, 88, 57, 27, 3, 91, 9, 12, 31, 16, 3, 53, 85, 11, 12, 98, 56, 9, 71, 91, 1, 79, 17, 52,
        38, 31, 21, 21, 55, 10, 97, 51, 25, 61, 26, 83, 12, 77, 29, 33, 68, 38, 96, 14, 59, 66, 60, 20, 26, 93, 18, 95,
        24, 93, 13, 80, 88, 31, 94, 11, 35, 77, 64, 57, 43, 8, 52, 6, 13, 40, 50, 81, 19, 78, 45, 16, 74, 72, 6, 9, 30,
        13, 31, 53, 45, 48, 52, 19, 32, 46, 47, 7, 2, 44, 31, 63, 44, 44, 78, 1, 68, 94, 85, 6, 36, 61, 17, 9, 88, 58,
        1, 74, 63, 13, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    const uint32_t tile3D5[8 * 8 * 8] =
    { 25, 59, 20, 65, 23, 0, 0, 0, 80, 74, 1, 68, 25, 0, 0, 0, 63, 37, 76, 6, 68, 0, 0, 0, 23, 45, 57, 22, 65, 0, 0, 0,
        38, 51, 65, 17, 70, 0, 0, 0, 45, 93, 63, 93, 84, 0, 0, 0, 50, 45, 73, 52, 73, 0, 0, 0, 54, 64, 89, 67, 2, 0, 0,
        0, 14, 84, 34, 80, 7, 0, 0, 0, 9, 58, 42, 1, 6, 0, 0, 0, 31, 72, 8, 92, 66, 0, 0, 0, 81, 34, 8, 2, 34, 0, 0, 0,
        51, 1, 7, 3, 13, 0, 0, 0, 87, 19, 90, 71, 64, 0, 0, 0, 98, 51, 97, 54, 75, 0, 0, 0, 33, 2, 39, 52, 81, 0, 0, 0,
        7, 20, 8, 79, 47, 0, 0, 0, 12, 72, 8, 35, 0, 0, 0, 0, 68, 67, 93, 97, 31, 0, 0, 0, 22, 6, 20, 84, 49, 0, 0, 0,
        24, 38, 96, 71, 26, 0, 0, 0, 1, 49, 34, 21, 68, 0, 0, 0, 73, 77, 96, 2, 91, 0, 0, 0, 56, 15, 34, 21, 57, 0, 0,
        0, 12, 41, 65, 77, 52, 0, 0, 0, 61, 83, 42, 13, 35, 0, 0, 0, 65, 15, 60, 9, 98, 0, 0, 0, 87, 73, 35, 66, 99, 0,
        0, 0, 90, 15, 22, 38, 90, 0, 0, 0, 5, 84, 4, 11, 74, 0, 0, 0, 76, 55, 15, 62, 66, 0, 0, 0, 86, 96, 37, 62, 73,
        0, 0, 0, 92, 0, 83, 42, 7, 0, 0, 0, 50, 13, 76, 59, 95, 0, 0, 0, 93, 78, 72, 28, 24, 0, 0, 0, 70, 2, 82, 59,
        63, 0, 0, 0, 69, 14, 76, 59, 21, 0, 0, 0, 0, 32, 12, 12, 24, 0, 0, 0, 54, 90, 38, 8, 74, 0, 0, 0, 41, 61, 3,
        3, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    const uint32_t tile3D6[8 * 8 * 8] =
    { 19, 83, 83, 3, 11, 51, 18, 25, 67, 18, 15, 22, 8, 49, 3, 70, 73, 95, 68, 71, 61, 86, 93, 77, 63, 20, 75, 55, 23,
        97, 48, 82, 47, 22, 1, 87, 14, 76, 79, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 83, 72, 35, 31, 18, 9, 2, 54, 68, 20, 65, 61, 20, 26, 70, 33, 69, 64, 93, 68, 72, 34, 2, 24, 53, 0, 9, 99,
        44, 38, 68, 55, 19, 51, 74, 99, 85, 15, 88, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 22, 98, 76, 31, 3, 83, 8, 70, 99, 16, 30, 59, 88, 15, 6, 27, 95, 62, 42, 38, 52, 76, 63, 42, 64, 31,
        6, 95, 81, 88, 62, 37, 50, 9, 3, 74, 45, 50, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 44, 62, 77, 85, 86, 96, 6, 80, 88, 75, 93, 24, 27, 50, 99, 4, 70, 31, 18, 73, 54, 43, 67, 31, 50,
        92, 2, 40, 55, 57, 62, 97, 61, 78, 83, 31, 96, 61, 15, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 61, 14, 78, 20, 60, 94, 60, 71, 6, 81, 71, 88, 80, 29, 13, 61, 66, 25, 14, 22, 16, 24, 12,
        41, 58, 2, 69, 32, 74, 92, 44, 91, 7, 53, 39, 34, 76, 93, 27, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    const uint32_t tile3D7[8 * 8 * 8] =
    { 35, 46, 67, 67, 58, 0, 0, 0, 29, 55, 12, 8, 92, 0, 0, 0, 70, 4, 71, 23, 9, 0, 0, 0, 93, 80, 45, 28, 55, 0, 0, 0,
        59, 67, 64, 91, 93, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22, 55,
        31, 50, 27, 0, 0, 0, 52, 28, 55, 7, 43, 0, 0, 0, 35, 70, 79, 23, 40, 0, 0, 0, 86, 88, 80, 12, 61, 0, 0, 0, 26,
        2, 86, 99, 77, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 49, 6, 54,
        27, 0, 0, 0, 74, 7, 61, 11, 93, 0, 0, 0, 82, 79, 56, 85, 6, 0, 0, 0, 39, 39, 91, 91, 63, 0, 0, 0, 14, 24, 20,
        33, 95, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 48, 8, 88, 91, 44, 0,
        0, 0, 8, 93, 44, 61, 40, 0, 0, 0, 9, 36, 64, 49, 22, 0, 0, 0, 20, 7, 39, 58, 88, 0, 0, 0, 78, 97, 84, 98, 94,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 2, 0, 17, 82, 0, 0, 0, 54,
        31, 1, 52, 9, 0, 0, 0, 22, 19, 17, 84, 98, 0, 0, 0, 55, 28, 15, 31, 65, 0, 0, 0, 86, 3, 52, 19, 28, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    const uint32_t sampling222Answer3D[8 * 8 * 8] =
    { 39,33,81,56,19,43,73,0,60,18,67,76,49,23,16,0,99,31,82,60,4,87,42,0,85,91,2,71,63,3,28,0,87,3,51,34,6,47,79,0,21,
        4,84,84,5,33,62,0,3,52,97,51,59,2,67,0,0,0,0,0,0,0,0,0,43,35,67,4,83,6,33,0,57,76,94,33,97,63,0,0,35,45,23,50,
        20,32,15,0,71,35,32,93,19,30,17,0,38,1,100,59,63,46,66,0,43,39,93,90,64,83,5,0,22,21,10,7,25,7,6,0,0,0,0,0,0,0,
        0,0,60,65,44,9,16,46,95,0,83,12,9,90,67,34,78,0,25,17,40,48,9,99,49,0,66,87,57,19,37,75,46,0,81,13,65,62,45,80,
        67,0,41,67,27,67,81,28,76,0,14,50,62,42,45,18,27,0,0,0,0,0,0,0,0,0,35,88,3,85,73,46,50,0,29,34,30,96,90,79,71,
        0,17,25,78,53,22,14,26,0,93,29,84,48,81,0,59,0,5,14,70,2,88,1,59,0,1,82,82,74,66,71,47,0,91,67,86,39,56,27,43,
        0,0,0,0,0,0,0,0,0,84,1,74,25,25,20,23,0,44,27,97,76,63,76,68,0,58,40,1,82,38,65,70,0,21,39,49,64,50,73,73,0,19,
        83,11,18,35,67,58,0,73,68,61,93,70,71,9,0,47,1,14,79,59,64,93,0,0,0,0,0,0,0,0,0,94,88,89,57,7,8,47,0,26,30,69,
        98,68,93,31,0,99,34,22,35,24,96,26,0,90,65,17,26,73,96,91,0,22,76,3,8,55,6,27,0,95,42,52,63,82,56,6,0,50,3,45,
        8,14,20,95,0,0,0,0,0,0,0,0,0,95,93,80,31,92,83,7,0,6,40,81,78,93,72,24,0,53,48,19,46,69,76,21,0,1,94,6,61,54,
        38,74,0,61,78,60,60,63,0,82,0,66,14,16,12,22,17,98,0,7,39,76,27,86,52,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

    const uint32_t sampling221Answer3D[8 * 8 * 8] =
    { 39,33,81,56,19,43,73,0,60,18,67,76,49,23,16,0,99,31,82,60,4,87,42,0,85,91,2,71,63,3,28,0,87,3,51,34,6,47,79,0,21,
        4,84,84,5,33,62,0,3,52,97,51,59,2,67,0,0,0,0,0,0,0,0,0,35,24,60,76,89,24,4,0,30,36,65,75,94,90,19,0,39,50,94,
        92,25,85,77,0,21,35,33,51,55,63,31,0,82,95,50,52,69,92,8,0,51,7,38,56,40,52,72,0,60,93,28,6,21,34,92,0,0,0,0,0,
        0,0,0,0,43,35,67,4,83,6,33,0,57,76,94,33,97,63,0,0,35,45,23,50,20,32,15,0,71,35,32,93,19,30,17,0,38,1,100,59,
        63,46,66,0,43,39,93,90,64,83,5,0,22,21,10,7,25,7,6,0,0,0,0,0,0,0,0,0,7,82,8,30,69,47,91,0,97,1,95,83,6,16,52,0,
        75,94,17,51,69,76,69,0,12,39,29,15,57,97,60,0,89,67,8,99,81,57,66,0,99,44,17,83,70,29,46,0,29,82,23,89,96,9,42,
        0,0,0,0,0,0,0,0,0,60,65,44,9,16,46,95,0,83,12,9,90,67,34,78,0,25,17,40,48,9,99,49,0,66,87,57,19,37,75,46,0,81,
        13,65,62,45,80,67,0,41,67,27,67,81,28,76,0,14,50,62,42,45,18,27,0,0,0,0,0,0,0,0,0,34,70,16,44,21,29,25,0,39,4,
        45,68,41,13,81,0,87,33,26,20,82,18,92,0,17,22,10,63,50,33,14,0,65,98,43,61,83,55,2,0,38,98,53,70,10,69,82,0,52,
        25,35,57,79,57,49,0,0,0,0,0,0,0,0,0,35,88,3,85,73,46,50,0,29,34,30,96,90,79,71,0,17,25,78,53,22,14,26,0,93,29,
        84,48,81,0,59,0,5,14,70,2,88,1,59,0,1,82,82,74,66,71,47,0,91,67,86,39,56,27,43,0,0,0,0,0,0,0,0,0,76,34,98,80,
        50,51,65,0,27,79,59,40,18,42,10,0,13,61,62,41,76,69,58,0,45,64,28,64,67,31,45,0,23,70,73,79,29,39,18,0,68,69,
        80,74,4,63,63,0,51,54,83,44,17,37,64,0,0,0,0,0,0,0,0,0 };


    const uint32_t tile2D0[8 * 8] =
    { 39, 83, 33, 84, 81, 37, 56, 71,
        82, 26, 60, 63, 72, 32, 4, 79,
        60, 68, 18, 26, 67, 78, 76, 33,
        78, 79, 9, 90, 91, 21, 66, 91,
        99, 68, 31, 77, 82, 96, 60, 58,
        79, 65, 23, 56, 55, 93, 23, 29,
        85, 24, 91, 90, 2, 37, 71, 27,
        49, 48, 94, 37, 9, 34, 72, 89 };

    const uint32_t tile2D1[8 * 8] =
    { 19, 69, 43, 51, 73, 0, 0, 0,
        83, 65, 4, 33, 11, 0, 0, 0,
        49, 62, 23, 34, 16, 0, 0, 0,
        59, 95, 67, 95, 30, 0, 0, 0,
        4, 18, 87, 75, 42, 0, 0, 0,
        56, 47, 34, 33, 53, 0, 0, 0,
        63, 84, 3, 63, 28, 0, 0, 0,
        50, 50, 91, 42, 83, 0, 0, 0 };

    const uint32_t tile2D2[8 * 8] =
    { 87, 94, 3, 34, 51, 69, 34, 37,
        33, 24, 59, 62, 95, 72, 3, 15,
        21, 21, 4, 9, 84, 80, 84, 95,
        49, 27, 42, 38, 94, 59, 77, 65,
        3, 78, 52, 19, 97, 10, 51, 98,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0 };

    const uint32_t tile2D3[8 * 8] =
    { 6, 76, 47, 58, 79, 0, 0, 0,
        52, 18, 91, 96, 27, 0, 0, 0,
        5, 69, 33, 62, 62, 0, 0, 0,
        84, 91, 69, 15, 10, 0, 0, 0,
        59, 16, 2, 84, 67, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0 };

    const uint32_t samplingAnswer2D[8 * 8] =
    { 39, 33, 81, 56, 19, 43, 73, 0,
        60, 18, 67, 76, 49, 23, 16, 0,
        99, 31, 82, 60, 4, 87, 42, 0,
        85, 91, 2, 71, 63, 3, 28, 0,
        87, 3, 51, 34, 6, 47, 79, 0,
        21, 4, 84, 84, 5, 33, 62, 0,
        3, 52, 97, 51, 59, 2, 67, 0,
        0, 0, 0, 0, 0, 0, 0, 0 };

    auto CompareData(const uint32_t* data1, const uint32_t* data2, const uint32_t& numberOfElements) ->bool {
        auto dataSame = true;
        for (uint32_t i = 0; i < numberOfElements; ++i) {
            if(data1[i] != data2[i]) {
                dataSame = false;
                break;
            }
        }
        return dataSame;
    }

    TEST_CASE("StorageOnePickSampler") {
        StorageOnePickSampler storageOnePickSampler;

        SECTION("3D Sampling") {
            const std::string fileName = "StorageOnePickSamplerTest.h5";

            H5::H5File file(fileName, H5F_ACC_TRUNC);
            const auto rank = 3;
            hsize_t dims[rank] = { 8,8,8 };

            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT32);
            auto dataSpace = H5::DataSpace(rank, dims);

            auto dataSet0 = file.createDataSet("TILE_1", dataType, dataSpace);
            auto dataSet1 = file.createDataSet("TILE_2", dataType, dataSpace);
            auto dataSet2 = file.createDataSet("TILE_3", dataType, dataSpace);
            auto dataSet3 = file.createDataSet("TILE_4", dataType, dataSpace);
            auto dataSet4 = file.createDataSet("TILE_5", dataType, dataSpace);
            auto dataSet5 = file.createDataSet("TILE_6", dataType, dataSpace);
            auto dataSet6 = file.createDataSet("TILE_7", dataType, dataSpace);
            auto dataSet7 = file.createDataSet("TILE_8", dataType, dataSpace);
            dataSpace.close();

            dataSet0.write(tile3D0, dataType);
            dataSet1.write(tile3D1, dataType);
            dataSet2.write(tile3D2, dataType);
            dataSet3.write(tile3D3, dataType);
            dataSet4.write(tile3D4, dataType);
            dataSet5.write(tile3D5, dataType);
            dataSet6.write(tile3D6, dataType);
            dataSet7.write(tile3D7, dataType);

            Octree3DConfigurator octree3DConfigurator(Dimension(16, 16, 16), Dimension(8, 8, 8));
            const auto ldmConfiguration = octree3DConfigurator.Configure();

            H5::DataSet dummyDataSet(hid_t{ -1 });
            LdmReading::Hdf5DataSetHybridSamplerReader::Pointer hdf5DataSetHybridSamplerReader = 
                std::make_shared<LdmReading::Hdf5DataSetHybridSamplerReader>(dummyDataSet, file);
            storageOnePickSampler.SetSamplerReader(hdf5DataSetHybridSamplerReader);
            const auto sampledResult = storageOnePickSampler.Sample(ldmConfiguration, 0);

            auto sampledResultDataRawPointer = 
                std::any_cast<std::shared_ptr<uint32_t[]>>(sampledResult->GetData()).get();

            CHECK(CompareData(sampledResultDataRawPointer, sampling222Answer3D, 8 * 8 * 8));

            dataSet0.close();
            dataSet1.close();
            dataSet2.close();
            dataSet3.close();
            dataSet4.close();
            dataSet5.close();
            dataSet6.close();
            dataSet7.close();
            file.close();
        }

        SECTION("2DStack Sampling") {
            const std::string fileName = "StorageOnePickSamplerTest.h5";

            H5::H5File file(fileName, H5F_ACC_TRUNC);
            const auto rank = 3;
            hsize_t dims[rank] = { 8,8,8 };

            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT32);
            auto dataSpace = H5::DataSpace(rank, dims);

            auto dataSet0 = file.createDataSet("TILE_1", dataType, dataSpace);
            auto dataSet1 = file.createDataSet("TILE_2", dataType, dataSpace);
            auto dataSet2 = file.createDataSet("TILE_3", dataType, dataSpace);
            auto dataSet3 = file.createDataSet("TILE_4", dataType, dataSpace);
            dataSpace.close();

            dataSet0.write(tile3D0, dataType);
            dataSet1.write(tile3D1, dataType);
            dataSet2.write(tile3D2, dataType);
            dataSet3.write(tile3D3, dataType);

            Octree2DStackConfigurator octree2DStackConfigurator(Dimension(16, 16, 8), Dimension(8, 8, 8));
            const auto ldmConfiguration = octree2DStackConfigurator.Configure();

            H5::DataSet dummyDataSet(hid_t{ -1 });
            LdmReading::Hdf5DataSetHybridSamplerReader::Pointer hdf5DataSetHybridSamplerReader =
                std::make_shared<LdmReading::Hdf5DataSetHybridSamplerReader>(dummyDataSet, file);
            storageOnePickSampler.SetSamplerReader(hdf5DataSetHybridSamplerReader);
            const auto sampledResult = storageOnePickSampler.Sample(ldmConfiguration, 0);

            auto sampledResultDataRawPointer =
                std::any_cast<std::shared_ptr<uint32_t[]>>(sampledResult->GetData()).get();

            CHECK(CompareData(sampledResultDataRawPointer, sampling221Answer3D, 8 * 8 * 8));

            dataSet0.close();
            dataSet1.close();
            dataSet2.close();
            dataSet3.close();
            file.close();
        }
        
        SECTION("2D Sampling") {
            const std::string fileName = "StorageOnePickSamplerTest.h5";

            H5::H5File file(fileName, H5F_ACC_TRUNC);
            const auto rank = 2;
            hsize_t dims[rank] = { 8,8 };

            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT32);
            auto dataSpace = H5::DataSpace(rank, dims);

            auto dataSet0 = file.createDataSet("TILE_1", dataType, dataSpace);
            auto dataSet1 = file.createDataSet("TILE_2", dataType, dataSpace);
            auto dataSet2 = file.createDataSet("TILE_3", dataType, dataSpace);
            auto dataSet3 = file.createDataSet("TILE_4", dataType, dataSpace);
            dataSpace.close();

            dataSet0.write(tile2D0, dataType);
            dataSet1.write(tile2D1, dataType);
            dataSet2.write(tile2D2, dataType);
            dataSet3.write(tile2D3, dataType);

            Octree2DConfigurator octree2DConfigurator(Dimension(16, 16), Dimension(8, 8));
            const auto ldmConfiguration = octree2DConfigurator.Configure();

            H5::DataSet dummyDataSet(hid_t{ -1 });
            LdmReading::Hdf5DataSetHybridSamplerReader::Pointer hdf5DataSetHybridSamplerReader =
                std::make_shared<LdmReading::Hdf5DataSetHybridSamplerReader>(dummyDataSet, file);
            storageOnePickSampler.SetSamplerReader(hdf5DataSetHybridSamplerReader);
            const auto sampledResult = storageOnePickSampler.Sample(ldmConfiguration, 0);

            auto sampledResultDataRawPointer =
                std::any_cast<std::shared_ptr<uint32_t[]>>(sampledResult->GetData()).get();

            CHECK(CompareData(sampledResultDataRawPointer, samplingAnswer2D, 8 * 8));

            dataSet0.close();
            dataSet1.close();
            dataSet2.close();
            dataSet3.close();
            file.close();
        }
    }
}
