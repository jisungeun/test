#include <catch2/catch.hpp>

#include "ITileWriter.h"

using namespace TC::IO;
using namespace LdmWriting;
using namespace LdmCore;

namespace ITileWriterTest {
    class DummyTileWriter final : public ITileWriter {
    public:
        DummyTileWriter() = default;
        ~DummyTileWriter() = default;

        auto GetResult()->bool {
            return writingFlag;
        }

        auto WriteTile(const LdmConfiguration& ldmConfiguration, const int32_t& ldmTile,
            const MemoryChunk::Pointer& ldmTileMemoryChunkPointer) -> void override {
            writingFlag = true;
            return;
        }
    private:
        bool writingFlag{ false };
    };

    TEST_CASE("ITileWirter") {
        SECTION("WriteTile()") {
            DummyTileWriter dummyTileWriter;
            const LdmConfiguration ldmConfiguration{};
            const int32_t tileIndex{};
            const MemoryChunk::Pointer ldmTileMemoryChunkPointer{ nullptr };

            dummyTileWriter.WriteTile(ldmConfiguration, tileIndex, ldmTileMemoryChunkPointer);

            CHECK(dummyTileWriter.GetResult() == true);
        }
    }
}
