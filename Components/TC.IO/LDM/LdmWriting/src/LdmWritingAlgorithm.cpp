#include "LdmWritingAlgorithm.h"

using namespace TC::IO::LdmCore;

namespace TC::IO::LdmWriting {
    struct LdmWritingAlgorithm::Impl {
        Impl() = default;
        ~Impl() = default;

        ILdmConfigurator::Pointer ldmConfiguratorPointer{ nullptr };
        ITileWriter::Pointer tileWriterPointer{ nullptr };
        IDataSampler::Pointer dataSamplerPointer{ nullptr };
    };

    LdmWritingAlgorithm::LdmWritingAlgorithm(const ILdmConfigurator::Pointer& ldmConfiguratorPointer,
        const ITileWriter::Pointer& tileWriterPointer, const IDataSampler::Pointer& dataSamplerPointer)
        : d(new Impl()) {
        d->ldmConfiguratorPointer = ldmConfiguratorPointer;
        d->tileWriterPointer = tileWriterPointer;
        d->dataSamplerPointer = dataSamplerPointer;
    }

    LdmWritingAlgorithm::~LdmWritingAlgorithm() = default;

    auto LdmWritingAlgorithm::Write(const ILdmInfoOutputPort::Pointer& ldmInfoOutputPort) const -> void {
        const auto ldmConfiguration = d->ldmConfiguratorPointer->Configure();
        const auto numberOfTiles = static_cast<int32_t>(ldmConfiguration.GetNumberOfTiles());

        ldmInfoOutputPort->SetLdmConfiguration(ldmConfiguration);

        for (auto tileIndex = numberOfTiles - 1; tileIndex >= 0; --tileIndex) {
            const auto ldmSampledMemoryChunk = d->dataSamplerPointer->Sample(ldmConfiguration, tileIndex);
            d->tileWriterPointer->WriteTile(ldmConfiguration, tileIndex, ldmSampledMemoryChunk);
        }
    }
}
