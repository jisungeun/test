#include "MemoryAverageSampler.h"

#include <complex>

using namespace TC::IO::LdmCore;

namespace TC::IO::LdmWriting {
    struct MemoryAverageSampler::Impl {
        Impl() = default;
        ~Impl() = default;
        MemoryChunk::Pointer sourceData{ nullptr };
    };

    MemoryAverageSampler::MemoryAverageSampler()
        : d(new Impl()) {
    }

    MemoryAverageSampler::~MemoryAverageSampler() = default;

    auto MemoryAverageSampler::Sample(const LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
        -> MemoryChunk::Pointer {
        const auto ldmTilePointer = ldmConfiguration.GetLdmTilePointerByTileIndex(tileIndex);
        const auto dataType = d->sourceData->GetDataType();
        MemoryChunk::Pointer sampledMemoryChunkPointer;
        if (dataType._value == ChunkDataType::Int8Type) {
            sampledMemoryChunkPointer = Sample<int8_t>(ldmTilePointer);

        } else if (dataType._value == ChunkDataType::UInt8Type) {
            sampledMemoryChunkPointer = Sample<uint8_t>(ldmTilePointer);

        } else if (dataType._value == ChunkDataType::Int16Type) {
            sampledMemoryChunkPointer = Sample<int16_t>(ldmTilePointer);

        } else if (dataType._value == ChunkDataType::UInt16Type) {
            sampledMemoryChunkPointer = Sample<uint16_t>(ldmTilePointer);

        } else if (dataType._value == ChunkDataType::Int32Type) {
            sampledMemoryChunkPointer = Sample<int32_t>(ldmTilePointer);

        } else if (dataType._value == ChunkDataType::UInt32Type) {
            sampledMemoryChunkPointer = Sample<uint32_t>(ldmTilePointer);

        } else if (dataType._value == ChunkDataType::FloatType) {
            sampledMemoryChunkPointer = Sample<float>(ldmTilePointer);

        } else if (dataType._value == ChunkDataType::DoubleType) {
            sampledMemoryChunkPointer = Sample<double>(ldmTilePointer);
        }

        return sampledMemoryChunkPointer;
    }

    auto MemoryAverageSampler::SetSourceData(const MemoryChunk::Pointer& sourceData) -> void {
        d->sourceData = sourceData;
    }

    template <class T>
    auto MemoryAverageSampler::Sample(const LdmTile::Pointer& ldmTilePointer) const -> MemoryChunk::Pointer {
        const auto dataRange = ldmTilePointer->GetDataRange();
        const auto offsetPointX = dataRange.GetOffsetPoint().X();
        const auto offsetPointY = dataRange.GetOffsetPoint().Y();
        const auto offsetPointZ = dataRange.GetOffsetPoint().Z();

        const auto dataBoundaryX = dataRange.GetLastPoint().X();
        const auto dataBoundaryY = dataRange.GetLastPoint().Y();
        const auto dataBoundaryZ = dataRange.GetLastPoint().Z();

        const auto tileUnitDimension = ldmTilePointer->GetTileUnitDimension();

        const auto tileDimensionX = static_cast<int64_t>(tileUnitDimension.X());
        const auto tileDimensionY = static_cast<int64_t>(tileUnitDimension.Y());
        const auto tileDimensionZ = static_cast<int64_t>(tileUnitDimension.Z());

        const auto tileDataDimension = ldmTilePointer->GetTileDataDimension();

        const auto samplingStepX = static_cast<int64_t>(tileDataDimension.X() / tileUnitDimension.X());
        const auto samplingStepY = static_cast<int64_t>(tileDataDimension.Y() / tileUnitDimension.Y());
        const auto samplingStepZ = static_cast<int64_t>(tileDataDimension.Z() / tileUnitDimension.Z());

        const auto numberOfSampledElements = tileUnitDimension.GetNumberOfElements();

        const auto sampledRawData = std::shared_ptr<T[]>(new T[numberOfSampledElements]());
        const auto sourceRawData = std::any_cast<std::shared_ptr<T[]>>(d->sourceData->GetData());

        const auto sourceDimensionX = d->sourceData->GetDimension().X();
        const auto sourceDimensionY = d->sourceData->GetDimension().Y();

        for (int64_t tileIndexZ = 0; tileIndexZ < tileDimensionZ; ++tileIndexZ) {
            const auto sourceIndexZ = tileIndexZ * samplingStepZ + offsetPointZ;
            const auto outBoundZ = sourceIndexZ > dataBoundaryZ;
            if (outBoundZ) { continue; }

            for (int64_t tileIndexY = 0; tileIndexY < tileDimensionY; ++tileIndexY) {
                const auto sourceIndexY = tileIndexY * samplingStepY + offsetPointY;
                const auto outBoundY = sourceIndexY > dataBoundaryY;
                if (outBoundY) { continue; }

                for (int64_t tileIndexX = 0; tileIndexX < tileDimensionX; ++tileIndexX) {
                    const auto sourceIndexX = tileIndexX * samplingStepX + offsetPointX;
                    const auto outBoundX = sourceIndexX > dataBoundaryX;
                    if (outBoundX) { continue; }

                    auto averageValue = 0.f;
                    auto numberOfAverageSample = 0;
                    for (int64_t averageIndexZ = 0; averageIndexZ < samplingStepZ; ++averageIndexZ) {
                        const auto averageSourceIndexZ = sourceIndexZ + averageIndexZ;
                        const auto averageOutBoundZ = averageSourceIndexZ > dataBoundaryZ;
                        if (averageOutBoundZ) { continue; }

                        for (int64_t averageIndexY = 0; averageIndexY < samplingStepY; ++averageIndexY) {
                            const auto averageSourceIndexY = sourceIndexY + averageIndexY;
                            const auto averageOutBoundY = averageSourceIndexY > dataBoundaryY;
                            if (averageOutBoundY) { continue; }

                            for (int64_t averageIndexX = 0; averageIndexX < samplingStepX; ++averageIndexX) {
                                const auto averageSourceIndexX = sourceIndexX + averageIndexX;
                                const auto averageOutBoundX = averageSourceIndexX > dataBoundaryX;
                                if (averageOutBoundX) { continue; }

                                const auto averageIndex =
                                    averageSourceIndexX +
                                    (averageSourceIndexY * sourceDimensionX) +
                                    (averageSourceIndexZ * sourceDimensionX * sourceDimensionY);
                                averageValue += static_cast<float>(sourceRawData.get()[averageIndex]);
                                numberOfAverageSample++;
                            }
                        }
                    }
                    averageValue /= static_cast<float>(numberOfAverageSample);
                    const auto sampleIndex =
                        tileIndexX +
                        (tileIndexY * tileDimensionX) +
                        (tileIndexZ * tileDimensionX * tileDimensionY);
                    sampledRawData.get()[sampleIndex] = static_cast<T>(std::round(averageValue));
                }
            }
        }

        MemoryChunk::Pointer sampledMemoryChunk(new MemoryChunk);
        sampledMemoryChunk->SetData(sampledRawData, tileUnitDimension);
        return sampledMemoryChunk;
    }
}