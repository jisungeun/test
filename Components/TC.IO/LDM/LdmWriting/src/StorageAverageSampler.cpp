#include "StorageAverageSampler.h"

using namespace TC::IO::LdmCore;

namespace TC::IO::LdmWriting {
    struct StorageAverageSampler::Impl {
        Impl() = default;
        ~Impl() = default;
        LdmReading::ISamplerReader::Pointer samplerReader{};
    };

    StorageAverageSampler::StorageAverageSampler()
        : d(new Impl()) {
    }

    StorageAverageSampler::~StorageAverageSampler() = default;

    auto StorageAverageSampler::SetSamplerReader(const LdmReading::ISamplerReader::Pointer& samplerReader) -> void {
        d->samplerReader = samplerReader;
    }

    auto StorageAverageSampler::Sample(const LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
        -> MemoryChunk::Pointer {
        MemoryChunk::Pointer sampledMemoryChunk;

        const auto parentLdmTilePointer = ldmConfiguration.GetLdmTilePointerByTileIndex(tileIndex);
        const auto tileHasChildren = !parentLdmTilePointer->GetChildrenPointerList().isEmpty();

        if (tileHasChildren) {
            sampledMemoryChunk = SampleFromChildren(ldmConfiguration, parentLdmTilePointer);
        } else {
            sampledMemoryChunk = d->samplerReader->Read(ldmConfiguration, tileIndex);
        }

        return sampledMemoryChunk;
    }

    auto StorageAverageSampler::SampleFromChildren(const LdmConfiguration& ldmConfiguration,
        const LdmTile::Pointer& parentTile) -> MemoryChunk::Pointer {
        const auto childrenMemoryChunkMap = LoadAllChildrenMemoryChunk(ldmConfiguration, parentTile);
        const auto samplingStep = GetSamplingStep(parentTile);

        const auto sampledChildrenMemoryChunkMap = SampleChildrenMemory(childrenMemoryChunkMap, samplingStep);

        auto parentChunkMemory = GenerateParentMemoryChunk(parentTile, sampledChildrenMemoryChunkMap);

        return parentChunkMemory;
    }

    auto StorageAverageSampler::LoadAllChildrenMemoryChunk(const LdmConfiguration& ldmConfiguration,
        const LdmTile::Pointer& parentLdmTilePointer) const -> QMap<LdmTile::Pointer, MemoryChunk::Pointer> {
        QMap<LdmTile::Pointer, MemoryChunk::Pointer> childrenMemoryChunkList;

        const auto childrenTilesList = parentLdmTilePointer->GetChildrenPointerList();
        for (auto& childTile : childrenTilesList) {
            const auto childTileIndex = childTile->GetTileIndex();
            const auto childMemoryChunk = d->samplerReader->Read(ldmConfiguration, childTileIndex);
            childrenMemoryChunkList[childTile] = (childMemoryChunk);
        }
        return childrenMemoryChunkList;
    }

    auto StorageAverageSampler::GetSamplingStep(const LdmTile::Pointer& parentLdmTile) -> SamplingStep {
        const auto parentTileDataDimension = parentLdmTile->GetTileDataDimension();
        const auto childTileDataDimension = parentLdmTile->GetChildrenPointerList().front()->GetTileDataDimension();

        const auto dimensionIs2D = (parentTileDataDimension.GetDimensionality() == 2);
        const auto dimensionIs3D = (parentTileDataDimension.GetDimensionality() == 3);

        const auto samplingStepX = parentTileDataDimension.X() / childTileDataDimension.X();
        const auto samplingStepY = parentTileDataDimension.Y() / childTileDataDimension.Y();
        const auto samplingStepZ = parentTileDataDimension.Z() / childTileDataDimension.Z();

        SamplingStep samplingStep;
        if (dimensionIs2D) {
            samplingStep = SamplingStep(samplingStepX, samplingStepY);
        } else if (dimensionIs3D) {
            samplingStep = SamplingStep(samplingStepX, samplingStepY, samplingStepZ);
        }
        return samplingStep;
    }

    auto StorageAverageSampler::SampleChildrenMemory(const ChildrenMap& childrenMemoryChunkMap,
        const SamplingStep& samplingStep)->ChildrenMap {

        ChildrenMap sampledChildrenMemoryMap;
        for (auto iter = childrenMemoryChunkMap.begin(); iter != childrenMemoryChunkMap.end(); ++iter) {
            auto childLdmTile = iter.key();
            auto childMemoryChunk = iter.value();

            auto sampledChildData = SampleChildMemory(childLdmTile, childMemoryChunk, samplingStep);
            sampledChildrenMemoryMap[childLdmTile] = sampledChildData;
        }

        return sampledChildrenMemoryMap;
    }

    auto StorageAverageSampler::SampleChildMemory(const LdmTile::Pointer& childLdmTile,
        const MemoryChunk::Pointer& childMemoryChunk, const SamplingStep& samplingStep)
        -> MemoryChunk::Pointer {
        auto data = childMemoryChunk->GetData();
        const auto dataType = childMemoryChunk->GetDataType();
        const auto dataDimension = childLdmTile->GetTileUnitDimension();

        auto sampledMemoryChunk = SampleData(data, dataDimension, dataType, samplingStep);

        return sampledMemoryChunk;
    }

    auto StorageAverageSampler::SampleData(std::any& data, const Dimension& dataDimension,
        const ChunkDataType& dataType, const SamplingStep& samplingStep) -> MemoryChunk::Pointer {

        MemoryChunk::Pointer sampledMemoryChunk;
        if (dataType == +ChunkDataType::Int8Type) {
            auto srcData = std::any_cast<std::shared_ptr<int8_t[]>>(data);
            sampledMemoryChunk = SampleRawData(srcData.get(), dataDimension, samplingStep);

        } else if (dataType == +ChunkDataType::UInt8Type) {
            auto srcData = std::any_cast<std::shared_ptr<uint8_t[]>>(data);
            sampledMemoryChunk = SampleRawData(srcData.get(), dataDimension, samplingStep);

        } else if (dataType == +ChunkDataType::Int16Type) {
            auto srcData = std::any_cast<std::shared_ptr<int16_t[]>>(data);
            sampledMemoryChunk = SampleRawData(srcData.get(), dataDimension, samplingStep);

        } else if (dataType == +ChunkDataType::UInt16Type) {
            auto srcData = std::any_cast<std::shared_ptr<uint16_t[]>>(data);
            sampledMemoryChunk = SampleRawData(srcData.get(), dataDimension, samplingStep);

        } else if (dataType == +ChunkDataType::Int32Type) {
            auto srcData = std::any_cast<std::shared_ptr<int32_t[]>>(data);
            sampledMemoryChunk = SampleRawData(srcData.get(), dataDimension, samplingStep);

        } else if (dataType == +ChunkDataType::UInt32Type) {
            auto srcData = std::any_cast<std::shared_ptr<uint32_t[]>>(data);
            sampledMemoryChunk = SampleRawData(srcData.get(), dataDimension, samplingStep);

        } else if (dataType == +ChunkDataType::FloatType) {
            auto srcData = std::any_cast<std::shared_ptr<float[]>>(data);
            sampledMemoryChunk = SampleRawData(srcData.get(), dataDimension, samplingStep);

        } else if (dataType == +ChunkDataType::DoubleType) {
            auto srcData = std::any_cast<std::shared_ptr<double[]>>(data);
            sampledMemoryChunk = SampleRawData(srcData.get(), dataDimension, samplingStep);

        }

        return sampledMemoryChunk;
    }

    template <class T>
    auto StorageAverageSampler::SampleRawData(T* srcData, const Dimension& dataDimension,
        const SamplingStep& samplingStep) -> MemoryChunk::Pointer {
        const auto samplingStepX = samplingStep.X();
        const auto samplingStepY = samplingStep.Y();
        const auto samplingStepZ = samplingStep.Z();

        const auto dataDimensionX = dataDimension.X();
        const auto dataDimensionY = dataDimension.Y();

        const auto sampledDimension = GetSampledDimension(dataDimension, samplingStep);

        const auto sampledDimensionX = sampledDimension.X();
        const auto sampledDimensionY = sampledDimension.Y();
        const auto sampledDimensionZ = sampledDimension.Z();

        const auto numberOfSampleElements = sampledDimension.GetNumberOfElements();
        auto sampledRawData = new T[numberOfSampleElements]();

        const auto samplingNumber = static_cast<float>(samplingStepX * samplingStepY * samplingStepZ);

        for (size_t k = 0; k < sampledDimensionZ; ++k) {
            for (size_t j = 0; j < sampledDimensionY; ++j) {
                for (size_t i = 0; i < sampledDimensionX; ++i) {

                    float sumValue{ 0 };
                    for (size_t samplingK = 0; samplingK < samplingStepZ; ++samplingK) {
                        for (size_t samplingJ = 0; samplingJ < samplingStepY; ++samplingJ) {
                            for (size_t samplingI = 0; samplingI < samplingStepX; ++samplingI) {
                                const auto srcDataIndex =
                                    ((i * samplingStepX) + samplingI) +
                                    (((j * samplingStepY) + samplingJ) * dataDimensionX) +
                                    (((k * samplingStepZ) + samplingK) * dataDimensionX * dataDimensionY);
                                sumValue += static_cast<float>(srcData[srcDataIndex]);
                            }
                        }
                    }
                    const auto sampleIndex =
                        i +
                        (j * sampledDimensionX) +
                        (k * sampledDimensionX * sampledDimensionY);

                    sampledRawData[sampleIndex] = static_cast<T>(std::round(sumValue / samplingNumber));
                }
            }
        }

        MemoryChunk::Pointer sampledMemoryChunk(new MemoryChunk);
        sampledMemoryChunk->SetData(std::shared_ptr<T[]>(sampledRawData), sampledDimension);

        return sampledMemoryChunk;
    }

    auto StorageAverageSampler::GetSampledDimension(const Dimension& dimension, const SamplingStep& samplingStep)
        -> Dimension {
        const auto sampledDimensionX = dimension.X() / samplingStep.X();
        const auto sampledDimensionY = dimension.Y() / samplingStep.Y();
        const auto sampledDimensionZ = dimension.Z() / samplingStep.Z();

        const auto dimensionIs2D = (dimension.GetDimensionality() == 2);
        const auto dimensionIs3D = (dimension.GetDimensionality() == 3);
        Dimension sampledDimension;
        if (dimensionIs2D) {
            sampledDimension = Dimension(sampledDimensionX, sampledDimensionY);
        } else if (dimensionIs3D) {
            sampledDimension = Dimension(sampledDimensionX, sampledDimensionY, sampledDimensionZ);
        }
        return sampledDimension;
    }

    auto StorageAverageSampler::GenerateParentMemoryChunk(const LdmTile::Pointer& parentLdmTile,
        const ChildrenMap& childrenMemoryChunkMap) -> MemoryChunk::Pointer {

        const auto tileUnitDimension = parentLdmTile->GetTileUnitDimension();
        const auto dataType = GetMemoryChunkDataType(childrenMemoryChunkMap);

        auto parentMemory = AllocateParentMemory(tileUnitDimension, dataType);

        for (auto iter = childrenMemoryChunkMap.begin(); iter != childrenMemoryChunkMap.end(); ++iter) {
            auto childLdmTile = iter.key();
            auto childrenSampledMemoryChunk = iter.value();

            auto childMemory = childrenSampledMemoryChunk->GetData();
            const auto subTileIndices = GetSubTileIndices(parentLdmTile->GetDataRange(), childLdmTile->GetDataRange());
            const auto childDataDimension = childrenSampledMemoryChunk->GetDimension();

            if (dataType == +ChunkDataType::Int8Type) {
                auto parentMemoryPointer = std::any_cast<std::shared_ptr<int8_t[]>>(parentMemory).get();
                auto childMemoryPointer = std::any_cast<std::shared_ptr<int8_t[]>>(childMemory).get();
                FillParentDataWithSampledChildData<int8_t>(childMemoryPointer, childDataDimension, subTileIndices,
                    tileUnitDimension, parentMemoryPointer);

            } else if (dataType == +ChunkDataType::UInt8Type) {
                auto parentMemoryPointer = std::any_cast<std::shared_ptr<uint8_t[]>>(parentMemory).get();
                auto childMemoryPointer = std::any_cast<std::shared_ptr<uint8_t[]>>(childMemory).get();
                FillParentDataWithSampledChildData<uint8_t>(childMemoryPointer, childDataDimension, subTileIndices,
                    tileUnitDimension, parentMemoryPointer);

            } else if (dataType == +ChunkDataType::Int16Type) {
                auto parentMemoryPointer = std::any_cast<std::shared_ptr<int16_t[]>>(parentMemory).get();
                auto childMemoryPointer = std::any_cast<std::shared_ptr<int16_t[]>>(childMemory).get();
                FillParentDataWithSampledChildData<int16_t>(childMemoryPointer, childDataDimension, subTileIndices,
                    tileUnitDimension, parentMemoryPointer);

            } else if (dataType == +ChunkDataType::UInt16Type) {
                auto parentMemoryPointer = std::any_cast<std::shared_ptr<uint16_t[]>>(parentMemory).get();
                auto childMemoryPointer = std::any_cast<std::shared_ptr<uint16_t[]>>(childMemory).get();
                FillParentDataWithSampledChildData<uint16_t>(childMemoryPointer, childDataDimension, subTileIndices,
                    tileUnitDimension, parentMemoryPointer);

            } else if (dataType == +ChunkDataType::Int32Type) {
                auto parentMemoryPointer = std::any_cast<std::shared_ptr<int32_t[]>>(parentMemory).get();
                auto childMemoryPointer = std::any_cast<std::shared_ptr<int32_t[]>>(childMemory).get();
                FillParentDataWithSampledChildData<int32_t>(childMemoryPointer, childDataDimension, subTileIndices,
                    tileUnitDimension, parentMemoryPointer);

            } else if (dataType == +ChunkDataType::UInt32Type) {
                auto parentMemoryPointer = std::any_cast<std::shared_ptr<uint32_t[]>>(parentMemory).get();
                auto childMemoryPointer = std::any_cast<std::shared_ptr<uint32_t[]>>(childMemory).get();
                FillParentDataWithSampledChildData<uint32_t>(childMemoryPointer, childDataDimension, subTileIndices,
                    tileUnitDimension, parentMemoryPointer);

            } else if (dataType == +ChunkDataType::FloatType) {
                auto parentMemoryPointer = std::any_cast<std::shared_ptr<float[]>>(parentMemory).get();
                auto childMemoryPointer = std::any_cast<std::shared_ptr<float[]>>(childMemory).get();
                FillParentDataWithSampledChildData<float>(childMemoryPointer, childDataDimension, subTileIndices,
                    tileUnitDimension, parentMemoryPointer);

            } else if (dataType == +ChunkDataType::DoubleType) {
                auto parentMemoryPointer = std::any_cast<std::shared_ptr<double[]>>(parentMemory).get();
                auto childMemoryPointer = std::any_cast<std::shared_ptr<double[]>>(childMemory).get();
                FillParentDataWithSampledChildData<double>(childMemoryPointer, childDataDimension, subTileIndices,
                    tileUnitDimension, parentMemoryPointer);
            }
        }
        auto parentMemoryChunk = GenerateParentMemoryChunkWithRawMemory(parentMemory, tileUnitDimension, dataType);
        return parentMemoryChunk;
    }

    auto StorageAverageSampler::GetMemoryChunkDataType(const ChildrenMap& childrenMemoryChunkMap)-> ChunkDataType {
        return (*childrenMemoryChunkMap.begin())->GetDataType();;
    }

    auto StorageAverageSampler::AllocateParentMemory(const Dimension& tileUnitDimension, const ChunkDataType& dataType)
        -> std::any {
        std::any parentMemory;
        const auto numberOfElements = tileUnitDimension.GetNumberOfElements();
        if (dataType == +ChunkDataType::Int8Type) {
            parentMemory = std::shared_ptr<int8_t[]>(new int8_t[numberOfElements]());

        } else if (dataType == +ChunkDataType::UInt8Type) {
            parentMemory = std::shared_ptr<uint8_t[]>(new uint8_t[numberOfElements]());

        } else if (dataType == +ChunkDataType::Int16Type) {
            parentMemory = std::shared_ptr<int16_t[]>(new int16_t[numberOfElements]());

        } else if (dataType == +ChunkDataType::UInt16Type) {
            parentMemory = std::shared_ptr<uint16_t[]>(new uint16_t[numberOfElements]());

        } else if (dataType == +ChunkDataType::Int32Type) {
            parentMemory = std::shared_ptr<int32_t[]>(new int32_t[numberOfElements]());

        } else if (dataType == +ChunkDataType::UInt32Type) {
            parentMemory = std::shared_ptr<uint32_t[]>(new uint32_t[numberOfElements]());

        } else if (dataType == +ChunkDataType::FloatType) {
            parentMemory = std::shared_ptr<float[]>(new float[numberOfElements]());

        } else if (dataType == +ChunkDataType::DoubleType) {
            parentMemory = std::shared_ptr<double[]>(new double[numberOfElements]());

        }
        return parentMemory;
    }

    auto StorageAverageSampler::GetSubTileIndices(const DataRange& parentDataRange,
        const DataRange& childDataRange) -> Point {
        const auto parentOffsetPointX = parentDataRange.GetOffsetPoint().X();
        const auto parentOffsetPointY = parentDataRange.GetOffsetPoint().Y();
        const auto parentOffsetPointZ = parentDataRange.GetOffsetPoint().Z();

        const auto childOffsetPointX = childDataRange.GetOffsetPoint().X();
        const auto childOffsetPointY = childDataRange.GetOffsetPoint().Y();
        const auto childOffsetPointZ = childDataRange.GetOffsetPoint().Z();

        const size_t subTileIndexX = (parentOffsetPointX < childOffsetPointX) ? 1 : 0;
        const size_t subTileIndexY = (parentOffsetPointY < childOffsetPointY) ? 1 : 0;
        const size_t subTileIndexZ = (parentOffsetPointZ < childOffsetPointZ) ? 1 : 0;

        return Point(subTileIndexX, subTileIndexY, subTileIndexZ);
    }

    template <class T>
    auto StorageAverageSampler::FillParentDataWithSampledChildData(T* sampledChildData,
        const Dimension& sampledDimension, const Point& subTileIndices,
        const Dimension& parentDimension, T* parentData) -> void {
        const auto sampledDimensionX = sampledDimension.X();
        const auto sampledDimensionY = sampledDimension.Y();
        const auto sampledDimensionZ = sampledDimension.Z();

        const auto parentDimensionX = parentDimension.X();
        const auto parentDimensionY = parentDimension.Y();

        const auto parentIndexOffsetX = sampledDimensionX * subTileIndices.X();
        const auto parentIndexOffsetY = sampledDimensionY * subTileIndices.Y();
        const auto parentIndexOffsetZ = sampledDimensionZ * subTileIndices.Z();

        for (size_t k = 0; k < sampledDimensionZ; ++k) {
            for (size_t j = 0; j < sampledDimensionY; ++j) {
                for (size_t i = 0; i < sampledDimensionX; ++i) {
                    const auto parentIndex =
                        (i + parentIndexOffsetX) +
                        ((j + parentIndexOffsetY) * parentDimensionX) +
                        ((k + parentIndexOffsetZ) * parentDimensionX * parentDimensionY);
                    const auto sampledChildIndex =
                        i +
                        (j * sampledDimensionX) +
                        (k * sampledDimensionX * sampledDimensionY);

                    parentData[parentIndex] = sampledChildData[sampledChildIndex];
                }
            }
        }
    }

    auto StorageAverageSampler::GenerateParentMemoryChunkWithRawMemory(const std::any& parentRawMemory,
        const Dimension& parentDataDimension, const ChunkDataType& dataType) -> MemoryChunk::Pointer {
        MemoryChunk::Pointer parentMemoryChunk(new MemoryChunk);
        if (dataType == +ChunkDataType::Int8Type) {
            auto data = std::any_cast<std::shared_ptr<int8_t[]>>(parentRawMemory);
            parentMemoryChunk->SetData(data, parentDataDimension);

        } else if (dataType == +ChunkDataType::UInt8Type) {
            auto data = std::any_cast<std::shared_ptr<uint8_t[]>>(parentRawMemory);
            parentMemoryChunk->SetData(data, parentDataDimension);

        } else if (dataType == +ChunkDataType::Int16Type) {
            auto data = std::any_cast<std::shared_ptr<int16_t[]>>(parentRawMemory);
            parentMemoryChunk->SetData(data, parentDataDimension);

        } else if (dataType == +ChunkDataType::UInt16Type) {
            auto data = std::any_cast<std::shared_ptr<uint16_t[]>>(parentRawMemory);
            parentMemoryChunk->SetData(data, parentDataDimension);

        } else if (dataType == +ChunkDataType::Int32Type) {
            auto data = std::any_cast<std::shared_ptr<int32_t[]>>(parentRawMemory);
            parentMemoryChunk->SetData(data, parentDataDimension);

        } else if (dataType == +ChunkDataType::UInt32Type) {
            auto data = std::any_cast<std::shared_ptr<uint32_t[]>>(parentRawMemory);
            parentMemoryChunk->SetData(data, parentDataDimension);

        } else if (dataType == +ChunkDataType::FloatType) {
            auto data = std::any_cast<std::shared_ptr<float[]>>(parentRawMemory);
            parentMemoryChunk->SetData(data, parentDataDimension);

        } else if (dataType == +ChunkDataType::DoubleType) {
            auto data = std::any_cast<std::shared_ptr<double[]>>(parentRawMemory);
            parentMemoryChunk->SetData(data, parentDataDimension);

        }
        return parentMemoryChunk;
    }
}
