#include "LdmProperty.h"

namespace TC::IO::LdmWriting {
    using namespace LdmCore;
    struct LdmProperty::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        LdmConfiguration ldmConfiguration{};
    };

    LdmProperty::LdmProperty()
        : d(new Impl()) {
    }

    LdmProperty::LdmProperty(const LdmProperty& other)
        : d(new Impl(*other.d)){
    }

    LdmProperty::~LdmProperty() = default;

    auto LdmProperty::SetLdmConfiguration(const LdmConfiguration& ldmConfiguration) -> void {
        d->ldmConfiguration = ldmConfiguration;
    }

    auto LdmProperty::operator=(const LdmProperty& other) -> LdmProperty& {
        *d = *(other.d);
        return *this;
    }

    auto LdmProperty::GetNumberOfTiles() const -> uint32_t {
        return d->ldmConfiguration.GetNumberOfTiles();
    }

    auto LdmProperty::GetTileSize() const -> Count {
        Count tileSize;

        const auto tileUnitDimension = d->ldmConfiguration.GetTileUnitDimension();
        if(tileUnitDimension.GetDimensionality() == 2) {
            tileSize = Count(tileUnitDimension.X(), tileUnitDimension.Y());
        } else if( tileUnitDimension.GetDimensionality() == 3) {
            tileSize = Count(tileUnitDimension.X(), tileUnitDimension.Y(), tileUnitDimension.Z());
        }

        return tileSize;
    }

    auto LdmProperty::GetDataDimension() const -> Dimension {
        const auto lowResolutionTile = d->ldmConfiguration.GetLdmTilePointerByTileIndex(0);
        const auto dataDimension = Dimension(lowResolutionTile->GetDataRange().GetCount());
        return dataDimension;
    }
}
