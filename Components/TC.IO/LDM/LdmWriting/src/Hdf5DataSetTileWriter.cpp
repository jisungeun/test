#include "Hdf5DataSetTileWriter.h"

#include "Hdf5Utilities.h"
#include "LdmCoreUtilities.h"

#include <complex>

namespace TC::IO::LdmWriting {
    using namespace LdmCore;

    struct Hdf5DataSetTileWriter::Impl {
        Impl() = default;
        ~Impl() = default;

        H5::Group* writingGroup{};
        bool compressionFlag{ false };

        int32_t chunkSizeX{};
        int32_t chunkSizeY{};
        int32_t chunkSizeZ{};
        bool chunkSizeSet{ false };

        auto CreatePropertyList(const Dimension& tileDimension)->H5::DSetCreatPropList;
    };

    auto Hdf5DataSetTileWriter::Impl::CreatePropertyList(const Dimension& tileDimension) -> H5::DSetCreatPropList {
        H5::DSetCreatPropList propList;

        if (this->compressionFlag) {
            if (tileDimension.GetDimensionality() == 2) {
                constexpr int32_t rank = 2;
                std::shared_ptr<hsize_t[]> dims{ new hsize_t[rank] };

                if (this->chunkSizeSet) {
                    dims[0] = this->chunkSizeY;
                    dims[1] = this->chunkSizeX;
                } else {
                    dims[0] = tileDimension.Y();
                    dims[1] = tileDimension.X();
                }
                propList.setChunk(rank, dims.get());
            } else if (tileDimension.GetDimensionality() == 3) {
                constexpr int32_t rank = 3;
                std::shared_ptr<hsize_t[]> dims{ new hsize_t[rank] };
                if (this->chunkSizeSet) {
                    dims[0] = this->chunkSizeZ;
                    dims[1] = this->chunkSizeY;
                    dims[2] = this->chunkSizeX;
                } else {
                    dims[0] = tileDimension.Z();
                    dims[1] = tileDimension.Y();
                    dims[2] = tileDimension.X();
                }


                propList.setChunk(rank, dims.get());
            }

            propList.setDeflate(1);
        } 

        return propList;
    }

    Hdf5DataSetTileWriter::Hdf5DataSetTileWriter(H5::Group& writingGroup)
        : d(new Impl()) {
        d->writingGroup = &writingGroup;
    }

    Hdf5DataSetTileWriter::~Hdf5DataSetTileWriter() = default;

    auto Hdf5DataSetTileWriter::SetChunkSize(const int32_t& chunkSizeX, const int32_t& chunkSizeY,
        const int32_t& chunkSizeZ) -> void {
        d->chunkSizeX = chunkSizeX;
        d->chunkSizeY = chunkSizeY;
        d->chunkSizeZ = chunkSizeZ;
        d->chunkSizeSet = true;
    }

    auto Hdf5DataSetTileWriter::WriteTile(const LdmConfiguration& ldmConfiguration, const int32_t& tileIndex,
        const MemoryChunk::Pointer& ldmTileMemoryChunkPointer) -> void {
        const auto numberOfTiles = ldmConfiguration.GetNumberOfTiles();
        const auto tileDataSetName = TileName(tileIndex, numberOfTiles);

        const auto chunkDataType = ldmTileMemoryChunkPointer->GetDataType();
        const auto writingDimension = ldmTileMemoryChunkPointer->GetDimension();

        const auto h5DataType = HDF5Utilities::ToH5DataType(chunkDataType);
        const auto dataSpace = HDF5Utilities::GetDataSpace(writingDimension);
        const auto rawData = ldmTileMemoryChunkPointer->GetDataPointer();

        const auto propList = d->CreatePropertyList(writingDimension);

        const auto dataSet = d->writingGroup->createDataSet(tileDataSetName, h5DataType, dataSpace, propList);
        dataSet.write(rawData, h5DataType);
        WriteLdmAttributes(dataSet, ldmConfiguration.GetLdmTilePointerByTileIndex(tileIndex));
    }

    auto Hdf5DataSetTileWriter::SetCompressionFlag(const bool& compressionFlag) -> void {
        d->compressionFlag = compressionFlag;
    }

    auto Hdf5DataSetTileWriter::WriteLdmAttributes(const H5::DataSet& dataSet, const LdmTile::Pointer& ldmTile)
        ->void {
        const auto tileUnitDimension = ldmTile->GetTileUnitDimension();
        {
            const auto is3D = (tileUnitDimension.GetDimensionality() == 3);
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "TileUnitSizeX", 
                static_cast<int64_t>(tileUnitDimension.X()));
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "TileUnitSizeY", 
                static_cast<int64_t>(tileUnitDimension.Y()));
            if (is3D) {
                HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "TileUnitSizeZ", 
                    static_cast<int64_t>(tileUnitDimension.Z()));
            }
        }

        const auto tileDataDimension = ldmTile->GetTileDataDimension();
        {
            const auto is3D = (tileDataDimension.GetDimensionality() == 3);
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "TileDataSizeX", 
                static_cast<int64_t>(tileDataDimension.X()));
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "TileDataSizeY", 
                static_cast<int64_t>(tileDataDimension.Y()));
            if (is3D) {
                HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "TileDataSizeZ", 
                    static_cast<int64_t>(tileDataDimension.Z()));
            }
        }

        const auto dataRange = ldmTile->GetDataRange();
        {
            const auto is3D = (dataRange.GetDimensionality() == 3);
            const auto dataOffsetPoint = dataRange.GetOffsetPoint();
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "DataIndexOffsetPointX",
                static_cast<int64_t>(dataOffsetPoint.X()));
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "DataIndexOffsetPointY",
                static_cast<int64_t>(dataOffsetPoint.Y()));

            const auto dataLastPoint = dataRange.GetLastPoint();
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "DataIndexLastPointX",
                static_cast<int64_t>(dataLastPoint.X()));
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "DataIndexLastPointY",
                static_cast<int64_t>(dataLastPoint.Y()));

            if (is3D) {
                HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "DataIndexOffsetPointZ",
                    static_cast<int64_t>(dataOffsetPoint.Z()));
                HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "DataIndexLastPointZ",
                    static_cast<int64_t>(dataLastPoint.Z()));
            }

        }

        const auto samplingLevel = ldmTile->GetSamplingLevel();
        {
            const auto samplingStep = static_cast<int64_t>(std::pow(2, samplingLevel));
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "SamplingStep", samplingStep);
        }

        const auto tileIndex = ldmTile->GetTileIndex();
        {
            HDF5Utilities::CreateAndWriteAttributeInt64(dataSet, "TileIndex", static_cast<int64_t>(tileIndex));
        }
    }
}
