#include "TcfLdmGroupAttributeCopier.h"

#include <QString>
#include <QStringList>

#include "Hdf5Utilities.h"

namespace TC::IO::LdmWriting {
    TcfLdmGroupAttributeCopier::TcfLdmGroupAttributeCopier() = default;
    TcfLdmGroupAttributeCopier::~TcfLdmGroupAttributeCopier() = default;

    auto TcfLdmGroupAttributeCopier::Copy(const H5::H5File& srcFile, const H5::H5File& destFile) -> void {
        CopyAllAttributes(srcFile, destFile);

        const auto numberOfObject = srcFile.getNumObjs();
        for (hsize_t i = 0; i < numberOfObject; ++i) {
            const auto objectIsGroup = (srcFile.getObjTypeByIdx(i) == H5G_GROUP);
            if (objectIsGroup) {
                const auto groupName = srcFile.getObjnameByIdx(i);

                if (groupName == "View") {
                    continue;;
                }

                auto group = srcFile.openGroup(groupName);
                CopyRecursive(group, destFile);
                group.close();
            }
        }
    }

    auto TcfLdmGroupAttributeCopier::CopyAllAttributes(const H5::H5Object& srcObject, const H5::H5Object& destObject) -> void {
        const auto numberOfAttributes = srcObject.getNumAttrs();
        for (hsize_t i = 0; i < numberOfAttributes; ++i) {
            auto attribute = srcObject.openAttribute(i);

            HDF5Utilities::CopyAttribute(attribute, destObject);
            attribute.close();
        }
    }

    auto TcfLdmGroupAttributeCopier::CopyRecursive(const H5::Group& srcGroup, const H5::Group& destPositionGroup)->void {
        HDF5Utilities::CopyGroupOnly(srcGroup, destPositionGroup);
        const auto nameOfGroup = HDF5Utilities::GetGroupName(srcGroup);
        auto copiedGroup = destPositionGroup.openGroup(nameOfGroup);

        CopyAllAttributes(srcGroup, copiedGroup);

        const auto numberOfObject = srcGroup.getNumObjs();
        for (hsize_t i = 0; i < numberOfObject; ++i) {
            const auto objectIsGroup = (srcGroup.getObjTypeByIdx(i) == H5G_GROUP);
            if (objectIsGroup) {
                const auto groupName = srcGroup.getObjnameByIdx(i);
                if (groupName == "View") {
                    continue;;
                }

                auto group = srcGroup.openGroup(groupName);
                CopyRecursive(group, copiedGroup);
                group.close();
            }
        }

        copiedGroup.close();
    }
}
