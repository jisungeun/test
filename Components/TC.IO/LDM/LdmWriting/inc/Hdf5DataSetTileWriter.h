#pragma once
#include "TCLdmWritingExport.h"
#include "ITileWriter.h"
#pragma warning(push)
#pragma warning(disable : 4268)
#include "H5Cpp.h"
#pragma warning(pop)

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API Hdf5DataSetTileWriter final : public ITileWriter{
    public:
        explicit Hdf5DataSetTileWriter(H5::Group& writingGroup);
        ~Hdf5DataSetTileWriter();

        auto SetChunkSize(const int32_t& chunkSizeX, const int32_t& chunkSizeY, const int32_t& chunkSizeZ)->void;
        auto WriteTile(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex,
            const MemoryChunk::Pointer& ldmTileMemoryChunkPointer) -> void override;

        auto SetCompressionFlag(const bool& compressionFlag)->void;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto WriteLdmAttributes(const H5::DataSet& dataSet, const LdmCore::LdmTile::Pointer& ldmTile)->void;
    };
}
