#pragma once

#include "TCLdmWritingExport.h"
#pragma warning(push)
#pragma warning(disable : 4268)
#include "H5Cpp.h"
#pragma warning(pop)

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API TcfLdmGroupAttributeCopier {
    public:
        TcfLdmGroupAttributeCopier();
        ~TcfLdmGroupAttributeCopier();

        static auto Copy(const H5::H5File& srcFile, const H5::H5File& destFile)->void;
    private:
        static auto CopyAllAttributes(const H5::H5Object& srcObject, const H5::H5Object& destObject)->void;
        static auto CopyRecursive(const H5::Group& srcGroup, const H5::Group& destPositionGroup)->void;
    };
}
