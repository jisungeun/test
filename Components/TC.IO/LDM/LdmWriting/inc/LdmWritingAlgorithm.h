#pragma once
#include <memory>

#include "TCLdmWritingExport.h"

#include "ILdmConfigurator.h"
#include "ITileWriter.h"
#include "IDataSampler.h"
#include "ILdmInfoOutputPort.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API LdmWritingAlgorithm {
    public:
        LdmWritingAlgorithm(const LdmCore::ILdmConfigurator::Pointer& ldmConfiguratorPointer,
            const ITileWriter::Pointer& tileWriterPointer, const IDataSampler::Pointer& dataSamplerPointer);
        ~LdmWritingAlgorithm();

        auto Write(const ILdmInfoOutputPort::Pointer& ldmInfoOutputPort) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}