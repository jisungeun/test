#pragma once
#include <memory>

#include "LdmConfiguration.h"
#include "TCLdmWritingExport.h"

#include "MemoryChunk.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API IDataSampler {
    public:
        typedef IDataSampler Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~IDataSampler() = default;
        virtual auto Sample(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer = 0;
    };
}
