#pragma once
#include <memory>
#include "TCLdmWritingExport.h"

#include "LdmConfiguration.h"
#include "MemoryChunk.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API ITileWriter {
    public:
        typedef ITileWriter Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ITileWriter() = default;
        virtual auto WriteTile(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& ldmTile,
            const MemoryChunk::Pointer& ldmTileMemoryChunkPointer)->void = 0;
    };
}
