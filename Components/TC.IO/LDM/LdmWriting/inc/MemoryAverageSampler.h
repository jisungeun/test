#pragma once
#include <memory>
#include "TCLdmWritingExport.h"
#include "IMemoryDataSampler.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API MemoryAverageSampler final : public IMemoryDataSampler{
    public:
        MemoryAverageSampler();
        ~MemoryAverageSampler();
        auto Sample(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            ->MemoryChunk::Pointer override;
        auto SetSourceData(const MemoryChunk::Pointer& sourceData)->void override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        template <class T>
        auto Sample(const LdmCore::LdmTile::Pointer& ldmTilePointer) const-> MemoryChunk::Pointer;
    };
}
