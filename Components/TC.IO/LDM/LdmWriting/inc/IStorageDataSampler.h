#pragma once

#include "TCLdmWritingExport.h"
#include "IDataSampler.h"
#include "ISamplerReader.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API IStorageDataSampler : public IDataSampler {
    public:
        virtual ~IStorageDataSampler() = default;
        virtual auto SetSamplerReader(const LdmReading::ISamplerReader::Pointer& samplerReader)->void = 0;
        auto Sample(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer override = 0;
    };
}
