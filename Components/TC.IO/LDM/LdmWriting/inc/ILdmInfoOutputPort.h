#pragma once

#include "TCLdmWritingExport.h"
#include "LdmConfiguration.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API ILdmInfoOutputPort {
    public:
        typedef ILdmInfoOutputPort Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ILdmInfoOutputPort() = default;
        virtual auto SetLdmConfiguration(const LdmCore::LdmConfiguration& ldmConfiguration)->void = 0;
    };
}
