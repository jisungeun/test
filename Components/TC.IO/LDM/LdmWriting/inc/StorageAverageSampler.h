#pragma once
#include <QMap>

#include "TCLdmWritingExport.h"
#include "IStorageDataSampler.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API StorageAverageSampler final : public IStorageDataSampler{
    public:
        StorageAverageSampler();
        ~StorageAverageSampler();

        auto SetSamplerReader(const LdmReading::ISamplerReader::Pointer& samplerReader) -> void override;
        auto Sample(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)-> MemoryChunk::Pointer override;
    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    private:
        typedef QMap<LdmCore::LdmTile::Pointer, MemoryChunk::Pointer> ChildrenMap;
        typedef Dimension SamplingStep;

        auto SampleFromChildren(const LdmCore::LdmConfiguration& ldmConfiguration, const LdmCore::LdmTile::Pointer& parentTile)
            -> MemoryChunk::Pointer;

        auto LoadAllChildrenMemoryChunk(const LdmCore::LdmConfiguration& ldmConfiguration,
            const LdmCore::LdmTile::Pointer& parentLdmTilePointer) const->ChildrenMap;

        static auto GetSamplingStep(const LdmCore::LdmTile::Pointer& parentLdmTile)->SamplingStep;
        auto SampleChildrenMemory(const ChildrenMap& childrenMemoryChunkMap, const SamplingStep& samplingStep)
            ->ChildrenMap;
        auto SampleChildMemory(const LdmCore::LdmTile::Pointer& childLdmTile, const MemoryChunk::Pointer& childMemoryChunk,
            const SamplingStep& samplingStep)-> MemoryChunk::Pointer;
        auto SampleData(std::any& data, const Dimension& dataDimension, const ChunkDataType& dataType,
            const SamplingStep& samplingStep)-> MemoryChunk::Pointer;
        template <class T>
        auto SampleRawData(T* srcData, const Dimension& dataDimension, const SamplingStep& samplingStep)
            -> MemoryChunk::Pointer;
        static auto GetSampledDimension(const Dimension& dimension, const SamplingStep& samplingStep)
            -> Dimension;

        auto GenerateParentMemoryChunk(const LdmCore::LdmTile::Pointer& parentLdmTile,
            const ChildrenMap& childrenMemoryChunkMap)-> MemoryChunk::Pointer;
        static auto GetMemoryChunkDataType(const ChildrenMap& childrenMemoryChunkMap)-> ChunkDataType;
        static auto AllocateParentMemory(const Dimension& tileUnitDimension, 
            const ChunkDataType& dataType)->std::any;
        static auto GetSubTileIndices(const DataRange& parentDataRange,
            const DataRange& childDataRange)->Point;

        template <class T>
        auto FillParentDataWithSampledChildData(T* sampledChildData, const Dimension& sampledDimension,
            const Point& subTileIndices, const Dimension& parentDimension, T* parentData)->void;
        auto GenerateParentMemoryChunkWithRawMemory(const std::any& parentRawMemory,
            const Dimension& parentDataDimension, const ChunkDataType& dataType)
            ->MemoryChunk::Pointer;
    };
}
