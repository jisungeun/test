#pragma once

#include "TCLdmWritingExport.h"

#include "IDataSampler.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API IMemoryDataSampler : public IDataSampler{
    public:
        virtual ~IMemoryDataSampler() = default;
        auto Sample(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer override = 0;
        virtual auto SetSourceData(const MemoryChunk::Pointer& sourceData)->void = 0;
    };
}
