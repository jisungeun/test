#pragma once
#include <memory>
#include "TCLdmWritingExport.h"
#include "Count.h"
#include "ILdmInfoOutputPort.h"

namespace TC::IO::LdmWriting {
    class TCLdmWriting_API LdmProperty : public ILdmInfoOutputPort {
    public:
        LdmProperty();
        LdmProperty(const LdmProperty& other);
        ~LdmProperty();

        auto SetLdmConfiguration(const LdmCore::LdmConfiguration& ldmConfiguration) -> void override;

        auto operator=(const LdmProperty& other)->LdmProperty&;

        auto GetNumberOfTiles() const ->uint32_t;
        auto GetTileSize() const -> Count;
        auto GetDataDimension() const-> Dimension;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
