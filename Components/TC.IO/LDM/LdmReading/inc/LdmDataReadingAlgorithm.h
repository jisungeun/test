#pragma once

#include "TCLdmReadingExport.h"
#include "ILdmDataReader.h"
#include "ILdmDataOutputPort.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API LdmDataReadingAlgorithm {
    public:
        LdmDataReadingAlgorithm();
        ~LdmDataReadingAlgorithm();
        auto ReadData(ILdmDataReader::Pointer& ldmDataReader, ILdmDataOutputPort::Pointer& ldmDataOutputPort)
            ->void;
    };
}
