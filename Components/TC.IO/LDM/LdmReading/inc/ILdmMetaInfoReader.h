#pragma once
#include <memory>
#include "TCLdmReadingExport.h"
#include "LdmMetaInfo.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API ILdmMetaInfoReader {
    public:
        typedef ILdmMetaInfoReader Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ILdmMetaInfoReader() = default;
        virtual auto Read()-> LdmCore::LdmMetaInfo = 0;
    };
}
