#pragma once
#pragma warning(disable:4458)

#include <string>
#include <memory>
#include <qmap.h>

#include "TCLdmReadingExport.h"

#include "ILdmDataReader.h"
#pragma warning(push)
#pragma warning(disable : 4268)
#include "H5Cpp.h"
#pragma warning(pop)
#include "DataRange.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API TcfH5GroupRoiLdmDataReader final : public ILdmDataReader {
    public:
        TcfH5GroupRoiLdmDataReader();
        ~TcfH5GroupRoiLdmDataReader();

        auto SetLdmDataGroup(H5::Group& ldmDataGroup)->void;
        auto SetReadingRoi(const DataRange& roiDataRange)->void;
        auto SetSamplingStep(const int32_t& samplingStep)->void;
        auto SetLdmDataIs2DStack(const bool& ldmDataIs2DStack)->void;
        auto Read()-> MemoryChunk::Pointer override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto MergeRoiMemoryChunk(const QMap<std::string, MemoryChunk::Pointer>& subTileDataMap, 
            const QMap<std::string, DataRange>& mappingRangeMap, const Dimension& roiDataDimension)
        ->MemoryChunk::Pointer;
        auto CopyDataForMerge(void* tileRangeDataPointer, const DataRange& mappingRange,
            const ChunkDataType& dataType, const Dimension& roiDataDimension, void* roiDataPointer)
            ->void;

        template <class T>
        auto CopyData(void* tileDataPointer, const DataRange& mappingRange, 
            const Dimension& roiDataDimension, void* roiDataPointer)->void;
    };
}
