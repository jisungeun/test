#pragma once
#include <memory>
#include "TCLdmReadingExport.h"
#include "ISamplerReader.h"
#pragma warning(push)
#pragma warning(disable : 4268)
#include "H5Cpp.h"
#pragma warning(pop)

namespace TC::IO::LdmReading {
    class TCLdmReading_API Hdf5DataSetHybridSamplerReader final : public ISamplerReader {
    public:
        Hdf5DataSetHybridSamplerReader(H5::DataSet& sourceDataSet, H5::Group& sampledDataGroup);
        ~Hdf5DataSetHybridSamplerReader();

        auto SetOffsetPoint(const Point& offset)->void;
        auto Read(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            ->MemoryChunk::Pointer override;
    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    private:
        auto ReadTileFromDataGroup(const std::string& tileDataSetName) const -> MemoryChunk::Pointer;
        auto ReadTileFromDataSet(const LdmCore::LdmTile::Pointer& targetLdmTile) const -> MemoryChunk::Pointer;
    };
}
