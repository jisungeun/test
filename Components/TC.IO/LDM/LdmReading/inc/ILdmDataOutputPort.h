#pragma once
#include "MemoryChunk.h"
#include "TCLdmReadingExport.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API ILdmDataOutputPort {
    public:
        typedef ILdmDataOutputPort Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ILdmDataOutputPort() = default;
        virtual auto SetMemoryChunk(const MemoryChunk::Pointer& memoryChunk)->void = 0;
    };
}
