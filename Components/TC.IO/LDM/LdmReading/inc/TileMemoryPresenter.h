#pragma once

#include "ILdmDataOutputPort.h"
#include "TCLdmReadingExport.h"
#include "TileMemory.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API TileMemoryPresenter final : public ILdmDataOutputPort{
    public:
        TileMemoryPresenter();
        ~TileMemoryPresenter();

        auto GetTileMemory() const ->TileMemory;
        auto SetMemoryChunk(const MemoryChunk::Pointer& memoryChunk) -> void override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto ConvertToTileMemoryType(const ChunkDataType& chunkDataType)->TileMemoryType;
    };
}
