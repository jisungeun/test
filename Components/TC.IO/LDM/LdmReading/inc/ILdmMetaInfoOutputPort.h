#pragma once
#include <memory>
#include "TCLdmReadingExport.h"
#include "LdmMetaInfo.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API ILdmMetaInfoOutputPort {
    public:
        typedef ILdmMetaInfoOutputPort Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ILdmMetaInfoOutputPort() = default;
        virtual auto SetLdmMetaInfo(const LdmCore::LdmMetaInfo& ldmMetaInfo)->void = 0;
    };
}
