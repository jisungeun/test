#pragma once
#include <memory>

#include "TCLdmReadingExport.h"
#include "MemoryChunk.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API ILdmDataReader {
    public:
        typedef ILdmDataReader Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ILdmDataReader() = default;
        virtual auto Read()-> MemoryChunk::Pointer = 0;
    };
}
