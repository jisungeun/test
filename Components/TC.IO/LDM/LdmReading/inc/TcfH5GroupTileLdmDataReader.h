#pragma once

#include <memory>

#include "TCLdmReadingExport.h"
#include "ILdmDataReader.h"
#pragma warning(push)
#pragma warning(disable : 4268)
#include "H5Cpp.h"
#pragma warning(pop)

namespace TC::IO::LdmReading {
    class TCLdmReading_API TcfH5GroupTileLdmDataReader : public ILdmDataReader{
    public:
        TcfH5GroupTileLdmDataReader();
        ~TcfH5GroupTileLdmDataReader();

        auto SetDataGroup(H5::Group& dataGroup)->void;
        auto SetReadingTileIndex(const int32_t& tileIndex)->void;

        auto Read() -> MemoryChunk::Pointer override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto GetTileName(const H5::Group& dataGroup, const int32_t& tileIndex)->std::string;
        static auto GetNumberOfTiles(const H5::Group& dataGroup)->uint32_t;
    };
}
