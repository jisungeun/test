#pragma once
#include <memory>
#include "TCLdmReadingExport.h"
#include "ILdmDataReader.h"
#pragma warning(push)
#pragma warning(disable : 4268)
#include "H5Cpp.h"
#pragma warning(pop)
#include "DataRange.h"
#include "LdmTile.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API Hdf5DataSet3DTileLdmDataReader final : public ILdmDataReader{
    public:
        Hdf5DataSet3DTileLdmDataReader();
        ~Hdf5DataSet3DTileLdmDataReader();

        auto SetTargetDataSet(H5::DataSet& dataSet)->void;
        auto SetTileUnitDimension(const Dimension& tileUnitDimension)->void;
        auto SetReadingTileIndex(const int32_t& tileIndex)->void;

        auto Read()-> MemoryChunk::Pointer override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto GetLdmTile(const Dimension& dataDimension, const Dimension& tileUnitDimension, 
            const int32_t& tileIndex)-> LdmCore::LdmTile::Pointer;
        static auto ReadTileData(const H5::DataSet& dataSet, const LdmCore::LdmTile::Pointer& ldmTile)
            ->MemoryChunk::Pointer;
        static auto GetSelectedH5Space(const H5::DataSet& dataSet, const DataRange& dataRange,
            const int32_t& samplingLevel)->H5::DataSpace;
    };
}
