#pragma once
#pragma warning(disable:4458)

#include <memory>
#pragma warning(push)
#pragma warning(disable : 4268)
#include "H5Cpp.h"
#pragma warning(pop)
#include "MemoryChunk.h"
#include "TCLdmReadingExport.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API TcfLdmThumbnailReader {
    public:
        TcfLdmThumbnailReader();
        ~TcfLdmThumbnailReader();

        auto SetLdmDataGroup(H5::Group& ldmDataGroup)->void;
        auto SetTargetImageSizes(const int32_t& sizeX, const int32_t& sizeY)->void;
        auto SetColorFlag(const bool& colorFlag)->void;

        auto Read()->MemoryChunk::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
