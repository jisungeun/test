#pragma once
#include <memory>
#include <any>
#include <enum.h>
#include "TCLdmReadingExport.h"

namespace TC::IO::LdmReading {
    BETTER_ENUM(TileMemoryType, uint32_t, INT8, UINT8, INT16, UINT16, INT32, UINT32, FLOAT, DOUBLE)
    class TCLdmReading_API TileMemory {
    public:
		typedef TileMemory Self;
		typedef std::shared_ptr<Self> Pointer;
	
        TileMemory(const std::any& data, const TileMemoryType& tileMemoryType,
            const uint32_t& dimensionX, const uint32_t& dimensionY, const uint32_t& dimensionZ = 1);
        TileMemory(const TileMemory& other);
        auto operator=(const TileMemory& other)->TileMemory&;
        ~TileMemory();

        auto GetTileMemoryType() const ->TileMemoryType;
        auto GetDimensionX() const ->uint32_t;
        auto GetDimensionY() const ->uint32_t;
        auto GetDimensionZ() const ->uint32_t;
        auto GetRawPointer() const ->void*;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
