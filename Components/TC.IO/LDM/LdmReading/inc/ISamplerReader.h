#pragma once
#include <memory>
#include "TCLdmReadingExport.h"

#include "LdmConfiguration.h"
#include "MemoryChunk.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API ISamplerReader {
    public:
        typedef ISamplerReader Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ISamplerReader() = default;
        virtual auto Read(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer = 0;
    };
}
