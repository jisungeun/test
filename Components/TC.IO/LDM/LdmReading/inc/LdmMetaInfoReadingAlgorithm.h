#pragma once

#include "TCLdmReadingExport.h"
#include "ILdmMetaInfoReader.h"
#include "ILdmMetaInfoOutputPort.h"

namespace TC::IO::LdmReading {
    class TCLdmReading_API LdmMetaInfoReadingAlgorithm {
    public:
        LdmMetaInfoReadingAlgorithm();
        ~LdmMetaInfoReadingAlgorithm();
        auto ReadLdmMetaInfo(ILdmMetaInfoReader::Pointer& ldmMetaInfoReader,
            ILdmMetaInfoOutputPort::Pointer& ldmMetaInfoOutputPort)->void;
    };
}
