#pragma once
#include "DataRange.h"
#include "TCLdmReadingExport.h"
#pragma warning(push)
#pragma warning(disable : 4268)
#include "H5Cpp.h"
#pragma warning(pop)

namespace TC::IO::LdmReading {
    auto TCLdmReading_API ReadSamplingStep(const H5::DataSet& tileDataSet)->uint32_t; //TODO : implement test
    auto TCLdmReading_API ReadTileDataRange(const H5::DataSet& tileDataSet)->DataRange; //TODO : implement test
    auto TCLdmReading_API ReadOffsetPoint(const H5::DataSet& tileDataSet)->Point; //TODO : implement test
    auto TCLdmReading_API ReadLastPoint(const H5::DataSet& tileDataSet)->Point; //TODO : implement test
}