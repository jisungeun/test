#include <catch2/catch.hpp>

#include "ISamplerReader.h"
using namespace TC::IO;

namespace ISamplerReaderTest {
    class DummySamplerReader final : public LdmReading::ISamplerReader {
    public:
        DummySamplerReader() = default;
        ~DummySamplerReader() = default;

        auto Read(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
            -> MemoryChunk::Pointer override {
            readingFlag = true;
            return MemoryChunk::Pointer{ nullptr };
        }

        auto GetReadingFlag()->bool {
            return readingFlag;
        }
    private:
        bool readingFlag{false};
    };

    TEST_CASE("ISamplerReader") {
        SECTION("Read()") {
            DummySamplerReader dummySamplerReader;
            CHECK(dummySamplerReader.GetReadingFlag() == false);
            CHECK(dummySamplerReader.Read(LdmCore::LdmConfiguration{}, 0) == nullptr);
            CHECK(dummySamplerReader.GetReadingFlag() == true);
        }
    }
}
