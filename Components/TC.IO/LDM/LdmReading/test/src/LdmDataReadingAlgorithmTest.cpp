#include <catch2/catch.hpp>

#include "LdmDataReadingAlgorithm.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace LdmDataReadingAlgorithmTest {
    class DummyLdmDataReader : public ILdmDataReader {
    public:
        DummyLdmDataReader() = default;
        ~DummyLdmDataReader() = default;

        auto Read() -> MemoryChunk::Pointer override {
            MemoryChunk::Pointer dummyMemoryChunk(new MemoryChunk{});
            return dummyMemoryChunk;
        }
    };

    class DummyLdmDataOutputPort : public ILdmDataOutputPort {
    public:
        DummyLdmDataOutputPort() = default;
        ~DummyLdmDataOutputPort() = default;

        auto SetMemoryChunk(const MemoryChunk::Pointer& memoryChunk) -> void override {
            memoryChunkIsSet = true;
        }

        auto IsMemoryChunkSet() const ->bool {
            return memoryChunkIsSet;
        }
    private:
        bool memoryChunkIsSet{ false };
    };

    TEST_CASE("LdmDataReadingAlgorithm") {
        SECTION("ReadData()") {
            auto dummyLdmDataReader = new DummyLdmDataReader;
            auto dummyLdmDataOutputPort = new DummyLdmDataOutputPort;

            ILdmDataReader::Pointer ldmDataReader(dummyLdmDataReader);
            ILdmDataOutputPort::Pointer ldmDataOutputPort(dummyLdmDataOutputPort);

            LdmDataReadingAlgorithm ldmDataReadingAlgorithm;
            ldmDataReadingAlgorithm.ReadData(ldmDataReader, ldmDataOutputPort);

            CHECK(dummyLdmDataOutputPort->IsMemoryChunkSet());
        }
    }
}
