#include <catch2/catch.hpp>

#include "ILdmDataOutputPort.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace ILdmDataOutputPortTest {
    class DummyLdmDataOutputPort : public ILdmDataOutputPort {
    public:
        DummyLdmDataOutputPort() = default;
        ~DummyLdmDataOutputPort() = default;
        auto SetMemoryChunk(const MemoryChunk::Pointer& memoryChunk) -> void override {
            memoryChunkIsSet = true;
        }
        auto IsMemoryChunkSet() const -> bool {
            return memoryChunkIsSet;
        }
    private:
        bool memoryChunkIsSet{ false };
    };

    TEST_CASE("ILdmDataOutputPort") {
        SECTION("SetMemoryChunk()") {
            DummyLdmDataOutputPort dummyLdmDataOutputPort;
            dummyLdmDataOutputPort.SetMemoryChunk(nullptr);

            CHECK(dummyLdmDataOutputPort.IsMemoryChunkSet() == true);
        }
    }
}