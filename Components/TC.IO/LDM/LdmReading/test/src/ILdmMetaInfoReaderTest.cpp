#include <catch2/catch.hpp>

#include "ILdmMetaInfoReader.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace ILdmMetaInfoReaderTest {
    class DummyLdmMetaInfoReader : public ILdmMetaInfoReader {
    public:
        DummyLdmMetaInfoReader() = default;
        ~DummyLdmMetaInfoReader() = default;

        auto Read() -> LdmCore::LdmMetaInfo override {
            LdmCore::LdmMetaInfo ldmMetaInfo;
            ldmMetaInfo.SetNumberOfTiles(1);
            return ldmMetaInfo;
        }
    };

    TEST_CASE("ILdmMetaInfoReader") {
        SECTION("Read()") {
            DummyLdmMetaInfoReader dummyLdmMetaInfoReader;
            const auto dummyLdmMetaInfo = dummyLdmMetaInfoReader.Read();
            CHECK(dummyLdmMetaInfo.GetNumberOfTiles() == 1);
        }
    }
}