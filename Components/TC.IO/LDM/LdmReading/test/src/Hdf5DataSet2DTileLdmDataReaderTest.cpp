#include <catch2/catch.hpp>

#include "Hdf5DataSet2DTileLdmDataReader.h"
#include "H5Cpp.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace Hdf5DataSet2DTileLdmDataReaderTest {
    const uint16_t data[8 * 8] =
    { 39,83,33,84,81,37,56,0,82,26,60,63,72,32,4,0,60,68,18,26,67,78,76,0,78,79,9,90,91,21,66,0,99,68,31,77,82,96,60,
        0,79,65,23,56,55,93,23,0,85,24,91,90,2,37,71,0,0,0,0,0,0,0,0,0 };

    const uint16_t cubic1[4 * 4] =
    { 39,33,81,56,60,18,67,76,99,31,82,60,85,91,2,71 };
    const uint16_t cubic2[4 * 4] =
    { 39,83,33,84,82,26,60,63,60,68,18,26,78,79,9,90 };
    const uint16_t cubic3[4 * 4] =
    { 82,96,60,0,55,93,23,0,2,37,71,0,0,0,0,0 };

    auto Compare(const uint16_t* data1, const uint16_t* data2, const size_t& numberOfElements) -> bool {
        bool isSame = true;
        for (size_t i = 0; i < numberOfElements; ++i) {
            if (data1[i] != data2[i]) {
                isSame = false;
                break;
            }
        }
        return isSame;
    }

    TEST_CASE("Hdf5DataSet2DTileLdmDataReader") {
        H5::H5File file("Hdf5DataSet2DTileLdmDataReader.h5", H5F_ACC_TRUNC);

        const auto rank = 2;
        const hsize_t dims[rank] = { 8,8 };
        auto dataSpace = H5::DataSpace(rank, dims);
        auto dataSet = file.createDataSet("data", H5::PredType::NATIVE_UINT16, dataSpace);
        dataSet.write(data, H5::PredType::NATIVE_UINT16);
        dataSpace.close();

        Hdf5DataSet2DTileLdmDataReader hdf5DataSet2DTileLdmDataReader;

        SECTION("cubic 1 : low resolution") {
            hdf5DataSet2DTileLdmDataReader.SetTileUnitDimension(Dimension(4, 4));
            hdf5DataSet2DTileLdmDataReader.SetReadingTileIndex(0);
            hdf5DataSet2DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet2DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cubic1, dimension.GetNumberOfElements()));
        }

        SECTION("cubic 2 : high resolution inbound") {
            hdf5DataSet2DTileLdmDataReader.SetTileUnitDimension(Dimension(4, 4));
            hdf5DataSet2DTileLdmDataReader.SetReadingTileIndex(1);
            hdf5DataSet2DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet2DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cubic2, dimension.GetNumberOfElements()));
        }

        SECTION("cubic 3 : high resolution outbound") {
            hdf5DataSet2DTileLdmDataReader.SetTileUnitDimension(Dimension(4, 4));
            hdf5DataSet2DTileLdmDataReader.SetReadingTileIndex(4);
            hdf5DataSet2DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet2DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cubic3, dimension.GetNumberOfElements()));
        }

        dataSet.close();
        file.close();
    }
}