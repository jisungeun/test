#include <catch2/catch.hpp>

#include "Hdf5DataSet3DTileLdmDataReader.h"
#include "H5Cpp.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace Hdf5DataSet3DTileLdmDataReaderTest {
    const uint16_t data[8 * 8 * 8] = 
    { 39,83,33,84,81,37,56,0,82,26,60,63,72,32,4,0,60,68,18,26,67,78,76,0,78,79,9,90,91,21,66,0,99,68,31,77,82,96,60,
        0,79,65,23,56,55,93,23,0,85,24,91,90,2,37,71,0,0,0,0,0,0,0,0,0,35,20,24,11,60,65,76,0,67,41,23,65,25,92,84,0,
        30,85,36,15,65,33,75,0,68,43,92,69,6,81,94,0,39,93,50,63,94,79,92,0,39,37,80,4,9,12,22,0,21,70,35,80,33,16,51,
        0,0,0,0,0,0,0,0,0,43,50,35,94,67,25,4,0,8,4,23,97,47,93,44,0,57,56,76,38,94,48,33,0,83,5,23,98,59,88,29,0,35,
        87,45,12,23,78,50,0,78,86,26,76,68,2,18,0,71,34,35,58,32,77,93,0,0,0,0,0,0,0,0,0,7,19,82,1,8,21,30,0,2,74,11,
        14,31,22,24,0,97,72,1,48,95,98,83,0,73,19,28,58,85,38,44,0,75,43,94,29,17,64,51,0,3,5,67,44,54,5,19,0,12,66,
        39,40,29,23,15,0,0,0,0,0,0,0,0,0,60,63,65,31,44,80,9,0,32,25,22,32,33,56,24,0,83,84,12,91,9,2,90,0,12,84,4,45,
        81,79,22,0,25,45,17,75,40,27,48,0,94,39,51,19,66,82,91,0,66,96,87,8,57,69,19,0,0,0,0,0,0,0,0,0,34,30,70,52,16,
        40,44,0,84,48,61,71,99,60,3,0,39,1,4,26,45,91,68,0,8,17,20,11,55,13,11,0,87,30,33,55,26,4,20,0,44,74,64,22,88,
        40,53,0,17,58,22,31,10,60,63,0,0,0,0,0,0,0,0,0,35,71,88,94,3,1,85,0,77,24,7,43,17,100,69,0,29,59,34,61,30,28,
        96,0,82,94,18,97,96,3,99,0,17,90,25,36,78,15,53,0,86,61,73,66,67,48,91,0,93,79,29,31,84,89,48,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0 };

    const uint16_t cubic1[4 * 4 * 4] = 
    { 39,33,81,56,60,18,67,76,99,31,82,60,85,91,2,71,43,35,67,4,57,76,94,33,35,45,23,50,71,35,32,93,60,65,44,9,83,12,
        9,90,25,17,40,48,66,87,57,19,35,88,3,85,29,34,30,96,17,25,78,53,93,29,84,48 };
    const uint16_t cubic2[4 * 4 * 4] =
    { 39,83,33,84,82,26,60,63,60,68,18,26,78,79,9,90,35,20,24,11,67,41,23,65,30,85,36,15,68,43,92,69,43,50,35,94,8,4,
        23,97,57,56,76,38,83,5,23,98,7,19,82,1,2,74,11,14,97,72,1,48,73,19,28,58 };
    const uint16_t cubic3[4 * 4 * 4] =
    { 40,27,48,0,66,82,91,0,57,69,19,0,0,0,0,0,26,4,20,0,88,40,53,0,10,60,63,0,0,0,0,0,78,15,53,0,67,48,91,0,84,89,48,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

    const uint16_t cuboid1[4 * 4 * 2] = { 39,81,0,0,99,82,0,0,0,0,0,0,0,0,0,0,60,44,0,0,25,40,0,0,0,0,0,0,0,0,0,0 };
    const uint16_t cuboid2[4 * 4 * 2] = 
    { 39,83,33,84,82,26,60,63,60,68,18,26,78,79,9,90,35,20,24,11,67,41,23,65,30,85,36,15,68,43,92,69 };
    const uint16_t cuboid3[4 * 4 * 2] = 
    { 78,15,53,0,67,48,91,0,84,89,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    auto Compare(const uint16_t* data1, const uint16_t* data2, const size_t& numberOfElements) -> bool {
        bool isSame = true;
        for (size_t i = 0; i < numberOfElements; ++i) {
            if (data1[i] != data2[i]) {
                isSame = false;
                break;
            }
        }
        return isSame;
    }

    TEST_CASE("Hdf5DataSet3DTileLdmDataReader") {
        H5::H5File file("Hdf5DataSet3DTileLdmDataReader.h5", H5F_ACC_TRUNC);

        const auto rank = 3;
        const hsize_t dims[rank] = { 8,8,8 };
        auto dataSpace = H5::DataSpace(rank, dims);
        auto dataSet = file.createDataSet("data", H5::PredType::NATIVE_UINT16, dataSpace);
        dataSet.write(data, H5::PredType::NATIVE_UINT16);
        dataSpace.close();

        Hdf5DataSet3DTileLdmDataReader hdf5DataSet3DTileLdmDataReader;

        SECTION("cubic 1 : low resolution") {
            hdf5DataSet3DTileLdmDataReader.SetTileUnitDimension(Dimension(4,4,4));
            hdf5DataSet3DTileLdmDataReader.SetReadingTileIndex(0);
            hdf5DataSet3DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet3DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4, 4));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cubic1, dimension.GetNumberOfElements()));
        }

        SECTION("cubic 2 : high resolution inbound") {
            hdf5DataSet3DTileLdmDataReader.SetTileUnitDimension(Dimension(4, 4, 4));
            hdf5DataSet3DTileLdmDataReader.SetReadingTileIndex(1);
            hdf5DataSet3DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet3DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4, 4));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cubic2, dimension.GetNumberOfElements()));
        }

        SECTION("cubic 3 : high resolution outbound") {
            hdf5DataSet3DTileLdmDataReader.SetTileUnitDimension(Dimension(4, 4, 4));
            hdf5DataSet3DTileLdmDataReader.SetReadingTileIndex(8);
            hdf5DataSet3DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet3DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4, 4));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cubic3, dimension.GetNumberOfElements()));
        }

        SECTION("cuboid 1 : low resolution") {
            hdf5DataSet3DTileLdmDataReader.SetTileUnitDimension(Dimension(4, 4, 2));
            hdf5DataSet3DTileLdmDataReader.SetReadingTileIndex(0);
            hdf5DataSet3DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet3DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4, 2));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cuboid1, dimension.GetNumberOfElements()));
        }

        SECTION("cuboid 2 : high resolution inbound") {
            hdf5DataSet3DTileLdmDataReader.SetTileUnitDimension(Dimension(4, 4, 2));
            hdf5DataSet3DTileLdmDataReader.SetReadingTileIndex(3);
            hdf5DataSet3DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet3DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4, 2));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cuboid2, dimension.GetNumberOfElements()));
        }

        SECTION("cuboid 3 : high resolution outbound") {
            hdf5DataSet3DTileLdmDataReader.SetTileUnitDimension(Dimension(4, 4, 2));
            hdf5DataSet3DTileLdmDataReader.SetReadingTileIndex(18);
            hdf5DataSet3DTileLdmDataReader.SetTargetDataSet(dataSet);
            auto dataMemoryChunk = hdf5DataSet3DTileLdmDataReader.Read();
            const auto dimension = dataMemoryChunk->GetDimension();
            const auto dimensionCheck = (dimension == Dimension(4, 4, 2));
            CHECK(dimensionCheck);

            const auto dataRead = std::any_cast<std::shared_ptr<uint16_t[]>>(dataMemoryChunk->GetData()).get();
            CHECK(Compare(dataRead, cuboid3, dimension.GetNumberOfElements()));
        }
        dataSet.close();
        file.close();
    }
}