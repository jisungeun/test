#include <catch2/catch.hpp>

#include "LdmMetaInfoReadingAlgorithm.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace LdmMetaInfoReadingAlgorithmTest {
    class DummyLdmMetaInfoReader : public ILdmMetaInfoReader {
    public:
        DummyLdmMetaInfoReader() = default;
        ~DummyLdmMetaInfoReader() = default;

        auto Read() -> LdmCore::LdmMetaInfo override {
            LdmCore::LdmMetaInfo dummyLdmMetaInfo{};
            return dummyLdmMetaInfo;
        }
    };

    class DummyLdmMetaInfoOutputPort : public ILdmMetaInfoOutputPort {
    public:
        DummyLdmMetaInfoOutputPort() = default;
        ~DummyLdmMetaInfoOutputPort() = default;

        auto SetLdmMetaInfo(const LdmCore::LdmMetaInfo& ldmMetaInfo) -> void override {
            ldmMetaInfoIsSet = true;
        }

        auto IsLdmMetaInfoSet() const ->bool {
            return ldmMetaInfoIsSet;
        }
    private:
        bool ldmMetaInfoIsSet{ false };
    };

    TEST_CASE("LdmMetaInfoReadingAlgorithm") {
        SECTION("ReadLdmMetaInfo()") {
            auto dummyLdmMetaInfoReader = new DummyLdmMetaInfoReader;
            auto dummyLdmMetaInfoOutputPort = new DummyLdmMetaInfoOutputPort;

            ILdmMetaInfoReader::Pointer ldmMetaInfoReader(dummyLdmMetaInfoReader);
            ILdmMetaInfoOutputPort::Pointer ldmMetaInfoOutputPort(dummyLdmMetaInfoOutputPort);

            LdmMetaInfoReadingAlgorithm ldmMetaInfoReadingAlgorithm;
            ldmMetaInfoReadingAlgorithm.ReadLdmMetaInfo(ldmMetaInfoReader, ldmMetaInfoOutputPort);

            CHECK(dummyLdmMetaInfoOutputPort->IsLdmMetaInfoSet());
        }
    }
}
