#include <catch2/catch.hpp>

#include "ILdmDataReader.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace ILdmDataReaderTest {
    class DummyLdmDataReader : public ILdmDataReader {
    public:
        DummyLdmDataReader() = default;
        ~DummyLdmDataReader() = default;

        auto Read()-> MemoryChunk::Pointer override {
            MemoryChunk::Pointer dummyMemoryChunk(new MemoryChunk);
            std::shared_ptr<uint8_t[]> data(new uint8_t[1]());
            dummyMemoryChunk->SetData(data, Dimension(1, 2, 3));
            return dummyMemoryChunk;
        }
    };

    TEST_CASE("ILdmDataReader") {
        SECTION("Read()") {
            DummyLdmDataReader dummyLdmDataReader;
            const auto dummyMemoryChunk = dummyLdmDataReader.Read();
            const auto dimensionSame = (dummyMemoryChunk->GetDimension() == Dimension(1, 2, 3));
            CHECK(dimensionSame);
        }
    }

}