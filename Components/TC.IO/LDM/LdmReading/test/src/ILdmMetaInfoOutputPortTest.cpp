#include <catch2/catch.hpp>

#include "ILdmMetaInfoOutputPort.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace ILdmMetaInfoOutputPortTest {
    class DummyLdmMetaInfoOutputPort : public ILdmMetaInfoOutputPort {
    public:
        DummyLdmMetaInfoOutputPort() = default;
        ~DummyLdmMetaInfoOutputPort() = default;
        auto SetLdmMetaInfo(const LdmCore::LdmMetaInfo& ldmMetaInfo) -> void override {
            ldmMetaInfoIsSet = true;
        }
        auto IsLdmMetaInfoSet() const ->bool {
            return ldmMetaInfoIsSet;
        }
    private:
        bool ldmMetaInfoIsSet{ false };
    };


    TEST_CASE("ILdmMetaInfoOutputPort") {
        SECTION("SetLdmMetaInfo()") {
            DummyLdmMetaInfoOutputPort dummyLdmMetaInfoOutputPort;
            dummyLdmMetaInfoOutputPort.SetLdmMetaInfo(LdmCore::LdmMetaInfo{});

            CHECK(dummyLdmMetaInfoOutputPort.IsLdmMetaInfoSet());
        }

    }
}