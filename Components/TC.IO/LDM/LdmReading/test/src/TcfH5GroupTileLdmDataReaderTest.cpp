#include <catch2/catch.hpp>

#include "CommonUtilities.h"
#include "TcfH5GroupTileLdmDataReader.h"
#include "Hdf5DataSetTileWriter.h"
#include "Octree3DConfigurator.h"
#include "MemoryOnePickSampler.h"

#include "H5Cpp.h"
using namespace TC::IO;
using namespace LdmReading;
using namespace LdmCore;

namespace TcfH5GroupTileLdmDataReaderTest {
    const uint16_t data[8 * 8 * 8] =
    { 39, 83, 33, 84, 81, 37, 56, 71, 82, 26, 60, 63, 72, 32, 4, 79, 60, 68, 18, 26, 67, 78, 76, 33, 78, 79, 9, 90,
    91, 21, 66, 91, 99, 68, 31, 77, 82, 96, 60, 58, 79, 65, 23, 56, 55, 93, 23, 29, 85, 24, 91, 90, 2, 37, 71, 27,
    49, 48, 94, 37, 9, 34, 72, 89, 35, 20, 24, 11, 60, 65, 76, 49, 67, 41, 23, 65, 25, 92, 84, 23, 30, 85, 36, 15,
    65, 33, 75, 34, 68, 43, 92, 69, 6, 81, 94, 55, 39, 93, 50, 63, 94, 79, 92, 74, 39, 37, 80, 4, 9, 12, 22, 41, 21,
    70, 35, 80, 33, 16, 51, 51, 72, 59, 46, 99, 65, 41, 79, 13, 43, 50, 35, 94, 67, 25, 4, 100, 8, 4, 23, 97, 47, 93,
    44, 31, 57, 56, 76, 38, 94, 48, 33, 81, 83, 5, 23, 98, 59, 88, 29, 46, 35, 87, 45, 12, 23, 78, 50, 59, 78, 86,
    26, 76, 68, 2, 18, 48, 71, 34, 35, 58, 32, 77, 93, 55, 59, 97, 90, 62, 75, 69, 94, 18, 7, 19, 82, 1, 8, 21, 30,
    84, 2, 74, 11, 14, 31, 22, 24, 68, 97, 72, 1, 48, 95, 98, 83, 17, 73, 19, 28, 58, 85, 38, 44, 24, 75, 43, 94, 29,
    17, 64, 51, 43, 3, 5, 67, 44, 54, 5, 19, 3, 12, 66, 39, 40, 29, 23, 15, 27, 26, 78, 86, 14, 90, 87, 91, 98, 60,
    63, 65, 31, 44, 80, 9, 80, 32, 25, 22, 32, 33, 56, 24, 93, 83, 84, 12, 91, 9, 2, 90, 47, 12, 84, 4, 45, 81, 79,
    22, 31, 25, 45, 17, 75, 40, 27, 48, 44, 94, 39, 51, 19, 66, 82, 91, 31, 66, 96, 87, 8, 57, 69, 19, 28, 75, 1, 77,
    21, 0, 60, 45, 14, 34, 30, 70, 52, 16, 40, 44, 25, 84, 48, 61, 71, 99, 60, 3, 73, 39, 1, 4, 26, 45, 91, 68, 34,
    8, 17, 20, 11, 55, 13, 11, 8, 87, 30, 33, 55, 26, 4, 20, 30, 44, 74, 64, 22, 88, 40, 53, 30, 17, 58, 22, 31, 10,
    60, 63, 2, 0, 70, 8, 97, 82, 92, 37, 18, 35, 71, 88, 94, 3, 1, 85, 60, 77, 24, 7, 43, 17, 100, 69, 2, 29, 59, 34,
    61, 30, 28, 96, 39, 82, 94, 18, 97, 96, 3, 99, 6, 17, 90, 25, 36, 78, 15, 53, 69, 86, 61, 73, 66, 67, 48, 91, 52,
    93, 79, 29, 31, 84, 89, 48, 8, 19, 84, 24, 89, 42, 28, 89, 41, 76, 69, 34, 22, 98, 97, 80, 85, 100, 60, 43, 91,
    29, 42, 5, 49, 27, 51, 79, 37, 59, 5, 40, 95, 79, 78, 14, 8, 57, 79, 97, 80, 13, 69, 61, 3, 62, 63, 41, 13, 4,
    32, 93, 63, 56, 51, 33, 24, 45, 83, 64, 94, 28, 57, 64, 13, 56, 66, 5, 32, 15, 31, 86, 53 };

    uint16_t ans0[4 * 4 * 4] =
    { 39, 33, 81, 56, 60, 18, 67, 76, 99, 31, 82, 60, 85, 91, 2, 71, 43, 35, 67, 4, 57, 76, 94, 33, 35, 45, 23, 50,
    71, 35, 32, 93, 60, 65, 44, 9, 83, 12, 9, 90, 25, 17, 40, 48, 66, 87, 57, 19, 35, 88, 3, 85, 29, 34, 30, 96, 17,
    25, 78, 53, 93, 29, 84, 48 };

    uint16_t ans1[4 * 4 * 4] =
    { 39, 83, 33, 84, 82, 26, 60, 63, 60, 68, 18, 26, 78, 79, 9, 90, 35, 20, 24, 11, 67, 41, 23, 65, 30, 85, 36, 15,
    68, 43, 92, 69, 43, 50, 35, 94, 8, 4, 23, 97, 57, 56, 76, 38, 83, 5, 23, 98, 7, 19, 82, 1, 2, 74, 11, 14, 97, 72,
    1, 48, 73, 19, 28, 58 };

    auto CompareData(uint16_t* data1, uint16_t* data2, const size_t& numberOfElements)->bool {
        bool dataSame{ true };
        for(size_t i = 0; i<numberOfElements;++i) {
            if(data1[i]!= data2[i]) {
                dataSame = false;
                break;
            }
        }
        return dataSame;
    }

    auto GenerateSampleFile(H5::H5File& file) -> void{
        Octree3DConfigurator octree3DConfigurator(Dimension(8, 8, 8), Dimension(4, 4, 4));
        const auto ldmConfiguration = octree3DConfigurator.Configure();

        const auto inputData = new uint16_t[8 * 8 * 8]();
        std::copy(data, data + 8 * 8 * 8, inputData);
        auto memoryChunk = GenerateMemoryChunk(inputData, Dimension(8, 8, 8), ChunkDataType::UInt16Type);

        auto group = file.createGroup("group");

        auto numberOfTileAttribute = group.createAttribute("NumberOfTiles", H5::PredType::NATIVE_INT64, H5S_SCALAR);
        int64_t numberOfTiles = 9;
        numberOfTileAttribute.write(H5::PredType::NATIVE_INT64, &numberOfTiles);
        numberOfTileAttribute.close();

        LdmWriting::Hdf5DataSetTileWriter hdf5DataSetTileWriter(group);
        LdmWriting::MemoryOnePickSampler memoryOnePickSampler;
        memoryOnePickSampler.SetSourceData(memoryChunk);

        const auto sample0Data = memoryOnePickSampler.Sample(ldmConfiguration, 0);
        hdf5DataSetTileWriter.WriteTile(ldmConfiguration, 0, sample0Data);

        const auto sample1Data = memoryOnePickSampler.Sample(ldmConfiguration, 1);
        hdf5DataSetTileWriter.WriteTile(ldmConfiguration, 1, sample1Data);

        group.close();
    }

    TEST_CASE("TcfH5GroupTileLdmDataReader") {
        TcfH5GroupTileLdmDataReader TcfH5GroupTileLdmDataReader;
        H5::H5File file("TcfH5GroupTileLdmDataReader.h5", H5F_ACC_TRUNC);
        GenerateSampleFile(file);
        auto group = file.openGroup("group");

        TcfH5GroupTileLdmDataReader.SetDataGroup(group);

        TcfH5GroupTileLdmDataReader.SetReadingTileIndex(0);
        auto memoryChunk0 = TcfH5GroupTileLdmDataReader.Read();
        auto result0 = memoryChunk0->GetDataPointerAs<uint16_t>();
        CHECK(CompareData(result0, ans0, 4 * 4 * 4));

        TcfH5GroupTileLdmDataReader.SetReadingTileIndex(1);
        auto memoryChunk1 = TcfH5GroupTileLdmDataReader.Read();
        auto result1 = memoryChunk1->GetDataPointerAs<uint16_t>();
        CHECK(CompareData(result1, ans1, 4 * 4 * 4));

        group.close();
        file.close();
    }
}
