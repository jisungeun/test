#include <catch2/catch.hpp>
#include <iostream>

#include "Hdf5Utilities.h"
#include "TcfLdmThumbnailReader.h"
#include <QImage>

using namespace TC::IO::LdmReading;

namespace TcfLdmThumbnailReaderTest {
    // https://app.box.com/folder/140251616428?s=rgoadaoad9abmj5b73e76jep80r480jf
    // Box -> 15 SW Test Data -> 20210630_TcfLdmThumbnailReaderTest
    const std::string data2DFilePath = "TcfLdmThumbnailReader2D.h5";
    const std::string data2DStackFilePath = "TcfLdmThumbnailReader2DStack.h5";

    const std::string answer2DFilePath = "answer2D.h5";
    const std::string answer2DStackFilePath = "answer2DStack.h5";

    const std::string answer1Name = "ans1";
    const std::string answer2Name = "ans2";
    const std::string answer3Name = "ans3";
    const std::string answer4Name = "ans4";

    auto CompareArray(const uint16_t* array1, const uint16_t* array2, const size_t& arraySize) ->bool{
        for (size_t arrayIndex = 0; arrayIndex < arraySize; ++arrayIndex) {
            const auto value1 = array1[arrayIndex];
            const auto value2 = array2[arrayIndex];

            if (value1 != value2) {
                return false;
            }
        }

        return true;
    }

    auto ReadAnswer2D(const std::string& answerName)->TC::IO::MemoryChunk::Pointer {
        H5::H5File file(answer2DFilePath, H5F_ACC_RDONLY);

        auto dataSet = file.openDataSet(answerName);
        auto answerMemory = TC::HDF5Utilities::ReadDataSet(dataSet);
        dataSet.close();

        file.close();

        return answerMemory;
    }

    auto ReadAnswer2DStack(const std::string& answerName)->TC::IO::MemoryChunk::Pointer {
        H5::H5File file(answer2DStackFilePath, H5F_ACC_RDONLY);

        auto dataSet = file.openDataSet(answerName);
        auto answerMemory = TC::HDF5Utilities::ReadDataSet(dataSet);
        dataSet.close();

        file.close();

        return answerMemory;
    }

    TEST_CASE("TcfLdmThumbnailReader") {
        //SECTION("2D") {
        //    // https://tomocube.box.com/s/rgoadaoad9abmj5b73e76jep80r480jf
        //    // Box -> 15 SW Test Data -> 20210630_TcfLdmThumbnailReaderTest
        //    H5::H5File file(data2DFilePath, H5F_ACC_RDONLY);
        //    auto ldmDataGroup = file.openGroup("/Data/000000");

        //    TcfLdmThumbnailReader tcfLdmThumbnailReader;
        //    tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);

        //    SECTION("Target Image Size x600 y600") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(600, 600);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2D(answer1Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(500, 500));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x500 y500") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(500, 500);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2D(answer1Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(500, 500));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x400 y400") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(400, 400);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2D(answer1Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(500, 500));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x300 y300") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(300, 300);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2D(answer1Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(500, 500));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x200 y200") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(200, 200);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2D(answer2Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(250, 250));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x100 y100") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(100, 100);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2D(answer3Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(125, 125));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x50 y50") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(50, 50);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2D(answer4Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(63, 63));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }


        //    ldmDataGroup.close();
        //    file.close();
        //}
        //SECTION("2DStack (BF)") {
        //    // https://tomocube.box.com/s/rgoadaoad9abmj5b73e76jep80r480jf
        //    // Box -> 15 SW Test Data -> 20210630_TcfLdmThumbnailReaderTest
        //    H5::H5File file(data2DStackFilePath, H5F_ACC_RDONLY);
        //    auto ldmDataGroup = file.openGroup("/Data/000000");

        //    TcfLdmThumbnailReader tcfLdmThumbnailReader;
        //    tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
        //    tcfLdmThumbnailReader.SetColorFlag(true);

        //    SECTION("Target Image Size x600 y600") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(600, 600);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2DStack(answer1Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(500, 500, 3));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x500 y500") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(500, 500);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2DStack(answer1Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(500, 500, 3));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x400 y400") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(400, 400);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2DStack(answer1Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(500, 500, 3));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x300 y300") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(300, 300);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2DStack(answer1Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(500, 500, 3));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x200 y200") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(200, 200);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2DStack(answer2Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(250, 250, 3));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x100 y100") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(100, 100);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2DStack(answer3Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(125, 125, 3));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    SECTION("Target Image Size x50 y50") {
        //        tcfLdmThumbnailReader.SetTargetImageSizes(50, 50);
        //        auto resultData = tcfLdmThumbnailReader.Read();
        //        auto answerData = ReadAnswer2DStack(answer4Name);
        //        CHECK(resultData->GetDimension() == TC::IO::Dimension(63, 63, 3));

        //        auto resultArray = resultData->GetDataPointerAs<uint16_t>();
        //        auto answerArray = answerData->GetDataPointerAs<uint16_t>();
        //        const auto arraySize = resultData->GetDimension().GetNumberOfElements();

        //        CHECK(CompareArray(resultArray, answerArray, arraySize));
        //    }

        //    ldmDataGroup.close();
        //    file.close();
        //}

        SECTION("Practical Test") {
            SECTION("2DMIP") {
                const std::string ldmTcfFilePath = "E:/CellLibrary2/20210625_Hetero timelapse/20210607.172355.230.All Modality DoublePosition Heterotimelapse 3x3-001P00Stitching/20210607.172355.230.All Modality DoublePosition Heterotimelapse 3x3-001P00Stitching_LDM.TCF";

                H5::H5File file(ldmTcfFilePath, H5F_ACC_RDONLY);
                auto ldmDataGroup = file.openGroup("/Data/2DMIP/000000");

                const auto targetImageSizeX = 1000;
                const auto targetImageSizeY = 1000;

                TcfLdmThumbnailReader tcfLdmThumbnailReader;
                tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
                tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);

                const auto memoryChunk = tcfLdmThumbnailReader.Read();

                const auto imageDimension = memoryChunk->GetDimension();
                const auto imageSizeX = static_cast<int32_t>(imageDimension.X());
                const auto imageSizeY = static_cast<int32_t>(imageDimension.Y());

                const auto numberOfElements = imageDimension.GetNumberOfElements();

                const float minValue = 13370;
                const float maxValue = 14282;

                auto imageRawData = new uint8_t[numberOfElements];

                for (uint32_t i = 0; i < static_cast<uint32_t>(numberOfElements); ++i) {
                    imageRawData[i] = static_cast<uint8_t>((static_cast<float>(memoryChunk->GetDataPointerAs<uint16_t>()[i]) - minValue) / (maxValue - minValue) * 255);
                }

                const auto bytePerLine = imageSizeX;

                QImage qImage(imageRawData, imageSizeX, imageSizeY, bytePerLine, QImage::Format_Grayscale8);
                const auto saveResult = qImage.save("C:/Temp/Test.png");
                CHECK(saveResult);

                delete[] imageRawData;
                ldmDataGroup.close();
                file.close();
            }
            SECTION("BF") {
                const std::string ldmTcfFilePath = "E:/CellLibrary2/20210625_Hetero timelapse/20210607.172355.230.All Modality DoublePosition Heterotimelapse 3x3-001P00Stitching/20210607.172355.230.All Modality DoublePosition Heterotimelapse 3x3-001P00Stitching_LDM.TCF";

                H5::H5File file(ldmTcfFilePath, H5F_ACC_RDONLY);
                auto ldmDataGroup = file.openGroup("/Data/BF/000000");

                const auto targetImageSizeX = 1024;
                const auto targetImageSizeY = 1024;

                TcfLdmThumbnailReader tcfLdmThumbnailReader;
                tcfLdmThumbnailReader.SetLdmDataGroup(ldmDataGroup);
                tcfLdmThumbnailReader.SetTargetImageSizes(targetImageSizeX, targetImageSizeY);
                tcfLdmThumbnailReader.SetColorFlag(true);

                const auto memoryChunk = tcfLdmThumbnailReader.Read();

                const auto imageDimension = memoryChunk->GetDimension();
                const auto imageSizeX = static_cast<int32_t>(imageDimension.X());
                const auto imageSizeY = static_cast<int32_t>(imageDimension.Y());

                auto imageRawData = memoryChunk->GetDataPointerAs<uint8_t>();

                // reorder rrggbb -> rgb rgb
                {
                    const auto pixelLength = imageSizeX * imageSizeY;
                    const auto dataLength = static_cast<uint32_t>(3 * pixelLength);

                    std::shared_ptr<uint8_t> dataBuffer(new uint8_t[dataLength]());
                    memcpy(dataBuffer.get(), imageRawData, sizeof(uint8_t) * dataLength);

                    for (uint32_t i = 0; i < dataLength; i += 3) {
                        const auto pixelNumber = i / 3;
                        imageRawData[i] = dataBuffer.get()[pixelNumber];
                        imageRawData[i + 1] = dataBuffer.get()[pixelLength + pixelNumber];
                        imageRawData[i + 2] = dataBuffer.get()[2 * pixelLength + pixelNumber];
                    }
                }

                const auto bytePerLine = imageSizeX;
                QImage qImage(imageRawData, imageSizeX, imageSizeY, bytePerLine*3, QImage::Format_RGB888);
                const auto saveResult = qImage.save("C:/Temp/TestRBG.png");
                CHECK(saveResult);

                ldmDataGroup.close();
                file.close();
            }
        }
    }
}
