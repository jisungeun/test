#include <catch2/catch.hpp>

#include "TileMemoryPresenter.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace TileMemoryPresenterTest {
    TEST_CASE("TileMemoryPresenter") {
        SECTION("TileMemoryPresenter()") {
            TileMemoryPresenter tileMemoryPresenter;

            const auto tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetDimensionX() == 0);
            CHECK(tileMemory.GetDimensionY() == 0);
            CHECK(tileMemory.GetDimensionZ() == 0);
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::UINT8);
        }

        SECTION("SetMemoryChunk() / GetTileMemory()") {
            MemoryChunk::Pointer memoryChunk(new MemoryChunk);
            void* data = new int8_t[1];
            memoryChunk->SetData(std::shared_ptr<int8_t[]>(static_cast<int8_t*>(data)), Dimension(1, 2, 3));

            TileMemoryPresenter tileMemoryPresenter;
            tileMemoryPresenter.SetMemoryChunk(memoryChunk);

            auto tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetDimensionX() == 1);
            CHECK(tileMemory.GetDimensionY() == 2);
            CHECK(tileMemory.GetDimensionZ() == 3);
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::INT8);
            CHECK(data == tileMemory.GetRawPointer());

            data = new uint8_t[1];
            memoryChunk->SetData(std::shared_ptr<uint8_t[]>(static_cast<uint8_t*>(data)), Dimension(1, 2, 3));
            tileMemoryPresenter.SetMemoryChunk(memoryChunk);
            tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::UINT8);

            data = new int16_t[1];
            memoryChunk->SetData(std::shared_ptr<int16_t[]>(static_cast<int16_t*>(data)), Dimension(1, 2, 3));
            tileMemoryPresenter.SetMemoryChunk(memoryChunk);
            tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::INT16);

            data = new uint16_t[1];
            memoryChunk->SetData(std::shared_ptr<uint16_t[]>(static_cast<uint16_t*>(data)), Dimension(1, 2, 3));
            tileMemoryPresenter.SetMemoryChunk(memoryChunk);
            tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::UINT16);

            data = new int32_t[1];
            memoryChunk->SetData(std::shared_ptr<int32_t[]>(static_cast<int32_t*>(data)), Dimension(1, 2, 3));
            tileMemoryPresenter.SetMemoryChunk(memoryChunk);
            tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::INT32);

            data = new uint32_t[1];
            memoryChunk->SetData(std::shared_ptr<uint32_t[]>(static_cast<uint32_t*>(data)), Dimension(1, 2, 3));
            tileMemoryPresenter.SetMemoryChunk(memoryChunk);
            tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::UINT32);

            data = new float[1];
            memoryChunk->SetData(std::shared_ptr<float[]>(static_cast<float*>(data)), Dimension(1, 2, 3));
            tileMemoryPresenter.SetMemoryChunk(memoryChunk);
            tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::FLOAT);

            data = new double[1];
            memoryChunk->SetData(std::shared_ptr<double[]>(static_cast<double*>(data)), Dimension(1, 2, 3));
            tileMemoryPresenter.SetMemoryChunk(memoryChunk);
            tileMemory = tileMemoryPresenter.GetTileMemory();
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::DOUBLE);
        }
    }

}
