#include <catch2/catch.hpp>
#include <iostream>

#include "TcfH5GroupRoiLdmDataReader.h"
#include "TcfLdmConverter.h"
#include <string>
#include "H5Cpp.h"
#include "Hdf5Utilities.h"
using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace TcfH5GroupRoiLdmDataReaderTest {
    const uint16_t data[8 * 8 * 8] =
    { 39,83,33,84,81,37,56,71,82,26,60,63,72,32,4,79,60,68,18,26,67,78,76,33,78,79,9,90,91,21,66,91,99,68,31,77,82,96,
        60,58,79,65,23,56,55,93,23,29,85,24,91,90,2,37,71,27,49,48,94,37,9,34,72,89,35,20,24,11,60,65,76,49,67,41,23,
        65,25,92,84,23,30,85,36,15,65,33,75,34,68,43,92,69,6,81,94,55,39,93,50,63,94,79,92,74,39,37,80,4,9,12,22,41,
        21,70,35,80,33,16,51,51,72,59,46,99,65,41,79,13,43,50,35,94,67,25,4,100,8,4,23,97,47,93,44,31,57,56,76,38,94,
        48,33,81,83,5,23,98,59,88,29,46,35,87,45,12,23,78,50,59,78,86,26,76,68,2,18,48,71,34,35,58,32,77,93,55,59,97,
        90,62,75,69,94,18,7,19,82,1,8,21,30,84,2,74,11,14,31,22,24,68,97,72,1,48,95,98,83,17,73,19,28,58,85,38,44,24,
        75,43,94,29,17,64,51,43,3,5,67,44,54,5,19,3,12,66,39,40,29,23,15,27,26,78,86,14,90,87,91,98,60,63,65,31,44,80
        ,9,80,32,25,22,32,33,56,24,93,83,84,12,91,9,2,90,47,12,84,4,45,81,79,22,31,25,45,17,75,40,27,48,44,94,39,51,
        19,66,82,91,31,66,96,87,8,57,69,19,28,75,1,77,21,0,60,45,14,34,30,70,52,16,40,44,25,84,48,61,71,99,60,3,73,39,
        1,4,26,45,91,68,34,8,17,20,11,55,13,11,8,87,30,33,55,26,4,20,30,44,74,64,22,88,40,53,30,17,58,22,31,10,60,63,
        2,0,70,8,97,82,92,37,18,35,71,88,94,3,1,85,60,77,24,7,43,17,100,69,2,29,59,34,61,30,28,96,39,82,94,18,97,96,3,
        99,6,17,90,25,36,78,15,53,69,86,61,73,66,67,48,91,52,93,79,29,31,84,89,48,8,19,84,24,89,42,28,89,41,76,69,34,
        22,98,97,80,85,100,60,43,91,29,42,5,49,27,51,79,37,59,5,40,95,79,78,14,8,57,79,97,80,13,69,61,3,62,63,41,13,4,
        32,93,63,56,51,33,24,45,83,64,94,28,57,64,13,56,66,5,32,15,31,86,53 };

    const DataRange ans1DataRange(Point(0, 0, 0), Count(8, 8, 1));
    uint16_t ans1Data[8 * 8 * 1] =
    { 39, 83, 33, 84, 81, 37, 56, 71, 82, 26, 60, 63, 72, 32, 4, 79, 60, 68, 18, 26, 67, 78, 76, 33, 78, 79, 9, 90,
        91, 21, 66, 91, 99, 68, 31, 77, 82, 96, 60, 58, 79, 65, 23, 56, 55, 93, 23, 29, 85, 24, 91, 90, 2, 37, 71, 27,
        49, 48, 94, 37, 9, 34, 72, 89 };

    const DataRange ans2DataRange(Point(0, 0, 0), Count(1, 8, 8));
    uint16_t ans2Data[1 * 8 * 8] =
    { 39, 82, 60, 78, 99, 79, 85, 49, 35, 67, 30, 68, 39, 39, 21, 72, 43, 8, 57, 83, 35, 78, 71, 59, 7, 2, 97, 73, 75,
        3, 12, 26, 60, 32, 83, 12, 25, 94, 66, 75, 34, 84, 39, 8, 87, 44, 17, 0, 35, 77, 29, 82, 17, 86, 93, 19, 76,
        100, 27, 79, 13, 4, 45, 56 };

    const DataRange ans3DataRange(Point(0, 0, 0), Count(8, 1, 8));
    uint16_t ans3Data[8 * 1 * 8] =
    { 39, 83, 33, 84, 81, 37, 56, 71, 35, 20, 24, 11, 60, 65, 76, 49, 43, 50, 35, 94, 67, 25, 4, 100, 7, 19, 82, 1,
        8, 21, 30, 84, 60, 63, 65, 31, 44, 80, 9, 80, 34, 30, 70, 52, 16, 40, 44, 25, 35, 71, 88, 94, 3, 1, 85, 60,
        76, 69, 34, 22, 98, 97, 80, 85 };

    const DataRange ans4DataRange(Point(0, 0, 0), Count(4, 4, 4));
    uint16_t ans4Data[4 * 4 * 4] =
    { 39, 83, 33, 84, 82, 26, 60, 63, 60, 68, 18, 26, 78, 79, 9, 90, 35, 20, 24, 11, 67, 41, 23, 65, 30, 85, 36, 15,
        68, 43, 92, 69, 43, 50, 35, 94, 8, 4, 23, 97, 57, 56, 76, 38, 83, 5, 23, 98, 7, 19, 82, 1, 2, 74, 11, 14, 97,
        72, 1, 48, 73, 19, 28, 58 };

    const DataRange ans5DataRange(Point(0, 0, 0), Count(5, 5, 5));
    uint16_t ans5Data[5 * 5 * 5] =
    { 39, 83, 33, 84, 81, 82, 26, 60, 63, 72, 60, 68, 18, 26, 67, 78, 79, 9, 90, 91, 99, 68, 31, 77, 82, 35, 20, 24,
        11, 60, 67, 41, 23, 65, 25, 30, 85, 36, 15, 65, 68, 43, 92, 69, 6, 39, 93, 50, 63, 94, 43, 50, 35, 94, 67, 8,
        4, 23, 97, 47, 57, 56, 76, 38, 94, 83, 5, 23, 98, 59, 35, 87, 45, 12, 23, 7, 19, 82, 1, 8, 2, 74, 11, 14, 31,
        97, 72, 1, 48, 95, 73, 19, 28, 58, 85, 75, 43, 94, 29, 17, 60, 63, 65, 31, 44, 32, 25, 22, 32, 33, 83, 84, 12,
        91, 9, 12, 84, 4, 45, 81, 25, 45, 17, 75, 40 };

    const DataRange ans6DataRange(Point(4, 4, 4), Count(1, 2, 3));
    uint16_t ans6Data[1 * 2 * 3] =
    { 40, 66, 26, 88, 78, 67 };

    const DataRange ans7DataRange(Point(0, 0, 0), Count(8, 8, 1));
    uint16_t ans7Data[4 * 4 * 1] =
    { 39, 33, 81, 56, 60, 18, 67, 76, 99, 31, 82, 60, 85, 91, 2, 71 };

    const DataRange ans8DataRange(Point(0, 0, 0), Count(7, 7, 1));
    uint16_t ans8Data[4 * 4 * 1] =
    { 39, 33, 81, 56, 60, 18, 67, 76, 99, 31, 82, 60, 85, 91, 2, 71 };

    const DataRange ans9DataRange(Point(1, 1, 0), Count(7, 7, 1));
    uint16_t ans9Data[3 * 3 * 1] =
    { 18, 67, 76, 31, 82, 60, 91, 2, 71 };

    const DataRange ans10DataRange(Point(0, 0, 0), Count(1, 8, 8));
    uint16_t ans10Data[1 * 8 * 8] =
    { 39, 60, 99, 85, 43, 57, 35, 71, 60, 83, 25, 66, 35, 29, 17, 93 };

    const uint16_t data2d[8 * 8] =
    { 39,83,33,84,81,37,56,71,82,26,60,63,72,32,4,79,60,68,18,26,67,78,76,33,78,79,9,90,91,21,66,91,99,68,31,77,82,96,
        60,58,79,65,23,56,55,93,23,29,85,24,91,90,2,37,71,27,49,48,94,37,9,34,72,89 };

    const DataRange ans2DDataRange(Point(1, 2), Count(5, 5));
    uint16_t ans2DData[5 * 5] =
    { 68,18,26,67,78,79,9,90,91,21,68,31,77,82,96,65,23,56,55,93,24,91,90,2,37 };

    const DataRange ans2DDataRange2(Point(0, 0), Count(8, 8));
    uint16_t ans2DData2[4 * 4] =
    { 39, 33, 81, 56, 60, 18, 67, 76, 99, 31, 82, 60, 85, 91, 2, 71 };

    const uint8_t data2dStack[8 * 8 * 3] =
    { 39,83,33,84,81,37,56,71,82,26,60,63,72,32,4,79,60,68,18,26,67,78,76,33,78,79,9,90,91,21,66,91,99,68,31,77,82,96,
        60,58,79,65,23,56,55,93,23,29,85,24,91,90,2,37,71,27,49,48,94,37,9,34,72,89,35,20,24,11,60,65,76,49,67,41,23,
        65,25,92,84,23,30,85,36,15,65,33,75,34,68,43,92,69,6,81,94,55,39,93,50,63,94,79,92,74,39,37,80,4,9,12,22,41,
        21,70,35,80,33,16,51,51,72,59,46,99,65,41,79,13,43,50,35,94,67,25,4,100,8,4,23,97,47,93,44,31,57,56,76,38,94,
        48,33,81,83,5,23,98,59,88,29,46,35,87,45,12,23,78,50,59,78,86,26,76,68,2,18,48,71,34,35,58,32,77,93,55,59,97,
        90,62,75,69,94,18};

    const DataRange ans2DStackDataRange1(Point(0, 0, 0), Count(8, 8, 3));
    uint8_t ans2DStackData1[8 * 8 * 3] =
    { 39,83,33,84,81,37,56,71,82,26,60,63,72,32,4,79,60,68,18,26,67,78,76,33,78,79,9,90,91,21,66,91,99,68,31,77,82,96,
        60,58,79,65,23,56,55,93,23,29,85,24,91,90,2,37,71,27,49,48,94,37,9,34,72,89,35,20,24,11,60,65,76,49,67,41,23,
        65,25,92,84,23,30,85,36,15,65,33,75,34,68,43,92,69,6,81,94,55,39,93,50,63,94,79,92,74,39,37,80,4,9,12,22,41,
        21,70,35,80,33,16,51,51,72,59,46,99,65,41,79,13,43,50,35,94,67,25,4,100,8,4,23,97,47,93,44,31,57,56,76,38,94,
        48,33,81,83,5,23,98,59,88,29,46,35,87,45,12,23,78,50,59,78,86,26,76,68,2,18,48,71,34,35,58,32,77,93,55,59,97,
        90,62,75,69,94,18 };

    const DataRange ans2DStackDataRange2(Point(0, 0, 0), Count(8, 8, 3));
    uint8_t ans2DStackData2[4 * 4 * 3] =
    { 39,33,81,56,60,18,67,76,99,31,82,60,85,91,2,71,35,24,60,76,30,36,65,75,39,50,94,92,21,35,33,51,43,35,67,4,57,76,
        94,33,35,45,23,50,71,35,32,93 };

    const DataRange ans2DStackDataRange3(Point(0, 0), Count(8, 8));
    uint8_t ans2DStackData3[4 * 4 * 3] =
    { 39,33,81,56,60,18,67,76,99,31,82,60,85,91,2,71,35,24,60,76,30,36,65,75,39,50,94,92,21,35,33,51,43,35,67,4,57,76,
        94,33,35,45,23,50,71,35,32,93 };

    auto WriteData3D(const H5::H5File& h5File)->void {
        auto dataGroup = h5File.createGroup("Data");
        auto group3D = dataGroup.createGroup("3D");

        const Dimension dimension(8, 8, 8);
        auto dataSpace = TC::HDF5Utilities::GetDataSpace(dimension);

        auto dataSet = group3D.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpace);
        dataSet.write(data, H5::PredType::NATIVE_UINT16);

        dataSpace.close();
        dataSet.close();

        group3D.close();
        dataGroup.close();
    }

    auto WriteData2D(const H5::H5File& h5File)->void {
        auto dataGroup = h5File.createGroup("Data");
        auto group2DMIP = dataGroup.createGroup("2DMIP");

        const Dimension dimension(8, 8);
        auto dataSpace = TC::HDF5Utilities::GetDataSpace(dimension);

        auto dataSet = group2DMIP.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpace);
        dataSet.write(data2d, H5::PredType::NATIVE_UINT16);

        dataSpace.close();
        dataSet.close();

        group2DMIP.close();
        dataGroup.close();
    }

    auto WriteDataBF(const H5::H5File& h5File)->void {
        auto dataGroup = h5File.createGroup("Data");
        auto groupBf = dataGroup.createGroup("BF");

        const Dimension dimension(8, 8, 3);
        auto dataSpace = TC::HDF5Utilities::GetDataSpace(dimension);

        auto dataSet = groupBf.createDataSet("000000", H5::PredType::NATIVE_UINT8, dataSpace);
        dataSet.write(data2dStack, H5::PredType::NATIVE_UINT8);

        dataSpace.close();
        dataSet.close();

        groupBf.close();
        dataGroup.close();
    }

    auto CompareData(uint16_t* data1, uint16_t* data2, const size_t& numberOfElements)->bool {
        bool dataSame{ true };
        for (size_t i = 0; i < numberOfElements; ++i) {
            const auto data1Value = data1[i];
            const auto data2Value = data2[i];
            if (data1Value != data2Value) {
                dataSame = false;
                break;
            }
        }
        return dataSame;
    }

    auto CompareData(uint8_t* data1, uint8_t* data2, const size_t& numberOfElements)->bool {
        bool dataSame{ true };
        for (size_t i = 0; i < numberOfElements; ++i) {
            const auto data1Value = data1[i];
            const auto data2Value = data2[i];
            if (data1Value != data2Value) {
                dataSame = false;
                break;
            }
        }
        return dataSame;
    }

    TEST_CASE("TcfH5GroupRoiLdmDataReader 3D") {
        H5::H5File file("TcfH5GroupRoiLdmDataReaderTest.h5", H5F_ACC_TRUNC);
        WriteData3D(file);
        file.close();

        LdmConverting::TcfLdmConverter tcfLdmConverter;
        tcfLdmConverter.SetHtTileSize(4, 4, 4);
        tcfLdmConverter.SetInOutTcfPaths("TcfH5GroupRoiLdmDataReaderTest.h5", "TcfH5GroupRoiLdmDataReaderLdmTest.h5");
        tcfLdmConverter.Write();

        H5::H5File ldmFile("TcfH5GroupRoiLdmDataReaderLdmTest.h5", H5F_ACC_RDONLY);
        auto ldmDataGroup = ldmFile.openGroup("/Data/3D/000000");

        SECTION("Read() case 1") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans1DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(1);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result1 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(8, 8, 1));
            CHECK(CompareData(ans1Data, result1, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 2") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans2DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(1);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result2 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(1, 8, 8));
            CHECK(CompareData(ans2Data, result2, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 3") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans3DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(1);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result3 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(8, 1, 8));
            CHECK(CompareData(ans3Data, result3, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 4") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans4DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(1);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result4 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(4, 4, 4));
            CHECK(CompareData(ans4Data, result4, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 5") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans5DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(1);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result5 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(5, 5, 5));
            CHECK(CompareData(ans5Data, result5, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 6") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans6DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(1);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result6 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(1, 2, 3));
            CHECK(CompareData(ans6Data, result6, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 7") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans7DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(2);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result7 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(4, 4, 1));
            CHECK(CompareData(ans7Data, result7, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 8") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans8DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(2);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result8 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(4, 4, 1));
            CHECK(CompareData(ans8Data, result8, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 9") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans9DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(2);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result9 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(3, 3, 1));
            CHECK(CompareData(ans9Data, result9, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 10") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans10DataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(2);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result10 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(1, 4, 4));
            CHECK(CompareData(ans10Data, result10, dimension.GetNumberOfElements()));
        }

        ldmDataGroup.close();
        ldmFile.close();
    }

    TEST_CASE("TcfH5GroupRoiLdmDataReader 2D") {
        H5::H5File file("TcfH5GroupRoiLdmDataReaderTest.h5", H5F_ACC_TRUNC);
        WriteData2D(file);
        file.close();

        LdmConverting::TcfLdmConverter tcfLdmConverter;
        tcfLdmConverter.SetHtTileSize(4, 4, 4);
        tcfLdmConverter.SetInOutTcfPaths("TcfH5GroupRoiLdmDataReaderTest.h5", "TcfH5GroupRoiLdmDataReaderLdmTest.h5");
        tcfLdmConverter.Write();

        H5::H5File ldmFile("TcfH5GroupRoiLdmDataReaderLdmTest.h5", H5F_ACC_RDONLY);
        auto ldmDataGroup = ldmFile.openGroup("/Data/2DMIP/000000");

        SECTION("Read() case 1") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans2DDataRange);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(1);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result2D = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(5, 5));
            CHECK(CompareData(ans2DData, result2D, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 2") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans2DDataRange2);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(2);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result2D2 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(4, 4));
            CHECK(CompareData(ans2DData2, result2D2, dimension.GetNumberOfElements()));
        }

        ldmDataGroup.close();
        ldmFile.close();
    }

    TEST_CASE("TcfH5GroupRoiLdmDataReader 2D Stack") {
        H5::H5File file("TcfH5GroupRoiLdmDataReaderTest.h5", H5F_ACC_TRUNC);
        WriteDataBF(file);
        file.close();

        LdmConverting::TcfLdmConverter tcfLdmConverter;
        tcfLdmConverter.SetBfTileSize(4, 4);
        tcfLdmConverter.SetInOutTcfPaths("TcfH5GroupRoiLdmDataReaderTest.h5", "TcfH5GroupRoiLdmDataReaderLdmTest.h5");
        tcfLdmConverter.Write();

        H5::H5File ldmFile("TcfH5GroupRoiLdmDataReaderLdmTest.h5", H5F_ACC_RDONLY);
        auto ldmDataGroup = ldmFile.openGroup("/Data/BF/000000");

        SECTION("Read() case 1") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans2DStackDataRange1);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(1);
            tcfH5GroupRoiLdmDataReader.SetLdmDataIs2DStack(true);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result2DStack = memoryChunk->GetDataPointerAs<uint8_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(8, 8, 3));
            CHECK(CompareData(ans2DStackData1, result2DStack, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 2") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);
            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans2DStackDataRange2);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(2);
            tcfH5GroupRoiLdmDataReader.SetLdmDataIs2DStack(true);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result2DStack = memoryChunk->GetDataPointerAs<uint8_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(4, 4, 3));
            CHECK(CompareData(ans2DStackData2, result2DStack, dimension.GetNumberOfElements()));
        }

        SECTION("Read() case 3") {
            TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
            tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(ldmDataGroup);

            const auto dimensionality = ans2DStackDataRange3.GetDimensionality();

            tcfH5GroupRoiLdmDataReader.SetReadingRoi(ans2DStackDataRange3);
            tcfH5GroupRoiLdmDataReader.SetSamplingStep(2);
            tcfH5GroupRoiLdmDataReader.SetLdmDataIs2DStack(true);

            const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();
            auto result2DStack = memoryChunk->GetDataPointerAs<uint8_t>();

            const auto dimension = memoryChunk->GetDimension();
            CHECK(dimension == Dimension(4, 4, 3));
            CHECK(CompareData(ans2DStackData3, result2DStack, dimension.GetNumberOfElements()));
        }

        ldmDataGroup.close();
        ldmFile.close();
    }
}
