#include <catch2/catch.hpp>

#include "Hdf5DataSetHybridSamplerReader.h"

#include "H5Cpp.h"
using namespace TC::IO;

namespace Hdf5DataSetHybridSamplerReaderTest {

    const auto sizeX = 100;
    const auto sizeY = 110;
    const auto sizeZ = 120;

    auto GenerateRandomData3DUint16() -> std::shared_ptr<uint16_t[]> {
        const auto numberOfElements = sizeX * sizeY * sizeZ;

        std::shared_ptr<uint16_t[]> data(new uint16_t[numberOfElements]);

        std::random_device rd;
        std::mt19937 gen(rd());
        const std::uniform_int_distribution<uint16_t> dis(0, 65535);

        for (auto i = 0; i < numberOfElements; ++i) {
            data.get()[i] = dis(gen);
        }

        return data;
    }

    auto GenerateRandomData2DUint16() -> std::shared_ptr<uint16_t[]> {
        const auto numberOfElements = sizeX * sizeY;

        std::shared_ptr<uint16_t[]> data(new uint16_t[numberOfElements]);
        std::random_device rd;
        std::mt19937 gen(rd());
        const std::uniform_int_distribution<uint16_t> dis(0, 65535);

        for (auto i = 0; i < numberOfElements; ++i) {
            data.get()[i] = dis(gen);
        }

        return data;
    }

    auto GenerateRandomData3DFloat() -> std::shared_ptr<float[]> {
        const auto numberOfElements = sizeX * sizeY * sizeZ;

        std::shared_ptr<float[]> data(new float[numberOfElements]);

        std::random_device rd;
        std::mt19937 gen(rd());
        const std::uniform_int_distribution<uint16_t> dis(0, 65535);

        for (auto i = 0; i < numberOfElements; ++i) {
            data.get()[i] = dis(gen);
        }

        return data;
    }

    auto GenerateRandomData2DFloat() -> std::shared_ptr<float[]> {
        const auto numberOfElements = sizeX * sizeY;

        std::shared_ptr<float[]> data(new float[numberOfElements]);

        std::random_device rd;
        std::mt19937 gen(rd());
        const std::uniform_int_distribution<uint16_t> dis(0, 65535);

        for (auto i = 0; i < numberOfElements; ++i) {
            data.get()[i] = dis(gen);
        }

        return data;
    }

    auto CompareTwoData(const uint16_t* data1, const uint16_t* data2, const uint32_t& numberOfElements)->bool {
        auto dataSame = true;
        for(uint32_t i = 0; i<numberOfElements;++i) {
            if(data1[i] != data2[i]) {
                const auto comp1 = data1[i];
                const auto comp2 = data2[i];

                dataSame = false;
                //break;
            }
        }
        return dataSame;
    }

    auto CompareTwoData(const float* data1, const float* data2, const uint32_t& numberOfElements)->bool {
        auto dataSame = true;
        for (uint32_t i = 0; i < numberOfElements; ++i) {
            if (data1[i] != data2[i]) {
                dataSame = false;
                break;
            }
        }
        return dataSame;
    }

    auto Sample3D(uint16_t* srcData, const DataRange& dataRange) -> std::shared_ptr<uint16_t[]> {
        const auto sampleSizeX = dataRange.GetCount().X();
        const auto sampleSizeY = dataRange.GetCount().Y();
        const auto sampleSizeZ = dataRange.GetCount().Z();
        const auto numberOfSampleElements = sampleSizeX * sampleSizeY * sampleSizeZ;

        const auto offsetX = dataRange.GetOffsetPoint().X();
        const auto offsetY = dataRange.GetOffsetPoint().Y();
        const auto offsetZ = dataRange.GetOffsetPoint().Z();

        std::shared_ptr<uint16_t[]> sampledData(new uint16_t[numberOfSampleElements]());

        for (auto k = 0; k < sampleSizeZ; ++k) {
            for (auto j = 0; j < sampleSizeY; ++j) {
                for (auto i = 0; i < sampleSizeX; ++i) {
                    const auto srcIndex = (offsetX + i) + (offsetY + j) * sizeX + (offsetZ + k) * sizeX * sizeY;
                    const auto sampleIndex = i + j * sampleSizeX + k * sampleSizeX * sampleSizeY;
                    sampledData[sampleIndex] = srcData[srcIndex];
                }
            }
        }
        return sampledData;
    }

    auto Sample2D(uint16_t* srcData, const DataRange& dataRange) -> std::shared_ptr<uint16_t[]> {
        const auto sampleSizeX = dataRange.GetCount().X();
        const auto sampleSizeY = dataRange.GetCount().Y();
        const auto numberOfSampleElements = sampleSizeX * sampleSizeY;

        const auto offsetX = dataRange.GetOffsetPoint().X();
        const auto offsetY = dataRange.GetOffsetPoint().Y();

        std::shared_ptr<uint16_t[]> sampledData(new uint16_t[numberOfSampleElements]());

        for (auto j = 0; j < sampleSizeY; ++j) {
            for (auto i = 0; i < sampleSizeX; ++i) {
                const auto srcIndex = (offsetX + i) + (offsetY + j) * sizeX;
                const auto sampleIndex = i + j * sampleSizeX;
                sampledData[sampleIndex] = srcData[srcIndex];
            }
        }
        return sampledData;
    }

    auto Sample3D(float* srcData, const DataRange& dataRange) -> std::shared_ptr<float[]> {
        const auto sampleSizeX = dataRange.GetCount().X();
        const auto sampleSizeY = dataRange.GetCount().Y();
        const auto sampleSizeZ = dataRange.GetCount().Z();
        const auto numberOfSampleElements = sampleSizeX * sampleSizeY * sampleSizeZ;

        const auto offsetX = dataRange.GetOffsetPoint().X();
        const auto offsetY = dataRange.GetOffsetPoint().Y();
        const auto offsetZ = dataRange.GetOffsetPoint().Z();

        std::shared_ptr<float[]> sampledData(new float[numberOfSampleElements]());

        for (auto k = 0; k < sampleSizeZ; ++k) {
            for (auto j = 0; j < sampleSizeY; ++j) {
                for (auto i = 0; i < sampleSizeX; ++i) {
                    const auto srcIndex = (offsetX + i) + (offsetY + j) * sizeX + (offsetZ + k) * sizeX * sizeY;
                    const auto sampleIndex = i + j * sampleSizeX + k * sampleSizeX * sampleSizeY;
                    sampledData[sampleIndex] = srcData[srcIndex];
                }
            }
        }
        return sampledData;
    }

    auto Sample2D(float* srcData, const DataRange& dataRange) -> std::shared_ptr<float[]> {
        const auto sampleSizeX = dataRange.GetCount().X();
        const auto sampleSizeY = dataRange.GetCount().Y();
        const auto numberOfSampleElements = sampleSizeX * sampleSizeY;

        const auto offsetX = dataRange.GetOffsetPoint().X();
        const auto offsetY = dataRange.GetOffsetPoint().Y();

        std::shared_ptr<float[]> sampledData(new float[numberOfSampleElements]());

        for (auto j = 0; j < sampleSizeY; ++j) {
            for (auto i = 0; i < sampleSizeX; ++i) {
                const auto srcIndex = (offsetX + i) + (offsetY + j) * sizeX;
                const auto sampleIndex = i + j * sampleSizeX;
                sampledData[sampleIndex] = srcData[srcIndex];
            }
        }
        return sampledData;
    }

    TEST_CASE("Hdf5DataSetHybridSamplerReaderTest 2D from DataSet") {
        const std::string fileName = "Hdf5DataSetHybridSamplerReaderTest2D.h5";

        H5::H5File file(fileName, H5F_ACC_TRUNC);
        const auto rank = 2;

        const hsize_t dims[rank] = { sizeY,sizeX };

        SECTION("Read uint16_t") {
            auto data = GenerateRandomData2DUint16();

            H5::DataSpace space(rank, dims);
            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT16);

            const std::string dataSetName = "RawData2DUInt16";
            auto dataSet = file.createDataSet(dataSetName, dataType, space);
            dataSet.write(data.get(), dataType);

            space.close();
            dataType.close();
            LdmCore::LdmConfiguration ldmConfiguration;
            ldmConfiguration.SetTileUnitDimension(Dimension(20, 20));

            LdmCore::LdmTile::Pointer ldmTile(new LdmCore::LdmTile);
            const auto tileIndex = 0;
            ldmTile->SetTileIndex(tileIndex);
            ldmTile->SetTileUnitDimension(Dimension(20, 20));
            ldmTile->SetDataRange(DataRange(Point(10, 11), Count(20, 20)));
            ldmConfiguration.AddLdmTile(tileIndex, ldmTile);

            LdmReading::Hdf5DataSetHybridSamplerReader Hdf5DataSetHybridSamplerReader(dataSet, file);
            const auto memoryChunk = Hdf5DataSetHybridSamplerReader.Read(ldmConfiguration, tileIndex);

            dataSet.close();

            CHECK(memoryChunk->GetDimension() == Dimension(20, 20));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::UInt16Type);

            const auto resultData = std::any_cast<std::shared_ptr<uint16_t[]>>(memoryChunk->GetData());
            const auto answerData = Sample2D(data.get(), DataRange(Point(10, 11), Count(20, 20)));
            CHECK(CompareTwoData(resultData.get(), answerData.get(), 400));
        }

        SECTION("Read float") {
            auto data = GenerateRandomData2DFloat();

            H5::DataSpace space(rank, dims);
            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_FLOAT);

            const std::string dataSetName = "RawData2DFloat";
            auto dataSet = file.createDataSet(dataSetName, dataType, space);
            dataSet.write(data.get(), dataType);

            space.close();
            dataType.close();

            LdmCore::LdmConfiguration ldmConfiguration;
            ldmConfiguration.SetTileUnitDimension(Dimension(30, 30));

            LdmCore::LdmTile::Pointer ldmTile(new LdmCore::LdmTile);
            const auto tileIndex = 0;
            ldmTile->SetTileIndex(tileIndex);
            ldmTile->SetTileUnitDimension(Dimension(30, 30));
            ldmTile->SetDataRange(DataRange(Point(10, 11), Count(30, 30)));
            ldmConfiguration.AddLdmTile(tileIndex, ldmTile);

            LdmReading::Hdf5DataSetHybridSamplerReader Hdf5DataSetHybridSamplerReader(dataSet, file);
            const auto memoryChunk = Hdf5DataSetHybridSamplerReader.Read(ldmConfiguration, tileIndex);

            dataSet.close();

            CHECK(memoryChunk->GetDimension() == Dimension(30, 30));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::FloatType);

            const auto resultData = std::any_cast<std::shared_ptr<float[]>>(memoryChunk->GetData());
            const auto answerData = Sample2D(data.get(), DataRange(Point(10, 11), Count(30, 30)));
            CHECK(CompareTwoData(resultData.get(), answerData.get(), 900));
        }

        file.close();
    }

    TEST_CASE("Hdf5DataSetHybridSamplerReaderTest 2D from Tile") {
        const std::string fileName = "Hdf5DataSetHybridSamplerReaderTest2D.h5";

        H5::H5File file(fileName, H5F_ACC_TRUNC);
        const auto rank = 2;
        const auto sizeX = 100;
        const auto sizeY = 110;
        const hsize_t dims[rank] = { sizeY,sizeX };
        const auto numberOfElements = sizeX * sizeY;

        SECTION("Read uint16_t") {
            std::shared_ptr<uint16_t[]> data(new uint16_t[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) {
                data.get()[i] = i;
            }
            H5::DataSpace space(rank, dims);
            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT16);

            const std::string dataSetName = "TILE_10";
            auto dataSet = file.createDataSet(dataSetName, dataType, space);
            dataSet.write(data.get(), dataType);

            space.close();
            dataType.close();
            dataSet.close();

            LdmCore::LdmConfiguration ldmConfiguration;
            for (auto tileIndex = 0; tileIndex < 20; ++tileIndex) {
                LdmCore::LdmTile::Pointer ldmTile(new LdmCore::LdmTile);
                ldmTile->SetTileIndex(tileIndex);
                ldmConfiguration.AddLdmTile(tileIndex, ldmTile);
            }

            H5::DataSet dummyDataSet(hid_t{ -1 });
            LdmReading::Hdf5DataSetHybridSamplerReader Hdf5DataSetHybridSamplerReader(dummyDataSet, file);
            const auto memoryChunk = Hdf5DataSetHybridSamplerReader.Read(ldmConfiguration, 10);

            CHECK(memoryChunk->GetDimension() == Dimension(100, 110));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::UInt16Type);

            const auto resultData = std::any_cast<std::shared_ptr<uint16_t[]>>(memoryChunk->GetData());
            CHECK(CompareTwoData(resultData.get(), data.get(), numberOfElements));
        }

        SECTION("Read float") {
            std::shared_ptr<float[]> data(new float[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) {
                data.get()[i] = static_cast<float>(i);
            }
            H5::DataSpace space(rank, dims);
            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_FLOAT);

            const std::string dataSetName = "TILE_0123";
            auto dataSet = file.createDataSet(dataSetName, dataType, space);
            dataSet.write(data.get(), dataType);

            space.close();
            dataType.close();
            dataSet.close();

            LdmCore::LdmConfiguration ldmConfiguration;
            for (auto tileIndex = 0; tileIndex < 1001; ++tileIndex) {
                LdmCore::LdmTile::Pointer ldmTile(new LdmCore::LdmTile);
                ldmTile->SetTileIndex(tileIndex);
                ldmConfiguration.AddLdmTile(tileIndex, ldmTile);
            }

            H5::DataSet dummyDataSet(hid_t{ -1 });
            LdmReading::Hdf5DataSetHybridSamplerReader Hdf5DataSetHybridSamplerReader(dummyDataSet, file);
            const auto memoryChunk = Hdf5DataSetHybridSamplerReader.Read(ldmConfiguration, 123);

            CHECK(memoryChunk->GetDimension() == Dimension(100, 110));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::FloatType);

            const auto resultData = std::any_cast<std::shared_ptr<float[]>>(memoryChunk->GetData());
            CHECK(CompareTwoData(resultData.get(), data.get(), numberOfElements));
        }

        file.close();
    }

    TEST_CASE("Hdf5DataSetHybridSamplerReaderTest 3D from DataSet") {
        const std::string fileName = "Hdf5DataSetHybridSamplerReaderTest3D.h5";

        H5::H5File file(fileName, H5F_ACC_TRUNC);
        const auto rank = 3;

        const hsize_t dims[rank] = { sizeZ,sizeY,sizeX };

        SECTION("Read uint16_t") {
            auto data = GenerateRandomData3DUint16();

            H5::DataSpace space(rank, dims);
            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT16);

            const std::string dataSetName = "RawData3DUInt16";
            auto dataSet = file.createDataSet(dataSetName, dataType, space);
            dataSet.write(data.get(), dataType);

            space.close();
            dataType.close();

            LdmCore::LdmConfiguration ldmConfiguration;
            ldmConfiguration.SetTileUnitDimension(Dimension(20, 20, 10));

            LdmCore::LdmTile::Pointer ldmTile(new LdmCore::LdmTile);
            const auto tileIndex = 0;
            ldmTile->SetTileIndex(tileIndex);
            ldmTile->SetTileUnitDimension(Dimension(20, 20, 10));
            ldmTile->SetDataRange(DataRange(Point(10, 11, 12), Count(20, 20, 10)));
            ldmConfiguration.AddLdmTile(tileIndex, ldmTile);

            LdmReading::Hdf5DataSetHybridSamplerReader Hdf5DataSetHybridSamplerReader(dataSet, file);
            const auto memoryChunk = Hdf5DataSetHybridSamplerReader.Read(ldmConfiguration, tileIndex);

            dataSet.close();

            CHECK(memoryChunk->GetDimension() == Dimension(20, 20, 10));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::UInt16Type);

            const auto resultData = std::any_cast<std::shared_ptr<uint16_t[]>>(memoryChunk->GetData());
            const auto answerData = Sample3D(data.get(), DataRange(Point(10, 11, 12), Count(20, 20, 10)));
            CHECK(CompareTwoData(resultData.get(), answerData.get(), 4000));
        }

        SECTION("Read float") {
            auto data = GenerateRandomData3DFloat();

            H5::DataSpace space(rank, dims);
            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_FLOAT);

            const std::string dataSetName = "RawData3DFloat";
            auto dataSet = file.createDataSet(dataSetName, dataType, space);
            dataSet.write(data.get(), dataType);

            space.close();
            dataType.close();

            LdmCore::LdmConfiguration ldmConfiguration;
            ldmConfiguration.SetTileUnitDimension(Dimension(30, 30, 20));

            LdmCore::LdmTile::Pointer ldmTile(new LdmCore::LdmTile);
            const auto tileIndex = 1;
            ldmTile->SetTileIndex(tileIndex);
            ldmTile->SetTileUnitDimension(Dimension(30, 30, 20));
            ldmTile->SetDataRange(DataRange(Point(10, 11, 12), Count(30, 30, 20)));
            ldmConfiguration.AddLdmTile(tileIndex, ldmTile);

            LdmReading::Hdf5DataSetHybridSamplerReader Hdf5DataSetHybridSamplerReader(dataSet, file);
            const auto memoryChunk = Hdf5DataSetHybridSamplerReader.Read(ldmConfiguration, tileIndex);

            dataSet.close();

            CHECK(memoryChunk->GetDimension() == Dimension(30, 30, 20));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::FloatType);

            const auto resultData = std::any_cast<std::shared_ptr<float[]>>(memoryChunk->GetData());
            const auto answerData = Sample3D(data.get(), DataRange(Point(10, 11, 12), Count(30, 30, 20)));
            CHECK(CompareTwoData(resultData.get(), answerData.get(), 18000));
        }

        file.close();
    }

    TEST_CASE("Hdf5DataSetHybridSamplerReaderTest 3D") {
        const std::string fileName = "Hdf5DataSetHybridSamplerReaderTest3D.h5";

        H5::H5File file(fileName, H5F_ACC_TRUNC);
        const auto rank = 3;
        const auto sizeX = 100;
        const auto sizeY = 110;
        const auto sizeZ = 120;
        const hsize_t dims[rank] = { sizeZ,sizeY,sizeX };
        const auto numberOfElements = sizeX * sizeY * sizeZ;


        SECTION("Read uint16_t") {
            std::shared_ptr<uint16_t[]> data(new uint16_t[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) {
                data.get()[i] = i;
            }
            H5::DataSpace space(rank, dims);
            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_UINT16);

            const std::string dataSetName = "TILE_01";
            auto dataSet = file.createDataSet(dataSetName, dataType, space);
            dataSet.write(data.get(), dataType);

            space.close();
            dataType.close();
            dataSet.close();

            LdmCore::LdmConfiguration ldmConfiguration;
            for (auto tileIndex = 0; tileIndex < 20; ++tileIndex) {
                LdmCore::LdmTile::Pointer ldmTile(new LdmCore::LdmTile);
                ldmTile->SetTileIndex(tileIndex);
                ldmConfiguration.AddLdmTile(tileIndex, ldmTile);
            }

            H5::DataSet dummyDataSet(hid_t{ -1 });
            LdmReading::Hdf5DataSetHybridSamplerReader Hdf5DataSetHybridSamplerReader(dummyDataSet, file);
            const auto memoryChunk = Hdf5DataSetHybridSamplerReader.Read(ldmConfiguration, 1);

            CHECK(memoryChunk->GetDimension() == Dimension(100, 110, 120));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::UInt16Type);

            const auto resultData = std::any_cast<std::shared_ptr<uint16_t[]>>(memoryChunk->GetData());
            CHECK(CompareTwoData(resultData.get(), data.get(), numberOfElements));
        }

        SECTION("Read float") {
            std::shared_ptr<float[]> data(new float[numberOfElements]());
            for (auto i = 0; i < numberOfElements; ++i) {
                data.get()[i] = static_cast<float>(i);
            }
            H5::DataSpace space(rank, dims);
            auto dataType = static_cast<H5::DataType>(H5::PredType::NATIVE_FLOAT);

            const std::string dataSetName = "TILE_004";
            auto dataSet = file.createDataSet(dataSetName, dataType, space);
            dataSet.write(data.get(), dataType);

            space.close();
            dataType.close();
            dataSet.close();

            LdmCore::LdmConfiguration ldmConfiguration;
            for (auto tileIndex = 0; tileIndex < 101; ++tileIndex) {
                LdmCore::LdmTile::Pointer ldmTile(new LdmCore::LdmTile);
                ldmTile->SetTileIndex(tileIndex);
                ldmConfiguration.AddLdmTile(tileIndex, ldmTile);
            }

            H5::DataSet dummyDataSet(hid_t{ -1 });
            LdmReading::Hdf5DataSetHybridSamplerReader Hdf5DataSetHybridSamplerReader(dummyDataSet, file);
            const auto memoryChunk = Hdf5DataSetHybridSamplerReader.Read(ldmConfiguration, 4);

            CHECK(memoryChunk->GetDimension() == Dimension(100, 110, 120));
            CHECK(memoryChunk->GetDataType()._value == ChunkDataType::FloatType);

            const auto resultData = std::any_cast<std::shared_ptr<float[]>>(memoryChunk->GetData());
            CHECK(CompareTwoData(resultData.get(), data.get(), numberOfElements));
        }

        file.close();
    }
}
