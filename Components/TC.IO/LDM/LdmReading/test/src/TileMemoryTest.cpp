#include <catch2/catch.hpp>

#include "TileMemory.h"

using namespace TC::IO::LdmReading;
using namespace TC::IO;

namespace TileMemoryTest {
    TEST_CASE("TileMemory") {
        SECTION("TileMemory()") {
            std::shared_ptr<uint16_t[]> rawData(new uint16_t[6]);
            rawData.get()[0] = 1;
            rawData.get()[1] = 2;
            rawData.get()[2] = 3;

            const TileMemoryType tileMemoryType = TileMemoryType::UINT16;
            const uint32_t dimensionX = 1;
            const uint32_t dimensionY = 2;
            const uint32_t dimensionZ = 3;

            TileMemory tileMemory3D(rawData, tileMemoryType, dimensionX, dimensionY, dimensionZ);
            TileMemory tileMemory2D(rawData, tileMemoryType, dimensionX, dimensionY);

            CHECK(static_cast<uint16_t*>(tileMemory3D.GetRawPointer())[0] == 1);
            CHECK(static_cast<uint16_t*>(tileMemory3D.GetRawPointer())[1] == 2);
            CHECK(static_cast<uint16_t*>(tileMemory3D.GetRawPointer())[2] == 3);

            CHECK(tileMemory3D.GetDimensionX() == 1);
            CHECK(tileMemory3D.GetDimensionY() == 2);
            CHECK(tileMemory3D.GetDimensionZ() == 3);
            CHECK(tileMemory3D.GetTileMemoryType() == +TileMemoryType::UINT16);

            CHECK(static_cast<uint16_t*>(tileMemory2D.GetRawPointer())[0] == 1);
            CHECK(static_cast<uint16_t*>(tileMemory2D.GetRawPointer())[1] == 2);
            CHECK(static_cast<uint16_t*>(tileMemory2D.GetRawPointer())[2] == 3);

            CHECK(tileMemory2D.GetDimensionX() == 1);
            CHECK(tileMemory2D.GetDimensionY() == 2);
            CHECK(tileMemory2D.GetDimensionZ() == 1);
            CHECK(tileMemory2D.GetTileMemoryType() == +TileMemoryType::UINT16);
        }

        SECTION("TileMemory(TileMemory)") {
            std::shared_ptr<uint16_t[]> rawData(new uint16_t[6]);
            rawData.get()[0] = 1;
            rawData.get()[1] = 2;
            rawData.get()[2] = 3;

            const TileMemoryType tileMemoryType = TileMemoryType::UINT16;
            const uint32_t dimensionX = 1;
            const uint32_t dimensionY = 2;
            const uint32_t dimensionZ = 3;

            TileMemory srcTileMemory(rawData, tileMemoryType, dimensionX, dimensionY, dimensionZ);
            const auto destTileMemory(srcTileMemory);

            CHECK(static_cast<uint16_t*>(destTileMemory.GetRawPointer())[0] == 1);
            CHECK(static_cast<uint16_t*>(destTileMemory.GetRawPointer())[1] == 2);
            CHECK(static_cast<uint16_t*>(destTileMemory.GetRawPointer())[2] == 3);

            CHECK(destTileMemory.GetDimensionX() == 1);
            CHECK(destTileMemory.GetDimensionY() == 2);
            CHECK(destTileMemory.GetDimensionZ() == 3);
            CHECK(destTileMemory.GetTileMemoryType() == +TileMemoryType::UINT16);
        }

        SECTION("operator=()") {
            std::shared_ptr<uint16_t[]> rawData(new uint16_t[6]);
            rawData.get()[0] = 1;
            rawData.get()[1] = 2;
            rawData.get()[2] = 3;

            const TileMemoryType tileMemoryType = TileMemoryType::UINT16;
            const uint32_t dimensionX = 1;
            const uint32_t dimensionY = 2;
            const uint32_t dimensionZ = 3;

            TileMemory srcTileMemory(rawData, tileMemoryType, dimensionX, dimensionY, dimensionZ);
            TileMemory destTileMemory(std::any{}, TileMemoryType::UINT8, 0, 0, 0);
            destTileMemory = srcTileMemory;

            CHECK(static_cast<uint16_t*>(destTileMemory.GetRawPointer())[0] == 1);
            CHECK(static_cast<uint16_t*>(destTileMemory.GetRawPointer())[1] == 2);
            CHECK(static_cast<uint16_t*>(destTileMemory.GetRawPointer())[2] == 3);

            CHECK(destTileMemory.GetDimensionX() == 1);
            CHECK(destTileMemory.GetDimensionY() == 2);
            CHECK(destTileMemory.GetDimensionZ() == 3);
            CHECK(destTileMemory.GetTileMemoryType() == +TileMemoryType::UINT16);
        }

        SECTION("GetTileMemoryType()") {
            const std::any dummyRawData{};
            const TileMemoryType tileMemoryType = TileMemoryType::UINT16;

            TileMemory tileMemory(dummyRawData, tileMemoryType, 0, 0, 0);
            CHECK(tileMemory.GetTileMemoryType() == +TileMemoryType::UINT16);
        }

        SECTION("GetDimensionX()") {
            const std::any dummyRawData{};
            const auto dummyTileMemoryType = TileMemoryType::UINT16;

            const uint32_t dimensionX = 1;

            TileMemory tileMemory(dummyRawData, dummyTileMemoryType, dimensionX, 0, 0);
            CHECK(tileMemory.GetDimensionX() == 1);
        }

        SECTION("GetDimensionY()") {
            const std::any dummyRawData{};
            const auto dummyTileMemoryType = TileMemoryType::UINT16;

            const uint32_t dimensionY = 1;

            TileMemory tileMemory(dummyRawData, dummyTileMemoryType, 0, dimensionY, 0);
            CHECK(tileMemory.GetDimensionY() == 1);
        }

        SECTION("GetDimensionZ()") {
            const std::any dummyRawData{};
            const auto dummyTileMemoryType = TileMemoryType::UINT16;

            const uint32_t dimensionZ = 1;

            TileMemory tileMemory(dummyRawData, dummyTileMemoryType, 0, 0, dimensionZ);
            CHECK(tileMemory.GetDimensionZ() == 1);
        }

        SECTION("GetRawPointer()") {
            std::shared_ptr<uint16_t[]> rawData(new uint16_t[1]);
            const auto dummyTileMemoryType = TileMemoryType::UINT16;
            TileMemory tileMemory(rawData, dummyTileMemoryType, 0, 0, 0);

            CHECK(tileMemory.GetRawPointer() == rawData.get());
        }
    }
    
}
