#include "TcfLdmReadingUtilities.h"

#include "Hdf5Utilities.h"

namespace TC::IO::LdmReading {
    auto ReadSamplingStep(const H5::DataSet& tileDataSet) -> uint32_t {
        const auto samplingStep = HDF5Utilities::ReadAttributeInt64(tileDataSet, "SamplingStep");
        return static_cast<uint32_t>(samplingStep);
    }

    auto ReadTileDataRange(const H5::DataSet& tileDataSet) -> DataRange {
        const auto offsetPoint = ReadOffsetPoint(tileDataSet);
        const auto lastPoint = ReadLastPoint(tileDataSet);

        return DataRange(offsetPoint, lastPoint);
    }

    auto ReadOffsetPoint(const H5::DataSet& tileDataSet) -> Point {
        const auto offsetPointIs3D = tileDataSet.attrExists("DataIndexOffsetPointZ");
        const auto offsetPointIs2D = !offsetPointIs3D;

        const auto offsetX = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(tileDataSet, "DataIndexOffsetPointX"));
        const auto offsetY = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(tileDataSet, "DataIndexOffsetPointY"));

        Point offsetPoint;
        if (offsetPointIs2D) {
            offsetPoint = Point(offsetX, offsetY);
        } else if (offsetPointIs3D) {
            const auto offsetZ = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(tileDataSet, "DataIndexOffsetPointZ"));
            offsetPoint = Point(offsetX, offsetY, offsetZ);
        }

        return offsetPoint;
    }

    auto ReadLastPoint(const H5::DataSet& tileDataSet) -> Point {
        const auto lastPointIs3D = tileDataSet.attrExists("DataIndexLastPointZ");
        const auto lastPointIs2D = !lastPointIs3D;

        const auto lastX = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(tileDataSet, "DataIndexLastPointX"));
        const auto lastY = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(tileDataSet, "DataIndexLastPointY"));

        Point lastPoint;
        if (lastPointIs2D) {
            lastPoint = Point(lastX, lastY);
        } else if (lastPointIs3D) {
            const auto lastZ = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(tileDataSet, "DataIndexLastPointZ"));
            lastPoint = Point(lastX, lastY, lastZ);
        }

        return lastPoint;
    }
}
