#include "TcfLdmThumbnailReader.h"

#include <algorithm>
#include <cmath>

#include "Hdf5Utilities.h"
#include "LdmCoreUtilities.h"

#include "TcfH5GroupRoiLdmDataReader.h"

namespace TC::IO::LdmReading {
    struct TcfLdmThumbnailReader::Impl {
        Impl() = default;
        ~Impl() = default;

        H5::Group* ldmDataGroup{ nullptr };

        int32_t targetImageSizeX{};
        int32_t targetImageSizeY{};

        bool colorFlag{ false };

        auto CheckInputsAreValid() const ->bool;
        auto CalculateMaximumSamplingLevel(const int32_t& sizeX, const int32_t& sizeY)const->int32_t;
    };

    auto TcfLdmThumbnailReader::Impl::CheckInputsAreValid() const -> bool {
        if (this->ldmDataGroup == nullptr) {
            return false;
        }
        if (this->targetImageSizeX == 0) {
            return false;
        }
        if (this->targetImageSizeY == 0) {
            return false;
        }
        return true;
    }

    auto TcfLdmThumbnailReader::Impl::CalculateMaximumSamplingLevel(const int32_t& sizeX, const int32_t& sizeY) const
        -> int32_t {
        const auto& ldmDataGroup = *this->ldmDataGroup;

        const auto tileSizeX = HDF5Utilities::ReadAttributeInt64(ldmDataGroup, "TileSizeX");
        const auto tileSizeY = HDF5Utilities::ReadAttributeInt64(ldmDataGroup, "TileSizeY");

        const auto maximumSamplingLevelX = LdmCore::CalculateMaxSamplingLevel(static_cast<uint32_t>(sizeX), tileSizeX);
        const auto maximumSamplingLevelY = LdmCore::CalculateMaxSamplingLevel(static_cast<uint32_t>(sizeY), tileSizeY);

        return static_cast<int32_t>(std::max(maximumSamplingLevelX, maximumSamplingLevelY));
    }

    TcfLdmThumbnailReader::TcfLdmThumbnailReader()
        : d(new Impl()) {
    }

    TcfLdmThumbnailReader::~TcfLdmThumbnailReader() = default;

    auto TcfLdmThumbnailReader::SetLdmDataGroup(H5::Group& ldmDataGroup) -> void {
        d->ldmDataGroup = &ldmDataGroup;
    }

    auto TcfLdmThumbnailReader::SetTargetImageSizes(const int32_t& sizeX, const int32_t& sizeY) -> void {
        d->targetImageSizeX = sizeX;
        d->targetImageSizeY = sizeY;
    }

    auto TcfLdmThumbnailReader::SetColorFlag(const bool& colorFlag) -> void {
        d->colorFlag = colorFlag;
    }

    auto TcfLdmThumbnailReader::Read() -> MemoryChunk::Pointer {        
        if (!d->CheckInputsAreValid()) {
            return MemoryChunk::Pointer{ nullptr };
        }

        const auto dataSizeX = HDF5Utilities::ReadAttributeInt64(*d->ldmDataGroup, "DataSizeX");
        const auto dataSizeY = HDF5Utilities::ReadAttributeInt64(*d->ldmDataGroup, "DataSizeY");

        const auto maximumSamplingLevel = d->CalculateMaximumSamplingLevel(dataSizeX, dataSizeY);
        const auto targetMaximumSamplingLevel = d->CalculateMaximumSamplingLevel(d->targetImageSizeX, d->targetImageSizeY);

        const auto samplingLevelDifference = std::max(0, maximumSamplingLevel - targetMaximumSamplingLevel);
        const auto samplingStep = std::pow(2, samplingLevelDifference);

        const auto readingRoi = DataRange(Point(0, 0), Count(dataSizeX, dataSizeY));

        TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
        tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(*d->ldmDataGroup);
        tcfH5GroupRoiLdmDataReader.SetSamplingStep(samplingStep);
        tcfH5GroupRoiLdmDataReader.SetReadingRoi(readingRoi);
        tcfH5GroupRoiLdmDataReader.SetLdmDataIs2DStack(d->colorFlag);

        auto thumbnailMemoryChunk = tcfH5GroupRoiLdmDataReader.Read();
        return thumbnailMemoryChunk;
    }
}
