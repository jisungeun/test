#include "Hdf5DataSetHybridSamplerReader.h"
#include <iostream>
#include <QString>
#include "Hdf5DataSetRoiReader.h"
#include "Hdf5Utilities.h"
#include "LdmCoreUtilities.h"

namespace TC::IO::LdmReading {
    struct Hdf5DataSetHybridSamplerReader::Impl {
        Impl() = default;
        ~Impl() = default;

        H5::DataSet* sourceDataSet{};
        H5::Group* sampledDataGroup{};

        Point offsetPoint{};
    };

    Hdf5DataSetHybridSamplerReader::Hdf5DataSetHybridSamplerReader(H5::DataSet& sourceDataSet,
        H5::Group& sampledDataGroup)
        : d(new Impl()) {
        d->sourceDataSet = &sourceDataSet;
        d->sampledDataGroup = &sampledDataGroup;
    }

    Hdf5DataSetHybridSamplerReader::~Hdf5DataSetHybridSamplerReader() = default;

    auto Hdf5DataSetHybridSamplerReader::SetOffsetPoint(const Point& offset) -> void {
        d->offsetPoint = offset;
    }

    auto Hdf5DataSetHybridSamplerReader::Read(const LdmCore::LdmConfiguration& ldmConfiguration, const int32_t& tileIndex)
        -> MemoryChunk::Pointer {
        MemoryChunk::Pointer resultMemoryChunk;
        const auto tileDataSetName = LdmCore::TileName(tileIndex, ldmConfiguration.GetNumberOfTiles());
        const auto tileDataSetExist = d->sampledDataGroup->exists(tileDataSetName);
        if (tileDataSetExist) {
            resultMemoryChunk = ReadTileFromDataGroup(tileDataSetName);
        } else {
            resultMemoryChunk = ReadTileFromDataSet(ldmConfiguration.GetLdmTilePointerByTileIndex(tileIndex));
        }
        return resultMemoryChunk;
    }

    auto Hdf5DataSetHybridSamplerReader::ReadTileFromDataGroup(const std::string& tileDataSetName) const
        -> MemoryChunk::Pointer {
        auto tileDataSet = d->sampledDataGroup->openDataSet(tileDataSetName);
        auto tileDataSetMemoryChunk = HDF5Utilities::ReadDataSet(tileDataSet);
        tileDataSet.close();

        return tileDataSetMemoryChunk;
    }

    auto Hdf5DataSetHybridSamplerReader::ReadTileFromDataSet(const LdmCore::LdmTile::Pointer& targetLdmTile) const
        -> MemoryChunk::Pointer {
        const auto readingRoiRange = targetLdmTile->GetDataRange();

        DataRange offsetAddedRoiRange;
        if (readingRoiRange.GetDimensionality() == 2) {
            const auto roiOffsetPoint = readingRoiRange.GetOffsetPoint() + Point(d->offsetPoint.X(), d->offsetPoint.Y());
            const auto roiLastPoint = readingRoiRange.GetLastPoint() + Point(d->offsetPoint.X(), d->offsetPoint.Y());

            offsetAddedRoiRange = DataRange{ roiOffsetPoint, roiLastPoint };
        } else if (readingRoiRange.GetDimensionality() == 3) {
            const auto roiOffsetPoint = readingRoiRange.GetOffsetPoint() + d->offsetPoint;
            const auto roiLastPoint = readingRoiRange.GetLastPoint() + d->offsetPoint;

            offsetAddedRoiRange = DataRange{ roiOffsetPoint, roiLastPoint };
        }

        HDF5Utilities::Hdf5DataSetRoiReader hdf5DataSetRoiReader(*d->sourceDataSet);
        auto roiData = hdf5DataSetRoiReader.Read(offsetAddedRoiRange);
        roiData->ZeroPadding(targetLdmTile->GetTileUnitDimension());
        return roiData;
    }
}
