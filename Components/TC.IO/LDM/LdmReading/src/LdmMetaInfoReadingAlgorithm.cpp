#include "LdmMetaInfoReadingAlgorithm.h"

namespace TC::IO::LdmReading {
    LdmMetaInfoReadingAlgorithm::LdmMetaInfoReadingAlgorithm() = default;
    LdmMetaInfoReadingAlgorithm::~LdmMetaInfoReadingAlgorithm() = default;

    auto LdmMetaInfoReadingAlgorithm::ReadLdmMetaInfo(ILdmMetaInfoReader::Pointer& ldmMetaInfoReader,
        ILdmMetaInfoOutputPort::Pointer& ldmMetaInfoOutputPort) -> void {
        const auto ldmMetaInfo = ldmMetaInfoReader->Read();
        ldmMetaInfoOutputPort->SetLdmMetaInfo(ldmMetaInfo);
    }
}
