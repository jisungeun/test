#include "LdmDataReadingAlgorithm.h"

namespace TC::IO::LdmReading {
    LdmDataReadingAlgorithm::LdmDataReadingAlgorithm() = default;
    LdmDataReadingAlgorithm::~LdmDataReadingAlgorithm() = default;

    auto LdmDataReadingAlgorithm::ReadData(ILdmDataReader::Pointer& ldmDataReader,
        ILdmDataOutputPort::Pointer& ldmDataOutputPort) -> void {
        const auto dataRead = ldmDataReader->Read();
        ldmDataOutputPort->SetMemoryChunk(dataRead);
    }
}
