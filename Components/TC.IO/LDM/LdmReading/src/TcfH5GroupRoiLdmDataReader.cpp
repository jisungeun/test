#include "TcfH5GroupRoiLdmDataReader.h"

#include <iostream>
#include <QMap>

#include "CommonUtilities.h"
#include "Hdf5DataSetRoiReader.h"
#include "Hdf5Utilities.h"
#include "LdmCoreUtilities.h"
#include "TcfLdmReadingUtilities.h"

namespace TC::IO::LdmReading {
    struct TcfH5GroupRoiLdmDataReader::Impl {
        Impl() = default;
        ~Impl() {
            if (ldmDataGroup != nullptr) {
                ldmDataGroup->close();
                delete ldmDataGroup;
            }
        }

        H5::Group* ldmDataGroup{nullptr};
        int32_t samplingStep{0};
        DataRange roiDataRange;
        bool ldmDataIs2DStack{ false };

        auto CheckInputsAreValid() const ->bool;
        auto CheckRoiDataRangeIsValid() const ->bool;
        auto ReadDataDimension() const ->Dimension;

        auto GetOverlapTileDataRanges() const ->QMap<std::string, DataRange>;
        auto GetOverlapTileReadingRanges(const QMap<std::string, DataRange>& overlapTileDataRangeMap) const
            ->QMap<std::string, DataRange>;
        auto ApplySamplingStep(const QMap<std::string, DataRange>& dataRangeMap) const
            ->QMap<std::string, DataRange>;
        auto SampleDataRange(const DataRange& dataRange) const ->DataRange;
        auto ReadSubTileData(const QMap<std::string, DataRange>& subTileReadingRangeMap) const
            ->QMap<std::string, MemoryChunk::Pointer>;
        auto CalculateMappingRanges(const QMap<std::string, DataRange>& dataRangeMap,
            const DataRange& mappingDataRange)->QMap<std::string, DataRange>;
    };

    auto TcfH5GroupRoiLdmDataReader::Impl::CheckInputsAreValid() const -> bool {
        if (this->ldmDataGroup == nullptr) {
            return false;
        }

        if (this->samplingStep < 1) {
            return false;
        }

        if (!this->CheckRoiDataRangeIsValid()) {
            return false;
        }

        return true;
    }

    auto TcfH5GroupRoiLdmDataReader::Impl::CheckRoiDataRangeIsValid() const -> bool {
        const auto dataDimension = this->ReadDataDimension();
        const auto roiDataRangeIsValid = CheckReadingRangeIsIncludedInDimension(roiDataRange, dataDimension);
        return roiDataRangeIsValid;
    }

    auto TcfH5GroupRoiLdmDataReader::Impl::ReadDataDimension() const -> Dimension {
        const auto& ldmDataGroup = *this->ldmDataGroup;

        const auto dataDimensionIs3D = ldmDataGroup.attrExists("DataSizeZ");
        const auto dataDimensionIs2D = !dataDimensionIs3D;

        const auto dataSizeX = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(ldmDataGroup, "DataSizeX"));
        const auto dataSizeY = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(ldmDataGroup, "DataSizeY"));

        Dimension dataDimension;
        if (this->ldmDataIs2DStack) {
            dataDimension = Dimension(dataSizeX, dataSizeY, 3);
        } else if (dataDimensionIs2D) {
            dataDimension = Dimension(dataSizeX, dataSizeY);
        } else if (dataDimensionIs3D) {
            const auto dataSizeZ = static_cast<size_t>(HDF5Utilities::ReadAttributeInt64(ldmDataGroup, "DataSizeZ"));
            dataDimension = Dimension(dataSizeX, dataSizeY, dataSizeZ);
        }

        return dataDimension;
    }

    auto TcfH5GroupRoiLdmDataReader::Impl::GetOverlapTileDataRanges() const -> QMap<std::string, DataRange> {
        const auto& ldmDataGroup = *this->ldmDataGroup;

        const auto numberOfTileDataSet = static_cast<int32_t>(ldmDataGroup.getNumObjs());

        QMap<std::string, DataRange> overlapTileDataRanges;

        for (auto tileIndex = 0; tileIndex < numberOfTileDataSet; ++tileIndex) {
            const auto nameOfTileDataSet = LdmCore::TileName(tileIndex, numberOfTileDataSet);

            auto tileDataSet = ldmDataGroup.openDataSet(nameOfTileDataSet);

            const int samplingStep = ReadSamplingStep(tileDataSet);
            if (samplingStep != this->samplingStep) {
                tileDataSet.close();
                continue;
            }

            const auto tileDataRange = ReadTileDataRange(tileDataSet);
            tileDataSet.close();

            const auto overlapDataRange = roiDataRange.GetOverlapRange(tileDataRange);
            const auto tileIsOverlappedWithRoiDataRange = !overlapDataRange.IsEmpty();
            if (tileIsOverlappedWithRoiDataRange) {
                overlapTileDataRanges[nameOfTileDataSet] = overlapDataRange;
            }
        }

        return overlapTileDataRanges;
    }

    auto TcfH5GroupRoiLdmDataReader::Impl::GetOverlapTileReadingRanges(
        const QMap<std::string, DataRange>& overlapTileDataRangeMap) const -> QMap<std::string, DataRange> {
        const auto& ldmDataGroup = *this->ldmDataGroup;

        QMap<std::string, DataRange> subTileReadingRangeMap;
        for (auto iterator = overlapTileDataRangeMap.begin(); iterator != overlapTileDataRangeMap.end(); ++iterator) {
            const auto& nameOfTileDataSet = iterator.key();
            const auto& overlapTileDataRange = iterator.value();

            auto tileDataSet = ldmDataGroup.openDataSet(nameOfTileDataSet);
            const auto tileDataRange = ReadTileDataRange(tileDataSet);
            tileDataSet.close();

            const auto tileOffsetPoint = tileDataRange.GetOffsetPoint();
            const auto overlapOffsetPoint = overlapTileDataRange.GetOffsetPoint();

            const auto tileReadingStartPoint = overlapOffsetPoint - tileOffsetPoint;
            const auto tileReadingCount = overlapTileDataRange.GetCount();
            const auto tileReadingDataRange = DataRange(tileReadingStartPoint, tileReadingCount);

            subTileReadingRangeMap[nameOfTileDataSet] = tileReadingDataRange;
        }

        return subTileReadingRangeMap;
    }

    auto TcfH5GroupRoiLdmDataReader::Impl::ApplySamplingStep(const QMap<std::string, DataRange>& dataRangeMap) const
        -> QMap<std::string, DataRange> {
        QMap<std::string, DataRange> sampledDataRangeMap;
        for (auto iterator = dataRangeMap.begin(); iterator != dataRangeMap.end();
            ++iterator) {
            const auto& nameOfTileDataSet = iterator.key();
            const auto& overlapTileDataRange = iterator.value();
            const auto sampledDataRange = this->SampleDataRange(overlapTileDataRange);

            sampledDataRangeMap[nameOfTileDataSet] = sampledDataRange;
        }
        return sampledDataRangeMap;
    }

    auto TcfH5GroupRoiLdmDataReader::Impl::SampleDataRange(const DataRange& dataRange) const -> DataRange {
        const auto offsetPoint = dataRange.GetOffsetPoint();
        const auto lastPoint = dataRange.GetLastPoint();

        const auto offsetPointX = static_cast<int64_t>(
            std::ceil(static_cast<double>(offsetPoint.X() / static_cast<double>(this->samplingStep))));
        const auto offsetPointY = static_cast<int64_t>(
            std::ceil(static_cast<double>(offsetPoint.Y() / static_cast<double>(this->samplingStep))));
        const auto offsetPointZ = static_cast<int64_t>(
            std::ceil(static_cast<double>(offsetPoint.Z() / static_cast<double>(this->samplingStep))));

        const auto lastPointX = static_cast<int64_t>(
            std::floor(static_cast<double>(lastPoint.X() / static_cast<double>(this->samplingStep))));
        const auto lastPointY = static_cast<int64_t>(
            std::floor(static_cast<double>(lastPoint.Y() / static_cast<double>(this->samplingStep))));
        const auto lastPointZ = static_cast<int64_t>(
            std::floor(static_cast<double>(lastPoint.Z() / static_cast<double>(this->samplingStep))));

        DataRange sampledReadingDataRange;

        if (this->ldmDataIs2DStack) {
            sampledReadingDataRange =
                DataRange(Point(offsetPointX, offsetPointY, 0), Point(lastPointX, lastPointY, 2));
        } else if (dataRange.GetDimensionality() == 3) {
            sampledReadingDataRange =
                DataRange(Point(offsetPointX, offsetPointY, offsetPointZ), Point(lastPointX, lastPointY, lastPointZ));
        } else { //dimensionality 2
            sampledReadingDataRange =
                DataRange(Point(offsetPointX, offsetPointY), Point(lastPointX, lastPointY));
        }
        return sampledReadingDataRange;
    }

    auto TcfH5GroupRoiLdmDataReader::Impl::ReadSubTileData(
        const QMap<std::string, DataRange>& subTileReadingRangeMap) const -> QMap<std::string, MemoryChunk::Pointer> {
        QMap<std::string, MemoryChunk::Pointer> subTileDataMap;
        for (auto iterator = subTileReadingRangeMap.begin(); iterator != subTileReadingRangeMap.end(); ++iterator) {
            const auto& nameOfTileDataSet = iterator.key();
            const auto& subTileReadingRange = iterator.value();

            if(subTileReadingRange.IsEmpty()) {
                continue;
            }

            auto tileDataSet = this->ldmDataGroup->openDataSet(nameOfTileDataSet);

            HDF5Utilities::Hdf5DataSetRoiReader hdf5DataSetRoiReader(tileDataSet);
            const auto subTileRangeData = hdf5DataSetRoiReader.Read(subTileReadingRange);

            subTileDataMap[nameOfTileDataSet] = subTileRangeData;

            tileDataSet.close();
        }
        return subTileDataMap;
    }

    auto TcfH5GroupRoiLdmDataReader::Impl::CalculateMappingRanges(
        const QMap<std::string, DataRange>& dataRangeMap, const DataRange& mappingDataRange)
        -> QMap<std::string, DataRange> {

        QMap<std::string, DataRange> mappingRangeMap;
        for (auto iterator = dataRangeMap.begin(); iterator != dataRangeMap.end(); ++iterator) {
            const auto& nameOfTileDataSet = iterator.key();
            const auto& dataRange = iterator.value();

            const auto offsetPoint = dataRange.GetOffsetPoint();
            const auto count = dataRange.GetCount();

            const auto mappingOffsetPoint = mappingDataRange.GetOffsetPoint();

            const auto mappedOffsetPoint = offsetPoint - mappingOffsetPoint;
            const auto mappingRange = DataRange(mappedOffsetPoint, count);

            mappingRangeMap[nameOfTileDataSet] = mappingRange;
        }
        return mappingRangeMap;
    }

    TcfH5GroupRoiLdmDataReader::TcfH5GroupRoiLdmDataReader()
        : d(new Impl()) {
    }

    TcfH5GroupRoiLdmDataReader::~TcfH5GroupRoiLdmDataReader() = default;

    auto TcfH5GroupRoiLdmDataReader::SetLdmDataGroup(H5::Group& ldmDataGroup) -> void {
        if (d->ldmDataGroup != nullptr) {
            d->ldmDataGroup->close();
            delete d->ldmDataGroup;
        }
        d->ldmDataGroup = new H5::Group(ldmDataGroup.getId());
    }

    auto TcfH5GroupRoiLdmDataReader::SetReadingRoi(const DataRange& roiDataRange) -> void {
        d->roiDataRange = roiDataRange;
    }

    auto TcfH5GroupRoiLdmDataReader::SetSamplingStep(const int32_t& samplingStep) -> void {
        d->samplingStep = samplingStep;
    }

    auto TcfH5GroupRoiLdmDataReader::SetLdmDataIs2DStack(const bool& ldmDataIs2DStack) -> void {
        d->ldmDataIs2DStack = ldmDataIs2DStack;
    }

    auto TcfH5GroupRoiLdmDataReader::Read() -> MemoryChunk::Pointer {
        if(d->ldmDataIs2DStack) {
            const auto offsetPoint = d->roiDataRange.GetOffsetPoint();
            const auto count = d->roiDataRange.GetCount();
            d->roiDataRange = DataRange(Point(offsetPoint.X(), offsetPoint.Y(), 0), Count(count.X(), count.Y(), 3));
        }

        const auto inputsAreInvalid = !d->CheckInputsAreValid();
        if (inputsAreInvalid) {
            return MemoryChunk::Pointer{ nullptr };
        }

        const auto overlapTileDataRangeMap = d->GetOverlapTileDataRanges();
        const auto overlapTileReadingDataRangeMap = d->GetOverlapTileReadingRanges(overlapTileDataRangeMap);

        const auto sampledOverlapTileDataRangeMap = d->ApplySamplingStep(overlapTileDataRangeMap);
        const auto sampledOverlapTileReadingDataRangeMap = d->ApplySamplingStep(overlapTileReadingDataRangeMap);

        const auto sampledRoi = d->SampleDataRange(d->roiDataRange);
        const auto mappingRangeMap = d->CalculateMappingRanges(sampledOverlapTileDataRangeMap, sampledRoi);

        const auto subTileDataMap = d->ReadSubTileData(sampledOverlapTileReadingDataRangeMap);

        const auto sampledRoiDataDimension = Dimension(sampledRoi.GetCount());
        const auto roiMemoryChunk = MergeRoiMemoryChunk(subTileDataMap, mappingRangeMap, sampledRoiDataDimension);

        return roiMemoryChunk;
    }

    auto TcfH5GroupRoiLdmDataReader::MergeRoiMemoryChunk(
        const QMap<std::string, MemoryChunk::Pointer>& subTileDataMap,
        const QMap<std::string, DataRange>& mappingRangeMap, const Dimension& roiDataDimension)
        -> MemoryChunk::Pointer {

        const auto chunkDataType = subTileDataMap.begin().value()->GetDataType();
        const auto roiDataPointer = AllocateRawData(chunkDataType, roiDataDimension.GetNumberOfElements());

        for (auto iterator = subTileDataMap.begin(); iterator != subTileDataMap.end(); ++iterator) {
            const auto& nameOfTileDataSet = iterator.key();
            const auto& subTileData = subTileDataMap[nameOfTileDataSet];
            const auto& mappingRange = mappingRangeMap[nameOfTileDataSet];

            const auto tileRangeDataPointer = subTileData->GetDataPointer();

            CopyDataForMerge(tileRangeDataPointer, mappingRange, chunkDataType, roiDataDimension, roiDataPointer);
        }

        return GenerateMemoryChunk(roiDataPointer, roiDataDimension, chunkDataType);
    }

    auto TcfH5GroupRoiLdmDataReader::CopyDataForMerge(void* tileRangeDataPointer, const DataRange& mappingRange,
        const ChunkDataType& dataType, const Dimension& roiDataDimension, void* roiDataPointer) -> void {
        if (dataType == +ChunkDataType::Int8Type) {
            CopyData<int8_t>(tileRangeDataPointer, mappingRange, roiDataDimension, roiDataPointer);
        } else if (dataType == +ChunkDataType::UInt8Type) {
            CopyData<uint8_t>(tileRangeDataPointer, mappingRange, roiDataDimension, roiDataPointer);
        } else if (dataType == +ChunkDataType::Int16Type) {
            CopyData<int16_t>(tileRangeDataPointer, mappingRange, roiDataDimension, roiDataPointer);
        } else if (dataType == +ChunkDataType::UInt16Type) {
            CopyData<uint16_t>(tileRangeDataPointer, mappingRange, roiDataDimension, roiDataPointer);
        } else if (dataType == +ChunkDataType::Int32Type) {
            CopyData<int32_t>(tileRangeDataPointer, mappingRange, roiDataDimension, roiDataPointer);
        } else if (dataType == +ChunkDataType::UInt32Type) {
            CopyData<uint32_t>(tileRangeDataPointer, mappingRange, roiDataDimension, roiDataPointer);
        } else if (dataType == +ChunkDataType::FloatType) {
            CopyData<float>(tileRangeDataPointer, mappingRange, roiDataDimension, roiDataPointer);
        } else if (dataType == +ChunkDataType::DoubleType) {
            CopyData<double>(tileRangeDataPointer, mappingRange, roiDataDimension, roiDataPointer);
        } 
    }

    template <class T>
    auto TcfH5GroupRoiLdmDataReader::CopyData(void* tileDataPointer, const DataRange& mappingRange,
        const Dimension& roiDataDimension, void* roiDataPointer) -> void {
        const auto castTileRangeDataPointer = static_cast<T*>(tileDataPointer);
        const auto castRoiDataPointer = static_cast<T*>(roiDataPointer);

        const auto offsetPointX = mappingRange.GetOffsetPoint().X();
        const auto offsetPointY = mappingRange.GetOffsetPoint().Y();
        const auto offsetPointZ = mappingRange.GetOffsetPoint().Z();

        const auto countX = mappingRange.GetCount().X();
        const auto countY = mappingRange.GetCount().Y();
        auto countZ = mappingRange.GetCount().Z();
        if (mappingRange.GetDimensionality() == 2) {
            countZ = 1;
        }

        const auto roiDataDimensionX = static_cast<int64_t>(roiDataDimension.X());
        const auto roiDataDimensionY = static_cast<int64_t>(roiDataDimension.Y());

        for (auto k = 0; k < countZ; ++k) {
            for (auto j = 0; j < countY; ++j) {
                const auto tileDataStartIndex = j * countX + k * countX * countY;
                const auto tileDataEndIndex = tileDataStartIndex + countX;

                const auto tileDataStartPointer = &castTileRangeDataPointer[tileDataStartIndex];
                const auto tileDataEndPointer = &castTileRangeDataPointer[tileDataEndIndex];

                const auto roiDataIndex = 
                    offsetPointX +
                    (offsetPointY + j) * roiDataDimensionX +
                    (offsetPointZ + k) * roiDataDimensionX * roiDataDimensionY;

                const auto roiDataStartPointer = &castRoiDataPointer[roiDataIndex];

                std::copy(tileDataStartPointer, tileDataEndPointer, roiDataStartPointer);
            }
        }
    }
}
