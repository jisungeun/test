#include "TcfH5GroupTileLdmDataReader.h"

#include <QString>

#include "Hdf5Utilities.h"
#include "LdmCoreUtilities.h"

namespace TC::IO::LdmReading {
    struct TcfH5GroupTileLdmDataReader::Impl {
        Impl() = default;
        ~Impl() {
            if (dataGroup != nullptr) {
                dataGroup->close();
                delete dataGroup;
            }
        }

        H5::Group* dataGroup{};
        int32_t tileIndex{-1};
    };

    TcfH5GroupTileLdmDataReader::TcfH5GroupTileLdmDataReader()
        : d(new Impl()) {
    }

    TcfH5GroupTileLdmDataReader::~TcfH5GroupTileLdmDataReader() = default;    

    auto TcfH5GroupTileLdmDataReader::SetDataGroup(H5::Group& dataGroup) -> void {
        if (d->dataGroup != nullptr) {
            d->dataGroup->close();
            delete d->dataGroup;
        }

        d->dataGroup = new H5::Group(dataGroup.getId());
    }

    auto TcfH5GroupTileLdmDataReader::SetReadingTileIndex(const int32_t& tileIndex) -> void {
        d->tileIndex = tileIndex;
    }

    auto TcfH5GroupTileLdmDataReader::Read() -> MemoryChunk::Pointer {
        const auto tileName = GetTileName(*d->dataGroup, d->tileIndex);
        const auto dataSetExists = d->dataGroup->exists(tileName);

        MemoryChunk::Pointer memoryChunk{};
        if (dataSetExists) {
            auto tileDataSet = d->dataGroup->openDataSet(tileName);
            memoryChunk = HDF5Utilities::ReadDataSet(tileDataSet);
            tileDataSet.close();
        }

        return memoryChunk;
    }

    auto TcfH5GroupTileLdmDataReader::GetTileName(const H5::Group& dataGroup, const int32_t& tileIndex) -> std::string {
        const auto numberOfTiles = GetNumberOfTiles(dataGroup);
        return LdmCore::TileName(tileIndex, numberOfTiles);
    }

    auto TcfH5GroupTileLdmDataReader::GetNumberOfTiles(const H5::Group& dataGroup) -> uint32_t {
        uint32_t numberOfTiles;
        const std::string numberOfTilesAttributeName = "NumberOfTiles";
        const auto numberOfTileAttributeExists = dataGroup.attrExists(numberOfTilesAttributeName);
        if (numberOfTileAttributeExists) {
            numberOfTiles = 
                static_cast<uint32_t>(HDF5Utilities::ReadAttributeInt64(dataGroup, numberOfTilesAttributeName));
        } else {
            numberOfTiles = 0;
        }
        return numberOfTiles;
    }
}
