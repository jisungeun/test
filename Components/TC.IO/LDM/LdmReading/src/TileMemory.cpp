#include "TileMemory.h"

namespace TC::IO::LdmReading {
    struct TileMemory::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        std::any data{};
        TileMemoryType tileMemoryType{ TileMemoryType::INT8 };
        uint32_t dimensionX{};
        uint32_t dimensionY{};
        uint32_t dimensionZ{};
    };

    TileMemory::TileMemory(const std::any& data, const TileMemoryType& tileMemoryType, const uint32_t& dimensionX,
        const uint32_t& dimensionY, const uint32_t& dimensionZ)
        : d(new Impl()) {
        d->data = data;
        d->tileMemoryType = tileMemoryType;
        d->dimensionX = dimensionX;
        d->dimensionY = dimensionY;
        d->dimensionZ = dimensionZ;
    }

    TileMemory::TileMemory(const TileMemory& other)
        : d(new Impl(*other.d)){
    }

    auto TileMemory::operator=(const TileMemory& other) -> TileMemory& {
        *d = *(other.d);
        return *this;
    }

    TileMemory::~TileMemory() = default;

    auto TileMemory::GetTileMemoryType() const -> TileMemoryType {
        return d->tileMemoryType;
    }

    auto TileMemory::GetDimensionX() const -> uint32_t {
        return d->dimensionX;
    }

    auto TileMemory::GetDimensionY() const -> uint32_t {
        return d->dimensionY;
    }

    auto TileMemory::GetDimensionZ() const -> uint32_t {
        return d->dimensionZ;
    }

    auto TileMemory::GetRawPointer() const -> void* {
        void* rawPointer;
        if (d->tileMemoryType == +TileMemoryType::INT8) {
            rawPointer = std::any_cast<std::shared_ptr<int8_t[]>>(d->data).get();
        } else if (d->tileMemoryType == +TileMemoryType::UINT8) {
            rawPointer = std::any_cast<std::shared_ptr<uint8_t[]>>(d->data).get();
        } else if (d->tileMemoryType == +TileMemoryType::INT16) {
            rawPointer = std::any_cast<std::shared_ptr<int16_t[]>>(d->data).get();
        } else if (d->tileMemoryType == +TileMemoryType::UINT16) {
            rawPointer = std::any_cast<std::shared_ptr<uint16_t[]>>(d->data).get();
        } else if (d->tileMemoryType == +TileMemoryType::INT32) {
            rawPointer = std::any_cast<std::shared_ptr<int32_t[]>>(d->data).get();
        } else if (d->tileMemoryType == +TileMemoryType::UINT32) {
            rawPointer = std::any_cast<std::shared_ptr<uint32_t[]>>(d->data).get();
        } else if (d->tileMemoryType == +TileMemoryType::FLOAT) {
            rawPointer = std::any_cast<std::shared_ptr<float[]>>(d->data).get();
        } else if (d->tileMemoryType == +TileMemoryType::DOUBLE) {
            rawPointer = std::any_cast<std::shared_ptr<double[]>>(d->data).get();
        } else {
            rawPointer = nullptr;
        }
        return rawPointer;
    }
}
