#include "TileMemoryPresenter.h"

namespace TC::IO::LdmReading {
    struct TileMemoryPresenter::Impl {
        Impl() = default;
        ~Impl() = default;

        TileMemory tileMemory{ std::any{},TileMemoryType::UINT8,0,0,0 };
    };

    TileMemoryPresenter::TileMemoryPresenter()
        : d(new Impl()) {
    }

    TileMemoryPresenter::~TileMemoryPresenter() = default;

    auto TileMemoryPresenter::GetTileMemory() const -> TileMemory {
        return d->tileMemory;
    }

    auto TileMemoryPresenter::SetMemoryChunk(const MemoryChunk::Pointer& memoryChunk) -> void {
        const auto& data = memoryChunk->GetData();
        const auto tileMemoryType = ConvertToTileMemoryType(memoryChunk->GetDataType());

        const auto dimension = memoryChunk->GetDimension();

        const auto dimensionIs2D = dimension.GetDimensionality() == 2;
        const auto dimensionIs3D = dimension.GetDimensionality() == 3;

        const auto dimensionX = static_cast<uint32_t>(dimension.X());
        const auto dimensionY = static_cast<uint32_t>(dimension.Y());
        const auto dimensionZ = static_cast<uint32_t>(dimension.Z());

        if (dimensionIs2D) {
            d->tileMemory = TileMemory(data, tileMemoryType, dimensionX, dimensionY);
        } else if(dimensionIs3D) {
            d->tileMemory = TileMemory(data, tileMemoryType, dimensionX, dimensionY, dimensionZ);
        }
    }

    auto TileMemoryPresenter::ConvertToTileMemoryType(const ChunkDataType& chunkDataType) -> TileMemoryType {
        if (chunkDataType == +ChunkDataType::Int8Type) {
            return TileMemoryType::INT8;
        } else if (chunkDataType == +ChunkDataType::UInt8Type) {
            return TileMemoryType::UINT8;
        } else if (chunkDataType == +ChunkDataType::Int16Type) {
            return TileMemoryType::INT16;
        } else if (chunkDataType == +ChunkDataType::UInt16Type) {
            return TileMemoryType::UINT16;
        } else if (chunkDataType == +ChunkDataType::Int32Type) {
            return TileMemoryType::INT32;
        } else if (chunkDataType == +ChunkDataType::UInt32Type) {
            return TileMemoryType::UINT32;
        } else if (chunkDataType == +ChunkDataType::FloatType) {
            return TileMemoryType::FLOAT;
        } else if (chunkDataType == +ChunkDataType::DoubleType) {
            return TileMemoryType::DOUBLE;
        } else {
            return TileMemoryType::INT8;
        }
    }
}
