#include "Hdf5DataSet3DTileLdmDataReader.h"

#include "CommonUtilities.h"
#include "Hdf5Utilities.h"
#include "LdmCoreUtilities.h"
#include "Octree3DConfigurator.h"

namespace TC::IO::LdmReading {
    struct Hdf5DataSet3DTileLdmDataReader::Impl {
        Impl() = default;
        ~Impl() {
            if (dataSet != nullptr) {
                dataSet->close();
                delete dataSet;
            }
        }

        H5::DataSet* dataSet{nullptr};
        Dimension tileUnitDimension{};
        int32_t tileIndex{-1};
    };

    Hdf5DataSet3DTileLdmDataReader::Hdf5DataSet3DTileLdmDataReader()
        : d(new Impl()) {
    }

    Hdf5DataSet3DTileLdmDataReader::~Hdf5DataSet3DTileLdmDataReader() = default;

    auto Hdf5DataSet3DTileLdmDataReader::SetTargetDataSet(H5::DataSet& dataSet) -> void {
        const auto dataDimension = HDF5Utilities::GetDimension(dataSet);
        if (dataDimension.GetDimensionality() != 3) {
            return;
        }

        if (d->dataSet != nullptr) {
            d->dataSet->close();
            delete d->dataSet;
        }
        d->dataSet = new H5::DataSet(dataSet.getId());
    }

    auto Hdf5DataSet3DTileLdmDataReader::SetTileUnitDimension(const Dimension& tileUnitDimension) -> void {
        if (tileUnitDimension.GetDimensionality() != 3) {
            return;
        }

        d->tileUnitDimension = tileUnitDimension;
    }

    auto Hdf5DataSet3DTileLdmDataReader::SetReadingTileIndex(const int32_t& tileIndex) -> void {
        d->tileIndex = tileIndex;
    }

    auto Hdf5DataSet3DTileLdmDataReader::Read() -> MemoryChunk::Pointer {
        const auto dataDimension = HDF5Utilities::GetDimension(*d->dataSet);
        const auto ldmTile = GetLdmTile(dataDimension, d->tileUnitDimension, d->tileIndex);
        auto tileData = ReadTileData(*d->dataSet, ldmTile);

        return tileData;
    }

    auto Hdf5DataSet3DTileLdmDataReader::GetLdmTile(const Dimension& dataDimension, const Dimension& tileUnitDimension,
        const int32_t& tileIndex) -> LdmCore::LdmTile::Pointer {
        LdmCore::Octree3DConfigurator octree3DConfigurator(dataDimension, tileUnitDimension);
        const auto ldmConfiguration = octree3DConfigurator.Configure();
        const auto ldmTile = ldmConfiguration.GetLdmTilePointerByTileIndex(tileIndex);

        return ldmTile;
    }

    auto Hdf5DataSet3DTileLdmDataReader::ReadTileData(const H5::DataSet& dataSet, const LdmCore::LdmTile::Pointer& ldmTile)
        -> MemoryChunk::Pointer {
        const auto dataRange = ldmTile->GetDataRange();
        const auto samplingLevel = ldmTile->GetSamplingLevel();

        auto selectedSpace = GetSelectedH5Space(dataSet, dataRange, samplingLevel);
        const auto sampledDimension = LdmCore::CalculateSampledDimension(dataRange, samplingLevel);
        auto outputSpace = HDF5Utilities::GetDataSpace(sampledDimension);

        auto dataType = dataSet.getDataType();

        const auto numberOfElements = static_cast<size_t>(outputSpace.getSimpleExtentNpoints());
        const auto rawData = HDF5Utilities::AllocateRawData(dataType, numberOfElements);

        dataSet.read(rawData, dataType, outputSpace, selectedSpace);

        auto tileMemoryChunk = 
            GenerateMemoryChunk(rawData, sampledDimension, HDF5Utilities::ToChunkDataType(dataType));

        selectedSpace.close();
        outputSpace.close();
        dataType.close();

        tileMemoryChunk->ZeroPadding(ldmTile->GetTileUnitDimension());

        return tileMemoryChunk;
    }

    auto Hdf5DataSet3DTileLdmDataReader::GetSelectedH5Space(const H5::DataSet& dataSet, 
        const DataRange& dataRange, const int32_t& samplingLevel) -> H5::DataSpace {
        auto selectedSpace = dataSet.getSpace();

        const auto offsetX = static_cast<hsize_t>(dataRange.GetOffsetPoint().X());
        const auto offsetY = static_cast<hsize_t>(dataRange.GetOffsetPoint().Y());
        const auto offsetZ = static_cast<hsize_t>(dataRange.GetOffsetPoint().Z());

        const auto sampledDimension = LdmCore::CalculateSampledDimension(dataRange, samplingLevel);
        const auto sampledLengthX = sampledDimension.X();
        const auto sampledLengthY = sampledDimension.Y();
        const auto sampledLengthZ = sampledDimension.Z();

        const auto strideLength = static_cast<hsize_t>(LdmCore::GetSampledStep(samplingLevel));

        const auto rank = 3;
        const hsize_t offset[rank] = { offsetZ, offsetY, offsetX };
        const hsize_t count[rank] = { sampledLengthZ, sampledLengthY, sampledLengthX };
        const hsize_t stride[rank] = { strideLength, strideLength, strideLength };
        const hsize_t block[rank] = { 1, 1, 1 };

        selectedSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
        return selectedSpace;
    }
}
