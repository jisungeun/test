#pragma once
#include <memory>
#include "IReadTileOutputPort.h"

#include "TC.IO.OIV.UtilityExport.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API ReadTileOutputRgba final : public IReadTileOutputPort{
    public:
        ReadTileOutputRgba();
        ~ReadTileOutputRgba();

        auto SetTileMemory(const LdmReading::TileMemory::Pointer& tileMemory) -> void override;
        auto GetBufferMemory(uint32_t* buffer) const->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}