#pragma once
#include <memory>
#include "ITcfVolumeReadingPolicy.h"
#include "TC.IO.OIV.UtilityExport.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API TcfVolumeReadingPolicy final : public ITcfVolumeReadingPolicy {
    public:
        TcfVolumeReadingPolicy();
        ~TcfVolumeReadingPolicy();
        auto SetTcfFile(H5::H5File& tcfFile) -> void override;
        auto SetOivTcfVolumeMetaData(const OivTcfVolumeMetaData::Pointer& metaData) -> void override;
        auto GetLdmDataReader(const int32_t& tileIndex) -> LdmReading::ILdmDataReader::Pointer override;
    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    private:
        static auto GetUpperGroupPath(const TcfReadingDataType& tcfReadingDataType)->std::string;
        static auto GetTimeFrameIndexString(const int32_t& timeFrameIndex)->std::string;
    };
}