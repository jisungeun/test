#pragma once

#include "TC.IO.OIV.UtilityExport.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoSwitch.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

class TC_IO_OIV_Utility_API IOivTcfReader {
public:
	IOivTcfReader();
	virtual ~IOivTcfReader();

	virtual auto SetImageFilePath(std::string path) -> void = 0;
	virtual auto ReadTcfData(void) -> void = 0;
	virtual auto Get3DHTNode(void)->SoSwitch* = 0;
	virtual auto Get3DFLNode(int idx)->SoSwitch* = 0;
	virtual auto GetTimeSteps(void) -> int = 0;

	//Functions for single loading
	virtual auto ReadInfo(void)->void = 0;//before read specific step, read Data's Infomation first. for now only time step info.
	virtual auto ReadSingleTcfData(int idx)->void = 0;
	virtual auto GetSingle3DHTData(void)->SoVolumeData* = 0;
	virtual auto GetSingle3DFLData(int idx)->SoVolumeData* = 0;

	virtual auto IsHT3dExist(void) -> bool = 0;
	virtual auto IsFL3dExist(void) -> bool = 0;
	virtual auto IsFL3dChExist(int ch) -> bool = 0;

	virtual auto IsHT2dExist(void) -> bool = 0;
	virtual auto IsFL2dExist(void) -> bool = 0;
	virtual auto IsFL2dChExist(int ch) -> bool = 0;

	virtual auto GetSingle2DHTData(void)->SoVolumeData* = 0;
	virtual auto GetSingle2DFLData(int idx)->SoVolumeData* = 0;
};
