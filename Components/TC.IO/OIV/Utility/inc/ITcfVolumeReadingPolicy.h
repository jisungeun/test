#pragma once
#include <memory>
#include "TC.IO.OIV.UtilityExport.h"
#include "ILdmDataReader.h"
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include "OivTcfVolumeMetaData.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API ITcfVolumeReadingPolicy {
    public:
        typedef ITcfVolumeReadingPolicy Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~ITcfVolumeReadingPolicy() = default;

        virtual auto SetTcfFile(H5::H5File& tcfFile)->void = 0;
        virtual auto SetOivTcfVolumeMetaData(const OivTcfVolumeMetaData::Pointer& metaData)->void = 0;
        virtual auto GetLdmDataReader(const int32_t& tileIndex)->LdmReading::ILdmDataReader::Pointer = 0;
    };
}
