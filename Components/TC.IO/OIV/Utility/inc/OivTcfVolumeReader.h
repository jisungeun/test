#pragma once

#include <memory>
#include "TC.IO.OIV.UtilityExport.h"

#include "ITcfVolumeReadingPolicy.h"
#include "OivTcfVolumeMetaData.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <LDM/readers/SoVolumeReader.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API OivTcfVolumeReader : public SoVolumeReader{
        SO_FIELDCONTAINER_HEADER(OivTcfVolumeReader);
    public:
        OivTcfVolumeReader();
        ~OivTcfVolumeReader();

        auto SetTcfVolumeReadingPolicy(const ITcfVolumeReadingPolicy::Pointer& tcfVolumeReadingPolicy)->void;
        auto SetOivTcfVolumeMetaData(const OivTcfVolumeMetaData::Pointer& oivTcfVolumeMetaData)->void;

        auto getDataChar(SbBox3f& size, SoDataSet::DataType& type, SbVec3i32& dim)->ReadError override;
        auto getSubSlice(const SbBox2i32& subSlice, int sliceNumber, void* data)->void override;
        auto getMinMax(int64_t& min, int64_t& max)->SbBool override;
        auto getMinMax(double& min, double& max)->SbBool override;
        auto isDataConverted() const->SbBool override;
        auto readTile(int index, const SbBox3i32& tilePosition)->SoBufferObject* override;
        auto getTileSize(SbVec3i32& size)->SbBool override;
        auto isRGBA() const->SbBool override;
        auto getNumSignificantBits() -> int override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    protected:
    private:
        static auto GetVolumeDataType(const TcfReadingDataType& tcfReadingDataType)->SoVolumeData::DataType;
        auto AllocateTileSoBufferObject() const ->SoBufferObject*;
        static auto GetNumberOfTileElements(const OivTcfVolumeMetaData::Pointer& oivTcfVolumeMetaData)->size_t;
    };
}
