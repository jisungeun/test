#pragma once
#include <memory>
#include "TC.IO.OIV.UtilityExport.h"

#include "TcfReadingDataType.h"
#pragma warning(push)
#pragma warning(disable:4268)
#include "H5Cpp.h"
#pragma warning(pop)
#include "OivTcfVolumeMetaData.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API OivTcfVolumeMetaReader {
    public:
        OivTcfVolumeMetaReader();
        ~OivTcfVolumeMetaReader();

        auto SetTcfFile(H5::H5File& tcfFile)->void;
        auto SetTimeFrameIndex(const int32_t& timeFrameIndex)->void;
        auto SetReadingDataType(const TcfReadingDataType& readingDataType)->void;
        auto Read()->OivTcfVolumeMetaData::Pointer;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto IsVolumeDataOctree(const H5::H5File& tcfFile, const TcfReadingDataType& tcfReadingDataType,
            const int32_t& timeFrameIndex)->bool;
        auto GetUpperGroupPath(const TcfReadingDataType& tcfReadingDataType)->std::string;
        auto GetModalityGroup(const H5::H5File& tcfFile, const TcfReadingDataType& tcfReadingDataType)->H5::Group;
        auto GetModalityGroupPath(const TcfReadingDataType& tcfReadingDataType)->std::string;
        auto GetPixelSizes(const H5::Group& modalityGroup)->std::tuple<uint32_t, uint32_t, uint32_t>;
        static auto GetResolutions(const H5::Group& modalityGroup)->std::tuple<double, double, double>;
        auto GetTilePixelSizes(const H5::H5File& tcfFile, const TcfReadingDataType& tcfReadingDataType, 
            const int32_t& timeFrameIndex)->std::tuple<uint32_t, uint32_t, uint32_t>;
        static auto GenerateTilePixelSizes(const TcfReadingDataType& tcfReadingDataType)
            ->std::tuple<uint32_t, uint32_t, uint32_t>;
        auto GetMinMaxFromGroup(const H5::H5File& tcfFile, const TcfReadingDataType& tcfReadingDataType,
            const int32_t& timeFrameIndex)->std::tuple<double, double, bool>;
        static auto GetMinMaxAttributeName(const TcfReadingDataType& tcfReadingDataType)
        ->std::tuple<std::string, std::string>;
        auto GetMinMaxFromDataSet(const H5::H5File& tcfFile, const TcfReadingDataType& tcfReadingDataType, 
            const int32_t& timeFrameIndex)->std::tuple<double, double, bool>;
        auto GetMinMaxFromObject(const H5::H5Object& object, const TcfReadingDataType& tcfReadingDataType) const
            ->std::tuple<double, double, bool>;
    };
}
