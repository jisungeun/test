#pragma once
#include <memory>
#include "IReadTileOutputPort.h"

#include "TC.IO.OIV.UtilityExport.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API ReadTileOutputVoxel final : public IReadTileOutputPort{
    public:
        ReadTileOutputVoxel();
        ~ReadTileOutputVoxel();

        auto SetTileMemory(const LdmReading::TileMemory::Pointer& tileMemory) -> void override;
        auto GetBufferMemoryAsFloat(float* buffer) const->bool;
        auto GetBufferMemoryAsUint16(uint16_t* buffer) const->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}