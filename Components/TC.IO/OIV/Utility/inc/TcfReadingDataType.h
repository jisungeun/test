#pragma once

#include "enum.h"

namespace TC::IO::OIVTCFIO {
    BETTER_ENUM(TcfReadingDataType, int16_t, HT, HTMIP, FLBLUE, FLBLUEMIP, FLGREEN, FLGREENMIP, FLRED, FLREDMIP, 
        PHASE, BF)
}
