#pragma once
#include "TC.IO.OIV.UtilityExport.h"
#include "TileMemory.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API IReadTileOutputPort {
    public:
        typedef IReadTileOutputPort Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~IReadTileOutputPort() = default;
        virtual auto SetTileMemory(const LdmReading::TileMemory::Pointer& tileMemory)->void = 0;
    };
}
