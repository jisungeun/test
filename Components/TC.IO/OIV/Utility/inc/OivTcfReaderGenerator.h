#pragma once
#include <memory>
#include <QString>

#include "OivTcfVolumeReader.h"
#include "TC.IO.OIV.UtilityExport.h"
#include "TcfReadingDataType.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API OivTcfReaderGenerator {
    public:
        typedef OivTcfReaderGenerator Self;
        typedef std::shared_ptr<Self> Pointer;

        ~OivTcfReaderGenerator();

        static auto GetInstance()->Pointer;

        auto OpenTcf(const QString& tcfPath)->bool;
        auto CloseTcf(const QString& tcfPath)->void;

        auto Generate(const QString& tcfPath, const TcfReadingDataType& tcfReadingDataType, 
            const int32_t& timeFrameIndex)->OivTcfVolumeReader*;
    protected:
        OivTcfReaderGenerator();
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto IsTcfOpened(const QString& tcfPath) const ->bool;
    };
}
