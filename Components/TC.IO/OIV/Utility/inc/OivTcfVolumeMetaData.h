#pragma once
#include <memory>
#include "TC.IO.OIV.UtilityExport.h"
#include "TcfReadingDataType.h"
#include "enum.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API OivTcfVolumeMetaData {
    public:
        typedef OivTcfVolumeMetaData Self;
        typedef std::shared_ptr<Self> Pointer;

        OivTcfVolumeMetaData();
        ~OivTcfVolumeMetaData();

        auto SetWorldSizes(const double& micrometerX, const double& micrometerY, const double& micrometerZ)->void;
        auto SetPixelSizes(const uint32_t& pixelsX, const uint32_t& pixelsY, const uint32_t& pixelsZ)->void;
        auto SetTilePixelSizes(const uint32_t& pixelsX, const uint32_t& pixelsY, const uint32_t& pixelsZ)->void;
        auto SetMinMaxValues(const double& minValue, const double& maxValue)->void;
        auto SetTcfReadingDataType(const TcfReadingDataType& tcfReadingDataType)->void;
        auto SetTimeFrameIndex(const int32_t& timeFrameIndex)->void;
        auto SetVolumeStructure(const bool& volumeIsOctreeStructure)->void;

        auto GetWorldSizeMicrometerX() const -> double;
        auto GetWorldSizeMicrometerY() const -> double;
        auto GetWorldSizeMicrometerZ() const -> double;

        auto GetPixelSizeX() const->uint32_t;
        auto GetPixelSizeY() const->uint32_t;
        auto GetPixelSizeZ() const->uint32_t;

        auto GetTilePixelSizeX() const->uint32_t;
        auto GetTilePixelSizeY() const->uint32_t;
        auto GetTilePixelSizeZ() const->uint32_t;

        auto GetMinValue() const ->double;
        auto GetMaxValue() const ->double;
        auto IsMinMaxValid() const ->bool;

        auto GetTcfReadingDataType() const->TcfReadingDataType;

        auto GetTimeFrameIndex() const->int32_t;

        auto IsVolumeOctreeStructure() const ->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
