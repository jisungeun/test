#pragma once
#include <memory>
#include "TC.IO.OIV.UtilityExport.h"

#include "ILdmDataReader.h"
#include "IReadTileOutputPort.h"

namespace TC::IO::OIVTCFIO {
    class TC_IO_OIV_Utility_API ReadTileController {
    public:
        ReadTileController();
        ~ReadTileController();

        auto SetLdmDataReader(const LdmReading::ILdmDataReader::Pointer& ldmDataReader)->void;
        auto SetReadTileOutputPort(const IReadTileOutputPort::Pointer& readTileOutputPort)->void;

        auto Read()->void;
    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}
