project(TC.IO.OIV.Utility)

FIND_PACKAGE(HDF5 COMPONENTS CXX REQUIRED)

#Header files for external use
set(INTERFACE_HEADERS		
	inc/IOivTcfReader.h
	inc/OivTcfReaderGenerator.h
	inc/ReadTileController.h
	inc/ReadTileOutputRgba.h
	inc/ReadTileOutputVoxel.h
	inc/TcfVolumeReadingPolicy.h
	inc/OivTcfVolumeMetaData.h
	inc/OivTcfVolumeReader.h
	inc/OivTcfVolumeMetaReader.h
)
	
#Header files for internal use
set(PRIVATE_HEADERS		
	inc/IReadTileOutputPort.h
	inc/ITcfVolumeReadingPolicy.h
	inc/TcfReadingDataType.h
)

#Sources
set(SOURCES		
	src/IOivTcfReader.cpp
	src/OivTcfReaderGenerator.cpp
	src/ReadTileController.cpp	
	src/ReadTileOutputRgba.cpp	
	src/ReadTileOutputVoxel.cpp
	src/TcfVolumeReadingPolicy.cpp
	src/OivTcfVolumeMetaData.cpp
	src/OivTcfVolumeReader.cpp
	src/OivTcfVolumeMetaReader.cpp
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(TC::IO::OIV::Utility ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		$<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/Dependency/hdf5/include>
		${CURRENT_OIVHOME}/include 	
		${CURRENT_OIVHOME}/src/Inventor/gui/
        $<INSTALL_INTERFACE:inc>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_compile_features(${PROJECT_NAME} 
	PRIVATE
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PRIVATE
		$<$<COMPILE_LANGUAGE:C>:/wd4251>
		$<$<COMPILE_LANGUAGE:CXX>:/wd4251>
		$<$<COMPILE_LANGUAGE:C>:/wd4819>
		$<$<COMPILE_LANGUAGE:CXX>:/wd4819>
)

target_link_directories(${PROJECT_NAME}
	PUBLIC		
		${CURRENT_OIVHOME}/${OIVARCH_}-$(Configuration)/lib
)

target_link_libraries(${PROJECT_NAME} 
	PUBLIC
		Extern::BetterEnums
		Qt5::Core
		optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_cpp.lib
        optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5.lib
        optimized ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/libhdf5.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_cpp_D.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/hdf5_D.lib
        debug ${CMAKE_BINARY_DIR}/Dependency/hdf5/lib/libhdf5_D.lib		
		TC::Components::IO::LdmReading
	PRIVATE
		${VIEWER_COMPONENT_LINK_LIBRARY}
		Qt5::Widgets
		Qt5::Gui
		TC::Components::DataTypes
		TC::Components::ImageIO
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/IO/OIV/Utility")
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_DEFINITIONS H5_BUILT_AS_DYNAMIC_LIB=1)

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT io)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

# Install required external packages
#install(FILES ${HDF5_C_DLL} ${HDF5_CXX_DLL} DESTINATION ${CMAKE_INSTALL_BINDIR})