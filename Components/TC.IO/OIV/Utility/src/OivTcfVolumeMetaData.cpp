#include "OivTcfVolumeMetaData.h"

namespace TC::IO::OIVTCFIO {
    struct OivTcfVolumeMetaData::Impl {
        Impl() = default;
        ~Impl() = default;

        double worldSizeMicrometerX{ 0 };
        double worldSizeMicrometerY{ 0 };
        double worldSizeMicrometerZ{ 0 };

        uint32_t pixelSizeX{ 0 };
        uint32_t pixelSizeY{ 0 };
        uint32_t pixelSizeZ{ 0 };

        uint32_t tilePixelSizeX{ 0 };
        uint32_t tilePixelSizeY{ 0 };
        uint32_t tilePixelSizeZ{ 0 };

        double minValue{ 0 };
        double maxValue{ 0 };
        bool minMaxValuesValid{ false };

        TcfReadingDataType tcfReadingDataType{ TcfReadingDataType::HT };

        int32_t timeFrameIndex{ -1 };

        bool volumeIsOctreeStructure{ false };
    };

    OivTcfVolumeMetaData::OivTcfVolumeMetaData()
        : d(new Impl()) {
    }

    OivTcfVolumeMetaData::~OivTcfVolumeMetaData() = default;

    auto OivTcfVolumeMetaData::SetWorldSizes(const double& micrometerX, const double& micrometerY,
        const double& micrometerZ) -> void {
        d->worldSizeMicrometerX = micrometerX;
        d->worldSizeMicrometerY = micrometerY;
        d->worldSizeMicrometerZ = micrometerZ;
    }

    auto OivTcfVolumeMetaData::SetPixelSizes(const uint32_t& pixelsX, const uint32_t& pixelsY,
        const uint32_t& pixelsZ) -> void {
        d->pixelSizeX = pixelsX;
        d->pixelSizeY = pixelsY;
        d->pixelSizeZ = pixelsZ;
    }

    auto OivTcfVolumeMetaData::SetTilePixelSizes(const uint32_t& pixelsX, const uint32_t& pixelsY,
        const uint32_t& pixelsZ) -> void {
        d->tilePixelSizeX = pixelsX;
        d->tilePixelSizeY = pixelsY;
        d->tilePixelSizeZ = pixelsZ;
    }

    auto OivTcfVolumeMetaData::GetTilePixelSizeX() const -> uint32_t {
        return d->tilePixelSizeX;
    }

    auto OivTcfVolumeMetaData::GetTilePixelSizeY() const -> uint32_t {
        return d->tilePixelSizeY;
    }

    auto OivTcfVolumeMetaData::GetTilePixelSizeZ() const -> uint32_t {
        return d->tilePixelSizeZ;
    }

    auto OivTcfVolumeMetaData::SetMinMaxValues(const double& minValue, const double& maxValue) -> void {
        d->minValue = minValue;
        d->maxValue = maxValue;
        d->minMaxValuesValid = true;
    }

    auto OivTcfVolumeMetaData::SetTcfReadingDataType(const TcfReadingDataType& tcfReadingDataType) -> void {
        d->tcfReadingDataType = tcfReadingDataType;
    }

    auto OivTcfVolumeMetaData::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
        d->timeFrameIndex = timeFrameIndex;
    }

    auto OivTcfVolumeMetaData::SetVolumeStructure(const bool& volumeIsOctreeStructure) -> void {
        d->volumeIsOctreeStructure = volumeIsOctreeStructure;
    }

    auto OivTcfVolumeMetaData::GetWorldSizeMicrometerX() const -> double {
        return d->worldSizeMicrometerX;
    }

    auto OivTcfVolumeMetaData::GetWorldSizeMicrometerY() const -> double {
        return d->worldSizeMicrometerY;
    }

    auto OivTcfVolumeMetaData::GetWorldSizeMicrometerZ() const -> double {
        return d->worldSizeMicrometerZ;
    }

    auto OivTcfVolumeMetaData::GetPixelSizeX() const -> uint32_t {
        return d->pixelSizeX;
    }

    auto OivTcfVolumeMetaData::GetPixelSizeY() const -> uint32_t {
        return d->pixelSizeY;
    }

    auto OivTcfVolumeMetaData::GetPixelSizeZ() const -> uint32_t {
        return d->pixelSizeZ;
    }

    auto OivTcfVolumeMetaData::GetMinValue() const -> double {
        return d->minValue;
    }

    auto OivTcfVolumeMetaData::GetMaxValue() const -> double {
        return d->maxValue;
    }

    auto OivTcfVolumeMetaData::IsMinMaxValid() const -> bool {
        return d->minMaxValuesValid;
    }

    auto OivTcfVolumeMetaData::GetTcfReadingDataType() const -> TcfReadingDataType {
        return d->tcfReadingDataType;
    }

    auto OivTcfVolumeMetaData::GetTimeFrameIndex() const -> int32_t {
        return d->timeFrameIndex;
    }

    auto OivTcfVolumeMetaData::IsVolumeOctreeStructure() const -> bool {
        return d->volumeIsOctreeStructure;
    }
}
