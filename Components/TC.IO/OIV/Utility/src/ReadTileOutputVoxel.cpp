#include "ReadTileOutputVoxel.h"

namespace TC::IO::OIVTCFIO {
    struct ReadTileOutputVoxel::Impl {
        Impl() = default;
        ~Impl() = default;

        LdmReading::TileMemory::Pointer tileMemory{};
    };

    ReadTileOutputVoxel::ReadTileOutputVoxel()
        : d(new Impl()) {
    }

    ReadTileOutputVoxel::~ReadTileOutputVoxel() = default;

    auto ReadTileOutputVoxel::SetTileMemory(const LdmReading::TileMemory::Pointer& tileMemory) -> void {
        d->tileMemory = tileMemory;
    }

    auto ReadTileOutputVoxel::GetBufferMemoryAsFloat(float* buffer) const -> bool {
        const auto sizeX = static_cast<uint64_t>(d->tileMemory->GetDimensionX());
        const auto sizeY = static_cast<uint64_t>(d->tileMemory->GetDimensionY());
        const auto sizeZ = static_cast<uint64_t>(d->tileMemory->GetDimensionZ());
        const auto numberOfElements = sizeX * sizeY * sizeZ;

        const auto tileMemoryDataType = d->tileMemory->GetTileMemoryType();
        if (tileMemoryDataType == +LdmReading::TileMemoryType::INT8) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<float>(static_cast<int8_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::UINT8) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<float>(static_cast<uint8_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::INT16) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<float>(static_cast<int16_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::UINT16) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<float>(static_cast<uint16_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::INT32) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<float>(static_cast<int32_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::UINT32) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<float>(static_cast<uint32_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::FLOAT) {
            std::copy(static_cast<float*>(d->tileMemory->GetRawPointer()), 
                static_cast<float*>(d->tileMemory->GetRawPointer()) + numberOfElements, buffer);
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::DOUBLE) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<float>(static_cast<double*>(d->tileMemory->GetRawPointer())[i]);
            }
        }

        return true;
    }

    auto ReadTileOutputVoxel::GetBufferMemoryAsUint16(uint16_t* buffer) const -> bool {
        const auto sizeX = static_cast<uint64_t>(d->tileMemory->GetDimensionX());
        const auto sizeY = static_cast<uint64_t>(d->tileMemory->GetDimensionY());
        const auto sizeZ = static_cast<uint64_t>(d->tileMemory->GetDimensionZ());
        const auto numberOfElements = sizeX * sizeY * sizeZ;

        const auto tileMemoryDataType = d->tileMemory->GetTileMemoryType();
        if (tileMemoryDataType == +LdmReading::TileMemoryType::INT8) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<uint16_t>(static_cast<int8_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::UINT8) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<uint16_t>(static_cast<uint8_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::INT16) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<uint16_t>(static_cast<int16_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::UINT16) {
            std::copy(static_cast<uint16_t*>(d->tileMemory->GetRawPointer()),
                static_cast<uint16_t*>(d->tileMemory->GetRawPointer()) + numberOfElements, buffer);
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::INT32) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<uint16_t>(static_cast<int32_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::UINT32) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<uint16_t>(static_cast<uint32_t*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::FLOAT) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<uint16_t>(static_cast<float*>(d->tileMemory->GetRawPointer())[i]);
            }
        } else if (tileMemoryDataType == +LdmReading::TileMemoryType::DOUBLE) {
            for (uint64_t i = 0; i < static_cast<uint64_t>(numberOfElements); ++i) {
                buffer[i] = static_cast<uint16_t>(static_cast<double*>(d->tileMemory->GetRawPointer())[i]);
            }
        }

        return true;
    }
}
