#include "OivTcfReaderGenerator.h"

#include <QMap>
#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)
#include "OivTcfVolumeMetaReader.h"
#include "TcfVolumeReadingPolicy.h"

namespace TC::IO::OIVTCFIO {
    struct OivTcfReaderGenerator::Impl {
        Impl() = default;
        ~Impl() {
            try {
                for (auto iterator = openedTcfMap.begin(); iterator != openedTcfMap.end(); ++iterator) {
                    auto& filePointer = iterator.value();
                    filePointer->close();
                    delete filePointer;
                }
                openedTcfMap.clear();
            } catch(...) {
            }
        }

        QMap<QString, H5::H5File*> openedTcfMap;
    };

    OivTcfReaderGenerator::~OivTcfReaderGenerator() = default;

    auto OivTcfReaderGenerator::GetInstance() -> Pointer {
        static Pointer instance{ new OivTcfReaderGenerator };
        return instance;
    }

    auto OivTcfReaderGenerator::OpenTcf(const QString& tcfPath) -> bool {
        const auto tcfIsNotOpened = !IsTcfOpened(tcfPath);

        auto openIsSucceed{ true };
        if (tcfIsNotOpened) {
            try {
                const auto tcfPathString = std::string{ tcfPath.toLocal8Bit().constData() };
                const auto h5File = new H5::H5File(tcfPathString, H5F_ACC_RDONLY);
                d->openedTcfMap[tcfPath] = h5File;
            } catch (const H5::Exception&) {
                openIsSucceed = false;
            }
        }

        return openIsSucceed;
    }

    auto OivTcfReaderGenerator::CloseTcf(const QString& tcfPath) -> void {
        const auto tcfIsAlreadyOpened = IsTcfOpened(tcfPath);
        if (tcfIsAlreadyOpened) {
            const auto h5File = d->openedTcfMap[tcfPath];
            h5File->close();
            d->openedTcfMap.remove(tcfPath);
            delete h5File;
        }
    }

    auto OivTcfReaderGenerator::Generate(const QString& tcfPath, const TcfReadingDataType& tcfReadingDataType,
        const int32_t& timeFrameIndex) -> OivTcfVolumeReader* {
        OpenTcf(tcfPath);

        const auto tcfFile = d->openedTcfMap[tcfPath];

        OivTcfVolumeMetaReader metaReader;
        metaReader.SetTcfFile(*tcfFile);
        metaReader.SetReadingDataType(tcfReadingDataType);
        metaReader.SetTimeFrameIndex(timeFrameIndex);

        const auto metaData = metaReader.Read();

        auto volumeReadingPolicy = new TcfVolumeReadingPolicy;
        volumeReadingPolicy->SetTcfFile(*tcfFile);
        volumeReadingPolicy->SetOivTcfVolumeMetaData(metaData);

        auto reader = new OivTcfVolumeReader;
        reader->SetOivTcfVolumeMetaData(metaData);
        reader->SetTcfVolumeReadingPolicy(ITcfVolumeReadingPolicy::Pointer{ volumeReadingPolicy });

        return reader;
    }

    OivTcfReaderGenerator::OivTcfReaderGenerator()
        : d(new Impl()) {
    }

    auto OivTcfReaderGenerator::IsTcfOpened(const QString& tcfPath) const -> bool {
        const auto tcfIsOpened = d->openedTcfMap.find(tcfPath) != d->openedTcfMap.end();
        return tcfIsOpened;
    }
}
