#include "ReadTileController.h"
#include "TileMemoryPresenter.h"
#include "LdmDataReadingAlgorithm.h"

namespace TC::IO::OIVTCFIO {
    struct ReadTileController::Impl {
        Impl() = default;
        ~Impl() = default;

        LdmReading::ILdmDataReader::Pointer ldmDataReader{ nullptr };
        IReadTileOutputPort::Pointer readTileOutputPort{ nullptr };
    };

    ReadTileController::ReadTileController()
        : d(new Impl()) {
    }

    ReadTileController::~ReadTileController() = default;

    auto ReadTileController::SetLdmDataReader(const LdmReading::ILdmDataReader::Pointer& ldmDataReader) -> void {
        d->ldmDataReader = ldmDataReader;
    }

    auto ReadTileController::SetReadTileOutputPort(const IReadTileOutputPort::Pointer& readTileOutputPort) -> void {
        d->readTileOutputPort = readTileOutputPort;
    }

    auto ReadTileController::Read() -> void {
        const auto tileMemoryPresenter(new LdmReading::TileMemoryPresenter);
        LdmReading::ILdmDataOutputPort::Pointer ldmDataOutputPort(tileMemoryPresenter);

        LdmReading::LdmDataReadingAlgorithm ldmDataReadingAlgorithm;
        ldmDataReadingAlgorithm.ReadData(d->ldmDataReader, ldmDataOutputPort);

        const auto tileMemory = tileMemoryPresenter->GetTileMemory();
        const LdmReading::TileMemory::Pointer tileMemoryPointer(new LdmReading::TileMemory{ tileMemory });
        d->readTileOutputPort->SetTileMemory(tileMemoryPointer);
    }
}
