#include "TcfVolumeReadingPolicy.h"

#include "TcfH5GroupTileLdmDataReader.h"
#include "Hdf5DataSet3DTileLdmDataReader.h"
#include "Hdf5DataSet2DTileLdmDataReader.h"
#include "Hdf5DataSet2DStackTileLdmDataReader.h"

namespace TC::IO::OIVTCFIO {
    struct TcfVolumeReadingPolicy::Impl {
        Impl() = default;
        ~Impl() {
            if (tcfFile != nullptr) {
                delete tcfFile;
            }
        }

        H5::H5File* tcfFile{ nullptr };
        OivTcfVolumeMetaData::Pointer metaData{ nullptr };
    };

    TcfVolumeReadingPolicy::TcfVolumeReadingPolicy()
        : d(new Impl()) {
    }

    TcfVolumeReadingPolicy::~TcfVolumeReadingPolicy() = default;

    auto TcfVolumeReadingPolicy::SetTcfFile(H5::H5File& tcfFile) -> void {
        if (d->tcfFile != nullptr) {
            delete d->tcfFile;
        }
        d->tcfFile = new H5::H5File(tcfFile.getId());
    }

    auto TcfVolumeReadingPolicy::SetOivTcfVolumeMetaData(const OivTcfVolumeMetaData::Pointer& metaData) -> void {
        d->metaData = metaData;
    }

    auto TcfVolumeReadingPolicy::GetLdmDataReader(const int32_t& tileIndex) -> LdmReading::ILdmDataReader::Pointer {
        const auto tcfReadingDataType = d->metaData->GetTcfReadingDataType();
        const auto timeFrameIndex = d->metaData->GetTimeFrameIndex();

        const auto dataIsOctreeStructure = d->metaData->IsVolumeOctreeStructure();

        LdmReading::ILdmDataReader::Pointer ldmDataReader{};

        const auto upperGroupPath = GetUpperGroupPath(tcfReadingDataType);
        const auto timeFrameIndexString = GetTimeFrameIndexString(timeFrameIndex);

        auto upperGroup = d->tcfFile->openGroup(upperGroupPath);

        if (dataIsOctreeStructure) {
            auto ldmVolumeGroup = upperGroup.openGroup(timeFrameIndexString);

            auto tcfH5GroupTileLdmReader = new LdmReading::TcfH5GroupTileLdmDataReader;
            tcfH5GroupTileLdmReader->SetDataGroup(ldmVolumeGroup);
            tcfH5GroupTileLdmReader->SetReadingTileIndex(tileIndex);

            ldmDataReader = LdmReading::ILdmDataReader::Pointer(tcfH5GroupTileLdmReader);
        } else {
            const auto tilePixelSizeX = d->metaData->GetTilePixelSizeX();
            const auto tilePixelSizeY = d->metaData->GetTilePixelSizeY();
            const auto tilePixelSizeZ = d->metaData->GetTilePixelSizeZ();

            auto ldmVolumeDataSet = upperGroup.openDataSet(timeFrameIndexString);

            const auto typeIs3d =
                tcfReadingDataType == +TcfReadingDataType::HT ||
                tcfReadingDataType == +TcfReadingDataType::FLBLUE ||
                tcfReadingDataType == +TcfReadingDataType::FLGREEN ||
                tcfReadingDataType == +TcfReadingDataType::FLRED;

            if (typeIs3d) {
                auto hdf5DataSet3DTileLdmDataReader = new LdmReading::Hdf5DataSet3DTileLdmDataReader;
                hdf5DataSet3DTileLdmDataReader->SetTargetDataSet(ldmVolumeDataSet);
                hdf5DataSet3DTileLdmDataReader->SetTileUnitDimension(
                    Dimension{ tilePixelSizeX,tilePixelSizeY,tilePixelSizeZ });
                hdf5DataSet3DTileLdmDataReader->SetReadingTileIndex(tileIndex);

                ldmDataReader = LdmReading::ILdmDataReader::Pointer(hdf5DataSet3DTileLdmDataReader);
            }

            const auto typeIs2d =
                tcfReadingDataType == +TcfReadingDataType::HTMIP ||
                tcfReadingDataType == +TcfReadingDataType::FLBLUEMIP ||
                tcfReadingDataType == +TcfReadingDataType::FLGREENMIP ||
                tcfReadingDataType == +TcfReadingDataType::FLREDMIP;

            if (typeIs2d) {
                auto hdf5DataSet2DTileLdmDataReader = new LdmReading::Hdf5DataSet2DTileLdmDataReader;
                hdf5DataSet2DTileLdmDataReader->SetTargetDataSet(ldmVolumeDataSet);
                hdf5DataSet2DTileLdmDataReader->SetTileUnitDimension(
                    Dimension{ tilePixelSizeX,tilePixelSizeY });
                hdf5DataSet2DTileLdmDataReader->SetReadingTileIndex(tileIndex);

                ldmDataReader = LdmReading::ILdmDataReader::Pointer(hdf5DataSet2DTileLdmDataReader);
            }

            const auto typeIs2dStack =
                tcfReadingDataType == +TcfReadingDataType::PHASE ||
                tcfReadingDataType == +TcfReadingDataType::BF;

            if (typeIs2dStack) {
                auto hdf5DataSet2DStackTileLdmDataReader = new LdmReading::Hdf5DataSet2DStackTileLdmDataReader;
                hdf5DataSet2DStackTileLdmDataReader->SetTargetDataSet(ldmVolumeDataSet);
                hdf5DataSet2DStackTileLdmDataReader->SetTileUnitDimension(
                    Dimension{ tilePixelSizeX,tilePixelSizeY });
                hdf5DataSet2DStackTileLdmDataReader->SetReadingTileIndex(tileIndex);

                ldmDataReader = LdmReading::ILdmDataReader::Pointer(hdf5DataSet2DStackTileLdmDataReader);
            }
        }

        upperGroup.close();

        return ldmDataReader;
    }

    auto TcfVolumeReadingPolicy::GetUpperGroupPath(const TcfReadingDataType& tcfReadingDataType) -> std::string {
        if (tcfReadingDataType == +TcfReadingDataType::HT) {
            return "/Data/3D";
        } else if (tcfReadingDataType == +TcfReadingDataType::HTMIP) {
            return "/Data/2DMIP";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLBLUE) {
            return "/Data/3DFL/CH0";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLBLUEMIP) {
            return "/Data/2DFLMIP/CH0";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLGREEN) {
            return "/Data/3DFL/CH1";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLGREENMIP) {
            return "/Data/2DFLMIP/CH1";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLRED) {
            return "/Data/3DFL/CH2";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLREDMIP) {
            return "/Data/2DFLMIP/CH2";
        } else if (tcfReadingDataType == +TcfReadingDataType::PHASE) {
            return "/Data/2D";
        } else if (tcfReadingDataType == +TcfReadingDataType::BF) {
            return "/Data/BF";
        } else {
            return "";
        }
    }

    auto TcfVolumeReadingPolicy::GetTimeFrameIndexString(const int32_t& timeFrameIndex)->std::string {
        const auto timeFrameString = QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));
        const auto timeFrameStdString = std::string{ timeFrameString.toLocal8Bit().constData() };
        return timeFrameStdString;
    }
}
