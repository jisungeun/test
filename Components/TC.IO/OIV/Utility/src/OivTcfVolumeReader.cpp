#include "OivTcfVolumeReader.h"

#include "ReadTileController.h"
#include "ReadTileOutputRgba.h"
#include "ReadTileOutputVoxel.h"

namespace TC::IO::OIVTCFIO {
    SO_FIELDCONTAINER_SOURCE(OivTcfVolumeReader);

    struct OivTcfVolumeReader::Impl {
        Impl() = default;
        ~Impl() = default;

        ITcfVolumeReadingPolicy::Pointer tcfVolumeReadingPolicy{ nullptr };
        OivTcfVolumeMetaData::Pointer oivTcfVolumeMetaData{ nullptr };

        SoDataSet::DataType soDataType;
        size_t numberOfTileElements{};
    };

    OivTcfVolumeReader::OivTcfVolumeReader()
        : d(new Impl()) {
        SO_FIELDCONTAINER_CONSTRUCTOR(OivTcfVolumeReader);
    }

    auto OivTcfVolumeReader::SetTcfVolumeReadingPolicy(
        const ITcfVolumeReadingPolicy::Pointer& tcfVolumeReadingPolicy)-> void {
        d->tcfVolumeReadingPolicy = tcfVolumeReadingPolicy;
    }

    auto OivTcfVolumeReader::SetOivTcfVolumeMetaData(const OivTcfVolumeMetaData::Pointer& oivTcfVolumeMetaData)
        -> void {
        d->oivTcfVolumeMetaData = oivTcfVolumeMetaData;
        d->soDataType = GetVolumeDataType(d->oivTcfVolumeMetaData->GetTcfReadingDataType());
        d->numberOfTileElements = GetNumberOfTileElements(d->oivTcfVolumeMetaData);
    }

    auto OivTcfVolumeReader::getDataChar(SbBox3f& size, SoDataSet::DataType& type, SbVec3i32& dim) -> ReadError {
        const auto lengthMicrometerX = static_cast<float>(d->oivTcfVolumeMetaData->GetWorldSizeMicrometerX());
        const auto lengthMicrometerY = static_cast<float>(d->oivTcfVolumeMetaData->GetWorldSizeMicrometerY());
        const auto lengthMicrometerZ = static_cast<float>(d->oivTcfVolumeMetaData->GetWorldSizeMicrometerZ());

        const auto pixelSizeX = static_cast<int32_t>(d->oivTcfVolumeMetaData->GetPixelSizeX());
        const auto pixelSizeY = static_cast<int32_t>(d->oivTcfVolumeMetaData->GetPixelSizeY());
        const auto pixelSizeZ = static_cast<int32_t>(d->oivTcfVolumeMetaData->GetPixelSizeZ());

        size = SbBox3f(SbVec3f(0.f, 0.f, 0.f), SbVec3f(lengthMicrometerX, lengthMicrometerY, lengthMicrometerZ));
        type = d->soDataType;
        dim = SbVec3i32(pixelSizeX, pixelSizeY, pixelSizeZ);

        return RD_NO_ERROR;
    }

    auto OivTcfVolumeReader::getSubSlice(const SbBox2i32& , int , void* ) -> void {
        // This will be not Implemented. Instead, readTile should be implemented.
    }

    auto OivTcfVolumeReader::getMinMax(int64_t& , int64_t& ) -> SbBool {
        // use getMinMax(double, double) instead of this method
        return FALSE;
    }

    auto OivTcfVolumeReader::getMinMax(double& min, double& max)->SbBool {
        if (d->oivTcfVolumeMetaData->IsMinMaxValid()) {
            min = d->oivTcfVolumeMetaData->GetMinValue();
            max = d->oivTcfVolumeMetaData->GetMaxValue();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    auto OivTcfVolumeReader::isDataConverted() const -> SbBool {
        // this must return true to use readTile()
        return TRUE;
    }

    auto OivTcfVolumeReader::readTile(int index, const SbBox3i32& ) -> SoBufferObject* {
        auto tileBufferObject = AllocateTileSoBufferObject();
        const auto tileBuffer = tileBufferObject->map(SoBufferObject::SET);

        d->tcfVolumeReadingPolicy->SetOivTcfVolumeMetaData(d->oivTcfVolumeMetaData);
        const auto ldmDataReader = d->tcfVolumeReadingPolicy->GetLdmDataReader(index);

        ReadTileController readTileController;
        readTileController.SetLdmDataReader(ldmDataReader);

        if (d->oivTcfVolumeMetaData->GetTcfReadingDataType() == +TcfReadingDataType::BF) {
            const auto readTileOutputRgba = new ReadTileOutputRgba;
            const IReadTileOutputPort::Pointer readTileOutputPort(readTileOutputRgba);
            
            readTileController.SetReadTileOutputPort(readTileOutputPort);
            readTileController.Read();

            readTileOutputRgba->GetBufferMemory(reinterpret_cast<uint32_t*>(tileBuffer));
        } else {
            const auto readTileOutputVoxel = new ReadTileOutputVoxel;
            const IReadTileOutputPort::Pointer readTileOutputPort(readTileOutputVoxel);

            readTileController.SetReadTileOutputPort(readTileOutputPort);
            readTileController.Read();

            if (d->oivTcfVolumeMetaData->GetTcfReadingDataType() == +TcfReadingDataType::PHASE) {
                readTileOutputVoxel->GetBufferMemoryAsFloat(reinterpret_cast<float*>(tileBuffer));
            } else {
                readTileOutputVoxel->GetBufferMemoryAsUint16(reinterpret_cast<uint16_t*>(tileBuffer));
            }
        }

        tileBufferObject->unmap();
        return tileBufferObject;
    }

    auto OivTcfVolumeReader::getTileSize(SbVec3i32& size) -> SbBool {
        const auto tileSizeX = static_cast<int32_t>(d->oivTcfVolumeMetaData->GetTilePixelSizeX());
        const auto tileSizeY = static_cast<int32_t>(d->oivTcfVolumeMetaData->GetTilePixelSizeY());
        const auto tileSizeZ = static_cast<int32_t>(d->oivTcfVolumeMetaData->GetTilePixelSizeZ());

        size = SbVec3i32(tileSizeX, tileSizeY, tileSizeZ);

        return TRUE;
    }

    auto OivTcfVolumeReader::isRGBA() const -> SbBool {
        if (d->oivTcfVolumeMetaData->GetTcfReadingDataType() == +TcfReadingDataType::BF) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    auto OivTcfVolumeReader::getNumSignificantBits() -> int {
        const auto tcfReadingDataType = d->oivTcfVolumeMetaData->GetTcfReadingDataType();

        const auto readingIsHT =
            (tcfReadingDataType == +TcfReadingDataType::HT) || (tcfReadingDataType == +TcfReadingDataType::HTMIP);
        if (readingIsHT) {
            return 16;
        }

        const auto readingIsFL =
            (tcfReadingDataType == +TcfReadingDataType::FLBLUE) ||
            (tcfReadingDataType == +TcfReadingDataType::FLBLUEMIP) ||
            (tcfReadingDataType == +TcfReadingDataType::FLGREEN) ||
            (tcfReadingDataType == +TcfReadingDataType::FLGREENMIP) ||
            (tcfReadingDataType == +TcfReadingDataType::FLRED) ||
            (tcfReadingDataType == +TcfReadingDataType::FLREDMIP);
        if (readingIsFL) {
            return 16;
        }

        const auto readingIsBF = (tcfReadingDataType == +TcfReadingDataType::BF);
        if (readingIsBF) {
            return 32;
        }
        const auto readingIsPhase = (tcfReadingDataType == +TcfReadingDataType::PHASE);
        if (readingIsPhase) {
            return 32;
        }
        return 32;
    }

    OivTcfVolumeReader::~OivTcfVolumeReader() = default;

    auto OivTcfVolumeReader::GetVolumeDataType(const TcfReadingDataType& tcfReadingDataType)
        -> SoVolumeData::DataType {
        const auto readingIsHT =
            (tcfReadingDataType == +TcfReadingDataType::HT) || (tcfReadingDataType == +TcfReadingDataType::HTMIP);
        if (readingIsHT) {
            return SoDataSet::DataType::UNSIGNED_SHORT;
        }

        const auto readingIsFL =
            (tcfReadingDataType == +TcfReadingDataType::FLBLUE) ||
            (tcfReadingDataType == +TcfReadingDataType::FLBLUEMIP) ||
            (tcfReadingDataType == +TcfReadingDataType::FLGREEN) ||
            (tcfReadingDataType == +TcfReadingDataType::FLGREENMIP) ||
            (tcfReadingDataType == +TcfReadingDataType::FLRED) ||
            (tcfReadingDataType == +TcfReadingDataType::FLREDMIP);
        if (readingIsFL) {
            return SoDataSet::DataType::UNSIGNED_SHORT;
        }

        const auto readingIsBF = (tcfReadingDataType == +TcfReadingDataType::BF);
        if (readingIsBF) {
            return SoDataSet::DataType::UNSIGNED_INT32;
        }
        const auto readingIsPhase = (tcfReadingDataType == +TcfReadingDataType::PHASE);
        if (readingIsPhase) {
            return SoDataSet::DataType::FLOAT;
        }

        return SoDataSet::DataType::UNSIGNED_SHORT;
    }

    auto OivTcfVolumeReader::AllocateTileSoBufferObject() const -> SoBufferObject* {
        auto tileBufferObject = new SoCpuBufferObject;

        const auto voxelBytes = static_cast<size_t>(SoDataSet::dataSize(d->soDataType));
        const auto bufferSize = voxelBytes * d->numberOfTileElements;
        tileBufferObject->setSize(bufferSize);

        return tileBufferObject;
    }

    auto OivTcfVolumeReader::GetNumberOfTileElements(const OivTcfVolumeMetaData::Pointer& oivTcfVolumeMetaData)
        -> size_t {
        const auto tilePixelSizeX = static_cast<size_t>(oivTcfVolumeMetaData->GetTilePixelSizeX());
        const auto tilePixelSizeY = static_cast<size_t>(oivTcfVolumeMetaData->GetTilePixelSizeY());
        const auto tilePixelSizeZ = static_cast<size_t>(oivTcfVolumeMetaData->GetTilePixelSizeZ());

        return tilePixelSizeX * tilePixelSizeY * tilePixelSizeZ;
    }

    auto OivTcfVolumeReader::initClass() -> void {
        SO_FIELDCONTAINER_INIT_CLASS(OivTcfVolumeReader, "OivTcfVolumeReader", SoVolumeReader);
    }

    auto OivTcfVolumeReader::exitClass() -> void {
        SO__FIELDCONTAINER_EXIT_CLASS(OivTcfVolumeReader);
    }
}
