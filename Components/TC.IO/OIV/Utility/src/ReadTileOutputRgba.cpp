#include "ReadTileOutputRgba.h"

namespace TC::IO::OIVTCFIO {
    struct ReadTileOutputRgba::Impl {
        Impl() = default;
        ~Impl() = default;

        LdmReading::TileMemory::Pointer tileMemory{};
    };

    ReadTileOutputRgba::ReadTileOutputRgba()
        : d(new Impl()) {
    }

    ReadTileOutputRgba::~ReadTileOutputRgba() = default;

    auto ReadTileOutputRgba::SetTileMemory(const LdmReading::TileMemory::Pointer& tileMemory) -> void {
        d->tileMemory = tileMemory;
    }

    auto ReadTileOutputRgba::GetBufferMemory(uint32_t* buffer) const -> bool {
        // tileMemory is rrrrrr....gggggg....bbbbb
        // convert to rgba rgba .... rgba

        const auto sizeX = static_cast<uint64_t>(d->tileMemory->GetDimensionX());
        const auto sizeY = static_cast<uint64_t>(d->tileMemory->GetDimensionY());
        const auto sizeZ = static_cast<uint64_t>(d->tileMemory->GetDimensionZ());

        if (sizeZ != 3) {
            return false;
        }
        const auto numberOfElements = sizeX * sizeY;

        const auto tileMemoryDataType = d->tileMemory->GetTileMemoryType();
        if (tileMemoryDataType == +LdmReading::TileMemoryType::UINT8) {
            const auto tileMemoryPointer = static_cast<uint8_t*>(d->tileMemory->GetRawPointer());

            const uint8_t valueOfAlpha = 255;
            for (uint64_t indexOfElements = 0; indexOfElements < static_cast<uint64_t>(numberOfElements); 
                ++indexOfElements) {
                const auto uint8CastBufferMemory = reinterpret_cast<uint8_t*>(buffer);

                const auto indexOfTileRed = indexOfElements;
                const auto indexOfTileGreen = indexOfTileRed + numberOfElements;
                const auto indexOfTileBlue = indexOfTileGreen + numberOfElements;

                const auto indexOfBufferRed = indexOfElements * 4;
                const auto indexOfBufferGreen = indexOfBufferRed + 1;
                const auto indexOfBufferBlue = indexOfBufferGreen + 1;
                const auto indexOfBufferAlpha = indexOfBufferBlue + 1;

                uint8CastBufferMemory[indexOfBufferRed] = tileMemoryPointer[indexOfTileRed];
                uint8CastBufferMemory[indexOfBufferGreen] = tileMemoryPointer[indexOfTileGreen];
                uint8CastBufferMemory[indexOfBufferBlue] = tileMemoryPointer[indexOfTileBlue];
                uint8CastBufferMemory[indexOfBufferAlpha] = valueOfAlpha;
            }
        } else {
            return false;
        }

        return true;
    }
}
