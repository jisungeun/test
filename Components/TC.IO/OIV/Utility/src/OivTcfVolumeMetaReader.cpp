#include <HDF5Mutex.h>

#include "OivTcfVolumeMetaReader.h"
#include <QString>

namespace TC::IO::OIVTCFIO {
    static const int32_t basicTileSizeX = 256;
    static const int32_t basicTileSizeY = 256;
    static const int32_t basicTileSizeZ = 256;

    struct OivTcfVolumeMetaReader::Impl {
        Impl() = default;
        ~Impl() {
            delete tcfFile;
        }
        H5::H5File* tcfFile{ nullptr };
        int32_t timeFrameIndex{-1};
        TcfReadingDataType tcfReadingDataType{ TcfReadingDataType::HT };
    };

    OivTcfVolumeMetaReader::OivTcfVolumeMetaReader()
        : d(new Impl()) {
    }

    OivTcfVolumeMetaReader::~OivTcfVolumeMetaReader() = default;

    auto OivTcfVolumeMetaReader::SetTcfFile(H5::H5File& tcfFile) -> void {
        delete d->tcfFile;
        d->tcfFile = new H5::H5File(tcfFile.getId());
    }

    auto OivTcfVolumeMetaReader::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
        d->timeFrameIndex = timeFrameIndex;
    }

    auto OivTcfVolumeMetaReader::SetReadingDataType(const TcfReadingDataType& readingDataType) -> void {
        d->tcfReadingDataType = readingDataType;
    }

    auto OivTcfVolumeMetaReader::Read() -> OivTcfVolumeMetaData::Pointer {
        HDF5MutexLocker lock(HDF5Mutex::GetInstance());
        OivTcfVolumeMetaData::Pointer oivTcfVolumeMetaData(new OivTcfVolumeMetaData);
        oivTcfVolumeMetaData->SetTcfReadingDataType(d->tcfReadingDataType);
        oivTcfVolumeMetaData->SetTimeFrameIndex(d->timeFrameIndex);

        auto modalityGroup = GetModalityGroup(*d->tcfFile, d->tcfReadingDataType);
        {
            const auto pixelSizes = GetPixelSizes(modalityGroup);
            const auto resolutions = GetResolutions(modalityGroup);

            const auto pixelSizeX = std::get<0>(pixelSizes);
            const auto pixelSizeY = std::get<1>(pixelSizes);
            const auto pixelSizeZ = std::get<2>(pixelSizes);

            const auto resolutionX = std::get<0>(resolutions);
            const auto resolutionY = std::get<1>(resolutions);
            const auto resolutionZ = std::get<2>(resolutions);

            const auto worldSizeX = static_cast<double>(pixelSizeX)* resolutionX;
            const auto worldSizeY = static_cast<double>(pixelSizeY)* resolutionY;
            const auto worldSizeZ = static_cast<double>(pixelSizeZ)* resolutionZ;

            oivTcfVolumeMetaData->SetWorldSizes(worldSizeX, worldSizeY, worldSizeZ);
            oivTcfVolumeMetaData->SetPixelSizes(pixelSizeX, pixelSizeY, pixelSizeZ);
        }

        std::tuple<uint32_t, uint32_t, uint32_t> tilePixelSizes;
        std::tuple<double, double, bool> minMaxValidValues;
        {
            if (IsVolumeDataOctree(*d->tcfFile, d->tcfReadingDataType, d->timeFrameIndex)) {
                oivTcfVolumeMetaData->SetVolumeStructure(true);
                tilePixelSizes = GetTilePixelSizes(*d->tcfFile, d->tcfReadingDataType, d->timeFrameIndex);
                minMaxValidValues = GetMinMaxFromGroup(*d->tcfFile, d->tcfReadingDataType, d->timeFrameIndex);
            } else {
                oivTcfVolumeMetaData->SetVolumeStructure(false);
                tilePixelSizes = GenerateTilePixelSizes(d->tcfReadingDataType);
                minMaxValidValues = GetMinMaxFromDataSet(*d->tcfFile, d->tcfReadingDataType, d->timeFrameIndex);
            }
        }

        {
            const auto tilePixelSizeX = std::get<0>(tilePixelSizes);
            const auto tilePixelSizeY = std::get<1>(tilePixelSizes);
            const auto tilePixelSizeZ = std::get<2>(tilePixelSizes);
            oivTcfVolumeMetaData->SetTilePixelSizes(tilePixelSizeX, tilePixelSizeY, tilePixelSizeZ);
        }

        {
            const auto minValue = std::get<0>(minMaxValidValues);
            const auto maxValue = std::get<1>(minMaxValidValues);
            const auto minMaxValid = std::get<2>(minMaxValidValues);
            if (minMaxValid) {
                oivTcfVolumeMetaData->SetMinMaxValues(minValue, maxValue);
            }
        }

        modalityGroup.close();

        return oivTcfVolumeMetaData;
    }

    auto OivTcfVolumeMetaReader::IsVolumeDataOctree(const H5::H5File& tcfFile,
        const TcfReadingDataType& tcfReadingDataType, const int32_t& timeFrameIndex) -> bool {
        const auto upperGroupPath = GetUpperGroupPath(tcfReadingDataType);
        auto upperGroup = tcfFile.openGroup(upperGroupPath);

        const auto timeFrameString = QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));
        const auto timeFrameStdString = std::string{ timeFrameString.toLocal8Bit().constData() };

        auto volumeDataIsOctree{ false };

        const auto numberOfObject = upperGroup.getNumObjs();
        for (hsize_t i = 0; i < numberOfObject; ++i) {
            if (upperGroup.getObjnameByIdx(i) == timeFrameStdString) {
                const auto objectIsGroup = (upperGroup.getObjTypeByIdx(i) == H5G_GROUP);
                if (objectIsGroup) {
                    volumeDataIsOctree = true;
                    break;
                }

                const auto objectIsDataSet = (upperGroup.getObjTypeByIdx(i) == H5G_DATASET);
                if (objectIsDataSet) {
                    volumeDataIsOctree = false;
                    break;
                }
            }
        }
        upperGroup.close();

        return volumeDataIsOctree;
    }

    auto OivTcfVolumeMetaReader::GetUpperGroupPath(const TcfReadingDataType& tcfReadingDataType) -> std::string {
        if (tcfReadingDataType == +TcfReadingDataType::HT) {
            return "/Data/3D";
        } else if (tcfReadingDataType == +TcfReadingDataType::HTMIP) {
            return "/Data/2DMIP";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLBLUE) {
            return "/Data/3DFL/CH0";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLBLUEMIP) {
            return "/Data/2DFLMIP/CH0";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLGREEN) {
            return "/Data/3DFL/CH1";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLGREENMIP) {
            return "/Data/2DFLMIP/CH1";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLRED) {
            return "/Data/3DFL/CH2";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLREDMIP) {
            return "/Data/2DFLMIP/CH2";
        } else if (tcfReadingDataType == +TcfReadingDataType::PHASE) {
            return "/Data/2D";
        } else if (tcfReadingDataType == +TcfReadingDataType::BF) {
            return "/Data/BF";
        } else {
            return "";
        }
    }

    auto OivTcfVolumeMetaReader::GetModalityGroup(const H5::H5File& tcfFile,
        const TcfReadingDataType& tcfReadingDataType)-> H5::Group {
        const auto modalityGroupPath = GetModalityGroupPath(tcfReadingDataType);
        auto modalityGroup = tcfFile.openGroup(modalityGroupPath);
        return modalityGroup;
    }

    auto OivTcfVolumeMetaReader::GetModalityGroupPath(const TcfReadingDataType& tcfReadingDataType) -> std::string {
        if (tcfReadingDataType == +TcfReadingDataType::HT) {
            return "/Data/3D";
        } else if (tcfReadingDataType == +TcfReadingDataType::HTMIP) {
            return "/Data/2DMIP";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLBLUE) {
            return "/Data/3DFL";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLBLUEMIP) {
            return "/Data/2DFLMIP";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLGREEN) {
            return "/Data/3DFL";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLGREENMIP) {
            return "/Data/2DFLMIP";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLRED) {
            return "/Data/3DFL";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLREDMIP) {
            return "/Data/2DFLMIP";
        } else if (tcfReadingDataType == +TcfReadingDataType::PHASE) {
            return "/Data/2D";
        } else if (tcfReadingDataType == +TcfReadingDataType::BF) {
            return "/Data/BF";
        } else {
            return "";
        }
    }

    auto OivTcfVolumeMetaReader::GetPixelSizes(const H5::Group& modalityGroup)
        -> std::tuple<uint32_t, uint32_t, uint32_t> {
        int64_t pixelSizeX{ 0 };
        int64_t pixelSizeY{ 0 };
        int64_t pixelSizeZ{ 0 };

        if (modalityGroup.attrExists("SizeX")) {
            auto sizeXAttribute = modalityGroup.openAttribute("SizeX");
            sizeXAttribute.read(sizeXAttribute.getDataType(), &pixelSizeX);
            sizeXAttribute.close();
        }

        if (modalityGroup.attrExists("SizeY")) {
            auto sizeYAttribute = modalityGroup.openAttribute("SizeY");
            sizeYAttribute.read(sizeYAttribute.getDataType(), &pixelSizeY);
            sizeYAttribute.close();
        }

        if (modalityGroup.attrExists("SizeZ")) {
            auto sizeZAttribute = modalityGroup.openAttribute("SizeZ");
            sizeZAttribute.read(sizeZAttribute.getDataType(), &pixelSizeZ);
            sizeZAttribute.close();
        }

        if (pixelSizeZ == 0) {
            pixelSizeZ = 1;
        }

        return std::make_tuple(static_cast<uint32_t>(pixelSizeX), static_cast<uint32_t>(pixelSizeY), 
            static_cast<uint32_t>(pixelSizeZ));
    }

    auto OivTcfVolumeMetaReader::GetResolutions(const H5::Group& modalityGroup) -> std::tuple<double, double, double> {
        double resolutionX{ 1 };
        double resolutionY{ 1 };
        double resolutionZ{ 1 };

        if (modalityGroup.attrExists("ResolutionX")) {
            auto resolutionXAttribute = modalityGroup.openAttribute("ResolutionX");
            resolutionXAttribute.read(resolutionXAttribute.getDataType(), &resolutionX);
            resolutionXAttribute.close();
        }

        if (modalityGroup.attrExists("ResolutionY")) {
            auto resolutionYAttribute = modalityGroup.openAttribute("ResolutionY");
            resolutionYAttribute.read(resolutionYAttribute.getDataType(), &resolutionY);
            resolutionYAttribute.close();
        }

        if (modalityGroup.attrExists("ResolutionZ")) {
            auto resolutionZAttribute = modalityGroup.openAttribute("ResolutionZ");
            resolutionZAttribute.read(resolutionZAttribute.getDataType(), &resolutionZ);
            resolutionZAttribute.close();
        }

        return std::make_tuple(resolutionX, resolutionY, resolutionZ);
    }

    auto OivTcfVolumeMetaReader::GetTilePixelSizes(const H5::H5File& tcfFile, 
        const TcfReadingDataType& tcfReadingDataType, const int32_t& timeFrameIndex)
        -> std::tuple<uint32_t, uint32_t, uint32_t> {
        const auto upperGroupPath = GetUpperGroupPath(tcfReadingDataType);
        auto upperGroup = tcfFile.openGroup(upperGroupPath);

        const auto timeFrameString = QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));
        const auto timeFrameStdString = std::string{ timeFrameString.toLocal8Bit().constData() };

        auto octreeVolumeGroup = upperGroup.openGroup(timeFrameStdString);

        int64_t tileSizeX{ 0 };
        int64_t tileSizeY{ 0 };
        int64_t tileSizeZ{ 0 };

        if (octreeVolumeGroup.attrExists("TileSizeX")) {
            auto tileSizeXAttribute = octreeVolumeGroup.openAttribute("TileSizeX");
            tileSizeXAttribute.read(tileSizeXAttribute.getDataType(), &tileSizeX);
            tileSizeXAttribute.close();
        }

        if (octreeVolumeGroup.attrExists("TileSizeY")) {
            auto tileSizeYAttribute = octreeVolumeGroup.openAttribute("TileSizeY");
            tileSizeYAttribute.read(tileSizeYAttribute.getDataType(), &tileSizeY);
            tileSizeYAttribute.close();
        }

        if (octreeVolumeGroup.attrExists("TileSizeZ")) {
            auto tileSizeZAttribute = octreeVolumeGroup.openAttribute("TileSizeZ");
            tileSizeZAttribute.read(tileSizeZAttribute.getDataType(), &tileSizeZ);
            tileSizeZAttribute.close();
        }

        if (tileSizeZ == 0) {
            tileSizeZ = 1;
        }

        if (tcfReadingDataType == +TcfReadingDataType::BF || tcfReadingDataType == +TcfReadingDataType::PHASE) {
            tileSizeZ = 1;
        }

        octreeVolumeGroup.close();
        upperGroup.close();

        return std::make_tuple(static_cast<uint32_t>(tileSizeX), static_cast<uint32_t>(tileSizeY),
            static_cast<uint32_t>(tileSizeZ));
    }

    auto OivTcfVolumeMetaReader::GenerateTilePixelSizes(const TcfReadingDataType& tcfReadingDataType)
        -> std::tuple<uint32_t, uint32_t, uint32_t> {
        const auto basicTilePixelSizes3d = std::make_tuple(basicTileSizeX, basicTileSizeY, basicTileSizeZ);
        const auto basicTilePixelSizes2d = std::make_tuple(basicTileSizeX, basicTileSizeY, 1);

        const auto data3d =
            tcfReadingDataType == +TcfReadingDataType::HT ||
            tcfReadingDataType == +TcfReadingDataType::FLBLUE ||
            tcfReadingDataType == +TcfReadingDataType::FLGREEN ||
            tcfReadingDataType == +TcfReadingDataType::FLRED;

        const auto data2d =
            tcfReadingDataType == +TcfReadingDataType::HTMIP ||
            tcfReadingDataType == +TcfReadingDataType::FLBLUEMIP ||
            tcfReadingDataType == +TcfReadingDataType::FLGREENMIP ||
            tcfReadingDataType == +TcfReadingDataType::FLREDMIP ||
            tcfReadingDataType == +TcfReadingDataType::PHASE ||
            tcfReadingDataType == +TcfReadingDataType::BF;

        if (data3d) {
            return basicTilePixelSizes3d;
        } else if (data2d) {
            return basicTilePixelSizes2d;
        } else {
            return basicTilePixelSizes3d;
        }
    }

    auto OivTcfVolumeMetaReader::GetMinMaxFromGroup(const H5::H5File& tcfFile, 
        const TcfReadingDataType& tcfReadingDataType, const int32_t& timeFrameIndex)
        -> std::tuple<double, double, bool> {
        const auto upperGroupPath = GetUpperGroupPath(tcfReadingDataType);
        auto upperGroup = tcfFile.openGroup(upperGroupPath);

        const auto timeFrameString = QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));
        const auto timeFrameStdString = std::string{ timeFrameString.toLocal8Bit().constData() };

        auto volumeGroup = upperGroup.openGroup(timeFrameStdString);
        const auto minMaxValidValues = GetMinMaxFromObject(volumeGroup, tcfReadingDataType);

        volumeGroup.close();
        upperGroup.close();

        return minMaxValidValues;
    }

    auto OivTcfVolumeMetaReader::GetMinMaxAttributeName(const TcfReadingDataType& tcfReadingDataType)
        -> std::tuple<std::string, std::string> {
        std::string minAttributeName{"min"};
        std::string maxAttributeName{"max"};

        if (tcfReadingDataType == +TcfReadingDataType::HT || tcfReadingDataType == +TcfReadingDataType::HTMIP) {
            minAttributeName = "RIMin";
            maxAttributeName = "RIMax";
        } else if (tcfReadingDataType == +TcfReadingDataType::FLBLUE ||
            tcfReadingDataType == +TcfReadingDataType::FLBLUEMIP ||
            tcfReadingDataType == +TcfReadingDataType::FLGREEN ||
            tcfReadingDataType == +TcfReadingDataType::FLGREENMIP ||
            tcfReadingDataType == +TcfReadingDataType::FLRED ||
            tcfReadingDataType == +TcfReadingDataType::FLREDMIP) {
            minAttributeName = "MinIntensity";
            maxAttributeName = "MaxIntensity";
        } else if (tcfReadingDataType == +TcfReadingDataType::PHASE) {
            minAttributeName = "MinPhase";
            maxAttributeName = "MaxPhase";
        } else if (tcfReadingDataType == +TcfReadingDataType::BF) {
        } 

        return std::make_tuple(minAttributeName, maxAttributeName);
    }

    auto OivTcfVolumeMetaReader::GetMinMaxFromDataSet(const H5::H5File& tcfFile,
        const TcfReadingDataType& tcfReadingDataType, const int32_t& timeFrameIndex)
    -> std::tuple<double, double, bool> {
        const auto upperGroupPath = GetUpperGroupPath(tcfReadingDataType);
        auto upperGroup = tcfFile.openGroup(upperGroupPath);

        const auto timeFrameString = QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));
        const auto timeFrameStdString = std::string{ timeFrameString.toLocal8Bit().constData() };

        auto volumeDataSet = upperGroup.openDataSet(timeFrameStdString);
        const auto minMaxValidValues = GetMinMaxFromObject(volumeDataSet, tcfReadingDataType);

        volumeDataSet.close();
        upperGroup.close();

        return minMaxValidValues;
    }

    auto OivTcfVolumeMetaReader::GetMinMaxFromObject(const H5::H5Object& object, 
        const TcfReadingDataType& tcfReadingDataType) const -> std::tuple<double, double, bool> {
        const auto minMaxAttributeNames = GetMinMaxAttributeName(tcfReadingDataType);

        const auto minAttributeName = std::get<0>(minMaxAttributeNames);
        const auto maxAttributeName = std::get<1>(minMaxAttributeNames);

        const auto minAttributeExist = object.attrExists(minAttributeName);
        const auto maxAttributeExist = object.attrExists(maxAttributeName);

        const auto attributeExist = minAttributeExist && maxAttributeExist;

        double minValue{ 0 };
        double maxValue{ 0 };
        bool minMaxValid{ false };

        if (attributeExist) {
            auto minAttribute = object.openAttribute(minAttributeName);
            minAttribute.read(minAttribute.getDataType(), &minValue);
            minAttribute.close();

            auto maxAttribute = object.openAttribute(maxAttributeName);
            maxAttribute.read(maxAttribute.getDataType(), &maxValue);
            maxAttribute.close();

            minMaxValid = true;
        }

        if (tcfReadingDataType == +TcfReadingDataType::HT || tcfReadingDataType == +TcfReadingDataType::HTMIP) {
            minValue *= 10000;
            maxValue *= 10000;
        }

        return std::make_tuple(minValue, maxValue, minMaxValid);
    }
}
