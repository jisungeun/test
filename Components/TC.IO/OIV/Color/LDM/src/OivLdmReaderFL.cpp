#pragma warning(push)
#pragma warning(disable : 4819)

#define LOGGER_TAG "[LDMFL]"
#include <TCLogger.h>

#include <Inventor/SoInput.h>
#include <LDM/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>

#pragma warning(pop)

#include "OivLdmReaderFL.h"

#include <iomanip>
SO_FIELDCONTAINER_SOURCE(OivLdmReaderFL);

struct OivLdmReaderFL::Impl {
	std::shared_ptr<TCFLdmReader> hdfReader{ nullptr };
	int num_tiles{ 0 };
	int num_zeros{ 1 };
	int tileSize[3]{ 0, };
	bool is2D{ false };
	int sigBit{ 16 };
	int ch{0};
	float flOffset{ 0 };
};

OivLdmReaderFL::OivLdmReaderFL(int ch,bool is2D,bool is8Bit) : d{ new Impl } {
	d->hdfReader = std::make_shared<TCFLdmReader>(true);
	d->hdfReader->SetIs8Bit(is8Bit);
	if (is8Bit) {		
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
	m_dataConverted = TRUE;	
	d->is2D = is2D;
	d->ch = ch;
	SO_FIELDCONTAINER_CONSTRUCTOR(OivLdmReaderFL);
}

OivLdmReaderFL::~OivLdmReaderFL() {
}

auto OivLdmReaderFL::setTileDimension(int dimX, int dimY, int dimZ) -> void {
	d->tileSize[0] = dimX;
	d->tileSize[1] = dimY;
	d->tileSize[2] = dimZ;
}

auto OivLdmReaderFL::set8Bit(bool is8Bit) -> void {
	d->hdfReader->SetIs8Bit(is8Bit);
	if(is8Bit) {		
		d->sigBit = 8;		
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);		
	}else {
		d->sigBit = 16;		
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
}

auto OivLdmReaderFL::setTileName(const std::string& tileName) -> void {
	auto header = d->hdfReader->GetTCFHeader();
	header->tileName = tileName;
}

auto OivLdmReaderFL::setDataGroupPath(const std::string& dataGroup) -> void {
	auto header = d->hdfReader->GetTCFHeader();
	header->dataGroup = dataGroup;
}

auto OivLdmReaderFL::getOffsetZ() -> double {
	auto header = d->hdfReader->GetTCFHeader();
	double offset = header->offsetZ;
	int size_z = header->sizeZ;
	double resolutionZ = header->resolutionZ;
		
	return (offset - ((double)size_z * resolutionZ) / 2.0);
}

auto OivLdmReaderFL::setFilename(const SbString& filename) -> int {	
	if (nullptr != d->hdfReader) {
		getNumZeros(filename.toStdString());
		int errStatus = d->hdfReader->SetFileName(filename.toStdString(), d->num_zeros);
		if (errStatus < 0) {
			QLOG_ERROR() << "failed to set file name";
			return -1;
		}
		auto header = d->hdfReader->GetTCFHeader();
		
		QLOG_INFO() << "tile size: " << d->tileSize[0];
		QLOG_INFO() << "size :" << header->sizeX << "," << header->sizeY << "," << header->sizeZ;				
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));
		return 1;
	}
	return 0;
}

auto OivLdmReaderFL::getNumZeros(const std::string& filename) -> int {
	H5::H5File file(filename, H5F_ACC_RDONLY);
	if (d->is2D) {
		auto gName = QString("Data/2DFLMIP/CH%1").arg(d->ch).toStdString();
		if (true == file.exists(gName)) {
			auto default_group_2d = QString("Data/2DFLMIP/CH%1/000000").arg(d->ch).toStdString();
			H5::Group group = file.openGroup(default_group_2d);
			d->num_tiles = group.getNumObjs();

			auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
			std::size_t pos = final_tile_name.find("_");
			std::string numTxt = final_tile_name.substr(pos + 1);

			d->num_zeros = static_cast<int>(numTxt.length());
			group.close();
		}
	}
	else {
		auto gName = QString("/Data/3DFL/CH%1").arg(d->ch).toStdString();
		if (true == file.exists(gName)){
			auto default_group_3d = QString("/Data/3DFL/CH%1/000000").arg(d->ch).toStdString();
			H5::Group group = file.openGroup(default_group_3d);

			d->num_tiles = group.getNumObjs();
			QLOG_INFO() << "# of object in LDM group : " << d->num_tiles;			
			auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
			QLOG_INFO() << "final tile name: " << final_tile_name.c_str();
			std::size_t pos = final_tile_name.find("_");
			std::string numTxt = final_tile_name.substr(pos + 1);
			QLOG_INFO() << "number text: " << numTxt.c_str();
			QLOG_INFO() << "Zeros : " << numTxt.length();
			d->num_zeros = static_cast<int>(numTxt.length());
			group.close();
		}
	}
	file.close();
	return 0;
}

auto OivLdmReaderFL::setFLOffset(float offset) -> void {
	d->flOffset = offset;
}

auto OivLdmReaderFL::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm) -> SoVolumeReader::ReadError {	
	auto header = d->hdfReader->GetTCFHeader();
	const auto offset = d->flOffset;
	SbVec3f _min = SbVec3f((float)(-header->sizeX * header->resolutionX / 2.),
		(float)(-header->sizeY * header->resolutionY / 2.),
		(float)(-header->sizeZ * header->resolutionZ / 2.) + offset);
	SbVec3f _max = SbVec3f((float)(header->sizeX * header->resolutionX / 2.),
		(float)(header->sizeY * header->resolutionY / 2.),
		(float)(header->sizeZ * header->resolutionZ / 2.) + offset);

	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	if (d->is2D) {
		idm = SbVec3i32(header->sizeX, header->sizeY, 1);
	}
	else {		
		idm = SbVec3i32(header->sizeX, header->sizeY, header->sizeZ);		
	}

	return RD_NO_ERROR;
}

auto OivLdmReaderFL::getBorderFlag() -> int {
	return 0;
}

auto OivLdmReaderFL::getNumSignificantBits() -> int {
	return d->sigBit;
}

auto OivLdmReaderFL::getTileSize(SbVec3i32& size) -> SbBool {
	if (d->is2D) {
		size = SbVec3i32(d->tileSize[0], d->tileSize[1], 1);
	}
	else {		
		size = SbVec3i32(d->tileSize[0], d->tileSize[1], d->tileSize[2]);
	}
	return TRUE;
}

auto OivLdmReaderFL::getMinMax(int64_t& min, int64_t& max) -> SbBool {
	auto header = d->hdfReader->GetTCFHeader();
	min = header->riMin;
	max = header->riMax;
	return TRUE;
}

auto OivLdmReaderFL::getMinMax(double& min, double& max) -> SbBool {
	auto header = d->hdfReader->GetTCFHeader();
	min = header->riMin;
	max = header->riMax;
	return TRUE;
}

auto OivLdmReaderFL::getTileString(int _tileId, int _nDigits) -> std::string {
	//int tileNum = _tileId;

	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(_nDigits) << std::setfill('0') << _tileId;

	return tmpStr.str();
}


auto OivLdmReaderFL::readTile(int index, const SbBox3i32& tilePosition) -> SoBufferObject* {
	Q_UNUSED(tilePosition)
	auto header = d->hdfReader->GetTCFHeader();
	//int maxNumTiles = header->NumOfTiles;	
	int maxNumTiles = d->num_tiles;
	if (index >= maxNumTiles)
	{
		QLOG_ERROR() << "Could not find tile at index " << index;
		return NULL;
	}
	// Get name of the tile corresponding to current index	
	std::string tileName = getTileString(index, d->num_zeros);	
	int voxelBytes = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

	SoCpuBufferObject* tileBuffer = new SoCpuBufferObject;
	size_t bufferSize;
	if (d->is2D) {
		bufferSize = voxelBytes * d->tileSize[0] * d->tileSize[1] * 1;
	}
	else {
		bufferSize = voxelBytes * d->tileSize[0] * d->tileSize[1] * d->tileSize[2];
	}
	tileBuffer->setSize(bufferSize);

	void* buffer = (void*)tileBuffer->map(SoBufferObject::SET);
	int retBufSize = d->hdfReader->ReadHTLdmData3D(tileName, &buffer);


	tileBuffer->unmap();
#if 0	// This implementation makes a copy of Dataset in memory!
	// Read Dataset from HDF5 file
	void* tileData = NULL;
	int bufferSize = _hdfReader->ReadLdmTileData(tileName, &tileData);

	// Copy data buffer to SoCpyBufferObject
	SoCpuBufferObject* tileBuffer = new SoCpuBufferObject;
	if (!tileBuffer->setSize(bufferSize))
		return NULL;

	unsigned char* buffer = (unsigned char*)tileBuffer->map(SoBufferObject::SET);
	memcpy(buffer, tileData, bufferSize);
	tileBuffer->unmap();

	// Free the local buffer
	free(tileData);
	tileData = NULL;
#endif
	return tileBuffer;
}

auto OivLdmReaderFL::getSubSlice(const SbBox2i32&, int, void*) -> void {
	QLOG_ERROR() << "OivCustomVRHdf::getSubSlice : Not Implemented";
}

auto OivLdmReaderFL::getSubSlice(const SbBox2i32& , int , SoBufferObject* ) -> void {		
	QLOG_ERROR() << "OivCustomVRHdf::getSubSlice : Not Implemented";
}

auto OivLdmReaderFL::getHistogram(std::vector<int64_t>& ) -> SbBool {
	return FALSE;
}

auto OivLdmReaderFL::isDataConverted() const -> SbBool {	
	return TRUE;
}

auto OivLdmReaderFL::isRGBA() const -> SbBool {	
	return FALSE;
}

auto OivLdmReaderFL::isThreadSafe() const -> SbBool {
	//return TRUE;
	return FALSE;
}

void OivLdmReaderFL::initClass() {
	SO_FIELDCONTAINER_INIT_CLASS(OivLdmReaderFL, "OivCustomLdmReaderFL", SoVolumeReader);
}

void OivLdmReaderFL::exitClass() {
	SO__FIELDCONTAINER_EXIT_CLASS(OivLdmReaderFL);
}