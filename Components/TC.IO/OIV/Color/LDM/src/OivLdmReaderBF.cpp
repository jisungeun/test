#pragma warning(push)
#pragma warning(disable : 4819)

#define LOGGER_TAG "[LDM]"
#include <TCLogger.h>

#include <Inventor/SoInput.h>
#include <LDM/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>

#pragma warning(pop)

#include "OivLdmReaderBF.h"

#include <iomanip>
SO_FIELDCONTAINER_SOURCE(OivLdmReaderBF);

struct OivLdmReaderBF::Impl {
	std::shared_ptr<ITCFLdmReader> hdfReader{ nullptr };
	int num_tiles{ 0 };
	int num_zeros{ 1 };
	int tileSize[3]{ 0, };
	bool is2D{ false };
	int ch{ 0 };
};

OivLdmReaderBF::OivLdmReaderBF(bool is2D) : d{ new Impl } {
    d->hdfReader = std::make_shared<TCFLdmReader>(false, true);	
	m_dataConverted = TRUE;	
	d->is2D = is2D;

	SO_FIELDCONTAINER_CONSTRUCTOR(OivLdmReaderBF);
}

OivLdmReaderBF::~OivLdmReaderBF() {
}

auto OivLdmReaderBF::setColorChannel(int ch) -> void {
	d->ch = ch;
}


auto OivLdmReaderBF::setTileDimension(int dimX, int dimY, int dimZ) -> void {
	d->tileSize[0] = dimX;
	d->tileSize[1] = dimY;
	d->tileSize[2] = dimZ;
}


auto OivLdmReaderBF::setTileName(const std::string& tileName) -> void {
	auto header = d->hdfReader->GetTCFHeader();
	header->tileName = tileName;
}

auto OivLdmReaderBF::setDataGroupPath(const std::string& dataGroup) -> void {
	auto header = d->hdfReader->GetTCFHeader();
	header->dataGroup = dataGroup;
}

auto OivLdmReaderBF::getOffsetZ() -> double {
	auto header = d->hdfReader->GetTCFHeader();
	double offset = header->offsetZ;
	int size_z = header->sizeZ;
	double resolutionZ = header->resolutionZ;

	return (offset - ((double)size_z * resolutionZ) / 2.0);
}

auto OivLdmReaderBF::setFilename(const SbString& filename) -> int {
	if (nullptr != d->hdfReader) {
		getNumZeros(filename.toStdString());
		int errStatus = d->hdfReader->SetFileName(filename.toStdString(), d->num_zeros);
		if (errStatus < 0) {
			QLOG_ERROR() << "failed to set file name";
			return -1;
		}
		auto header = d->hdfReader->GetTCFHeader();
		
		QLOG_INFO() << "tile size: " << d->tileSize[0];
		QLOG_INFO() << "size :" << header->sizeX << "," << header->sizeY << "," << header->sizeZ;		
		if (header->dataType == SoDataSet::UNSIGNED_SHORT) {
			QLOG_INFO() << "data type is unsigned short ";
		}
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));				

		return 1;
	}
	return 0;
}

auto OivLdmReaderBF::getNumZeros(const std::string& filename) -> int {
	H5::H5File file(filename, H5F_ACC_RDONLY);
	if(true == file.exists("/Data/BF/000000")) {
		H5::Group group = file.openGroup("/Data/BF/000000");
		d->num_tiles = group.getNumObjs();

		auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
		std::size_t pos = final_tile_name.find("_");
		std::string numTxt = final_tile_name.substr(pos + 1);
		d->num_zeros = static_cast<int>(numTxt.length());		
		group.close();
	}	
	file.close();
	return 0;
}


auto OivLdmReaderBF::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm) -> SoVolumeReader::ReadError {
	SbVec3f _min = SbVec3f(0.f, 0.f, 0.f);
	auto header = d->hdfReader->GetTCFHeader();
	SbVec3f _max = SbVec3f((float)(header->sizeX * header->resolutionX),
		(float)(header->sizeY * header->resolutionY),
		(float)(header->sizeZ * header->resolutionZ));

	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	if (d->is2D) {
		idm = SbVec3i32(header->sizeX, header->sizeY, 1);
	}
	else {		
		idm = SbVec3i32(header->sizeX, header->sizeY, header->sizeZ);		
	}

	return RD_NO_ERROR;
}

auto OivLdmReaderBF::getBorderFlag() -> int {
	return 0;
}

auto OivLdmReaderBF::getNumSignificantBits() -> int {
	return 8;
}

auto OivLdmReaderBF::getTileSize(SbVec3i32& size) -> SbBool {
	if (d->is2D) {
		size = SbVec3i32(d->tileSize[0], d->tileSize[1], 1);
	}
	else {		
		size = SbVec3i32(d->tileSize[0], d->tileSize[1], d->tileSize[2]);
	}
	return TRUE;
}

auto OivLdmReaderBF::getMinMax(int64_t& min, int64_t& max) -> SbBool {
	Q_UNUSED(min);
	Q_UNUSED(max);
	//auto header = d->hdfReader->GetTCFHeader();
	//min = header->riMin;
	//max = header->riMax;
	return TRUE;
}

auto OivLdmReaderBF::getMinMax(double& min, double& max) -> SbBool {
	Q_UNUSED(min);
	Q_UNUSED(max);
	//auto header = d->hdfReader->GetTCFHeader();
	//min = header->riMin;
	//max = header->riMax;
	return TRUE;
}

auto OivLdmReaderBF::getTileString(int _tileId, int _nDigits) -> std::string {
	//int tileNum = _tileId;

	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(_nDigits) << std::setfill('0') << _tileId;

	return tmpStr.str();
}


auto OivLdmReaderBF::readTile(int index, const SbBox3i32& tilePosition) -> SoBufferObject* {
	Q_UNUSED(tilePosition)
	auto header = d->hdfReader->GetTCFHeader();
	//int maxNumTiles = header->NumOfTiles;	
	int maxNumTiles = d->num_tiles;
	if (index >= maxNumTiles)
	{
		QLOG_ERROR() << "Could not find tile at index " << index;
		return NULL;
	}
	// Get name of the tile corresponding to current index	
	std::string tileName = getTileString(index, d->num_zeros);	
	int voxelBytes = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

	SoCpuBufferObject* tileBuffer = new SoCpuBufferObject;
	size_t bufferSize;
	if (d->is2D) {
		bufferSize = voxelBytes * d->tileSize[0] * d->tileSize[1] * 1;
	}
	else {
		bufferSize = voxelBytes * d->tileSize[0] * d->tileSize[1] * d->tileSize[2];
	}
	tileBuffer->setSize(bufferSize);

	void* buffer = (void*)tileBuffer->map(SoBufferObject::SET);
	//int retBufSize = d->hdfReader->ReadHTLdmData3D(tileName, &buffer);
	int retBufSize = d->hdfReader->ReadBFLdmData(tileName, &buffer, d->ch);


	tileBuffer->unmap();
#if 0	// This implementation makes a copy of Dataset in memory!
	// Read Dataset from HDF5 file
	void* tileData = NULL;
	int bufferSize = _hdfReader->ReadLdmTileData(tileName, &tileData);

	// Copy data buffer to SoCpyBufferObject
	SoCpuBufferObject* tileBuffer = new SoCpuBufferObject;
	if (!tileBuffer->setSize(bufferSize))
		return NULL;

	unsigned char* buffer = (unsigned char*)tileBuffer->map(SoBufferObject::SET);
	memcpy(buffer, tileData, bufferSize);
	tileBuffer->unmap();

	// Free the local buffer
	free(tileData);
	tileData = NULL;
#endif
	return tileBuffer;
}

auto OivLdmReaderBF::getSubSlice(const SbBox2i32&, int, void*) -> void {
	QLOG_ERROR() << "OivCustomVRHdf::getSubSlice : Not Implemented";
}

auto OivLdmReaderBF::getSubSlice(const SbBox2i32& , int , SoBufferObject* ) -> void {		
	QLOG_ERROR() << "OivCustomVRHdf::getSubSlice : Not Implemented";
}

auto OivLdmReaderBF::getHistogram(std::vector<int64_t>& ) -> SbBool {
	return FALSE;
}

auto OivLdmReaderBF::isDataConverted() const -> SbBool {	
	return TRUE;
}

auto OivLdmReaderBF::isRGBA() const -> SbBool {	
	return FALSE;
}

auto OivLdmReaderBF::isThreadSafe() const -> SbBool {
	//return TRUE;
	return FALSE;
}

void OivLdmReaderBF::initClass() {
	SO_FIELDCONTAINER_INIT_CLASS(OivLdmReaderBF, "OivCustomLdmReaderBF", SoVolumeReader);
}

void OivLdmReaderBF::exitClass() {
	SO__FIELDCONTAINER_EXIT_CLASS(OivLdmReaderBF);
}