#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "TC.IO.OIV.Color.NaiveExport.h"

class TC_IO_OIV_Color_Naive_API OivThumbnailReader : public SoVolumeReader {
    SO_FIELDCONTAINER_HEADER(OivThumbnailReader);

public:
    OivThumbnailReader(bool isFL = false);

    auto SetTileDimension(int dimX,int dimY,int dimZ)->void;
	auto SetTileIndex(int index) -> void;
	auto SetColorChannel(int ch)->void;

	// SoVolumeReader
    virtual auto setFilename(const SbString& filename) -> int override;
	virtual auto getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm)->SoVolumeReader::ReadError override;
	virtual auto getBorderFlag() -> int override;
	virtual auto getNumSignificantBits() -> int override;
	virtual auto getTileSize(SbVec3i32& size)->SbBool override;
	virtual auto readTile(int index, const SbBox3i32& tilePosition)->SoBufferObject* override;
	virtual auto getSubSlice(const SbBox2i32&, int, void*) -> void override;
	virtual auto getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void override;
	virtual auto getHistogram(std::vector<int64_t>& numVox)->SbBool override;	
	virtual auto isDataConverted() const->SbBool override;	
	virtual auto isRGBA() const->SbBool override;
	virtual auto isThreadSafe() const->SbBool override;

protected:
	~OivThumbnailReader();

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};