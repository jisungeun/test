#pragma once

#include <memory>
#include <string>

#include <QString>

#include "TC.IO.OIV.Color.NaiveExport.h"

class TC_IO_OIV_Color_Naive_API ThumbnailReader {
public:
	ThumbnailReader(bool isFL = false);
	~ThumbnailReader();

	auto SetFileName(const QString& name) -> void;

	auto GetSize() const -> std::tuple<int64_t, int64_t, int64_t>;
	auto GetResolution() const -> std::tuple<double, double, double>;

	auto ReadHT(int timeIndex, void** data) const -> int;
	auto ReadFL(int timeIndex, int channel, void** data) const -> int;
	auto ReadBF(int timeIndex, void** data) const -> int;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
