#pragma once

#include "TC.IO.OIV.Color.NaiveExport.h"

#pragma warning(push)
#pragma warning(disable:4268)
#include "hdf5.h"
#include "H5Cpp.h"
#pragma warning(pop)

#include <QMutex>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SbVec.h>
#include <LDM/nodes/SoDataSet.h>
#pragma warning(pop)

//static QMutex _mutex;

typedef struct HEADER_OF_TCFCOLOR{
	int			dataCount = -1;
	double      resolutionZ = -1.0;
	double		resolutionX = -1.0;
	double		resolutionY = -1.0;
	double      offsetZ = -1.0;
	int         sizeZ = -1;
	int			sizeX = -1;
	int			sizeY = -1;
	float		timeInterval = -1.f;
	float		positionC = -1.f;
	float		positionX = -1.f;
	float		positionY = -1.f;
	float		positionZ = -1.f;
	int			dataType = -1;
	std::string dataGroup;
	std::string tileName;
	std::string filePath;
} TCFColorHeader;

class TC_IO_OIV_Color_Naive_API ColorReader {
public:
	ColorReader();
	ColorReader(const std::string& name,const std::string& dataGroup = "/Data/3D", const std::string& tileName = "000000");
	~ColorReader();

	auto GetColorHeader(void)->TCFColorHeader*;
	auto SetFileName(const std::string& name)->int;
	auto ReadColorData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data)->int;
	auto ReadColorData2D(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data,int ch)->int;
	auto ReadBFMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFColorHeader& header)->bool;

private:
	auto Init()->int;
	auto OpenHdfFile(const std::string& name)->int;
	auto CloseHdfFile(void)->int;

	struct Impl;
	std::unique_ptr<Impl> d;
};