#pragma once

#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/readers/SoVolumeReader.h>
//#include "TCFClasses.h"

#include "TC.IO.OIV.Color.NaiveExport.h"

class TC_IO_OIV_Color_Naive_API OivColorReader : public SoVolumeReader {
    SO_FIELDCONTAINER_HEADER(OivColorReader);
public:
    OivColorReader();
	~OivColorReader();
	auto setTileDimension(int tileSize) -> void;
	auto setTileName(const std::string& tileName) -> void;
	auto setDataGroupPath(const std::string& dataGroup) -> void;
	auto getOffsetZ()->double;
	auto setColorChannel(int ch)->void;

	auto getTimeSteps()->int;

	virtual auto setFilename(const SbString& filename) -> int override;
	virtual auto getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm)->SoVolumeReader::ReadError override;
	virtual auto getBorderFlag() -> int override;
	virtual auto getNumSignificantBits() -> int override;
	virtual auto getTileSize(SbVec3i32& size)->SbBool override;
	virtual auto getMinMax(int64_t& min, int64_t& max)->SbBool override;
	virtual auto getMinMax(double& min, double& max)->SbBool override;
	virtual auto readTile(int index, const SbBox3i32& tilePosition)->SoBufferObject* override;
	virtual auto getSubSlice(const SbBox2i32&, int, void*) -> void override;
	virtual auto getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void override;
	virtual auto getHistogram(std::vector<int64_t>& numVox)->SbBool override;
	virtual auto isDataConverted() const->SbBool override;
	virtual auto isRGBA() const->SbBool override;
	virtual auto isThreadSafe() const->SbBool override;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};