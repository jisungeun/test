#define LOGGER_TAG "[OivThumbnailReader]"

#include <TCLogger.h>

#include "TCFThumbnail.h"
#include "OivThumbnailReader.h"


SO_FIELDCONTAINER_SOURCE(OivThumbnailReader);

struct OivThumbnailReader::Impl {
	std::shared_ptr<ThumbnailReader> hdfReader{ nullptr };

	int tileSize[3]{ 1024, 1024, 1 };
	int tileIndex{ 0 };
	bool isFL{ false };
	int ch{ 0 };
};

OivThumbnailReader::OivThumbnailReader(bool isFL) : d{ new Impl } {
	m_dataConverted = TRUE;

    d->hdfReader = std::make_shared<ThumbnailReader>();	
	d->isFL = isFL;

	SO_FIELDCONTAINER_CONSTRUCTOR(OivThumbnailReader);
}

OivThumbnailReader::~OivThumbnailReader() {
}

auto OivThumbnailReader::SetTileDimension(int dimX, int dimY, int dimZ) -> void {
	d->tileSize[0] = dimX;
	d->tileSize[1] = dimY;
	d->tileSize[2] = dimZ;
}

auto OivThumbnailReader::SetTileIndex(int index) -> void {
	d->tileIndex = index;
}

auto OivThumbnailReader::SetColorChannel(int ch) -> void {
	d->ch = ch;
}

auto OivThumbnailReader::setFilename(const SbString& filename) -> int {
	if (d->hdfReader == nullptr) return 0;

	d->hdfReader->SetFileName(filename.getString());
	m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);

	return 1;
}

auto OivThumbnailReader::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm) -> SoVolumeReader::ReadError {
	SbVec3f min = SbVec3f(0.f, 0.f, 0.f);

	const auto [sizeX, sizeY, sizeZ] = d->hdfReader->GetSize();
	const auto [resX, resY, resZ] = d->hdfReader->GetResolution();

	SbVec3f max = SbVec3f(
		static_cast<float>(sizeX * resX),
		static_cast<float>(sizeY * resY),
		static_cast<float>(sizeZ * resZ)
	);

	size = SbBox3f(min, max);
	type = SoDataSet::DataType(SoDataSet::UNSIGNED_BYTE);
	idm = SbVec3i32(sizeX, sizeY, 1);

	return RD_NO_ERROR;
}

auto OivThumbnailReader::getBorderFlag() -> int {
	return 0;
}

auto OivThumbnailReader::getNumSignificantBits() -> int {
	return 8;
}

auto OivThumbnailReader::getTileSize(SbVec3i32& size) -> SbBool {
	size = SbVec3i32(d->tileSize[0], d->tileSize[1], 1);

	return TRUE;
}

auto OivThumbnailReader::readTile(int index, const SbBox3i32& tilePosition) -> SoBufferObject* {
	Q_UNUSED(tilePosition)

	int voxelBytes = SoDataSet::dataSize(SoDataSet::DataType(SoDataSet::UNSIGNED_BYTE));

	SoCpuBufferObject* tileBuffer = new SoCpuBufferObject;
	size_t bufferSize;
	bufferSize = voxelBytes * d->tileSize[0] * d->tileSize[1] * d->tileSize[2];

	tileBuffer->setSize(bufferSize);

	void* buffer = (void*)tileBuffer->map(SoBufferObject::SET);

	if (d->isFL)
		d->hdfReader->ReadFL(d->tileIndex, d->ch, &buffer);	
	else
		d->hdfReader->ReadHT(d->tileIndex, &buffer);

	tileBuffer->unmap();

	return tileBuffer;
}

auto OivThumbnailReader::getSubSlice(const SbBox2i32&, int, void*) -> void {
	QLOG_ERROR() << "OivThumbnailReader::getSubSlice : Not Implemented";
}

auto OivThumbnailReader::getSubSlice(const SbBox2i32& , int , SoBufferObject* ) -> void {		
	QLOG_ERROR() << "OivThumbnailReader::getSubSlice : Not Implemented";
}

auto OivThumbnailReader::getHistogram(std::vector<int64_t>& ) -> SbBool {
	return FALSE;
}

auto OivThumbnailReader::isDataConverted() const -> SbBool {	
	return TRUE;
}

auto OivThumbnailReader::isRGBA() const -> SbBool {	
	return FALSE;
}

auto OivThumbnailReader::isThreadSafe() const -> SbBool {
	//return TRUE;
	return FALSE;
}

void OivThumbnailReader::initClass() {
	SO_FIELDCONTAINER_INIT_CLASS(OivThumbnailReader, "OivCustomThumbnailReader", SoVolumeReader);
}

void OivThumbnailReader::exitClass() {
	SO__FIELDCONTAINER_EXIT_CLASS(OivThumbnailReader);
}