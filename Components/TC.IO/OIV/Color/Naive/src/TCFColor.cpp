#include "TCFColor.h"

#include <QMutexLocker>
#include <HDF5Mutex.h>

struct ColorReader::Impl {
    hid_t fileId = -1;
    hid_t tcfGrpId = -1;
    TCFColorHeader* header{ nullptr };
};

ColorReader::ColorReader() :d{ new Impl } {
    d->header = new TCFColorHeader;
    this->Init();
}

ColorReader::ColorReader(const std::string& name, const std::string& dataGroup, const std::string& tileName)
	: d{ new Impl } {
	this->Init();
	d->header->dataGroup = dataGroup;
	d->header->tileName = tileName;
	this->SetFileName(name);
}

ColorReader::~ColorReader() {
    delete d->header;

    CloseHdfFile();
}

auto ColorReader::GetColorHeader() -> TCFColorHeader* {
    return d->header;
}

auto ColorReader::SetFileName(const std::string& name) -> int {
    if (OpenHdfFile(name) < 0)
        return -1;
    return 0;
}

auto ColorReader::ReadColorData(const SbVec3i32& , const SbVec3i32& , void** ) -> int {
	
    return 0;
}

auto ColorReader::ReadColorData2D(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data,int ch) -> int {
	//QMutexLocker locker(&_mutex);
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	H5::Group group(d->tcfGrpId);	
	H5::DataSet dataset = group.openDataSet(d->header->tileName.c_str());
	
	auto cast_data = static_cast<unsigned char*>(*data);

	H5::DataSpace dataSpace = dataset.getSpace();
	
	hsize_t dims[3];
	int dim = dataSpace.getSimpleExtentDims(dims);

	if (dim == 2) {
		group.close();
		return 1;
	}
		
	// The start, stride, count, and block arrays must be the same size as the rank of the dataspace.
	// Stride parameter is NULL, a contiguous hyperslab is selected	
	hsize_t tcfOffset[3] = { (hsize_t)(offset[2]+ch), (hsize_t)(offset[1]), (hsize_t)(offset[0]) };		
	// How many blocks to select from the dataspace, in each dimension.
	hsize_t tcfCount[3] = { (hsize_t)(dataSize[2]), (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]) };

	dataSpace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);
	
	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };
	memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);
	//auto size = dataSize[2] * dataSize[1] * dataSize[0];	
	
	dataset.read(cast_data, H5::PredType::NATIVE_UCHAR, memoryspace, dataSpace);

	memoryspace.close();
	dataSpace.close();
	dataset.close();
	group.close();

	return 1;	
}

auto ColorReader::ReadBFMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFColorHeader& header) -> bool {
    if (true == filePath.empty())
        return false;
    if (true == dataGroup.empty())
        return false;
    if (true == tileName.empty())
        return false;
    if (0 > H5open())
        return false;
    hid_t fileId = H5Fopen(filePath.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    if (0 > fileId)
        return false;
    if (1 > H5Lexists(fileId, dataGroup.c_str(), H5P_DEFAULT))
        return false;

	// Find the 2D group
	// Read the attributes
	// have to fill data group & tilename 
	const hid_t groupId = H5Gopen(fileId, dataGroup.c_str(), H5P_DEFAULT);
	if (0 > groupId) {
		return false;
	}

	const hid_t setId = H5Dopen2(groupId, tileName.c_str(), H5P_DEFAULT);
	if (0 > setId) {
		return false;
	}

	const hid_t dataType = H5Dget_type(setId);
	if (0 > dataType) {
		return false;
	}

	//header.dataType = SoDataSet::DataType::UNSIGNED_INT32; // 0~255로 이루어져있다 무조건
		
	const H5T_class_t t_class = H5Tget_class(dataType);
	if (t_class == H5T_INTEGER) {
		header.dataType = SoDataSet::UNSIGNED_SHORT;
	} else if (t_class == H5T_FLOAT) {
		header.dataType = SoDataSet::FLOAT;
	}

	H5O_info1_t objInfo;
	if (0 > H5Oget_info1(groupId, &objInfo)) {
		return false;
	}

	for (int i = 0; i < objInfo.num_attrs; ++i)
	{
		hid_t attributeId = H5Aopen_by_idx(groupId, ".", H5_INDEX_NAME, H5_ITER_INC, i, H5P_DEFAULT, H5P_DEFAULT);
		if (0 > attributeId) {
			continue;
		}

		const hid_t attributeType = H5Aget_type(attributeId);
		if (0 > attributeType) {
			continue;
		}

		const hid_t nativeType = H5Tget_native_type(attributeType, H5T_DIR_ASCEND);
		if (0 > nativeType) {
			continue;
		}

		char attributebName[32];
		H5Aget_name(attributeId, 32, attributebName);

		if (H5Tequal(nativeType, H5T_NATIVE_INT64))
		{
			int64_t val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "DataCount"))
				header.dataCount = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeX"))
				header.sizeX = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeZ"))
				header.sizeZ = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeY"))
				header.sizeY = static_cast<int>(val);
		} else if (H5Tequal(nativeType, H5T_NATIVE_DOUBLE))
		{
			double val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "ResolutionX"))
				header.resolutionX = val;
			else if (!strcmp(attributebName, "ResolutionZ"))
				header.resolutionZ = val;
			else if (!strcmp(attributebName, "ResolutionY"))
				header.resolutionY = val;
			else if (!strcmp(attributebName, "TimeInterval"))
				header.timeInterval = static_cast<float>(val);

		}

		header.dataGroup = dataGroup;
		header.tileName = tileName;

		H5Tclose(nativeType);
		H5Tclose(attributeType);
		H5Aclose(attributeId);

	}

	return true;
}

auto ColorReader::Init() -> int {
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_SHORT;

	return H5open();
}

auto ColorReader::OpenHdfFile(const std::string& name) -> int {
	d->fileId = H5Fopen(name.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	if (d->fileId < 0)
		return (int)d->fileId;

	// Find the 2D group
	herr_t errStatus;

	// Read the attributes
	hid_t grpId = H5Gopen(d->fileId, d->header->dataGroup.c_str(), H5P_DEFAULT);

	hid_t dSetId = H5Dopen2(grpId, d->header->tileName.c_str(), H5P_DEFAULT);
	hid_t datatype = H5Dget_type(dSetId);
	H5T_class_t t_class = H5Tget_class(datatype);
	if (t_class == H5T_INTEGER) {
		d->header->dataType = SoDataSet::UNSIGNED_BYTE;
		//d->header->dataType = SoDataSet::UNSIGNED_SHORT;
	}
	else if (t_class == H5T_FLOAT)
		d->header->dataType = SoDataSet::FLOAT;

	H5O_info1_t objInfo;
	errStatus = H5Oget_info1(grpId, &objInfo);

	for (int i = 0; i < objInfo.num_attrs; ++i)
	{
		hid_t attribId;
		char attribName[32];

		attribId = H5Aopen_by_idx(grpId, ".", H5_INDEX_NAME, H5_ITER_INC, i,
								  H5P_DEFAULT, H5P_DEFAULT);
		hid_t attribType = H5Aget_type(attribId);
		hid_t nativeType = H5Tget_native_type(attribType, H5T_DIR_ASCEND);

		H5Aget_name(attribId, 32, attribName);
		std::string tmpName(attribName);

		if (H5Tequal(nativeType, H5T_NATIVE_INT64))
		{
			int64_t val = 0;
			errStatus = H5Aread(attribId, nativeType, &val);

			if (!strcmp(attribName, "DataCount"))
				d->header->dataCount = (int)val;
			else if (!strcmp(attribName, "SizeX"))
				d->header->sizeX = (int)val;
			else if (!strcmp(attribName, "SizeZ"))
				d->header->sizeZ = (int)val;
			else if (!strcmp(attribName, "SizeY"))
				d->header->sizeY = (int)val;
		} else if (H5Tequal(nativeType, H5T_NATIVE_DOUBLE))
		{
			double val = 0;
			errStatus = H5Aread(attribId, nativeType, &val);

			if (!strcmp(attribName, "ResolutionX"))
				d->header->resolutionX = val;
			else if (!strcmp(attribName, "ResolutionZ"))
				d->header->resolutionZ = val;
			else if (!strcmp(attribName, "ResolutionY"))
				d->header->resolutionY = val;
			else if (!strcmp(attribName, "TimeInterval"))
				d->header->timeInterval = (float)val;
			else if (!strcmp(attribName, "OffsetZ"))
				d->header->offsetZ = val;
		}

		errStatus = H5Tclose(nativeType);
		errStatus = H5Tclose(attribType);
		errStatus = H5Aclose(attribId);
	}
	
	// Assign grpId to class variable _tcfGrpId for further use
	d->tcfGrpId = grpId;

	return 0;
}

auto ColorReader::CloseHdfFile() -> int {
	herr_t errStatus = -1;
	if(d->fileId>=0) {
		errStatus = H5Gclose(d->tcfGrpId);
		errStatus = H5Fclose(d->fileId);
	}
    return errStatus;
}