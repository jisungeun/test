#define LOGGER_TAG "[ThumbnailReader]"

#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include <TCLogger.h>

#include "TCFThumbnail.h"


std::string_view HTGroup{ "/Thumbnail/HT" };
std::string_view FLGroup{ "/Thumbnail/FL" };
std::string_view BFGroup{ "/Thumbnail/BF" };

struct ThumbnailHeader {
	QString path;
	QString dataGroup;

	int dataType = -1;
	bool isColor = false;
	double resolutionZ = -1.0;
	double resolutionX = -1.0;
	double resolutionY = -1.0;
	int64_t sizeZ = -1;
	int64_t sizeX = -1;
	int64_t sizeY = -1;
};

struct ThumbnailReader::Impl {
	std::shared_ptr<ThumbnailHeader> header{ nullptr };
	
	bool isFL{ false };	

	auto InitHeader() -> void;

	auto ReadDataSet(const QString& groupName, int timeIndex, void** data) -> bool;
	auto ReadDataSetFL(const QString& groupName, int timeIndex, int ch, void** data) -> bool;
};

ThumbnailReader::ThumbnailReader(bool isFL) : d{ new Impl } {
	d->InitHeader();
	d->isFL = isFL;
}

ThumbnailReader::~ThumbnailReader() {
}

auto ThumbnailReader::Impl::InitHeader() -> void {
	header = std::make_shared<ThumbnailHeader>();
	header->sizeX = 1;
	header->sizeY = 1;
	header->sizeZ = 1;
	header->resolutionX = 1;
	header->resolutionY = 1;
	header->resolutionZ = 1;
	header->dataType = SoDataSet::UNSIGNED_BYTE;
}

auto ThumbnailReader::Impl::ReadDataSet(const QString& groupName, int timeIndex, void** data) -> bool {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	try {
		auto file = H5::H5File(header->path.toStdString(), H5F_ACC_RDONLY);
		auto group = file.openGroup(groupName.toStdString());
		const auto tileName = QString("%1").arg(timeIndex, 6, 10, QChar('0'));

		auto tile = group.openDataSet(tileName.toStdString());
		const auto buf = static_cast<unsigned char*>(*data);
		tile.read(buf, H5::PredType::NATIVE_UCHAR);

		// get attributes
		if (tile.attrExists("IsColor")) {
			auto attr = tile.openAttribute("IsColor");
			int64_t value;
			attr.read(H5::PredType::NATIVE_INT64, &value);
			header->isColor = (value > 0);
			attr.close();
		}

		if (tile.attrExists("SizeX")) {
			auto attr = tile.openAttribute("SizeX");
			int64_t size;
			attr.read(H5::PredType::NATIVE_INT64, &size);
			header->sizeX = size;
			attr.close();
		}

		if (tile.attrExists("SizeY")) {
			auto attr = tile.openAttribute("SizeY");
			int64_t size;
			attr.read(H5::PredType::NATIVE_INT64, &size);
			header->sizeY = size;
			attr.close();
		}

		if (tile.attrExists("SizeZ")) {
			auto attr = tile.openAttribute("SizeZ");
			int64_t size;
			attr.read(H5::PredType::NATIVE_INT64, &size);
			header->sizeZ = size;
			attr.close();
		}

		if (tile.attrExists("ResolutionX")) {
			auto attr = tile.openAttribute("ResolutionX");
			double res;
			attr.read(H5::PredType::NATIVE_DOUBLE, &res);
			header->resolutionX = res;
			attr.close();
		}

		if (tile.attrExists("ResolutionY")) {
			auto attr = tile.openAttribute("ResolutionY");
			double res;
			attr.read(H5::PredType::NATIVE_DOUBLE, &res);
			header->resolutionY = res;
			attr.close();
		}

		if (tile.attrExists("ResolutionZ")) {
			auto attr = tile.openAttribute("ResolutionZ");
			double res;
			attr.read(H5::PredType::NATIVE_DOUBLE, &res);
			header->resolutionZ = res;
			attr.close();
		}

		tile.close();
		group.close();
		file.close();

	} catch (H5::Exception& e) {
		QLOG_ERROR() << QString::fromStdString(e.getDetailMsg());
		return false;
	} catch (...) {
		QLOG_ERROR() << "UNKNOWN ERROR";
		return false;
	}

	return true;
}

auto ThumbnailReader::Impl::ReadDataSetFL(const QString& groupName, int timeIndex, int ch, void** data) -> bool {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	try {
		auto file = H5::H5File(header->path.toStdString(), H5F_ACC_RDONLY);
		auto group = file.openGroup(groupName.toStdString());
		const auto tileName = QString("%1").arg(timeIndex, 6, 10, QChar('0'));
		auto tile = group.openDataSet(tileName.toStdString());

		H5::DataSpace dataSpace = tile.getSpace();

		hsize_t dims[3];
		if (dataSpace.getSimpleExtentDims(dims) < 3) {
			tile.close();
			group.close();
			file.close();

			return false;
		}

		const hsize_t count[3] = { 1, dims[1], dims[2] };
		const hsize_t offset[3] = { static_cast<hsize_t>(ch), 0, 0 };

		dataSpace.selectHyperslab(H5S_SELECT_SET, count, offset);

		const hsize_t outDims[2]{ dims[1], dims[2] };
		H5::DataSpace memoryspace(2, outDims);

		const auto buf = static_cast<unsigned char*>(*data);
		tile.read(buf, H5::PredType::NATIVE_UCHAR, memoryspace, dataSpace);
		

		// get attributes
		if (tile.attrExists("IsColor")) {
			auto attr = tile.openAttribute("IsColor");
			int64_t value;
			attr.read(H5::PredType::NATIVE_INT64, &value);
			header->isColor = (value > 0);
			attr.close();
		}

		if (tile.attrExists("SizeX")) {
			auto attr = tile.openAttribute("SizeX");
			int64_t size;
			attr.read(H5::PredType::NATIVE_INT64, &size);
			header->sizeX = size;
			attr.close();
		}

		if (tile.attrExists("SizeY")) {
			auto attr = tile.openAttribute("SizeY");
			int64_t size;
			attr.read(H5::PredType::NATIVE_INT64, &size);
			header->sizeY = size;
			attr.close();
		}

		if (tile.attrExists("SizeZ")) {
			auto attr = tile.openAttribute("SizeZ");
			int64_t size;
			attr.read(H5::PredType::NATIVE_INT64, &size);
			header->sizeZ = size;
			attr.close();
		}

		if (tile.attrExists("ResolutionX")) {
			auto attr = tile.openAttribute("ResolutionX");
			double res;
			attr.read(H5::PredType::NATIVE_DOUBLE, &res);
			header->resolutionX = res;
			attr.close();
		}

		if (tile.attrExists("ResolutionY")) {
			auto attr = tile.openAttribute("ResolutionY");
			double res;
			attr.read(H5::PredType::NATIVE_DOUBLE, &res);
			header->resolutionY = res;
			attr.close();
		}

		if (tile.attrExists("ResolutionZ")) {
			auto attr = tile.openAttribute("ResolutionZ");
			double res;
			attr.read(H5::PredType::NATIVE_DOUBLE, &res);
			header->resolutionZ = res;
			attr.close();
		}

		memoryspace.close();
		dataSpace.close();
		tile.close();
		group.close();
		file.close();

	} catch (H5::Exception& e) {
		QLOG_ERROR() << QString::fromStdString(e.getDetailMsg());
		return false;
	} catch (...) {
		QLOG_ERROR() << "UNKNOWN ERROR";
		return false;
	}

	return true;
}

auto ThumbnailReader::SetFileName(const QString& name) -> void {
	d->header->path = name;
}

auto ThumbnailReader::GetSize() const -> std::tuple<int64_t, int64_t, int64_t> {
	return std::make_tuple(d->header->sizeX, d->header->sizeY, d->header->sizeZ);
}

auto ThumbnailReader::GetResolution() const -> std::tuple<double, double, double> {
	return std::make_tuple(d->header->resolutionX, d->header->resolutionY, d->header->resolutionZ);
}

auto ThumbnailReader::ReadHT(int timeIndex, void** data) const -> int {
	d->ReadDataSet(HTGroup.data(), timeIndex, data);
	return 0;	// TODO: return read buffer size
}

auto ThumbnailReader::ReadFL(int timeIndex, int channel, void** data) const -> int {
	d->ReadDataSetFL(FLGroup.data(), timeIndex, channel, data);
	return 0;	// TODO: return read buffer size
}

auto ThumbnailReader::ReadBF(int timeIndex, void** data) const -> int {
	d->ReadDataSet(BFGroup.data(), timeIndex, data);
	return 0;	// TODO: return read buffer size
}