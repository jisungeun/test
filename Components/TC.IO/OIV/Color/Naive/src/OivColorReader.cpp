
#pragma warning(push)
#pragma warning(disable : 4819)
#include <Inventor/SoInput.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "OivColorReader.h"
#include "TCFColor.h"



SO_FIELDCONTAINER_SOURCE(OivColorReader);

struct OivColorReader::Impl {
	ColorReader* _hdfReader;
	//SoLDMTopoOctree* _ldmTopo;
	int _tileSize;
	int _channel;
};

OivColorReader::OivColorReader()
	:d(new Impl())
{	
	d->_hdfReader = new ColorReader;
	//d->_ldmTopo = NULL;
	m_dataConverted = FALSE;
	
	d->_tileSize = 0;

	SO_FIELDCONTAINER_CONSTRUCTOR(OivColorReader);
}

OivColorReader::~OivColorReader()
{
	delete d->_hdfReader;
}

auto OivColorReader::setColorChannel(int ch) -> void {
	d->_channel = ch;
}


auto OivColorReader::setTileDimension(int tileSize) -> void
{
	d->_tileSize = tileSize;
}

auto OivColorReader::setTileName(const std::string& tileName) -> void
{
	auto header = d->_hdfReader->GetColorHeader();
	header->tileName = tileName;
}

auto OivColorReader::setDataGroupPath(const std::string& dataGroup) -> void
{
	auto header = d->_hdfReader->GetColorHeader();
	header->dataGroup = dataGroup;
}

auto OivColorReader::setFilename(const SbString& filename) -> int
{
	if (d->_hdfReader != NULL)
	{
		int errStatus = d->_hdfReader->SetFileName(filename.toStdString());
		if (errStatus < 0)
			return -1;
		else
		{
			auto header = d->_hdfReader->GetColorHeader();
			//d->_ldmTopo = new SoLDMTopoOctree;
			//d->_ldmTopo->init(SbVec3i32(header->sizeX, header->sizeY, header->sizeZ), d->_tileSize);
			//d->_ldmTopo->init(SbVec3i32(header->sizeX, header->sizeY, 1), d->_tileSize);

			m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));			

			return 1;
		}
	}
	return 0;
}
auto OivColorReader::getOffsetZ() -> double {
	auto header = d->_hdfReader->GetColorHeader();
	double offset = header->offsetZ;
	int size_z = header->sizeZ;
	double resolutionZ = header->resolutionZ;

	return (offset - ((double)size_z * resolutionZ) / 2.0);
}

auto OivColorReader::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm) -> SoVolumeReader::ReadError
{	
	auto header = d->_hdfReader->GetColorHeader();
	SbVec3f _min = SbVec3f((float)(-header->sizeX * header->resolutionX) / 2.f,
		(float)(-header->sizeY * header->resolutionY) / 2.f,
		(float)(-header->sizeZ * header->resolutionZ) / 2.f);
	SbVec3f _max = SbVec3f((float)(header->sizeX * header->resolutionX)/2.f,
						   (float)(header->sizeY * header->resolutionY)/2.f,
						   (float)(header->sizeZ * header->resolutionZ)/2.f);

	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	idm = SbVec3i32(header->sizeX, header->sizeY, header->sizeZ);

	return RD_NO_ERROR;
}

auto OivColorReader::getBorderFlag() -> int
{
	return 0;
}

auto OivColorReader::getNumSignificantBits() -> int
{
	return 0;
}

auto OivColorReader::getTileSize(SbVec3i32& size) -> SbBool
{
	size = SbVec3i32(d->_tileSize, d->_tileSize, 1);
	return TRUE;
}

auto OivColorReader::getMinMax(int64_t& , int64_t& ) -> SbBool
{
	return FALSE;
}

auto OivColorReader::getMinMax(double& , double& ) -> SbBool
{
	return FALSE;
}

auto OivColorReader::readTile(int , const SbBox3i32& ) -> SoBufferObject*
{
	std::cerr << "OivCustomColor::readTile : Not Implemented" << std::endl;
	return NULL;
}

auto OivColorReader::getSubSlice(const SbBox2i32&, int, void*) -> void
{
	std::cerr << "OivCustomColor::getSubSlice : Not Implemented" << std::endl;
	return;
}

auto OivColorReader::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void
{
	SoCpuBufferObject* cpuObj = static_cast<SoCpuBufferObject*>(bufferObject);
	
	SbVec3i32 offset(subSlice.getMin()[0], subSlice.getMin()[1], sliceNumber);
	SbVec3i32 size(subSlice.getMax()[0] - subSlice.getMin()[0] + 1, subSlice.getMax()[1] - subSlice.getMin()[1] + 1, 1);

	//auto rsize = size[0] * size[1] * size[2] * 4;

	void* data = cpuObj->map(SoCpuBufferObject::SET);

	try {
		d->_hdfReader->ReadColorData2D(offset, size, &data,d->_channel);
	} catch (...) {

	}

	cpuObj->unmap();
}

auto OivColorReader::getHistogram(std::vector<int64_t>& ) -> SbBool
{
	return FALSE;
}

auto OivColorReader::isDataConverted() const -> SbBool
{
	return FALSE;
}

auto OivColorReader::isRGBA() const -> SbBool
{
	//return TRUE;
	return FALSE;
}

auto OivColorReader::isThreadSafe() const -> SbBool
{
	return TRUE;
}

auto OivColorReader::initClass() -> void
{
	SO_FIELDCONTAINER_INIT_CLASS(OivColorReader, "OivCustomColorReader", SoVolumeReader);
}

auto OivColorReader::exitClass() -> void
{
	SO__FIELDCONTAINER_EXIT_CLASS(OivColorReader);
}

auto OivColorReader::getTimeSteps()->int {
	if(nullptr == d->_hdfReader) {
		return -1;
	}

	auto header = d->_hdfReader->GetColorHeader();
	if (nullptr == header) {
		return -1;
	}

	return header->dataCount;
}