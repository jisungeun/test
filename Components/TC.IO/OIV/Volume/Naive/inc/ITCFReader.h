#pragma once

#pragma warning(push)
#pragma warning(disable:4268)
#include "hdf5.h"
#include "H5Cpp.h"
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SbVec.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

typedef struct HEADER_OF_TCF
{
	int			dataCount = -1;
	double		resolutionZ = -1.0;
	double		resolutionX = -1.0;
	double		resolutionY = -1.0;
	double      offsetZ = -1.0;
	int			oriZ = -1;
	int			oriX = -1;
	int			oriY = -1;
	int			sizeZ = -1;
	int			sizeX = -1;
	int			sizeY = -1;
	int			cropOffsetX = 0;
	int			cropOffsetY = 0;
	float		timeInterval = -1.f;
	float		maxIntensity = -1.f;
	float		minIntensity = -1.f;
	float		riMin = -1.f;
	float		riMax = -1.f;
	float		positionC = -1.f;
	float		positionX = -1.f;
	float		positionY = -1.f;
	float		positionZ = -1.f;
	int			dataType = -1;
	std::string dataGroup;
	std::string tileName;
	std::string filePath;
} TCFHeader;

class ITCFReader {
public:
	ITCFReader();
	~ITCFReader();

	virtual auto GetTCFHeader()->TCFHeader* = 0;
	virtual auto SetFilename(const std::string& name)->int = 0;
	virtual auto CreateDummyData(const SbVec3i32& offset, const SbVec3i32& dataSize, void* data)->void = 0;
	virtual auto ReadTCFData(const SbVec3i32& offset, const SbVec3i32& dataSize, void* data)->int = 0;
	virtual auto ReadTCFData2D(const hid_t& setId, const SbVec3i32& offset, const SbVec3i32& dataSize, void** data)->int = 0;
	virtual auto ReadTCFData2D(H5::DataSet& dataset, H5::DataSpace& dataspace, const SbVec3i32& offset, const SbVec3i32& dataSize, void* data)->int = 0;
	virtual auto ReadHTMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header)->bool = 0;
	virtual auto ReadFLMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header)->bool = 0;
	virtual auto ReadFLCommonMeta(const std::string& filePath, const std::string& dataGroup, TCFHeader& header)->bool = 0;
	virtual auto ReadBFMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header)->bool = 0;	
};