#pragma once

#include "ITCFReader.h"

class TCFReader8Bit : public ITCFReader
{
public:
	TCFReader8Bit(bool isFL = false);
	TCFReader8Bit(const std::string& name, const std::string& dataGroup = "/Data/3D", const std::string& tileName = "000000");
	~TCFReader8Bit(void);

	auto GetTCFHeader()->TCFHeader* override;
	auto SetFilename(const std::string& name) -> int override;
	auto CreateDummyData(const SbVec3i32& offset, const SbVec3i32& dataSize, void* data) -> void override;
	auto ReadTCFData(const SbVec3i32& offset, const SbVec3i32& dataSize, void* data) -> int override;
	auto ReadTCFData2D(const hid_t& setId, const SbVec3i32& offset, const SbVec3i32& dataSize, void** data) -> int override;
	auto ReadTCFData2D(H5::DataSet& dataset, H5::DataSpace& dataspace, const SbVec3i32& offset, const SbVec3i32& dataSize, void* data) -> int override;
	auto ReadHTMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header) -> bool override;
	auto ReadFLMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header) -> bool override;
	auto ReadFLCommonMeta(const std::string& filePath, const std::string& dataGroup, TCFHeader& header) -> bool override;
	auto ReadBFMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header) -> bool override;

private:
	int Init(void);
	int OpenHdfFile(const std::string& name);
	int CloseHdfFile(void);

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
