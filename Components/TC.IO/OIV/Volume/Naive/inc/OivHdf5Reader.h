#pragma once

// OIV Headers

#include "TCFClasses.h"
#include "TCFClasses8Bit.h"

#include "TC.IO.OIV.Volume.NaiveExport.h"

class TC_IO_OIV_Volume_Naive_API OivHdf5Reader : public SoVolumeReader
{
	SO_FIELDCONTAINER_HEADER(OivHdf5Reader);

public:
	OivHdf5Reader(bool isFL = false,bool is8Bit=false);
	~OivHdf5Reader();
	auto setTileDimension(int tileSize) -> void;
	auto setTileName(const std::string& tileName) -> void;
	auto setXYCropOffset(const int& offsetX, const int& offsetY)->void;
	auto setXYCropSize(const int& sizeX, const int& sizeY)->void;
	auto setDataGroupPath(const std::string& dataGroup) -> void;
	auto getOffsetZ()->double;
	auto set8Bit(bool is8Bit)->void;
	auto setZOffset(float offset)->void;

	virtual auto setFilename(const SbString& filename) -> int override;
	virtual auto getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm)->SoVolumeReader::ReadError override;
	virtual auto getBorderFlag() -> int override;
	virtual auto getNumSignificantBits() -> int override;	
	virtual auto getTileSize(SbVec3i32& size)->SbBool override;
	virtual auto getMinMax(int64_t& min, int64_t& max)->SbBool override;
	virtual auto getMinMax(double& min, double& max)->SbBool override;
	virtual auto readTile(int index, const SbBox3i32& tilePosition)->SoBufferObject* override;
	virtual auto getSubSlice(const SbBox2i32&, int, void*) -> void override;
	virtual auto getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void override;
	virtual auto getHistogram(std::vector<int64_t>& numVox)->SbBool override;
	virtual auto isDataConverted() const->SbBool override;
	virtual auto isRGBA() const->SbBool override;
	virtual auto isThreadSafe() const->SbBool override;	

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
