#include "TCFClasses.h"

#include <QString>
#include <QStringList>
#include <HDF5Mutex.h>

struct TCFReader::Impl {
	H5::H5File file;
	H5::Group group;
	int minor_version{ 0 };
	TCFHeader* header = new TCFHeader;

	bool isFL{ false };
	bool is8Bit{ false };
};

TCFReader::TCFReader(bool isFL)
	:d(new Impl()) {
	this->Init();
	d->isFL = isFL;
}

TCFReader::TCFReader(const std::string& name, const std::string& dataGroup, const std::string& tileName)
	: d(new Impl()) {
	this->Init();
	d->header->dataGroup = dataGroup;
	d->header->tileName = tileName;	
	this->SetFilename(name);
}

TCFReader::~TCFReader() {
	delete d->header;

	CloseHdfFile();
}

auto TCFReader::SetIs8Bit(bool is8Bit) -> void {
	d->is8Bit = is8Bit;
	if(d->header) {
		if (is8Bit) {
			d->header->dataType = SoDataSet::UNSIGNED_BYTE;
		}
		else {
			d->header->dataType = SoDataSet::UNSIGNED_SHORT;
		}
	}
}

auto TCFReader::GetTCFHeader() -> TCFHeader* {
	return d->header;
}

auto TCFReader::SetFilename(const std::string& name) -> int {
	if (OpenHdfFile(name) < 0)
	{
		return -1;
	}
	return 0;
}

auto TCFReader::CreateDummyData(const SbVec3i32& offset, const SbVec3i32& dataSize, void* data) -> void {
	unsigned short* cast_data = static_cast<unsigned short*>(data);

	int x, y, z;
	dataSize.getValue(x, y, z);
	auto dummyData = new unsigned short[x * y * z]();
	for (auto i = 0; i<x * y * z; i++) {
		dummyData[i] = 14900;
	}
	memcpy(cast_data, dummyData, x * y * z * sizeof(unsigned short));	

	delete[] dummyData;
}

auto TCFReader::ReadTCFData(const SbVec3i32& offset, const SbVec3i32& dataSize, void* data) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	H5::DataSet dataset = d->group.openDataSet(d->header->tileName.c_str());
	
	H5::DataSpace dataSpace = dataset.getSpace();

	hsize_t dims[3];
	int dim = dataSpace.getSimpleExtentDims(dims);

	if (dim == 2) {
		ReadTCFData2D(dataset, dataSpace, offset, dataSize, data);
		return 1;
	}

	// The start, stride, count, and block arrays must be the same size as the rank of the dataspace.
	// Stride parameter is NULL, a contiguous hyperslab is selected
	hsize_t tcfOffset[3] = { (hsize_t)(offset[2]), (hsize_t)(offset[1] + d->header->cropOffsetY), (hsize_t)(offset[0] + d->header->cropOffsetX) };

	// How many blocks to select from the dataspace, in each dimension.
	hsize_t tcfCount[3] = { (hsize_t)(dataSize[2]), (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]) };

	dataSpace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };
	memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);

	if (d->minor_version < 3) {
		unsigned short* cast_data = static_cast<unsigned short*>(data);
		auto double_data = std::shared_ptr<double[]>(new double[(hsize_t)dataSize[0] * (hsize_t)dataSize[1]]);
		dataset.read(double_data.get(), H5::PredType::NATIVE_DOUBLE, memoryspace, dataSpace);
		for (auto i = 0; i < dataSize[0] * dataSize[1]; i++) {
			cast_data[i] = double_data.get()[i] * 10000.0f;
		}
		double_data = nullptr;
	}
	else {
		if(d->is8Bit) {
			unsigned char* cast_data = static_cast<unsigned char*>(data);
			dataset.read(cast_data, H5::PredType::NATIVE_UCHAR, memoryspace, dataSpace);
		}else {
			unsigned short* cast_data = static_cast<unsigned short*>(data);
			dataset.read(cast_data, H5::PredType::NATIVE_UINT16, memoryspace, dataSpace);
		}		
	}

	memoryspace.close();
	dataSpace.close();
	dataset.close();	

	return 1;
}

auto TCFReader::ReadTCFData2D(H5::DataSet& dataset, H5::DataSpace& dataspace, const SbVec3i32& offset, const SbVec3i32& dataSize, void* data) -> int {
    // The start, stride, count, and block arrays must be the same size as the rank of the dataspace.
	// Stride parameter is NULL, a contiguous hyperslab is selected
	hsize_t tcfOffset[2] = { (hsize_t)(offset[1]), (hsize_t)(offset[0]) };
	// How many blocks to select from the dataspace, in each dimension.
	hsize_t tcfCount[2] = { (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]) };

	dataspace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };

	memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);

	if (d->minor_version < 3) {
		unsigned short* cast_data = static_cast<unsigned short*>(data);
		auto double_data = std::shared_ptr<double[]>(new double[(hsize_t)dataSize[0] * (hsize_t)dataSize[1]]);
		dataset.read(double_data.get(), H5::PredType::NATIVE_DOUBLE, memoryspace, dataspace);
		for (auto i = 0; i < dataSize[0] * dataSize[1]; i++) {
			cast_data[i] = double_data.get()[i] * 10000.0f;
		}
		double_data = nullptr;
	}
	else {
		if(d->is8Bit) {
			unsigned char* cast_data = static_cast<unsigned char*>(data);
			dataset.read(cast_data, H5::PredType::NATIVE_UCHAR, memoryspace, dataspace);
		}
		else {
			unsigned short* cast_data = static_cast<unsigned short*>(data);
			dataset.read(cast_data, H5::PredType::NATIVE_UINT16, memoryspace, dataspace);
		}
	}

	memoryspace.close();
	dataspace.close();
	dataset.close();

	return 1;
}

auto TCFReader::ReadTCFData2D(const hid_t& setId, const SbVec3i32& offset, const SbVec3i32& dataSize, void** data) -> int {
	//deprecated
	unsigned short* cast_data = (unsigned short*)(*data);
	hid_t typeId = H5Dget_type(setId);
	hid_t fileSpaceId = H5Dget_space(setId);

	// The start, stride, count, and block arrays must be the same size as the rank of the dataspace.
	// Stride parameter is NULL, a contiguous hyperslab is selected
	hsize_t tcfOffset[2] = { (hsize_t)(offset[1]), (hsize_t)(offset[0]) };
	// How many blocks to select from the dataspace, in each dimension.
	hsize_t tcfCount[2] = { (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]) };

	herr_t errStatus = H5Sselect_hyperslab(fileSpaceId, H5S_SELECT_SET, tcfOffset, NULL,
		tcfCount, NULL);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };
	hid_t memspace = H5Screate_simple(out_rank, out_dim, NULL);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };
	errStatus = H5Sselect_hyperslab(memspace, H5S_SELECT_SET, out_offset, NULL,
		out_count, NULL);

	errStatus = H5Dread(setId, typeId, memspace, fileSpaceId, H5P_DEFAULT, cast_data);

	errStatus = H5Tclose(typeId);
	errStatus = H5Sclose(fileSpaceId);
	errStatus = H5Sclose(memspace);
	errStatus = H5Dclose(setId);

	return 1;
}

auto TCFReader::ReadHTMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header) -> bool {
	if (true == filePath.empty()) {
		return false;
	}

	if (true == dataGroup.empty()) {
		return false;
	}

	if (true == tileName.empty()) {
		return false;
	}

	if (0 > H5open()) {
		return false;
	}

	hid_t fileId = H5Fopen(filePath.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	if (0 > fileId) {
		return false;
	}

	if (1 > H5Lexists(fileId, dataGroup.c_str(), H5P_DEFAULT)) {
		return false;
	}

	// Find the 2D group
	// Read the attributes
	// have to fill data group & tilename 
	const hid_t groupId = H5Gopen(fileId, dataGroup.c_str(), H5P_DEFAULT);
	if (0 > groupId) {
		return false;
	}

	const hid_t setId = H5Dopen2(groupId, tileName.c_str(), H5P_DEFAULT);
	if (0 > setId) {
		return false;
	}

	const hid_t dataType = H5Dget_type(setId);
	if (0 > dataType) {
		return false;
	}

	const H5T_class_t t_class = H5Tget_class(dataType);
	header.dataType = SoDataSet::UNSIGNED_SHORT;

	H5O_info1_t objInfo;
	if (0 > H5Oget_info1(groupId, &objInfo)) {
		return false;
	}

	for (int i = 0; i < objInfo.num_attrs; ++i)
	{
		hid_t attributeId = H5Aopen_by_idx(groupId, ".", H5_INDEX_NAME, H5_ITER_INC, i, H5P_DEFAULT, H5P_DEFAULT);
		if (0 > attributeId) {
			continue;
		}

		const hid_t attributeType = H5Aget_type(attributeId);
		if (0 > attributeType) {
			continue;
		}

		const hid_t nativeType = H5Tget_native_type(attributeType, H5T_DIR_ASCEND);
		if (0 > nativeType) {
			continue;
		}

		char attributebName[32];
		H5Aget_name(attributeId, 32, attributebName);

		if (H5Tequal(nativeType, H5T_NATIVE_INT64))
		{
			int64_t val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "DataCount"))
				header.dataCount = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeX"))
				header.sizeX = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeZ"))
				header.sizeZ = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeY"))
				header.sizeY = static_cast<int>(val);
		}
		else if (H5Tequal(nativeType, H5T_NATIVE_DOUBLE))
		{
			double val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "ResolutionX"))
				header.resolutionX = val;
			else if (!strcmp(attributebName, "ResolutionZ"))
				header.resolutionZ = val;
			else if (!strcmp(attributebName, "ResolutionY"))
				header.resolutionY = val;
			else if (!strcmp(attributebName, "TimeInterval"))
				header.timeInterval = static_cast<float>(val);
			else if (!strcmp(attributebName, "RIMin"))
				header.riMin = val;
			else if (!strcmp(attributebName, "RIMax"))
				header.riMax = val;
		}

		header.dataGroup = dataGroup;
		header.tileName = tileName;

		H5Tclose(nativeType);
		H5Tclose(attributeType);
		H5Aclose(attributeId);

	}

	return true;
}

auto TCFReader::ReadFLMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header) -> bool {
	if (true == filePath.empty()) {
		return false;
	}

	if (true == dataGroup.empty()) {
		return false;
	}

	if (true == tileName.empty()) {
		return false;
	}

	if (0 > H5open()) {
		return false;
	}

	hid_t fileId = H5Fopen(filePath.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	if (0 > fileId) {
		return false;
	}

	if (1 > H5Lexists(fileId, dataGroup.c_str(), H5P_DEFAULT)) {
		return false;
	}

	// Find the 2D group
	// Read the attributes
	// have to fill data group & tilename 
	const hid_t groupId = H5Gopen(fileId, dataGroup.c_str(), H5P_DEFAULT);
	if (0 > groupId) {
		return false;
	}

	const hid_t setId = H5Dopen2(groupId, tileName.c_str(), H5P_DEFAULT);
	if (0 > setId) {
		return false;
	}

	const hid_t dataType = H5Dget_type(setId);
	if (0 > dataType) {
		return false;
	}

	const H5T_class_t t_class = H5Tget_class(dataType);
	header.dataType = SoDataSet::UNSIGNED_SHORT;


	H5O_info1_t objInfo;
	if (0 > H5Oget_info1(setId, &objInfo)) {
		return false;
	}

	for (int i = 0; i < objInfo.num_attrs; ++i)
	{
		hid_t attributeId = H5Aopen_by_idx(setId, ".", H5_INDEX_NAME, H5_ITER_INC, i, H5P_DEFAULT, H5P_DEFAULT);
		if (0 > attributeId) {
			continue;
		}

		const hid_t attributeType = H5Aget_type(attributeId);
		if (0 > attributeType) {
			continue;
		}

		const hid_t nativeType = H5Tget_native_type(attributeType, H5T_DIR_ASCEND);
		if (0 > nativeType) {
			continue;
		}

		char attributebName[32];
		H5Aget_name(attributeId, 32, attributebName);

		if (H5Tequal(nativeType, H5T_NATIVE_INT64))
		{
		}
		else if (H5Tequal(nativeType, H5T_NATIVE_DOUBLE))
		{
			double val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "MaxIntensity"))
				header.maxIntensity = val;
			else if (!strcmp(attributebName, "MinIntensity"))
				header.minIntensity = val;
			else if (!strcmp(attributebName, "PositionC"))
				header.positionC = val;
			else if (!strcmp(attributebName, "PositionX"))
				header.positionX = val;
			else if (!strcmp(attributebName, "PositionY"))
				header.positionY = val;
			else if (!strcmp(attributebName, "PositionZ"))
				header.positionZ = val;
		}

		header.dataGroup = dataGroup;
		header.tileName = tileName;

		H5Tclose(nativeType);
		H5Tclose(attributeType);
		H5Aclose(attributeId);

	}

	return true;
}

auto TCFReader::ReadFLCommonMeta(const std::string& filePath, const std::string& dataGroup, TCFHeader& header) -> bool {
	if (true == filePath.empty()) {
		return false;
	}

	if (true == dataGroup.empty()) {
		return false;
	}

	if (0 > H5open()) {
		return false;
	}

	hid_t fileId = H5Fopen(filePath.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	if (0 > fileId) {
		return false;
	}

	if (1 > H5Lexists(fileId, dataGroup.c_str(), H5P_DEFAULT)) {
		return false;
	}

	// Find the 2D group
	// Read the attributes
	// have to fill data group & tilename 
	const hid_t groupId = H5Gopen(fileId, dataGroup.c_str(), H5P_DEFAULT);
	if (0 > groupId) {
		return false;
	}

	H5O_info1_t objInfo;
	if (0 > H5Oget_info1(groupId, &objInfo)) {
		return false;
	}

	for (int i = 0; i < objInfo.num_attrs; ++i)
	{
		hid_t attributeId = H5Aopen_by_idx(groupId, ".", H5_INDEX_NAME, H5_ITER_INC, i, H5P_DEFAULT, H5P_DEFAULT);
		if (0 > attributeId) {
			continue;
		}

		const hid_t attributeType = H5Aget_type(attributeId);
		if (0 > attributeType) {
			continue;
		}

		const hid_t nativeType = H5Tget_native_type(attributeType, H5T_DIR_ASCEND);
		if (0 > nativeType) {
			continue;
		}

		char attributebName[32];
		H5Aget_name(attributeId, 32, attributebName);

		if (H5Tequal(nativeType, H5T_NATIVE_INT64))
		{
			int64_t val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "DataCount"))
				header.dataCount = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeX"))
				header.sizeX = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeZ"))
				header.sizeZ = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeY"))
				header.sizeY = static_cast<int>(val);
		}
		else if (H5Tequal(nativeType, H5T_NATIVE_DOUBLE))
		{
			double val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "ResolutionX"))
				header.resolutionX = val;
			else if (!strcmp(attributebName, "ResolutionZ"))
				header.resolutionZ = val;
			else if (!strcmp(attributebName, "ResolutionY"))
				header.resolutionY = val;
			else if (!strcmp(attributebName, "TimeInterval"))
				header.timeInterval = static_cast<float>(val);
			else if (!strcmp(attributebName, "RIMin"))
				header.riMin = val;
			else if (!strcmp(attributebName, "RIMax"))
				header.riMax = val;
		}

		header.dataGroup = dataGroup;

		H5Tclose(nativeType);
		H5Tclose(attributeType);
		H5Aclose(attributeId);
	}

	return true;
}

auto TCFReader::ReadBFMeta(const std::string& filePath, const std::string& dataGroup, const std::string& tileName, TCFHeader& header) -> bool {
	if (true == filePath.empty()) {
		return false;
	}

	if (true == dataGroup.empty()) {
		return false;
	}

	if (true == tileName.empty()) {
		return false;
	}

	if (0 > H5open()) {
		return false;
	}

	hid_t fileId = H5Fopen(filePath.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	if (0 > fileId) {
		return false;
	}

	if (1 > H5Lexists(fileId, dataGroup.c_str(), H5P_DEFAULT)) {
		return false;
	}

	// Find the 2D group
	// Read the attributes
	// have to fill data group & tilename 
	const hid_t groupId = H5Gopen(fileId, dataGroup.c_str(), H5P_DEFAULT);
	if (0 > groupId) {
		return false;
	}

	const hid_t setId = H5Dopen2(groupId, tileName.c_str(), H5P_DEFAULT);
	if (0 > setId) {
		return false;
	}

	const hid_t dataType = H5Dget_type(setId);
	if (0 > dataType) {
		return false;
	}

	const H5T_class_t t_class = H5Tget_class(dataType);
	header.dataType = SoDataSet::UNSIGNED_SHORT;

	H5O_info1_t objInfo;
	if (0 > H5Oget_info1(groupId, &objInfo)) {
		return false;
	}

	for (int i = 0; i < objInfo.num_attrs; ++i)
	{
		hid_t attributeId = H5Aopen_by_idx(groupId, ".", H5_INDEX_NAME, H5_ITER_INC, i, H5P_DEFAULT, H5P_DEFAULT);
		if (0 > attributeId) {
			continue;
		}

		const hid_t attributeType = H5Aget_type(attributeId);
		if (0 > attributeType) {
			continue;
		}

		const hid_t nativeType = H5Tget_native_type(attributeType, H5T_DIR_ASCEND);
		if (0 > nativeType) {
			continue;
		}

		char attributebName[32];
		H5Aget_name(attributeId, 32, attributebName);

		if (H5Tequal(nativeType, H5T_NATIVE_INT64))
		{
			int64_t val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "DataCount"))
				header.dataCount = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeX"))
				header.sizeX = static_cast<int>(val);
			else if (!strcmp(attributebName, "SizeY"))
				header.sizeY = static_cast<int>(val);
		}
		else if (H5Tequal(nativeType, H5T_NATIVE_DOUBLE))
		{
			double val = 0;
			if (0 > H5Aread(attributeId, nativeType, &val)) {
				continue;
			}

			if (!strcmp(attributebName, "ResolutionX"))
				header.resolutionX = val;
			else if (!strcmp(attributebName, "ResolutionY"))
				header.resolutionY = val;
			else if (!strcmp(attributebName, "TimeInterval"))
				header.timeInterval = static_cast<float>(val);
		}

		header.dataGroup = dataGroup;
		header.tileName = tileName;

		H5Tclose(nativeType);
		H5Tclose(attributeType);
		H5Aclose(attributeId);

	}

	return true;
}

// private
int TCFReader::Init(void) {
	d->header->oriX = 1;
	d->header->oriY = 1;
	d->header->oriZ = 1;
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_SHORT;

	return H5open();
}

//	Open HDF5 file, finds Data group and reads Attributes
int TCFReader::OpenHdfFile(const std::string& name) {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	d->file.openFile(name, H5F_ACC_RDONLY);	
	auto ver_attr = d->file.openAttribute("FormatVersion");
	H5std_string h5str;

	H5::DataType dt = ver_attr.getDataType();
	ver_attr.read(dt, h5str);
	ver_attr.close();

	d->minor_version = QString(h5str.c_str()).split(".")[1].toInt();

	// Find the 2D group
	herr_t errStatus;

	// Read the attributes
	d->group = d->file.openGroup(d->header->dataGroup.c_str());

	if (d->is8Bit) {
		d->header->dataType = SoDataSet::UNSIGNED_BYTE;//
	}else {
		d->header->dataType = SoDataSet::UNSIGNED_SHORT;//
	}

	if(d->isFL) {
		d->group.close();
		auto realName = QString(d->header->dataGroup.c_str()).split("/CH")[0].toStdString();
		d->group = d->file.openGroup(realName.c_str());
	}
	
	if(d->group.attrExists("DataCount")) {
		int64_t val;
		auto attr = d->group.openAttribute("DataCount");
		attr.read(H5::PredType::NATIVE_INT64, &val);
		attr.close();
		d->header->dataCount = val;
	}
	if (d->group.attrExists("SizeX")) {
		int64_t val;
		auto attr = d->group.openAttribute("SizeX");
		attr.read(H5::PredType::NATIVE_INT64, &val);
		attr.close();
		d->header->oriX = d->header->sizeX = val;
	}
	if (d->group.attrExists("SizeY")) {
		int64_t val;
		auto attr = d->group.openAttribute("SizeY");
		attr.read(H5::PredType::NATIVE_INT64, &val);
		attr.close();
		d->header->oriY = d->header->sizeY = val;
	}
	if (d->group.attrExists("SizeZ")) {
		int64_t val;
		auto attr = d->group.openAttribute("SizeZ");
		attr.read(H5::PredType::NATIVE_INT64, &val);
		attr.close();
		d->header->oriZ = d->header->sizeZ = val;
	}
	if (d->group.attrExists("ResolutionX")) {
		double val;
		auto attr = d->group.openAttribute("ResolutionX");
		attr.read(H5::PredType::NATIVE_DOUBLE, &val);
		attr.close();
		d->header->resolutionX = val;
	}
	if (d->group.attrExists("ResolutionY")) {
		double val;
		auto attr = d->group.openAttribute("ResolutionY");
		attr.read(H5::PredType::NATIVE_DOUBLE, &val);
		attr.close();
		d->header->resolutionY = val;
	}
	if (d->group.attrExists("ResolutionZ")) {
		double val;
		auto attr = d->group.openAttribute("ResolutionZ");
		attr.read(H5::PredType::NATIVE_DOUBLE, &val);
		attr.close();
		d->header->resolutionZ = val;
	}
	if (d->group.attrExists("TimeInterval")) {
		double val;
		auto attr = d->group.openAttribute("TimeInterval");
		attr.read(H5::PredType::NATIVE_DOUBLE, &val);
		attr.close();
		d->header->timeInterval = static_cast<float>(val);
	}
	if (d->group.attrExists("OffsetZ")) {
		double val;
		auto attr = d->group.openAttribute("OffsetZ");
		attr.read(H5::PredType::NATIVE_DOUBLE, &val);
		attr.close();
		d->header->offsetZ = val;
	}	

	if(d->isFL) {
		d->group.close();
		d->group = d->file.openGroup(d->header->dataGroup.c_str());
	}
	
	return 0;
}

int TCFReader::CloseHdfFile() {
	herr_t errStatus = -1;

	d->group.close();
	d->file.close();
	return errStatus;
}
