#include <QGridLayout>

#pragma warning(push)
#pragma warning(disable : 4819)

#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/SoInput.h>
#include <LDM/nodes/SoMultiDataSeparator.h>

// VolumeViz Header
#include <LDM/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>

//IvTune
#include <IvTune/SoIvTune.h>

#pragma warning(pop)

#include "OivHdf5Reader.h"


SO_FIELDCONTAINER_SOURCE(OivHdf5Reader);

struct OivHdf5Reader::Impl {
	//TCFReader* _hdfReader;
	std::shared_ptr<TCFReader> _hdfReader{ nullptr };
	int _tileSize;
	bool isXYCrop{ false };
	int xyCropOffset[2]{ 0,0 };
	int xyCropSize[2]{ 0,0 };
	int sigBit{ 16 };
	bool isFL{ false };
	float zOffset{ 0.0 };
};

OivHdf5Reader::OivHdf5Reader(bool isFL,bool is8Bit)
	:d(new Impl())
{
	d->isFL = isFL;
	d->_hdfReader = std::make_shared<TCFReader>(isFL);
	if (is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}
	else {
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
	m_dataConverted = FALSE;

	d->_tileSize = 0;

	SO_FIELDCONTAINER_CONSTRUCTOR(OivHdf5Reader);
}

OivHdf5Reader::~OivHdf5Reader()
{
	//delete d->_hdfReader;
}

auto OivHdf5Reader::setTileDimension(int tileSize) -> void
{
	d->_tileSize = tileSize;
}

auto OivHdf5Reader::set8Bit(bool is8Bit) -> void {
	d->_hdfReader->SetIs8Bit(is8Bit);
	if (is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}
	else {
		d->sigBit = 16;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
}

auto OivHdf5Reader::setTileName(const std::string& tileName) -> void
{
	TCFHeader* header = d->_hdfReader->GetTCFHeader();
	header->tileName = tileName;
}

auto OivHdf5Reader::setDataGroupPath(const std::string& dataGroup) -> void
{
	TCFHeader* header = d->_hdfReader->GetTCFHeader();
	header->dataGroup = dataGroup;
}

auto OivHdf5Reader::setXYCropOffset(const int& offsetX, const int& offsetY) -> void {
	d->isXYCrop = true;
	d->xyCropOffset[0] = offsetX;
	d->xyCropOffset[1] = offsetY;
}

auto OivHdf5Reader::setXYCropSize(const int& sizeX, const int& sizeY) -> void {
	d->isXYCrop = true;
	d->xyCropSize[0] = sizeX;
	d->xyCropSize[1] = sizeY;
}

auto OivHdf5Reader::setFilename(const SbString& filename) -> int
{
	if (d->_hdfReader != NULL)
	{
		int errStatus = d->_hdfReader->SetFilename(filename.toStdString());
		if (errStatus < 0)
			return -1;

	    TCFHeader* header = d->_hdfReader->GetTCFHeader();

	    if(d->isXYCrop) {
	        header->cropOffsetX = d->xyCropOffset[0];
	        header->cropOffsetY = d->xyCropOffset[1];
	        header->sizeX = d->xyCropSize[0];
	        header->sizeY = d->xyCropSize[1];
	    }

	    m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));			    
	}		
	return 0;
}
auto OivHdf5Reader::getOffsetZ() -> double {
	TCFHeader* header = d->_hdfReader->GetTCFHeader();
	double offset = header->offsetZ;
	int size_z = header->sizeZ;
	double resolutionZ = header->resolutionZ;

	return (offset - ((double)size_z * resolutionZ) / 2.0);
}

auto OivHdf5Reader::setZOffset(float offset) -> void {
	d->zOffset = offset;
}

auto OivHdf5Reader::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm) -> SoVolumeReader::ReadError
{	
	TCFHeader* header = d->_hdfReader->GetTCFHeader();

	auto xmin = header->cropOffsetX * header->resolutionX + (-header->oriX * header->resolutionX) / 2;
	auto ymin = header->cropOffsetY * header->resolutionY + (-header->oriY * header->resolutionY) / 2;
	auto zmin = -header->sizeZ * header->resolutionZ / 2.f + d->zOffset;
	auto xmax = header->cropOffsetX * header->resolutionX + (-header->oriX * header->resolutionX) / 2 + header->sizeX * header->resolutionX;
	auto ymax = header->cropOffsetY * header->resolutionY + (-header->oriY * header->resolutionY) / 2 + header->sizeY * header->resolutionY;
	auto zmax = header->sizeZ * header->resolutionZ / 2.f + d->zOffset;

	SbVec3f _min = SbVec3f(static_cast<float>(xmin), static_cast<float>(ymin), static_cast<float>(zmin));
	SbVec3f _max = SbVec3f(static_cast<float>(xmax), static_cast<float>(ymax), static_cast<float>(zmax));
	
	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	idm = SbVec3i32(header->sizeX, header->sizeY, header->sizeZ);	
	
	return RD_NO_ERROR;
}

auto OivHdf5Reader::getBorderFlag() -> int
{
	return 0;
}

auto OivHdf5Reader::getNumSignificantBits() -> int
{	
	return d->sigBit;
}

auto OivHdf5Reader::getTileSize(SbVec3i32& size) -> SbBool
{
	size = SbVec3i32(d->_tileSize, d->_tileSize, d->_tileSize);
	return TRUE;
}

auto OivHdf5Reader::getMinMax(int64_t& , int64_t& ) -> SbBool
{
	return FALSE;
}

auto OivHdf5Reader::getMinMax(double& , double& ) -> SbBool
{
	return FALSE;
}

auto OivHdf5Reader::readTile(int , const SbBox3i32& ) -> SoBufferObject*
{
	std::cerr << "OivCustomVRHdf::readTile : Not Implemented" << std::endl;
	return NULL;
}

auto OivHdf5Reader::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, void* data) -> void
{
	std::cerr << "OivCustomVRHdf::getSubSlice : Not Implemented" << std::endl;	
	return;
}

auto OivHdf5Reader::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void
{	
	SoRef<SoCpuBufferObject> cpuObj = dynamic_cast<SoCpuBufferObject*>(bufferObject);	

	SbVec3i32 offset(subSlice.getMin()[0], subSlice.getMin()[1], sliceNumber);
	SbVec3i32 size(subSlice.getMax()[0] - subSlice.getMin()[0]+1, subSlice.getMax()[1] - subSlice.getMin()[1]+1, 1);
		
	void* data = cpuObj->map(SoCpuBufferObject::SET);

	try {				
		//std::cout << d->_hdfReader->GetTCFHeader()->tileName << std::endl;
		d->_hdfReader->ReadTCFData(offset, size, data);
		//d->_hdfReader->CreateDummyData(offset, size, &data);
	}
	catch (...) {
		
	}

	cpuObj->unmap();	
	return;
}

auto OivHdf5Reader::getHistogram(std::vector<int64_t>& ) -> SbBool
{
	return FALSE;
}

auto OivHdf5Reader::isDataConverted() const -> SbBool
{
	return FALSE;
}

auto OivHdf5Reader::isRGBA() const -> SbBool
{
	return FALSE;
}

auto OivHdf5Reader::isThreadSafe() const -> SbBool
{
	return TRUE;
	//return FALSE;
}

auto OivHdf5Reader::initClass() -> void
{
	SO_FIELDCONTAINER_INIT_CLASS(OivHdf5Reader, "OivCustomHdfReader", SoVolumeReader);
}

auto OivHdf5Reader::exitClass() -> void
{
	SO__FIELDCONTAINER_EXIT_CLASS(OivHdf5Reader);
}
