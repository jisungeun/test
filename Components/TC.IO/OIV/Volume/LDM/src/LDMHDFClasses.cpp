#define LOGGER_TAG "[LDM]"
#include <TCLogger.h>

#include "LDMHDFClasses.h"

#include <iomanip>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <HDF5Mutex.h>

struct TCFLdmReader::Impl {
	LdmHeader* header{ nullptr };
	
	bool isFL{ false };	
	bool isBF{ false };
	bool is8Bit{ false };

	H5::H5File file;
};

TCFLdmReader::TCFLdmReader(bool isFL, bool isBF) : d{ new Impl } {
	Init();
	d->isFL = isFL;
	d->isBF = isBF;
	if(d->isBF) {
		SetIs8Bit(true);
	}
}

TCFLdmReader::~TCFLdmReader() {
	delete d->header;
	//std::cout << "CLose HDF5" << std::endl;
	CloseHdf5File();
	//H5close();
}

auto TCFLdmReader::ReadDataSetBF(const std::string _name, std::string _tile_name, void** _data, int& _rank, hsize_t* _dims, int ch) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	//H5::Group group(d->tcfGrpId);
	H5::Group group = d->file.openGroup(d->header->dataGroup);
	H5::Group tileGroup = group.openGroup(_name);
	H5::DataSet dataset = tileGroup.openDataSet(_tile_name);

	auto cast_data = static_cast<unsigned char*>(*_data);

	H5::DataSpace dataSpace = dataset.getSpace();

	hsize_t dims[3];
	int dim = dataSpace.getSimpleExtentDims(dims);

	if(dim == 2) {
		group.close();
		return 1;
	}
		
	hsize_t tcfOffset[3] = { static_cast<hsize_t>(ch),0,0 };
	hsize_t tcfCount[3] = { 1,dims[1],dims[2] };

	dataSpace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dims[1]),(hsize_t)(dims[2]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	dataset.read(cast_data, H5::PredType::NATIVE_UINT8, memoryspace, dataSpace);

	memoryspace.close();
	dataSpace.close();
	dataset.close();
	tileGroup.close();
	group.close();

	return 1;	
}

auto TCFLdmReader::SetIs8Bit(bool is8Bit) -> void {
	d->is8Bit = is8Bit;
	if(d->header) {
		if (is8Bit) {
			d->header->dataType = SoDataSet::UNSIGNED_BYTE;
		}else {
			d->header->dataType = SoDataSet::UNSIGNED_SHORT;
		}
	}
}

auto TCFLdmReader::ReadDataSet(std::string _tile_name, void** data) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	auto group = d->file.openGroup(d->header->dataGroup);
	auto dataSet = group.openGroup(d->header->tileName);
	auto tile = dataSet.openDataSet(_tile_name);
	if (d->is8Bit) {
		auto cast_data = static_cast<unsigned char*>(*data);
		tile.read(cast_data, H5::PredType::NATIVE_UCHAR);
	}else {
		auto cast_data = static_cast<unsigned short*>(*data);
		tile.read(cast_data, H5::PredType::NATIVE_UINT16);
	}	

	tile.close();
	dataSet.close();
	group.close();

	return 1;
}
/*
auto TCFLdmReader::ReadDataSet(hid_t _grpId, const std::string _name, std::string _tile_name, void** _data, int& _rank, hsize_t* _dims) -> int {
	//QMutexLocker locker(&d->mutex);
    TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

    herr_t errStatus;
	hid_t dSetId, memSpaceId, fileSpaceId;

	// Does not handle Chunked dataset as of now

	auto ggId = H5Gopen2(_grpId,_name.c_str(),H5P_DEFAULT);

	//dSetId = H5Dopen2(_grpId, _name.c_str(), H5P_DEFAULT);
	dSetId = H5Dopen2(ggId, _tile_name.c_str(), H5P_DEFAULT);
	if (dSetId < 0)
		return -1;

	hid_t typeId = H5Dget_type(dSetId);
	hid_t nativeType = H5Tget_native_type(typeId, H5T_DIR_ASCEND);
	size_t typeSize = H5Tget_size(nativeType);

	fileSpaceId = H5Dget_space(dSetId);

	int dRank = H5Sget_simple_extent_ndims(fileSpaceId);
	hsize_t* dims = new hsize_t[dRank];

	errStatus = H5Sget_simple_extent_dims(fileSpaceId, dims, NULL);
	size_t nElements = 1;

	for (int i = 0; i < dRank; ++i)
	{
		nElements *= dims[i];
		_dims[i] = dims[i];
	}
	_rank = dRank;

	//-------------------------------------------------------------
	//	Read dataset into buffer pointed by *_data
	if (nullptr == *_data)
	{
		*_data = malloc(nElements * typeSize);
		QLOG_ERROR() << "Null Pointer was allocated!";
	}
	memSpaceId = H5Screate_simple(dRank, dims, NULL);

	errStatus = H5Dread(dSetId, nativeType, memSpaceId, fileSpaceId, H5P_DEFAULT, *_data);

	errStatus = H5Tclose(nativeType);
	errStatus = H5Tclose(typeId);
	errStatus = H5Sclose(memSpaceId);
	errStatus = H5Sclose(fileSpaceId);
	errStatus = H5Dclose(dSetId);

	return (int)(typeSize * nElements);
}*/

auto TCFLdmReader::ReadHTLdmData3D(const std::string _tileName, void** _tileData) -> int {
	int tileRank;
	hsize_t tileDims[3];

	//int bufSize = ReadDataSet(d->tcfGrpId, d->header->tileName,_tileName, _tileData, tileRank, tileDims);
	int bufSize = ReadDataSet(_tileName, _tileData);

	return bufSize;
}

auto TCFLdmReader::ReadBFLdmData(const std::string _tileName, void** _tileData,int ch) -> int {
	int tileRank;
	hsize_t tileDims[3];	
	int bufSize = ReadDataSetBF(d->header->tileName, _tileName, _tileData, tileRank, tileDims,ch);

	return bufSize;	
}

auto TCFLdmReader::GetTCFHeader() -> LdmHeader* {
	return d->header;
}


auto TCFLdmReader::SetFileName(const std::string& name,int numZero)->int {
	if (OpenHdfFile(name,numZero) < 0) {
		return -1;
	}
	return 0;
}

auto TCFLdmReader::CloseHdf5File() -> int {
	d->file.close();
	return 0;
}

auto TCFLdmReader::OpenHdfFile(const std::string& name, int numZero) -> int {
	d->file = H5::H5File(name.c_str(), H5F_ACC_RDONLY);

	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(numZero) << std::setfill('0') << 0;
	auto ress = tmpStr.str();
	H5::Group metaGrp;
	H5::Group grp;
	if (d->isFL) {
		std::string delimiter = "/";
		auto s = d->header->dataGroup;
		size_t pos = 0;
		std::string token;
		std::vector<std::string> tokens;
		while ((pos = s.find(delimiter)) != std::string::npos) {
			token = s.substr(0, pos);
			tokens.push_back(token);
			s.erase(0, pos + delimiter.length());
		}
		std::string final_grp = "/" + tokens[1] + "/" + tokens[2];
		metaGrp = d->file.openGroup(final_grp.c_str());

		grp = metaGrp.openGroup(s.c_str());
	}
	else {
		metaGrp = grp = d->file.openGroup(d->header->dataGroup.c_str());
	}
	auto dset = grp.openGroup(d->header->tileName.c_str());

	auto tile = dset.openDataSet(ress.c_str());

	if (d->is8Bit) {
		d->header->dataType = SoDataSet::UNSIGNED_BYTE;
	}else {
		d->header->dataType = SoDataSet::UNSIGNED_SHORT;
	}
	auto maxTile = 0;
	if (tile.attrExists("TileDataSizeX")) {
		auto attr = tile.openAttribute("TileDataSizeX");
		int64_t tileSize;
		attr.read(H5::PredType::NATIVE_INT64, &tileSize);
		d->header->maxTileSize = tileSize;
		attr.close();
	}

	tile.close();
	dset.close();

	if (metaGrp.attrExists("DataCount")) {
		auto attr = metaGrp.openAttribute("DataCount");
		int64_t datacount;
		attr.read(H5::PredType::NATIVE_INT64, &datacount);
		d->header->dataCount = datacount;
		attr.close();
	}
	if (metaGrp.attrExists("SizeX")) {
		auto attr = metaGrp.openAttribute("SizeX");
		int64_t SizeX;
		attr.read(H5::PredType::NATIVE_INT64, &SizeX);
		d->header->sizeX = SizeX;
		attr.close();
	}
	if (metaGrp.attrExists("SizeY")) {
		auto attr = metaGrp.openAttribute("SizeY");
		int64_t SizeY;
		attr.read(H5::PredType::NATIVE_INT64, &SizeY);
		d->header->sizeY = SizeY;
		attr.close();
	}
	if (metaGrp.attrExists("SizeZ")) {
		auto attr = metaGrp.openAttribute("SizeZ");
		int64_t SizeZ;
		attr.read(H5::PredType::NATIVE_INT64, &SizeZ);
		d->header->sizeZ = SizeZ;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionX")) {
		auto attr = metaGrp.openAttribute("ResolutionX");
		double resX;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resX);
		d->header->resolutionX = resX;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionY")) {
		auto attr = metaGrp.openAttribute("ResolutionY");
		double resY;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resY);
		d->header->resolutionY = resY;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionZ")) {
		auto attr = metaGrp.openAttribute("ResolutionZ");
		double resZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resZ);
		d->header->resolutionZ = resZ;
		attr.close();
	}
	if (metaGrp.attrExists("OffsetZ")) {
		auto attr = metaGrp.openAttribute("OffsetZ");
		double ofZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &ofZ);
		d->header->offsetZ = ofZ;
		attr.close();
	}

	grp.close();
	metaGrp.close();

	return 0;
}

auto TCFLdmReader::Init() -> void {

	d->header = new LdmHeader;
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_SHORT;

	H5open();
}
