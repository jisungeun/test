#pragma once

#include "TC.IO.OIV.Volume.LDMExport.h"

#include <memory>

#include <QMutexLocker>

#pragma warning(push)
#pragma warning(disable:4268)
#include <Inventor/SbBox.h>
#include <hdf5.h>
#include <H5Cpp.h>
#pragma warning(pop)

#include "ITCFLdmReader.h"

class TC_IO_OIV_Volume_LDM_API TCFLdmReader : public ITCFLdmReader {
public:
	TCFLdmReader(bool isFL = false,bool isBF = false);
	~TCFLdmReader();

	auto SetIs8Bit(bool is8Bit)->void;
	auto GetTCFHeader() -> LdmHeader* override;
	auto SetFileName(const std::string& name, int numZero) -> int override;	
	auto ReadHTLdmData3D(const std::string _tileName, void** _tileData) -> int override;
	auto ReadBFLdmData(const std::string _tileName, void** tileData, int ch) -> int override;

protected:	
	auto ReadDataSet(std::string _tile_name, void** data)->int;
	auto ReadDataSetBF(const std::string _name, std::string _tile_name, void** _data, int& _rank, hsize_t* _dims,int ch)->int;

private:
	auto Init(void)->void;
	auto OpenHdfFile(const std::string& name,int numZero)->int;
	auto CloseHdf5File()->int;
	struct Impl;
	std::unique_ptr<Impl> d;
};