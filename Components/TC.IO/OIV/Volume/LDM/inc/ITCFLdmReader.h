#pragma once

#include <QMutexLocker>

#pragma warning(push)
#pragma warning(disable:4268)
#include <hdf5.h>
#include <H5Cpp.h>
#pragma warning(pop)


#include <memory>

enum HDFChunkType
{
	NONE = 0,
	ROW,
	COLUME,
	TILE
};

typedef struct HeaderOfLDM {
	int dataCount = -1;
	double		resolutionZ = -1.0;
	double		resolutionX = -1.0;
	double		resolutionY = -1.0;
	double      offsetZ = -1.0;
	int			sizeZ = -1;
	int			sizeX = -1;
	int			sizeY = -1;
	float		timeInterval = -1.f;
	float		maxIntensity = -1.f;
	float		minIntensity = -1.f;
	float		riMin = -1.f;
	float		riMax = -1.f;
	float		positionC = -1.f;
	float		positionX = -1.f;
	float		positionY = -1.f;
	float		positionZ = -1.f;
	int			dataType = -1;
	int			maxTileSize = -1;
	std::string dataGroup;
	std::string tileName;
	std::string filePath;
}LdmHeader;

class ITCFLdmReader {
public:
	ITCFLdmReader();
	~ITCFLdmReader();

	virtual auto GetTCFHeader(void)->LdmHeader* = 0;
	virtual auto SetFileName(const std::string& name, int numZero)->int = 0;
	virtual auto ReadHTLdmData3D(const std::string _tileName, void** _tileData)->int = 0 ;
	virtual auto ReadBFLdmData(const std::string _tileName, void** tileData, int ch)->int = 0;
};