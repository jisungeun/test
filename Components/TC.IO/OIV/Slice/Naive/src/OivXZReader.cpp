#pragma warning(push)
#pragma warning(disable : 4819)
#include <Inventor/SoInput.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "OivXZReader.h"

SO_FIELDCONTAINER_SOURCE(OivXZReader);

struct OivXZReader::Impl {
	std::shared_ptr<SliceReaderXZ> _hdfReader{ nullptr };
	int _tileSize{256};
	int _Index{0};	
	bool isFL{ false };
	int sigBit{ 16 };
	float zOffset{ 0 };
};

OivXZReader::OivXZReader(bool isFL, bool is8Bit)
	:d(new Impl())
{
	d->isFL = isFL;
	d->_hdfReader = std::make_shared<SliceReaderXZ>(isFL);
	d->_hdfReader->SetIs8Bit(is8Bit);
	if(is8Bit) {		
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}
	else {
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
	m_dataConverted = FALSE;

	d->_tileSize = 0;

	SO_FIELDCONTAINER_CONSTRUCTOR(OivXZReader);
}

OivXZReader::~OivXZReader()
{
}

auto OivXZReader::setOffsetZ(float offset) -> void {
	d->zOffset = offset;
}

auto OivXZReader::set8Bit(bool is8Bit) -> void {
	d->_hdfReader->SetIs8Bit(is8Bit);
	if (is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}
	else {		
		d->sigBit = 16;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
}


auto OivXZReader::setIndex(int idx) -> void {	
	d->_Index = idx;
}

auto OivXZReader::getIndex() -> int {
	return d->_Index;
}

auto OivXZReader::setTileDimension(int tileSize) -> void
{
	d->_tileSize = tileSize;
}

auto OivXZReader::setTileName(const std::string& tileName) -> void
{
	auto header = d->_hdfReader->GetSliceHeader();
	header->tileName = tileName;
}

auto OivXZReader::setDataGroupPath(const std::string& dataGroup) -> void
{
	auto header = d->_hdfReader->GetSliceHeader();
	header->dataGroup = dataGroup;
}

auto OivXZReader::setXYCropOffset(const int& offsetX, const int& offsetY) -> void {
	auto header = d->_hdfReader->GetSliceHeader();
	header->cropOffsetX = offsetX;
	header->cropOffsetY = offsetY;
}

auto OivXZReader::setXYCropSize(const int& sizeX, const int& sizeY) -> void {
	auto header = d->_hdfReader->GetSliceHeader();
	header->sizeX = sizeX;
	header->sizeY = sizeY;
}

auto OivXZReader::setFilename(const SbString& filename) -> int
{
	if (d->_hdfReader != NULL)
	{
		int errStatus = d->_hdfReader->SetFileName(filename.toStdString());
		if (errStatus < 0)
			return -1;
		else
		{
			auto header = d->_hdfReader->GetSliceHeader();			

			m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

			return 1;
		}
	}
	return 0;
}
auto OivXZReader::getOffsetZ() -> double {
	auto header = d->_hdfReader->GetSliceHeader();
	double offset = header->offsetZ;
	int size_z = header->sizeZ;
	double resolutionZ = header->resolutionZ;

	return (offset - ((double)size_z * resolutionZ) / 2.0);
}

auto OivXZReader::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm) -> SoVolumeReader::ReadError
{
	auto header = d->_hdfReader->GetSliceHeader();

	auto xmin = header->cropOffsetX * header->resolutionX + (-header->oriX * header->resolutionX) / 2;
	double zmin;
	if(header->sizeZ==1) {
		zmin = -header->resolutionX + d->zOffset;
	}else {
		zmin = -header->sizeZ * header->resolutionZ / 2 + d->zOffset;
	}

	auto ymin = -0.5;
	auto xmax = header->cropOffsetX * header->resolutionX + (-header->oriX * header->resolutionX) / 2 + header->sizeX * header->resolutionX;
	double zmax;
	if(header->sizeZ == 1) {
		zmax = header->resolutionX + d->zOffset;;
	}else {
		zmax = header->sizeZ * header->resolutionZ / 2 + d->zOffset;;
	}
	
	auto ymax = 0.5;

	SbVec3f _min = SbVec3f(static_cast<float>(xmin), static_cast<float>(zmin), static_cast<float>(ymin));
	SbVec3f _max = SbVec3f(static_cast<float>(xmax), static_cast<float>(zmax), static_cast<float>(ymax));
		
	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	idm = SbVec3i32(header->sizeX, header->sizeZ, 1);

	return RD_NO_ERROR;
}

auto OivXZReader::getBorderFlag() -> int
{
	return 0;
}

auto OivXZReader::getNumSignificantBits() -> int
{
	return d->sigBit;
}

auto OivXZReader::getTileSize(SbVec3i32& size) -> SbBool
{
	size = SbVec3i32(d->_tileSize, d->_tileSize, 1);
	return TRUE;
}

auto OivXZReader::getMinMax(int64_t&, int64_t&) -> SbBool
{
	return FALSE;
}

auto OivXZReader::getMinMax(double&, double&) -> SbBool
{
	return FALSE;
}

auto OivXZReader::readTile(int, const SbBox3i32&) -> SoBufferObject*
{
	std::cerr << "OivCustomColor::readTile : Not Implemented" << std::endl;
	return NULL;
}

auto OivXZReader::getSubSlice(const SbBox2i32&, int, void*) -> void
{
	std::cerr << "OivCustomColor::getSubSlice : Not Implemented" << std::endl;
	return;
}

auto OivXZReader::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void
{	
	SoCpuBufferObject* cpuObj = static_cast<SoCpuBufferObject*>(bufferObject);
	SbVec3i32 offset = SbVec3i32(subSlice.getMin()[0], subSlice.getMin()[1], sliceNumber);
	SbVec3i32 size = SbVec3i32(subSlice.getMax()[0] - subSlice.getMin()[0] + 1, subSlice.getMax()[1] - subSlice.getMin()[1] + 1, 1);

	void* data = cpuObj->map(SoCpuBufferObject::SET);

	try {		
		d->_hdfReader->ReadSliceData(offset, size, &data, d->_Index);
	}
	catch (...) {

	}

	cpuObj->unmap();
}

auto OivXZReader::getHistogram(std::vector<int64_t>&) -> SbBool
{
	return FALSE;
}

auto OivXZReader::isDataConverted() const -> SbBool
{
	return FALSE;
}

auto OivXZReader::isRGBA() const -> SbBool
{
	//return TRUE;
	return FALSE;
}

auto OivXZReader::isThreadSafe() const -> SbBool
{
	//return TRUE;	
	return FALSE;
}

auto OivXZReader::initClass() -> void
{
	SO_FIELDCONTAINER_INIT_CLASS(OivXZReader, "OivCustomSliceReaderXZ", SoVolumeReader);
}

auto OivXZReader::exitClass() -> void
{
	SO__FIELDCONTAINER_EXIT_CLASS(OivXZReader);
}

auto OivXZReader::getTimeSteps()->int {
	if (nullptr == d->_hdfReader) {
		return -1;
	}

	auto header = d->_hdfReader->GetSliceHeader();
	if (nullptr == header) {
		return -1;
	}

	return header->dataCount;
}