#pragma warning(push)
#pragma warning(disable : 4819)
#include <Inventor/SoInput.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "OivXYReader.h"

SO_FIELDCONTAINER_SOURCE(OivXYReader);

struct OivXYReader::Impl {
	std::shared_ptr<SliceReader> _hdfReader{ nullptr };
	int _tileSize{256};
	int _Index{0};	
	bool isFL{ false };
	int numBit{ 16 };
	std::string path;
};

OivXYReader::OivXYReader(bool isFL,bool is8Bit)
	:d(new Impl())
{
	d->isFL = isFL;
	d->_hdfReader = std::make_shared<SliceReader>(isFL);
	d->_hdfReader->SetIs8Bit(is8Bit);
	if(is8Bit) {		
		d->numBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}	
	m_dataConverted = FALSE;

	d->_tileSize = 0;

	SO_FIELDCONTAINER_CONSTRUCTOR(OivXYReader);
}

OivXYReader::~OivXYReader()
{
}

auto OivXYReader::setIndex(int idx) -> void {	
	d->_Index = idx;		
}

auto OivXYReader::getIndex() -> int {
	return d->_Index;
}

auto OivXYReader::setTileDimension(int tileSize) -> void
{
	d->_tileSize = tileSize;
}

auto OivXYReader::setTileName(const std::string& tileName) -> void
{
	auto header = d->_hdfReader->GetSliceHeader();
	header->tileName = tileName;
}

auto OivXYReader::setXYCropOffset(const int& offsetX, const int& offsetY) -> void {
	auto header = d->_hdfReader->GetSliceHeader();
	header->cropOffsetX = offsetX;
	header->cropOffsetY = offsetY;		
}

auto OivXYReader::setXYCropSize(const int& sizeX, const int& sizeY) -> void {
	auto header = d->_hdfReader->GetSliceHeader();
	header->sizeX = sizeX;
	header->sizeY = sizeY;
}


auto OivXYReader::setDataGroupPath(const std::string& dataGroup) -> void
{
	auto header = d->_hdfReader->GetSliceHeader();
	header->dataGroup = dataGroup;
}
auto OivXYReader::set8Bit(bool is8Bit) -> void {
	d->_hdfReader->SetIs8Bit(is8Bit);
	if (is8Bit) {
		d->numBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		d->numBit = 16;		
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}		
}
auto OivXYReader::setFilename(const SbString& filename) -> int
{
	if (d->_hdfReader != NULL)
	{
		d->path = filename.toStdString();
		int errStatus = d->_hdfReader->SetFileName(d->path);
		if (errStatus < 0)
			return -1;
		else
		{
			auto header = d->_hdfReader->GetSliceHeader();			

			m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

			return 1;
		}
	}
	return 0;
}
auto OivXYReader::getOffsetZ() -> double {
	auto header = d->_hdfReader->GetSliceHeader();
	double offset = header->offsetZ;
	int size_z = header->sizeZ;
	double resolutionZ = header->resolutionZ;

	return (offset - ((double)size_z * resolutionZ) / 2.0);
}

auto OivXYReader::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm) -> SoVolumeReader::ReadError
{	
	auto header = d->_hdfReader->GetSliceHeader();
	
	auto xmin = header->cropOffsetX * header->resolutionX + (-header->oriX * header->resolutionX) / 2;
	auto ymin = header->cropOffsetY * header->resolutionY + (-header->oriY * header->resolutionY) / 2;
	auto zmin = -0.5;
	auto xmax = header->cropOffsetX * header->resolutionX + (-header->oriX * header->resolutionX)/2 + header->sizeX * header->resolutionX;
	auto ymax = header->cropOffsetY * header->resolutionY + (-header->oriY * header->resolutionY)/2 + header->sizeY * header->resolutionY;
	auto zmax = 0.5;
	SbVec3f _min = SbVec3f(static_cast<float>(xmin), static_cast<float>(ymin), static_cast<float>(zmin));
	SbVec3f _max = SbVec3f(static_cast<float>(xmax), static_cast<float>(ymax), static_cast<float>(zmax));
		
	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	idm = SbVec3i32(header->sizeX, header->sizeY, 1);	
	
	return RD_NO_ERROR;
}

auto OivXYReader::getBorderFlag() -> int
{
	return 0;
}

auto OivXYReader::getNumSignificantBits() -> int
{	
	return d->numBit;
}

auto OivXYReader::getTileSize(SbVec3i32& size) -> SbBool
{
	size = SbVec3i32(d->_tileSize, d->_tileSize, 1);
	return TRUE;
}

auto OivXYReader::getMinMax(int64_t&, int64_t&) -> SbBool
{
	return FALSE;
}

auto OivXYReader::getMinMax(double&, double&) -> SbBool
{
	return FALSE;
}

auto OivXYReader::readTile(int, const SbBox3i32&) -> SoBufferObject*
{
	std::cerr << "OivCustomColor::readTile : Not Implemented" << std::endl;
	return NULL;
}

auto OivXYReader::getSubSlice(const SbBox2i32&, int, void*) -> void
{
	std::cerr << "OivCustomColor::getSubSlice : Not Implemented" << std::endl;
	return;
}

auto OivXYReader::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void
{	
	SoCpuBufferObject* cpuObj = static_cast<SoCpuBufferObject*>(bufferObject);
	SbVec3i32 offset = SbVec3i32(subSlice.getMin()[0], subSlice.getMin()[1], sliceNumber);
	SbVec3i32 size = SbVec3i32(subSlice.getMax()[0] - subSlice.getMin()[0] + 1, subSlice.getMax()[1] - subSlice.getMin()[1] + 1, 1);

	void* data = cpuObj->map(SoCpuBufferObject::SET);

	try {		
		d->_hdfReader->ReadSliceData(offset, size, &data, d->_Index);
	}
	catch (...) {

	}

	cpuObj->unmap();
}

auto OivXYReader::getHistogram(std::vector<int64_t>&) -> SbBool
{
	return FALSE;
}

auto OivXYReader::isDataConverted() const -> SbBool
{
	return FALSE;
}

auto OivXYReader::isRGBA() const -> SbBool
{
	//return TRUE;
	return FALSE;
}

auto OivXYReader::isThreadSafe() const -> SbBool
{
	//return TRUE;	
	return FALSE;
}

auto OivXYReader::initClass() -> void
{
	SO_FIELDCONTAINER_INIT_CLASS(OivXYReader, "OivCustomSliceReaderXY", SoVolumeReader);
}

auto OivXYReader::exitClass() -> void
{
	SO__FIELDCONTAINER_EXIT_CLASS(OivXYReader);
}

auto OivXYReader::getTimeSteps()->int {
	if (nullptr == d->_hdfReader) {
		return -1;
	}

	auto header = d->_hdfReader->GetSliceHeader();
	if (nullptr == header) {
		return -1;
	}

	return header->dataCount;
}