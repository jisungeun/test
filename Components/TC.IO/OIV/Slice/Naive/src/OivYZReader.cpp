#pragma warning(push)
#pragma warning(disable : 4819)
#include <Inventor/SoInput.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "OivYZReader.h"

SO_FIELDCONTAINER_SOURCE(OivYZReader);

struct OivYZReader::Impl {
	std::shared_ptr<SliceReaderYZ> _hdfReader{ nullptr };
	int _tileSize{256};
	int _Index{0};	
	bool isFL{ false };
	int sigBit{ 16 };
	float zOffset{ 0 };
};

OivYZReader::OivYZReader(bool isFL,bool is8Bit)
	:d(new Impl())
{
	d->isFL = isFL;
	d->_hdfReader = std::make_shared<SliceReaderYZ>(isFL);
	d->_hdfReader->SetIs8Bit(is8Bit);
	if(is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
	m_dataConverted = FALSE;

	d->_tileSize = 0;

	SO_FIELDCONTAINER_CONSTRUCTOR(OivYZReader);
}

OivYZReader::~OivYZReader()
{
}

auto OivYZReader::set8Bit(bool is8Bit) -> void {
	d->_hdfReader->SetIs8Bit(is8Bit);
	if (is8Bit) {		
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}
	else {		
		d->sigBit = 16;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
}

auto OivYZReader::setIndex(int idx) -> void {	
	d->_Index = idx;
}

auto OivYZReader::getIndex() -> int {
	return d->_Index;
}

auto OivYZReader::setTileDimension(int tileSize) -> void
{
	d->_tileSize = tileSize;
}

auto OivYZReader::setTileName(const std::string& tileName) -> void
{
	auto header = d->_hdfReader->GetSliceHeader();
	header->tileName = tileName;
}

auto OivYZReader::setDataGroupPath(const std::string& dataGroup) -> void
{
	auto header = d->_hdfReader->GetSliceHeader();
	header->dataGroup = dataGroup;
}

auto OivYZReader::setXYCropOffset(const int& offsetX, const int& offsetY) -> void {
	auto header = d->_hdfReader->GetSliceHeader();
	header->cropOffsetX = offsetX;
	header->cropOffsetY = offsetY;
}

auto OivYZReader::setXYCropSize(const int& sizeX, const int& sizeY) -> void {
	auto header = d->_hdfReader->GetSliceHeader();
	header->sizeX = sizeX;
	header->sizeY = sizeY;
}

auto OivYZReader::setFilename(const SbString& filename) -> int
{
	if (d->_hdfReader != NULL)
	{
		int errStatus = d->_hdfReader->SetFileName(filename.toStdString());
		if (errStatus < 0)
			return -1;
		else
		{
			auto header = d->_hdfReader->GetSliceHeader();			

			m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

			return 1;
		}
	}
	return 0;
}
auto OivYZReader::getOffsetZ() -> double {
	auto header = d->_hdfReader->GetSliceHeader();
	double offset = header->offsetZ;
	int size_z = header->sizeZ;
	double resolutionZ = header->resolutionZ;		

	return (offset - ((double)size_z * resolutionZ) / 2.0);
}

auto OivYZReader::setOffsetZ(float offset) -> void {
	d->zOffset = offset;
}

auto OivYZReader::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm) -> SoVolumeReader::ReadError
{
	auto header = d->_hdfReader->GetSliceHeader();	
	auto floffset = d->zOffset;

	auto ymin = header->cropOffsetY * header->resolutionY + (-header->oriY * header->resolutionY) / 2;
	double zmin;
	if(header->sizeZ == 1) {
		zmin = -header->resolutionX + floffset;
	}else {
		zmin = -header->sizeZ * header->resolutionZ / 2 + floffset;
	}	
	auto xmin = -0.5;
	auto ymax = header->cropOffsetY * header->resolutionY + (-header->oriY * header->resolutionY) / 2 + header->sizeY * header->resolutionY;
	double zmax;
	if(header->sizeZ == 1) {
		zmax = header->resolutionX + floffset;
	}else {
		zmax = header->sizeZ * header->resolutionZ / 2 + floffset;
	}
	auto xmax = 0.5;

	SbVec3f _min = SbVec3f(static_cast<float>(ymin), static_cast<float>(zmin), static_cast<float>(xmin));
	SbVec3f _max = SbVec3f(static_cast<float>(ymax), static_cast<float>(zmax), static_cast<float>(xmax));
	
	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	idm = SbVec3i32(header->sizeY, header->sizeZ, 1);

	return RD_NO_ERROR;
}

auto OivYZReader::getBorderFlag() -> int
{
	return 0;
}

auto OivYZReader::getNumSignificantBits() -> int
{
	return d->sigBit;
}

auto OivYZReader::getTileSize(SbVec3i32& size) -> SbBool
{
	size = SbVec3i32(d->_tileSize, d->_tileSize, 1);
	return TRUE;
}

auto OivYZReader::getMinMax(int64_t&, int64_t&) -> SbBool
{
	return FALSE;
}

auto OivYZReader::getMinMax(double&, double&) -> SbBool
{
	return FALSE;
}

auto OivYZReader::readTile(int, const SbBox3i32&) -> SoBufferObject*
{
	std::cerr << "OivCustomColor::readTile : Not Implemented" << std::endl;
	return NULL;
}

auto OivYZReader::getSubSlice(const SbBox2i32&, int, void*) -> void
{
	std::cerr << "OivCustomColor::getSubSlice : Not Implemented" << std::endl;
	return;
}

auto OivYZReader::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void
{	
	SoCpuBufferObject* cpuObj = static_cast<SoCpuBufferObject*>(bufferObject);
	SbVec3i32 offset = SbVec3i32(subSlice.getMin()[0], subSlice.getMin()[1], sliceNumber);
	SbVec3i32 size = SbVec3i32(subSlice.getMax()[0] - subSlice.getMin()[0] + 1, subSlice.getMax()[1] - subSlice.getMin()[1] + 1, 1);

	void* data = cpuObj->map(SoCpuBufferObject::SET);

	try {		
		d->_hdfReader->ReadSliceData(offset, size, &data, d->_Index);
	}
	catch (...) {

	}

	cpuObj->unmap();
}

auto OivYZReader::getHistogram(std::vector<int64_t>&) -> SbBool
{
	return FALSE;
}

auto OivYZReader::isDataConverted() const -> SbBool
{
	return FALSE;
}

auto OivYZReader::isRGBA() const -> SbBool
{
	//return TRUE;
	return FALSE;
}

auto OivYZReader::isThreadSafe() const -> SbBool
{
	//return TRUE;	
	return FALSE;
}

auto OivYZReader::initClass() -> void
{
	SO_FIELDCONTAINER_INIT_CLASS(OivYZReader, "OivCustomSliceReaderYZ", SoVolumeReader);
}

auto OivYZReader::exitClass() -> void
{
	SO__FIELDCONTAINER_EXIT_CLASS(OivYZReader);
}

auto OivYZReader::getTimeSteps()->int {
	if (nullptr == d->_hdfReader) {
		return -1;
	}

	auto header = d->_hdfReader->GetSliceHeader();
	if (nullptr == header) {
		return -1;
	}

	return header->dataCount;
}