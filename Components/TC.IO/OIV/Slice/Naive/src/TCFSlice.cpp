#include <QStringList>
#include <QMutexLocker>
#include <HDF5Mutex.h>

#include "TCFSlice.h"

struct SliceReader::Impl {
	H5::H5File file;
	H5::Group group;
	TCFSliceHeader* header{ nullptr };
	bool isFL{ false };
	bool is8Bit{ false };
};

SliceReader::SliceReader(bool isFL) :d{ new Impl } {
	d->header = new TCFSliceHeader;
	d->isFL = isFL;
	this->Init();
}

SliceReader::SliceReader(const std::string& name, const std::string& dataGroup, const std::string& tileName)
	: d{ new Impl } {
	this->Init();
	d->header->dataGroup = dataGroup;
	d->header->tileName = tileName;
	this->SetFileName(name);
}

SliceReader::~SliceReader() {
	delete d->header;

	CloseHdfFile();
}

auto SliceReader::SetIs8Bit(bool is8Bit) -> void {
	d->is8Bit = is8Bit;
	if (d->header) {
		if (is8Bit) {
			d->header->dataType = SoDataSet::UNSIGNED_BYTE;
		}
		else {
			d->header->dataType = SoDataSet::UNSIGNED_SHORT;
		}
	}
}

auto SliceReader::GetSliceHeader() -> TCFSliceHeader* {
	return d->header;
}

auto SliceReader::SetFileName(const std::string& name) -> int {
	if (OpenHdfFile(name) < 0)
		return -1;
	return 0;
}

auto SliceReader::ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	H5::Group group = d->group.openGroup(d->header->dataGroup.c_str());
	H5::DataSet dataset = group.openDataSet(d->header->tileName.c_str());

	H5::DataSpace dataSpace = dataset.getSpace();

	hsize_t dims[3];
	int dim = dataSpace.getSimpleExtentDims(dims);

	if (dim == 2) {
		dataSpace.close();
		dataset.close();
		group.close();
		return 1;
	}

	// The start, stride, count, and block arrays must be the same size as the rank of the dataspace.
	// Stride parameter is NULL, a contiguous hyperslab is selected	
    hsize_t tcfOffset[3] = { (hsize_t)(offset[2]+slice), (hsize_t)(offset[1] + d->header->cropOffsetY), (hsize_t)(offset[0] + d->header->cropOffsetX) };	
	// How many blocks to select from the dataspace, in each dimension.
	hsize_t tcfCount[3] = { (hsize_t)(dataSize[2]), (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]) };	

	dataSpace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };
	memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);

	if (d->is8Bit) {
		auto cast_data = static_cast<unsigned char*>(*data);
		dataset.read(cast_data, H5::PredType::NATIVE_UCHAR, memoryspace, dataSpace);
	}else {
		auto cast_data = static_cast<unsigned short*>(*data);
		dataset.read(cast_data, H5::PredType::NATIVE_UINT16, memoryspace, dataSpace);
	}	

	memoryspace.close();

	dataSpace.close();
	dataset.close();
	group.close();

	return 1;
}

auto SliceReader::Init() -> int {
	d->header->oriX = 1;
	d->header->oriY = 1;
	d->header->oriZ = 1;
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_SHORT;

	return H5open();
}

auto SliceReader::OpenHdfFile(const std::string& name) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
	d->file = H5::H5File(name.c_str(), H5F_ACC_RDONLY);	
	// Find the 2D group

	// Read the attributes	

	if(d->is8Bit) {
		d->header->dataType = SoDataSet::UNSIGNED_BYTE;
	}
	else {
		d->header->dataType = SoDataSet::UNSIGNED_SHORT;
	}
	
	auto group = d->file.openGroup("Data");
	H5::Group targetGroup;
	if (d->isFL) {
		targetGroup = group.openGroup("3DFL");
	}
	else {
		targetGroup = group.openGroup("3D");
	}

	if (targetGroup.attrExists("DataCount")) {
		auto attr = targetGroup.openAttribute("DataCount");
		int64_t datacount;
		attr.read(H5::PredType::NATIVE_INT64, &datacount);
		d->header->dataCount = datacount;
		attr.close();
	}
	if (targetGroup.attrExists("SizeX")) {
		auto attr = targetGroup.openAttribute("SizeX");
		int64_t SizeX;
		attr.read(H5::PredType::NATIVE_INT64, &SizeX);
		d->header->oriX = d->header->sizeX = SizeX;
		attr.close();
	}
	if (targetGroup.attrExists("SizeY")) {
		auto attr = targetGroup.openAttribute("SizeY");
		int64_t SizeY;
		attr.read(H5::PredType::NATIVE_INT64, &SizeY);
		d->header->oriY = d->header->sizeY = SizeY;
		attr.close();
	}
	if (targetGroup.attrExists("SizeZ")) {		
		auto attr = targetGroup.openAttribute("SizeZ");
		int64_t SizeZ;
		attr.read(H5::PredType::NATIVE_INT64, &SizeZ);
		d->header->oriZ = d->header->sizeZ = SizeZ;
		attr.close();		
	}
	if (targetGroup.attrExists("ResolutionX")) {
		auto attr = targetGroup.openAttribute("ResolutionX");
		double resX;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resX);
		d->header->resolutionX = resX;
		attr.close();
	}
	if (targetGroup.attrExists("ResolutionY")) {
		auto attr = targetGroup.openAttribute("ResolutionY");
		double resY;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resY);
		d->header->resolutionY = resY;
		attr.close();
	}
	if (targetGroup.attrExists("ResolutionZ")) {
		auto attr = targetGroup.openAttribute("ResolutionZ");
		double resZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resZ);
		d->header->resolutionZ = resZ;
		attr.close();
	}
	if (targetGroup.attrExists("OffsetZ")) {
		auto attr = targetGroup.openAttribute("OffsetZ");
		double ofZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &ofZ);
		d->header->offsetZ = ofZ;
		attr.close();
	}
	targetGroup.close();
	group.close();

	d->group = d->file.openGroup(d->header->dataGroup.c_str());

	return 0;
}

auto SliceReader::CloseHdfFile() -> int {
	herr_t errStatus = -1;

	d->group.close();
	d->file.close();

	return errStatus;
}

struct SliceReaderYZ::Impl {
	H5::H5File file;
	H5::Group group;
	TCFSliceHeader* header{ nullptr };
	bool isFL{ false };
	bool is8Bit{ false };
};

SliceReaderYZ::SliceReaderYZ(bool isFL) :d{ new Impl } {
	d->header = new TCFSliceHeader;
	d->isFL = isFL;
	this->Init();
}

auto SliceReaderYZ::SetIs8Bit(bool is8Bit) -> void {
	d->is8Bit = is8Bit;
	if(d->header) {
		if (is8Bit) {
			d->header->dataType = SoDataSet::UNSIGNED_BYTE;
		}
		else {
			d->header->dataType = SoDataSet::UNSIGNED_SHORT;
		}
	}
}

SliceReaderYZ::SliceReaderYZ(const std::string& name, const std::string& dataGroup, const std::string& tileName)
	: d{ new Impl } {
	this->Init();
	d->header->dataGroup = dataGroup;
	d->header->tileName = tileName;
	this->SetFileName(name);
}

SliceReaderYZ::~SliceReaderYZ() {
	delete d->header;

	CloseHdfFile();
}

auto SliceReaderYZ::GetSliceHeader() -> TCFSliceHeader* {
	return d->header;
}

auto SliceReaderYZ::SetFileName(const std::string& name) -> int {
	if (OpenHdfFile(name) < 0)
		return -1;
	return 0;
}

auto SliceReaderYZ::ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	H5::Group group = d->group.openGroup(d->header->dataGroup.c_str());
	H5::DataSet dataset = group.openDataSet(d->header->tileName.c_str());
	
	H5::DataSpace dataSpace = dataset.getSpace();

	hsize_t dims[3];
	int dim = dataSpace.getSimpleExtentDims(dims);

	if (dim == 2) {
		dataSpace.close();
		dataset.close();
		group.close();
		return 1;
	}

	// The start, stride, count, and block arrays must be the same size as the rank of the dataspace.
	// Stride parameter is NULL, a contiguous hyperslab is selected

	hsize_t tcfOffset[3] = { (hsize_t)(offset[1]), (hsize_t)(offset[0] + d->header->cropOffsetY), (hsize_t)(offset[2] + slice + d->header->cropOffsetX) };
	// How many blocks to select from the dataspace, in each dimension.
	hsize_t tcfCount[3] = { (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]), (hsize_t)(dataSize[2]) };

	dataSpace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };
	memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);

	if(d->is8Bit) {
		auto cast_data = static_cast<unsigned char*>(*data);
		dataset.read(cast_data, H5::PredType::NATIVE_UCHAR, memoryspace, dataSpace);
	}
	else {
		auto cast_data = static_cast<unsigned short*>(*data);
		dataset.read(cast_data, H5::PredType::NATIVE_UINT16, memoryspace, dataSpace);
	}

	memoryspace.close();

	dataSpace.close();
	dataset.close();
	group.close();

	return 1;
}

auto SliceReaderYZ::Init() -> int {
	d->header->oriX = 1;
	d->header->oriY = 1;
	d->header->oriZ = 1;
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_SHORT;

	return H5open();
}

auto SliceReaderYZ::OpenHdfFile(const std::string& name) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
	d->file = H5::H5File(name.c_str(), H5F_ACC_RDONLY);

	// Find the 2D group

	// Read the attributes	

	if (d->is8Bit) {
		d->header->dataType = SoDataSet::UNSIGNED_BYTE;
	}
	else {
		d->header->dataType = SoDataSet::UNSIGNED_SHORT;
	}


	auto group = d->file.openGroup("Data");
	H5::Group targetGroup;
	if (d->isFL) {
		targetGroup = group.openGroup("3DFL");
	}
	else {
		targetGroup = group.openGroup("3D");
	}

	if (targetGroup.attrExists("DataCount")) {
		auto attr = targetGroup.openAttribute("DataCount");
		int64_t datacount;
		attr.read(H5::PredType::NATIVE_INT64, &datacount);
		d->header->dataCount = datacount;
		attr.close();
	}
	if (targetGroup.attrExists("SizeX")) {
		auto attr = targetGroup.openAttribute("SizeX");
		int64_t SizeX;
		attr.read(H5::PredType::NATIVE_INT64, &SizeX);
		d->header->oriX = d->header->sizeX = SizeX;
		attr.close();
	}
	if (targetGroup.attrExists("SizeY")) {
		auto attr = targetGroup.openAttribute("SizeY");
		int64_t SizeY;
		attr.read(H5::PredType::NATIVE_INT64, &SizeY);
		d->header->oriY = d->header->sizeY = SizeY;
		attr.close();
	}
	if (targetGroup.attrExists("SizeZ")) {
		auto attr = targetGroup.openAttribute("SizeZ");
		int64_t SizeZ;
		attr.read(H5::PredType::NATIVE_INT64, &SizeZ);
		d->header->oriZ = d->header->sizeZ = SizeZ;
		attr.close();		
	}
	if (targetGroup.attrExists("ResolutionX")) {
		auto attr = targetGroup.openAttribute("ResolutionX");
		double resX;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resX);
		d->header->resolutionX = resX;
		attr.close();
	}
	if (targetGroup.attrExists("ResolutionY")) {
		auto attr = targetGroup.openAttribute("ResolutionY");
		double resY;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resY);
		d->header->resolutionY = resY;
		attr.close();
	}
	if (targetGroup.attrExists("ResolutionZ")) {
		auto attr = targetGroup.openAttribute("ResolutionZ");
		double resZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resZ);
		d->header->resolutionZ = resZ;
		attr.close();
	}
	if (targetGroup.attrExists("OffsetZ")) {
		auto attr = targetGroup.openAttribute("OffsetZ");
		double ofZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &ofZ);
		d->header->offsetZ = ofZ;
		attr.close();
	}
	targetGroup.close();
	group.close();		

	d->group = d->file.openGroup(d->header->dataGroup.c_str());

	return 0;
}

auto SliceReaderYZ::CloseHdfFile() -> int {
	herr_t errStatus = -1;

	d->group.close();
	d->file.close();

	return errStatus;
}

struct SliceReaderXZ::Impl {
	H5::H5File file;
	H5::Group group;
	TCFSliceHeader* header{ nullptr };
	bool isFL{ false };
	bool is8Bit{ false };
};

SliceReaderXZ::SliceReaderXZ(bool isFL) :d{ new Impl } {
	d->header = new TCFSliceHeader;
	d->isFL = isFL;
	this->Init();
}

auto SliceReaderXZ::SetIs8Bit(bool is8Bit) -> void {
	d->is8Bit = is8Bit;
	if (d->header) {
		if (is8Bit) {
			d->header->dataType = SoDataSet::UNSIGNED_BYTE;
		}
		else {
			d->header->dataType = SoDataSet::UNSIGNED_SHORT;
		}
	}
}

SliceReaderXZ::SliceReaderXZ(const std::string& name, const std::string& dataGroup, const std::string& tileName)
	: d{ new Impl } {
	this->Init();
	d->header->dataGroup = dataGroup;
	d->header->tileName = tileName;
	this->SetFileName(name);
}

SliceReaderXZ::~SliceReaderXZ() {
	delete d->header;

	CloseHdfFile();
}

auto SliceReaderXZ::GetSliceHeader() -> TCFSliceHeader* {
	return d->header;
}

auto SliceReaderXZ::SetFileName(const std::string& name) -> int {
	if (OpenHdfFile(name) < 0)
		return -1;
	return 0;
}

auto SliceReaderXZ::ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	H5::Group group = d->group.openGroup(d->header->dataGroup.c_str());
	H5::DataSet dataset = group.openDataSet(d->header->tileName.c_str());
		
	H5::DataSpace dataSpace = dataset.getSpace();

	hsize_t dims[3];
	int dim = dataSpace.getSimpleExtentDims(dims);

	if (dim == 2) {
		dataSpace.close();
		dataset.close();
		group.close();
		return 1;
	}

	// The start, stride, count, and block arrays must be the same size as the rank of the dataspace.
	// Stride parameter is NULL, a contiguous hyperslab is selected
	hsize_t tcfOffset[3] = { (hsize_t)(offset[1]), (hsize_t)(offset[2] + slice + d->header->cropOffsetY), (hsize_t)(offset[0] + d->header->cropOffsetX) };
	//hsize_t tcfOffset[3] = { (hsize_t)(offset[1]), (hsize_t)(offset[2] + slice + d->header->cropOffsetX), (hsize_t)(offset[0] + d->header->cropOffsetY) };
	// How many blocks to select from the dataspace, in each dimension.
	hsize_t tcfCount[3] = { (hsize_t)(dataSize[1]), (hsize_t)(dataSize[2]), (hsize_t)(dataSize[0]) };

	dataSpace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]) };
	memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);
	if(d->is8Bit) {
		auto cast_data = static_cast<unsigned char*>(*data);
		dataset.read(cast_data, H5::PredType::NATIVE_UCHAR, memoryspace, dataSpace);
	}
	else {
		auto cast_data = static_cast<unsigned short*>(*data);
		dataset.read(cast_data, H5::PredType::NATIVE_UINT16, memoryspace, dataSpace);
	}

	memoryspace.close();

	dataSpace.close();
	dataset.close();
	group.close();

	return 1;
}

auto SliceReaderXZ::Init() -> int {
	d->header->oriX = 1;
	d->header->oriY = 1;
	d->header->oriZ = 1;
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_SHORT;

	return H5open();
}

auto SliceReaderXZ::OpenHdfFile(const std::string& name) -> int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
	d->file = H5::H5File(name.c_str(), H5F_ACC_RDONLY);

	// Find the 2D group

	// Read the attributes	
	if(d->is8Bit) {
		d->header->dataType = SoDataSet::UNSIGNED_BYTE;
	}else {
		d->header->dataType = SoDataSet::UNSIGNED_SHORT;
	}

	auto group = d->file.openGroup("Data");
	H5::Group targetGroup;
	if (d->isFL) {
		targetGroup = group.openGroup("3DFL");
	}
	else {
		targetGroup = group.openGroup("3D");
	}

	if (targetGroup.attrExists("DataCount")) {
		auto attr = targetGroup.openAttribute("DataCount");
		int64_t datacount;
		attr.read(H5::PredType::NATIVE_INT64, &datacount);
		d->header->dataCount = datacount;
		attr.close();
	}
	if (targetGroup.attrExists("SizeX")) {
		auto attr = targetGroup.openAttribute("SizeX");
		int64_t SizeX;
		attr.read(H5::PredType::NATIVE_INT64, &SizeX);
		d->header->oriX = d->header->sizeX = SizeX;
		attr.close();
	}
	if (targetGroup.attrExists("SizeY")) {
		auto attr = targetGroup.openAttribute("SizeY");
		int64_t SizeY;
		attr.read(H5::PredType::NATIVE_INT64, &SizeY);
		d->header->oriY = d->header->sizeY = SizeY;
		attr.close();
	}
	if (targetGroup.attrExists("SizeZ")) {
		auto attr = targetGroup.openAttribute("SizeZ");
		int64_t SizeZ;
		attr.read(H5::PredType::NATIVE_INT64, &SizeZ);
		d->header->oriZ = d->header->sizeZ = SizeZ;
		attr.close();
	}
	if (targetGroup.attrExists("ResolutionX")) {
		auto attr = targetGroup.openAttribute("ResolutionX");
		double resX;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resX);
		d->header->resolutionX = resX;
		attr.close();
	}
	if (targetGroup.attrExists("ResolutionY")) {
		auto attr = targetGroup.openAttribute("ResolutionY");
		double resY;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resY);
		d->header->resolutionY = resY;
		attr.close();
	}
	if (targetGroup.attrExists("ResolutionZ")) {
		auto attr = targetGroup.openAttribute("ResolutionZ");
		double resZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resZ);
		d->header->resolutionZ = resZ;
		attr.close();
	}
	if (targetGroup.attrExists("OffsetZ")) {
		auto attr = targetGroup.openAttribute("OffsetZ");
		double ofZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &ofZ);
		d->header->offsetZ = ofZ;
		attr.close();
	}
	targetGroup.close();
	group.close();

	d->group = d->file.openGroup(d->header->dataGroup.c_str());

	return 0;
}

auto SliceReaderXZ::CloseHdfFile() -> int {
	herr_t errStatus = -1;

	d->group.close();
	d->file.close();

	return errStatus;
}
