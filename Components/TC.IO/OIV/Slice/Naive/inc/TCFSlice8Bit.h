#pragma once
#include <enum.h>
#include "TC.IO.OIV.Slice.NaiveExport.h"

#pragma warning(push)
#pragma warning(disable:4268)
#include "hdf5.h"
#include "H5Cpp.h"
#pragma warning(pop)

#include <QMutex>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SbVec.h>
#include <LDM/nodes/SoDataSet.h>
#pragma warning(pop)

#include "ISliceReader.h"

//static QMutex _mutex;

class TC_IO_OIV_Slice_Naive_API SliceReader8Bit : public ISliceReader {
public:
	SliceReader8Bit(bool isFL = false);
	SliceReader8Bit(const std::string& name, const std::string& dataGroup = "/Data/3D", const std::string& tileName = "000000");
	~SliceReader8Bit();

	auto GetSliceHeader(void)->TCFSliceHeader* override;
	auto SetFileName(const std::string& name)->int override;

	auto ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice)->int override;

private:
	auto Init()->int;
	auto OpenHdfFile(const std::string& name)->int;
	auto CloseHdfFile(void)->int;

	struct Impl;
	std::unique_ptr<Impl> d;
};

class TC_IO_OIV_Slice_Naive_API SliceReaderYZ8Bit : public ISliceReader {
public:
	SliceReaderYZ8Bit(bool isFL = false);
	SliceReaderYZ8Bit(const std::string& name, const std::string& dataGroup = "/Data/3D", const std::string& tileName = "000000");
	~SliceReaderYZ8Bit();

	auto GetSliceHeader(void)->TCFSliceHeader* override;
	auto SetFileName(const std::string& name)->int override;

	auto ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice)->int override;

private:
	auto Init()->int;
	auto OpenHdfFile(const std::string& name)->int;
	auto CloseHdfFile(void)->int;

	struct Impl;
	std::unique_ptr<Impl> d;
};

class TC_IO_OIV_Slice_Naive_API SliceReaderXZ8Bit : public ISliceReader {
public:
	SliceReaderXZ8Bit(bool isFL = false);
	SliceReaderXZ8Bit(const std::string& name, const std::string& dataGroup = "/Data/3D", const std::string& tileName = "000000");
	~SliceReaderXZ8Bit();

	auto GetSliceHeader(void)->TCFSliceHeader* override;
	auto SetFileName(const std::string& name)->int override;

	auto ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice)->int override;

private:
	auto Init()->int;
	auto OpenHdfFile(const std::string& name)->int;
	auto CloseHdfFile(void)->int;

	struct Impl;
	std::unique_ptr<Impl> d;
};