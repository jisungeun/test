#pragma once
#include <enum.h>
#include "TC.IO.OIV.Slice.NaiveExport.h"

#pragma warning(push)
#pragma warning(disable:4268)
#include "hdf5.h"
#include "H5Cpp.h"
#pragma warning(pop)

#include <QMutex>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SbVec.h>
#include <LDM/nodes/SoDataSet.h>
#pragma warning(pop)

//static QMutex _mutex;

typedef struct HEADER_OF_TCFCOLOR {
	int			dataCount = -1;
	double      resolutionZ = -1.0;
	double		resolutionX = -1.0;
	double		resolutionY = -1.0;
	double      offsetZ = -1.0;
	int			oriZ = -1;
	int			oriX = -1;
	int			oriY = -1;
	int         sizeZ = -1;
	int			sizeX = -1;
	int			sizeY = -1;
	int			cropOffsetX = 0;
	int			cropOffsetY = 0;
	float		timeInterval = -1.f;
	float		positionC = -1.f;
	float		positionX = -1.f;
	float		positionY = -1.f;
	float		positionZ = -1.f;
	int			dataType = -1;
	std::string dataGroup;
	std::string tileName;
	std::string filePath;
} TCFSliceHeader;

class TC_IO_OIV_Slice_Naive_API ISliceReader {
public:
	ISliceReader();
	~ISliceReader();

	virtual auto GetSliceHeader(void)->TCFSliceHeader* = 0;
	virtual auto SetFileName(const std::string& name)->int = 0;

	virtual auto ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice)->int = 0;
};