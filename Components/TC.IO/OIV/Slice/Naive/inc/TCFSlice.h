#pragma once
#include <enum.h>
#include "TC.IO.OIV.Slice.NaiveExport.h"

#pragma warning(push)
#pragma warning(disable:4268)
#include "hdf5.h"
#include "H5Cpp.h"
#pragma warning(pop)

#include <QMutex>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SbVec.h>
#include <LDM/nodes/SoDataSet.h>
#pragma warning(pop)

#include "ISliceReader.h"

//static QMutex _mutex;

class TC_IO_OIV_Slice_Naive_API SliceReader : public ISliceReader {
public:
	SliceReader(bool isFL = false);
	SliceReader(const std::string& name, const std::string& dataGroup = "/Data/3D", const std::string& tileName = "000000");
	~SliceReader();

	auto SetIs8Bit(bool is8Bit)->void;
	auto GetSliceHeader(void)->TCFSliceHeader* override;	
	auto SetFileName(const std::string& name)->int override;	
		
	auto ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice)->int override;

private:
	auto Init()->int;
	auto OpenHdfFile(const std::string& name)->int;
	auto CloseHdfFile(void)->int;

	struct Impl;
	std::unique_ptr<Impl> d;
};

class TC_IO_OIV_Slice_Naive_API SliceReaderYZ : public ISliceReader {
public:
	SliceReaderYZ(bool isFL = false);
	SliceReaderYZ(const std::string& name, const std::string& dataGroup = "/Data/3D", const std::string& tileName = "000000");
	~SliceReaderYZ();

	auto SetIs8Bit(bool is8Bit)->void;
	auto GetSliceHeader(void)->TCFSliceHeader* override;
	auto SetFileName(const std::string& name)->int override;

	auto ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice)->int override;

private:
	auto Init()->int;
	auto OpenHdfFile(const std::string& name)->int;
	auto CloseHdfFile(void)->int;

	struct Impl;
	std::unique_ptr<Impl> d;
};

class TC_IO_OIV_Slice_Naive_API SliceReaderXZ : public ISliceReader {
public:
	SliceReaderXZ(bool isFL = false);
	SliceReaderXZ(const std::string& name, const std::string& dataGroup = "/Data/3D", const std::string& tileName = "000000");
	~SliceReaderXZ();

	auto SetIs8Bit(bool is8Bit)->void;
	auto GetSliceHeader(void)->TCFSliceHeader* override;
	auto SetFileName(const std::string& name)->int override;

	auto ReadSliceData(const SbVec3i32& offset, const SbVec3i32& dataSize, void** data, int slice)->int override;

private:
	auto Init()->int;
	auto OpenHdfFile(const std::string& name)->int;
	auto CloseHdfFile(void)->int;

	struct Impl;
	std::unique_ptr<Impl> d;
};