#pragma once

#include <memory>
#include <QWidget>

class SoQtViewer;
class SoQtExaminerViewer;
class TestWindow : public QWidget {
    Q_OBJECT
public:
    TestWindow(QWidget* parent = nullptr);
    ~TestWindow();

    auto SetViewer(SoQtExaminerViewer* viewer)->void;
    auto Construction()->void;
    static void FPSCallback(float fps, void* userData, SoQtViewer* viewer);

protected slots:
    void OnSpinBoxChanged(int val);
    void OnFileOpen();
    void OnFixedResolution();
    void OnResolutionValue(int);
    void OnMaxResolutionValue(int);
    void OnMinTilesToLoad(int);

private:    

    struct Impl;
    std::unique_ptr<Impl> d;
};