#pragma warning(disable:4268)

#include <QFileDialog>

#include <H5Cpp.h>

//Inventor
#include <Inventor/SoDB.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <Inventor/Qt/viewers/SoQtPlaneViewer.h>
#include <Inventor/Xt/viewers/SoXtExaminerViewer.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/sensors/SoTimerSensor.h>

#include <OivXYReader.h>
#include <OivLdmReaderXY.h>
#include <OivHdf5Reader.h>
#include <OivLdmReader.h>

#include "ui_TestWindow.h"
#include "TestWindow.h"

struct TestWindow::Impl {
    Ui::Window ui;
    SoRef<SoSeparator> root{ nullptr };
    SoRef<SoMaterial> matl{ nullptr };
    SoRef<SoDataRange> range{ nullptr };
    SoRef<SoTransferFunction> tf{ nullptr };
    SoRef<SoVolumeData> volData{ nullptr };
    SoRef<SoVolumeRender> volRender{ nullptr };

    SoRef<OivHdf5Reader> reader{ nullptr };
    SoRef<OivHdf5Reader> reader8bit{ nullptr };
    SoRef<OivLdmReader> ldmReader{ nullptr };

    int fileFormat{ 0 };
    QString filePath;    

    auto InitUI()->void;
    auto BuildSceneGraph()->void;
    auto OpenTCF(const QString& path)->void;
    auto ResetParameter()->void;
    auto ResetUI()->void;
        
    TestWindow* thisPointer{ nullptr };
    SoQtExaminerViewer* viewer{ nullptr };
};

TestWindow::TestWindow(QWidget* parent) : QWidget(parent), d{ new Impl } {
    d->thisPointer = this;    
    d->InitUI();
    setWindowTitle("TCF Format Test - Volume Reader");

    //d->sensor = new SoTimerSensor(&FPSCallback, this);
    //d->sensor->setInterval(1.0);
    //d->sensor->schedule();
}

TestWindow::~TestWindow() {
    
}

void TestWindow::FPSCallback(float fps, void* /*userData*/, SoQtViewer* /*viewer*/) {
    std::cout << "FPS: "<< fps << std::endl;
}


auto TestWindow::SetViewer(SoQtExaminerViewer* viewer) -> void {
    d->ui.renderSocket->layout()->addWidget(viewer->getWidget());
    d->viewer = viewer;
    viewer->setSceneGraph(d->root.ptr());

    d->viewer->setFramesPerSecondCallback(&TestWindow::FPSCallback, this);    
}



auto TestWindow::Impl::InitUI() -> void {
    ui.setupUi(thisPointer);
    ui.fileFormat->setReadOnly(true);
    ui.filePath->setReadOnly(true);
    ui.spinBox->setRange(1, 17);
    ui.spinBox->setValue(1);

    ui.fixChk->setChecked(true);
    ui.resSpin->setRange(0, 3);
    ui.resSpin->setValue(0);

    ui.meanTileSpin->setRange(1, 300);
    ui.meanTileSpin->setValue(1);

    ui.maxResSpin->setRange(0, 3);
    ui.maxResSpin->setValue(0);

    connect(ui.spinBox, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnSpinBoxChanged(int)));
    connect(ui.pushButton, SIGNAL(clicked()), thisPointer, SLOT(OnFileOpen()));
    connect(ui.fixChk, SIGNAL(clicked()), thisPointer, SLOT(OnFixedResolution()));
    connect(ui.resSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnResolutionValue(int)));
    connect(ui.meanTileSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnMinTilesToLoad(int)));
    connect(ui.maxResSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnMaxResolutionValue(int)));
    
    auto layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    ui.renderSocket->setLayout(layout);
}

auto TestWindow::Construction() -> void {
    d->BuildSceneGraph();
}

auto TestWindow::Impl::BuildSceneGraph() -> void {
    root = new SoSeparator;
    matl = new SoMaterial;
    range = new SoDataRange;
    tf = new SoTransferFunction;
    volData = new SoVolumeData;
    volRender = new SoVolumeRender;

    matl->diffuseColor.setValue(1, 1, 1);
    matl->ambientColor.setValue(1, 1, 1);

    tf->predefColorMap = SoTransferFunction::AIRWAY;

    reader = new OivHdf5Reader;
    reader->setDataGroupPath("/Data/3D");
    reader->setTileName("000000");
    reader8bit = new OivHdf5Reader(false,true);
    reader8bit->setDataGroupPath("/Data/3D");
    reader8bit->setTileName("000000");
    ldmReader = new OivLdmReader(false, true);
    ldmReader->setDataGroupPath("/Data/3D");
    ldmReader->setTileDimension(128, 128, 128);
    ldmReader->setTileName("000000");


    root->addChild(matl.ptr());
    root->addChild(range.ptr());
    root->addChild(tf.ptr());
    root->addChild(volData.ptr());
    root->addChild(volRender.ptr());
}

auto TestWindow::Impl::ResetParameter() -> void {
    volData->ldmResourceParameters.getValue()->fixedResolution = FALSE;
    volData->ldmResourceParameters.getValue()->resolution = 0;
    volData->ldmResourceParameters.getValue()->maxResolutionThreshold = 0;
    volData->ldmResourceParameters.getValue()->minTilesToLoad = 1;
}

auto TestWindow::Impl::ResetUI() -> void {
    thisPointer->blockSignals(true);

    ui.fixChk->setChecked(false);

    ui.spinBox->setValue(1);
        
    ui.resSpin->setValue(0);

    ui.meanTileSpin->setValue(1);
        
    ui.maxResSpin->setValue(0);
    thisPointer->blockSignals(false);
}

auto TestWindow::Impl::OpenTCF(const QString& path) -> void {
    ResetParameter();
    ResetUI();
    filePath = path;

    switch(fileFormat) {
        case 0:
        case 1:
            reader8bit->setFilename(filePath.toStdString());
            volData->setReader(*reader8bit);
            range->min = 10;
            range->max = 35;
            break;
        case 2:
        case 3:
            reader->setFilename(filePath.toStdString());
            volData->setReader(*reader);
            range->min = 13300;
            range->max = 13600;
            break;
        default://4,5
            ldmReader->setFilename(filePath.toStdString());
            volData->setReader(*ldmReader);
            range->min = 10;
            range->max = 35;
            break;
    }

    viewer->viewAll();
}

void TestWindow::OnFileOpen() {
    const auto path = QFileDialog::getOpenFileName(this, "Open TCF", d->filePath, "TCF (*.tcf)");

    if(path.isEmpty()) {
        d->ui.filePath->setText("");
        d->ui.fileFormat->setText("");
        return;
    }
    H5::H5File file(path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
    auto group = file.openGroup("Data");

    if(false == group.attrExists("StructureType")) {
        d->ui.filePath->setText("Selected TCF is not a test data");
        d->ui.fileFormat->setText("");
        return;
    }

    auto attr = group.openAttribute("StructureType");
    int64_t dataType;
    attr.read(H5::PredType::NATIVE_INT64, &dataType);
    attr.close();
    group.close();
    file.close();

    d->ui.filePath->setText(path);

    dataType = 5;

    switch(dataType) {
        case 0:
            d->ui.fileFormat->setText("uint8");
            break;
        case 1:
            d->ui.fileFormat->setText("uint8 compressed");
            break;
        case 2:
            d->ui.fileFormat->setText("uint16");
            break;
        case 3:
            d->ui.fileFormat->setText("uint16 compressed");
            break;
        case 4:
            d->ui.fileFormat->setText("uint8 LDM");
            break;
        case 5:
            d->ui.fileFormat->setText("uint8 LDM compressed");
            break;
        default:
            break;
    }
    d->fileFormat = dataType;
    d->OpenTCF(path);
}

void TestWindow::OnSpinBoxChanged(int val) {
    QString tileName = QString("%1").arg(val-1, 6, 10, QLatin1Char('0'));
    if(d->fileFormat == 0 || d->fileFormat == 1) {
        d->reader8bit->setTileName(tileName.toStdString());
        d->reader8bit->touch();
    }
    else if(d->fileFormat == 2 || d->fileFormat == 3) {
        d->reader->setTileName(tileName.toStdString());
        d->reader->touch();
    }else {
        d->ldmReader->setTileName(tileName.toStdString());
        d->ldmReader->touch();
    }
}

void TestWindow::OnMaxResolutionValue(int val) {
    d->volData->ldmResourceParameters.getValue()->maxResolutionThreshold = val;
}

void TestWindow::OnFixedResolution() {
    d->volData->ldmResourceParameters.getValue()->fixedResolution = d->ui.fixChk->isChecked();    
}

void TestWindow::OnResolutionValue(int val) {
    d->volData->ldmResourceParameters.getValue()->resolution = val;
}

void TestWindow::OnMinTilesToLoad(int val) {
    d->volData->ldmResourceParameters.getValue()->minTilesToLoad = val;
}