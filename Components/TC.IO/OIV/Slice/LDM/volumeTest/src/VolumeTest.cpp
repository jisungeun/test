#pragma warning(disable:4268)

#include <iostream>

#include <QVBoxLayout>
#include <QSpinBox>
#include <QMainWindow>

//Open Inventor
#include <Inventor/SoDB.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <Inventor/Qt/viewers/SoQtPlaneViewer.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Medical/helpers/MedicalHelper.h>
//
#include <OivXYReader.h>
#include <OivLdmReaderXY.h>
#include <OivHdf5Reader.h>
#include <OivLdmReader.h>

#include "TestWindow.h"

#define IS_VOLUME 1

int main(int argc, char* argv[]) {
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);    
    QApplication app(argc, argv);
    TestWindow* window = new TestWindow;

    SoQt::init(window);
    SoVolumeRendering::init();
    OivHdf5Reader::initClass();
    OivLdmReader::initClass();
    OivXYReader::initClass();
    OivLdmReaderXY::initClass();

    window->Construction();

    SoQtExaminerViewer* viewer = new SoQtExaminerViewer(window);
    window->SetViewer(viewer);
    
    window->showMaximized();
    app.exec();
            
    delete window;    

    OivLdmReaderXY::exitClass();
    OivXYReader::exitClass();
    OivLdmReader::exitClass();
    OivHdf5Reader::exitClass();
    SoVolumeRendering::finish();
    SoQt::finish();
    return 0;
}
