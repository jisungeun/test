#define LOGGER_TAG "[LDM]"
#include <TCLogger.h>

#include "LDMSliceClasses8Bit.h"

#include <iomanip>
#include <HDF5Mutex.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SbVec.h>
#include <LDM/nodes/SoDataSet.h>
#pragma warning(pop)

/// <summary>
/// LDM slice reader for XY Plane
/// </summary>

struct TCFLdmReaderXY8Bit::Impl {
	hid_t fileId = -1;
	hid_t tcfGrpId = -1;
	std::string ldmGroupPath;
	LdmHeaderSlice* header{ nullptr };

	bool isFL{ false };
	QMutex mutex;

	H5::H5File file;
};

TCFLdmReaderXY8Bit::TCFLdmReaderXY8Bit(bool isFL) :d{ new Impl } {
	d->isFL = isFL;
	d->header = new LdmHeaderSlice;
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_BYTE;
}

TCFLdmReaderXY8Bit::~TCFLdmReaderXY8Bit() {
	d->file.close();
}
auto TCFLdmReaderXY8Bit::ReadLdmSliceData(const std::string _tileName, void** _tileData, int zIndex, const SbBox3i32& tilePosition)->int {
	//d->header->tileName = _tileName;
	int bufSize = ReadDataSet(_tileName, _tileData, zIndex, tilePosition);

	return bufSize;
}

auto TCFLdmReaderXY8Bit::SetFileName(const std::string& name, int numZero)->int {
	if (OpenHdfFile(name, numZero) < 0) {
		return -1;
	}
	return 0;
}

auto TCFLdmReaderXY8Bit::GetTcfHeader()->LdmHeaderSlice* {
	return d->header;
}

auto TCFLdmReaderXY8Bit::GetLdmGroup()->int {
	return 0;
}

auto TCFLdmReaderXY8Bit::OpenHdfFile(const std::string& name, int numZero)->int {
	d->file = H5::H5File(name.c_str(), H5F_ACC_RDONLY);

	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(numZero) << std::setfill('0') << 0;
	auto ress = tmpStr.str();
	H5::Group metaGrp;
	H5::Group grp;
	if (d->isFL) {
		std::string delimiter = "/";
		auto s = d->header->dataGroup;
		size_t pos = 0;
		std::string token;
		std::vector<std::string> tokens;
		while ((pos = s.find(delimiter)) != std::string::npos) {
			token = s.substr(0, pos);
			tokens.push_back(token);
			s.erase(0, pos + delimiter.length());
		}
		std::string final_grp = "/" + tokens[1] + "/" + tokens[2];
		metaGrp = d->file.openGroup(final_grp.c_str());

		grp = metaGrp.openGroup(s.c_str());
	}
	else {
		metaGrp = grp = d->file.openGroup(d->header->dataGroup.c_str());
	}
	auto dset = grp.openGroup(d->header->tileName.c_str());

	auto tile = dset.openDataSet(ress.c_str());

	d->header->dataType = SoDataSet::UNSIGNED_BYTE;
	auto maxTile = 0;
	if (tile.attrExists("TileDataSizeX")) {
		auto attr = tile.openAttribute("TileDataSizeX");
		int64_t tileSize;
		attr.read(H5::PredType::NATIVE_INT64, &tileSize);
		d->header->maxTileSize = tileSize;
		attr.close();
	}

	tile.close();
	dset.close();

	if (metaGrp.attrExists("DataCount")) {
		auto attr = metaGrp.openAttribute("DataCount");
		int64_t datacount;
		attr.read(H5::PredType::NATIVE_INT64, &datacount);
		d->header->dataCount = datacount;
		attr.close();
	}
	if (metaGrp.attrExists("SizeX")) {
		auto attr = metaGrp.openAttribute("SizeX");
		int64_t SizeX;
		attr.read(H5::PredType::NATIVE_INT64, &SizeX);
		d->header->sizeX = SizeX;
		attr.close();
	}
	if (metaGrp.attrExists("SizeY")) {
		auto attr = metaGrp.openAttribute("SizeY");
		int64_t SizeY;
		attr.read(H5::PredType::NATIVE_INT64, &SizeY);
		d->header->sizeY = SizeY;
		attr.close();
	}
	if (metaGrp.attrExists("SizeZ")) {
		auto attr = metaGrp.openAttribute("SizeZ");
		int64_t SizeZ;
		attr.read(H5::PredType::NATIVE_INT64, &SizeZ);
		d->header->sizeZ = SizeZ;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionX")) {
		auto attr = metaGrp.openAttribute("ResolutionX");
		double resX;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resX);
		d->header->resolutionX = resX;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionY")) {
		auto attr = metaGrp.openAttribute("ResolutionY");
		double resY;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resY);
		d->header->resolutionY = resY;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionZ")) {
		auto attr = metaGrp.openAttribute("ResolutionZ");
		double resZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resZ);
		d->header->resolutionZ = resZ;
		attr.close();
	}
	if (metaGrp.attrExists("OffsetZ")) {
		auto attr = metaGrp.openAttribute("OffsetZ");
		double ofZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &ofZ);
		d->header->offsetZ = ofZ;
		attr.close();
	}

	grp.close();
	metaGrp.close();

	return 0;
}

auto TCFLdmReaderXY8Bit::ReadDataSet(std::string _tile_name, void** _data, int idx, const SbBox3i32& tilePosition)->int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
	
	H5::Group group = d->file.openGroup(d->header->dataGroup);
	H5::Group dataSet = group.openGroup(d->header->tileName);
	H5::DataSet tile = dataSet.openDataSet(_tile_name);

	auto cast_data = static_cast<unsigned char*>(*_data);

	H5::DataSpace dataSpace = tile.getSpace();
	hsize_t dims[3];
	dataSpace.getSimpleExtentDims(dims);

	auto sattr = tile.openAttribute("SamplingStep");
	int64_t ratio;
	sattr.read(H5::PredType::NATIVE_INT64, &ratio);
	sattr.close();		

	auto realIdx = static_cast<hsize_t>(static_cast<int>(static_cast<float>(idx) / static_cast<float>(ratio)) % d->header->tileSizeZ);


	hsize_t offset[3]{ 0,0, realIdx };
	hsize_t dataSize[3]{ static_cast<hsize_t>(d->header->tileSizeX),static_cast<hsize_t>(d->header->tileSizeY),1 };

	hsize_t tcfOffset[3] = { (hsize_t)(offset[2]), (hsize_t)(offset[1]), (hsize_t)(offset[0]) };

	hsize_t tcfCount[3] = { (hsize_t)(dataSize[2]), (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]) };

	dataSpace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[1]), (hsize_t)(dataSize[0]) };
	memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);

	tile.read(cast_data, H5::PredType::NATIVE_UCHAR, memoryspace, dataSpace);

	memoryspace.close();

	dataSpace.close();
	tile.close();
	dataSet.close();
	group.close();

	return 1;
}
/// <summary>
/// LDM slice reader for YZ Plane
/// </summary>

struct TCFLdmReaderYZ8Bit::Impl {
	hid_t fileId = -1;
	hid_t tcfGrpId = -1;
	std::string ldmGroupPath;
	LdmHeaderSlice* header{ nullptr };

	bool isFL{ false };
	QMutex mutex;

	H5::H5File file;
};

TCFLdmReaderYZ8Bit::TCFLdmReaderYZ8Bit(bool isFL) :d{ new Impl } {
	d->isFL = isFL;
	d->header = new LdmHeaderSlice;
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_BYTE;
}

TCFLdmReaderYZ8Bit::~TCFLdmReaderYZ8Bit() {
	d->file.close();
}

auto TCFLdmReaderYZ8Bit::ReadDataSet(std::string _tile_name, void** _data, int idx, const SbBox3i32& tilePosition)->int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	H5::Group group = d->file.openGroup(d->header->dataGroup);
	H5::Group dataSet = group.openGroup(d->header->tileName);
	H5::DataSet tile = dataSet.openDataSet(_tile_name);

	auto oxattr = tile.openAttribute("DataIndexOffsetPointX");
	int64_t offsetX;
	oxattr.read(H5::PredType::NATIVE_INT64, &offsetX);
	oxattr.close();

	auto lxattr = tile.openAttribute("DataIndexLastPointX");
	int64_t lastX;
	lxattr.read(H5::PredType::NATIVE_INT64, &lastX);
	lxattr.close();

	//if ((offsetX > idx || lastX < idx) && false == d->isFL) {
	if (offsetX > idx || lastX < idx) {
		tile.close();
		dataSet.close();
		group.close();
		return 1;
	}

	auto cast_data = static_cast<unsigned char*>(*_data);

	tile.read(cast_data, H5::PredType::NATIVE_UCHAR);
	tile.close();

	/*
	H5::DataSpace dataSpace = tile.getSpace();
	hsize_t dims[3];
	dataSpace.getSimpleExtentDims(dims);

	auto sattr = tile.openAttribute("SamplingStep");
	int64_t ratio;
	sattr.read(H5::PredType::NATIVE_INT64, &ratio);
	sattr.close();

	auto realIdx = static_cast<int>(idx / static_cast<float>(ratio));
	hsize_t offset[3]{0,0, 0};
	//hsize_t dataSize[3]{ d->header->tileSizeX, d->header->tileSizeY,d->header->tileSizeZ };
	hsize_t dataSize[3]{ 1, d->header->tileSizeY,d->header->tileSizeZ };

	hsize_t tcfOffset[3] = { (hsize_t)(offset[0]), (hsize_t)(offset[1]), (hsize_t)(offset[2]) };

	hsize_t tcfCount[3] = { (hsize_t)(dataSize[0]), (hsize_t)(dataSize[1]), (hsize_t)(dataSize[2]) };

	dataSpace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

	int out_rank = 2;
	const hsize_t out_dim[2] = { (hsize_t)(dataSize[1]), (hsize_t)(dataSize[2]) };

	H5::DataSpace memoryspace(out_rank, out_dim);

	hsize_t out_offset[2] = { 0, 0 };
	hsize_t out_count[2] = { (hsize_t)(dataSize[1]), (hsize_t)(dataSize[2]) };
	memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);
	try {
		tile.read(cast_data, H5::PredType::NATIVE_UINT16, memoryspace, dataSpace);
	}
	catch(H5::Exception& e){
		std::cout << e.getDetailMsg() << std::endl;
	}

	memoryspace.close();

	dataSpace.close();
	tile.close();*/
	dataSet.close();
	group.close();

	return 1;
}

auto TCFLdmReaderYZ8Bit::ReadLdmSliceData(const std::string _tileName, void** _tileData, int xIndex, const SbBox3i32& tilePosition)->int {
	//d->header->tileName = _tileName;
	int bufSize = ReadDataSet(_tileName, _tileData, xIndex, tilePosition);

	return bufSize;
}

auto TCFLdmReaderYZ8Bit::SetFileName(const std::string& name, int numZero)->int {
	if (OpenHdfFile(name, numZero) < 0) {
		return -1;
	}
	return 0;
}

auto TCFLdmReaderYZ8Bit::GetTcfHeader()->LdmHeaderSlice* {
	return d->header;
}

auto TCFLdmReaderYZ8Bit::OpenHdfFile(const std::string& name, int numZero)->int {
	d->file = H5::H5File(name.c_str(), H5F_ACC_RDONLY);

	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(numZero) << std::setfill('0') << 0;
	auto ress = tmpStr.str();
	H5::Group metaGrp;
	H5::Group grp;
	if (d->isFL) {
		std::string delimiter = "/";
		auto s = d->header->dataGroup;
		size_t pos = 0;
		std::string token;
		std::vector<std::string> tokens;
		while ((pos = s.find(delimiter)) != std::string::npos) {
			token = s.substr(0, pos);
			tokens.push_back(token);
			s.erase(0, pos + delimiter.length());
		}
		std::string final_grp = "/" + tokens[1] + "/" + tokens[2];
		metaGrp = d->file.openGroup(final_grp);
		grp = metaGrp.openGroup(s);

	}
	else {
		metaGrp = grp = d->file.openGroup(d->header->dataGroup.c_str());
	}

	auto dset = grp.openGroup(d->header->tileName.c_str());

	auto tile = dset.openDataSet(ress.c_str());

	d->header->dataType = SoDataSet::UNSIGNED_BYTE;
	auto maxTile = 0;
	if (tile.attrExists("TileDataSizeX")) {
		auto attr = tile.openAttribute("TileDataSizeX");
		int64_t tileSize;
		attr.read(H5::PredType::NATIVE_INT64, &tileSize);
		d->header->maxTileSize = tileSize;
		attr.close();
	}

	tile.close();
	dset.close();

	if (metaGrp.attrExists("DataCount")) {
		auto attr = metaGrp.openAttribute("DataCount");
		int64_t datacount;
		attr.read(H5::PredType::NATIVE_INT64, &datacount);
		d->header->dataCount = datacount;
		attr.close();
	}
	if (metaGrp.attrExists("SizeX")) {
		auto attr = metaGrp.openAttribute("SizeX");
		int64_t SizeX;
		attr.read(H5::PredType::NATIVE_INT64, &SizeX);
		d->header->sizeX = SizeX;
		attr.close();
	}
	if (metaGrp.attrExists("SizeY")) {
		auto attr = metaGrp.openAttribute("SizeY");
		int64_t SizeY;
		attr.read(H5::PredType::NATIVE_INT64, &SizeY);
		d->header->sizeY = SizeY;
		attr.close();
	}
	if (metaGrp.attrExists("SizeZ")) {
		auto attr = metaGrp.openAttribute("SizeZ");
		int64_t SizeZ;
		attr.read(H5::PredType::NATIVE_INT64, &SizeZ);
		d->header->sizeZ = SizeZ;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionX")) {
		auto attr = metaGrp.openAttribute("ResolutionX");
		double resX;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resX);
		d->header->resolutionX = resX;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionY")) {
		auto attr = metaGrp.openAttribute("ResolutionY");
		double resY;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resY);
		d->header->resolutionY = resY;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionZ")) {
		auto attr = metaGrp.openAttribute("ResolutionZ");
		double resZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resZ);
		d->header->resolutionZ = resZ;
		attr.close();
	}
	if (metaGrp.attrExists("OffsetZ")) {
		auto attr = metaGrp.openAttribute("OffsetZ");
		double ofZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &ofZ);
		d->header->offsetZ = ofZ;
		attr.close();
	}

	grp.close();
	metaGrp.close();

	return 0;
}

auto TCFLdmReaderYZ8Bit::GetLdmGroup()->int {
	return 0;
}



/// <summary>
/// LDM slice reader for XZ Plane
/// </summary>

struct TCFLdmReaderXZ8Bit::Impl {
	hid_t fileId = -1;
	hid_t tcfGrpId = -1;
	std::string ldmGroupPath;
	LdmHeaderSlice* header{ nullptr };

	bool isFL{ false };
	QMutex mutex;

	H5::H5File file;
};

TCFLdmReaderXZ8Bit::TCFLdmReaderXZ8Bit(bool isFL) :d{ new Impl } {
	d->isFL = isFL;
	d->header = new LdmHeaderSlice;
	d->header->sizeX = 1;
	d->header->sizeY = 1;
	d->header->sizeZ = 1;
	d->header->resolutionX = 1;
	d->header->resolutionY = 1;
	d->header->resolutionZ = 1;
	d->header->dataCount = 1;
	d->header->timeInterval = 0.f;
	d->header->dataType = SoDataSet::UNSIGNED_BYTE;
}

TCFLdmReaderXZ8Bit::~TCFLdmReaderXZ8Bit() {

}

auto TCFLdmReaderXZ8Bit::ReadDataSet(std::string _tile_name, void** _data, int idx, const SbBox3i32& tilePosition)->int {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

	H5::Group group = d->file.openGroup(d->header->dataGroup);
	H5::Group dataSet = group.openGroup(d->header->tileName);
	H5::DataSet tile = dataSet.openDataSet(_tile_name);

	auto oyattr = tile.openAttribute("DataIndexOffsetPointY");
	int64_t offsetY;
	oyattr.read(H5::PredType::NATIVE_INT64, &offsetY);
	oyattr.close();

	auto lyattr = tile.openAttribute("DataIndexLastPointY");
	int64_t lastY;
	lyattr.read(H5::PredType::NATIVE_INT64, &lastY);
	lyattr.close();

	//if ((offsetY > idx || lastY < idx) && false ==d->isFL) {
	if (offsetY > idx || lastY < idx) {
		tile.close();
		dataSet.close();
		group.close();
		return 1;
	}

	auto cast_data = static_cast<unsigned char*>(*_data);

	tile.read(cast_data, H5::PredType::NATIVE_UCHAR);
	tile.close();

	dataSet.close();
	group.close();

	return 1;
}

auto TCFLdmReaderXZ8Bit::ReadLdmSliceData(const std::string _tileName, void** _tileData, int xIndex, const SbBox3i32& tilePosition)->int {
	int bufSize = ReadDataSet(_tileName, _tileData, xIndex, tilePosition);

	return bufSize;
}

auto TCFLdmReaderXZ8Bit::SetFileName(const std::string& name, int numZero)->int {
	if (OpenHdfFile(name, numZero) < 0) {
		return -1;
	}
	return 0;
}

auto TCFLdmReaderXZ8Bit::GetTcfHeader()->LdmHeaderSlice* {
	return d->header;
}

auto TCFLdmReaderXZ8Bit::OpenHdfFile(const std::string& name, int numZero)->int {
	d->file = H5::H5File(name.c_str(), H5F_ACC_RDONLY);

	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(numZero) << std::setfill('0') << 0;
	auto ress = tmpStr.str();
	H5::Group metaGrp;
	H5::Group grp;
	if (d->isFL) {
		std::string delimiter = "/";
		auto s = d->header->dataGroup;
		size_t pos = 0;
		std::string token;
		std::vector<std::string> tokens;
		while ((pos = s.find(delimiter)) != std::string::npos) {
			token = s.substr(0, pos);
			tokens.push_back(token);
			s.erase(0, pos + delimiter.length());
		}
		std::string final_grp = "/" + tokens[1] + "/" + tokens[2];
		metaGrp = d->file.openGroup(final_grp);
		grp = metaGrp.openGroup(s);

	}
	else {
		metaGrp = grp = d->file.openGroup(d->header->dataGroup.c_str());
	}

	auto dset = grp.openGroup(d->header->tileName.c_str());

	auto tile = dset.openDataSet(ress.c_str());

	d->header->dataType = SoDataSet::UNSIGNED_BYTE;
	auto maxTile = 0;
	if (tile.attrExists("TileDataSizeX")) {
		auto attr = tile.openAttribute("TileDataSizeX");
		int64_t tileSize;
		attr.read(H5::PredType::NATIVE_INT64, &tileSize);
		d->header->maxTileSize = tileSize;
		attr.close();
	}

	tile.close();
	dset.close();

	if (metaGrp.attrExists("DataCount")) {
		auto attr = metaGrp.openAttribute("DataCount");
		int64_t datacount;
		attr.read(H5::PredType::NATIVE_INT64, &datacount);
		d->header->dataCount = datacount;
		attr.close();
	}
	if (metaGrp.attrExists("SizeX")) {
		auto attr = metaGrp.openAttribute("SizeX");
		int64_t SizeX;
		attr.read(H5::PredType::NATIVE_INT64, &SizeX);
		d->header->sizeX = SizeX;
		attr.close();
	}
	if (metaGrp.attrExists("SizeY")) {
		auto attr = metaGrp.openAttribute("SizeY");
		int64_t SizeY;
		attr.read(H5::PredType::NATIVE_INT64, &SizeY);
		d->header->sizeY = SizeY;
		attr.close();
	}
	if (metaGrp.attrExists("SizeZ")) {
		auto attr = metaGrp.openAttribute("SizeZ");
		int64_t SizeZ;
		attr.read(H5::PredType::NATIVE_INT64, &SizeZ);
		d->header->sizeZ = SizeZ;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionX")) {
		auto attr = metaGrp.openAttribute("ResolutionX");
		double resX;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resX);
		d->header->resolutionX = resX;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionY")) {
		auto attr = metaGrp.openAttribute("ResolutionY");
		double resY;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resY);
		d->header->resolutionY = resY;
		attr.close();
	}
	if (metaGrp.attrExists("ResolutionZ")) {
		auto attr = metaGrp.openAttribute("ResolutionZ");
		double resZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &resZ);
		d->header->resolutionZ = resZ;
		attr.close();
	}
	if (metaGrp.attrExists("OffsetZ")) {
		auto attr = metaGrp.openAttribute("OffsetZ");
		double ofZ;
		attr.read(H5::PredType::NATIVE_DOUBLE, &ofZ);
		d->header->offsetZ = ofZ;
		attr.close();
	}

	grp.close();
	metaGrp.close();

	return 0;
}

auto TCFLdmReaderXZ8Bit::GetLdmGroup()->int {
	return 0;
}