
#define LOGGER_TAG "[LDMYZ]"
#include <TCLogger.h>
#include <iomanip>
#pragma warning(push)
#pragma warning(disable : 4819)
#include <Inventor/SoInput.h>
#include <LDM/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#pragma warning(pop)

#include "OivLdmReaderXZ.h"

SO_FIELDCONTAINER_SOURCE(OivLdmReaderXZ);

struct OivLdmReaderXZ::Impl {
	//std::shared_ptr<ILdmSliceReader> hdfReader{ nullptr };
	std::shared_ptr<TCFLdmReaderXZ> hdfReader{ nullptr };

	int num_tiles{ 0 };
	int num_zeros{ 1 };
	int tileSize[3]{ 0, };
	int _Index{ 0 };
	bool isFL{ false };
	int sigBit{ 16 };
};

OivLdmReaderXZ::OivLdmReaderXZ(bool isFL,bool is8Bit) : d{ new Impl } {
	SO_FIELDCONTAINER_CONSTRUCTOR(OivLdmReaderXZ);
	d->hdfReader = std::make_shared<TCFLdmReaderXZ>(isFL);
	if(is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
	m_dataConverted = TRUE;
	d->isFL = isFL;
}

OivLdmReaderXZ::~OivLdmReaderXZ() {

}

auto OivLdmReaderXZ::set8Bit(bool is8Bit) -> void {
	d->hdfReader->SetIs8Bit(is8Bit);
	if (is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}
	else {
		d->sigBit = 16;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
}

auto OivLdmReaderXZ::setTileName(const std::string& tileName)->void {
	auto header = d->hdfReader->GetTcfHeader();
	header->tileName = tileName;
}

auto OivLdmReaderXZ::setDataGroupPath(const std::string& dataGroup)->void {
	auto header = d->hdfReader->GetTcfHeader();
	header->dataGroup = dataGroup;
}

auto OivLdmReaderXZ::setTileDimension(int dimX, int dimY, int dimZ)->void {
	d->tileSize[0] = dimX;
	d->tileSize[1] = dimY;
	d->tileSize[2] = dimZ;
}

auto OivLdmReaderXZ::getTileString(int _tileId, int _nDigits)->std::string {
	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(_nDigits) << std::setfill('0') << _tileId;

	return tmpStr.str();
}

auto OivLdmReaderXZ::setIndex(int index)->bool {
	auto header = d->hdfReader->GetTcfHeader();
	if (index >= header->sizeY) {
		return false;
	}
	if (index < 0) {
		return false;
	}
	d->_Index = index;
	return true;
}

auto OivLdmReaderXZ::getIndex()->int {
	return d->_Index;
}

auto OivLdmReaderXZ::getNumZeros(const std::string& filename)->int {
	H5::H5File file(filename, H5F_ACC_RDONLY);
	if (d->isFL) {
		for (auto i = 0; i < 3; i++) {
			auto gName = QString("/Data/3DFL/CH%1").arg(i);
			if (false == file.exists(gName.toStdString())) {
				continue;
			}
			auto chName = QString("/Data/3DFL/CH%1/000000").arg(i);
			H5::Group group = file.openGroup(chName.toStdString());

			d->num_tiles = group.getNumObjs();
			auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
			std::size_t pos = final_tile_name.find("_");
			std::string numTxt = final_tile_name.substr(pos + 1);
			d->num_zeros = static_cast<int>(numTxt.length());

			group.close();
			break;
		}
	}
	else {
		if (false == file.exists("/Data/3D/000000")) {
			file.close();
			return -1;
		}
		H5::Group group = file.openGroup("/Data/3D/000000");

		d->num_tiles = group.getNumObjs();
		auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
		std::size_t pos = final_tile_name.find("_");
		std::string numTxt = final_tile_name.substr(pos + 1);
		d->num_zeros = static_cast<int>(numTxt.length());

		group.close();
	}
	file.close();
	return 0;
}

auto OivLdmReaderXZ::setFilename(const SbString& filename) -> int {
	if (nullptr == d->hdfReader) {
		return 0;
	}
	getNumZeros(filename.toStdString());
	auto header = d->hdfReader->GetTcfHeader();
	header->filePath = filename.toStdString();
	header->tileSizeX = d->tileSize[0];
	header->tileSizeY = d->tileSize[1];
	header->tileSizeZ = d->tileSize[2];
	int errStatus = d->hdfReader->SetFileName(filename.toStdString(), d->num_zeros);
	if (errStatus < 0) {
		return 0;
	}

	m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

	return 1;
}
auto OivLdmReaderXZ::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm)->SoVolumeReader::ReadError {	
	auto header = d->hdfReader->GetTcfHeader();
	SbVec3f _min = SbVec3f((float)(-header->sizeX * header->resolutionX) / 2, (float)(-header->sizeY * header->resolutionY) / 2, (float)(-header->sizeZ * header->resolutionZ) / 2);
	SbVec3f _max = SbVec3f((float)(header->sizeX * header->resolutionX) / 2, (float)(header->sizeY * header->resolutionY) / 2, (float)(header->sizeZ * header->resolutionZ) / 2);
	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	//idm = SbVec3i32(header->sizeX, header->sizeY, header->sizeZ);
	idm = SbVec3i32(header->sizeX, 1, header->sizeZ);
	return SoVolumeReader::ReadError::RD_NO_ERROR;
}
auto OivLdmReaderXZ::getBorderFlag() -> int {
	return 0;
}
auto OivLdmReaderXZ::getNumSignificantBits() -> int {
	return d->sigBit;
}
auto OivLdmReaderXZ::getTileSize(SbVec3i32& size)->SbBool {
	//size = SbVec3i32(d->tileSize[0], d->tileSize[1], d->tileSize[2]);
	size = SbVec3i32(d->tileSize[0], 1, d->tileSize[2]);
	return TRUE;
}
auto OivLdmReaderXZ::getMinMax(int64_t& min, int64_t& max)->SbBool {
	auto header = d->hdfReader->GetTcfHeader();
	min = header->riMin;
	max = header->riMax;
	return TRUE;
}
auto OivLdmReaderXZ::getMinMax(double& min, double& max)->SbBool {
	auto header = d->hdfReader->GetTcfHeader();
	min = header->riMin;
	max = header->riMax;
	return TRUE;
}
auto OivLdmReaderXZ::readTile(int index, const SbBox3i32& tilePosition)->SoBufferObject* {
	auto header = d->hdfReader->GetTcfHeader();
	int maxNuTiles = d->num_tiles;
	if (index >= maxNuTiles) {
		return nullptr;
	}
	std::string tileName = getTileString(index, d->num_zeros);
	int voxelBytes = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

	SoCpuBufferObject* tileBuffer = new SoCpuBufferObject;
	size_t bufferSize;
	bufferSize = voxelBytes * d->tileSize[1] * d->tileSize[2] * d->tileSize[0];
	tileBuffer->setSize(bufferSize);
	void* buffer = (void*)tileBuffer->map(SoBufferObject::SET);
	d->hdfReader->ReadLdmSliceData(tileName, &buffer, d->_Index, tilePosition);
	
	tileBuffer->unmap();

	return tileBuffer;
}
auto OivLdmReaderXZ::getSubSlice(const SbBox2i32&, int, void*) -> void {
}
auto OivLdmReaderXZ::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void {
}
auto OivLdmReaderXZ::getHistogram(std::vector<int64_t>& numVox)->SbBool {
	return FALSE;
}
auto OivLdmReaderXZ::isDataConverted() const->SbBool {
	return TRUE;
}
auto OivLdmReaderXZ::isRGBA() const->SbBool {
	return FALSE;
}
auto OivLdmReaderXZ::isThreadSafe() const->SbBool {
	return FALSE;
}

void OivLdmReaderXZ::initClass() {
	SO_FIELDCONTAINER_INIT_CLASS(OivLdmReaderXZ, "OivCustomLdmReaderXZ", SoVolumeReader);
}

void OivLdmReaderXZ::exitClass() {
	SO__FIELDCONTAINER_EXIT_CLASS(OivLdmReaderXZ);
}