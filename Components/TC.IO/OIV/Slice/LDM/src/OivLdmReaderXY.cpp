#define LOGGER_TAG "[LDMXY]"
#include <TCLogger.h>
#include <iomanip>
#pragma warning(push)
#pragma warning(disable : 4819)
#include <Inventor/SoInput.h>
#include <LDM/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#pragma warning(pop)

#include "OivLdmReaderXY.h"

#include "HDF5Mutex.h"

SO_FIELDCONTAINER_SOURCE(OivLdmReaderXY);

struct OivLdmReaderXY::Impl {
	//std::shared_ptr<ILdmSliceReader> hdfReader{ nullptr };
	std::shared_ptr<TCFLdmReaderXY> hdfReader{ nullptr };

	int num_tiles{ 0 };
	int num_zeros{ 1 };
	int tileSize[3]{ 0, };
	int sliceNumDivByTileSize{ 1 };
	int _Index{ 0 };
	bool isFL = false;
	int sigBit{ 16 };
};

OivLdmReaderXY::OivLdmReaderXY(bool isFL,bool is8Bit) : d{ new Impl } {
	SO_FIELDCONTAINER_CONSTRUCTOR(OivLdmReaderXY);
	d->hdfReader = std::make_shared<TCFLdmReaderXY>(isFL);
	if(is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
	m_dataConverted = TRUE;
	d->isFL = isFL;
}

OivLdmReaderXY::~OivLdmReaderXY() {

}

auto OivLdmReaderXY::set8Bit(bool is8Bit) -> void {
	d->hdfReader->SetIs8Bit(is8Bit);
	if(is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		d->sigBit = 16;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
}

auto OivLdmReaderXY::setTileName(const std::string& tileName)->void {
	auto header = d->hdfReader->GetTcfHeader();
	header->tileName = tileName;
}

auto OivLdmReaderXY::setDataGroupPath(const std::string& dataGroup)->void {
	auto header = d->hdfReader->GetTcfHeader();
	header->dataGroup = dataGroup;
}

auto OivLdmReaderXY::setTileDimension(int dimX, int dimY, int dimZ)->void {
	d->tileSize[0] = dimX;
	d->tileSize[1] = dimY;
	d->tileSize[2] = dimZ;
}

auto OivLdmReaderXY::getTileString(int _tileId, int _nDigits)->std::string {
	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(_nDigits) << std::setfill('0') << _tileId;

	return tmpStr.str();
}

auto OivLdmReaderXY::setIndex(int index)->bool {
	auto header = d->hdfReader->GetTcfHeader();
	if (index >= header->sizeZ) {
		return false;
	}
	if (index < 0) {
		return false;
	}
	d->_Index = index;
	return true;
}

auto OivLdmReaderXY::getIndex()->int {
	return d->_Index;
}

auto OivLdmReaderXY::getNumZeros(const std::string& filename)->int {
	H5::H5File file(filename, H5F_ACC_RDONLY);
	if(d->isFL) {
	    for(auto i=0;i<3;i++) {			
			auto gName = QString("/Data/3DFL/CH%1").arg(i);
	        if(false == file.exists(gName.toStdString())){
				continue;
	        }
			auto chName = QString("/Data/3DFL/CH%1/000000").arg(i);
			H5::Group group = file.openGroup(chName.toStdString());

			d->num_tiles = group.getNumObjs();
			auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
			std::size_t pos = final_tile_name.find("_");
			std::string numTxt = final_tile_name.substr(pos + 1);
			d->num_zeros = static_cast<int>(numTxt.length());

			group.close();
			break;
	    }
	}
	else {
		if (false == file.exists("/Data/3D/000000")) {
			file.close();
			return -1;
		}
		H5::Group group = file.openGroup("/Data/3D/000000");

		d->num_tiles = group.getNumObjs();
		auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
		std::size_t pos = final_tile_name.find("_");
		std::string numTxt = final_tile_name.substr(pos + 1);
		d->num_zeros = static_cast<int>(numTxt.length());

		group.close();		
	}
	file.close();
	return 0;
}

auto OivLdmReaderXY::setFilename(const SbString& filename) -> int {
	if (nullptr == d->hdfReader) {
		return 0;
	}
	getNumZeros(filename.toStdString());
	auto header = d->hdfReader->GetTcfHeader();
	header->tileSizeX = d->tileSize[0];
	header->tileSizeY = d->tileSize[1];
	header->tileSizeZ = d->tileSize[2];
	int errStatus = d->hdfReader->SetFileName(filename.toStdString(), d->num_zeros);
	if (errStatus < 0) {
		return 0;
	}

	d->sliceNumDivByTileSize = static_cast<int>(ceil(header->sizeZ / header->tileSizeZ) + 1);	

	m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

	return 1;
}
auto OivLdmReaderXY::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm)->SoVolumeReader::ReadError {
	auto header = d->hdfReader->GetTcfHeader();
	SbVec3f _min = SbVec3f((float)(-header->sizeX * header->resolutionX)/2,
		(float)(-header->sizeY * header->resolutionY) / 2, -0.5);
	SbVec3f _max = SbVec3f((float)(header->sizeX * header->resolutionX) / 2,
		(float)(header->sizeY * header->resolutionY) / 2, 0.5);

	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	//idm = SbVec3i32(header->sizeX, header->sizeY, d->sliceNumDivByTileSize);//OIV > 2023.2
	idm = SbVec3i32(header->sizeX, header->sizeY, header->sizeZ);
	
	return RD_NO_ERROR;
}
auto OivLdmReaderXY::getBorderFlag() -> int {
	return 0;
}
auto OivLdmReaderXY::getNumSignificantBits() -> int {
	return d->sigBit;
}
auto OivLdmReaderXY::getTileSize(SbVec3i32& size)->SbBool {
	size = SbVec3i32(d->tileSize[0], d->tileSize[1], 1);	
	return TRUE;
}
auto OivLdmReaderXY::getMinMax(int64_t& min, int64_t& max)->SbBool {
	auto header = d->hdfReader->GetTcfHeader();
	min = header->riMin;
	max = header->riMax;
	return TRUE;
}
auto OivLdmReaderXY::getMinMax(double& min, double& max)->SbBool {
	auto header = d->hdfReader->GetTcfHeader();
	min = header->riMin;
	max = header->riMax;
	return TRUE;
}
auto OivLdmReaderXY::readTile(int index, const SbBox3i32& tilePosition)->SoBufferObject* {	
	auto header = d->hdfReader->GetTcfHeader();
	int maxNuTiles = d->num_tiles;	
	if (index >= maxNuTiles) {
		std::cout << index << " >= " << maxNuTiles << std::endl;
		std::cout << "return nullptr" << std::endl;
		return nullptr;
	}
	std::string tileName = getTileString(index, d->num_zeros);	
	int voxelBytes = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));
	SoCpuBufferObject* tileBuffer = new SoCpuBufferObject;
	size_t bufferSize;
	bufferSize = voxelBytes * d->tileSize[0] * d->tileSize[1] * 1;
	tileBuffer->setSize(bufferSize);
	void* buffer = (void*)tileBuffer->map(SoBufferObject::SET);	
	d->hdfReader->ReadLdmSliceData(tileName, &buffer, d->_Index, tilePosition);	
	tileBuffer->unmap();

	return tileBuffer;
}
auto OivLdmReaderXY::getSubSlice(const SbBox2i32&, int, void*) -> void {
}
auto OivLdmReaderXY::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void {
}
auto OivLdmReaderXY::getHistogram(std::vector<int64_t>& numVox)->SbBool {
	return FALSE;
}
auto OivLdmReaderXY::isDataConverted() const->SbBool {
	return TRUE;
}
auto OivLdmReaderXY::isRGBA() const->SbBool {
	return FALSE;
}
auto OivLdmReaderXY::isThreadSafe() const->SbBool {
	return FALSE;
}

void OivLdmReaderXY::initClass() {
	SO_FIELDCONTAINER_INIT_CLASS(OivLdmReaderXY, "OivCustomLdmReaderXY", SoVolumeReader);
}

void OivLdmReaderXY::exitClass() {
	SO__FIELDCONTAINER_EXIT_CLASS(OivLdmReaderXY);
}