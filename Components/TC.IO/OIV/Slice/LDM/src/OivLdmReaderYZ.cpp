#define LOGGER_TAG "[LDMYZ]"
#include <TCLogger.h>
#include <iomanip>
#pragma warning(push)
#pragma warning(disable : 4819)
#include <Inventor/SoInput.h>
#include <LDM/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#pragma warning(pop)

#include "OivLdmReaderYZ.h"

SO_FIELDCONTAINER_SOURCE(OivLdmReaderYZ);

struct OivLdmReaderYZ::Impl {
	//std::shared_ptr<ILdmSliceReader> hdfReader{nullptr};
	std::shared_ptr<TCFLdmReaderYZ> hdfReader{ nullptr };

	int num_tiles{ 0 };
	int num_zeros{ 1 };
	int tileSize[3]{ 0, };
	int _Index{ 0 };
	bool isFL{ false };	
	int sigBit{ 16 };
};

OivLdmReaderYZ::OivLdmReaderYZ(bool isFL,bool is8Bit) : d{ new Impl } {
	SO_FIELDCONTAINER_CONSTRUCTOR(OivLdmReaderYZ);
	d->hdfReader = std::make_shared<TCFLdmReaderYZ>(isFL);
	if(is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
	m_dataConverted = TRUE;
	d->isFL = isFL;
}

OivLdmReaderYZ::~OivLdmReaderYZ() {

}

auto OivLdmReaderYZ::set8Bit(bool is8Bit) -> void {
	d->hdfReader->SetIs8Bit(is8Bit);
	if(is8Bit) {
		d->sigBit = 8;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_BYTE);
	}else {
		d->sigBit = 16;
		m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::UNSIGNED_SHORT);
	}
}

auto OivLdmReaderYZ::setTileName(const std::string& tileName)->void {
	auto header = d->hdfReader->GetTcfHeader();
	header->tileName = tileName;
}

auto OivLdmReaderYZ::setDataGroupPath(const std::string& dataGroup)->void {
	auto header = d->hdfReader->GetTcfHeader();
	header->dataGroup = dataGroup;
}

auto OivLdmReaderYZ::setTileDimension(int dimX, int dimY, int dimZ)->void {
	d->tileSize[0] = dimX;
	d->tileSize[1] = dimY;
	d->tileSize[2] = dimZ;
}

auto OivLdmReaderYZ::getTileString(int _tileId, int _nDigits)->std::string {
	std::stringstream tmpStr;
	tmpStr << "TILE_" << std::setw(_nDigits) << std::setfill('0') << _tileId;

	return tmpStr.str();
}

auto OivLdmReaderYZ::setIndex(int index)->bool {
	auto header = d->hdfReader->GetTcfHeader();
	if (index >= header->sizeX) {
		return false;
	}
	if (index < 0) {
		return false;
	}
	d->_Index = index;
	return true;
}

auto OivLdmReaderYZ::getIndex()->int {
	return d->_Index;
}

auto OivLdmReaderYZ::getNumZeros(const std::string& filename)->int {
	H5::H5File file(filename, H5F_ACC_RDONLY);
	if (d->isFL) {
		for (auto i = 0; i < 3; i++) {
			auto gName = QString("/Data/3DFL/CH%1").arg(i);
			if (false == file.exists(gName.toStdString())) {
				continue;
			}
			auto chName = QString("/Data/3DFL/CH%1/000000").arg(i);
			H5::Group group = file.openGroup(chName.toStdString());

			d->num_tiles = group.getNumObjs();
			auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
			std::size_t pos = final_tile_name.find("_");
			std::string numTxt = final_tile_name.substr(pos + 1);
			d->num_zeros = static_cast<int>(numTxt.length());

			group.close();
			break;
		}
	}
	else {
		if (false == file.exists("/Data/3D/000000")) {
			file.close();
			return -1;
		}
		H5::Group group = file.openGroup("/Data/3D/000000");

		d->num_tiles = group.getNumObjs();
		auto final_tile_name = group.getObjnameByIdx(d->num_tiles - 1);
		std::size_t pos = final_tile_name.find("_");
		std::string numTxt = final_tile_name.substr(pos + 1);
		d->num_zeros = static_cast<int>(numTxt.length());

		group.close();
	}
	file.close();
	return 0;
}

auto OivLdmReaderYZ::setFilename(const SbString& filename) -> int {
	if (nullptr == d->hdfReader) {
		return 0;
	}
	getNumZeros(filename.toStdString());
	auto header = d->hdfReader->GetTcfHeader();
	header->filePath = filename.toStdString();
	header->tileSizeX = d->tileSize[0];
	header->tileSizeY = d->tileSize[1];
	header->tileSizeZ = d->tileSize[2];
	int errStatus = d->hdfReader->SetFileName(filename.toStdString(), d->num_zeros);
	if (errStatus < 0) {
		return 0;
	}

	m_bytesPerVoxel = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

	return 1;
}
auto OivLdmReaderYZ::getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm)->SoVolumeReader::ReadError {
	auto header = d->hdfReader->GetTcfHeader();
	SbVec3f _min = SbVec3f((float)(-header->sizeX * header->resolutionX) / 2,(float)(-header->sizeY * header->resolutionY) / 2, (float)(-header->sizeZ * header->resolutionZ) / 2);
	SbVec3f _max = SbVec3f((float)(header->sizeX * header->resolutionX) / 2, (float)(header->sizeY * header->resolutionY) / 2, (float)(header->sizeZ * header->resolutionZ) / 2);

	size = SbBox3f(_min, _max);
	type = SoDataSet::DataType(header->dataType);
	//idm = SbVec3i32(header->sizeX,header->sizeY,header->sizeZ);
	idm = SbVec3i32(1,header->sizeY, header->sizeZ);

	return RD_NO_ERROR;
}
auto OivLdmReaderYZ::getBorderFlag() -> int {
	return 0;
}
auto OivLdmReaderYZ::getNumSignificantBits() -> int {
	return d->sigBit;
}
auto OivLdmReaderYZ::getTileSize(SbVec3i32& size)->SbBool {
	//size = SbVec3i32(d->tileSize[0],d->tileSize[1], d->tileSize[2]);
	size = SbVec3i32(1,d->tileSize[1], d->tileSize[2]);
	return TRUE;
}
auto OivLdmReaderYZ::getMinMax(int64_t& min, int64_t& max)->SbBool {
	auto header = d->hdfReader->GetTcfHeader();
	min = header->riMin;
	max = header->riMax;
	return TRUE;
}
auto OivLdmReaderYZ::getMinMax(double& min, double& max)->SbBool {
	auto header = d->hdfReader->GetTcfHeader();
	min = header->riMin;
	max = header->riMax;
	return TRUE;
}
auto OivLdmReaderYZ::readTile(int index, const SbBox3i32& tilePosition)->SoBufferObject* {
	auto header = d->hdfReader->GetTcfHeader();
	int maxNuTiles = d->num_tiles;
	if (index >= maxNuTiles) {
		return nullptr;
	}
	std::string tileName = getTileString(index, d->num_zeros);
	
	int voxelBytes = SoDataSet::dataSize(SoDataSet::DataType(header->dataType));

	SoCpuBufferObject* tileBuffer = new SoCpuBufferObject;
	size_t bufferSize;
	bufferSize = voxelBytes * d->tileSize[1] * d->tileSize[2] * d->tileSize[0];
	//bufferSize = voxelBytes * d->tileSize[1] * d->tileSize[2] * 1;
	tileBuffer->setSize(bufferSize);
	void* buffer = (void*)tileBuffer->map(SoBufferObject::SET);
	d->hdfReader->ReadLdmSliceData(tileName, &buffer, d->_Index, tilePosition);

	tileBuffer->unmap();

	return tileBuffer;
}
auto OivLdmReaderYZ::getSubSlice(const SbBox2i32&, int, void*) -> void {
}
auto OivLdmReaderYZ::getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void {
}
auto OivLdmReaderYZ::getHistogram(std::vector<int64_t>& numVox)->SbBool {
	return FALSE;
}
auto OivLdmReaderYZ::isDataConverted() const->SbBool {
	return TRUE;
}
auto OivLdmReaderYZ::isRGBA() const->SbBool {
	return FALSE;
}
auto OivLdmReaderYZ::isThreadSafe() const->SbBool {
	return FALSE;
}

void OivLdmReaderYZ::initClass() {
	SO_FIELDCONTAINER_INIT_CLASS(OivLdmReaderYZ, "OivCustomLdmReaderYZ", SoVolumeReader);
}

void OivLdmReaderYZ::exitClass() {
	SO__FIELDCONTAINER_EXIT_CLASS(OivLdmReaderYZ);
}