#pragma once

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#include <LDM/readers/SoLDMReader.h>
#include <LDM/readers/SoVRLdmFileReader.h>
#include <Inventor/fields/SoSFInt32.h>
#pragma warning(pop)

#include "LDMSliceClasses.h"
#include "LDMSliceClasses8Bit.h"

#include "TC.IO.OIV.Slice.LDMExport.h"

class TC_IO_OIV_Slice_LDM_API  OivLdmReaderYZ : public SoVolumeReader {
	SO_FIELDCONTAINER_HEADER(OivLdmReaderYZ);
public:
	OivLdmReaderYZ(bool isFL = false,bool is8Bit=false);
	~OivLdmReaderYZ();

	auto setTileDimension(int dimX, int dimY, int dimZ)->void;
	auto setTileName(const std::string& tileName)->void;
	auto setDataGroupPath(const std::string& dataGroup)->void;
	auto setIndex(int idx)->bool;
	auto getIndex()->int;
	auto set8Bit(bool is8Bit)->void;

	virtual auto setFilename(const SbString& filename) -> int override;
	virtual auto getDataChar(SbBox3f& size, SoVolumeData::DataType& type, SbVec3i32& idm)->SoVolumeReader::ReadError override;
	virtual auto getBorderFlag() -> int override;
	virtual auto getNumSignificantBits() -> int override;
	virtual auto getTileSize(SbVec3i32& size)->SbBool override;
	virtual auto getMinMax(int64_t& min, int64_t& max)->SbBool override;
	virtual auto getMinMax(double& min, double& max)->SbBool override;
	virtual auto readTile(int index, const SbBox3i32& tilePosition)->SoBufferObject* override;
	virtual auto getSubSlice(const SbBox2i32&, int, void*) -> void override;
	virtual auto getSubSlice(const SbBox2i32& subSlice, int sliceNumber, SoBufferObject* bufferObject) -> void override;
	virtual auto getHistogram(std::vector<int64_t>& numVox)->SbBool override;
	virtual auto isDataConverted() const->SbBool override;
	virtual auto isRGBA() const->SbBool override;
	virtual auto isThreadSafe() const->SbBool override;

private:
	auto getNumZeros(const std::string& filename)->int;
	auto getTileString(int _tileId, int _nDigits)->std::string;

	struct Impl;
	std::unique_ptr<Impl> d;
};