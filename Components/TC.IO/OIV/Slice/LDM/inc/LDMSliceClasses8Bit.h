#pragma once

#include "TC.IO.OIV.Slice.LDMExport.h"

#include <QMutexLocker>
#include <Inventor/SbBox.h>
#pragma warning(push)
#pragma warning(disable:4268)
#include <hdf5.h>
#include <H5Cpp.h>
#pragma warning(pop)

#include <memory>

#include "ILdmSliceReader.h"

class TC_IO_OIV_Slice_LDM_API TCFLdmReaderXY8Bit : public ILdmSliceReader {
public:
	TCFLdmReaderXY8Bit(bool isFL = false);
	~TCFLdmReaderXY8Bit();

	auto GetTcfHeader()->LdmHeaderSlice* override;
	auto SetFileName(const std::string& name, int numZero)->int override;
	auto ReadLdmSliceData(const std::string _tileName, void** _tileData, int zIndex, const SbBox3i32& tilePosition)->int override;

protected:
	auto GetLdmGroup()->int;
private:
	auto OpenHdfFile(const std::string& name, int numZero)->int;
	auto ReadDataSet(std::string _tile_name, void** _data, int idx, const SbBox3i32& tilePosition)->int;
	struct Impl;
	std::unique_ptr<Impl> d;
};

class TC_IO_OIV_Slice_LDM_API TCFLdmReaderYZ8Bit : public ILdmSliceReader {
public:
	TCFLdmReaderYZ8Bit(bool isFL = false);
	~TCFLdmReaderYZ8Bit();

	auto GetTcfHeader()->LdmHeaderSlice* override;
	auto SetFileName(const std::string& name, int numZero)->int override;
	auto ReadLdmSliceData(const std::string _tileName, void** _tileData, int xIndex, const SbBox3i32& tilePosition)->int override;

protected:
	auto GetLdmGroup()->int;
private:
	auto OpenHdfFile(const std::string& name, int numZero)->int;
	auto ReadDataSet(std::string _tile_name, void** _data, int idx, const SbBox3i32& tilePosition)->int;
	struct Impl;
	std::unique_ptr<Impl> d;
};

class TC_IO_OIV_Slice_LDM_API TCFLdmReaderXZ8Bit : public ILdmSliceReader {
public:
	TCFLdmReaderXZ8Bit(bool isFL = false);
	~TCFLdmReaderXZ8Bit();

	auto GetTcfHeader()->LdmHeaderSlice* override;
	auto SetFileName(const std::string& name, int numZero)->int override;
	auto ReadLdmSliceData(const std::string _tileName, void** _tileData, int xIndex, const SbBox3i32& tilePosition)->int override;
protected:
	auto GetLdmGroup()->int;
private:
	auto OpenHdfFile(const std::string& name, int numZero)->int;
	auto ReadDataSet(std::string _tile_name, void** _data, int idx, const SbBox3i32& tilePosition)->int;
	struct Impl;
	std::unique_ptr<Impl> d;
};