#pragma once

#include "TC.IO.OIV.Slice.LDMExport.h"

#include <QMutexLocker>
#include <Inventor/SbBox.h>
#pragma warning(push)
#pragma warning(disable:4268)
#include <hdf5.h>
#include <H5Cpp.h>
#pragma warning(pop)

#include <memory>

typedef struct HeaderOfLDMSlice {
	int dataCount = -1;
	int maxTileSize = -1;
	double		resolutionZ = -1.0;
	double		resolutionX = -1.0;
	double		resolutionY = -1.0;
	double      offsetZ = -1.0;
	int			sizeZ = -1;
	int			sizeX = -1;
	int			sizeY = -1;
	int         tileSizeX = -1;
	int         tileSizeY = -1;
	int         tileSizeZ = -1;
	float		timeInterval = -1.f;
	float		maxIntensity = -1.f;
	float		minIntensity = -1.f;
	float		riMin = -1.f;
	float		riMax = -1.f;
	float		positionC = -1.f;
	float		positionX = -1.f;
	float		positionY = -1.f;
	float		positionZ = -1.f;
	int			dataType = -1;
	std::string dataGroup;
	std::string tileName;
	std::string filePath;
}LdmHeaderSlice;

class TC_IO_OIV_Slice_LDM_API ILdmSliceReader {
public:
	ILdmSliceReader();
	~ILdmSliceReader();

	virtual auto GetTcfHeader()->LdmHeaderSlice* = 0;
	virtual auto SetFileName(const std::string& name, int numZero)->int = 0;
	virtual auto ReadLdmSliceData(const std::string _tileName, void** _tileData, int zIndex, const SbBox3i32& tilePosition)->int = 0;
};