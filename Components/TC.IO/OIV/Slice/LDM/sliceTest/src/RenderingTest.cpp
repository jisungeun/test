#pragma warning(disable:4268)

#include <iostream>

#include <QFileDialog>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QMainWindow>

//Open Inventor
#include <Inventor/SoDB.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <Inventor/Qt/viewers/SoQtPlaneViewer.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Medical/helpers/MedicalHelper.h>
//
#include <OivXYReader.h>
#include <OivLdmReaderXY.h>
#include <OivHdf5Reader.h>
#include <OivLdmReader.h>

#include "RenderWindow.h"
#include "TestWindow.h"

#define IS_VOLUME 1

int main(int argc, char* argv[]) {
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);    
    QApplication app(argc, argv);
    /*auto path = QFileDialog::getOpenFileName(nullptr, "Open TCF", "F:/CellLibrary/Test data_Basic Analyzer/RAW 264.7 lps treatment 24 h/20200214.164035.756.RAW LPS-24h-017", "TCF (*.tcf)");

    if (path.isEmpty()) {
        return EXIT_FAILURE;
    }
    auto widget = new QWidget;
    SoQt::init(widget);
    SoVolumeRendering::init();

    H5::H5File file(path.toStdString(), H5F_ACC_RDONLY);
    auto group = file.openGroup("/Data/3D");
    auto dataSet = group.openDataSet("000000");

    int64_t sizeX, sizeY, sizeZ;
    auto attrtX = group.openAttribute("SizeX");
    attrtX.read(H5::PredType::NATIVE_INT64, &sizeX);
    attrtX.close();
    auto attrY = group.openAttribute("SizeY");
    attrY.read(H5::PredType::NATIVE_INT64, &sizeY);
    attrY.close();
    auto attrZ = group.openAttribute("SizeZ");
    attrZ.read(H5::PredType::NATIVE_INT64, &sizeZ);
    attrZ.close();

    std::cout << QString("Original size: %1,%2,%3").arg(sizeX).arg(sizeY).arg(sizeZ).toStdString() << std::endl;
    const auto numberOfElements = sizeX * sizeY * sizeZ;
    std::shared_ptr<uint16_t[]> originalArray{ new uint16_t[numberOfElements]() };
    dataSet.read(originalArray.get(), H5::PredType::NATIVE_UINT16);
    dataSet.close();
    group.close();
    file.close();

    SoRef<SoSeparator> root = new SoSeparator;
    SoRef<SoDataRange> range = new SoDataRange;
    SoRef<SoTransferFunction> tf = new SoTransferFunction;
    SoRef<SoVolumeData> volData = new SoVolumeData;
    volData->data.setValue(SbVec3i32(sizeX, sizeY, sizeZ), SbDataType::UNSIGNED_SHORT, 16, originalArray.get(), SoSFArray::COPY);
    MedicalHelper::dicomAdjustDataRange(range.ptr(), volData.ptr());
    tf->predefColorMap = SoTransferFunction::INTENSITY;

    root->addChild(volData.ptr());
    root->addChild(tf.ptr());
    root->addChild(range.ptr());

    SoRef<SoVolumeRender> render = new SoVolumeRender;
    root->addChild(render.ptr());

    auto ex = new SoQtExaminerViewer(widget);
    ex->setSceneGraph(root.ptr());
    ex->show();

    SoQt::show(widget);
    SoQt::mainLoop();

    SoVolumeRendering::finish();
    SoQt::finish();
    return 0;*/
    TestWindow* window = new TestWindow;
    
    SoQt::init(window);
    SoVolumeRendering::init();
    OivHdf5Reader::initClass();
    OivLdmReader::initClass();
    OivXYReader::initClass();
    OivLdmReaderXY::initClass();

    window->Construction();

    //SoQtPlaneViewer* viewer = new SoQtPlaneViewer(window);
    //window->SetViewer(viewer);
    RenderWindow* viewer = new RenderWindow(nullptr);
    window->SetViewer(viewer);
   
    window->show();
    app.exec();
            
    delete window;    

    OivLdmReaderXY::exitClass();
    OivXYReader::exitClass();
    OivLdmReader::exitClass();
    OivHdf5Reader::exitClass();
    SoVolumeRendering::finish();
    SoQt::finish();
    return 0;
}
