#pragma once

#include <QOivRenderWindow.h>

class SoEventCallback;
class SoVolumeDAta;
class SoMouseButtonEvent;

class RenderWindow : public QOivRenderWindow {
    Q_OBJECT
public:
    RenderWindow(QWidget* parent);
    ~RenderWindow();

    auto reset2DView()->void;    

signals:
    void sigZoomFactor(float);
    void sigResolution(int);
private:
    void resizeEvent(QResizeEvent* event) override;

    auto MouseButtonEvent(SoEventCallback* node) -> void override;
    auto MouseMoveEvent(SoEventCallback* node) -> void override;
    auto KeyboardEvent(SoEventCallback* node) -> void override;
    auto MouseWheelEvent(SoEventCallback* node) -> void override;

    auto setDefaultWindowType() -> void override;

    auto calcPixelPerRaster()->void;

    struct Impl;
    std::unique_ptr<Impl> d;     
};