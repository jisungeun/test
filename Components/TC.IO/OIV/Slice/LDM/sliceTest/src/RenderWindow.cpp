
#include "RenderWindow.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/draggers/SoScale2Dragger.h>
#include <Inventor/draggers/SoScale2UniformDragger.h>
#include <Inventor/draggers/SoTranslate2Dragger.h>
#include <Inventor/engines/SoCompose.h>
#include <Inventor/engines/SoCalculator.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMultiSwitch.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoRotation.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Medical/nodes/TextBox.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#pragma warning(pop)

struct RenderWindow::Impl {
    bool isFirstResize{ false };
    bool right_mouse_pressed{ false };
    bool middle_mouse_pressed{ false };
    bool left_mouse_pressed{ false };
    double right_stamp{ 0.0 };
    double prev_scale{ 1.0 };
    float zoomBase{ 1 };
    float zoomFactor{ 1 };
    int sizeX{ 1 };
    int sizeY{1};
};

RenderWindow::RenderWindow(QWidget* parent) :d{ new Impl } , QOivRenderWindow(parent,true) {
    setDefaultWindowType();
}

RenderWindow::~RenderWindow() {
    
}

auto RenderWindow::MouseMoveEvent(SoEventCallback* node) -> void {
    const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
    if (isNavigation()) {
        if (d->right_mouse_pressed) {
            auto diff = moveEvent->getNormalizedPosition(getViewportRegion())[1] - d->right_stamp;
            auto factor = 1.0 - diff * 1.5;
            if (factor > 0) {
                getCamera()->scaleHeight(factor / d->prev_scale);                                
                d->prev_scale = factor;
                d->zoomFactor = d->zoomBase / getCameraZoom();
                calcPixelPerRaster();
                emit sigZoomFactor(d->zoomFactor);
            }
        }
        if (d->left_mouse_pressed) {
            node->setHandled();
        }
        if (d->middle_mouse_pressed) {
            panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));            
        }
    }
}

auto RenderWindow::calcPixelPerRaster() -> void {    
    auto pixelX = static_cast<float>(d->sizeX) / d->zoomFactor;
    auto pixelY = static_cast<float>(d->sizeY) / d->zoomFactor;
    auto windowX = width();
    auto windowY = height();

    auto resolution = std::max(pixelX / static_cast<float>(windowX), pixelY / static_cast<float>(windowY));

    int intRes = static_cast<int>(resolution);

    emit sigResolution(intRes);
}

auto RenderWindow::MouseButtonEvent(SoEventCallback* node) -> void {
    const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();
    if(isNavigation()) {
        auto vr = getViewportRegion();
        auto normpos = mouseButton->getNormalizedPosition(vr);
        if (SoMouseButtonEvent::isButtonDoubleClickEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
            reset2DView();
        }
        if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
            d->right_mouse_pressed = true;
            d->right_stamp = normpos[1];
            d->prev_scale = 1.0;
        }
        if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
            d->right_mouse_pressed = false;
            d->right_stamp = 0.0;
            d->prev_scale = 1.0;
        }
        if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
            d->middle_mouse_pressed = true;
            startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
        }
        if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
            d->middle_mouse_pressed = false;
        }
        if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
            d->left_mouse_pressed = true;            
        }
        if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
            d->left_mouse_pressed = false;
        }
    }
}

auto RenderWindow::MouseWheelEvent(SoEventCallback* node) -> void {
    Q_UNUSED(node)
}

auto RenderWindow::KeyboardEvent(SoEventCallback* node) -> void {
    Q_UNUSED(node)
}

auto RenderWindow::setDefaultWindowType() -> void {
    SbVec3f start_color = { 0.0,0.0,0.0 };
    SbVec3f end_color = { 0.0,0.0,0.0 };
    setGradientBackground(start_color, end_color);

    //set default camera type
    setCameraType(true);//orthographic
}

void RenderWindow::resizeEvent(QResizeEvent* event) {
    if(d->isFirstResize) {
        reset2DView();
        d->isFirstResize = false;
    }    
    QWidget::resizeEvent(event);
    calcPixelPerRaster();
}

auto RenderWindow::reset2DView()->void {
    MedicalHelper::Axis ax;
    auto root = getSceneGraph();
    auto volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
    if (volData) {
        float slack;
        auto dims = volData->getDimension();
        auto spacing = volData->getVoxelSize();

        float x_len = dims[0] * spacing[0];
        float y_len = dims[1] * spacing[1];

        auto wh = static_cast<float>(size().height());
        auto ww = static_cast<float>(size().width());
        auto windowSlack = wh > ww ? ww / wh : wh / ww;
        if (wh < 50) {
            d->isFirstResize = true;
        }        
        ax = MedicalHelper::Axis::AXIAL;
        slack = y_len < x_len ? y_len / x_len : x_len / y_len;        
        
        if (windowSlack > slack && windowSlack > 0) {
            slack = windowSlack > 1 ? 1.0 : windowSlack;
        }
        MedicalHelper::orientView(ax, getCamera(), volData, slack);
        d->sizeX = dims[0];
        d->sizeY = dims[1];
        d->zoomBase = getCameraZoom();
        d->zoomFactor = 1;
        calcPixelPerRaster();
    }
}