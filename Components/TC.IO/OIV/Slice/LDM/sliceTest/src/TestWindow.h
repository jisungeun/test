#pragma once

#include <memory>
#include <QWidget>

#include "RenderWindow.h"

class ARWidget : public QWidget {
    Q_OBJECT
public:
    ARWidget(QWidget* parent = nullptr);
    ~ARWidget();
    auto SetViewer(RenderWindow* viewer)->void;
    void resizeEvent(QResizeEvent* event) override;
    
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};

class TestWindow : public QWidget {
    Q_OBJECT
public:
    TestWindow(QWidget* parent = nullptr);
    ~TestWindow();

    auto SetViewer(RenderWindow* viewer)->void;
    auto Construction()->void;
    
protected slots:
    void OnSpinBoxChanged(double dval);
    void OnFileOpen();
    void OnFixedResolution();
    void OnResolutionValue(int);
    void OnMaxResolutionValue(int);
    void OnMinTilesToLoad(int);

private:    

    struct Impl;
    std::unique_ptr<Impl> d;
};