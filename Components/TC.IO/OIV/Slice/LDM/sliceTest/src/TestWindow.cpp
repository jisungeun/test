#pragma warning(disable:4268)

#include <QFileDialog>
#include <QResizeEvent>
#include <H5Cpp.h>

//Inventor
#include <Inventor/SoDB.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <Inventor/Qt/viewers/SoQtPlaneViewer.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Medical/helpers/MedicalHelper.h>
#include <OivXYReader.h>
#include <OivLdmReaderXY.h>
#include <OivHdf5Reader.h>
#include <OivLdmReader.h>

#include "ui_TestWindow.h"
#include "TestWindow.h"

struct ARWidget::Impl {
    QBoxLayout* layout;
    float arWidth {500}; // aspect ratio width
    float arHeight {500}; // aspect ratio height
};

ARWidget::ARWidget(QWidget* parent) : QWidget(parent), d{ new Impl } {
    d->layout = new QBoxLayout(QBoxLayout::LeftToRight, this);
}

ARWidget::~ARWidget() {
    
}

void ARWidget::resizeEvent(QResizeEvent* event) {
    float thisAspectRatio = (float)event->size().width() / event->size().height();
    int widgetStretch, outerStretch;

    if (thisAspectRatio > (d->arWidth / d->arHeight)) // too wide
    {
        d->layout->setDirection(QBoxLayout::LeftToRight);
        widgetStretch = height() * (d->arWidth / d->arHeight); // i.e., my width
        outerStretch = (width() - widgetStretch) / 2 + 0.5;
    }
    else // too tall
    {
        d->layout->setDirection(QBoxLayout::TopToBottom);
        widgetStretch = width() * (d->arHeight / d->arWidth); // i.e., my height
        outerStretch = (height() - widgetStretch) / 2 + 0.5;
    }

    d->layout->setStretch(0, outerStretch);
    d->layout->setStretch(1, widgetStretch);
    d->layout->setStretch(2, outerStretch);
}

auto ARWidget::SetViewer(RenderWindow* viewer) -> void {
    d->layout->addItem(new QSpacerItem(0, 0));
    d->layout->addWidget(viewer);
    d->layout->addItem(new QSpacerItem(0, 0));
}

struct TestWindow::Impl {
    Ui::Window ui;
    SoRef<SoSeparator> root{ nullptr };
    SoRef<SoMaterial> matl{ nullptr };
    SoRef<SoDataRange> range{ nullptr };
    SoRef<SoTransferFunction> tf{ nullptr };
    SoRef<SoVolumeData> volData{ nullptr };
    SoRef<SoOrthoSlice> slice{ nullptr };

    SoRef<OivXYReader> reader{ nullptr };
    SoRef<OivXYReader> reader8bit{ nullptr };
    //SoRef<OivLdmReaderXY> ldmReader{ nullptr };
    SoRef<OivLdmReader> ldmReader{ nullptr };

    int fileFormat{ 0 };
    QString filePath;    

    auto InitUI()->void;
    auto BuildSceneGraph()->void;
    auto OpenTCF(const QString& path)->void;
    auto ResetParameter()->void;
    auto ResetUI()->void;
        
    TestWindow* thisPointer{ nullptr };
    RenderWindow* viewer{ nullptr };
    ARWidget* socket{ nullptr };

    int maxRes{ 3 };
    double singleStep{ 0.95 };
};

TestWindow::TestWindow(QWidget* parent) : QWidget(parent), d{ new Impl } {
    d->thisPointer = this;    

    d->InitUI();
    setWindowTitle("TCF Format Test - Slice Reader");
}

TestWindow::~TestWindow() {
    
}

auto TestWindow::SetViewer(RenderWindow* viewer) -> void {
    d->socket->SetViewer(viewer);
    d->viewer = viewer;
    viewer->setSceneGraph(d->root.ptr());
    viewer->toggleFPS(true);

    connect(d->viewer, SIGNAL(sigResolution(int)), this, SLOT(OnMaxResolutionValue(int)));
}

auto TestWindow::Impl::InitUI() -> void {
    ui.setupUi(thisPointer);
    ui.fileFormat->setReadOnly(true);
    ui.filePath->setReadOnly(true);
    ui.sliceSpin->setRange(0, 62.5);
    ui.sliceSpin->setValue(20.83);
    ui.sliceSpin->setDecimals(2);
    ui.sliceSpin->setSingleStep(0.95);

    ui.fixChk->setChecked(false);
    ui.resSpin->setRange(0, 3);
    ui.resSpin->setValue(2);

    ui.meanTileSpin->setRange(1, 300);
    ui.meanTileSpin->setValue(1);

    ui.maxResSpin->setRange(0, 3);
    ui.maxResSpin->setValue(2);

    connect(ui.sliceSpin, SIGNAL(valueChanged(double)), thisPointer, SLOT(OnSpinBoxChanged(double)));
    connect(ui.pushButton, SIGNAL(clicked()), thisPointer, SLOT(OnFileOpen()));
    connect(ui.fixChk, SIGNAL(clicked()), thisPointer, SLOT(OnFixedResolution()));
    connect(ui.resSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnResolutionValue(int)));
    connect(ui.meanTileSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnMinTilesToLoad(int)));
    connect(ui.maxResSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnMaxResolutionValue(int)));

    ui.maxResSpin->setReadOnly(true);
    
    auto layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    ui.renderSocket->setLayout(layout);

    socket = new ARWidget(nullptr);
    layout->addWidget(socket);
}

auto TestWindow::Construction() -> void {
    d->BuildSceneGraph();
}

auto TestWindow::Impl::BuildSceneGraph() -> void {
    root = new SoSeparator;
    matl = new SoMaterial;
    range = new SoDataRange;
    tf = new SoTransferFunction;
    volData = new SoVolumeData;    
    slice = new SoOrthoSlice;

    matl->diffuseColor.setValue(1, 1, 1);
    matl->ambientColor.setValue(1, 1, 1);

    tf->predefColorMap = SoTransferFunction::INTENSITY;

    reader = new OivXYReader;
    reader->setDataGroupPath("/Data/3D");
    reader->setTileName("000000");
    reader8bit = new OivXYReader(false,true);
    reader8bit->setDataGroupPath("/Data/3D");
    reader8bit->setTileName("000000");
    //ldmReader = new OivLdmReaderXY(false, true);
    ldmReader = new OivLdmReader(false, true);
    ldmReader->setDataGroupPath("/Data/3D");
    ldmReader->setTileDimension(128, 128, 128);
    ldmReader->setTileName("000000");


    root->addChild(matl.ptr());
    root->addChild(range.ptr());
    root->addChild(tf.ptr());
    root->addChild(volData.ptr());
    root->addChild(slice.ptr());
}

auto TestWindow::Impl::ResetParameter() -> void {    
    volData->ldmResourceParameters.getValue()->fixedResolution = FALSE;
    volData->ldmResourceParameters.getValue()->resolution = 0;
    volData->ldmResourceParameters.getValue()->maxResolutionThreshold = 0;
    volData->ldmResourceParameters.getValue()->minTilesToLoad = 1;
}

auto TestWindow::Impl::ResetUI() -> void {
    thisPointer->blockSignals(true);

    ui.fixChk->setChecked(false);

    //ui.sliceSpin->setValue(33);
    ui.sliceSpin->setValue(20.83);

    ui.resSpin->setValue(2);

    ui.meanTileSpin->setValue(1);
        
    ui.maxResSpin->setValue(2);
    thisPointer->blockSignals(false);
}

auto TestWindow::Impl::OpenTCF(const QString& path) -> void {
    ResetParameter();
    ResetUI();
    filePath = path;
    slice->sliceNumber = 0;
    switch(fileFormat) {
        case 0:
        case 1:
            reader8bit->setFilename(filePath.toStdString());
            reader8bit->setIndex(32);
            volData->setReader(*reader8bit);        
            //range->min = 10;
            //range->max = 35;
            range->min = 20;
            range->max = 80;
            break;
        case 2:
        case 3:
            reader->setFilename(filePath.toStdString());
            reader->setIndex(32);
            volData->setReader(*reader);
            range->min = 13300;
            range->max = 13600;
            break;
        default://4,5
            ldmReader->setFilename(filePath.toStdString());
            //ldmReader->setIndex(32);
            slice->sliceNumber = 32;
            volData->setReader(*ldmReader);
            //range->min = 10;
            //range->max = 35;            
            range->min = 20;
            range->max = 80;
            break;
    }
    volData->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
    auto dimX = volData->getDimension()[0];
    maxRes = 0;
    while (dimX > 64) {
        dimX /= 2;
        maxRes++;
    }
    ui.maxResSpin->setRange(0, maxRes);
    ui.resSpin->setRange(0, maxRes);

    viewer->reset2DView();
}

void TestWindow::OnFileOpen() {
    const auto path = QFileDialog::getOpenFileName(this, "Open TCF", d->filePath, "TCF (*.tcf)");

    if(path.isEmpty()) {
        d->ui.filePath->setText("");
        d->ui.fileFormat->setText("");
        return;
    }
    H5::H5File file(path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
    auto group = file.openGroup("Data");

    if (false == group.attrExists("StructureType")) {
        d->ui.filePath->setText("Selected TCF is not a test data");
        d->ui.fileFormat->setText("");
        return;
    }

    auto attr = group.openAttribute("StructureType");
    int64_t dataType;
    attr.read(H5::PredType::NATIVE_INT64, &dataType);
    attr.close();
    group.close();
    file.close();
    
    d->ui.filePath->setText(path);        
    dataType = 5;
    switch(dataType) {
        case 0:
            d->ui.fileFormat->setText("uint8");
            break;
        case 1:
            d->ui.fileFormat->setText("uint8 compressed");
            break;
        case 2:
            d->ui.fileFormat->setText("uint16");
            break;
        case 3:
            d->ui.fileFormat->setText("uint16 compressed");
            break;
        case 4:
            d->ui.fileFormat->setText("uint8 LDM");
            break;
        case 5:
            d->ui.fileFormat->setText("uint8 LDM compressed");
            break;
        default:
            break;
    }
    d->fileFormat = dataType;
    d->OpenTCF(path);
}

void TestWindow::OnSpinBoxChanged(double dval) {
    int val = static_cast<int>(dval / 0.9469);
    if(val < 1) {
        val = 1;
    }
    if(val >66) {
        val = 66;
    }
    if(d->fileFormat == 0 || d->fileFormat == 1) {
        d->reader8bit->setIndex(val-1);
        d->reader8bit->touch();
    }
    else if(d->fileFormat == 2 || d->fileFormat == 3) {
        d->reader->setIndex(val-1);
        d->reader->touch();
    }else {
        //d->ldmReader->setIndex(val-1);
        //d->ldmReader->touch();
        d->slice->sliceNumber = val - 1;
    }
}

void TestWindow::OnMaxResolutionValue(int val) {
    auto realVal = val > d->maxRes ? d->maxRes : val;    
    realVal /= 2;        

    d->singleStep = 0.95 * pow(2,realVal);
    d->ui.sliceSpin->setSingleStep(d->singleStep);

    d->ui.maxResSpin->blockSignals(true);
    d->ui.maxResSpin->setValue(realVal);
    d->ui.maxResSpin->blockSignals(false);
    d->ui.resSpin->blockSignals(true);
    d->ui.resSpin->setValue(realVal);
    d->ui.resSpin->blockSignals(false);
    d->volData->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::ScreenResolutionCullingPolicy::SCREEN_RESOLUTION_CULLING_ON;    
    d->volData->ldmResourceParameters.getValue()->resolution = realVal;
    d->volData->ldmResourceParameters.getValue()->maxResolutionThreshold = realVal;
}

void TestWindow::OnFixedResolution() {    
    d->volData->ldmResourceParameters.getValue()->fixedResolution = d->ui.fixChk->isChecked();    
}

void TestWindow::OnResolutionValue(int val) {
    d->volData->ldmResourceParameters.getValue()->resolution = val;
}

void TestWindow::OnMinTilesToLoad(int val) {
    d->volData->ldmResourceParameters.getValue()->minTilesToLoad = val;
}