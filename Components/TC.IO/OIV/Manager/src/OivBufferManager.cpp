#include "OivBufferManager.h"

#include <QVector>

#include "HDF5Mutex.h"
#include "OivHdf5Reader.h"
#include "OivColorReader.h"
#include "OivXYReader.h"
#include "OivYZReader.h"
#include "OivXZReader.h"
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivLdmReaderBF.h>
#include <OivLdmReaderXY.h>
#include <OivLdmReaderYZ.h>
#include <OivLdmReaderXZ.h>

#include "TCImageReader.h"
#include "TCDataConverter.h"

struct OivBufferManager::Impl {
	int buf_cnt { 2 };
	int image_cnt { 1 };
	int imageFL_cnt { 1 };
	int imageBF_cnt { 1 };
	int time_step { 0 };

	int htIdxX { 0 };
	int htIdxY { 0 };
	int htIdxZ { 0 };

	int flIdxX { 0 };
	int flIdxY { 0 };
	int flIdxZ { 0 };

	//Fixed resolution
	int fixedRes { 0 };
	bool isFixedResSlice { false };
	bool isFixedResVolume { false };

	//Floating resolution range
	int lowerRes { -1 };
	int upperRes { 0 };

	//Memory allocation
	int max2DTexMemory { -1 };
	int max3DTexMemory { -1 };
	int maxMainMemory { -1 };

	////////////////////////////

	QString path;

	//Current Time-step information
	QList<int> cur_stepHT;
	QList<int> cur_stepFL[3];
	QList<int> cur_stepBF;

	//HT3D	
	QList<SoRef<SoVolumeData>> volData;
	//HT Slice
	QList<SoRef<SoVolumeData>> volDataXY;
	QList<SoRef<SoVolumeData>> volDataYZ;
	QList<SoRef<SoVolumeData>> volDataXZ;

	//HT2D	
	QList<SoRef<SoVolumeData>> volData2D;

	//FL3D	
	QList<SoRef<SoVolumeData>> flVolData[3];
	//FL Slice
	QList<SoRef<SoVolumeData>> flVolDataXY[3];
	QList<SoRef<SoVolumeData>> flVolDataYZ[3];
	QList<SoRef<SoVolumeData>> flVolDataXZ[3];

	//FL2D	
	QList<SoRef<SoVolumeData>> flVolData2D[3];

	//BF2D
	QList<SoRef<SoVolumeData>> bfVolData2D[3];

	//HT3D memory
	//QList<TC::IO::TCImageReader*> rawReader;
	QList<SoRef<OivHdf5Reader>> reader;
	QList<SoRef<OivXYReader>> readerXY;
	QList<SoRef<OivYZReader>> readerYZ;
	QList<SoRef<OivXZReader>> readerXZ;
	QList<SoRef<OivHdf5Reader>> readerFL[3];
	QList<SoRef<OivXYReader>> readerFLXY[3];
	QList<SoRef<OivYZReader>> readerFLYZ[3];
	QList<SoRef<OivXZReader>> readerFLXZ[3];

	QList<SoRef<OivHdf5Reader>> reader2D;
	QList<SoRef<OivHdf5Reader>> readerFL2D[3];
	QList<SoRef<OivColorReader>> bfReader2D[3]; // only XY plane like MIP //unified color image,    	

	//HT3D Ldm
	QList<SoRef<OivLdmReader>> ldmReader;
	QList<SoRef<OivLdmReaderXY>> sldmReaderXY;
	//QList<OivLdmReader*> ldmReaderXY;
	//QList<OivLdmReaderYZ*> sldmReaderYZ;
	QList<SoRef<OivLdmReader>> ldmReaderYZ;
	//QList<OivLdmReaderXZ*> sldmReaderXZ;
	QList<SoRef<OivLdmReader>> ldmReaderXZ;
	//FL3D Ldm
	QList<SoRef<OivLdmReaderFL>> ldmFLReader[3];
	QList<SoRef<OivLdmReaderXY>> sldmFLReaderXY[3];
	//QList<OivLdmReaderFL*> ldmFLReaderXY[3];
	//QList<OivLdmReaderYZ*> sldmFLReaderYZ[3];
	QList<SoRef<OivLdmReaderFL>> ldmFLReaderYZ[3];
	//QList<OivLdmReaderXZ*> sldmFLReaderXZ[3];
	QList<SoRef<OivLdmReaderFL>> ldmFLReaderXZ[3];
	//HT2D Ldm
	QList<SoRef<OivLdmReader>> ldm2DReader;
	//FL2D Ldm
	QList<SoRef<OivLdmReaderFL>> ldm2DFLReader[3];
	//BF2D Ldm
	QList<SoRef<OivLdmReaderBF>> ldm2DBFReader[3];

	TC::IO::TCFMetaReader::Meta::Pointer metaInfo { nullptr };

	float fl_offset { 0 };

	bool customHTCrop { false };
	int htOffsetX { 0 };
	int htOffsetY { 0 };
	int htCropSizeX { 0 };
	int htCropSizeY { 0 };

	bool customFLCrop { false };
	int flOffsetX { 0 };
	int flOffsetY { 0 };
	int flCropSizeX { 0 };
	int flCropSizeY { 0 };

	bool sliceWise { false };
	bool xyOnly { false };
};

OivBufferManager::OivBufferManager(bool sliceWise, bool xyOnly) : d { new Impl } {
	d->sliceWise = sliceWise;
	d->xyOnly = xyOnly;
}

OivBufferManager::~OivBufferManager() {}

auto OivBufferManager::ClearMemory() -> void {}

auto OivBufferManager::setMetaInfo(TC::IO::TCFMetaReader::Meta::Pointer metaInfo) -> void {
	d->metaInfo = metaInfo;
}

auto OivBufferManager::setFilePath(QString path) -> void {
	d->path = path;
	d->fl_offset = readFLOffset();
}

auto OivBufferManager::setXYCropOffset(int offsetX, int offsetY) -> void {
	if (d->metaInfo->data.isLDM) {
		return;
	}
	d->customHTCrop = true;
	d->htOffsetX = offsetX;
	d->htOffsetY = offsetY;
}

auto OivBufferManager::setXYCropSize(int sizeX, int sizeY) -> void {
	if (d->metaInfo->data.isLDM) {
		return;
	}
	d->customHTCrop = true;
	d->htCropSizeX = sizeX;
	d->htCropSizeY = sizeY;
}

auto OivBufferManager::setXYCropOffsetFL(int offsetX, int offsetY) -> void {
	if (d->metaInfo->data.isLDM) {
		return;
	}
	d->customFLCrop = true;
	d->flOffsetX = offsetX;
	d->flOffsetY = offsetY;
}

auto OivBufferManager::setXYCropSizeFL(int sizeX, int sizeY) -> void {
	if (d->metaInfo->data.isLDM) {
		return;
	}
	d->customFLCrop = true;
	d->flCropSizeX = sizeX;
	d->flCropSizeY = sizeY;
}

auto OivBufferManager::setFixedResolution(int res, bool forVolume) -> void {
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	d->fixedRes = res;
	if (false == forVolume) {
		for (auto i = 0; i < max; i++) {
			if (d->metaInfo->data.data3D.exist && d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->resolution.setValue(res);
				if (false == d->xyOnly) {
					d->volDataYZ[i]->ldmResourceParameters.getValue()->resolution.setValue(res);
					d->volDataXZ[i]->ldmResourceParameters.getValue()->resolution.setValue(res);
				}
			}
			if (d->metaInfo->data.data2DMIP.exist) {
				d->volData2D[i]->ldmResourceParameters.getValue()->resolution.setValue(res);
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (false == d->metaInfo->data.data3DFL.valid[ch]) {
					continue;
				}
				if (d->metaInfo->data.data3DFL.exist && d->sliceWise) {
					d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->resolution.setValue(res);
					if (false == d->xyOnly) {
						d->flVolDataYZ[ch][i]->ldmResourceParameters.getValue()->resolution.setValue(res);
						d->flVolDataXZ[ch][i]->ldmResourceParameters.getValue()->resolution.setValue(res);
					}
				}
				if (d->metaInfo->data.data2DFLMIP.exist) {
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->resolution.setValue(res);
				}
			}
			if (d->metaInfo->data.dataBF.exist) {
				d->bfVolData2D[0][i]->ldmResourceParameters.getValue()->resolution.setValue(res);
				d->bfVolData2D[1][i]->ldmResourceParameters.getValue()->resolution.setValue(res);
				d->bfVolData2D[2][i]->ldmResourceParameters.getValue()->resolution.setValue(res);
			}
		}
	} else {
		for (int i = 0; i < max; i++) {
			if (d->metaInfo->data.data3D.exist) {
				d->volData[i]->ldmResourceParameters.getValue()->resolution.setValue(res);
			}
			for (int ch = 0; ch < 3; ch++) {
				if (d->metaInfo->data.data3DFL.valid[ch]) {
					if (d->metaInfo->data.data3DFL.exist) {
						d->flVolData[ch][i]->ldmResourceParameters.getValue()->resolution.setValue(res);
					}
				}
			}
		}
	}
}

auto OivBufferManager::getFixedResolution() -> int {
	//power of 2 is divider
	//0 means full resolution
	//when value grow, the loadable resolution becomes lower
	return d->fixedRes;
}

auto OivBufferManager::setMainMemory(int mem) -> void {
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	d->maxMainMemory = mem;
	if (d->xyOnly) {
		for (auto i = 0; i < max; i++) {
			if (d->metaInfo->data.data3D.exist && d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
			}
			if (d->metaInfo->data.data2DMIP.exist) {
				d->volData2D[i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (false == d->metaInfo->data.data3DFL.valid[ch]) {
					continue;
				}
				if (d->metaInfo->data.data2DFLMIP.exist && d->sliceWise) {
					d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
				}
				if (d->metaInfo->data.data2DFLMIP.exist) {
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
				}
			}
			if (d->metaInfo->data.dataBF.exist) {
				d->bfVolData2D[0][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
				d->bfVolData2D[1][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
				d->bfVolData2D[2][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
			}
		}
		return;
	}
	for (int i = 0; i < max; i++) {
		if (d->metaInfo->data.data3D.exist) {
			d->volData[i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
			if (d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
				d->volDataYZ[i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
				d->volDataXZ[i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
			}
		}
		if (d->metaInfo->data.data2DMIP.exist)
			d->volData2D[i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
		for (int ch = 0; ch < 3; ch++) {
			if (d->metaInfo->data.data3DFL.valid[ch]) {
				if (d->metaInfo->data.data3DFL.exist) {
					d->flVolData[ch][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
					if (d->sliceWise) {
						d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
						d->flVolDataYZ[ch][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
						d->flVolDataXZ[ch][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
					}
				}
				if (d->metaInfo->data.data2DFLMIP.exist)
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
			}
			if (d->metaInfo->data.dataBF.exist)
				d->bfVolData2D[ch][i]->ldmResourceParameters.getValue()->maxMainMemory.setValue(mem);
		}
	}
}

auto OivBufferManager::getMainMemory() -> int {
	return d->maxMainMemory;
}

auto OivBufferManager::setTex2DMemory(int mem) -> void {
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	d->max2DTexMemory = mem;
	if (d->xyOnly) {
		for (auto i = 0; i < max; i++) {
			if (d->metaInfo->data.data2DMIP.exist) {
				d->volData2D[i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
			}
			if (d->sliceWise && d->metaInfo->data.data3D.exist) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
			}
			for (int ch = 0; ch < 3; ch++) {
				if (false == d->metaInfo->data.data3DFL.valid[ch]) {
					continue;
				}
				if (d->metaInfo->data.data2DFLMIP.exist) {
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
				}
				if (d->sliceWise && d->metaInfo->data.data3DFL.exist) {
					d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
				}
			}
			if (d->metaInfo->data.dataBF.exist) {
				d->bfVolData2D[0][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
				d->bfVolData2D[1][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
				d->bfVolData2D[2][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
			}
		}
		return;
	}
	for (int i = 0; i < max; i++) {
		if (d->metaInfo->data.data2DMIP.exist) {
			d->volData2D[i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
		}
		if (d->sliceWise && d->metaInfo->data.data3D.exist) {
			d->volDataXY[i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
			d->volDataYZ[i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
			d->volDataXZ[i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
		}
		for (int ch = 0; ch < 3; ch++) {
			if (d->metaInfo->data.data3DFL.valid[ch]) {
				if (d->metaInfo->data.data2DFLMIP.exist) {
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
				}
				if (d->sliceWise && d->metaInfo->data.data3DFL.exist) {
					d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
					d->flVolDataYZ[ch][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
					d->flVolDataXZ[ch][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
				}
			}
			if (d->metaInfo->data.dataBF.exist)
				d->bfVolData2D[ch][i]->ldmResourceParameters.getValue()->max2DTexMemory.setValue(mem);
		}
	}
}

auto OivBufferManager::getTex2DMemory() -> int {
	return d->max2DTexMemory;
}

auto OivBufferManager::setTex3DMemory(int mem) -> void {
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	d->max3DTexMemory = mem;
	if (d->xyOnly) {
		return;
	}
	for (int i = 0; i < max; i++) {
		if (d->metaInfo->data.data3D.exist) {
			d->volData[i]->ldmResourceParameters.getValue()->maxTexMemory.setValue(mem);
		}
		for (int ch = 0; ch < 3; ch++) {
			if (d->metaInfo->data.data3DFL.valid[ch]) {
				if (d->metaInfo->data.data3DFL.exist)
					d->flVolData[ch][i]->ldmResourceParameters.getValue()->maxTexMemory.setValue(mem);
			}
		}
	}
}

auto OivBufferManager::convertFL(int step, int ch) -> void {
	auto buf_idx = step;
	auto dim = d->volData[buf_idx]->getDimension();
	std::shared_ptr<unsigned short> volume(new unsigned short[dim[0] * dim[1] * dim[2]](), std::default_delete<unsigned short[]>());
	auto fl_dim = d->flVolData[ch][buf_idx]->getDimension();
	SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(fl_dim[0] - 1, fl_dim[1] - 1, fl_dim[2] - 1));
	SoLDMDataAccess::DataInfoBox dataInfoBox =
		d->flVolData[ch][buf_idx]->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
	SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
	dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

	d->flVolData[ch][buf_idx]->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
	unsigned short* fl_pointer = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

	for (auto k = 0; k < dim[2]; k++) {
		for (auto j = 0; j < dim[1]; j++) {
			for (auto i = 0; i < dim[0]; i++) {
				SbVec3f index;
				index[0] = i;
				index[1] = j;
				index[2] = k;
				auto key = k * dim[0] * dim[1] + j * dim[0] + i;
				auto phys = d->volData[buf_idx]->voxelToXYZ(index);
				phys[2] -= d->fl_offset;
				auto voxel = d->flVolData[ch][buf_idx]->XYZToVoxel(phys);
				int vv[3] = { static_cast<int>(voxel[0]), static_cast<int>(voxel[1]), static_cast<int>(voxel[2]) };
				if (vv[0] >= 0 && vv[0] < fl_dim[0]
					&& vv[1] >= 0 && vv[1] < fl_dim[1]
					&& vv[2] >= 0 && vv[2] < fl_dim[2]) {
					int vKey = vv[2] * fl_dim[0] * fl_dim[1] + vv[1] * fl_dim[0] + vv[0];
					auto val = *(fl_pointer + vKey);
					*(volume.get() + key) = val;
				}
			}
		}
	}
}

auto OivBufferManager::getTex3DMemory() -> int {
	return d->max3DTexMemory;
}


auto OivBufferManager::setLowerResThreshold(int res) -> void {
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	d->lowerRes = res;
	if (d->xyOnly) {
		for (auto i = 0; i < max; i++) {
			if (d->metaInfo->data.data3D.exist && d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
			}
			if (d->metaInfo->data.data2DMIP.exist) {
				d->volData2D[i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (false == d->metaInfo->data.data3DFL.valid[ch]) {
					continue;
				}
				if (d->metaInfo->data.data3DFL.exist && d->sliceWise) {
					d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
				}
				if (d->metaInfo->data.data2DFLMIP.exist) {
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
				}
			}
			if (d->metaInfo->data.dataBF.exist) {
				d->bfVolData2D[0][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
				d->bfVolData2D[1][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
				d->bfVolData2D[2][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
			}
		}
		return;
	}
	for (int i = 0; i < max; i++) {
		if (d->metaInfo->data.data3D.exist) {
			d->volData[i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
			if (d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
				d->volDataYZ[i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
				d->volDataXZ[i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
			}
		}
		if (d->metaInfo->data.data2DMIP.exist)
			d->volData2D[i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
		for (int ch = 0; ch < 3; ch++) {
			if (d->metaInfo->data.data3DFL.valid[ch]) {
				if (d->metaInfo->data.data3DFL.exist) {
					d->flVolData[ch][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
					if (d->sliceWise) {
						d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
						d->flVolDataYZ[ch][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
						d->flVolDataXZ[ch][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
					}
				}
				if (d->metaInfo->data.data2DFLMIP.exist)
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
			}
			if (d->metaInfo->data.dataBF.exist)
				d->bfVolData2D[ch][i]->ldmResourceParameters.getValue()->minResolutionThreshold.setValue(res);
		}
	}
}

auto OivBufferManager::getLowerResThreshold() -> int {
	return d->lowerRes;
}

auto OivBufferManager::setUpperResThreshold(int res) -> void {
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	d->upperRes = res;
	if (d->xyOnly) {
		for (auto i = 0; i < max; i++) {
			if (d->metaInfo->data.data3D.exist && d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
			}
			if (d->metaInfo->data.data2DMIP.exist) {
				d->volData2D[i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (false == d->metaInfo->data.data3DFL.valid[ch]) {
					continue;
				}
				if (d->metaInfo->data.data3DFL.exist && d->sliceWise) {
					d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
				}
				if (d->metaInfo->data.data2DFLMIP.exist) {
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
				}
			}
			if (d->metaInfo->data.dataBF.exist) {
				d->bfVolData2D[0][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
				d->bfVolData2D[1][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
				d->bfVolData2D[2][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
			}
		}
		return;
	}
	for (int i = 0; i < max; i++) {
		if (d->metaInfo->data.data3D.exist) {
			d->volData[i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
			if (d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
				d->volDataYZ[i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
				d->volDataXZ[i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
			}
		}
		if (d->metaInfo->data.data2DMIP.exist)
			d->volData2D[i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);

		for (int ch = 0; ch < 3; ch++) {
			if (d->metaInfo->data.data3DFL.valid[ch]) {
				if (d->metaInfo->data.data3DFL.exist) {
					d->flVolData[ch][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
					if (d->sliceWise) {
						d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
						d->flVolDataYZ[ch][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
						d->flVolDataXZ[ch][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
					}
				}
				if (d->metaInfo->data.data2DFLMIP.exist)
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
			}
			if (d->metaInfo->data.dataBF.exist)
				d->bfVolData2D[ch][i]->ldmResourceParameters.getValue()->maxResolutionThreshold.setValue(res);
		}
	}
}

auto OivBufferManager::getUpperResThreshold() -> int {
	return d->upperRes;
}

auto OivBufferManager::enableFixedResolution(bool isFixedRes, bool forVolume) -> void {
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	if (forVolume) {
		d->isFixedResVolume = isFixedRes;
	} else {
		if (false == isFixedRes) {
			std::cout << "Someone set interactive to slice " << std::endl;
		}
		d->isFixedResSlice = isFixedRes;
	}
	if (d->xyOnly) {
		for (auto i = 0; i < max; i++) {
			if (d->metaInfo->data.data3D.exist && d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
			}
			if (d->metaInfo->data.data2DMIP.exist) {
				d->volData2D[i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (false == d->metaInfo->data.data3DFL.valid[ch]) {
					continue;
				}
				if (d->metaInfo->data.data3DFL.exist && d->sliceWise) {
					d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
				}
				if (d->metaInfo->data.data2DFLMIP.exist) {
					d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
				}
			}
			if (d->metaInfo->data.dataBF.exist) {
				d->bfVolData2D[0][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
				d->bfVolData2D[1][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
				d->bfVolData2D[2][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
			}
		}
		return;
	}
	if (forVolume) {
		for (int i = 0; i < max; i++) {
			if (d->metaInfo->data.data3D.exist) {
				d->volData[i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
			}

			for (int ch = 0; ch < 3; ch++) {
				if (d->metaInfo->data.data3DFL.valid[ch]) {
					if (d->metaInfo->data.data3DFL.exist) {
						d->flVolData[ch][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
					}
				}
			}
		}
	} else {
		for (int i = 0; i < max; i++) {
			if (d->metaInfo->data.data3D.exist && d->sliceWise) {
				d->volDataXY[i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
				d->volDataYZ[i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
				d->volDataXZ[i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
			}
			if (d->metaInfo->data.data2DMIP.exist)
				d->volData2D[i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);

			for (int ch = 0; ch < 3; ch++) {
				if (d->metaInfo->data.data3DFL.valid[ch]) {
					if (d->metaInfo->data.data3DFL.exist && d->sliceWise) {
						d->flVolDataXY[ch][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
						d->flVolDataYZ[ch][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
						d->flVolDataXZ[ch][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
					}
					if (d->metaInfo->data.data2DFLMIP.exist)
						d->flVolData2D[ch][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
				}
				if (d->metaInfo->data.dataBF.exist)
					d->bfVolData2D[ch][i]->ldmResourceParameters.getValue()->fixedResolution.setValue(isFixedRes);
			}
		}
	}

}

auto OivBufferManager::isFixedResolutionEnabled(bool forVolume) -> bool {
	if (forVolume) {
		return d->isFixedResVolume;
	}
	return d->isFixedResSlice;
}

auto OivBufferManager::getDefaultVolume() -> SoVolumeData* {
	if (d->metaInfo->data.data3D.exist) {
		if (d->xyOnly) {
			return d->volDataXY[0].ptr();
		}
		return d->volData[0].ptr();
	}
	if (d->metaInfo->data.data3DFL.exist) {
		if (d->xyOnly) {
			if (d->metaInfo->data.data3DFL.valid[0]) {
				return d->flVolDataXY[0][0].ptr();
			}
			if (d->metaInfo->data.data3DFL.valid[1]) {
				return d->flVolDataXY[1][0].ptr();
			}
			if (d->metaInfo->data.data3DFL.valid[2]) {
				return d->flVolDataXY[2][0].ptr();
			}
		}
		if (d->metaInfo->data.data3DFL.valid[0]) {
			return d->flVolData[0][0].ptr();
		}
		if (d->metaInfo->data.data3DFL.valid[1]) {
			return d->flVolData[1][0].ptr();
		}
		if (d->metaInfo->data.data3DFL.valid[2]) {
			return d->flVolData[2][0].ptr();
		}
	}
	return nullptr;
}


auto OivBufferManager::getHT3dVolume(int step) -> SoVolumeData* {
	if (step < d->image_cnt) {
		if (connectHT3dReader(step)) {
			return d->volData[step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getHTXYSlice(int step) -> SoVolumeData* {
	if (step < d->image_cnt) {
		if (connectHTXYReader(step)) {
			return d->volDataXY[step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getHTYZSlice(int step) -> SoVolumeData* {
	if (step < d->image_cnt) {
		if (connectHTYZReader(step)) {
			double min, max;
			d->volDataYZ[step % d->buf_cnt]->getMinMax(min, max);
			return d->volDataYZ[step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getHTXZSlice(int step) -> SoVolumeData* {
	if (step < d->image_cnt) {
		if (connectHTXZReader(step)) {
			double min, max;
			d->volDataXZ[step % d->buf_cnt]->getMinMax(min, max);
			return d->volDataXZ[step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getHT2dVolume(int step) -> SoVolumeData* {
	if (step < d->image_cnt) {
		if (connectHT2dReader(step)) {
			return d->volData2D[step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getFL3dVolume(int step, int ch) -> SoVolumeData* {
	if (step < d->imageFL_cnt) {
		if (connectFL3dReader(step, ch)) {
			return d->flVolData[ch][step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getFLXYSlice(int step, int ch) -> SoVolumeData* {
	if (step < d->imageFL_cnt) {
		if (connectFLXYReader(step, ch)) {
			return d->flVolDataXY[ch][step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getFLYZSlice(int step, int ch) -> SoVolumeData* {
	if (step < d->imageFL_cnt) {
		if (connectFLYZReader(step, ch)) {
			return d->flVolDataYZ[ch][step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getFLXZSlice(int step, int ch) -> SoVolumeData* {
	if (step < d->imageFL_cnt) {
		if (connectFLXZReader(step, ch)) {
			return d->flVolDataXZ[ch][step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::readFLOffset() -> float {
	TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
	H5::Exception::dontPrint();
	H5::H5File file(d->path.toStdString().c_str(), H5F_ACC_RDONLY);
	float flOffsetFromHTBottom = 0.0;
	float htHalfSize = 0.0;
	if (file.exists("/Data/3DFL/")) {
		auto group = file.openGroup("/Data/3DFL/");
		if (group.attrExists("OffsetZ") && group.attrExists("SizeZ") && group.attrExists("ResolutionZ")) {
			auto sAttr = group.openAttribute("SizeZ");
			uint64_t sizeZ;
			sAttr.read(H5::PredType::NATIVE_INT64, &sizeZ);
			sAttr.close();
			double offset;
			auto attr = group.openAttribute("OffsetZ");
			attr.read(H5::PredType::NATIVE_DOUBLE, &offset);
			attr.close();

			flOffsetFromHTBottom = offset;
		}
		group.close();
	}
	if (file.exists("/Data/3D/")) {
		auto group = file.openGroup("/Data/3D/");
		if (group.attrExists("SizeZ") && group.attrExists("ResolutionZ")) {
			auto sAttr = group.openAttribute("SizeZ");
			uint64_t sizeZ;
			sAttr.read(H5::PredType::NATIVE_INT64, &sizeZ);
			sAttr.close();
			auto rAttr = group.openAttribute("ResolutionZ");
			double resZ;
			rAttr.read(H5::PredType::NATIVE_DOUBLE, &resZ);
			rAttr.close();
			htHalfSize = sizeZ * resZ / 2;
		}
	}
	file.close();
	//return 0.0;
	return flOffsetFromHTBottom - htHalfSize;
}

auto OivBufferManager::getFL2dVolume(int step, int ch) -> SoVolumeData* {
	if (step < d->imageFL_cnt) {
		if (connectFL2dReader(step, ch)) {
			return d->flVolData2D[ch][step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}

auto OivBufferManager::getBF2dVolume(int step, int ch) -> SoVolumeData* {
	if (step < d->imageBF_cnt) {
		if (connectBF2dReader(step, ch)) {
			return d->bfVolData2D[ch][step % d->buf_cnt].ptr();
		}
	}
	return nullptr;
}


auto OivBufferManager::setNumberOfBFImage(int n) -> void {
	d->imageBF_cnt = n;
}


auto OivBufferManager::setNumberOfFLImage(int n) -> void {
	d->imageFL_cnt = n;
}

auto OivBufferManager::setNumberOfImage(int n) -> void {
	d->image_cnt = n;
}

auto OivBufferManager::setDefaultReader() -> void {
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	auto maxFL = (d->imageFL_cnt > 1) ? d->buf_cnt : 1;
	auto maxBF = (d->imageBF_cnt > 1) ? d->buf_cnt : 1;
	if (d->xyOnly) {
		for (auto i = 0; i < max; i++) {
			if (d->sliceWise && d->metaInfo->data.data3D.exist) {
				connectHTXYReader(i, true);
			}
			if (d->metaInfo->data.data2DMIP.exist) {
				connectHT2dReader(i, true);
			}
		}
		for (auto i = 0; i < maxFL; i++) {
			if (false == (d->metaInfo->data.data3DFL.exist && d->sliceWise)) {
				continue;
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (false == d->metaInfo->data.data3DFL.valid[ch]) {
					continue;
				}
				connectFLXYReader(i, ch, true);
				if (d->metaInfo->data.data2DFLMIP.exist) {
					connectFL2dReader(i, ch, true);
				}
			}
		}
		for (auto i = 0; i < maxBF; i++) {
			if (false == d->metaInfo->data.dataBF.exist) {
				continue;
			}
			connectBF2dReader(i, 0, true);
			connectBF2dReader(i, 1, true);
			connectBF2dReader(i, 2, true);
		}
		return;
	}
	for (int i = 0; i < max; i++) {
		if (d->metaInfo->data.data3D.exist) {
			connectHT3dReader(i, true);
			if (d->sliceWise) {
				connectHTXYReader(i, true);
				connectHTYZReader(i, true);
				connectHTXZReader(i, true);
			}
		}
		if (d->metaInfo->data.data2DMIP.exist) {
			connectHT2dReader(i, true);
		}
	}
	for (int i = 0; i < maxFL; i++) {
		if (d->metaInfo->data.data3DFL.exist) {
			for (int ch = 0; ch < 3; ch++) {
				if (d->metaInfo->data.data3DFL.valid[ch]) {
					connectFL3dReader(i, ch, true);
					if (d->sliceWise) {
						connectFLXYReader(i, ch, true);
						connectFLYZReader(i, ch, true);
						connectFLXZReader(i, ch, true);
					}
				}
			}
		}
		if (d->metaInfo->data.data2DFLMIP.exist) {
			for (int ch = 0; ch < 3; ch++) {
				if (d->metaInfo->data.data3DFL.valid[ch]) {
					connectFL2dReader(i, ch, true);
				}
			}
		}
	}
	for (int i = 0; i < maxBF; i++) {
		if (d->metaInfo->data.dataBF.exist) {
			for (int ch = 0; ch < 3; ch++) {
				connectBF2dReader(i, ch, true);
			}
		}
	}
}

auto OivBufferManager::connectHT3dReader(int time_step, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;

	if (time_step != d->cur_stepHT[buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data3D.scalarType[time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			d->ldmReader[buf_idx]->set8Bit(is8Bit);
			d->ldmReader[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldmReader[buf_idx]->setDataGroupPath("/Data/3D");
				d->ldmReader[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ);
				d->ldmReader[buf_idx]->setFilename(d->path.toStdString());
				d->volData[buf_idx]->setReader(*d->ldmReader[buf_idx], FALSE);
			}
			d->ldmReader[buf_idx]->touch();
		} else {
			d->reader[buf_idx]->setTileName(tileName.toStdString());
			if (d->customHTCrop) {
				d->reader[buf_idx]->setXYCropOffset(d->htOffsetX, d->htOffsetY);
				d->reader[buf_idx]->setXYCropSize(d->htCropSizeX, d->htCropSizeY);
			}
			if (false == init) {
				d->reader[buf_idx]->set8Bit(is8Bit);
			} else {
				d->reader[buf_idx]->setDataGroupPath("/Data/3D");
				d->reader[buf_idx]->setFilename(d->path.toStdString());
				d->reader[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
				d->volData[buf_idx]->setReader(*d->reader[buf_idx], FALSE);
			}
			d->reader[buf_idx]->touch();
		}
	}

	if (d->volData[buf_idx])
		return true;
	return false;
}

auto OivBufferManager::connectHTXYReader(int time_step, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;

	if (time_step != d->cur_stepHT[buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data3D.scalarType[time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			d->sldmReaderXY[buf_idx]->set8Bit(is8Bit);
			d->sldmReaderXY[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->sldmReaderXY[buf_idx]->setDataGroupPath("/Data/3D");
				d->sldmReaderXY[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ);
				d->sldmReaderXY[buf_idx]->setFilename(d->path.toStdString());
				d->volDataXY[buf_idx]->setReader(*d->sldmReaderXY[buf_idx], FALSE);
			}
			d->sldmReaderXY[buf_idx]->touch();
			/*d->ldmReaderXY[buf_idx]->set8Bit(is8Bit);
			d->ldmReaderXY[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldmReaderXY[buf_idx]->setDataGroupPath("/Data/3D");
				d->ldmReaderXY[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ);
				d->ldmReaderXY[buf_idx]->setFilename(d->path.toStdString());
				d->volDataXY[buf_idx]->setReader(*d->ldmReaderXY[buf_idx], FALSE);
			}
			d->ldmReaderXY[buf_idx]->touch();*/

		} else {
			if (false == init) {
				d->readerXY[buf_idx]->set8Bit(is8Bit);
			} else {
				d->readerXY[buf_idx]->setDataGroupPath("/Data/3D");
				d->readerXY[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
				d->readerXY[buf_idx]->setFilename(d->path.toStdString());
				d->volDataXY[buf_idx]->setReader(*d->readerXY[buf_idx], FALSE);
			}
			d->readerXY[buf_idx]->setTileName(tileName.toStdString());
			if (d->customHTCrop) {
				d->readerXY[buf_idx]->setXYCropOffset(d->htOffsetX, d->htOffsetY);
				d->readerXY[buf_idx]->setXYCropSize(d->htCropSizeX, d->htCropSizeY);
			}
			d->readerXY[buf_idx]->touch();
		}
	}
	if (d->volDataXY[buf_idx]) {
		return true;
	}
	return false;
}

auto OivBufferManager::connectHTYZReader(int time_step, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;

	if (time_step != d->cur_stepHT[buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data3D.scalarType[time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			/*d->sldmReaderYZ[buf_idx]->set8Bit(is8Bit);
			d->sldmReaderYZ[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->sldmReaderYZ[buf_idx]->setDataGroupPath("/Data/3D");
				d->sldmReaderYZ[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ);
				d->sldmReaderYZ[buf_idx]->setFilename(d->path.toStdString());
				d->volDataYZ[buf_idx]->setReader(*d->sldmReaderYZ[buf_idx], FALSE);
			}
			d->sldmReaderYZ[buf_idx]->touch();*/
			d->ldmReaderYZ[buf_idx]->set8Bit(is8Bit);
			d->ldmReaderYZ[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldmReaderYZ[buf_idx]->setDataGroupPath("/Data/3D");
				d->ldmReaderYZ[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ);
				d->ldmReaderYZ[buf_idx]->setFilename(d->path.toStdString());
				d->volDataYZ[buf_idx]->setReader(*d->ldmReaderYZ[buf_idx], FALSE);
			}
			d->ldmReaderYZ[buf_idx]->touch();

		} else {
			if (false == init) {
				d->readerYZ[buf_idx]->set8Bit(is8Bit);
			} else {
				d->readerYZ[buf_idx]->setDataGroupPath("/Data/3D");
				d->readerYZ[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
				d->readerYZ[buf_idx]->setFilename(d->path.toStdString());
				d->volDataYZ[buf_idx]->setReader(*d->readerYZ[buf_idx], FALSE);
			}
			d->readerYZ[buf_idx]->setTileName(tileName.toStdString());
			if (d->customHTCrop) {
				d->readerYZ[buf_idx]->setXYCropOffset(d->htOffsetX, d->htOffsetY);
				d->readerYZ[buf_idx]->setXYCropSize(d->htCropSizeX, d->htCropSizeY);
			}
			d->readerYZ[buf_idx]->touch();
		}
	}
	if (d->volDataYZ[buf_idx]) {
		return true;
	}
	return false;
}

auto OivBufferManager::connectHTXZReader(int time_step, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;

	if (time_step != d->cur_stepHT[buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data3D.scalarType[time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			/*d->sldmReaderXZ[buf_idx]->set8Bit(is8Bit);
			d->sldmReaderXZ[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->sldmReaderXZ[buf_idx]->setDataGroupPath("/Data/3D");
				d->sldmReaderXZ[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ);
				d->sldmReaderXZ[buf_idx]->setFilename(d->path.toStdString());
				d->volDataXZ[buf_idx]->setReader(*d->sldmReaderXZ[buf_idx], FALSE);
			}
			d->sldmReaderXZ[buf_idx]->touch();*/
			d->ldmReaderXZ[buf_idx]->set8Bit(is8Bit);
			d->ldmReaderXZ[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldmReaderXZ[buf_idx]->setDataGroupPath("/Data/3D");
				d->ldmReaderXZ[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ);
				d->ldmReaderXZ[buf_idx]->setFilename(d->path.toStdString());
				d->volDataXZ[buf_idx]->setReader(*d->ldmReaderXZ[buf_idx], FALSE);
			}
			d->ldmReaderXZ[buf_idx]->touch();

		} else {
			if (false == init) {
				d->readerXZ[buf_idx]->set8Bit(is8Bit);
			} else {
				d->readerXZ[buf_idx]->setDataGroupPath("/Data/3D");
				d->readerXZ[buf_idx]->setTileDimension(d->metaInfo->data.data3D.tileSizeX);
				d->readerXZ[buf_idx]->setFilename(d->path.toStdString());
				d->volDataXZ[buf_idx]->setReader(*d->readerXZ[buf_idx], FALSE);
			}
			d->readerXZ[buf_idx]->setTileName(tileName.toStdString());
			if (d->customHTCrop) {
				d->readerXZ[buf_idx]->setXYCropOffset(d->htOffsetX, d->htOffsetY);
				d->readerXZ[buf_idx]->setXYCropSize(d->htCropSizeX, d->htCropSizeY);
			}
			d->readerXZ[buf_idx]->touch();
		}
	}
	if (d->volDataXZ[buf_idx]) {
		return true;
	}
	return false;
}

auto OivBufferManager::connectFL3dReader(int time_step, int ch, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;
	if (time_step != d->cur_stepFL[ch][buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data3DFL.scalarType[ch][time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			d->ldmFLReader[ch][buf_idx]->set8Bit(is8Bit);
			d->ldmFLReader[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldmFLReader[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->ldmFLReader[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ);
				d->ldmFLReader[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolData[ch][buf_idx]->setReader(*d->ldmFLReader[ch][buf_idx], FALSE);
			}
			d->ldmFLReader[ch][buf_idx]->touch();
		} else {
			d->readerFL[ch][buf_idx]->setTileName(tileName.toStdString());
			if (d->customFLCrop) {
				d->readerFL[ch][buf_idx]->setXYCropOffset(d->flOffsetX, d->flOffsetY);
				d->readerFL[ch][buf_idx]->setXYCropSize(d->flCropSizeX, d->flCropSizeY);
			}
			if (false == init) {
				d->readerFL[ch][buf_idx]->set8Bit(is8Bit);
			} else {
				d->readerFL[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->readerFL[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX);
				d->readerFL[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolData[ch][buf_idx]->setReader(*d->readerFL[ch][buf_idx], FALSE);
			}
			d->readerFL[ch][buf_idx]->touch();
		}
	}
	if (d->flVolData[ch][buf_idx]) {
		return true;
	} else
		return false;
}

auto OivBufferManager::connectFLXYReader(int time_step, int ch, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;
	if (time_step != d->cur_stepFL[ch][buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data3DFL.scalarType[ch][time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			d->sldmFLReaderXY[ch][buf_idx]->set8Bit(is8Bit);
			d->sldmFLReaderXY[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->sldmFLReaderXY[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->sldmFLReaderXY[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ);
				d->sldmFLReaderXY[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataXY[ch][buf_idx]->setReader(*d->sldmFLReaderXY[ch][buf_idx], FALSE);
			}
			d->sldmFLReaderXY[ch][buf_idx]->touch();
			/*d->ldmFLReaderXY[ch][buf_idx]->set8Bit(is8Bit);
			d->ldmFLReaderXY[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldmFLReaderXY[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->ldmFLReaderXY[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ);
				d->ldmFLReaderXY[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataXY[ch][buf_idx]->setReader(*d->ldmFLReaderXY[ch][buf_idx], FALSE);
			}
			d->ldmFLReaderXY[ch][buf_idx]->touch();		*/
		} else {
			if (false == init) {
				d->readerFLXY[ch][buf_idx]->set8Bit(is8Bit);
			} else {
				d->readerFLXY[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->readerFLXY[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX);
				d->readerFLXY[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataXY[ch][buf_idx]->setReader(*d->readerFLXY[ch][buf_idx], FALSE);
			}
			d->readerFLXY[ch][buf_idx]->setTileName(tileName.toStdString());
			if (d->customFLCrop) {
				d->readerFLXY[ch][buf_idx]->setXYCropOffset(d->flOffsetX, d->flOffsetY);
				d->readerFLXY[ch][buf_idx]->setXYCropSize(d->flCropSizeX, d->flCropSizeY);
			}
			d->readerFLXY[ch][buf_idx]->touch();
		}
	}
	if (d->flVolDataXY[ch][buf_idx]) {
		return true;
	}
	return false;
}

auto OivBufferManager::connectFLYZReader(int time_step, int ch, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;
	if (time_step != d->cur_stepFL[ch][buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data3DFL.scalarType[ch][time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			/*d->sldmFLReaderYZ[ch][buf_idx]->set8Bit(is8Bit);
			d->sldmFLReaderYZ[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->sldmFLReaderYZ[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->sldmFLReaderYZ[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ);
				d->sldmFLReaderYZ[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataYZ[ch][buf_idx]->setReader(*d->sldmFLReaderYZ[ch][buf_idx], FALSE);
			}
			d->sldmFLReaderYZ[ch][buf_idx]->touch();
			auto oriExtent = d->flVolDataYZ[ch][buf_idx]->extent.getValue();
			d->flVolDataYZ[ch][buf_idx]->extent.setValue(oriExtent.getMin()[0], oriExtent.getMin()[1], oriExtent.getMin()[2], oriExtent.getMax()[0], oriExtent.getMax()[1], oriExtent.getMax()[2]);*/
			d->ldmFLReaderYZ[ch][buf_idx]->set8Bit(is8Bit);
			d->ldmFLReaderYZ[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldmFLReaderYZ[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->ldmFLReaderYZ[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ);
				d->ldmFLReaderYZ[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataYZ[ch][buf_idx]->setReader(*d->ldmFLReaderYZ[ch][buf_idx], FALSE);
			}
			d->ldmFLReaderYZ[ch][buf_idx]->touch();
		} else {
			if (false == init) {
				d->readerFLYZ[ch][buf_idx]->set8Bit(is8Bit);
			} else {
				d->readerFLYZ[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->readerFLYZ[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX);
				d->readerFLYZ[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataYZ[ch][buf_idx]->setReader(*d->readerFLYZ[ch][buf_idx], FALSE);
			}
			d->readerFLYZ[ch][buf_idx]->setTileName(tileName.toStdString());
			if (d->customFLCrop) {
				d->readerFLYZ[ch][buf_idx]->setXYCropOffset(d->flOffsetX, d->flOffsetY);
				d->readerFLYZ[ch][buf_idx]->setXYCropSize(d->flCropSizeX, d->flCropSizeY);
			}
			d->readerFLYZ[ch][buf_idx]->touch();
		}
	}
	if (d->flVolDataYZ[ch][buf_idx]) {
		return true;
	}
	return false;
}

auto OivBufferManager::connectFLXZReader(int time_step, int ch, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;
	if (time_step != d->cur_stepFL[ch][buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data3DFL.scalarType[ch][time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			/*d->sldmFLReaderXZ[ch][buf_idx]->set8Bit(is8Bit);
			d->sldmFLReaderXZ[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->sldmFLReaderXZ[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->sldmFLReaderXZ[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ);
				d->sldmFLReaderXZ[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataXZ[ch][buf_idx]->setReader(*d->sldmFLReaderXZ[ch][buf_idx], FALSE);
			}
			d->sldmFLReaderXZ[ch][buf_idx]->touch();
			auto oriExtent = d->flVolDataXZ[ch][buf_idx]->extent.getValue();
			d->flVolDataXZ[ch][buf_idx]->extent.setValue(oriExtent.getMin()[0], oriExtent.getMin()[1], oriExtent.getMin()[2], oriExtent.getMax()[0], oriExtent.getMax()[1], oriExtent.getMax()[2]);*/
			d->ldmFLReaderXZ[ch][buf_idx]->set8Bit(is8Bit);
			d->ldmFLReaderXZ[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldmFLReaderXZ[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->ldmFLReaderXZ[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ);
				d->ldmFLReaderXZ[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataXZ[ch][buf_idx]->setReader(*d->ldmFLReaderXZ[ch][buf_idx], FALSE);
			}
			d->ldmFLReaderXZ[ch][buf_idx]->touch();
		} else {
			if (false == init) {
				d->readerFLXZ[ch][buf_idx]->set8Bit(is8Bit);
			} else {
				d->readerFLXZ[ch][buf_idx]->setDataGroupPath(QString("/Data/3DFL/CH%1").arg(ch).toStdString());
				d->readerFLXZ[ch][buf_idx]->setTileDimension(d->metaInfo->data.data3DFL.tileSizeX);
				d->readerFLXZ[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolDataXZ[ch][buf_idx]->setReader(*d->readerFLXZ[ch][buf_idx], FALSE);
			}
			d->readerFLXZ[ch][buf_idx]->setTileName(tileName.toStdString());
			if (d->customFLCrop) {
				d->readerFLXZ[ch][buf_idx]->setXYCropOffset(d->flOffsetX, d->flOffsetY);
				d->readerFLXZ[ch][buf_idx]->setXYCropSize(d->flCropSizeX, d->flCropSizeY);
			}
			d->readerFLXZ[ch][buf_idx]->touch();
		}
	}
	if (d->flVolDataXZ[ch][buf_idx]) {
		return true;
	}
	return false;
}

auto OivBufferManager::connectBF2dReader(int time_step, int ch, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;
	if (time_step != d->cur_stepBF[buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		if (d->metaInfo->data.isLDM) {
			d->ldm2DBFReader[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldm2DBFReader[ch][buf_idx]->setFilename(d->path.toStdString());
				d->bfVolData2D[ch][buf_idx]->setReader(*d->ldm2DBFReader[ch][buf_idx], TRUE);
			}
			d->ldm2DBFReader[ch][buf_idx]->touch();
		} else {
			d->bfReader2D[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->bfReader2D[ch][buf_idx]->setFilename(d->path.toStdString());
				d->bfVolData2D[ch][buf_idx]->setReader(*d->bfReader2D[ch][buf_idx], TRUE);
			}
			d->bfReader2D[ch][buf_idx]->touch();
		}
	}
	if (d->bfVolData2D[ch][buf_idx])
		return true;
	return false;
}

auto OivBufferManager::connectHT2dReader(int time_step, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;

	if (time_step != d->cur_stepHT[buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data2DMIP.scalarType[time_step] == 1;
		if (d->metaInfo->data.isLDM) {
			d->ldm2DReader[buf_idx]->set8Bit(is8Bit);
			d->ldm2DReader[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldm2DReader[buf_idx]->setDataGroupPath("/Data/2DMIP");
				d->ldm2DReader[buf_idx]->setTileDimension(d->metaInfo->data.data2DMIP.tileSizeX, d->metaInfo->data.data2DMIP.tileSizeY, 1);
				d->ldm2DReader[buf_idx]->setFilename(d->path.toStdString());
				d->volData2D[buf_idx]->setReader(*d->ldm2DReader[buf_idx], FALSE);
			}
			d->ldm2DReader[buf_idx]->touch();
		} else {
			d->reader2D[buf_idx]->set8Bit(is8Bit);
			d->reader2D[buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->reader2D[buf_idx]->setDataGroupPath("/Data/2DMIP");
				d->reader2D[buf_idx]->setTileDimension(d->metaInfo->data.data2DMIP.tileSizeX);
				d->reader2D[buf_idx]->setFilename(d->path.toStdString());
				d->volData2D[buf_idx]->setReader(*d->reader2D[buf_idx], FALSE);
			}
			d->reader2D[buf_idx]->touch();
		}
	}
	if (d->volData2D[buf_idx])
		return true;
	else
		return false;
}

auto OivBufferManager::connectFL2dReader(int time_step, int ch, bool init) -> bool {
	auto buf_idx = time_step % d->buf_cnt;

	if (time_step != d->cur_stepFL[ch][buf_idx] || init) {
		QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
		auto is8Bit = d->metaInfo->data.data2DFLMIP.scalarType[ch][time_step];
		if (d->metaInfo->data.isLDM) {
			d->ldm2DFLReader[ch][buf_idx]->set8Bit(is8Bit);
			d->ldm2DFLReader[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->ldm2DFLReader[ch][buf_idx]->setDataGroupPath(QString("/Data/2DFLMIP/CH%1").arg(ch).toStdString());
				d->ldm2DFLReader[ch][buf_idx]->setTileDimension(d->metaInfo->data.data2DFLMIP.tileSizeX, d->metaInfo->data.data2DFLMIP.tileSizeY, 1);
				d->ldm2DFLReader[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolData2D[ch][buf_idx]->setReader(*d->ldm2DFLReader[ch][buf_idx], FALSE);
			}
			d->ldm2DFLReader[ch][buf_idx]->touch();
		} else {
			d->readerFL2D[ch][buf_idx]->set8Bit(is8Bit);
			d->readerFL2D[ch][buf_idx]->setTileName(tileName.toStdString());
			if (init) {
				d->readerFL2D[ch][buf_idx]->setDataGroupPath(QString("/Data/2DFLMIP/CH%1").arg(ch).toStdString());
				d->readerFL2D[ch][buf_idx]->setTileDimension(d->metaInfo->data.data2DFLMIP.tileSizeX);
				d->readerFL2D[ch][buf_idx]->setFilename(d->path.toStdString());
				d->flVolData2D[ch][buf_idx]->setReader(*d->readerFL2D[ch][buf_idx], FALSE);
			}
			d->readerFL2D[ch][buf_idx]->touch();
		}
	}
	if (d->flVolData2D[ch][buf_idx])
		return true;
	else
		return false;
}

auto OivBufferManager::setPlayStepHT(int t) -> void {
	setTimeStepHT(t);//check current step;
	for (int i = 1; i < d->buf_cnt; i++) {
		auto next_step = (t + 1) % d->image_cnt;
		setTimeStepHT(next_step);//check up-comming steps;
	}
}

auto OivBufferManager::setPlayStemFL(int t) -> void {
	for (auto ch = 0; ch < 3; ch++) {
		setTimeStepFL(t, ch);
		for (int i = 1; i < d->buf_cnt; i++) {
			auto next_step = (t + 1) % d->imageFL_cnt;
			setTimeStepFL(next_step, ch);
		}
	}
}

auto OivBufferManager::setPlayStemBF(int t) -> void {
	setTimeStepBF(t);
	for (int i = 1; i < d->buf_cnt; i++) {
		auto next_step = (t + 1) % d->imageBF_cnt;
		setTimeStepBF(next_step);
	}
}


auto OivBufferManager::setTimeStepHT(int t) -> void {
	auto buf_idx = t % d->buf_cnt;
	if (d->xyOnly) {
		if (d->cur_stepHT.size() <= buf_idx) {
			return;
		}
		if (d->cur_stepHT[buf_idx] == t) {
			return;
		}
		if (d->sliceWise && d->metaInfo->data.data3D.exist) {
			connectHTXYReader(t);
		}
		if (d->metaInfo->data.data2DMIP.exist) {
			connectHT2dReader(t);
		}
		d->cur_stepHT[buf_idx] = t;
		return;
	}
	if (d->cur_stepHT.size() > buf_idx) {
		if (d->cur_stepHT[buf_idx] != t) {
			if (d->metaInfo->data.data3D.exist) {
				connectHT3dReader(t);
				if (d->sliceWise) {
					connectHTXYReader(t);
					connectHTYZReader(t);
					connectHTXZReader(t);
				}
			}
			if (d->metaInfo->data.data2DMIP.exist) {
				connectHT2dReader(t);
			}
			d->cur_stepHT[buf_idx] = t;
		}
	}
}

auto OivBufferManager::prepareTimeHT(int t) -> void {
	if (d->xyOnly) {
		return;
	}
	connectHT3dReader(t);
}


auto OivBufferManager::setTimeStepBF(int t) -> void {
	if (t < 0) {
		return;
	}
	auto buf_idx = t % d->buf_cnt;
	if (d->xyOnly) {
		if (d->metaInfo->data.dataBF.exist) {
			connectBF2dReader(t, 0);
			connectBF2dReader(t, 1);
			connectBF2dReader(t, 2);
		}
		d->cur_stepBF[buf_idx] = t;
		return;
	}
	if (d->cur_stepBF.size() > buf_idx) {
		if (d->metaInfo->data.dataBF.exist) {
			connectBF2dReader(t, 0);
			connectBF2dReader(t, 1);
			connectBF2dReader(t, 2);
		}
		d->cur_stepBF[buf_idx] = t;
	}
}

auto OivBufferManager::setTimeStepFL(int t, int ch) -> void {
	auto buf_idx = t % d->buf_cnt;
	if (d->xyOnly) {
		if (d->cur_stepFL[ch].size() <= buf_idx) {
			return;
		}
		if (d->cur_stepFL[ch][buf_idx] == t) {
			return;
		}
		if (d->metaInfo->data.data3DFL.exist && d->sliceWise) {
			if (d->metaInfo->data.data3DFL.valid[ch]) {
				connectFLXYReader(t, ch);
			}
		}
		if (d->metaInfo->data.data2DFLMIP.exist) {
			if (d->metaInfo->data.data3DFL.valid[ch]) {
				connectFL2dReader(t, ch);
			}
		}
		d->cur_stepFL[ch][buf_idx] = t;
		return;
	}
	if (d->cur_stepFL[ch].size() > buf_idx) {
		if (d->cur_stepFL[ch][buf_idx] != t) {
			if (d->metaInfo->data.data3DFL.exist) {
				for (int i = 0; i < 3; i++) {
					if (d->metaInfo->data.data3DFL.valid[i]) {
						connectFL3dReader(t, i);
						if (d->sliceWise) {
							connectFLXYReader(t, i);
							connectFLYZReader(t, i);
							connectFLXZReader(t, i);
						}
					}
				}
			}
			if (d->metaInfo->data.data2DFLMIP.exist) {
				for (int i = 0; i < 3; i++) {
					if (d->metaInfo->data.data3DFL.valid[i])
						connectFL2dReader(t, i);
				}
			}
			d->cur_stepFL[ch][buf_idx] = t;
		}
	}
}


auto OivBufferManager::initialize() -> void {
	if (d->path.length() == 0)//file path must be set before initialize;
		return;
	//initilizae containers
	auto max = (d->image_cnt > 1) ? d->buf_cnt : 1;
	auto max_fl = (d->imageFL_cnt > 1) ? d->buf_cnt : 1;
	auto max_bf = (d->imageBF_cnt > 1) ? d->buf_cnt : 1;
	//common render items
	for (auto i = 0; i < max; i++) {
		if (d->metaInfo->data.data2DMIP.exist) {
			auto is8Bit = d->metaInfo->data.data2DMIP.scalarType[0] == 1;
			auto volData2D = new SoVolumeData;
			d->volData2D.push_back(volData2D);
			volData2D->texturePrecision = is8Bit ? 8 : 16;
			volData2D->setName(("HTMIP" + std::to_string(i)).c_str());
			volData2D->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data2DMIP.tileSizeX, d->metaInfo->data.data2DMIP.tileSizeY, 1);
			//volData2D->ldmResourceParameters.getValue()->minTilesToLoad = 512;
			volData2D->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
			volData2D->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;

			if (d->metaInfo->data.isLDM) {
				auto ldm2DReader = new OivLdmReader(true, is8Bit);
				d->ldm2DReader.push_back(ldm2DReader);
			} else {
				auto reader2d = new OivHdf5Reader(false, is8Bit);
				d->reader2D.push_back(reader2d);
			}
		}
	}
	for (auto i = 0; i < max_fl; i++) {
		for (auto ch = 0; ch < 3; ch++) {
			if (d->metaInfo->data.data2DFLMIP.exist && d->metaInfo->data.data2DFLMIP.valid[ch]) {
				auto is8Bit = d->metaInfo->data.data2DFLMIP.scalarType[ch][0] == 1;
				auto flVolData2D = new SoVolumeData;
				d->flVolData2D[ch].push_back(flVolData2D);
				flVolData2D->texturePrecision = 8;
				std::string chName2d = "FL2D_CH_" + std::to_string(ch) + "_";
				flVolData2D->setName(SbName(chName2d + std::to_string(i)));
				flVolData2D->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data2DFLMIP.tileSizeX, d->metaInfo->data.data2DFLMIP.tileSizeY, 1);
				//flVolData2D->ldmResourceParameters.getValue()->minTilesToLoad = 512;
				flVolData2D->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
				flVolData2D->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;

				flVolData2D->dataSetId = ch + 1;
				if (d->metaInfo->data.isLDM) {
					auto ldm2DFLReader = new OivLdmReaderFL(ch, true, is8Bit);
					d->ldm2DFLReader[ch].push_back(ldm2DFLReader);
				} else {
					auto readerFL2d = new OivHdf5Reader(true, is8Bit);
					d->readerFL2D[ch].push_back(readerFL2d);
				}
			}
		}
	}
	for (auto bfi = 0; bfi < max_bf; bfi++) {
		if (d->metaInfo->data.dataBF.exist)
			d->cur_stepBF.push_back(bfi);
		for (int ch = 0; ch < 3; ch++) {
			SoVolumeData* bfVoldata = new SoVolumeData;
			d->bfVolData2D[ch].push_back(bfVoldata);
			bfVoldata->texturePrecision = 8;
			bfVoldata->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.dataBF.tileSizeX, d->metaInfo->data.dataBF.tileSizeY, 1);
			//bfVoldata->ldmResourceParameters.getValue()->minTilesToLoad = 512;
			bfVoldata->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
			bfVoldata->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;

			std::string chName = "BF_CH" + std::to_string(ch) + "_";
			bfVoldata->setName(SbName(chName + std::to_string(bfi)));
			bfVoldata->dataSetId = ch + 1;
			if (d->metaInfo->data.isLDM) {
				auto bfldmReader = new OivLdmReaderBF(true);
				d->ldm2DBFReader[ch].push_back(bfldmReader);
				bfldmReader->setDataGroupPath("/Data/BF");
				bfldmReader->setTileDimension(d->metaInfo->data.dataBF.tileSizeX, d->metaInfo->data.dataBF.tileSizeY, 1);
				bfldmReader->setColorChannel(ch);
			} else {
				auto bfreader = new OivColorReader;
				d->bfReader2D[ch].push_back(bfreader);
				bfreader->setDataGroupPath("/Data/BF/");
				bfreader->setTileDimension(d->metaInfo->data.dataBF.tileSizeX);
				bfreader->setColorChannel(ch);
			}
		}
	}
	if (d->xyOnly) {
		for (int i = 0; i < max; i++) {
			if (d->metaInfo->data.data2DMIP.exist || d->metaInfo->data.data3D.exist)
				d->cur_stepHT.push_back(i);
			if (d->metaInfo->data.data3D.exist && d->sliceWise) {
				auto is8Bit = d->metaInfo->data.data3D.scalarType[0] == 1;
				auto volDataXY = new SoVolumeData;
				d->volDataXY.push_back(volDataXY);
				volDataXY->texturePrecision = is8Bit ? 8 : 16;
				volDataXY->setName(("volData" + std::to_string(i)).c_str());
				volDataXY->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, 1);
				volDataXY->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
				volDataXY->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;

				//volDataXY->ldmResourceParameters.getValue()->minTilesToLoad = 512;
				if (d->metaInfo->data.isLDM) {
					auto sldmReaderXY = new OivLdmReaderXY(false, is8Bit);
					d->sldmReaderXY.push_back(sldmReaderXY);
					//auto ldmReaderXY = new OivLdmReader(false, is8Bit);
					//d->ldmReaderXY.push_back(ldmReaderXY);					
				} else {
					auto readerXY = new OivXYReader(false, is8Bit);
					d->readerXY.push_back(readerXY);
				}
			}
		}
		for (int i = 0; i < max_fl; i++) {
			if (d->metaInfo->data.data2DFLMIP.exist || d->metaInfo->data.data3DFL.exist) {
				d->cur_stepFL[0].push_back(i);
				d->cur_stepFL[1].push_back(i);
				d->cur_stepFL[2].push_back(i);
			}
			for (int ch = 0; ch < 3; ch++) {
				if (d->metaInfo->data.data3DFL.exist && d->metaInfo->data.data3DFL.valid[ch] && d->sliceWise) {
					auto is8Bit = d->metaInfo->data.data3DFL.scalarType[ch][0] == 1;
					auto flVolDataXY = new SoVolumeData;
					d->flVolDataXY[ch].push_back(flVolDataXY);
					flVolDataXY->texturePrecision = 8;
					flVolDataXY->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, 1);
					std::string chName = "FL_CH" + std::to_string(ch) + "_";
					flVolDataXY->setName(SbName(chName + std::to_string(i)));
					flVolDataXY->dataSetId = ch + 2;
					//flVolDataXY->ldmResourceParameters.getValue()->minTilesToLoad = 512;
					flVolDataXY->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
					flVolDataXY->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;

					if (d->metaInfo->data.isLDM) {
						auto sldmFLReaderXY = new OivLdmReaderXY(true, is8Bit);
						d->sldmFLReaderXY[ch].push_back(sldmFLReaderXY);
						//auto ldmFLReaderXY = new OivLdmReaderFL(ch,false,is8Bit);
						//d->ldmFLReaderXY[ch].push_back(ldmFLReaderXY);						
					} else {
						auto readerFLXY = new OivXYReader(true, is8Bit);
						d->readerFLXY[ch].push_back(readerFLXY);
					}
				}
			}
		}
		return;
	}
	for (int i = 0; i < max; i++) {
		if (d->metaInfo->data.data2DMIP.exist || d->metaInfo->data.data3D.exist)
			d->cur_stepHT.push_back(i);
		if (d->metaInfo->data.data3D.exist) {
			auto is8Bit = d->metaInfo->data.data3D.scalarType[0] == 1;
			auto volData = new SoVolumeData;
			d->volData.push_back(volData);
			volData->texturePrecision = is8Bit ? 8 : 16;
			volData->setName(("volData" + std::to_string(i)).c_str());
			volData->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ);
			if (d->sliceWise) {
				auto volDataXY = new SoVolumeData;
				d->volDataXY.push_back(volDataXY);
				volDataXY->texturePrecision = is8Bit ? 8 : 16;
				volDataXY->setName(("volData" + std::to_string(i)).c_str());
				volDataXY->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeY, 1);

				auto volDataYZ = new SoVolumeData;
				d->volDataYZ.push_back(volDataYZ);
				volDataYZ->texturePrecision = is8Bit ? 8 : 16;
				volDataYZ->setName(("volData" + std::to_string(i)).c_str());
				volDataYZ->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3D.tileSizeY, d->metaInfo->data.data3D.tileSizeZ, 1);

				auto volDataXZ = new SoVolumeData;
				d->volDataXZ.push_back(volDataXZ);
				volDataXZ->texturePrecision = is8Bit ? 8 : 16;
				volDataXZ->setName(("volData" + std::to_string(i)).c_str());
				volDataXZ->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3D.tileSizeX, d->metaInfo->data.data3D.tileSizeZ, 1);

				if (d->metaInfo->data.isLDM) {
					//volDataYZ->ldmResourceParameters.getValue()->minTilesToLoad = 512;
					//volDataXY->ldmResourceParameters.getValue()->minTilesToLoad = 512;
					//volDataXZ->ldmResourceParameters.getValue()->minTilesToLoad = 512;
					volDataXY->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;
					volDataYZ->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;
					volDataXZ->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;
					volDataXY->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
					volDataYZ->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
					volDataXZ->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
				}
			}
			if (d->metaInfo->data.isLDM) {
				//for LDM
				auto ldmReader = new OivLdmReader(false, is8Bit);
				d->ldmReader.push_back(ldmReader);
				if (d->sliceWise) {
					auto sldmReaderXY = new OivLdmReaderXY(false, is8Bit);
					d->sldmReaderXY.push_back(sldmReaderXY);
					//auto ldmReaderXY = new OivLdmReader(false, is8Bit);
					//d->ldmReaderXY.push_back(ldmReaderXY);

					//auto sldmReaderYZ = new OivLdmReaderYZ(false, is8Bit);
					//d->sldmReaderYZ.push_back(sldmReaderYZ);
					auto ldmReaderYZ = new OivLdmReader(false, is8Bit);
					d->ldmReaderYZ.push_back(ldmReaderYZ);

					//auto sldmReaderXZ = new OivLdmReaderXZ(false, is8Bit);
					//d->sldmReaderXZ.push_back(sldmReaderXZ);
					auto ldmReaderXZ = new OivLdmReader(false, is8Bit);
					d->ldmReaderXZ.push_back(ldmReaderXZ);
				}
			} else {
				auto reader = new OivHdf5Reader(false, is8Bit);
				d->reader.push_back(reader);
				if (d->sliceWise) {
					auto readerXY = new OivXYReader(false, is8Bit);
					d->readerXY.push_back(readerXY);

					auto readerYZ = new OivYZReader(false, is8Bit);
					d->readerYZ.push_back(readerYZ);

					auto readerXZ = new OivXZReader(false, is8Bit);
					d->readerXZ.push_back(readerXZ);
				}
			}
		}
	}
	for (int i = 0; i < max_fl; i++) {
		if (d->metaInfo->data.data2DFLMIP.exist || d->metaInfo->data.data3DFL.exist) {
			d->cur_stepFL[0].push_back(i);
			d->cur_stepFL[1].push_back(i);
			d->cur_stepFL[2].push_back(i);
		}
		for (int ch = 0; ch < 3; ch++) {
			if (d->metaInfo->data.data3DFL.exist && d->metaInfo->data.data3DFL.valid[ch]) {
				auto is8Bit = d->metaInfo->data.data3DFL.scalarType[ch][0] == 1;
				auto flVolData = new SoVolumeData;
				d->flVolData[ch].push_back(flVolData);
				flVolData->texturePrecision = 8;
				flVolData->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ);
				std::string chName = "FL_CH" + std::to_string(ch) + "_";
				flVolData->setName(SbName(chName + std::to_string(i)));
				flVolData->dataSetId = ch + 1;
				if (d->sliceWise) {
					auto flVolDataXY = new SoVolumeData;
					d->flVolDataXY[ch].push_back(flVolDataXY);
					flVolDataXY->texturePrecision = 8;
					flVolDataXY->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeY, 1);
					std::string chName = "FL_CH" + std::to_string(ch) + "_";
					flVolDataXY->setName(SbName(chName + std::to_string(i)));
					flVolDataXY->dataSetId = ch + 2;

					auto flVolDataYZ = new SoVolumeData;
					d->flVolDataYZ[ch].push_back(flVolDataYZ);
					flVolDataYZ->texturePrecision = 8;
					flVolDataYZ->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3DFL.tileSizeY, d->metaInfo->data.data3DFL.tileSizeZ, 1);
					flVolDataYZ->setName(SbName(chName + std::to_string(i)));
					flVolDataYZ->dataSetId = ch + 2;

					auto flVolDataXZ = new SoVolumeData;
					d->flVolDataXZ[ch].push_back(flVolDataXZ);
					flVolDataXZ->texturePrecision = 8;
					flVolDataXZ->ldmResourceParameters.getValue()->tileDimension.setValue(d->metaInfo->data.data3DFL.tileSizeX, d->metaInfo->data.data3DFL.tileSizeZ, 1);
					flVolDataXZ->setName(SbName(chName + std::to_string(i)));
					flVolDataXZ->dataSetId = ch + 2;
					if (d->metaInfo->data.isLDM) {
						//flVolDataXY->ldmResourceParameters.getValue()->minTilesToLoad = 512;
						//flVolDataYZ->ldmResourceParameters.getValue()->minTilesToLoad = 512;
						//flVolDataXZ->ldmResourceParameters.getValue()->minTilesToLoad = 512;
						flVolDataXY->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;
						flVolDataYZ->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;
						flVolDataXZ->ldmResourceParameters.getValue()->screenResolutionCullingPolicy = SoLDMResourceParameters::SCREEN_RESOLUTION_CULLING_ON;
						flVolDataXY->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
						flVolDataYZ->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
						flVolDataXZ->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::LoadPolicy::ALWAYS;
					}
				}
				if (d->metaInfo->data.isLDM) {
					auto ldmFLReader = new OivLdmReaderFL(ch, false, is8Bit);
					ldmFLReader->setFLOffset(d->fl_offset);
					d->ldmFLReader[ch].push_back(ldmFLReader);
					if (d->sliceWise) {
						auto sldmFLReaderXY = new OivLdmReaderXY(true, is8Bit);
						d->sldmFLReaderXY[ch].push_back(sldmFLReaderXY);
						//auto ldmFLReaderXY = new OivLdmReaderFL(ch, false , is8Bit);
						//d->ldmFLReaderXY[ch].push_back(ldmFLReaderXY);						

						//auto sldmFLReaderYZ = new OivLdmReaderYZ(true, is8Bit);
						//d->sldmFLReaderYZ[ch].push_back(sldmFLReaderYZ);
						auto ldmFLReaderYZ = new OivLdmReaderFL(ch, false, is8Bit);
						ldmFLReaderYZ->setFLOffset(d->fl_offset);
						d->ldmFLReaderYZ[ch].push_back(ldmFLReaderYZ);

						//auto sldmFLReaderXZ = new OivLdmReaderXZ(true, is8Bit);
						//d->sldmFLReaderXZ[ch].push_back(sldmFLReaderXZ);
						auto ldmFLReaderXZ = new OivLdmReaderFL(ch, false, is8Bit);
						ldmFLReaderXZ->setFLOffset(d->fl_offset);
						d->ldmFLReaderXZ[ch].push_back(ldmFLReaderXZ);
					}
				} else {
					auto readerFL = new OivHdf5Reader(true, is8Bit);
					readerFL->setZOffset(d->fl_offset);
					d->readerFL[ch].push_back(readerFL);
					if (d->sliceWise) {
						auto readerFLXY = new OivXYReader(true, is8Bit);
						d->readerFLXY[ch].push_back(readerFLXY);

						auto readerFLYZ = new OivYZReader(true, is8Bit);
						readerFLYZ->setOffsetZ(d->fl_offset);
						d->readerFLYZ[ch].push_back(readerFLYZ);

						auto readerFLXZ = new OivXZReader(true, is8Bit);
						readerFLXZ->setOffsetZ(d->fl_offset);
						d->readerFLXZ[ch].push_back(readerFLXZ);
					}
				}
			}
		}
	}
}

auto OivBufferManager::getNumberOfBuffer(void) -> int {
	return d->buf_cnt;
}

auto OivBufferManager::setNumberOfBuffer(int n) -> void {
	d->buf_cnt = n;
}

auto OivBufferManager::setHTSliceIndexX(int index) -> void {
	d->htIdxX = index;
	if (d->metaInfo->data.isLDM) {
		/*for (auto slice : d->sldmReaderYZ) {
			slice->setIndex(index);
			slice->touch();
		}*/
	} else {
		for (auto slice : d->readerYZ) {
			slice->setIndex(index);
			slice->touch();
		}
	}
}

auto OivBufferManager::getHTSliceIndexX() -> int {
	return d->htIdxX;
}

auto OivBufferManager::setHTSliceIndexY(int index) -> void {
	d->htIdxY = index;
	if (d->metaInfo->data.isLDM) {
		/*for (auto slice : d->sldmReaderXZ) {
			slice->setIndex(index);
			slice->touch();
		}*/
	} else {
		for (auto slice : d->readerXZ) {
			slice->setIndex(index);
			slice->touch();
		}
	}
}

auto OivBufferManager::getHTSliceIndexY() -> int {
	return d->htIdxY;
}

auto OivBufferManager::setHTSliceIndexZ(int index) -> void {
	d->htIdxZ = index;
	if (d->metaInfo->data.isLDM) {
		for (auto slice : d->sldmReaderXY) {
			slice->setIndex(index);
			slice->touch();
		}
	} else {
		for (auto slice : d->readerXY) {
			slice->setIndex(index);
			slice->touch();
		}
	}
}

auto OivBufferManager::getHTSliceIndexZ() -> int {
	return d->htIdxZ;
}

auto OivBufferManager::setFLSliceIndexX(int index) -> void {
	d->flIdxX = index;
	for (auto ch = 0; ch < 3; ch++) {
		if (d->metaInfo->data.data3DFL.valid[ch]) {
			if (d->metaInfo->data.isLDM) {
				/*for (auto slice : d->sldmFLReaderYZ[ch]) {
					slice->setIndex(index);
					slice->touch();
				}*/
			} else {
				for (auto slice : d->readerFLYZ[ch]) {
					slice->setIndex(index);
					slice->touch();
				}
			}
		}
	}
}

auto OivBufferManager::getFLSliceIndexX() -> int {
	return d->flIdxX;
}

auto OivBufferManager::setFLSliceIndexY(int index) -> void {
	d->flIdxY = index;
	for (auto ch = 0; ch < 3; ch++) {
		if (d->metaInfo->data.data3DFL.valid[ch]) {
			if (d->metaInfo->data.isLDM) {
				/*for (auto slice : d->sldmFLReaderXZ[ch]) {
					slice->setIndex(index);
					slice->touch();
				}*/
			} else {
				for (auto slice : d->readerFLXZ[ch]) {
					slice->setIndex(index);
					slice->touch();
				}
			}
		}
	}
}

auto OivBufferManager::getFLSliceIndexY() -> int {
	return d->flIdxY;
}

auto OivBufferManager::setFLSliceIndexZ(int index) -> void {
	d->flIdxZ = index;
	for (auto ch = 0; ch < 3; ch++) {
		if (d->metaInfo->data.data3DFL.valid[ch]) {
			if (d->metaInfo->data.isLDM) {
				for (auto slice : d->sldmFLReaderXY[ch]) {
					slice->setIndex(index);
					slice->touch();
				}
			} else {
				for (auto slice : d->readerFLXY[ch]) {
					slice->setIndex(index);
					slice->touch();
				}
			}
		}
	}
}

auto OivBufferManager::getFLSliceIndexZ() -> int {
	return d->flIdxZ;
}
