#pragma once

#include <QString>

#pragma warning(push)
#pragma warning(disable:4268)
#include <H5Cpp.h>
#pragma warning(pop)

#include <TCFMetaReader.h>
#include "TC.IO.OIV.ManagerExport.h"

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

using namespace H5;

class TC_IO_OIV_Manager_API OivBufferManager {
public:
	OivBufferManager(bool sliceWise = false, bool xyOnly = false);
	~OivBufferManager();

	auto initialize(void) -> void;
	auto setNumberOfBuffer(int n) -> void;
	auto getNumberOfBuffer(void) -> int;

	auto setNumberOfImage(int n) -> void;
	auto setNumberOfFLImage(int n) -> void;
	auto setNumberOfBFImage(int n) -> void;
	auto setMetaInfo(TC::IO::TCFMetaReader::Meta::Pointer metaInfo) -> void;
	auto setDefaultReader(void) -> void;
	auto setXYCropOffset(int offsetX, int offsetY) -> void;
	auto setXYCropSize(int sizeX, int sizeY) -> void;
	auto setXYCropOffsetFL(int offsetX, int offsetY) -> void;
	auto setXYCropSizeFL(int sizeX, int sizeY) -> void;

	//LDM properties        
	auto setFixedResolution(int res,bool forVolume) -> void;
	auto getFixedResolution(void) -> int;
	auto enableFixedResolution(bool isFixedRes, bool forVolume) -> void;
	auto isFixedResolutionEnabled(bool forVolume) -> bool;
	auto setFilePath(QString path) -> void;
	auto setLowerResThreshold(int res) -> void;
	auto getLowerResThreshold(void) -> int;
	auto setUpperResThreshold(int res) -> void;
	auto getUpperResThreshold(void) -> int;
	auto setMainMemory(int mem) -> void;
	auto getMainMemory(void) -> int;
	auto setTex3DMemory(int mem) -> void;
	auto getTex3DMemory(void) -> int;
	auto setTex2DMemory(int mem) -> void;
	auto getTex2DMemory(void) -> int;

	//for time stepping
	auto setTimeStepHT(int t) -> void;
	auto setTimeStepFL(int t, int ch) -> void;
	auto setTimeStepBF(int t) -> void;
	auto setHTSliceIndexX(int index) -> void;
	auto getHTSliceIndexX() -> int;
	auto setHTSliceIndexY(int index) -> void;
	auto getHTSliceIndexY() -> int;
	auto setHTSliceIndexZ(int index) -> void;
	auto getHTSliceIndexZ() -> int;
	auto setFLSliceIndexX(int index) -> void;
	auto getFLSliceIndexX() -> int;
	auto setFLSliceIndexY(int index) -> void;
	auto getFLSliceIndexY() -> int;
	auto setFLSliceIndexZ(int index) -> void;
	auto getFLSliceIndexZ() -> int;

	auto prepareTimeHT(int t) -> void;

	auto setPlayStepHT(int t) -> void;
	auto setPlayStemFL(int t) -> void;
	auto setPlayStemBF(int t) -> void;

	//volumd data aquisition
	auto getHT3dVolume(int step) -> SoVolumeData*;
	auto getHTXYSlice(int step) -> SoVolumeData*;
	auto getHTYZSlice(int step) -> SoVolumeData*;
	auto getHTXZSlice(int step) -> SoVolumeData*;
	auto getHT2dVolume(int step) -> SoVolumeData*;
	auto getFL3dVolume(int step, int ch) -> SoVolumeData*;
	auto getFLXYSlice(int step, int ch) -> SoVolumeData*;
	auto getFLYZSlice(int step, int ch) -> SoVolumeData*;
	auto getFLXZSlice(int step, int ch) -> SoVolumeData*;
	auto getFL2dVolume(int step, int ch) -> SoVolumeData*;
	auto getBF2dVolume(int step, int ch) -> SoVolumeData*;
	auto getDefaultVolume() -> SoVolumeData*;

	auto ClearMemory() -> void;

private:
	auto connectHT3dReader(int time_step, bool init = false) -> bool;
	auto connectHT2dReader(int time_step, bool init = false) -> bool;
	auto connectFL3dReader(int time_step, int ch, bool init = false) -> bool;
	auto connectFL2dReader(int time_step, int ch, bool init = false) -> bool;
	auto connectBF2dReader(int time_step, int ch, bool init = false) -> bool;

	auto connectHTXYReader(int time_step, bool init = false) -> bool;
	auto connectHTYZReader(int time_step, bool init = false) -> bool;
	auto connectHTXZReader(int time_step, bool init = false) -> bool;

	auto connectFLXYReader(int time_step, int ch, bool init = false) -> bool;
	auto connectFLYZReader(int time_step, int ch, bool init = false) -> bool;
	auto connectFLXZReader(int time_step, int ch, bool init = false) -> bool;

	auto readFLOffset() -> float;
	auto convertFL(int step, int ch) -> void;

	struct Impl;
	std::unique_ptr<Impl> d;
};
