#pragma once

#include <memory>
#include <optional>

#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API WorksetHasItemMetadata : public IJsonEntity{
	public:
		WorksetHasItemMetadata();
		WorksetHasItemMetadata(WorksetHasItemMetadata&&) noexcept;
		WorksetHasItemMetadata(const WorksetHasItemMetadata&);
		WorksetHasItemMetadata& operator=(const WorksetHasItemMetadata&);
		WorksetHasItemMetadata& operator=(WorksetHasItemMetadata&&) noexcept;
		bool operator==(const WorksetHasItemMetadata&) const;
		~WorksetHasItemMetadata() override;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}