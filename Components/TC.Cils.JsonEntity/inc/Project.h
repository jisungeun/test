#pragma once

#include <memory>

#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API Project : public IJsonEntity{
	public:
		Project();
		Project(Project&&) noexcept;
		Project(const Project&);
		Project& operator=(const Project&);
		Project& operator=(Project&&) noexcept;
		bool operator==(const Project&) const;
		~Project() override;

		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetName() const -> const QString&;
		[[nodiscard]] auto GetType() const -> const QString&;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}