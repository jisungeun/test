#pragma once

#include <memory>
#include <optional>

#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API User : public IJsonEntity{
	public:
		User();
		User(User&&) noexcept;
		User(const User&);
		User& operator =(const User&);
		User& operator =(User&&) noexcept;
		bool operator ==(const User&) const;
		~User() override;

		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetUsername() const -> const std::optional<QString>&;
		[[nodiscard]] auto GetName() const -> const std::optional<QString>&;

	protected:
        auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}