#pragma once

#include <memory>
#include <optional>

#include "IJsonEntity.h"
#include "Item.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API ItemExecutionResult : public IJsonEntity{
	public:
		ItemExecutionResult();
		ItemExecutionResult(ItemExecutionResult&&) noexcept;
		ItemExecutionResult(const ItemExecutionResult&);
		ItemExecutionResult& operator=(const ItemExecutionResult&);
		ItemExecutionResult& operator=(ItemExecutionResult&&) noexcept;
		bool operator==(const ItemExecutionResult&) const;
		~ItemExecutionResult() override;

		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetItem() const -> const Item&;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}