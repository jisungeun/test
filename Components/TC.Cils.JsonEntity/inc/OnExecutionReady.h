#pragma once

#include <memory>
#include <optional>

#include "IJsonEntity.h"
#include "ItemExecution.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API OnExecutionReady : public IJsonEntity {
	public:
		OnExecutionReady();
		OnExecutionReady(OnExecutionReady&&) noexcept;
		OnExecutionReady(const OnExecutionReady&);
		OnExecutionReady& operator=(const OnExecutionReady&);
		OnExecutionReady& operator=(OnExecutionReady&&) noexcept;
		bool operator==(const OnExecutionReady&) const;
		~OnExecutionReady() override;

		[[nodiscard]] auto GetItemExecution() const -> const ItemExecution&;
		[[nodiscard]] auto GetFilePath() const -> const QString&;
		[[nodiscard]] auto GetResult() const -> const std::optional<ItemExecutionResult>&;
		[[nodiscard]] auto GetResultPath() const -> const std::optional<QString>&;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
		[[nodiscard]] auto GetObjectNames() const->QStringList override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}