#pragma once

#include <memory>

#include "IJsonEntity.h"
#include "ItemProgress.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API SyncExecution : public IJsonEntity {
	public:
		SyncExecution();
		SyncExecution(SyncExecution&&) noexcept;
		SyncExecution(const SyncExecution&);
		SyncExecution& operator=(const SyncExecution&);
		SyncExecution& operator=(SyncExecution&&) noexcept;
		bool operator==(const SyncExecution&) const;
		~SyncExecution() override;

		[[nodiscard]] auto GetInfoId() const -> const QString&;
		[[nodiscard]] auto IsWorkset() const -> bool;
		[[nodiscard]] auto GetName() const->const QString&;
		[[nodiscard]] auto GetProgress() const -> const QVector<ItemProgress>&;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
		[[nodiscard]] auto GetObjectNames() const->QStringList override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}