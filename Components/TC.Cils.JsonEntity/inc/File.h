#pragma once

#include <memory>
#include <optional>

#include "IJsonEntity.h"
#include "CilsFile.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API File : public IJsonEntity{
	public:
		File();
		File(File&&) noexcept;
		File(const File&);
		File& operator=(const File&);
		File& operator=(File&&) noexcept;
		bool operator==(const File&) const;
		~File() override;
		
		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetFile() const -> const CilsFile&;
		[[nodiscard]] auto GetPath() const -> const QString&;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}