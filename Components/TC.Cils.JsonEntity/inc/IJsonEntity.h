#pragma once

#include <memory>
#include <optional>

#include <QJsonObject>

#include "TCCilsJsonEntityExport.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API IJsonEntity {
	public:
		IJsonEntity();
		IJsonEntity(IJsonEntity&&) noexcept;
		IJsonEntity(const IJsonEntity&);
		IJsonEntity& operator =(const IJsonEntity&);
		IJsonEntity& operator =(IJsonEntity&&) noexcept;
		virtual ~IJsonEntity();
		
        virtual auto Deserialize(const QJsonObject&) -> bool;

        [[nodiscard]] auto IsInitialized() const -> bool;

	protected:
		virtual auto DeserializeThis(const QJsonObject&) -> void = 0;
        [[nodiscard]] virtual auto GetObjectNames() const->QStringList = 0;

		template<typename T>
		static auto DeserializeType(const QJsonObject& obj) -> T {
			T t;
			t.Deserialize(obj);
			return t;
		}

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
