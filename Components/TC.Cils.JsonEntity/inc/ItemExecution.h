#pragma once

#include <memory>
#include <optional>

#include <QVector>

#include "IJsonEntity.h"
#include "Item.h"
#include "User.h"
#include "Workset.h"
#include "ItemExecutionResult.h"

namespace TC::Cils::JsonEntity {
	enum class TCCilsJsonEntity_API ExecutionStatus {
		Any,
		NotStarted,
		Executing,
		Executed,
		UploadFailed
	};

	class TCCilsJsonEntity_API ItemExecution : public IJsonEntity{
	public:
		ItemExecution();
		ItemExecution(ItemExecution&&) noexcept;
		ItemExecution(const ItemExecution&);
		ItemExecution& operator=(const ItemExecution&);
		ItemExecution& operator=(ItemExecution&&) noexcept;
		bool operator==(const ItemExecution&) const;
		~ItemExecution() override;

		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetItemId() const -> int;
		[[nodiscard]] auto GetDataId() const -> const QString&;
		[[nodiscard]] auto GetTitle() const -> const std::optional<QString>&;
		[[nodiscard]] auto GetItem() const -> const Item&;
		[[nodiscard]] auto GetLocker() const -> const std::optional<User>&;
		[[nodiscard]] auto GetAssignee() const -> const std::optional<User>&;
		[[nodiscard]] auto GetAssignStatus() const->ExecutionStatus;
		[[nodiscard]] auto GetReviewer() const -> const std::optional<User>&;
		[[nodiscard]] auto GetReviewStatus() const->ExecutionStatus;
		[[nodiscard]] auto GetWorksets() const -> const QVector<Workset>&;
		[[nodiscard]] auto GetProjectId() const -> int;
		[[nodiscard]] auto IsAvailable() const -> bool;
		[[nodiscard]] auto GetResult() const -> const std::optional<ItemExecutionResult>&;

		[[nodiscard]] static auto ExecutionStatusToString(ExecutionStatus status)->QString;
		[[nodiscard]] static auto StringToExecutionStatus(const QString& status)->ExecutionStatus;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}