#pragma once

#include <memory>
#include <optional>

#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	enum class TCCilsJsonEntity_API ProgressStatus {
		Pending,
		Paused,
		InProgress,
		Merging,
		Finished,
		Deleted
	};

	class TCCilsJsonEntity_API ItemProgress : public IJsonEntity{
	public:
		ItemProgress();
		ItemProgress(ItemProgress&&) noexcept;
		ItemProgress(const ItemProgress&);
		ItemProgress& operator=(const ItemProgress&);
		ItemProgress& operator=(ItemProgress&&) noexcept;
		bool operator==(const ItemProgress&) const;
		~ItemProgress() override;

		[[nodiscard]] auto GetInfoId() const -> const QString&;
		[[nodiscard]] auto GetDataId() const -> const QString&;
		[[nodiscard]] auto GetTitle() const -> const QString&;
		[[nodiscard]] auto GetProgressStatus() const->ProgressStatus;
		[[nodiscard]] auto GetPercentage() const -> int;
		[[nodiscard]] auto GetItemPath() const -> const QString&;
		[[nodiscard]] auto GetFinishedSize() const -> const QString&;
		[[nodiscard]] auto GetSize() const -> const QString&;

		static auto ProgressStatusToString(ProgressStatus status)->QString;
		static auto StringToProgressStatus(const QString& status)->ProgressStatus;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
		[[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}