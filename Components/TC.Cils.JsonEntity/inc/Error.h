#pragma once

#include <memory>

#include <QString>

#include "TCCilsJsonEntityExport.h"
#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API Error : public IJsonEntity {
	public:
		Error();
		Error(Error&&) noexcept;
		Error(const Error&);
		Error& operator=(const Error&);
		Error& operator=(Error&&) noexcept;
		bool operator==(const Error&) const;
		~Error() override;

		[[nodiscard]] auto GetStatus() const -> int;
		[[nodiscard]] auto GetCode() const -> int;
		[[nodiscard]] auto GetMessage() const->const QString&;
		
    protected:
        auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}