#pragma once

#include <memory>
#include <optional>

#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API CilsFile : public IJsonEntity {
	public:
		CilsFile();
		explicit CilsFile(QByteArray&& json);
		CilsFile(CilsFile&&) noexcept;
		CilsFile(const CilsFile&);
		CilsFile& operator=(const CilsFile&);
		CilsFile& operator=(CilsFile&&) noexcept;
		bool operator==(const CilsFile&) const;
		~CilsFile() override;

        [[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetHash() const->const QString&;
		[[nodiscard]] auto GetSize() const -> const QString&;
		[[nodiscard]] auto GetAvailable() const -> int;
		[[nodiscard]] auto GetBelongToDataId() const->QString;
		
	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}