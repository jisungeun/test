#pragma once

#include <memory>
#include <optional>

#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API Item : public IJsonEntity{
	public:
		Item();
		Item(Item&&) noexcept;
		Item(const Item&);
		Item& operator=(const Item&);
		Item& operator=(Item&&) noexcept;
		bool operator==(const Item&) const;
		~Item() override;

		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetDataId() const -> const QString&;
		[[nodiscard]] auto GetTitle() const -> const std::optional<QString>&;
		[[nodiscard]] auto GetOrganizationId() const -> int;
		[[nodiscard]] auto GetImageUrl() const->const QString&;
		[[nodiscard]] auto GetSize() const->unsigned long long;

	protected:
        auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}