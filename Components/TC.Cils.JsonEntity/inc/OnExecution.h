#pragma once

#include <memory>

#include "IJsonEntity.h"
#include "ItemExecution.h"

enum class TCCilsJsonEntity_API ItemStatus {
	Unknown,
	Queued,
	RequestingAgentDownload,
	AgentDownloadQueued,
	DownloadStarted,
	DownloadComplete,
	ExecutionStarted,
	ExecutionComplete,
	OutputUploadStarted,
	GenerateUploadItemInfo,
	OutputUploadQueue,
	OutputUploadComplete,
	FinishExecution,
	AutoDeleteQueue
};

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API OnExecution : public IJsonEntity {
	public:
		OnExecution();
		OnExecution(OnExecution&&) noexcept;
		OnExecution(const OnExecution&);
		OnExecution& operator=(const OnExecution&);
		OnExecution& operator=(OnExecution&&) noexcept;
		bool operator==(const OnExecution&) const;
		~OnExecution() override;

		[[nodiscard]] auto GetItemExecution() const -> const ItemExecution&;
		[[nodiscard]] auto GetTcfStatus() const -> const ItemStatus&;
		[[nodiscard]] auto GetTcfPath() const -> const QString&;
		[[nodiscard]] auto GetOutputPath() const -> const std::optional<QString>&;
		[[nodiscard]] auto GetOutputStatus() const -> const std::optional<ItemStatus>&;
		[[nodiscard]] auto GetOutputResult() const -> const std::optional<ItemExecutionResult>&;

		[[nodiscard]] static auto StringToItemStatus(const QString& status)->ItemStatus;

	protected:
		auto DeserializeThis(const QJsonObject&) -> void override;
		[[nodiscard]] auto GetObjectNames() const->QStringList override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}