#pragma once

#include <memory>

#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	class TCCilsJsonEntity_API Workset : public IJsonEntity{
	public:
		Workset();
		Workset(Workset&&) noexcept;
		Workset(const Workset&);
		Workset& operator =(const Workset&);
		Workset& operator =(Workset&&) noexcept;
		bool operator ==(const Workset&) const;
		~Workset() override;

		[[nodiscard]] auto GetId() const -> int;
		[[nodiscard]] auto GetName() const -> const QString&;

	protected:
        auto DeserializeThis(const QJsonObject&) -> void override;
        [[nodiscard]] auto GetObjectNames() const -> QStringList override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}