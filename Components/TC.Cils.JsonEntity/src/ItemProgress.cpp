#include "ItemProgress.h"

namespace TC::Cils::JsonEntity {
	struct ItemProgress::Impl {
		QString infoId;
		QString dataId;
		QString title;
		ProgressStatus status = ProgressStatus::Deleted;
		int percentage = -1;
		QString itemPath;
		QString finishedSize;
		QString size;
	};

	ItemProgress::ItemProgress() : IJsonEntity(), d(new Impl) {}

	ItemProgress::ItemProgress(ItemProgress&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	ItemProgress::ItemProgress(const ItemProgress& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	ItemProgress& ItemProgress::operator=(const ItemProgress& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	ItemProgress& ItemProgress::operator=(ItemProgress&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool ItemProgress::operator==(const ItemProgress& obj) const {
		return (d->infoId == obj.d->infoId);
	}

	ItemProgress::~ItemProgress() = default;

	auto ItemProgress::GetInfoId() const -> const QString& {
		return d->infoId;
	}

	auto ItemProgress::GetDataId() const -> const QString& {
		return d->dataId;
	}

	auto ItemProgress::GetTitle() const -> const QString& {
		return d->title;
	}

	auto ItemProgress::GetProgressStatus() const -> ProgressStatus {
		return d->status;
	}

	auto ItemProgress::GetPercentage() const -> int {
		return d->percentage;
	}

	auto ItemProgress::GetItemPath() const -> const QString& {
		return d->itemPath;
	}

	auto ItemProgress::GetFinishedSize() const -> const QString& {
		return d->finishedSize;
	}

	auto ItemProgress::GetSize() const -> const QString& {
		return d->size;
	}

	auto ItemProgress::ProgressStatusToString(ProgressStatus status) -> QString {
		switch (status) {
            case ProgressStatus::Pending:
				return "Pending";
            case ProgressStatus::Paused:
				return "Paused";
            case ProgressStatus::InProgress:
				return "InProgress";
            case ProgressStatus::Merging:
				return "Merging";
            case ProgressStatus::Finished:
				return "Finished";
            case ProgressStatus::Deleted:
				return "Deleted";
        }
		return "Deleted";
	}

	auto ItemProgress::StringToProgressStatus(const QString& status) -> ProgressStatus {
		if (status == "Pending")
			return ProgressStatus::Pending;
		else if (status == "Paused")
			return ProgressStatus::Paused;
		else if (status == "InProgress")
			return ProgressStatus::InProgress;
		else if (status == "Merging")
			return ProgressStatus::Merging;
		else if (status == "Finished")
			return ProgressStatus::Finished;
		else
			return ProgressStatus::Deleted;
	}

	auto ItemProgress::DeserializeThis(const QJsonObject& obj) -> void {
		d->infoId = obj["infoId"].toString();
		d->dataId = obj["dataId"].toString();
		d->title = obj["title"].toString();
		d->status = StringToProgressStatus(obj["progressStatus"].toString());
		d->percentage = obj["percentage"].toInt();
		d->itemPath = obj["itemPath"].toString();
		d->finishedSize = obj["finishedSize"].toString();
		d->size = obj["size"].toString();
	}

	auto ItemProgress::GetObjectNames() const -> QStringList {
		return { "infoId", "dataId", "title", "progressStatus", "percentage", "itemPath", "finishedSize", "size" };
	}
}
