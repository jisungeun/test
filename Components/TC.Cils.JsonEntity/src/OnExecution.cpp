#include <QJsonDocument>

#include "OnExecution.h"

namespace TC::Cils::JsonEntity {
	struct OnExecution::Impl {
		ItemExecution ie;
		ItemStatus tcfStatus;
		QString tcfPath;
		std::optional<ItemStatus> outputStatus;
		std::optional<QString> outputPath;
		std::optional<ItemExecutionResult> outputResult;
	};

	OnExecution::OnExecution() : IJsonEntity(), d(new Impl) {}

	OnExecution::OnExecution(OnExecution&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	OnExecution::OnExecution(const OnExecution& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	OnExecution& OnExecution::operator=(const OnExecution& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	OnExecution& OnExecution::operator=(OnExecution&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool OnExecution::operator==(const OnExecution& obj) const {
		return (d->ie == obj.d->ie);
	}

	OnExecution::~OnExecution() = default;

	auto OnExecution::GetItemExecution() const -> const ItemExecution& {
		return d->ie;
	}

	auto OnExecution::GetTcfStatus() const -> const ItemStatus& {
		return d->tcfStatus;
	}

	auto OnExecution::GetTcfPath() const -> const QString& {
		return d->tcfPath;
	}

	auto OnExecution::GetOutputPath() const -> const std::optional<QString>& {
		return d->outputPath;
	}

	auto OnExecution::GetOutputStatus() const -> const std::optional<ItemStatus>& {
		return d->outputStatus;
	}

	auto OnExecution::GetOutputResult() const -> const std::optional<ItemExecutionResult>& {
		return d->outputResult;
	}

	auto OnExecution::StringToItemStatus(const QString& status) -> ItemStatus {
		if (status == "QUEUED") {
			return ItemStatus::Queued;
		} else if (status == "REQUESTING_AGENT_DOWNLOAD") {
			return ItemStatus::RequestingAgentDownload;
		} else if (status == "AGENT_DOWNLOAD_QUEUED") {
			return ItemStatus::AgentDownloadQueued;
		} else if (status == "DOWNLOAD_STARTED") {
			return ItemStatus::DownloadStarted;
		} else if (status == "DOWNLOAD_COMPLETE") {
			return ItemStatus::DownloadComplete;
		} else if (status == "EXECUTION_STARTED") {
			return ItemStatus::ExecutionStarted;
		} else if (status == "EXECUTION_COMPLETE") {
			return ItemStatus::ExecutionComplete;
		} else if (status == "OUTPUT_UPLOAD_STARTED") {
			return ItemStatus::OutputUploadStarted;
		} else if (status == "GENERATE_UPLOAD_ITEM_INFO") {
			return ItemStatus::GenerateUploadItemInfo;
		} else if (status == "OUTPUT_UPLOAD_QUEUE") {
			return ItemStatus::OutputUploadQueue;
		} else if (status == "OUTPUT_UPLOAD_COMPLETE") {
			return ItemStatus::OutputUploadComplete;
		} else if (status == "FINISH_EXECUTION") {
			return ItemStatus::FinishExecution;
		} else if (status == "AUTO_DELETE_QUEUE") {
			return ItemStatus::AutoDeleteQueue;
		}
		return ItemStatus::Unknown;
	}


	auto OnExecution::DeserializeThis(const QJsonObject& obj) -> void {
		d->ie = DeserializeType<ItemExecution>(obj["itemExecution"].toObject());
		d->tcfStatus = StringToItemStatus(obj["status"].toString());
		d->tcfPath = obj["path"].toString();

		if (!obj["latestResult"].isNull()) {
			const auto latestResult = obj["latestResult"].toObject();

			d->outputPath = latestResult["path"].toString();
			d->outputStatus = StringToItemStatus(latestResult["status"].toString());
			d->outputResult = DeserializeType<ItemExecutionResult>(latestResult["result"].toObject());
		}
	}

	auto OnExecution::GetObjectNames() const -> QStringList {
		return { "itemExecution", "path", "latestResult" };
	}
}
