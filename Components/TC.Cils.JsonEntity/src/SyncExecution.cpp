#include <QJsonArray>

#include "SyncExecution.h"

namespace TC::Cils::JsonEntity {
	struct SyncExecution::Impl {
		QString infoId;
		bool isWorkset;
		QString name;
		QVector<ItemProgress> progress;
	};

	SyncExecution::SyncExecution() : IJsonEntity(), d(new Impl) {}

	SyncExecution::SyncExecution(SyncExecution&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	SyncExecution::SyncExecution(const SyncExecution& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	SyncExecution& SyncExecution::operator=(const SyncExecution& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	SyncExecution& SyncExecution::operator=(SyncExecution&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool SyncExecution::operator==(const SyncExecution& obj) const {
		return (d->infoId == obj.d->infoId) && (d->progress == obj.d->progress) && (d->name == obj.d->name);
	}

	SyncExecution::~SyncExecution() = default;

	auto SyncExecution::GetInfoId() const -> const QString& {
		return d->infoId;
	}

	auto SyncExecution::IsWorkset() const -> bool {
		return d->isWorkset;
	}

	auto SyncExecution::GetName() const -> const QString& {
		return d->name;
	}

	auto SyncExecution::GetProgress() const -> const QVector<ItemProgress>& {
		return d->progress;
	}

	auto SyncExecution::DeserializeThis(const QJsonObject& obj) -> void {
		d->infoId = obj["infoId"].toString();
		d->isWorkset = obj["isWorkset"].toBool();
		d->name = obj["name"].toString();
		d->progress.clear();

		auto array = obj["progress"].toArray();
		for (auto&& iter : array) {
			ItemProgress prog;
			prog.Deserialize(iter.toObject());
			d->progress.push_back(prog);
		}
	}

	auto SyncExecution::GetObjectNames() const -> QStringList {
		return { "infoId", "isWorkset", "name", "progress" };
	}
}
