#include "WorksetHasItemMetadata.h"

namespace TC::Cils::JsonEntity {
	struct WorksetHasItemMetadata::Impl {
		int worksetId = -1;
		int itemId = -1;
		QString dataId;
		QString key;
		QString value;
	};

	WorksetHasItemMetadata::WorksetHasItemMetadata() : IJsonEntity(), d(new Impl) {}

	WorksetHasItemMetadata::WorksetHasItemMetadata(WorksetHasItemMetadata&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	WorksetHasItemMetadata::WorksetHasItemMetadata(const WorksetHasItemMetadata& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	WorksetHasItemMetadata& WorksetHasItemMetadata::operator=(const WorksetHasItemMetadata& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	WorksetHasItemMetadata& WorksetHasItemMetadata::operator=(WorksetHasItemMetadata&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool WorksetHasItemMetadata::operator==(const WorksetHasItemMetadata& obj) const {
		return d->worksetId == obj.d->worksetId && d->itemId == obj.d->itemId;
	}

	WorksetHasItemMetadata::~WorksetHasItemMetadata() = default;

	auto WorksetHasItemMetadata::DeserializeThis(const QJsonObject& obj) -> void {
		d->worksetId = obj["wsId"].toInt();
		d->itemId = obj["itemId"].toInt();
		d->dataId = obj["dataId"].toString();
		d->key = obj["keyName"].toString();
		d->value = obj["value"].toString();
	}

	auto WorksetHasItemMetadata::GetObjectNames() const -> QStringList {
		return { "wsId", "itemId", "dataId", "keyName", "value" };
	}
}