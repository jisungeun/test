#include <QString>
#include <QJsonDocument>
#include <QJsonObject>

#include "Workset.h"

namespace TC::Cils::JsonEntity {
	struct Workset::Impl {
		int id{};
		QString name;
	};

	Workset::Workset() : IJsonEntity(), d(new Impl) {}

	Workset::Workset(Workset&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	Workset::Workset(const Workset& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	Workset& Workset::operator=(const Workset& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	Workset& Workset::operator=(Workset&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool Workset::operator==(const Workset& obj) const {
		return (d->id == obj.d->id);
	}

	Workset::~Workset() = default;

	auto Workset::GetId() const -> int {
		return d->id;
	}

	auto Workset::GetName() const -> const QString& {
		return d->name;
	}

	auto Workset::DeserializeThis(const QJsonObject& obj) -> void {
		d->id = obj["wsId"].toInt();
		d->name = obj["name"].toString();
	}

	auto Workset::GetObjectNames() const -> QStringList {
		return { "wsId" , "name" };
	}
}
