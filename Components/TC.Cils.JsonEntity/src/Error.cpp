#include <QJsonDocument>
#include <QJsonObject>

#include "Error.h"

namespace TC::Cils::JsonEntity {
	struct Error::Impl {
		int status = -1;
		int error = -1;
		QString message;
	};

	Error::Error() : IJsonEntity(), d(new Impl) {}

	Error::Error(Error&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	Error::Error(const Error& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	Error& Error::operator=(const Error& obj) {
		if (this == &obj) return *this;

		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	Error& Error::operator=(Error&& obj) noexcept {
		if (this == &obj) return *this;

		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool Error::operator==(const Error& obj) const {
		return (d->error == obj.d->error);
	}

	Error::~Error() = default;

	auto Error::GetStatus() const -> int {
		return d->status;
	}

	auto Error::GetCode() const -> int {
		return d->error;
	}

	auto Error::GetMessage() const -> const QString& {
		return d->message;
	}

	auto Error::DeserializeThis(const QJsonObject& obj) -> void {
		d->status = obj["status"].toInt();
		d->error = obj["code"].toInt();
		d->message = obj["message"].toString();
	}

	auto Error::GetObjectNames() const -> QStringList {
		return { "status", "code", "message" };
	}
}
