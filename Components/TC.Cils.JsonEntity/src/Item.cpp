#include <QJsonDocument>
#include <QJsonObject>

#include "Item.h"

namespace TC::Cils::JsonEntity {
	struct Item::Impl {
		int id = -1;
		QString dataId;
		std::optional<QString> title{};
		int orgId = -1;
		unsigned long long size = 0;
		QString imageUrl;
	};

	//Item::Item(int id, QString&& dataId, QString&& title, int orgId) : d(new Impl{ id, dataId, title, orgId }) {}

	Item::Item() : IJsonEntity(), d(new Impl) {}

	Item::Item(Item&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	Item::Item(const Item& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	Item& Item::operator=(const Item& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	Item& Item::operator=(Item&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool Item::operator==(const Item& obj) const {
		return (d->id == obj.d->id);
	}

	Item::~Item() = default;

	auto Item::GetId() const -> int {
		return d->id;
	}

	auto Item::GetDataId() const -> const QString& {
		return d->dataId;
	}

	auto Item::GetTitle() const -> const std::optional<QString>& {
		return d->title;
	}

	auto Item::GetOrganizationId() const -> int {
		return d->orgId;
	}

	auto Item::GetImageUrl() const -> const QString& {
		return d->imageUrl;
	}

	auto Item::GetSize() const -> unsigned long long {
		return d->size;
	}

	auto Item::DeserializeThis(const QJsonObject& obj) -> void {
		d->id = obj["itemId"].toInt(-1);
		d->dataId = obj["dataId"].toString();
		if (obj.contains("title") && !obj["title"].isNull())
			d->title = obj["title"].toString();
		d->orgId = obj["orgId"].toInt(-1);
		d->imageUrl = obj["imageFile"].toObject()["link"].toString();
		if (obj.contains("currentSnapshot"))
			d->size = obj["currentSnapshot"].toObject()["size"].toString().toULongLong();
	}

	auto Item::GetObjectNames() const -> QStringList {
		return { "itemId", "dataId", "title", "orgId", "imageFile" };
	}
}
