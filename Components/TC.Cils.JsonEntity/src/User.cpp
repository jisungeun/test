#include <QString>
#include <QJsonDocument>
#include <QJsonObject>

#include "User.h"

namespace TC::Cils::JsonEntity {
	struct User::Impl {
		int id = -1;
		std::optional<QString> username;
		std::optional<QString> name;
	};

	User::User() : IJsonEntity(), d(new Impl) {}
	
	User::User(User&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	User::User(const User& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	User& User::operator=(const User& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	User& User::operator=(User&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool User::operator==(const User& obj) const {
		return d->id == obj.d->id && d->name == obj.d->name && d->username == d->username;
	}

	User::~User() = default;

	auto User::GetId() const -> int {
		return d->id;
	}

	auto User::GetUsername() const -> const std::optional<QString>& {
		return d->username;
	}

	auto User::GetName() const -> const std::optional<QString>& {
		return d->name;
	}

	auto User::DeserializeThis(const QJsonObject& obj) -> void {
		d->id = obj["userId"].toInt();
		d->username = obj["username"].toString();
		d->name = obj["name"].toString();
	}

	auto User::GetObjectNames() const -> QStringList {
		return { "userId" , "username" , "name" };
	}
}
