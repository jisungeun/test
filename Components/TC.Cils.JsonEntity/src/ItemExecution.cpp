#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

#include "ItemExecution.h"

namespace TC::Cils::JsonEntity {
	struct ItemExecution::Impl {
		int id{};
		int itemId{};
		QString dataId;
		std::optional<QString> title = {};
		Item item;
		std::optional<User> locker = {};
		std::optional<User> assignee = {};
		ExecutionStatus assignStatus{};
		std::optional<User> reviewer = {};
		ExecutionStatus reviewStatus{};
		QVector<Workset> worksets;
		int pid{};
		bool availability{};
		std::optional<ItemExecutionResult> result;
	};

	ItemExecution::ItemExecution() : IJsonEntity(), d(new Impl) {}

	ItemExecution::ItemExecution(ItemExecution&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	ItemExecution::ItemExecution(const ItemExecution& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	ItemExecution& ItemExecution::operator=(const ItemExecution& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	ItemExecution& ItemExecution::operator=(ItemExecution&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool ItemExecution::operator==(const ItemExecution& obj) const {
		return (d->id == obj.d->id);
	}

	ItemExecution::~ItemExecution() = default;

	auto ItemExecution::GetId() const -> int {
		return d->id;
	}

	auto ItemExecution::GetItemId() const -> int {
		return d->itemId;
	}

	auto ItemExecution::GetDataId() const -> const QString& {
		return d->dataId;
	}

	auto ItemExecution::GetTitle() const -> const std::optional<QString>& {
		return d->title;
	}

	auto ItemExecution::GetItem() const -> const Item& {
		return d->item;
	}

	auto ItemExecution::GetLocker() const -> const std::optional<User>& {
		return d->locker;
	}

	auto ItemExecution::GetAssignee() const -> const std::optional<User>& {
		return d->assignee;
	}

	auto ItemExecution::GetAssignStatus() const -> ExecutionStatus {
		return d->assignStatus;
	}

	auto ItemExecution::GetReviewer() const -> const std::optional<User>& {
		return d->reviewer;
	}

	auto ItemExecution::GetReviewStatus() const -> ExecutionStatus {
		return d->reviewStatus;
	}

	auto ItemExecution::GetWorksets() const -> const QVector<Workset>& {
		return d->worksets;
	}

	auto ItemExecution::GetProjectId() const -> int {
		return d->pid;
	}

	auto ItemExecution::IsAvailable() const -> bool {
		return d->availability;
	}

	auto ItemExecution::GetResult() const -> const std::optional<ItemExecutionResult>& {
		return d->result;
	}

	auto ItemExecution::ExecutionStatusToString(ExecutionStatus status) -> QString {
		if (status == ExecutionStatus::NotStarted)
			return "NotStarted";
		else if (status == ExecutionStatus::Executing)
			return "Executing";
		else if (status == ExecutionStatus::Executed)
			return "Executed";
		else if (status == ExecutionStatus::UploadFailed)
			return "UploadFailed";
		return "Unknown";
	}

	auto ItemExecution::StringToExecutionStatus(const QString& status) -> ExecutionStatus {
		if (status == "NotStarted")
			return ExecutionStatus::NotStarted;
		else if (status == "Executing")
			return ExecutionStatus::Executing;
		if (status == "Executed")
			return ExecutionStatus::Executed;
		if (status == "UploadFailed")
			return ExecutionStatus::UploadFailed;
		return ExecutionStatus::Any;
	}

	auto ItemExecution::DeserializeThis(const QJsonObject& obj) -> void {
		d->id = obj["ieId"].toInt();
		d->itemId = obj["itemId"].toInt();
		d->dataId = obj["dataId"].toString();
		if (obj.contains("title"))
			d->title = obj["title"].toString();
		d->item = DeserializeType<Item>(obj["item"].toObject());
		if (obj.contains("locker") && !obj["locker"].isNull())
			d->locker = DeserializeType<User>(obj["locker"].toObject());
		if (obj.contains("assignee") && !obj["assignee"].isNull())
			d->assignee = DeserializeType<User>(obj["assignee"].toObject());
		d->assignStatus = StringToExecutionStatus(obj["assignStatus"].toString());
		if (obj.contains("reviewer") && !obj["reviewer"].isNull())
			d->reviewer = DeserializeType<User>(obj["reviewer"].toObject());
		d->reviewStatus = StringToExecutionStatus(obj["reviewStatus"].toString());
		d->pid = obj["projectId"].toInt();
		d->availability = obj["isAvailable"].toBool();
		if (!obj["latestResult"].isNull())
			d->result = DeserializeType<ItemExecutionResult>(obj["latestResult"].toObject());

		d->worksets.clear();
		auto array = obj["inWorksets"].toArray();
		for (auto&& iter : array) {
			Workset ws;
			ws.Deserialize(iter.toObject());
			d->worksets.push_back(ws);
		}
	}

	auto ItemExecution::GetObjectNames() const -> QStringList {
		return { "ieId", "itemId", "dataId", "title", "item" };
	}
}
