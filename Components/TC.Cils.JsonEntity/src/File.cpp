#include <QJsonDocument>

#include "File.h"
#include "CilsFile.h"

namespace TC::Cils::JsonEntity {
	struct File::Impl {
		int id = -1;
		CilsFile file;
		QString path;
	};

	File::File() : IJsonEntity(), d(new Impl) {}

	File::File(File&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	File::File(const File& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	File& File::operator=(const File& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	File& File::operator=(File&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool File::operator==(const File& obj) const {
		return (d->id = obj.d->id);
	}

	File::~File() = default;

    auto File::GetId() const -> int {
		return d->id;
    }

    auto File::GetFile() const -> const CilsFile& {
		return d->file;
    }

    auto File::GetPath() const -> const QString& {
		return d->path;
    }

    auto File::DeserializeThis(const QJsonObject& obj) -> void {
		d->id = obj["shfId"].toInt();
		d->file = DeserializeType<CilsFile>(obj["cilsFile"].toObject());
		d->path = obj["path"].toString();
	}

	auto File::GetObjectNames() const -> QStringList {
		return { "shfId", "cilsFile", "path" };
	}
}
