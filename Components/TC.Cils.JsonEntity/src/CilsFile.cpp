#include "CilsFile.h"

namespace TC::Cils::JsonEntity {
	struct CilsFile::Impl {
		int id = -1;
		QString hash;
		QString size;
		int available = -1;
		QString dataId;
	};

	CilsFile::CilsFile() : IJsonEntity(), d(new Impl) {}

	CilsFile::CilsFile(CilsFile&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	CilsFile::CilsFile(const CilsFile& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	CilsFile& CilsFile::operator=(const CilsFile& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	CilsFile& CilsFile::operator=(CilsFile&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool CilsFile::operator==(const CilsFile& obj) const {
		return (d->id == obj.d->id);
	}

	CilsFile::~CilsFile() = default;

    auto CilsFile::GetId() const -> int {
		return d->id;
    }

    auto CilsFile::GetHash() const -> const QString& {
		return d->hash;
    }

    auto CilsFile::GetSize() const -> const QString& {
		return d->size;
    }

    auto CilsFile::GetAvailable() const -> int {
		return d->available;
    }

    auto CilsFile::GetBelongToDataId() const -> QString {
		return d->dataId;
    }

    auto CilsFile::DeserializeThis(const QJsonObject& obj) -> void {
		d->id = obj["id"].toInt();
		d->hash = obj["md5"].toString();
		d->size = obj["size"].toString();
		d->available = obj["available"].toInt();
		d->dataId = obj["belongToDataId"].toString();
	}

	auto CilsFile::GetObjectNames() const -> QStringList {
		return { "id", "md5", "size", "available", "belongToDataId" };
	}
}
