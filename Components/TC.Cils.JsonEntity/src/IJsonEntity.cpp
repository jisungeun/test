#include <QJsonParseError>

#include "IJsonEntity.h"

namespace TC::Cils::JsonEntity {
	struct IJsonEntity::Impl {
		bool initialized = false;

		[[nodiscard]] static auto Contains(const QJsonObject& obj, QStringList&& list) -> bool {
			foreach(const auto & l, list)
				if (!obj.contains(l))
					return false;
			return true;
		}
	};

	IJsonEntity::IJsonEntity() : d(new Impl) {}

	IJsonEntity::IJsonEntity(IJsonEntity&& obj) noexcept : d(std::move(obj.d)) {}

	IJsonEntity::IJsonEntity(const IJsonEntity& obj) : d(new Impl{ *obj.d }) {}

	IJsonEntity& IJsonEntity::operator=(const IJsonEntity& obj) {
		if (this == &obj || d == obj.d) return *this;

		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	IJsonEntity& IJsonEntity::operator=(IJsonEntity&& obj) noexcept {
		if (this == &obj) return *this;

		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	IJsonEntity::~IJsonEntity() = default;

	auto IJsonEntity::Deserialize(const QJsonObject& obj) -> bool {
		if (d->Contains(obj, GetObjectNames())) {
			DeserializeThis(obj);
			d->initialized = true;
			return true;
		}

		return false;
	}

	auto IJsonEntity::IsInitialized() const -> bool {
		return d->initialized;
	}
}
