#include <QJsonDocument>

#include "ItemExecutionResult.h"

namespace TC::Cils::JsonEntity {
	struct ItemExecutionResult::Impl {
		int id = -1;
		Item item;
	};

	ItemExecutionResult::ItemExecutionResult() : IJsonEntity(), d(new Impl) {}

	ItemExecutionResult::ItemExecutionResult(ItemExecutionResult&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	ItemExecutionResult::ItemExecutionResult(const ItemExecutionResult& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	ItemExecutionResult& ItemExecutionResult::operator=(const ItemExecutionResult& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	ItemExecutionResult& ItemExecutionResult::operator=(ItemExecutionResult&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool ItemExecutionResult::operator==(const ItemExecutionResult& obj) const {
		return (d->id == obj.d->id);
	}

	ItemExecutionResult::~ItemExecutionResult() = default;

    auto ItemExecutionResult::GetId() const -> int {
		return d->id;
    }

    auto ItemExecutionResult::GetItem() const -> const Item& {
		return d->item;
    }

    auto ItemExecutionResult::DeserializeThis(const QJsonObject& obj) -> void {
		d->id = obj["ierId"].toInt();
		d->item = DeserializeType<Item>(obj["item"].toObject());
    }

	auto ItemExecutionResult::GetObjectNames() const -> QStringList {
		return { "ierId", "item" };
	}
}
