#include <QString>
#include <QJsonDocument>
#include <QJsonObject>

#include "Project.h"

namespace TC::Cils::JsonEntity {
	struct Project::Impl {
		int id = -1;
		QString name;
		QString type;
	};

	Project::Project() : IJsonEntity(), d(new Impl) {}

	Project::Project(Project&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	Project::Project(const Project& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	Project& Project::operator=(const Project& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	Project& Project::operator=(Project&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool Project::operator==(const Project& obj) const {
		return (d->id == obj.d->id);
	}

	Project::~Project() = default;

	auto Project::GetId() const -> int {
		return d->id;
	}

	auto Project::GetName() const -> const QString& {
		return d->name;
	}

	auto Project::GetType() const -> const QString& {
		return d->type;
	}

	auto Project::DeserializeThis(const QJsonObject& obj) -> void {
		d->id = obj["projectId"].toInt();
		d->name = obj["name"].toString();
		d->type = obj["type"].toString();
	}

	auto Project::GetObjectNames() const -> QStringList {
		return { "projectId", "name", "type" };
	}
}
