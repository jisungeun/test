#include <QJsonDocument>

#include "OnExecutionReady.h"

namespace TC::Cils::JsonEntity {
	struct OnExecutionReady::Impl {
		ItemExecution ie;
		QString filePath;
		std::optional<ItemExecutionResult> result;
		std::optional<QString> resultPath;
	};

	OnExecutionReady::OnExecutionReady() : IJsonEntity(), d(new Impl) {}

	OnExecutionReady::OnExecutionReady(OnExecutionReady&& obj) noexcept : IJsonEntity(std::move(obj)), d(std::move(obj.d)) {}

	OnExecutionReady::OnExecutionReady(const OnExecutionReady& obj) : IJsonEntity(obj), d(new Impl{ *obj.d }) {}

	OnExecutionReady& OnExecutionReady::operator=(const OnExecutionReady& obj) {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::make_unique<Impl>( *obj.d );
		return *this;
	}

	OnExecutionReady& OnExecutionReady::operator=(OnExecutionReady&& obj) noexcept {
		if (this == &obj) return *this;

		IJsonEntity::operator=(obj);
		d.reset();
		d = std::move(obj.d);
		return *this;
	}

	bool OnExecutionReady::operator==(const OnExecutionReady& obj) const {
		return (d->ie == obj.d->ie);
	}

	OnExecutionReady::~OnExecutionReady() = default;

    auto OnExecutionReady::GetItemExecution() const -> const ItemExecution& {
		return d->ie;
    }

    auto OnExecutionReady::GetFilePath() const -> const QString& {
		return d->filePath;
    }

    auto OnExecutionReady::GetResult() const -> const std::optional<ItemExecutionResult>& {
		return d->result;
    }

    auto OnExecutionReady::GetResultPath() const -> const std::optional<QString>& {
		return d->resultPath;
    }

    auto OnExecutionReady::DeserializeThis(const QJsonObject& obj) -> void {
		d->ie = DeserializeType<ItemExecution>(obj["itemExecution"].toObject());
		d->filePath = obj["path"].toString();

		if (obj.contains("latestResult")) {
			const auto result = obj["latestResult"].toObject();

			d->result = DeserializeType<ItemExecutionResult>(result["result"].toObject());
			d->resultPath = result["path"].toString();
		}
	}

	auto OnExecutionReady::GetObjectNames() const -> QStringList {
		return { "itemExecution", "path", "latestResult" };
	}
}
