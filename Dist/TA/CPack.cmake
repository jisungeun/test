include(InstallRequiredSystemLibraries)

set(PROJECT_NAME "TomoAnalysis")
set(EXECUTABLE_NAME "CellAnalyzer")

set(CPACK_MODULE_PATH ${CMAKE_SOURCE_DIR}/Dist/TA/)

#project
set(CPACK_PACKAGE_NAME ${PROJECT_NAME})

include(${CMAKE_SOURCE_DIR}/Dist/TA/Version.cmake)

set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(CPACK_PACKAGE_VERSION ${PROJECT_VERSION})

#general information
set(CPACK_PACKAGE_VENDOR "Tomocube, Inc")   
set(CPACK_PACKAGE_HOMEPAGE_URL "http://tomocube.com")
set(CPACK_PACKAGE_DESCRIPTION "Tomocube Analysis Software")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "TomoAnalysis Software")

set(CPACK_RESOURCE_FILE_WELCOME "${PROJECT_SOURCE_DIR}/Dist/Common/welcome.md")
set(CPACK_RESOURCE_FILE_README "${PROJECT_SOURCE_DIR}/Dist/Common/readme.md")
set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/Dist/Common/license.md")
#set(CPACK_PACKAGE_ICON "${PROJECT_SOURCE_DIR}/resource\\\\images\\\\install_logo2.bmp")

#set(CPACK_PACKAGE_INSTALL_DIRECTORY "${CMAKE_PROJECT_NAME}")
set(CPACK_PACKAGE_INSTALL_DIRECTORY ${PROJECT_NAME})
set(CPACK_PACKAGE_DIRECTORY "${CMAKE_BINARY_DIR}/package")
set(CPACK_PACKAGE_FILE_NAME ${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}${PROJECT_VERSION_RELEASE_TYPE}-win64)

#set(CPACK_STRIP_FILES ON)
#set(CPACK_ARCHIVE_COMPONENT_INSTALL ON)
set(CPACK_COMPONENTS_ALL 
	redist
	framework
	io
	utility
	rendering
	thirdparty	
	thirdparty_decon #ArrayFire
	thirdparty_ta	
	thirdparty_oiv	
	algorithm	
	#application_ta
	#application_ta_ve #Viewer Edition
	application_ta_de #Default Edition	
	)

#NSIS Options
set(CPACK_NSIS_DISPLAY_NAME ${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}${PROJECT_VERSION_RELEASE_TYPE}) #displayed on the titlebar
set(CPACK_NSIS_PACKAGE_NAME ${CPACK_PACKAGE_NAME})
set(CPACK_NSIS_INSTALLED_ICON_NAME "${PROJECT_SOURCE_DIR}/Dist/Common\\\\icon_TA.ico")
set(CPACK_NSIS_URL_INFO_ABOUT "http://www.tomocube.com")
set(CPACK_NSIS_CONTACT "support@tomocube.com")
set(CPACK_NSIS_MUI_ICON "${PROJECT_SOURCE_DIR}/Dist/Common\\\\icon_TA.ico")
set(CPACK_NSIS_MUI_UNIICON "${PROJECT_SOURCE_DIR}/Dist/Common\\\\icon_TA.ico")
set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)

#Short Cuts
set(CPACK_NSIS_CREATE_ICONS
    "CreateShortCut '$SMPROGRAMS\\\\$STARTMENU_FOLDER\\\\TomoAnalysis.lnk' '$INSTDIR\\\\bin\\\\CellAnalyzer.exe'"                    	
    )
string (REPLACE ";" "\n" CPACK_NSIS_CREATE_ICONS "${CPACK_NSIS_CREATE_ICONS}")

set(CPACK_NSIS_DELETE_ICONS
    "Delete '$SMPROGRAMS\\\\$STARTMENU\\\\TomoAnalysis.lnk'"    	
    )
string (REPLACE ";" "\n" CPACK_NSIS_DELETE_ICONS "${CPACK_NSIS_DELETE_ICONS}")

include(CPackComponent)

#Component grouping & addition
cpack_add_component_group(
	redistributable
	DISPLAY_NAME "Application requirements"
	DESCRIPTION "Prerequisite redistributable packages")
	
cpack_add_component(
	redist	
	DISPLAY_NAME "Redistributable"		
	DESCRIPTION "Redistributable installers"
	GROUP redistributable
	REQUIRED)
	
cpack_add_component_group(
	ta_app
	DISPLAY_NAME "Application elements"
	DESCRIPTION "Application specific classes for TomoAnalysis")

#cpack_add_component(
#	application_ta	
#	DISPLAY_NAME ${PROJECT_NAME}		
#	DESCRIPTION "Software to analysis cell image"
#	GROUP ta_app
#	REQUIRED)
	
#cpack_add_component(
#	application_ta_ve
#	DISPLAY_NAME ${PROJECT_NAME}		
#	DESCRIPTION "Software to visualize cell image"
#	GROUP ta_app
#	REQUIRED)
	
cpack_add_component(
	application_ta_de
	DISPLAY_NAME ${PROJECT_NAME}		
	DESCRIPTION "Software to analyze cell image"
	GROUP ta_app
	REQUIRED)

cpack_add_component_group(
	component_ta
	DISPLAY_NAME "TA components"
	DESCRIPTION "Component classes to construct TomoAnalysis engines")

cpack_add_component(
	framework
	DISPLAY_NAME "Framework components"	
	DESCRIPTION "Framework functions"
	GROUP component_ta
	REQUIRED)

cpack_add_component(
	io	
	DISPLAY_NAME "I/O components"	
	DESCRIPTION "Input/Output functions"
	GROUP component_ta
	REQUIRED)

cpack_add_component(
	utility	
	DISPLAY_NAME "Utility components"	
	DESCRIPTION "Util/Helper tools"
	GROUP component_ta
	REQUIRED)
	
cpack_add_component(
	rendering	
	DISPLAY_NAME "Rendering components"	
	DESCRIPTION "2D/3D rendering functions"
	GROUP component_ta
	REQUIRED)

cpack_add_component_group(algorithm_ta
	DISPLAY_NAME "TA algorithms"
	DESCRIPTION "Algorithm classes to construct TomoAnalysis processors")

cpack_add_component(
	algorithm	
	DISPLAY_NAME "Common algorithms"
	DESCRIPTION "Common image processing algorithms"
	GROUP algorithm_ta
	REQUIRED)

cpack_add_component_group(thirdparty_libs
	DISPLAY_NAME "TA thirdparty"	
	DESCRIPTION "Thirdparty libraries for TomoAnalysis")

cpack_add_component(
	thirdparty	
	DISPLAY_NAME "Common thirdparty"	
	DESCRIPTION "Common thirdparty for TomoAnalysis"
	GROUP thirdparty_libs
	REQUIRED)
	
cpack_add_component(
	thirdparty_ta
	DISPLAY_NAME "TA thirdparty"	
	DESCRIPTION "TA thirdparty for TomoAnalysis"
	GROUP thirdparty_libs
	REQUIRED)
	
cpack_add_component(
	thirdparty_oiv
	DISPLAY_NAME "Open Inventor thirdparty"	
	DESCRIPTION "Open Inventor thirdparty for TomoAnalysis"
	GROUP thirdparty_libs
	REQUIRED)
	
include(CPack REQUIRED)