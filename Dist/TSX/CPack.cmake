include(InstallRequiredSystemLibraries)

set(PROJECT_NAME "TomoStudio X")
set(PROJECT_NAME_NOSPACE "TomoStudioX")

set(CPACK_MODULE_PATH ${CMAKE_SOURCE_DIR}/Dist/TSX)

#project
set(CPACK_PACKAGE_NAME ${PROJECT_NAME_NOSPACE})

include(${CMAKE_SOURCE_DIR}/Dist/TSX/Version.cmake)

set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(CPACK_PACKAGE_VERSION ${PROJECT_VERSION})

#general information
set(CPACK_PACKAGE_VENDOR "Tomocube, Inc")   
set(CPACK_PACKAGE_HOMEPAGE_URL "http://tomocube.com")
set(CPACK_PACKAGE_DESCRIPTION "TomoStudio X")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "TomoStudio X")

set(CPACK_RESOURCE_FILE_WELCOME "${CMAKE_SOURCE_DIR}/Dist/Common/welcome.md")
set(CPACK_RESOURCE_FILE_README "${CMAKE_SOURCE_DIR}/Dist/Common/readme.md")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/Dist/Common/license.md")

#set(CPACK_PACKAGE_ICON "${PROJECT_SOURCE_DIR}/resource\\\\images\\\\install_logo2.bmp")
set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CPACK_PACKAGE_NAME})
set(CPACK_PACKAGE_DIRECTORY "${CMAKE_BINARY_DIR}/package")
set(CPACK_PACKAGE_FILE_NAME ${CPACK_PACKAGE_NAME}-v${CPACK_PACKAGE_VERSION}${PROJECT_VERSION_RELEASE_TYPE}-win64)

#options
#set(CPACK_STRIP_FILES TRUE)
set(CPACK_COMPONENTS_ALL 
    io
    utility
    hw #TCJoystickControl, TCMCUControl, TCMotionControl
    framework #DataTypes, EngineFramework, EventTypes, UiFramework, FrameworkWidgets
    thirdparty #BetterEnums, OpenSSL, QsLog, QtSingleApplication
	thirdparty_decon #autoquant
	thirdparty_hw #McuUpdate, SDL, Spinnaker
    thirdparty_util #7zip
    processing
    application_htx #relates to TomoStudioX
    application_htx_proc #relates to HTXProcessingServer
    )


#NSIS options
set(CPACK_NSIS_DISPLAY_NAME ${PROJECT_NAME}-${CPACK_PACKAGE_VERSION}${PROJECT_VERSION_RELEASE_TYPE}) #displayed on the titlebar
set(CPACK_NSIS_PACKAGE_NAME ${PROJECT_NAME})
set(CPACK_NSIS_INSTALLED_ICON_NAME "${CMAKE_SOURCE_DIR}/Dist/Common\\\\icon.ico")
set(CPACK_NSIS_URL_INFO_ABOUT "http://www.tomocube.com")
set(CPACK_NSIS_CONTACT "support@tomocube.com")
set(CPACK_NSIS_MUI_ICON "${CMAKE_SOURCE_DIR}/Dist/Common\\\\icon.ico")
set(CPACK_NSIS_MUI_UNIICON "${CMAKE_SOURCE_DIR}/Dist/Common\\\\icon.ico")
set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)

#Short Cuts
set(CPACK_NSIS_CREATE_ICONS 
    "CreateShortCut '$SMPROGRAMS\\\\$STARTMENU_FOLDER\\\\TomoStudio X.lnk' '$INSTDIR\\\\bin\\\\HTXpress.exe'"
    )
    
set(CPACK_NSIS_CREATE_ICONS_EXTRA
    "CreateShortCut '$SMPROGRAMS\\\\$STARTMENU_FOLDER\\\\TSX Processing Server.lnk' '$INSTDIR\\\\bin\\\\HTXProcessingServer.exe'"
    )

set(CPACK_NSIS_DELETE_ICONS
    "Delete '$SMPROGRAMS\\\\$STARTMENU\\\\TomoStudio X.lnk'"    
    )
set(CPACK_NSIS_DELETE_ICONS_EXTRA
    "Delete '$SMPROGRAMS\\\\$STARTMENU\\\\TSX Processing Server.lnk'"
    )
include(CPackComponent)

include(CPack Required)
