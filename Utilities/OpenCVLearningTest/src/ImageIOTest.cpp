#include <catch2/catch.hpp>

#include <memory>
#include <opencv2/core/hal/interface.h>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgcodecs.hpp>


namespace ImageIOTest {
    TEST_CASE("ImageRead") {
        SECTION("uint8_t data") {
            const int32_t sizeX = 100;
            const int32_t sizeY = 100;

            const auto type = CV_8U;

            const auto length = sizeX * sizeY;

            std::shared_ptr<uint8_t[]> data(new uint8_t[length]());

            for (auto dataIndex = 0; dataIndex < length; ++dataIndex) {
                data.get()[dataIndex] = static_cast<uint8_t>(dataIndex);
            }

            cv::Mat image(sizeY, sizeX, type, data.get());

            const cv::String filePath = "ImageReadingTestUint8.png";
            cv::imwrite(filePath, image);
            const auto readImage = cv::imread(filePath);

            SECTION("Mat type : CV8UC3") {
                const auto imageType = readImage.type();
                CHECK(imageType == CV_8UC3);
            }
        }
    }

    TEST_CASE("ImageWrite") {
        SECTION("uint8_t data") {
            const int32_t sizeX = 100;
            const int32_t sizeY = 100;

            const auto type = CV_8U;

            const auto length = sizeX * sizeY;

            std::shared_ptr<uint8_t[]> data(new uint8_t[length]());

            for (auto dataIndex = 0; dataIndex < length; ++dataIndex) {
                data.get()[dataIndex] = static_cast<uint8_t>(dataIndex);
            }

            cv::Mat image(sizeY, sizeX, type, data.get());

            const cv::String filePath = "ImageWritingTestUint8.png";
            cv::imwrite(filePath, image);
            const auto readImage = cv::imread(filePath);

            auto resultIsGood = true;
            for (auto dataIndex = 0; dataIndex < length; ++dataIndex) {
                const auto answer = data.get()[dataIndex];

                const auto pixel = readImage.at<cv::Vec3b>(dataIndex);
                const auto channel0Good = pixel[0] == answer;
                const auto channel1Good = pixel[1] == answer;
                const auto channel2Good = pixel[2] == answer;

                const auto pixelIsBad = !(channel0Good && channel1Good && channel2Good);
                if (pixelIsBad) {
                    resultIsGood = false;
                    break;
                }
            }
            CHECK(resultIsGood == true);
        }
    }
}
