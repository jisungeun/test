#include <catch2/catch.hpp>

#include <memory>

#include <opencv2/core/mat.hpp>
#include <opencv2/core/hal/interface.h>

namespace MatTest {
    TEST_CASE("Mat") {
        SECTION("Mat from raw host memory") {
            const int32_t sizeX = 10;
            const int32_t sizeY = 10;
            const auto type = CV_8U;

            const auto length = sizeX * sizeY;

            std::shared_ptr<uint8_t[]> data(new uint8_t[length]());

            for (auto dataIndex = 0; dataIndex < length; ++dataIndex) {
                data.get()[dataIndex] = dataIndex;
            }

            cv::Mat mat(sizeY, sizeX, type, data.get());

            auto resultIsGood = true;
            for (auto dataIndex = 0; dataIndex < length; ++dataIndex) {
                const auto differentResult = mat.at<uint8_t>(dataIndex) != data.get()[dataIndex];
                if (differentResult) {
                    resultIsGood = false;
                    break;
                }
            }

            CHECK(resultIsGood == true);
        }
    }
}