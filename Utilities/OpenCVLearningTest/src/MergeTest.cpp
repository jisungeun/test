#include <catch2/catch.hpp>

#include <memory>

#include <opencv2/core.hpp>

namespace MergeTest {
    auto GenerateChannelData(const size_t& numberOfElements, const uint8_t& valueOffset)->std::shared_ptr<uint8_t[]> {
        std::shared_ptr<uint8_t[]> data(new uint8_t[numberOfElements]());

        for (size_t index = 0; index < numberOfElements; ++index) {
            data.get()[index] = static_cast<uint8_t>(index) + valueOffset;
        }

        return data;
    }


    TEST_CASE("merge") {
        SECTION("merge rgb data") {
            const int32_t numberOfChannels = 3;

            const int32_t sizeX = 100;
            const int32_t sizeY = 100;
            const auto type = CV_8U;
            const auto numberOfElements = sizeX * sizeY;

            const auto redValueOffset = 0;
            const auto greenValueOffset = 50;
            const auto blueValueOffset = 100;

            const auto redHostData = GenerateChannelData(numberOfElements, redValueOffset);
            const auto greenHostData = GenerateChannelData(numberOfElements, greenValueOffset);
            const auto blueHostData = GenerateChannelData(numberOfElements, blueValueOffset);

            const cv::Mat redData(sizeY, sizeX, type, redHostData.get());
            const cv::Mat greenData(sizeY, sizeX, type, greenHostData.get());
            const cv::Mat blueData(sizeY, sizeX, type, blueHostData.get());

            cv::Mat bgr[numberOfChannels] = { blueData, greenData, redData };

            cv::Mat mergedData;
            cv::merge(bgr, numberOfChannels, mergedData);

            CHECK(mergedData.type() == CV_8UC3);
        }

        SECTION("merge only red data with empty mat : fail") {
            const int32_t numberOfChannels = 3;

            const int32_t sizeX = 100;
            const int32_t sizeY = 100;
            const auto type = CV_8U;
            const auto numberOfElements = sizeX * sizeY;

            const auto redValueOffset = 0;

            const auto redHostData = GenerateChannelData(numberOfElements, redValueOffset);

            const cv::Mat redData(sizeY, sizeX, type, redHostData.get());
            const cv::Mat greenData{};
            const cv::Mat blueData{};

            cv::Mat bgr[numberOfChannels] = { blueData, greenData, redData };

            cv::Mat mergedData;
            cv::merge(bgr, numberOfChannels, mergedData);

            CHECK(mergedData.type() == CV_8UC3);
        }

        SECTION("merge only red data with 0 mat : pass") {
            const int32_t numberOfChannels = 3;

            const int32_t sizeX = 100;
            const int32_t sizeY = 100;
            const auto type = CV_8U;
            const auto numberOfElements = sizeX * sizeY;

            const auto redValueOffset = 0;

            const auto redHostData = GenerateChannelData(numberOfElements, redValueOffset);

            const cv::Mat redData(sizeY, sizeX, type, redHostData.get());
            const cv::Mat greenData(sizeY, sizeX, type, cv::Scalar(0));
            const cv::Mat blueData(sizeY, sizeX, type, cv::Scalar(0));

            cv::Mat bgr[numberOfChannels] = { blueData, greenData, redData };

            cv::Mat mergedData;
            cv::merge(bgr, numberOfChannels, mergedData);

            CHECK(mergedData.type() == CV_8UC3);
        }
    }
}