#include <iostream>
#include <string>
#include "H5Cpp.h"
using namespace H5;

const H5std_string	FILE_NAME("h5tutr_dset.h5");
const H5std_string	DATASET_NAME("dset");
const int	 NX = 4;                     // dataset dimensions
const int	 NY = 6;
const int	 RANK = 2;

int main(int argc, char** argv) {
    try
    {
        Exception::dontPrint();

        H5File file(FILE_NAME, H5F_ACC_TRUNC);

        hsize_t dims[2];
        dims[0] = NX;
        dims[1] = NY;
        DataSpace dataSpace(RANK, dims);

        DataSet dataset = file.createDataSet(DATASET_NAME, PredType::STD_I32BE, dataSpace);
    } catch (FileIException& error) {
        error.printErrorStack();
        return -1;
    } catch (DataSetIException& error) {
        error.printErrorStack();
        return -1;
    } catch (DataSpaceIException& error) {
        error.printErrorStack();
        return -1;
    }

    return 0;
}