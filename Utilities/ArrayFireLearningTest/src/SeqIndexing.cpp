#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace SeqIndexing {
    TEST_CASE("SeqIndexing : indexing with 1D seq") {
        SECTION("3D z indexing : continuous seq") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto sizeZ = 3;

            af::array data = af::constant(0, sizeY, sizeX, sizeZ);
            data(af::span, af::span, 0) = 0;
            data(af::span, af::span, 1) = 1;
            data(af::span, af::span, 2) = 2;

            const auto continuousSeq = af::seq(1, 2);
            data(af::span, af::span, continuousSeq) = 0;

            const auto sumOfData = af::sum<float>(data);
            CHECK(sumOfData == 0);
        }

        SECTION("3D z indexing : continuous seq sampling") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto sizeZ = 3;

            af::array data = af::constant(0, sizeY, sizeX, sizeZ);
            data(af::span, af::span, 0) = 0;
            data(af::span, af::span, 1) = 1;
            data(af::span, af::span, 2) = 2;

            const auto continuousSeq = af::seq(1, 2);
            const auto sampledData = data(af::span, af::span, continuousSeq).copy();

            af::array answerData = af::constant(0, sizeY, sizeX, 2);
            answerData(af::span, af::span, 0) = 1;
            answerData(af::span, af::span, 1) = 2;

            const auto diffData = sampledData - answerData;

            const auto sumOfDiffData = af::sum<float>(diffData);
            CHECK(sumOfDiffData == 0);
        }

        SECTION("3D z indexing : discontinuous seq") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto sizeZ = 3;

            af::array data = af::constant(0, sizeY, sizeX, sizeZ);
            data(af::span, af::span, 0) = 1;
            data(af::span, af::span, 1) = 0;
            data(af::span, af::span, 2) = 2;

            const auto disContinuousSeq = af::seq(0, 2, 2);
            data(af::span, af::span, disContinuousSeq) = 0;

            const auto sumOfData = af::sum<float>(data);
            CHECK(sumOfData == 0);
        }

        SECTION("3D z indexing : discontinuous seq sampling") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto sizeZ = 3;

            af::array data = af::constant(0, sizeY, sizeX, sizeZ);
            data(af::span, af::span, 0) = 1;
            data(af::span, af::span, 1) = 0;
            data(af::span, af::span, 2) = 2;

            const auto continuousSeq = af::seq(0, 2, 2);
            const auto sampledData = data(af::span, af::span, continuousSeq).copy();

            af::array answerData = af::constant(0, sizeY, sizeX, 2);
            answerData(af::span, af::span, 0) = 1;
            answerData(af::span, af::span, 1) = 2;

            //af_print(sampledData);

            const auto diffData = sampledData - answerData;

            const auto sumOfDiffData = af::sum<float>(diffData);
            CHECK(sumOfDiffData == 0);
        }
    }
}


