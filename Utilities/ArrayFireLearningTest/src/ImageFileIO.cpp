﻿#include <catch2/catch.hpp>

#include <QFile>
#include <QString>

#include "arrayfire.h"

namespace ImageFileIO {
    auto GenerateRandomNumber0To1()->float {
        std::random_device rd;
        std::mt19937 gen(rd());
        const std::uniform_real_distribution<float> dis(0, 1);

        const auto number0To1 = dis(gen);
        return number0To1;
    }

    auto GenerateRandomNumber0To255()->float {
        const auto number0To255 = GenerateRandomNumber0To1() * 256.f;
        return number0To255;
    }

    template <class T>
    auto GenerateRandomRawData(const int32_t& sizeX, const int32_t& sizeY)->std::shared_ptr<T> {
        const auto numberOfPixels = sizeX * sizeY;
        const std::shared_ptr<T> data(new T[numberOfPixels]());
        for (auto j = 0; j < sizeY; ++j) {
            for (auto i = 0; i < sizeX; ++i) {
                const auto index = i + sizeX * j;
                data.get()[index] = static_cast<T>(GenerateRandomNumber0To255());
            }
        }
        return data;
    }

    TEST_CASE("loadImage()") {
        SECTION("PNG : uint8_t loaded array type is float") {
            auto data = af::randu(10, 10, u8);
            const std::string imagePath = "uint8_t.png";
            af::saveImageNative(imagePath.c_str(), data);

            const auto loadedArray = af::loadImage(imagePath.c_str());
            CHECK(loadedArray.type() != u8);
            CHECK(loadedArray.type() == f32);
        }

        SECTION("PNG : uint8_t image") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto randomData = GenerateRandomRawData<uint8_t>(sizeX, sizeY);

            const af::array randomArrayUint8(sizeY, sizeX, randomData.get());

            const std::string imagePath = "uint8_t.png";
            af::saveImageNative(imagePath.c_str(), randomArrayUint8);

            const auto loadedImage = af::loadImage(imagePath.c_str());
            const auto loadedData = loadedImage.host<float>();

            auto dataIsSame{ true };
            for (auto i = 0; i < sizeX * sizeY; ++i) {
                const auto value = static_cast<uint8_t>(loadedData[i]);
                const auto answer = randomData.get()[i];

                if (value != answer) {
                    dataIsSame = false;
                    break;
                }
            }
            CHECK(dataIsSame);
            af::freeHost(loadedData);
        }
    }

    TEST_CASE("loadImageNative()") {
        SECTION("PNG : uint8_t loaded array type is u8") {
            auto data = af::randu(10, 10, u8);
            const std::string imagePath = "uint8_t.png";
            af::saveImageNative(imagePath.c_str(), data);

            const auto loadedArray = af::loadImageNative(imagePath.c_str());
            CHECK(loadedArray.type() == u8);
        }

        SECTION("PNG : uint8_t image") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto randomData = GenerateRandomRawData<uint8_t>(sizeX, sizeY);

            const af::array randomArrayUint8(sizeY, sizeX, randomData.get());

            const std::string imagePath = "uint8_t.png";
            af::saveImageNative(imagePath.c_str(), randomArrayUint8);

            const auto loadedImage = af::loadImageNative(imagePath.c_str());
            const auto loadedData = loadedImage.host<uint8_t>();

            auto dataIsSame{ true };
            for (auto i = 0; i < sizeX * sizeY; ++i) {
                const auto value = loadedData[i];
                const auto answer = randomData.get()[i];

                if (value != answer) {
                    dataIsSame = false;
                    break;
                }
            }
            CHECK(dataIsSame);
            af::freeHost(loadedData);
        }
    }

    auto SaveAndLoadExceptionThrown(const QString imagePath)->bool {
        auto exceptionThrown = false;
        try {
            auto data = af::randu(10, 10, u8);
            if (QFile::exists(imagePath)) {
                QFile::remove(imagePath);
            }

            REQUIRE(!QFile::exists(imagePath));
            
            af::saveImageNative(imagePath.toStdString().c_str(), data);

            const auto loadedArray = af::loadImageNative(imagePath.toStdString().c_str());
        } catch (af::exception&) {
            exceptionThrown = true;
        }

        return exceptionThrown;
    }

    TEST_CASE("multibyte in QString path") {
        SECTION("English") {
            const QString imagePath = "image.png";
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("Korean") {
            const QString imagePath = QString::fromStdWString(L"한글.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("Chinese Char") {
            const QString imagePath = QString::fromStdWString(L"绘画.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("France Char") {
            const QString imagePath = QString::fromStdWString(L"Français.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("Japenese Char") {
            const QString imagePath = QString::fromStdWString(L"データ.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("Emoji Heart") {
            const QString imagePath = QString::fromStdWString(L"♥♡.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("Emoji") {
            const QString imagePath = QString::fromStdWString(L"✌🤷‍♀️🤷‍♂️👀👀🤔.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("Mongolian Char") {
            const QString imagePath = QString::fromStdWString(L"өгөгдөл.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("arabic Char") {
            const QString imagePath = QString::fromStdWString(L"بيانات.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }

        SECTION("thailand Char") {
            const QString imagePath = QString::fromStdWString(L"จิตรกรรม.png");
            CHECK(!SaveAndLoadExceptionThrown(imagePath));
        }
    }
}
