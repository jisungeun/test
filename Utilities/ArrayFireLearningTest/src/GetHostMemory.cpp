#include <catch2/catch.hpp>

#include "arrayfire.h"
namespace GetHostMemoryTest {
    TEST_CASE("GetHostMemory") {
        SECTION("GetHostMemory") {
            const float hostData[10] = { 1,2,3,4,5,6,7,8,9,10 };
            const auto data = af::array(1, 10, hostData);

            const auto hostMemory = data.host<float>();

            auto dataIsSame{ true };

            for(auto i = 0; i < 10; ++i) {
                const auto ansData = hostData[i];
                const auto hostMemoryData = hostMemory[i];

                if (hostMemoryData != ansData) {
                    dataIsSame = false;
                    break;
                }
            }

            CHECK(dataIsSame);

            af::freeHost(hostMemory);
        }

        SECTION("3D data order") {
            const auto sizeX = 2;
            const auto sizeY = 3;
            const auto sizeZ = 4;

            const auto numberOfElements = sizeX * sizeY * sizeZ;

            const float ansData[numberOfElements] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 };
            auto dataArray = af::constant(0, sizeY, sizeX, sizeZ);

            for (auto i = 0; i < numberOfElements; ++i) {
                dataArray(i) = i;
            }

            //af_print(dataArray);

            std::shared_ptr<float[]> hostData(new float[numberOfElements]());

            dataArray.host(hostData.get());

            auto dataSame{ true };
            for (auto i = 0 ; i < numberOfElements; ++i) {
                const auto hostDataElement = hostData.get()[i];
                const auto ansDataElement = ansData[i];

                if (hostDataElement != ansDataElement) {
                    dataSame = false;
                    break;
                }
            }
            CHECK(dataSame);

        }
    }
}