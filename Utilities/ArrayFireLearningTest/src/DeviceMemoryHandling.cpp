#include <catch2/catch.hpp>
#include <iostream>

#include <QMap>
#include "arrayfire.h"

namespace DeviceMemoryHandling {
    TEST_CASE("Free Memory") {
        const dim_t GB = 1024 * 1024 * 1024;
        SECTION("out of memory situation") {
            const auto sizeOfMemory = dim_t{ 20 } *GB;

            const dim_t sizeX = 1024;
            const dim_t sizeY = 1024;
            const auto sizeZ = sizeOfMemory / sizeX / sizeY;

            auto outOfMemory{ false };
            try {
                auto arrayForMemory = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory.eval();

                af::deviceGC();

                auto arrayForMemory2 = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory2.eval();
            } catch (const af::exception& exception) {
                std::string exceptionContents = exception.what();
                outOfMemory = true;
            }
            CHECK(outOfMemory);
        }

        SECTION("default destructor -> pass") {
            const auto sizeOfMemory = dim_t{ 8 } *GB;

            const dim_t sizeX = 1024;
            const dim_t sizeY = 1024;
            const auto sizeZ = sizeOfMemory / sizeX / sizeY;

            auto noOutOfMemory{ true };
            try {
                {
                    auto arrayForMemory = af::constant(10, sizeY, sizeX, sizeZ, u8);
                    arrayForMemory.eval();
                }

                af::deviceGC();

                auto arrayForMemory2 = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory2.eval();
            } catch (const af::exception & exception) {
                std::string exceptionContents = exception.what();
                std::cout << exceptionContents;
                noOutOfMemory = false;
            }
            CHECK(noOutOfMemory);
        }

        SECTION("af::free -> fail") {
            const auto sizeOfMemory = dim_t{ 8 } *GB;

            const dim_t sizeX = 1024;
            const dim_t sizeY = 1024;
            const auto sizeZ = sizeOfMemory / sizeX / sizeY;

            auto outOfMemory{ false };
            try {
                auto arrayForMemory = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory.eval();

                const auto deviceMemoryPointer = arrayForMemory.device<uint8_t>();
                af::free(deviceMemoryPointer);

                af::deviceGC();

                auto arrayForMemory2 = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory2.eval();
            } catch (const af::exception & exception) {
                std::string exceptionContents = exception.what();
                outOfMemory = true;
            }
            CHECK(outOfMemory);
        }

        SECTION("replace array -> pass") {
            const auto sizeOfMemory = dim_t{ 8 } *GB;

            const dim_t sizeX = 1024;
            const dim_t sizeY = 1024;
            const auto sizeZ = sizeOfMemory / sizeX / sizeY;

            auto noOutOfMemory{ true };
            try {
                auto arrayForMemory = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory.eval();

                arrayForMemory = af::constant(0, 1, u8);
                arrayForMemory.eval();

                af::deviceGC();

                auto arrayForMemory2 = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory2.eval();
            } catch (const af::exception & exception) {
                std::string exceptionContents = exception.what();
                std::cout << exceptionContents;
                noOutOfMemory = false;
            }
            CHECK(noOutOfMemory);
        }

        SECTION("replace array -> pass") {
            const auto sizeOfMemory = dim_t{ 8 } *GB;

            const dim_t sizeX = 1024;
            const dim_t sizeY = 1024;
            const auto sizeZ = sizeOfMemory / sizeX / sizeY;

            auto noOutOfMemory{ true };
            try {
                auto arrayForMemory = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory.eval();

                af::deviceGC();

                arrayForMemory = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayForMemory.eval();

                af::deviceGC();
            } catch (const af::exception & exception) {
                std::string exceptionContents = exception.what();
                std::cout << exceptionContents;
                noOutOfMemory = false;
            }
            CHECK(noOutOfMemory);
        }
    }

    TEST_CASE("QMap<int32_t, af::array>") {
        const dim_t GB = 1024 * 1024 * 1024;
        const auto sizeOfMemory = dim_t{ 8 } *GB;

        const dim_t sizeX = 1024;
        const dim_t sizeY = 1024;
        const auto sizeZ = sizeOfMemory / sizeX / sizeY;

        SECTION("out of memory situation") {
            auto outOfMemory{ false };
            try {
                QMap<int32_t, af::array> arrayMap;
                arrayMap[0] = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayMap[0].eval();

                af::deviceGC();

                arrayMap[1] = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayMap[1].eval();
            } catch (const af::exception & exception) {
                std::string exceptionContents = exception.what();
                outOfMemory = true;
            }
            CHECK(outOfMemory);
        }

        SECTION("Map.clear() -> pass") {
            auto noOutOfMemory{ true };
            try {
                QMap<int32_t, af::array> arrayMap;
                arrayMap[0] = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayMap[0].eval();

                arrayMap.clear();
                af::deviceGC();

                arrayMap[1] = af::constant(10, sizeY, sizeX, sizeZ, u8);
                arrayMap[1].eval();
            } catch (const af::exception & exception) {
                std::string exceptionContents = exception.what();
                std::cout << exceptionContents;
                noOutOfMemory = false;
            }
            CHECK(noOutOfMemory);
        }
    }
}