#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace ArrayTypeTest {
    TEST_CASE("Allocation with bool Type Array : fail") {
        //const auto dataSize = 3;

        //auto boolArray = new bool[dataSize]();
        //boolArray[0] = true;
        //boolArray[1] = true;
        //boolArray[2] = false;

        //af::array boolAfArray(dataSize, boolArray);
        //CHECK(boolAfArray.type() == b8);

        //delete[] boolArray;
    }

    TEST_CASE("Allocation with int8_t Type Array : fail(compile error)") {
        //const auto dataSize = 3;

        //auto data = new int8_t[dataSize]();
        //data[0] = 1;
        //data[1] = 0;
        //data[2] = 2;

        //af::array afArray(dataSize, data);
        //const auto boolAfArray = afArray.as(b8);

        //CHECK(boolAfArray.type() == b8);

        //delete[] data;
    }

    TEST_CASE("Allocation with uint8_t Type array : pass") {
        const auto dataSize = 3;

        auto data = new uint8_t[dataSize]();
        data[0] = 1;
        data[1] = 0;
        data[2] = 2;

        af::array afArray(dataSize, data);
        const auto boolAfArray = afArray.as(b8);

        CHECK(boolAfArray.type() == b8);

        delete[] data;
    }

    TEST_CASE("Scalar<bool> : fail (compile error)") {
        //const auto dataSize = 3;

        //auto data = new uint8_t[dataSize]();
        //data[0] = 1;
        //data[1] = 0;
        //data[2] = 2;

        //af::array afArray(dataSize, data);
        //const auto boolAfArray = afArray.as(b8);

        //CHECK(boolAfArray.type() == b8);
        //CHECK(afArray(0).scalar<bool>() == true);
        //CHECK(afArray(1).scalar<bool>() == true);
        //CHECK(afArray(2).scalar<bool>() == true);

        //delete[] data;
    }

    TEST_CASE("Scalar<uint8_t> : pass") {
        const auto dataSize = 3;

        auto data = new uint8_t[dataSize]();
        data[0] = 1;
        data[1] = 0;
        data[2] = 2;

        af::array afArray(dataSize, data);
        CHECK(afArray(0).scalar<uint8_t>() == 1);
        CHECK(afArray(1).scalar<uint8_t>() == 0);
        CHECK(afArray(2).scalar<uint8_t>() == 2);

        delete[] data;
    }

    TEST_CASE("Scalar<uint8_t> for bool type array : fail (run time error)") {
        const auto dataSize = 3;

        auto data = new uint8_t[dataSize]();
        data[0] = 1;
        data[1] = 0;
        data[2] = 2;

        af::array afArray(dataSize, data);
        const auto boolAfArray = afArray.as(b8);

        CHECK(boolAfArray.type() == b8);
        auto exceptionThrown = false;
        try {
            boolAfArray(0).scalar<uint8_t>();
        } catch (af::exception) {
            exceptionThrown = true;
        }
        CHECK(exceptionThrown == true);

        delete[] data;
    }

    TEST_CASE("Scalar<char> for bool type array : pass, value changed (non-zero value -> true(1))") {
        const auto dataSize = 4;

        auto data = new int32_t[dataSize]();
        data[0] = 1;
        data[1] = 0;
        data[2] = 2;
        data[3] = -1;

        af::array afArray(dataSize, data);
        const auto boolAfArray = afArray.as(b8);
        CHECK(boolAfArray(0).scalar<char>() == 1);
        CHECK(boolAfArray(1).scalar<char>() == 0);
        CHECK(boolAfArray(2).scalar<char>() == 1);
        CHECK(boolAfArray(3).scalar<char>() == 1);

        delete[] data;
    }
}