#include <catch2/catch.hpp>
#include <chrono>
#include <iostream>

#include "arrayfire.h"

namespace MemoryCopyTimeTest {
    const auto numberOfIteration = 3;

    TEST_CASE("MemoryCopyTime") {
        SECTION("Host To Device") {
            const unsigned int sizeX = 1464;
            const unsigned int sizeY = 1464;
            const unsigned int sizeZ = 67;
            const unsigned int patterns = 4;
            const unsigned int sampleBackCount = 2;

            const auto memorySizeInMB = static_cast<float>(sizeX * sizeY * sizeZ * patterns * sampleBackCount * 2) / 1024 / 1024;

            const auto dataPointer = new uint16_t[sizeX * sizeY * sizeZ * patterns * sampleBackCount]();

            for (auto i = 0; i < numberOfIteration; ++i) {
                af::deviceGC();

                const auto hostToDeviceCopyStart = std::chrono::system_clock::now();
                auto data = af::array(sizeY, sizeX, sizeZ * patterns * sampleBackCount, dataPointer).as(f32);
                data.eval();
                const auto hostToDeviceCopyEnd = std::chrono::system_clock::now();
                const std::chrono::duration<double> hostToDeviceCopy = hostToDeviceCopyEnd - hostToDeviceCopyStart;

                const auto copySpeed = memorySizeInMB / hostToDeviceCopy.count();

                std::cout <<"Host to Device Copy " << hostToDeviceCopy.count() << " sec (" << memorySizeInMB << " MB) -> "
                << copySpeed<<" MB/sec" << std::endl;
            }
            delete[] dataPointer;
        }

        SECTION("Device To Host") {
            const unsigned int sizeX = 3500;
            const unsigned int sizeY = 3500;
            const unsigned int sizeZ = 100;

            const auto dataPointer = new uint16_t[sizeX * sizeY * sizeZ]();

            constexpr auto memorySizeInMB = static_cast<float>(sizeX * sizeY * sizeZ * 2) / 1024 / 1024;

            const auto deviceData = af::array(sizeY, sizeX, sizeZ, dataPointer);

            const auto copyPointer = new uint16_t[sizeX * sizeY * sizeZ]();

            for (auto i = 0; i < numberOfIteration; ++i) {
                af::deviceGC();

                const auto deviceToHostCopyStart = std::chrono::system_clock::now();
                deviceData.host(copyPointer);
                const auto deviceToHostCopyEnd = std::chrono::system_clock::now();
                const std::chrono::duration<double> deviceToHostCopy = deviceToHostCopyEnd - deviceToHostCopyStart;

                const auto copySpeed = memorySizeInMB / deviceToHostCopy.count();

                std::cout << "Device to Host copy " << deviceToHostCopy.count() << " sec(" << memorySizeInMB << " MB) -> "
                    << copySpeed << " MB/sec" << std::endl;
            }

            delete[] dataPointer;
            delete[] copyPointer;
        }
    }
}