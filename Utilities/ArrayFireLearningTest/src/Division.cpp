#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace DivisionTest {
TEST_CASE("Division") {
    SECTION("af::array(size (N,N)) / af::array(size (1)) -> fail") {
        const auto dataArray = af::constant(4, 10, 10);
        //af_print(dataArray);

        const auto divider = af::constant(2, 1);
        //af_print(divider);

        auto exceptionThrown = false;
        try {
            const auto dividedDataArray = dataArray / divider;
        } catch (af::exception) {
            exceptionThrown = true;
        }
        CHECK(exceptionThrown);
    }

    SECTION("af::array(size (N,N)) / af::array(size (N,N)") {
        const auto dataArray = af::constant(4, 10, 10);
        //af_print(dataArray);

        const auto divider = af::constant(2, 10, 10);
        //af_print(divider);

        const auto dividedDataArray = dataArray / divider;
        //af_print(dividedDataArray);

        auto dataIsDivided = true;
        for (auto i = 0; i < 100; ++i) {
            if (dividedDataArray(i).scalar<float>() != 2) {
                dataIsDivided = false;
            }
        }
        CHECK(dataIsDivided);
    }

    SECTION("inf : constant / 0") {
        const auto constantArray = af::constant(10, 1);
        const auto zeroArray = af::constant(0, 1);

        const auto infArray = constantArray / zeroArray;

        const auto infMask = af::isInf(infArray);


        CHECK(infMask.scalar<char>() == 1);
        CHECK(std::isinf(infArray.scalar<float>()) == true);
    }

    SECTION("nan : 0 / 0") {
        const auto zeroArray1 = af::constant(0, 1);
        const auto zeroArray2 = af::constant(0, 1);

        const auto nanArray = zeroArray1 / zeroArray2;
        const auto nanMask = af::isNaN(nanArray);

        CHECK(nanMask.scalar<char>() == 1);
        CHECK(std::isnan(nanArray.scalar<float>()) == true);
    }
}
}