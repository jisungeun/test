#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace MultiplyTest {
    TEST_CASE("Multiply") {
        SECTION("array(y,x,z) * array(1,1,z) : fail") {
            const auto sizeX = 1;
            const auto sizeY = 2;
            const auto sizeZ = 3;

            auto ansArray = af::constant(0, sizeY, sizeX, sizeZ);
            ansArray(0) = 0;
            ansArray(1) = 2;
            ansArray(2) = 0;
            ansArray(3) = 2;
            ansArray(4) = 0;
            ansArray(5) = 2;

            const auto meshGrid = range(af::dim4(sizeY, sizeX, sizeZ), 0);
            const auto multiplyingArray = af::constant(2, 1, 1, sizeZ);

            auto afExceptionThrown{ false };
            try {
                const auto multipliedArray = meshGrid * multiplyingArray;
            } catch( af::exception&) {
                afExceptionThrown = true;
            }
            CHECK(afExceptionThrown);
        }
    }
}