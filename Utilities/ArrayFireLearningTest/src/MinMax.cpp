#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace MinMaxTest {
    TEST_CASE("af::min()") {
        float data[12] = { 1,2,3,4,5,6,7,8,9,10,11,12 };

        SECTION("1D") {
            const af::array data1D(12, data);

            const auto minArray = af::min(data1D);

            CHECK(minArray.dims() == af::dim4{1,1,1,1});
            CHECK(minArray.scalar<float>() == 1);
        }

        SECTION("2D") {
            const af::array data2D(6, 2, data);

            const auto minArray = af::min(data2D);

            CHECK(minArray.dims() == af::dim4{ 1,2,1,1 });
            CHECK(minArray(0).scalar<float>() == 1);
            CHECK(minArray(1).scalar<float>() == 7);
        }

        SECTION("3D") {
            const af::array data3D(2, 2, 3, data);

            const auto minArray = af::min(data3D);

            CHECK(minArray.dims() == af::dim4{ 1,2,3,1 });
            CHECK(minArray(0).scalar<float>() == 1);
            CHECK(minArray(1).scalar<float>() == 3);
            CHECK(minArray(2).scalar<float>() == 5);
            CHECK(minArray(3).scalar<float>() == 7);
            CHECK(minArray(4).scalar<float>() == 9);
            CHECK(minArray(5).scalar<float>() == 11);
        }

        SECTION("EntireArray") {
            const af::array data3D(2, 2, 3, data);
            SECTION("device") {
                float minValue{};
                uint32_t index{};

                af::min(&minValue, &index, data3D);

                CHECK(minValue == 1);
                CHECK(index == 0);
            }
            SECTION("host") {
                float minValue = af::min<float>(data3D);
                CHECK(minValue == 1);
            }
        }
    }

    TEST_CASE("af::max()") {
        float data[12] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
        SECTION("1D") {
            const af::array data1D(12, data);

            const auto maxArray = af::max(data1D);

            CHECK(maxArray.dims() == af::dim4{ 1,1,1,1 });
            CHECK(maxArray.scalar<float>() == 12);
        }

        SECTION("2D") {
            const af::array data2D(6, 2, data);

            const auto maxArray = af::max(data2D);

            CHECK(maxArray.dims() == af::dim4{ 1,2,1,1 });
            CHECK(maxArray(0).scalar<float>() == 6);
            CHECK(maxArray(1).scalar<float>() == 12);
        }

        SECTION("3D") {
            const af::array data3D(2, 2, 3, data);

            const auto maxArray = af::max(data3D);

            CHECK(maxArray.dims() == af::dim4{ 1,2,3,1 });
            CHECK(maxArray(0).scalar<float>() == 2);
            CHECK(maxArray(1).scalar<float>() == 4);
            CHECK(maxArray(2).scalar<float>() == 6);
            CHECK(maxArray(3).scalar<float>() == 8);
            CHECK(maxArray(4).scalar<float>() == 10);
            CHECK(maxArray(5).scalar<float>() == 12);
        }

        SECTION("EntireArray") {
            const af::array data3D(2, 2, 3, data);
            SECTION("device") {
                float maxValue{};
                uint32_t index{};

                af::max(&maxValue, &index, data3D);

                CHECK(maxValue == 12);
                CHECK(index == 11);
            }
            SECTION("host") {
                float minValue = af::max<float>(data3D);
                CHECK(minValue == 12);
            }
        }
    }
}
