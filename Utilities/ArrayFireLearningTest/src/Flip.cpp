#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace FlipTest {
    const auto sizeX = 10;
    const auto sizeY = 12;

    TEST_CASE("Flip") {
        auto data = af::constant(0, sizeY, sizeX);

        SECTION("Flip dim 0") {
            for (auto i = 0; i < sizeY; ++i) {
                data(i, af::span) = i;
            }
            const auto flipDim0 = af::flip(data, 0);

            auto flipIsGood{ true };

            for (auto i = 0; i < sizeY; ++i) {
                const auto valueIsGood = flipDim0(i, 0).scalar<float>() == static_cast<float>(sizeY - 1 - i);
                if (!valueIsGood) {
                    flipIsGood = false;
                    break;
                }
            }
            CHECK(flipIsGood);
        }

        SECTION("Flip dim 1") {
            for (auto i = 0; i < sizeX; ++i) {
                data(af::span, i) = i;
            }
            const auto flipDim1 = af::flip(data, 1);

            auto flipIsGood{ true };

            for (auto i = 0; i < sizeX; ++i) {
                const auto valueIsGood = flipDim1(0, i).scalar<float>() == static_cast<float>(sizeX - 1 - i);
                if (!valueIsGood) {
                    flipIsGood = false;
                    break;
                }
            }
            CHECK(flipIsGood);
        }

        SECTION("Flip dim 0, dim 1") {
            auto value = 0;
            for (auto i = 0; i < sizeX; ++i) {
                for (auto j = 0; j < sizeY; ++j) {
                    data(j, i) = value++;
                }
            }
            const auto flipDim0Dim1 = af::flip(af::flip(data, 0), 1);

            auto flipIsGood{ true };

            value = 0;
            for (auto i = 0; i < sizeX; ++i) {
                for (auto j = 0; j < sizeY; ++j) {
                    const auto valueIsGood = flipDim0Dim1(j, i).scalar<float>() == static_cast<float>(sizeX * sizeY - 1 - value++);
                    if (!valueIsGood) {
                        flipIsGood = false;
                        break;
                    }
                }
                if (!flipIsGood) {
                    break;
                }
            }
            CHECK(flipIsGood);
        }
    }
}