#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace maskIndexing {
    TEST_CASE("MaskIndexing") {
        const auto sizeX = 10;
        const auto sizeY = 10;

        af::array dataArray(sizeY, sizeX, f32);

        for (auto j = 0; j < sizeY / 2; ++j) {
            for (auto i = 0; i < sizeX / 2; ++i) {
                dataArray(j, i) = ((i * (sizeY / 2)) + j) + 1;
            }
        }
        //af_print(dataArray);

        auto maskArray = (dataArray != 0);
        //af_print(maskArray);

        af::array indexedArray = dataArray(maskArray);
        //af_print(indexedArray);

        auto dataIsCorrect = true;
        for (auto i = 0; i < (sizeX / 2) * (sizeY / 2); ++i) {
            if (indexedArray(i).scalar<float>() != static_cast<float>(i + 1)) {
                dataIsCorrect = false;
            }
        }

        CHECK(dataIsCorrect);
    }

    TEST_CASE("Mask Indexing (numbers>1)") {
        const auto sizeX = 3;

        af::array dataArray(sizeX, f32);
        dataArray(0) = 1;
        dataArray(1) = 2;
        dataArray(2) = 3;

        af::array maskArray(sizeX, b8);
        maskArray(0) = 0;
        maskArray(1) = 1;
        maskArray(2) = 2;

        dataArray(maskArray) = 0;

        CHECK(dataArray(0).scalar<float>() == 1);
        CHECK(dataArray(1).scalar<float>() == 0);
        CHECK(dataArray(2).scalar<float>() == 0);
    }

    TEST_CASE("Indexing with indexArray") {
        const auto sizeX = 2;
        const auto sizeY = 3;
        const auto indexNumber = 2;

        auto data = af::constant(0, sizeY, sizeX);
        data(0) = 0;
        data(1) = 1;
        data(2) = 2;
        data(3) = 3;
        data(4) = 4;
        data(5) = 5;

        auto index = af::constant(0, indexNumber);
        index(0) = 1;
        index(1) = 3;

        auto indexedData = data(index).copy();

        CHECK(indexedData(0).scalar<float>() == 1);
        CHECK(indexedData(1).scalar<float>() == 3);

    }

    TEST_CASE("") {
        const auto sizeX = 2;
        const auto sizeY = 2;
        const auto sizeZ = 2;

        const auto numberOfIndex = 2;

        af::array data2d = af::constant(1, sizeY, sizeX);
        af::array data3d = af::constant(2, sizeY, sizeX, sizeZ);

        af::array index3d = af::constant(0, numberOfIndex);
        index3d(0) = 1;
        index3d(1) = 2;

        af::array mask2d = af::constant(0, sizeY, sizeX).as(b8);
        mask2d(2) = true;
        mask2d(3) = true;

        data3d(index3d) = data2d(mask2d);

        CHECK(data3d(0).scalar<float>() == 2);
        CHECK(data3d(1).scalar<float>() == 1);
        CHECK(data3d(2).scalar<float>() == 1);
        CHECK(data3d(3).scalar<float>() == 2);
        CHECK(data3d(4).scalar<float>() == 2);
        CHECK(data3d(5).scalar<float>() == 2);
        CHECK(data3d(6).scalar<float>() == 2);
        CHECK(data3d(7).scalar<float>() == 2);
    }
}