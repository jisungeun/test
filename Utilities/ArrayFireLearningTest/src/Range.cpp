#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace RangeTest {
    TEST_CASE("Range : (meshgrid - 1) in Matlab") {
        SECTION("2D") {
            const auto sizeX = 2;
            const auto sizeY = 3;

            auto ansMeshGridY = af::constant(0, sizeY, sizeX);
            ansMeshGridY(0) = 0;
            ansMeshGridY(1) = 1;
            ansMeshGridY(2) = 2;
            ansMeshGridY(3) = 0;
            ansMeshGridY(4) = 1;
            ansMeshGridY(5) = 2;

            auto ansMeshGridX = af::constant(0, sizeY, sizeX);
            ansMeshGridX(0) = 0;
            ansMeshGridX(1) = 0;
            ansMeshGridX(2) = 0;
            ansMeshGridX(3) = 1;
            ansMeshGridX(4) = 1;
            ansMeshGridX(5) = 1;

            const auto meshGridY = range(af::dim4(sizeY, sizeX), 0);
            const auto meshGridX = range(af::dim4(sizeY, sizeX), 1);

            CHECK(meshGridY(0).scalar<float>() == ansMeshGridY(0).scalar<float>());
            CHECK(meshGridY(1).scalar<float>() == ansMeshGridY(1).scalar<float>());
            CHECK(meshGridY(2).scalar<float>() == ansMeshGridY(2).scalar<float>());
            CHECK(meshGridY(3).scalar<float>() == ansMeshGridY(3).scalar<float>());
            CHECK(meshGridY(4).scalar<float>() == ansMeshGridY(4).scalar<float>());
            CHECK(meshGridY(5).scalar<float>() == ansMeshGridY(5).scalar<float>());

            CHECK(meshGridX(0).scalar<float>() == ansMeshGridX(0).scalar<float>());
            CHECK(meshGridX(1).scalar<float>() == ansMeshGridX(1).scalar<float>());
            CHECK(meshGridX(2).scalar<float>() == ansMeshGridX(2).scalar<float>());
            CHECK(meshGridX(3).scalar<float>() == ansMeshGridX(3).scalar<float>());
            CHECK(meshGridX(4).scalar<float>() == ansMeshGridX(4).scalar<float>());
            CHECK(meshGridX(5).scalar<float>() == ansMeshGridX(5).scalar<float>());
        }

        SECTION("3D") {
            const auto sizeX = 2;
            const auto sizeY = 3;
            const auto sizeZ = 2;

            auto ansMeshGridY = af::constant(0, sizeY, sizeX, sizeZ);
            ansMeshGridY(0) = 0;
            ansMeshGridY(1) = 1;
            ansMeshGridY(2) = 2;
            ansMeshGridY(3) = 0;
            ansMeshGridY(4) = 1;
            ansMeshGridY(5) = 2;
            ansMeshGridY(6) = 0;
            ansMeshGridY(7) = 1;
            ansMeshGridY(8) = 2;
            ansMeshGridY(9) = 0;
            ansMeshGridY(10) = 1;
            ansMeshGridY(11) = 2;

            auto ansMeshGridX = af::constant(0, sizeY, sizeX, sizeZ);
            ansMeshGridX(0) = 0;
            ansMeshGridX(1) = 0;
            ansMeshGridX(2) = 0;
            ansMeshGridX(3) = 1;
            ansMeshGridX(4) = 1;
            ansMeshGridX(5) = 1;
            ansMeshGridX(6) = 0;
            ansMeshGridX(7) = 0;
            ansMeshGridX(8) = 0;
            ansMeshGridX(9) = 1;
            ansMeshGridX(10) = 1;
            ansMeshGridX(11) = 1;

            auto ansMeshGridZ = af::constant(0, sizeY, sizeX, sizeZ);
            ansMeshGridZ(0) = 0;
            ansMeshGridZ(1) = 0;
            ansMeshGridZ(2) = 0;
            ansMeshGridZ(3) = 0;
            ansMeshGridZ(4) = 0;
            ansMeshGridZ(5) = 0;
            ansMeshGridZ(6) = 1;
            ansMeshGridZ(7) = 1;
            ansMeshGridZ(8) = 1;
            ansMeshGridZ(9) = 1;
            ansMeshGridZ(10) = 1;
            ansMeshGridZ(11) = 1;

            const auto meshGridY = range(af::dim4(sizeY, sizeX, sizeZ), 0);
            const auto meshGridX = range(af::dim4(sizeY, sizeX, sizeZ), 1);
            const auto meshGridZ = range(af::dim4(sizeY, sizeX, sizeZ), 2);

            CHECK(meshGridY(0).scalar<float>() == ansMeshGridY(0).scalar<float>());
            CHECK(meshGridY(1).scalar<float>() == ansMeshGridY(1).scalar<float>());
            CHECK(meshGridY(2).scalar<float>() == ansMeshGridY(2).scalar<float>());
            CHECK(meshGridY(3).scalar<float>() == ansMeshGridY(3).scalar<float>());
            CHECK(meshGridY(4).scalar<float>() == ansMeshGridY(4).scalar<float>());
            CHECK(meshGridY(5).scalar<float>() == ansMeshGridY(5).scalar<float>());
            CHECK(meshGridY(6).scalar<float>() == ansMeshGridY(6).scalar<float>());
            CHECK(meshGridY(7).scalar<float>() == ansMeshGridY(7).scalar<float>());
            CHECK(meshGridY(8).scalar<float>() == ansMeshGridY(8).scalar<float>());
            CHECK(meshGridY(9).scalar<float>() == ansMeshGridY(9).scalar<float>());
            CHECK(meshGridY(10).scalar<float>() == ansMeshGridY(10).scalar<float>());
            CHECK(meshGridY(11).scalar<float>() == ansMeshGridY(11).scalar<float>());

            CHECK(meshGridX(0).scalar<float>() == ansMeshGridX(0).scalar<float>());
            CHECK(meshGridX(1).scalar<float>() == ansMeshGridX(1).scalar<float>());
            CHECK(meshGridX(2).scalar<float>() == ansMeshGridX(2).scalar<float>());
            CHECK(meshGridX(3).scalar<float>() == ansMeshGridX(3).scalar<float>());
            CHECK(meshGridX(4).scalar<float>() == ansMeshGridX(4).scalar<float>());
            CHECK(meshGridX(5).scalar<float>() == ansMeshGridX(5).scalar<float>());
            CHECK(meshGridX(6).scalar<float>() == ansMeshGridX(6).scalar<float>());
            CHECK(meshGridX(7).scalar<float>() == ansMeshGridX(7).scalar<float>());
            CHECK(meshGridX(8).scalar<float>() == ansMeshGridX(8).scalar<float>());
            CHECK(meshGridX(9).scalar<float>() == ansMeshGridX(9).scalar<float>());
            CHECK(meshGridX(10).scalar<float>() == ansMeshGridX(10).scalar<float>());
            CHECK(meshGridX(11).scalar<float>() == ansMeshGridX(11).scalar<float>());

            CHECK(meshGridZ(0).scalar<float>() == ansMeshGridZ(0).scalar<float>());
            CHECK(meshGridZ(1).scalar<float>() == ansMeshGridZ(1).scalar<float>());
            CHECK(meshGridZ(2).scalar<float>() == ansMeshGridZ(2).scalar<float>());
            CHECK(meshGridZ(3).scalar<float>() == ansMeshGridZ(3).scalar<float>());
            CHECK(meshGridZ(4).scalar<float>() == ansMeshGridZ(4).scalar<float>());
            CHECK(meshGridZ(5).scalar<float>() == ansMeshGridZ(5).scalar<float>());
            CHECK(meshGridZ(6).scalar<float>() == ansMeshGridZ(6).scalar<float>());
            CHECK(meshGridZ(7).scalar<float>() == ansMeshGridZ(7).scalar<float>());
            CHECK(meshGridZ(8).scalar<float>() == ansMeshGridZ(8).scalar<float>());
            CHECK(meshGridZ(9).scalar<float>() == ansMeshGridZ(9).scalar<float>());
            CHECK(meshGridZ(10).scalar<float>() == ansMeshGridZ(10).scalar<float>());
            CHECK(meshGridZ(11).scalar<float>() == ansMeshGridZ(11).scalar<float>());
        }
    }
}