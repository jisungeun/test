#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace RegionsTest {
    TEST_CASE("af::regions()") {
        SECTION("2D : af::regions() supports 3-dimension") {
            const auto sizeX = 10;
            const auto sizeY = 10;

            const auto blobOffsetX = 3;
            const auto blobOffsetY = 3;

            const auto blobSizeX = 4;
            const auto blobSizeY = 4;

            auto binaryData = af::constant(0, sizeY, sizeX, b8);

            const auto blobIndicesX = af::seq(blobOffsetX, blobSizeX + blobOffsetX - 1);
            const auto blobIndicesY = af::seq(blobOffsetY, blobSizeY + blobOffsetY - 1);

            binaryData(blobIndicesY, blobIndicesX) = true;

            auto exceptionThrown = false;
            try {
                const auto regionData = af::regions(binaryData);
            } catch (const af::exception&) {
                exceptionThrown = true;
            }
            CHECK(exceptionThrown == false);
        }

        SECTION("3D : af::regions() doesn't support 3-dimension") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto sizeZ = 10;

            const auto blobOffsetX = 3;
            const auto blobOffsetY = 3;
            const auto blobOffsetZ = 3;

            const auto blobSizeX = 4;
            const auto blobSizeY = 4;
            const auto blobSizeZ = 4;

            auto binaryData = af::constant(0, sizeY, sizeX, sizeZ, b8);

            const auto blobIndicesX = af::seq(blobOffsetX, blobSizeX + blobOffsetX - 1);
            const auto blobIndicesY = af::seq(blobOffsetY, blobSizeY + blobOffsetY - 1);
            const auto blobIndicesZ = af::seq(blobOffsetZ, blobSizeZ + blobOffsetZ - 1);

            binaryData(blobIndicesY, blobIndicesX, blobIndicesZ) = true;

            auto exceptionThrown = false;
            try {
                const auto regionData = af::regions(binaryData);
            } catch (const af::exception&) {
                exceptionThrown = true;
            }
            CHECK(exceptionThrown == true);
        }
    }
    
}