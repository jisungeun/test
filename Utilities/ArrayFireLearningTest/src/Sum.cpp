#include <catch2/catch.hpp>

#include "arrayfire.h"

namespace SumTest {
    TEST_CASE("af::sum()") {
        SECTION("2D : sum all element") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto oneData = af::constant(1, sizeY, sizeX, s32);

            const auto sumResult = af::sum<int32_t>(oneData);
            CHECK(sumResult == sizeX * sizeY);
        }

        SECTION("3D : sum all element") {
            const auto sizeX = 10;
            const auto sizeY = 10;
            const auto sizeZ = 10;
            const auto oneData = af::constant(1, sizeY, sizeX, sizeZ, s32);

            const auto sumResult = af::sum<int32_t>(oneData);
            CHECK(sumResult == sizeX * sizeY * sizeZ);
        }
    }

}