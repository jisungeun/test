#include <catch2/catch.hpp>

#include <QUuid>
#include <QStringList>

namespace QUuidTest {
    TEST_CASE("QUuid : unit test") {
        SECTION("QUuid()") {
            QUuid qUuid;
            CHECK(&qUuid != nullptr);
        }
        SECTION("createUuid()") {
            auto createdUuid = QUuid::createUuid();

            const auto uuidVersion = createdUuid.version();
            CHECK(uuidVersion == QUuid::Version::Random);

            const auto uuidString = createdUuid.toString();
            CHECK(uuidString.length() == 38);

            const auto uuidStringList = uuidString.mid(1, 36).split("-");
            CHECK(uuidStringList.size() == 5);

            const auto timeLowString = uuidStringList[0];
            const auto timeMidString = uuidStringList[1];
            const auto timeHiAndVersionString = uuidStringList[2];
            const auto clockSeqHiAndResClockSeqLowString = uuidStringList[3];
            const auto nodeString = uuidStringList[4];
        }
    }
}