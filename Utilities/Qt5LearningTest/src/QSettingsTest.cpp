#include <catch2/catch.hpp>
#include <QSettings>
#include <QFile>
#include <QTextCodec>

namespace QSettingsTest {
    TEST_CASE("QSettings") {
        SECTION("ini format : data types") {
            const QString path = "QSettingsDataTypes.dat";
            if (QFile::exists(path)) {
                QFile::remove(path);
            }
            CHECK(!QFile::exists(path));

            constexpr int8_t integer8 = 8;
            constexpr int8_t minusInteger8 = -8;
            constexpr uint8_t unsignedInteger8 = 8;

            constexpr int16_t integer16 = 16;
            constexpr int16_t minusInteger16 = -16;
            constexpr uint16_t unsignedInteger16 = 16;

            constexpr int32_t integer32 = 32;
            constexpr int32_t minusInteger32 = -32;
            constexpr uint32_t unsignedInteger32 = 32;

            constexpr int64_t integer64 = 64;
            constexpr int64_t minusInteger64 = -64;
            constexpr uint64_t unsignedInteger64 = 64;

            constexpr float floatNumber = 1.234;
            constexpr float minusFloatNumber = -1.234;
            constexpr double doubleNumber = 5.678;
            constexpr double minusDoubleNumber = -5.678;
            
            {
                QSettings qs(path, QSettings::IniFormat);
                
                qs.setValue("int8_t", integer8);
                qs.setValue("-int8_t", minusInteger8);
                qs.setValue("uint8_t", unsignedInteger8);
                qs.setValue("int16_t", integer16);
                qs.setValue("-int16_t", minusInteger16);
                qs.setValue("uint16_t", unsignedInteger16);
                qs.setValue("int32_t", integer32);
                qs.setValue("-int32_t", minusInteger32);
                qs.setValue("uint32_t", unsignedInteger32);
                qs.setValue("int64_t", integer64);
                qs.setValue("-int64_t", minusInteger64);
                qs.setValue("uint64_t", unsignedInteger64);
                qs.setValue("float", floatNumber);
                qs.setValue("-float", minusFloatNumber);
                qs.setValue("double", doubleNumber);
                qs.setValue("-double", minusDoubleNumber);
            }

            CHECK(QFile::exists(path));
        }

        SECTION("ini format : read double or float written as QString") {
            SECTION("read as float from QString variable") {
                const QString path = "readingfloat.dat";
                if (QFile::exists(path)) {
                    QFile::remove(path);
                }
                CHECK(!QFile::exists(path));

                {
                    QSettings qs(path, QSettings::IniFormat);
                    qs.setValue("QString", "1.234");
                }
                CHECK(QFile::exists(path));

                {
                    QSettings qs(path, QSettings::IniFormat);
                    const auto resultValue = qs.value("QString").toDouble();
                    CHECK(resultValue == 1.234);
                }
            }

            SECTION("read as float from QString variable") {
                const QString path = "readingfloat.dat";
                if (QFile::exists(path)) {
                    QFile::remove(path);
                }
                CHECK(!QFile::exists(path));

                {
                    QSettings qs(path, QSettings::IniFormat);
                    qs.setValue("QString", "1.234");
                }
                CHECK(QFile::exists(path));

                {
                    QSettings qs(path, QSettings::IniFormat);
                    const auto resultValue = qs.value("QString").toFloat();
                    CHECK(std::round(resultValue*1000) == std::round(1.234f*1000));
                }
            }
        }

        SECTION("ini format : file is not created without setValue") {
            const QString path = "QSettings_ini.ini";
            if (QFile::exists(path)) {
                QFile::remove(path);
            }
            CHECK(!QFile::exists(path));
            {
                QSettings qs(path, QSettings::IniFormat);
            }
            
            CHECK(!QFile::exists(path));
        }

        SECTION("ini format : file creation only setValue() -> file is created") {
            const QString path = "QSettings_ini.ini";
            if (QFile::exists(path)) {
                QFile::remove(path);
            }
            CHECK(!QFile::exists(path));
            {
                const QVariant value = uint32_t(10);

                QSettings qs(path, QSettings::IniFormat);
                qs.setValue("testValue", value);
            }

            CHECK(QFile::exists(path));
        }

        SECTION("ini format : file creation only beginGroup()/endGroup() -> file is not created") {
            const QString path = "QSettings_ini.ini";
            if (QFile::exists(path)) {
                QFile::remove(path);
            }
            CHECK(!QFile::exists(path));
            {
                QSettings qs(path, QSettings::IniFormat);
                qs.beginGroup("testGroup");
                qs.endGroup();
            }

            CHECK(!QFile::exists(path));
        }

        SECTION("ini format : write and read") {
            const QString path = "QSettings_ini.ini";
            QVariant readValue;
            {
                QSettings qs(path, QSettings::IniFormat);
                const QVariant value = uint32_t(10);

                qs.beginGroup("group1");
                qs.setValue("value1", value);
                qs.endGroup();

                qs.beginGroup("group1");
                readValue = qs.value("value1");
                qs.endGroup();
            }

            CHECK(readValue.toUInt() == 10);
        }

        SECTION("ini format : write QString") {
            const QString path = "QSettings_ini.ini";
            QVariant readValue;
            {
                QSettings qs(path, QSettings::IniFormat);
                const QVariant value = QString{"TestQString"};

                qs.beginGroup("group1");
                qs.setValue("QString", value);
                qs.endGroup();

                qs.beginGroup("group1");
                readValue = qs.value("QString");
                qs.endGroup();
            }

            CHECK(readValue.toString() == QString{ "TestQString" });
        }

        SECTION("ini format : read value without beginGroup() -> fail") {
            const QString path = "QSettings_ini.ini";
            QVariant readValue;
            {
                QSettings qs(path, QSettings::IniFormat);
                const QVariant value = QString{ "TestQString" };

                qs.beginGroup("group1");
                qs.setValue("QString", value);
                qs.endGroup();
            }
            {
                QSettings qs(path, QSettings::IniFormat);
                readValue = qs.value("QString");
            }
            CHECK(readValue.toString().isEmpty());

        }

        SECTION("ini format : boolean") {
            const QString path = "QSettings_ini.ini";

            {
                QSettings qs(path, QSettings::IniFormat);
                qs.beginGroup("booleanGroup");
                qs.setValue("BooleanTrue", true);
                qs.setValue("BooleanFalse", false);
                qs.endGroup();
            }

            QVariant readTrue, readFalse;
            {
                QSettings qs(path, QSettings::IniFormat);
                qs.beginGroup("booleanGroup");
                readTrue = qs.value("BooleanTrue");
                readFalse = qs.value("BooleanFalse");
                qs.endGroup();
            }

            CHECK(readTrue.toBool() == true);
            CHECK(readFalse.toBool() == false);
            CHECK(readTrue.toString() == "true");
            CHECK(readFalse.toString() == "false");
            CHECK(readTrue.toInt() == 1);
            CHECK(readFalse.toInt() == 0);
        }

        SECTION("ini format : read value with slash(/)") {
            const QString path = "QSettings_ini.ini";
            {
                QSettings qs(path, QSettings::IniFormat);
                qs.beginGroup("slashGroup");
                qs.setValue("value1", int32_t(10));
                qs.endGroup();
            }

            QVariant readValue;
            {
                QSettings qs(path, QSettings::IniFormat);
                readValue = qs.value("slashGroup/value1",int32_t(0));
            }

            CHECK(readValue.toInt() == 10);
        }

        SECTION("ini format : reading non-exist valueName -> return default QVariant") {
            const QString path = "QSettings_ini.ini";
            QSettings qs(path, QSettings::IniFormat);
            qs.clear();
            const auto resultValue = qs.value("value1").toInt();
            CHECK(resultValue == 0);
        }

        SECTION("ini format : read multibyte (using setIniCodec)") {
            const QString path = "QSettingsKorean.ini";
            const QString valueString = QString::fromStdWString(L"123456.987654.�ѱ�.100"); // 230410.094300.\xc2e4.001
            const int32_t valueInt = 123;
            const double valueDouble = 123.456;
            const bool valueBool = true;

            if (QFile::exists(path)) {
                QFile::remove(path);
            }

            {
                QSettings qs(path, QSettings::IniFormat);
                qs.setIniCodec(QTextCodec::codecForName("UTF-8"));
                qs.setValue("section/keyString", valueString);
                qs.setValue("section/keyInt", valueInt);
                qs.setValue("section/keyDouble", valueDouble);
                qs.setValue("section/keyBool", valueBool);
            }

            {
                QSettings qs(path, QSettings::IniFormat);
                const QString readFailString = "readFail";
                const bool readFailBool = false;

                bool okDouble = false;
                bool okInt = false;
                const auto readString = qs.value("section/keyString", readFailString).toString();
                const auto readInt = qs.value("section/keyInt").toInt(&okInt);
                const auto readDouble = qs.value("section/keyDouble").toDouble(&okDouble);
                const auto readBool = qs.value("section/keyBool", readFailBool).toBool();

                CHECK(okInt == true);
                CHECK(okDouble == true);

                CHECK(readString == valueString);
                CHECK(readInt == valueInt);
                CHECK(readDouble == valueDouble);
                CHECK(readBool == valueBool);
            }
        }

        SECTION("ini format : read multibyte (using setIniCodec)") {
            const QString path = "QSettingsKorean.ini";
            const QString valueString = QString::fromStdWString(L"123456.987654.�ѱ�.100"); // 230410.094300.\xc2e4.001
            const int32_t valueInt = 123;
            const double valueDouble = 123.456;
            const bool valueBool = true;

            if (QFile::exists(path)) {
                QFile::remove(path);
            }

            {
                QSettings qs(path, QSettings::IniFormat);
                qs.setValue("section/keyString", valueString);
                qs.setValue("section/keyInt", valueInt);
                qs.setValue("section/keyDouble", valueDouble);
                qs.setValue("section/keyBool", valueBool);
            }

            {
                QSettings qs(path, QSettings::IniFormat);
                const QString readFailString = "readFail";
                const bool readFailBool = false;

                bool okDouble = false;
                bool okInt = false;
                const auto readString = qs.value("section/keyString", readFailString).toString();
                const auto readInt = qs.value("section/keyInt").toInt(&okInt);
                const auto readDouble = qs.value("section/keyDouble").toDouble(&okDouble);
                const auto readBool = qs.value("section/keyBool", readFailBool).toBool();

                CHECK(okInt == true);
                CHECK(okDouble == true);

                CHECK(readString == valueString);
                CHECK(readInt == valueInt);
                CHECK(readDouble == valueDouble);
                CHECK(readBool == valueBool);
            }
        }
    }
}
