#include <catch2/catch.hpp>
#include <fstream>
#include <QDir>

namespace QDirTest {
    TEST_CASE("QDir") {
        SECTION("mkpath() / rmpath() : rmpath can remove directory") {
            const QString tempFolderDirectoryPath = "QDirTestmkpath";
            CHECK(QDir(tempFolderDirectoryPath).exists() == false);

            QDir().mkpath(tempFolderDirectoryPath);

            CHECK(QDir(tempFolderDirectoryPath).exists() == true);

            QDir().rmpath(tempFolderDirectoryPath);
            CHECK(QDir(tempFolderDirectoryPath).exists() == false);
        }

        SECTION("exists(QString)") {
            const QString tempFolderDirectory = "QDirTest";
            if (QDir(tempFolderDirectory).exists()) {
                QDir().rmpath(tempFolderDirectory);
            }
            CHECK(QDir(tempFolderDirectory).exists() == false);

            QDir().mkpath(tempFolderDirectory);
            CHECK(QDir(tempFolderDirectory).exists() == true);
        }

        SECTION("exists()") {
            const QString tempFolderDirectory = "QDirTest";
            if (QDir(tempFolderDirectory).exists()) {
                QDir().rmpath(tempFolderDirectory);
            }

            CHECK(QDir().exists(tempFolderDirectory) == false);

            QDir().mkpath(tempFolderDirectory);
            CHECK(QDir().exists(tempFolderDirectory) == true);
        }

        SECTION("removeRecursively() : removeRecursively removes target directory") {
            const QString tempFolderDirectoryPath = "QDirTestremoveRecursively";
            CHECK(QDir(tempFolderDirectoryPath).exists() == false);

            QDir().mkpath(tempFolderDirectoryPath);

            CHECK(QDir(tempFolderDirectoryPath).exists() == true);

            QDir(tempFolderDirectoryPath).removeRecursively();

            CHECK(QDir(tempFolderDirectoryPath).exists() == false);
        }

        SECTION("removeRecursively() : removeRecursively removes target directory and sub-directory and files") {
            const QString tempFolderDirectoryPath = "QDirTestremoveRecursively";
            CHECK(QDir(tempFolderDirectoryPath).exists() == false);

            QDir().mkpath(tempFolderDirectoryPath);

            const auto subDirectoryPath = QString("%1/subDirectory").arg(tempFolderDirectoryPath);
            QDir().mkpath(subDirectoryPath);
            CHECK(QDir(subDirectoryPath).exists() == true);

            const auto filePath = QString("%1/file.txt").arg(tempFolderDirectoryPath);
            std::ofstream writeFile(filePath.toLocal8Bit().constData(), std::ios::trunc | std::ios::binary);
            std::shared_ptr<char[]> dummyTextData(new char[100]());
            writeFile.write(dummyTextData.get(), sizeof(char) * 100);
            writeFile.close();
            CHECK(QFile::exists(filePath) == true);

            CHECK(QDir(tempFolderDirectoryPath).exists() == true);

            QDir(tempFolderDirectoryPath).removeRecursively();

            CHECK(QDir(tempFolderDirectoryPath).exists() == false);
        }

        SECTION("cleanPath()") {
            CHECK(QDir::cleanPath("C:\\Test\\Test").compare("C:/Test/Test") == 0);
            CHECK(QDir::cleanPath("C:/Test/../Test").compare("C:/Test") == 0);
            CHECK(QDir::cleanPath("C:/Test/Test/.").compare("C:/Test/Test") == 0);

            CHECK(QDir::cleanPath("C:/Test./Test").compare("C:/Test/Test") == -1);  //Not remove dot(.) at the end of directory
        }
    }
}