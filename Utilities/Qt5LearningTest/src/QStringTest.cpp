#include <catch2/catch.hpp>

#include <QString>
#include <iostream>
#include <locale>

#define KOREANString "한글"
#define KOREANWString L"한글"

TEST_CASE("QString") {
    SECTION("including Korean") {
        const QString includingKorean = KOREANString;
        SECTION("initialized with Korean") {
            CHECK(includingKorean == KOREANString);
        }

        SECTION("toStdString()") {
            const auto stdString = includingKorean.toStdString();

            CHECK(stdString != KOREANString);
        }

        SECTION("toStdWString()") {
            const auto stdWString = includingKorean.toStdWString();

            CHECK(stdWString.compare(KOREANWString) == true);
        }
    }
}