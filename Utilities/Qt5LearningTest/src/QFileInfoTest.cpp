#include <catch2/catch.hpp>

#include <QFileInfo>
#include <QDir>

namespace QFileInfoTest {
    TEST_CASE("QFileInfo") {
        SECTION("NonExist filePath in non-exist dir") {
            const QString filePath = "D:/abcde/abcd/a";

            QFileInfo fileInfo(filePath);

            const auto nonExistDir = fileInfo.dir();

            CHECK(nonExistDir.exists() == false);
            CHECK(nonExistDir.path() == "D:/abcde/abcd");
        }
    }
}