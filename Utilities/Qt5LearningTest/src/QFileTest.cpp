#include <catch2/catch.hpp>

#include <QDir>
#include <QFile>

namespace QFileTest {
    TEST_CASE("QFile") {
        SECTION("exists(\"\")") {
            const auto emptyPathExists = QFile::exists("");
            CHECK(emptyPathExists == false);
        }

        SECTION("exists(folderPath)") {
            const QString folderPath = "QFileFolder";
            if (QDir().exists(folderPath)) {
                QDir().rmpath(folderPath);
            }
            const auto mkPathResult = QDir().mkpath(folderPath);
            CHECK(mkPathResult == true);

            CHECK(QFile::exists(folderPath) == true);
        }
    }
}