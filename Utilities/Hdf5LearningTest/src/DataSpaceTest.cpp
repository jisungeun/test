#include <catch2/catch.hpp>

#include "H5Cpp.h"

namespace H5DataSpaceTest {
    TEST_CASE("H5DataSpace") {
        SECTION("getSimpleExtentNdims()") {
            SECTION("rank 3") {
                constexpr auto rank = 3;
                constexpr hsize_t dims[rank] = { 1,2,3 };

                H5::DataSpace dataSpace(rank, dims);
                CHECK(dataSpace.getSimpleExtentNdims() == 3);
                dataSpace.close();
            }
            SECTION("rank 2") {
                constexpr auto rank = 2;
                constexpr hsize_t dims[rank] = { 1,2 };

                H5::DataSpace dataSpace(rank, dims);
                CHECK(dataSpace.getSimpleExtentNdims() == 2);
                dataSpace.close();
            }
        }
        SECTION("getSimpleExtentNPoints()") {
            SECTION("rank 3") {
                constexpr auto rank = 3;
                constexpr hsize_t dims[rank] = { 1,2,3 };

                H5::DataSpace dataSpace(rank, dims);
                CHECK(dataSpace.getSimpleExtentNpoints() == 6);
                dataSpace.close();
            }
            SECTION("rank 2") {
                constexpr auto rank = 2;
                constexpr hsize_t dims[rank] = { 1,2 };

                H5::DataSpace dataSpace(rank, dims);
                CHECK(dataSpace.getSimpleExtentNpoints() == 2);
                dataSpace.close();
            }
        }
    }
}