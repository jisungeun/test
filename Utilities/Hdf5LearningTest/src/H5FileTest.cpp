#include <catch2/catch.hpp>

#include "H5Cpp.h"
#include <QFile>
#include <QDir>

namespace H5FileTest {
    TEST_CASE("H5File") {
        SECTION("including invalid character in file name") {
            SECTION("alphabet : pass") {
                bool exceptionThrown = false;
                try {
                    const H5::H5File file("abc", H5F_ACC_TRUNC);
                } catch(const H5::Exception&) {
                    exceptionThrown = true;
                }

                CHECK(exceptionThrown == false);
            }

            SECTION("only number : pass") {
                bool exceptionThrown = false;
                try {
                    const H5::H5File file("0123", H5F_ACC_TRUNC);
                } catch (const H5::Exception&) {
                    exceptionThrown = true;
                }

                CHECK(exceptionThrown == false);
            }

            SECTION("alphabet, number : pass") {
                bool exceptionThrown = false;
                try {
                    const H5::H5File file("abc0123", H5F_ACC_TRUNC);
                } catch (const H5::Exception&) {
                    exceptionThrown = true;
                }

                CHECK(exceptionThrown == false);
            }

            SECTION("alphabet, space : pass") {
                bool exceptionThrown = false;
                try {
                    const H5::H5File file("a b c", H5F_ACC_TRUNC);
                } catch (const H5::Exception&) {
                    exceptionThrown = true;
                }

                CHECK(exceptionThrown == false);
            }
            SECTION("alphabet, dot : pass") {
                bool exceptionThrown = false;
                try {
                    const H5::H5File file("a.b.c", H5F_ACC_TRUNC);
                } catch (const H5::Exception&) {
                    exceptionThrown = true;
                }

                CHECK(exceptionThrown == false);
            }
            SECTION("alphabet, minus Sign : pass") {
                bool exceptionThrown = false;
                try {
                    const H5::H5File file("a-b-c", H5F_ACC_TRUNC);
                } catch (const H5::Exception&) {
                    exceptionThrown = true;
                }

                CHECK(exceptionThrown == false);
            }
            SECTION("alphabet, plus Sign : pass") {
                bool exceptionThrown = false;
                try {
                    const H5::H5File file("a+b+c", H5F_ACC_TRUNC);
                } catch (const H5::Exception&) {
                    exceptionThrown = true;
                }

                CHECK(exceptionThrown == false);
            }
        }
        SECTION("Long Path Length") {
            const QString extremelyLongFolderPath = "\\\\?\\E:\\temp\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890\\12345678901234567890";

            CHECK(QDir().mkpath(extremelyLongFolderPath));

            const QString testFilePath = extremelyLongFolderPath + "\\hdf5File.h5";

            const H5::H5File file(testFilePath.toStdString(), H5F_ACC_TRUNC);
            const auto dataGroup = file.createGroup("Data");
            constexpr int32_t rank = 3;
            hsize_t dims[rank] = { 10,10,10 };
            auto data = new uint16_t[10 * 10 * 10]();
            for (auto i = 0; i < 1000; ++i) {
                data[i] = i;
            }

            const H5::DataSpace dataSpace{ rank, dims };
            const auto dataSet = dataGroup.createDataSet("000000", H5::PredType::NATIVE_UINT16, dataSpace);
            dataSet.write(data, H5::PredType::NATIVE_UINT16);
            
            delete[] data;
        }
    }
}