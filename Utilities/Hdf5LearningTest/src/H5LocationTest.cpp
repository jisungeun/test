#include <catch2/catch.hpp>

#include "H5Cpp.h"

namespace H5LocationTest {
    TEST_CASE("H5Location : practical test") {
        SECTION("Get Group Existence : group depth 1") {
            const std::string testH5File = "testH5File.h5";

            H5::H5File file(testH5File, H5F_ACC_TRUNC);
            CHECK(file.nameExists("Group1") == false);
            auto group1 = file.createGroup("Group1");
            CHECK(file.nameExists("Group1") == true);

            group1.close();
            file.close();
        }

        SECTION("Get Group Existence : group depth 2") {
            const std::string testH5File = "testH5File.h5";

            H5::H5File file(testH5File, H5F_ACC_TRUNC);
            auto group1 = file.createGroup("Group1");
            auto group2 = group1.createGroup("Group2");
            CHECK(file.nameExists("Group1/Group2") == true);

            group2.close();
            group1.close();
            file.close();
        }

        SECTION("Get Group Existence : group depth 2, second group doens't exist") {
            const std::string testH5File = "testH5File.h5";

            H5::H5File file(testH5File, H5F_ACC_TRUNC);
            auto group1 = file.createGroup("Group1");
            CHECK(file.nameExists("Group1/Group2") == false);

            group1.close();
            file.close();
        }

        SECTION("Get Group Existence : group depth 2 in Group") {
            const std::string testH5File = "testH5File.h5";

            H5::H5File file(testH5File, H5F_ACC_TRUNC);
            auto group1 = file.createGroup("Group1");
            auto group2 = group1.createGroup("Group2");
            auto group3 = group2.createGroup("Group3");
            CHECK(group1.nameExists("Group2/Group3") == true);

            group3.close();
            group2.close();
            group1.close();
            file.close();
        }

        SECTION("Create Group : group depth 2 -> fail") {
            const std::string testH5File = "testH5File.h5";

            H5::H5File file(testH5File, H5F_ACC_TRUNC);

            bool exceptionThrown1 = false;
            try {
                auto group2 = file.createGroup("Group1/Group2");
                group2.close();
            } catch (const H5::Exception&) {
                exceptionThrown1 = true;
            }

            CHECK(exceptionThrown1 == true);

            bool exceptionThrown2 = false;
            try {
                file.nameExists("Group1/Group2");
            } catch (const H5::Exception&) {
                exceptionThrown2 = true;
            }
            CHECK(exceptionThrown2 == true);

            file.close();
        }


        SECTION("Get DataSet Existence : depth 1") {
            const std::string testH5File = "testH5File.h5";

            H5::H5File file(testH5File, H5F_ACC_TRUNC);
            hsize_t dims[1] = { 1 };
            auto dataSet1 = file.createDataSet("DataSet1", H5::PredType::NATIVE_FLOAT, H5::DataSpace{ 1,dims });

            CHECK(file.nameExists("DataSet1") == true);

            dataSet1.close();
            file.close();
        }

        SECTION("Get DataSet Existence : depth 2") {
            const std::string testH5File = "testH5File.h5";

            H5::H5File file(testH5File, H5F_ACC_TRUNC);

            auto group1 = file.createGroup("Group1");

            hsize_t dims[1] = { 1 };
            auto dataSet1 = group1.createDataSet("DataSet1", H5::PredType::NATIVE_FLOAT, H5::DataSpace{ 1,dims });

            CHECK(file.nameExists("Group1/DataSet1") == true);

            dataSet1.close();
            group1.close();
            file.close();
        }

    }
}