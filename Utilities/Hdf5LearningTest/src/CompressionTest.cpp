#include <catch2/catch.hpp>
#include "H5Cpp.h"
#include "H5Zpublic.h"

namespace CompressionTest {
    TEST_CASE("including compression library") {
        SECTION("zlib including") {
            const auto zlibIncluded = H5Zfilter_avail(H5Z_FILTER_DEFLATE);
            CHECK(zlibIncluded > 0);
        }

        SECTION("szip including") {
            const auto szipIncluded = H5Zfilter_avail(H5Z_FILTER_SZIP);
            CHECK(szipIncluded > 0);
        }

    }
}