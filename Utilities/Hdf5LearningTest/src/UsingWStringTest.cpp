#include <catch2/catch.hpp>

#include <H5Cpp.h>
#include <QString>
#include <QDir>


#define KoreanWString L"한글";
#define EnglishString L"English";

TEST_CASE("UsingWStringTest") {
    SECTION("H5File Creation") {
        SECTION("When File Name is non-English wstring : impossible, assertion(no Exception)") {
            const std::wstring fileName = KoreanWString;
            const QString filePath = QString::fromStdWString(fileName + L".h5");

            //assertion
            //H5::H5File file{ filePath.toLocal8Bit().toStdString(), H5F_ACC_TRUNC };
        }

        SECTION("When path has non-English wstring") {
            const QString parentFolderPath = "D:/temp2";
            const std::wstring folderName = KoreanWString;

            const QString folderPath = parentFolderPath + "/" + QString::fromStdWString(folderName);

            if (!QDir().exists(folderPath)) {
                REQUIRE(QDir().mkpath(folderPath) == true);
            }

            const std::wstring fileName = EnglishString;
            const QString filePath = folderPath + "/" + QString::fromStdWString(fileName) + ".h5";

            {
                const H5::H5File file{ filePath.toStdString(), H5F_ACC_TRUNC };
                const auto group = file.createGroup("Data");
            }

            CHECK(QFile::exists(filePath));
            REQUIRE(QFile::remove(filePath));
        }
    }

    SECTION("H5File Open") {
        const QString parentFolderPath = "D:/temp2";
        const std::wstring folderName = KoreanWString;

        const QString folderPath = parentFolderPath + "/" + QString::fromStdWString(folderName);

        if (!QDir().exists(folderPath)) {
            REQUIRE(QDir().mkpath(folderPath) == true);
        }

        const std::wstring fileName = EnglishString;
        const QString filePath = folderPath + "/" + QString::fromStdWString(fileName) + ".h5";

        {
            const H5::H5File file{ filePath.toStdString(), H5F_ACC_TRUNC };
            const auto group = file.createGroup("Data");
        }

        REQUIRE(QFile::exists(filePath));

        const std::wstring koreanFileName = KoreanWString;

        QString filePathChangedToKorean = filePath;
        filePathChangedToKorean.replace(QString::fromStdWString(fileName), QString::fromStdWString(koreanFileName));

        REQUIRE(QFile::rename(filePath, filePathChangedToKorean));
        REQUIRE(QFile::exists(filePathChangedToKorean));

        {
            const H5::H5File file{ filePathChangedToKorean.toStdString(), H5F_ACC_RDONLY };
            const auto group = file.openGroup("Data");
        }
    }
}