#include <catch2/catch.hpp>

#include "H5Cpp.h"

namespace DataTypeTest {
    TEST_CASE("DataTypeTest") {
        SECTION("H5::PredType comparison with DataType of DataSet") {
            H5::H5File file("dataTypeTest.h5", H5F_ACC_TRUNC);
            SECTION("H5::PredType::NATIVE_FLOAT") {
                auto dataSet = file.createDataSet("dataSet", H5::PredType::NATIVE_FLOAT, H5::DataSpace{});
                auto dataType = dataSet.getDataType();

                CHECK(dataType == H5::PredType::NATIVE_FLOAT);
                dataSet.close();
                dataType.close();
            }

            SECTION("H5::PredType::NATIVE_UINT8") {
                auto dataSet = file.createDataSet("dataSet", H5::PredType::NATIVE_UINT8, H5::DataSpace{});
                auto dataType = dataSet.getDataType();

                CHECK(dataType == H5::PredType::NATIVE_UINT8);
                dataSet.close();
                dataType.close();
            }
            file.close();
        }
    }
}