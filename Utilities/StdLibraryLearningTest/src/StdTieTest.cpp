#include <catch2/catch.hpp>

#include <tuple>

namespace StdTieTest {
    TEST_CASE("std::tie") {
        SECTION("tuple to tuple") {
            SECTION("same data types") {
                std::tuple<int32_t, int32_t, int32_t> srcTuple{ 1,2,3 };
                std::tuple<int32_t, int32_t, int32_t> destTuple{ 0,0,0 };

                std::tie(destTuple) = srcTuple;

                CHECK(std::get<0>(destTuple) == 1);
                CHECK(std::get<1>(destTuple) == 2);
                CHECK(std::get<2>(destTuple) == 3);
            }
            SECTION("different data types") {
                std::tuple<int32_t, double, std::string> srcTuple{ 1,2,"3" };
                std::tuple<int32_t, double, std::string> destTuple{ 0,0,"0" };

                std::tie(destTuple) = srcTuple;

                CHECK(std::get<0>(destTuple) == 1);
                CHECK(std::get<1>(destTuple) == 2);
                CHECK(std::get<2>(destTuple) == "3");
            }
        }

        SECTION("tuple to each instance") {
            std::tuple<int32_t, double, std::string> srcTuple{ 1,2,"3" };

            int32_t var1 = 0;
            double var2 = 0;
            std::string var3 = "3";
            
            std::tie(var1, var2, var3) = srcTuple;

            CHECK(var1 == 1);
            CHECK(var2 == 2);
            CHECK(var3 == "3");
        }


        SECTION("struct to struct : Compile error") {
            SECTION("same data types") {
                struct SrcStruct {
                    int32_t var1{ 1 };
                    int32_t var2{ 2 };
                    int32_t var3{ 3 };
                };

                struct DestStruct {
                    int32_t var1{ 0 };
                    int32_t var2{ 0 };
                    int32_t var3{ 0 };
                };

                SrcStruct srcStruct{};
                DestStruct destStruct{};

                // struct cannot be used for std::tie()
                //std::tie(destStruct.var1, destStruct.var2, destStruct.var3) = srcStruct;
            }

            SECTION("different data types") {
                struct SrcStruct {
                    int32_t var1{ 1 };
                    double var2{ 2 };
                    std::string var3{ "3" };
                };

                struct DestStruct {
                    int32_t var1{ 0 };
                    double var2{ 0 };
                    std::string var3{ "0" };
                };

                SrcStruct srcStruct{};
                DestStruct destStruct{};

                // struct cannot be used for std::tie()
                //std::tie(destStruct.var1, destStruct.var2, destStruct.var3) = srcStruct;
            }
        }

        SECTION("struct to Instance : Compile error") {
            SECTION("struct has same data types") {
                struct TestStruct {
                    int32_t var1{ 1 };
                    int32_t var2{ 2 };
                    int32_t var3{ 3 };
                };

                int32_t resultVar1{ 0 };
                int32_t resultVar2{ 0 };
                int32_t resultVar3{ 0 };

                TestStruct testStruct;

                // struct cannot be used for std::tie()
                //std::tie(resultVar1, resultVar2, resultVar3) = testStruct;
            }

            SECTION("struct has different data types") {
                struct TestStruct {
                    int32_t var1{ 1 };
                    double var2{ 2 };
                    std::string var3{ "3" };
                };

                int32_t resultVar1{ 0 };
                double resultVar2{ 0 };
                std::string resultVar3{ "0" };

                TestStruct testStruct;

                // struct cannot be used for std::tie()
                //std::tie(resultVar1, resultVar2, resultVar3) = testStruct;
            }
        }
    }
}