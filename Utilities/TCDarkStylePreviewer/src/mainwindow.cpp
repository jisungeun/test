#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QListView>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->buttonsGroupBox, &QGroupBox::toggled, [=](bool on){
        auto children = ui->buttonsGroupBox->findChildren<QPushButton*>();
        for (auto child : children) {
            child->setEnabled(on);
        }
    });

    ui->squareLineButton->setObjectName("bt-square-line");
    ui->squreGray500Button->setObjectName("bt-square-gray500");
    ui->squrePrimaryButton->setObjectName("bt-square-primary");
    ui->roundGray700Button->setObjectName("bt-round-gray700");
    ui->roundGray500Button->setObjectName("bt-round-gray500");
    ui->arrowLeftButton->setObjectName("bt-arrow-left");
    ui->arrowRightButton->setObjectName("bt-arrow-right");
    ui->openButton->setObjectName("bt-open-prj");
    ui->deleteButton->setObjectName("bt-delete");
    ui->renameButton->setObjectName("bt-rename");
    ui->runButton->setObjectName("bt-run");
    ui->infoButton->setObjectName("bt-info");
    ui->viewerButton->setObjectName("bt-viewer");
    ui->resultButton->setObjectName("bt-result");
    ui->maskButton->setObjectName("bt-mask");
    ui->linkButton->setObjectName("bt-link");
    ui->unlinkButton->setObjectName("bt-unlink");

    ui->errorLineEdit->setObjectName("input-error");
    ui->errorMessageLabel->setObjectName("label-error");
    ui->highErrorLineEdit->setObjectName("input-high-error");

    ui->highComboBox->setObjectName("dropdown-high");

    ui->comboBox->setView(new QListView);
    ui->highComboBox->setView(new QListView);
}

MainWindow::~MainWindow()
{
    delete ui;
}
