#include "mainwindow.h"

#include <QApplication>

#include <QResource>
#include <QFontDatabase>

#include <QShortcut>
#include <QKeySequence>
#include <QFile>
#include <QTextStream>
#include <QDebug>

void LoadStylesheet() {
    if (!qApp) {
        return;
    }

    //QFile f(":/style/tcdark.qss");
    QFile f("E:/devel/TSS/Resources/style/tcdarkstyle/tcdark.qss");
    if (f.exists()) {
        f.open(QFile::ReadOnly | QFile::Text);

        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());

        qDebug() << "Load stylesheet";
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFontDatabase::addApplicationFont(":/font/NotoSansKR-Black.otf");
    QFontDatabase::addApplicationFont(":/font/NotoSansKR-Bold.otf");
    QFontDatabase::addApplicationFont(":/font/NotoSansKR-Light.otf");
    QFontDatabase::addApplicationFont(":/font/NotoSansKR-Medium.otf");
    QFontDatabase::addApplicationFont(":/font/NotoSansKR-Regular.otf");
    QFontDatabase::addApplicationFont(":/font/NotoSansKR-Thin.otf");

    QFont font;
    font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);

    QApplication::setFont(font);

    MainWindow w;

    auto qssReloadShortcut = new QShortcut(QKeySequence(Qt::Key_F5), &w);
    QObject::connect(qssReloadShortcut, &QShortcut::activated, &LoadStylesheet);

    LoadStylesheet();

    w.show();
    return a.exec();
}
