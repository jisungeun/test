#include <catch2/catch.hpp>

#include <chrono>
#include <iostream>

#include <QString>

#include "aqi3dDecon.h"
#include "aqiDataInfoValidationFlags.h"
#include "aqiDeconOpsValidationFlags.h"

#include "Unlocker.h"

namespace aqi3dDeconTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/AutoQuantTest";
    const QString rawFL3DFilePath = testFolderPath + "/rawBinary.data";
    const QString deconTestFolderPath = "C:/Temp/decontest";

    constexpr auto sizeX = 1080;
    constexpr auto sizeY = 1080;
    constexpr auto sizeZ = 57;

    TEST_CASE("aqi3dDecon : unit test") {
        Unlock();

        SECTION("aqi3dBlindDeconvolution()") {
            SECTION("invalid inputs") {
                aqiStructImgInfo imageInfo;
                aqiStructPsfInfo psfInInfo;
                aqiStructPsfInfo psfOutInfo;
                aqiStructDeconOpsStd deconOptionsStandard;
                aqiStructDeconOpsExp deconOptionsExpert;

                aqiInitStructImgInfo(&imageInfo);
                aqiInitStructPsfInfo(&psfInInfo);
                aqiInitStructPsfInfo(&psfOutInfo);
                aqiInitStructDeconOpsStd(&deconOptionsStandard);
                aqiInitStructDeconOpsExp(&deconOptionsExpert);

                const auto resultStatus = aqi3dBlindDeconvolution(&imageInfo, &psfInInfo, &psfOutInfo, &deconOptionsStandard, &deconOptionsExpert);

                CHECK(resultStatus == AQI_STATUS_INVALIDINPUT);
            }
        }

        SECTION("aqi3dDeconValidateInputs()") {
            aqiStructImgInfo imageInfo;
            aqiStructPsfInfo psfInInfo;
            aqiStructPsfInfo psfOutInfo;
            aqiStructDeconOpsStd deconOptionsStandard;
            aqiStructDeconOpsExp deconOptionsExpert;

            aqiInitStructImgInfo(&imageInfo);
            aqiInitStructPsfInfo(&psfInInfo);
            aqiInitStructPsfInfo(&psfOutInfo);
            aqiInitStructDeconOpsStd(&deconOptionsStandard);
            aqiInitStructDeconOpsExp(&deconOptionsExpert);

            aqiStructDecon3dValidationFlags decon3dValidationFlags{};

            const auto validationResult = 
                aqi3dDeconValidateInputs(&imageInfo, &psfInInfo, &psfOutInfo, &deconOptionsStandard, &deconOptionsExpert, &decon3dValidationFlags);
            CHECK(validationResult == AQI_STATUS_INVALIDINPUT);


            constexpr long imagInfoValidationErrorFlag =
                AQ_IMGINFO_ERR_FILENAME +
                AQ_IMGINFO_ERR_REALNAME +
                AQ_IMGINFO_ERR_MODALITY +
                AQ_IMGINFO_ERR_WIDTH +
                AQ_IMGINFO_ERR_HEIGHT +
                AQ_IMGINFO_ERR_NUMSLICES +
                AQ_IMGINFO_ERR_PIXELSIZE_X +
                AQ_IMGINFO_ERR_PIXELSIZE_Y +
                AQ_IMGINFO_ERR_PIXELSIZE_Z +
                AQ_IMGINFO_ERR_LENS_RI +
                AQ_IMGINFO_ERR_LENS_NA +
                AQ_IMGINFO_ERR_EMW;
                
            constexpr auto opsStdValidationErrorFlag = 
                AQ_STDOPS_ERR_FILENAME + 
                AQ_STDOPS_ERR_TEMPDIR;

            CHECK(decon3dValidationFlags.lImgInfoValidationFlags == imagInfoValidationErrorFlag);
            CHECK(decon3dValidationFlags.lPsfInfoInputValidationFlags == AQ_PSFINFO_NOERROR);
            CHECK(decon3dValidationFlags.lPsfInfoOutputValidationFlags == AQ_PSFINFO_NOERROR);
            CHECK(decon3dValidationFlags.lOpsStdValidationFlags == opsStdValidationErrorFlag);
            CHECK(decon3dValidationFlags.lOpsExpValidationFlags == AQ_EXPOPS_NOERROR);
            CHECK(decon3dValidationFlags.lDeconMixedValidationFlags == 0);
        }
    }

    TEST_CASE("aqi3dDecon : practical test") {
        SECTION("valid inputs case 1 : basic parameter settings") {
            constexpr auto maximumCharLength = 512;
            constexpr float pixelSizeOfImagingSensor = 4.5f; //micrometer
            constexpr float magnification = 50.f;
            constexpr float zScanStep = 0.156f; //micrometer
            constexpr float mediumRI = 1.337f;
            constexpr float numericalAperture = 1.2f;
            constexpr float emmissionWaveLength = 0.519f; // micrometer
            constexpr float sampleDepth = 3.0f;

            const std::string outputFilePathString = "flDecon.out";
            const std::string logFileEntryPathString = "logFileEntry.out";

            std::shared_ptr<char[]> imageFilePath{ new char[maximumCharLength]() };
            std::copy_n(rawFL3DFilePath.toStdString().c_str(), rawFL3DFilePath.length(), imageFilePath.get());

            std::shared_ptr<char[]> logFileEntryPath{ new char[maximumCharLength]() };
            std::copy_n(logFileEntryPathString.c_str(), logFileEntryPathString.length(), logFileEntryPath.get());

            std::shared_ptr<char[]> tempFolderPath{ new char[maximumCharLength]() };
            std::copy_n(deconTestFolderPath.toStdString().c_str(), deconTestFolderPath.length(), tempFolderPath.get());

            std::shared_ptr<char[]> outputFilePath{ new char[maximumCharLength]() };
            std::copy_n(outputFilePathString.c_str(), outputFilePathString.length(), outputFilePath.get());

            std::shared_ptr<char[]> psfInFilePath{ new char[maximumCharLength]() };
            std::shared_ptr<char[]> psfOutFilePath{ new char[maximumCharLength]() };

            aqiStructImgInfo imageInfo;
            aqiStructPsfInfo psfInInfo;
            aqiStructPsfInfo psfOutInfo;
            aqiStructDeconOpsStd deconOptionsStandard;
            aqiStructDeconOpsExp deconOptionsExpert;

            aqiInitStructImgInfo(&imageInfo);
            aqiInitStructPsfInfo(&psfInInfo);
            aqiInitStructPsfInfo(&psfOutInfo);
            aqiInitStructDeconOpsStd(&deconOptionsStandard);
            aqiInitStructDeconOpsExp(&deconOptionsExpert);

            imageInfo.lpstrImgFile = imageFilePath.get();
            imageInfo.lpstrRealName = logFileEntryPath.get();
            //imageInfo.lpbBadSlice = ;
            //imageInfo.lpfImgData = ;
            imageInfo.enDataType = DT_UINT_8BIT;
            imageInfo.enScopeModality = MOD_WF_FLUORESCENCE;
            imageInfo.shWidth = sizeX;
            imageInfo.shHeight = sizeY;
            imageInfo.shNumSlices = sizeZ;
            //imageInfo.shTotalChannels = ;
            imageInfo.fPixelSizeX = pixelSizeOfImagingSensor / magnification;
            imageInfo.fPixelSizeY = pixelSizeOfImagingSensor / magnification;
            imageInfo.fPixelSizeZ = zScanStep * mediumRI;
            imageInfo.fNumericAperture = numericalAperture;
            imageInfo.fRefractiveIndex = mediumRI;
            imageInfo.fEmmWavelength = emmissionWaveLength;
            //imageInfo.fSphereAberrationFactor = ;
            //imageInfo.fConfocalPinholeSize = ;
            //imageInfo.lpstrImgFirstGuess = ;
            //imageInfo.fEmbeddingRefractiveIndex = ;
            imageInfo.fSampleDepth = sampleDepth;
            //imageInfo.fSphereAberrationFactorO2 = ;


            //psfInInfo.lpstrPsfFile = psfInFilePath.get();
            //psfInInfo.lpfPsfEstimate = ;
            //psfInInfo.shWidth = ;
            //psfInInfo.shHeight = ;
            //psfInInfo.shNumSlices = ;
            //psfInInfo.fPixelSizeX = ;
            //psfInInfo.fPixelSizeY = ;
            //psfInInfo.fPixelSizeZ = ;
            //psfInInfo.enPsfSource = PSFSRC_CALCULATED;

            //psfOutInfo.lpstrPsfFile = psfOutFilePath.get();
            //psfOutInfo.lpfPsfEstimate = ;
            //psfOutInfo.shWidth = ;
            //psfOutInfo.shHeight = ;
            //psfOutInfo.shNumSlices = ;
            //psfOutInfo.fPixelSizeX = ;
            //psfOutInfo.fPixelSizeY = ;
            //psfOutInfo.fPixelSizeZ = ;
            //psfOutInfo.enPsfSource = PSFSRC_CALCULATED;

            deconOptionsStandard.lpstrImgOutFile = outputFilePath.get();
            deconOptionsStandard.lpstrTempDir = tempFolderPath.get();
            deconOptionsStandard.enDeconMeth = DM_EXPECTATION_MAX;
            deconOptionsStandard.enDarkCurMeth = DCM_AUTO_CALCULATION;
            deconOptionsStandard.fDarkCurrent = 0;
            deconOptionsStandard.shNumIterations = 10;
            deconOptionsStandard.shSaveInterval = 10;
            deconOptionsStandard.shBinFactorXY = 1;
            deconOptionsStandard.shBinFactorZ = 1;
            deconOptionsStandard.bEnableGpuProcessing = true;

            deconOptionsExpert.enImgGuessMeth = IGM_FILTERED_ORIGINAL;
            deconOptionsExpert.enPsfGuessMeth = PGM_THEORETICAL_EST;
            deconOptionsExpert.enFreqConsMeth = FCM_AUTO_SELECTION;
            deconOptionsExpert.enSubVolMeth = SVM_DYNAMIC;
            deconOptionsExpert.shGuardBand = 28;
            deconOptionsExpert.shGuardBandZ = 6;
            deconOptionsExpert.shSubVolOverlap = 28;
            deconOptionsExpert.bMontageXY = true;
            deconOptionsExpert.bMontageZ = false;
            //deconOptionsExpert.bDetectBlanks = ;
            //deconOptionsExpert.bSuppressNoise = ;
            //deconOptionsExpert.bDenseSample = ;
            deconOptionsExpert.bEnablePsfCons = true;
            //deconOptionsExpert.bEnableGuidedDecon = ;
            //deconOptionsExpert.bSuppressMessages = ;
            deconOptionsExpert.fSuppressNoiseFactor = 2.0f;
            deconOptionsExpert.fPsfStretchFactor = 1.0f;
            deconOptionsExpert.fPsfCentralRadius = 1.f;
            deconOptionsExpert.shGoldsSmoothIteration = 3;
            deconOptionsExpert.fGoldsSmoothGauss = 1.0f;
            //deconOptionsExpert.shSubpixelXYFactor = ;
            //deconOptionsExpert.shSubpixelZFactor = ;
            //deconOptionsExpert.shIntensityCorrection = 1;
            //deconOptionsExpert.fMaxMemoryUsage = ;
            deconOptionsExpert.bEnableClassicConfocalAlgorithm = false;
            deconOptionsExpert.enPsfGenMeth = TPM_GIBSON_LANNI;

            const auto start = std::chrono::system_clock::now();
            const auto resultStatus = aqi3dBlindDeconvolution(&imageInfo, &psfInInfo, &psfOutInfo, &deconOptionsStandard, &deconOptionsExpert);
            const auto end = std::chrono::system_clock::now();

            const auto timeDuration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            std::cout << static_cast<float>(timeDuration.count()) / 1000000 << " sec" << std::endl;

            CHECK(resultStatus == AQI_STATUS_NOERROR);
        }
    }
}
