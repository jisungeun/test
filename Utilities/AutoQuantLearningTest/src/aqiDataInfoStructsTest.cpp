#include <catch2/catch.hpp>

#include <aqiEnumerations.h>

#include "aqiDataInfoStructs.h"
#include "aqiStatusCodes.h"

#include "Unlocker.h"

namespace aqiDataInfoStructsTest {
    TEST_CASE("aqiDataInfoStructs : unit test") {
        Unlock();

        SECTION("aqiStructImgInfo") {
            aqiStructImgInfo aqiStructImgInfoVariable{};
            CHECK(aqiStructImgInfoVariable.lpstrImgFile == nullptr);
            CHECK(aqiStructImgInfoVariable.lpstrImgInFile == nullptr);
            CHECK(aqiStructImgInfoVariable.lpstrRealName == nullptr);
            CHECK(aqiStructImgInfoVariable.lpbBadSlice == nullptr);
            CHECK(aqiStructImgInfoVariable.lpfImgData == nullptr);
            CHECK(aqiStructImgInfoVariable.enDataType == enumDataTypes::DT_UINT_8BIT);
            CHECK(aqiStructImgInfoVariable.enScopeModality == enumModalities::MOD_INVALID_VALUE);
            CHECK(aqiStructImgInfoVariable.shWidth == 0);
            CHECK(aqiStructImgInfoVariable.shHeight == 0);
            CHECK(aqiStructImgInfoVariable.shNumSlices == 0);
            CHECK(aqiStructImgInfoVariable.shTotalChannels == 0);
            CHECK(aqiStructImgInfoVariable.fPixelSizeX == 0);
            CHECK(aqiStructImgInfoVariable.fPixelSizeY == 0);
            CHECK(aqiStructImgInfoVariable.fPixelSizeZ == 0);
            CHECK(aqiStructImgInfoVariable.fNumericAperture == 0);
            CHECK(aqiStructImgInfoVariable.fRefractiveIndex == 0);
            CHECK(aqiStructImgInfoVariable.fEmmWavelength == 0);
            CHECK(aqiStructImgInfoVariable.fSphereAberrationFactor == 0);
            CHECK(aqiStructImgInfoVariable.fConfocalPinholeSize == 0);
            CHECK(aqiStructImgInfoVariable.lpstrImgFirstGuess == nullptr);
            CHECK(aqiStructImgInfoVariable.fEmbeddingRefractiveIndex == 0);
            CHECK(aqiStructImgInfoVariable.fSampleDepth == 0);
            CHECK(aqiStructImgInfoVariable.fSphereAberrationFactorO2 == 0);
            CHECK(aqiStructImgInfoVariable.dwReserved2 == 0);
            CHECK(aqiStructImgInfoVariable.dwReserved3 == 0);
            CHECK(aqiStructImgInfoVariable.dwReserved4 == 0);
        }

        SECTION("aqiStructImgDispParams") {
            aqiStructImgDispParams aqiStructImgDispParamsVariable{};
            CHECK(aqiStructImgDispParamsVariable.lpfRotationMatrix == nullptr);
            CHECK(aqiStructImgDispParamsVariable.enProjType == PT_MAX_INTENSITY);
            CHECK(aqiStructImgDispParamsVariable.fMinThreshold == 0.f);
            CHECK(aqiStructImgDispParamsVariable.fMaxThreshold == 0.f);
            CHECK(aqiStructImgDispParamsVariable.fGamma == 0.f);
            CHECK(aqiStructImgDispParamsVariable.fBrightness == 0.f);
            CHECK(aqiStructImgDispParamsVariable.fDarkness == 0.f);
            CHECK(aqiStructImgDispParamsVariable.fAlpha == 0.f);
            CHECK(aqiStructImgDispParamsVariable.iUseTextureCaching == 0);
            CHECK(aqiStructImgDispParamsVariable.iCurrentTimePoint == 0);
            CHECK(aqiStructImgDispParamsVariable.iCurrentChannel == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved1 == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved2 == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved3 == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved4 == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved5 == 0);
        }

        SECTION("aqiStructPsfInfo") {
            aqiStructPsfInfo aqiStructPsfInfoVariable{};
            CHECK(aqiStructPsfInfoVariable.lpstrPsfFile == nullptr);
            CHECK(aqiStructPsfInfoVariable.lpstrPsfOutFile == nullptr);
            CHECK(aqiStructPsfInfoVariable.lpfPsfEstimate == nullptr);
            CHECK(aqiStructPsfInfoVariable.shWidth == 0);
            CHECK(aqiStructPsfInfoVariable.shHeight == 0);
            CHECK(aqiStructPsfInfoVariable.shNumSlices == 0);
            CHECK(aqiStructPsfInfoVariable.fPixelSizeX == 0.f);
            CHECK(aqiStructPsfInfoVariable.fPixelSizeY == 0.f);
            CHECK(aqiStructPsfInfoVariable.fPixelSizeZ == 0.f);
            CHECK(aqiStructPsfInfoVariable.enPsfSource == PSFSRC_CALCULATED);
            CHECK(aqiStructPsfInfoVariable.dwReserved1 == 0);
            CHECK(aqiStructPsfInfoVariable.dwReserved2 == 0);
            CHECK(aqiStructPsfInfoVariable.dwReserved3 == 0);
            CHECK(aqiStructPsfInfoVariable.dwReserved4 == 0);
            CHECK(aqiStructPsfInfoVariable.dwReserved5 == 0);
            CHECK(aqiStructPsfInfoVariable.dwReserved6 == 0);
            CHECK(aqiStructPsfInfoVariable.dwReserved7 == 0);
            CHECK(aqiStructPsfInfoVariable.dwReserved8 == 0);
        }

        SECTION("aqiInitStructImgInfo()") {
            aqiStructImgInfo aqiStructImgInfoVariable;
            CHECK(aqiInitStructImgInfo(&aqiStructImgInfoVariable) == AQI_STATUS_NOERROR);

            CHECK(aqiStructImgInfoVariable.lpstrImgFile == nullptr);
            CHECK(aqiStructImgInfoVariable.lpstrImgInFile == nullptr);
            CHECK(aqiStructImgInfoVariable.lpstrRealName == nullptr);
            CHECK(aqiStructImgInfoVariable.lpbBadSlice == nullptr);
            CHECK(aqiStructImgInfoVariable.lpfImgData == nullptr);
            CHECK(aqiStructImgInfoVariable.enDataType == enumDataTypes::DT_FLOAT_32BIT);
            CHECK(aqiStructImgInfoVariable.enScopeModality == enumModalities::MOD_INVALID_VALUE);
            CHECK(aqiStructImgInfoVariable.shWidth == 0);
            CHECK(aqiStructImgInfoVariable.shHeight == 0);
            CHECK(aqiStructImgInfoVariable.shNumSlices == 0);
            CHECK(aqiStructImgInfoVariable.shTotalChannels == 1);
            CHECK(aqiStructImgInfoVariable.fPixelSizeX == 0.f);
            CHECK(aqiStructImgInfoVariable.fPixelSizeY == 0.f);
            CHECK(aqiStructImgInfoVariable.fPixelSizeZ == 0.f);
            CHECK(aqiStructImgInfoVariable.fNumericAperture == 0.f);
            CHECK(aqiStructImgInfoVariable.fRefractiveIndex == 0.f);
            CHECK(aqiStructImgInfoVariable.fEmmWavelength == 0.f);
            CHECK(aqiStructImgInfoVariable.fSphereAberrationFactor == 0.f);
            CHECK(aqiStructImgInfoVariable.fConfocalPinholeSize == 1.0f);
            CHECK(aqiStructImgInfoVariable.lpstrImgFirstGuess == nullptr);
            CHECK(aqiStructImgInfoVariable.fEmbeddingRefractiveIndex == 0.f);
            CHECK(aqiStructImgInfoVariable.fSampleDepth == 0.f);
            CHECK(aqiStructImgInfoVariable.fSphereAberrationFactorO2 == 0.f);
            CHECK(aqiStructImgInfoVariable.dwReserved2 == 0);
            CHECK(aqiStructImgInfoVariable.dwReserved3 == 0);
            CHECK(aqiStructImgInfoVariable.dwReserved4 == 0);
        }

        SECTION("aqiInitStructImgDispParams()") {
            aqiStructImgDispParams aqiStructImgDispParamsVariable;
            CHECK(aqiInitStructImgDispParams(&aqiStructImgDispParamsVariable) == AQI_STATUS_NOERROR);
            CHECK(aqiStructImgDispParamsVariable.lpfRotationMatrix == nullptr);
            CHECK(aqiStructImgDispParamsVariable.enProjType == PT_SURFACE_SLICE);
            CHECK(aqiStructImgDispParamsVariable.fMinThreshold == 0.0f);
            CHECK(aqiStructImgDispParamsVariable.fMaxThreshold == 100.0f);
            CHECK(aqiStructImgDispParamsVariable.fGamma == 1.0f);
            CHECK(aqiStructImgDispParamsVariable.fBrightness == 100.0f);
            CHECK(aqiStructImgDispParamsVariable.fDarkness == 0.f);
            CHECK(aqiStructImgDispParamsVariable.fAlpha == 0.5f);
            CHECK(aqiStructImgDispParamsVariable.iUseTextureCaching != 0);
            CHECK(aqiStructImgDispParamsVariable.iCurrentTimePoint != 0);
            CHECK(aqiStructImgDispParamsVariable.iCurrentChannel != 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved1 == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved2 == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved3 == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved4 == 0);
            CHECK(aqiStructImgDispParamsVariable.dwReserved5 == 0);
        }
    }
}