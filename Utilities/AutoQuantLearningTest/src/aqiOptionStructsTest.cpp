#include <catch2/catch.hpp>

#include "aqiEnumerations.h"
#include "aqiOptionStructs.h"
#include "Unlocker.h"
#include "aqiStatusCodes.h"

namespace aqiOptionStructsTest {
    TEST_CASE("aqiOptionStructs : unit test") {
        Unlock();
        SECTION("aqiStructDeconOpsStd") {
            aqiStructDeconOpsStd aqiStructDeconOpsStdVariable{};
            CHECK(aqiStructDeconOpsStdVariable.lpstrImgOutFile == nullptr);
            CHECK(aqiStructDeconOpsStdVariable.lpstrTempDir == nullptr);
            CHECK(aqiStructDeconOpsStdVariable.enDeconMeth == DM_EXPECTATION_MAX);
            CHECK(aqiStructDeconOpsStdVariable.enDarkCurMeth == DCM_AUTO_CALCULATION);
            CHECK(aqiStructDeconOpsStdVariable.fDarkCurrent == 0.f);
            CHECK(aqiStructDeconOpsStdVariable.shNumIterations == 0);
            CHECK(aqiStructDeconOpsStdVariable.shSaveInterval == 0);
            CHECK(aqiStructDeconOpsStdVariable.shBinFactorXY == 0);
            CHECK(aqiStructDeconOpsStdVariable.shBinFactorZ == 0);
            CHECK(aqiStructDeconOpsStdVariable.bEnableGpuProcessing == false);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved1 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved2 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved3 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved4 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved5 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved6 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved7 == 0);
        }

        SECTION("aqiStructDeconOpsExp") {
            aqiStructDeconOpsExp aqiStructDeconOpsExpVariable{};
            CHECK(aqiStructDeconOpsExpVariable.enImgGuessMeth == IGM_ORIGINAL_DATA);
            CHECK(aqiStructDeconOpsExpVariable.enPsfGuessMeth == PGM_THEORETICAL_EST);
            CHECK(aqiStructDeconOpsExpVariable.enFreqConsMeth == FCM_AUTO_SELECTION);
            CHECK(aqiStructDeconOpsExpVariable.enSubVolMeth == SVM_STATIC);
            CHECK(aqiStructDeconOpsExpVariable.shGuardBand == 0);
            CHECK(aqiStructDeconOpsExpVariable.shGuardBandZ == 0);
            CHECK(aqiStructDeconOpsExpVariable.shSubVolOverlap == 0);
            CHECK(aqiStructDeconOpsExpVariable.bMontageXY == false);
            CHECK(aqiStructDeconOpsExpVariable.bMontageZ == false);
            CHECK(aqiStructDeconOpsExpVariable.bDetectBlanks == false);
            CHECK(aqiStructDeconOpsExpVariable.bSuppressNoise == false);
            CHECK(aqiStructDeconOpsExpVariable.bDenseSample == false);
            CHECK(aqiStructDeconOpsExpVariable.bEnablePsfCons == false);
            CHECK(aqiStructDeconOpsExpVariable.bEnableGuidedDecon == false);
            CHECK(aqiStructDeconOpsExpVariable.bSuppressMessages == false);
            CHECK(aqiStructDeconOpsExpVariable.fSuppressNoiseFactor == 0.f);
            CHECK(aqiStructDeconOpsExpVariable.fPsfStretchFactor == 0.f);
            CHECK(aqiStructDeconOpsExpVariable.fPsfCentralRadius == 0.f);
            CHECK(aqiStructDeconOpsExpVariable.shGoldsSmoothIteration == 0);
            CHECK(aqiStructDeconOpsExpVariable.fGoldsSmoothGauss == 0.f);
            CHECK(aqiStructDeconOpsExpVariable.shSubpixelXYFactor == 0);
            CHECK(aqiStructDeconOpsExpVariable.shSubpixelZFactor == 0);
            CHECK(aqiStructDeconOpsExpVariable.shIntensityCorrection == 0);
            CHECK(aqiStructDeconOpsExpVariable.fMaxMemoryUsage == 0.f);
            CHECK(aqiStructDeconOpsExpVariable.bEnableClassicConfocalAlgorithm == false);
            CHECK(aqiStructDeconOpsExpVariable.bReserved1 == false);
            CHECK(aqiStructDeconOpsExpVariable.shReserved2 == 0);
            CHECK(aqiStructDeconOpsExpVariable.enPsfGenMeth == TPM_AUTOQUANT);
            CHECK(aqiStructDeconOpsExpVariable.dwReserved3 == 0);
            CHECK(aqiStructDeconOpsExpVariable.dwReserved4 == 0);
        }

        SECTION("aqiStructDecon3dValidationFlags") {
            aqiStructDecon3dValidationFlags aqiStructDecon3dValidationFlags{};
            CHECK(aqiStructDecon3dValidationFlags.lImgInfoValidationFlags == 0);
            CHECK(aqiStructDecon3dValidationFlags.lPsfInfoInputValidationFlags == 0);
            CHECK(aqiStructDecon3dValidationFlags.lPsfInfoOutputValidationFlags == 0);
            CHECK(aqiStructDecon3dValidationFlags.lOpsStdValidationFlags == 0);
            CHECK(aqiStructDecon3dValidationFlags.lOpsExpValidationFlags == 0);
            CHECK(aqiStructDecon3dValidationFlags.lDeconMixedValidationFlags == 0);
        }

        SECTION("aqiInitStructDeconOpsStd()") {
            aqiStructDeconOpsStd aqiStructDeconOpsStdVariable;
            CHECK(aqiInitStructDeconOpsStd(&aqiStructDeconOpsStdVariable) == AQI_STATUS_NOERROR);
            CHECK(aqiStructDeconOpsStdVariable.lpstrImgOutFile == nullptr);
            CHECK(aqiStructDeconOpsStdVariable.lpstrTempDir == nullptr);
            CHECK(aqiStructDeconOpsStdVariable.enDeconMeth == DM_EXPECTATION_MAX);
            CHECK(aqiStructDeconOpsStdVariable.enDarkCurMeth == DCM_AUTO_CALCULATION);
            CHECK(aqiStructDeconOpsStdVariable.fDarkCurrent == 0.f);
            CHECK(aqiStructDeconOpsStdVariable.shNumIterations == 10);
            CHECK(aqiStructDeconOpsStdVariable.shSaveInterval == 10);
            CHECK(aqiStructDeconOpsStdVariable.shBinFactorXY == 1);
            CHECK(aqiStructDeconOpsStdVariable.shBinFactorZ == 1);
            CHECK(aqiStructDeconOpsStdVariable.bEnableGpuProcessing == false);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved1 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved2 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved3 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved4 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved5 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved6 == 0);
            CHECK(aqiStructDeconOpsStdVariable.dwReserved7 == 0);
        }

        SECTION("aqiInitStructDeconOpsExp()") {
            aqiStructDeconOpsExp aqiStructDeconOpsExpVariable;
            CHECK(aqiInitStructDeconOpsExp(&aqiStructDeconOpsExpVariable) == AQI_STATUS_NOERROR);
            CHECK(aqiStructDeconOpsExpVariable.enImgGuessMeth == IGM_FILTERED_ORIGINAL);
            CHECK(aqiStructDeconOpsExpVariable.enPsfGuessMeth == PGM_THEORETICAL_EST);
            CHECK(aqiStructDeconOpsExpVariable.enFreqConsMeth == FCM_AUTO_SELECTION);
            CHECK(aqiStructDeconOpsExpVariable.enSubVolMeth == SVM_DYNAMIC);
            CHECK(aqiStructDeconOpsExpVariable.shGuardBand == 10);
            CHECK(aqiStructDeconOpsExpVariable.shGuardBandZ == 6);
            CHECK(aqiStructDeconOpsExpVariable.shSubVolOverlap == 10);
            CHECK(aqiStructDeconOpsExpVariable.bMontageXY == true);
            CHECK(aqiStructDeconOpsExpVariable.bMontageZ == false);
            CHECK(aqiStructDeconOpsExpVariable.bDetectBlanks == false);
            CHECK(aqiStructDeconOpsExpVariable.bSuppressNoise == true);
            CHECK(aqiStructDeconOpsExpVariable.bDenseSample == false);
            CHECK(aqiStructDeconOpsExpVariable.bEnablePsfCons == true);
            CHECK(aqiStructDeconOpsExpVariable.bEnableGuidedDecon == false);
            CHECK(aqiStructDeconOpsExpVariable.bSuppressMessages == true);
            CHECK(aqiStructDeconOpsExpVariable.fSuppressNoiseFactor == 2.0f);
            CHECK(aqiStructDeconOpsExpVariable.fPsfStretchFactor == 1.0f);
            CHECK(aqiStructDeconOpsExpVariable.fPsfCentralRadius == 1.0f);
            CHECK(aqiStructDeconOpsExpVariable.shGoldsSmoothIteration == 3);
            CHECK(aqiStructDeconOpsExpVariable.fGoldsSmoothGauss == 1.0f);
            CHECK(aqiStructDeconOpsExpVariable.shSubpixelXYFactor == 1);
            CHECK(aqiStructDeconOpsExpVariable.shSubpixelZFactor == 1);
            CHECK(aqiStructDeconOpsExpVariable.shIntensityCorrection == 1);
            CHECK(aqiStructDeconOpsExpVariable.fMaxMemoryUsage == 100.0f);
            CHECK(aqiStructDeconOpsExpVariable.bEnableClassicConfocalAlgorithm == false);
            CHECK(aqiStructDeconOpsExpVariable.bReserved1 == false);
            CHECK(aqiStructDeconOpsExpVariable.shReserved2 == 0);
            CHECK(aqiStructDeconOpsExpVariable.enPsfGenMeth == TPM_ZERNIKE);
            CHECK(aqiStructDeconOpsExpVariable.dwReserved3 == 0);
            CHECK(aqiStructDeconOpsExpVariable.dwReserved4 == 0);
        }
    }
}