#include "Unlocker.h"

#include <cstdint>
#include <QSettings>
#include <QString>

#include "aqi3dDecon.h"
#include "aqiStatusCodes.h"
#include "aqiCommonInit.h"

constexpr auto REG_INIT_VAL = "3";
constexpr auto REG_DECON_VAL = "0";

#ifdef HL
#undef HL
#endif
#define HL(a)   ((a) + 0x50)

#ifdef UNHIDE_STRING
#undef UNHIDE_STRING
#endif
#define UNHIDE_STRING(strI, strC) do {		\
	int* ptrI = strI;						\
	char* ptrC = strC;						\
	while (*ptrI) {							\
		*ptrC++ = (char)((*ptrI++)-0x50);	\
	}									\
 } while(0)

#ifdef HIDE_STRING
#undef HIDE_STRING
#endif
#define HIDE_STRING(str)  do {char * ptr = str ; while (*ptr) *ptr++ = '0';} while(0)

const std::string registryKeyPath = "HKEY_CURRENT_USER\\Software\\Tomocube_dev\\AutoQuantTest\\Strings";

auto Unlock() -> void {
    QSettings settings(registryKeyPath.c_str(), QSettings::NativeFormat);
    settings.setValue("0", QString("BC86 3219 4926 1418 105A 66B2 A052 A2C0"));
    settings.setValue("1", QString("8318 6214 EB09 3A1B 0623 A881 0C23 11B8"));
    settings.setValue("2", QString("C280 662D E180 323B BC80 0209 0183 90C8"));
    settings.setValue("3", QString("1D80 1698 4102 1619 085A 61B2 82D2 B2C0"));
    settings.setValue("4", QString("1640 A219 9236 18A8 3291 B2DD D123 C2A1"));
    settings.setValue("5", QString("A823 1B20 52B1 D920 82A1 70B1 C0C1 9918"));
    settings.setValue("6", QString("2301 AB10 9012 E83E 2322 44A3 C125 125E"));
    settings.setValue("7", QString("BA12 90D1 105A 66B2 1131 B123 12A2 9012"));
    settings.setValue("8", QString("C120 5312 3320 A029 01AB 43B2 C087 5420"));
    settings.setValue("9", QString("19B0 B6D2 4E20 1B13 06C2 81DD B238 A289"));

    //2812 F100 98CE 42B1 0146 5000 5482 9622
    int32_t initialProgramIntegerKey[] = {
        HL('2'), HL('8'), HL('1'), HL('2'),
        HL('F'), HL('1'), HL('0'), HL('0'),
        HL('9'), HL('8'), HL('C'), HL('E'),
        HL('4'), HL('2'), HL('B'), HL('1'),
        HL('0'), HL('1'), HL('4'), HL('6'),
        HL('5'), HL('0'), HL('0'), HL('0'),
        HL('5'), HL('4'), HL('8'), HL('2'),
        HL('9'), HL('6'), HL('2'), HL('2'),
        0 };

    char initialProgramCharKey[33] = { 0, };

    UNHIDE_STRING(initialProgramIntegerKey, initialProgramCharKey);
    if (AQI_STATUS_DONE != aqiUnlockCommonInitDll(initialProgramCharKey, registryKeyPath.c_str(), REG_INIT_VAL)) {
        HIDE_STRING(initialProgramCharKey);
    }

    HIDE_STRING(initialProgramCharKey);

    //701B D040 9C1E 02A1 0756 2004 1083 1622
    int32_t deconvolutionProgramIntegerKey[] = {
        HL('7'), HL('0'), HL('1'), HL('B'),
        HL('D'), HL('0'), HL('4'), HL('0'),
        HL('9'), HL('C'), HL('1'), HL('E'),
        HL('0'), HL('2'), HL('A'), HL('1'),
        HL('0'), HL('7'), HL('5'), HL('6'),
        HL('2'), HL('0'), HL('0'), HL('4'),
        HL('1'), HL('0'), HL('8'), HL('3'),
        HL('1'), HL('6'), HL('2'), HL('2'),
        0 };

    char deconvolutionProgramCharKey[33] = { 0, };

    UNHIDE_STRING(deconvolutionProgramIntegerKey, deconvolutionProgramCharKey);
    if (AQI_STATUS_DONE != aqiUnlock3dBlindDeconDll(deconvolutionProgramCharKey, registryKeyPath.c_str(), REG_DECON_VAL)) {
        HIDE_STRING(deconvolutionProgramCharKey);
    }

    HIDE_STRING(deconvolutionProgramCharKey);
}
