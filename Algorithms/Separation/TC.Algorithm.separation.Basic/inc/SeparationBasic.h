#pragma once

#include <QObject>
#include "ICustomAlgorithm.h"

//#include "MaskRaw.h"
#include "TC_Algorithm_separation_BasicExport.h"

namespace TC::Algorithm::Separation::Basic {

    class TC_Algorithm_separation_Basic_API Algorithm
        :public QObject
        , public ICustomAlgorithm
    {
        Q_OBJECT
            Q_PLUGIN_METADATA(IID "org.tomocube.algorithm.separation.basic")
            Q_INTERFACES(ICustomAlgorithm)

    public:
        Algorithm();
        virtual ~Algorithm();

        auto GetName() const->QString override { return "Separation Labeled Oragn"; }
        auto GetFullName() const->QString override { return "org.tomocube.algorithm.separation.basic"; }
        auto GetDescription() const -> QString override { return "Baisc Separation algorithm"; }
        auto GetOutputFormat() const -> QString override { return "Mask!MultiLabelLayer"; }
        auto clone() const->IPluginModule* override { return new Algorithm(); }

        auto FreeMemory() -> void override;

        auto Execute()->bool override;
        auto SetInput(IBaseData::Pointer data) -> bool override;
        auto SetInput2(IBaseData::Pointer data) -> bool override;
        auto SetInput3(IBaseData::Pointer data) -> bool override;
        auto GetOutput()->IBaseData::Pointer override;
        auto Parameter(const QString& key) -> IParameter::Pointer override;
        auto DuplicateParameter(const QStringList& keys) -> void override;        

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}