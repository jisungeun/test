#include <ParameterRegistry.h>
#include "SeparationBasicParameter.h"

namespace TC::Algorithm::Separation::Basic {

	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() :IParameter()
	{
		//nothing for now
	}
}