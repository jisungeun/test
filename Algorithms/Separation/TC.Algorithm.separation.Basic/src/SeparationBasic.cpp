#include <iostream>

#include "SeparationBasic.h"
#include "SeparationBasicParameter.h"

//inventor engine for automatic otsu threshold
#include <ImageViz/SoImageViz.h>
#include <ImageViz/Engines/SoImageVizEngineHeaders.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <Medical/helpers/MedicalHelper.h>

#include "ParameterRegistry.h"

#include <TCDataConverter.h>
#include <TCMask.h>

#include <ImageViz/Engines/ImageSegmentation/SeparatingAndFilling/SoSeparateObjectsProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>

//Open Inventor includes

namespace TC::Algorithm::Separation::Basic {
	struct Algorithm::Impl {
		//IBaseImage::Pointer image;
		//MaskRaw::Pointer label_image;
		TCMask::Pointer label_image;
		//MaskRaw::Pointer organ_layers;
		TCMask::Pointer organ_layers;
		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.separation.basic") };
		//MaskRaw::Pointer result;
		TCMask::Pointer result;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
	}

	Algorithm::~Algorithm() {
	}

	auto Algorithm::SetInput(IBaseData::Pointer data) -> bool {
		//d->organ_layers = std::dynamic_pointer_cast<MaskRaw>(data);
		d->organ_layers = std::dynamic_pointer_cast<TCMask>(data);
		return true;
	}

	auto Algorithm::SetInput3(IBaseData::Pointer data) -> bool {
		return true;
    }


	auto Algorithm::SetInput2(IBaseData::Pointer data) -> bool {
		//d->label_image = std::dynamic_pointer_cast<MaskRaw>(data);
		d->label_image = std::dynamic_pointer_cast<TCMask>(data);
		return true;
	}		

	auto Algorithm::FreeMemory() -> void {
		d->organ_layers = nullptr;
		d->label_image = nullptr;
		d->result = nullptr;
    }

	auto Algorithm::GetOutput() -> IBaseData::Pointer {
		return d->result;
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		return d->param;
    }

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {

	}

	auto Algorithm::Execute()->bool {
		//separation using open Inventor
		//convert volume data into data accessor
		auto conv = new TCDataConverter;
		//auto labeledImage = d->label_image->getVolume(0);
		auto labeledImage = conv->MaskToSoVolumeData(d->label_image);

		SoRef<SoMemoryDataAdapter> labelAdapter = MedicalHelper::getImageDataAdapter(labeledImage);
		labelAdapter->interpretation = SoMemoryDataAdapter::Interpretation::VALUE;

		auto numLayer = d->organ_layers->GetLayerNames().size();
		auto names = d->organ_layers->GetLayerNames();
		QList<SoVolumeData*> results;

		for (int i = 0; i < numLayer; i++) {
			SoVolumeData* layer_volume = conv->MaskToSoVolumeData(d->organ_layers, names[i]);

			SoRef<SoMemoryDataAdapter> maskAdapter = MedicalHelper::getImageDataAdapter(layer_volume);
			maskAdapter->interpretation = SoMemoryDataAdapter::Interpretation::BINARY;

			SoRef<SoMaskImageProcessing> maskFilter = new SoMaskImageProcessing;
			maskFilter->inBinaryImage = maskAdapter.ptr();
			maskFilter->inImage = labelAdapter.ptr();

			SoRef<SoVRImageDataReader> inputImageReader = new SoVRImageDataReader;
			inputImageReader->imageData.connectFrom(&maskFilter->outImage);

			SoVolumeData* labeled_layer = new SoVolumeData;
			labeled_layer->setReader(*inputImageReader);

			results.push_back(labeled_layer);
		}

		//d->result = std::dynamic_pointer_cast<MaskRaw>(MaskRaw::New(results));
		d->result = conv->SoVolumeDataToMask(results, MaskTypeEnum::MultiLayerLabel, names);
		

		/*
		for (int i = 0; i < numLayer; i++) {
			auto layer_name = d->organ_layers->getLayerNames()[i];								
		}*/

		//d->result->setType(MASK_VALUE::ORGAN_LABEL);
		return true;
	}
}