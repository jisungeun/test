#pragma once

#include <IParameter.h>

namespace TC::Algorithm::MorphOp::FillHoles25D {
	class Parameter : public IParameter {
		Q_OBJECT

	public:
		static auto Register()->void;

		static IParameter::Pointer CreateMethod() {
			return std::make_shared<Parameter>();
		}

		static std::string GetName() { return "org.tomocube.algorithm.morphop.fillholes.2.5d"; }

		Parameter();
	};
}