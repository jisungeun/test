#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <TCFMetaReader.h>
#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataList.h>
#include <ScalarData.h>
#include <TCDataConverter.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;

auto GenerateInputData(std::string_view path) -> TCMask::Pointer {
	QStringList plugins{
		QString("%1/segmentation/TC.Algorithm.Segmentation.Threshold.dll").arg(_PLUGIN_DIR),
		QString("%1/segmentation/TC.Algorithm.Segmentation.ManualThreshold.dll").arg(_PLUGIN_DIR),
	};

	for (auto& plugin : plugins)
		REQUIRE(PluginRegistry::LoadPlugin(plugin) > 0);

	TCMask::Pointer binary{nullptr};

	try {
		auto GetBinaryMask = [plugins](TCImage::Pointer image) -> TCMask::Pointer {
			const auto autoThresholdModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[0], true));

			const auto autoThresholdParam = autoThresholdModule->Parameter();

			autoThresholdModule->SetInput(0, image);
			autoThresholdModule->Execute();

			const auto autoThresholdResult = std::dynamic_pointer_cast<TC::Framework::DataList>(autoThresholdModule->GetOutput(0));
			const auto ulThreshold = std::dynamic_pointer_cast<DataSet>(autoThresholdResult->GetData(0));

			const auto lower = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("LThreshold"))->ValueAsDouble();
			const auto upper = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("UThreshold"))->ValueAsDouble();

			const auto thresholdingModuleInstance = PluginRegistry::GetPlugin(plugins[1], true);
			const auto thresholdingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(thresholdingModuleInstance);

			const auto thresholdingParam = thresholdingModuleInstance->Parameter();
			thresholdingParam->SetValue("LThreshold", IParameter::ValueType(lower));
			thresholdingParam->SetValue("UThreshold", IParameter::ValueType(upper));

			thresholdingModule->SetInput(0, image);
			thresholdingModule->Execute();

			return std::dynamic_pointer_cast<TCMask>(thresholdingModule->GetOutput(0));
		};

		const auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
		const auto metaInfo = metaReader->Read(path.data());

		H5::H5File file(path.data(), H5F_ACC_RDONLY);

		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		int size[3] = {metaInfo->data.data3D.sizeX, metaInfo->data.data3D.sizeY, metaInfo->data.data3D.sizeZ};
		double res[3] = {metaInfo->data.data3D.resolutionX, metaInfo->data.data3D.resolutionY, metaInfo->data.data3D.resolutionZ};

		std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]](), std::default_delete<uint16_t[]>());
		dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
		dataSet.close();
		group.close();
		file.close();

		// get a center slice image
		Vector3d spacing{ res[0], res[1], res[2] };
		Vector3d origin{ -size[0] * res[0] / 2, -size[1] * res[1] / 2, -size[2] * res[2] / 2 };

		const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty(origin, spacing);

		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

		const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]), static_cast<uint64_t>(size[2]) };
		const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		imagedev::setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
		imagedev::setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

		const RegionXu64 imageRegion{{0, 0, 0}, imageShape};
		imageView->writeRegion(imageRegion, data.get());

		TCDataConverter converter;
		const auto image3d = converter.ImageViewToImage(imageView);
		image3d->SetMinMax(metaInfo->data.data3D.riMin * 10000, metaInfo->data.data3D.riMax * 10000);

		binary = GetBinaryMask(image3d);

		file.close();
	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	} catch (...) {
		FAIL("Unknown error");
	}

	return binary;
}

auto Save(const TCImage::Pointer image, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.ImageToImageView(image);
	ioformat::writeView(imageview, savePath.toStdString());
}

auto Save(const TCMask::Pointer mask, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.MaskToImageView(mask);
	ioformat::writeView(imageview, savePath.toStdString());
}

TEST_CASE("FillHoles") {
	REQUIRE(QFile::exists(_TEST_DATA));

	try {
		imagedev::init();
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	// load plugins
	const QString plugin = QString("%1/morphop/TC.Algorithm.MorphOp.FillHoles.2.5D.dll").arg(_PLUGIN_DIR);
	REQUIRE(PluginRegistry::LoadPlugin(plugin) > 0);

	const auto binaryMask = GenerateInputData(_TEST_DATA);

	SECTION("2.5D") {
		const auto algorithmModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugin, true));
		algorithmModule->SetInput(0, binaryMask);
		REQUIRE(algorithmModule->Execute() == true);

		const auto output = std::dynamic_pointer_cast<TCMask>(algorithmModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(binaryMask, "input");
		Save(output, "output");
	}

	imagedev::finish();
}
