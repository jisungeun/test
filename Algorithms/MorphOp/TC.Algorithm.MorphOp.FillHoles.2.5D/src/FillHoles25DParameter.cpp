#include <ParameterRegistry.h>
#include "FillHoles25DParameter.h"

namespace TC::Algorithm::MorphOp::FillHoles25D {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("Neighborhood", "Select neighborhood", "Select neighborhood configuration", "SelectOption", 1, 0, 1);
		RegisterNode("NeighborhoodIndex", "Select neighborhood", "Select neighborhood configuration", "ScalarValue.int", 1, 0, 1);
    }

}
