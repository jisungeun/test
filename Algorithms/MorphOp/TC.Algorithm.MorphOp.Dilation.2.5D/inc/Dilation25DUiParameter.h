#pragma once

#include <IUiParameter.h>

namespace TC::Algorithm::MorphOp::Dilation25D {
    class UiParameter: public IUiParameter {
        Q_OBJECT
    public:
        static auto Register()->void;

        static auto CreateMethod()->IUiParameter::Pointer {
            return std::make_shared<UiParameter>();
        }

        static auto GetName()->std::string { return "org.tomocube.algorithm.morphop.dilation.2.5d"; }

        UiParameter();
    };
}