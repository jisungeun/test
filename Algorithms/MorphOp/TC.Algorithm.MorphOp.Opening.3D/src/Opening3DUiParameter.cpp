#include <ParameterRegistry.h>
#include "Opening3DUiParameter.h"

namespace TC::Algorithm::MorphOp::Opening3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}