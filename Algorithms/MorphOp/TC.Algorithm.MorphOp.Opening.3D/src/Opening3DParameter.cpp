#include <ParameterRegistry.h>
#include "Opening3DParameter.h"

namespace TC::Algorithm::MorphOp::Opening3D {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("BorderPolicy", "Select border policy", "Select the border policy to apply", "SelectOption", 1, 0, 1);
		RegisterNode("BorderPolicyIndex", "Select border policy", "Select the border policy to apply", "ScalarValue.int", 1, 0, 1);

		RegisterNode("KernelRadius", "Kernel radius", "A square structuring element", "ScalarValue.int", 3, 1, 100);

		RegisterNode("Neighborhood", "Select neighborhood", "Select neighborhood configuration", "SelectOption", 1, 0, 2);
		RegisterNode("NeighborhoodIndex", "Select neighborhood", "Select neighborhood configuration", "ScalarValue.int", 1, 0, 2);
    }

}
