#include <ParameterRegistry.h>
#include "ErosionDisk3DParameter.h"

namespace TC::Algorithm::MorphOp::ErosionDisk3D {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("Theta", "Theta angle", "The azimuthal angle in degrees", "ScalarValue.double", 0, -360, 360);
		RegisterNode("Phi", "Phi angle", "The polar angle in degrees", "ScalarValue.double", 0, -360, 360);
		RegisterNode("KernelRadius", "Kernel radius", "A square structuring element", "ScalarValue.int", 3, 1, 100);
    }

}
