#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataList.h>
#include <ScalarData.h>
#include <TCDataConverter.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;


auto Save(const TCImage::Pointer image, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.ImageToImageView(image);
	ioformat::writeView(imageview, savePath.toStdString());
}

auto Save(const TCMask::Pointer mask, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.MaskToImageView(mask);
	ioformat::writeView(imageview, savePath.toStdString());
}

TEST_CASE("ErosionDisk 3D") {
	REQUIRE(QFile::exists(_TEST_DATA));

	try {
		imagedev::init();
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	// load plugins
	QStringList plugins{
		"/segmentation/TC.Algorithm.Segmentation.ManualThreshold.dll",
		"/segmentation/TC.Algorithm.Segmentation.Threshold.dll",
		"/morphop/TC.Algorithm.MorphOp.ErosionDisk.3D.dll"
	};

	for (auto& plugin : plugins) {
		REQUIRE(PluginRegistry::LoadPlugin(QString("%1%2").arg(_PLUGIN_DIR).arg(plugin)) > 0);
	}

	TCMask::Pointer instanceMask{nullptr};

	// get a center slice from mask volume
	try {
		// get HT 3D
		H5::H5File file(_TEST_DATA, H5F_ACC_RDONLY);
		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		auto GetResolutionAttrValue = [group](std::string_view name) -> float {
			float res;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_FLOAT, &res);
			sizeAttr.close();

			return res;
		};

		double res[3];
		res[0] = GetResolutionAttrValue("ResolutionX");
		res[1] = GetResolutionAttrValue("ResolutionY");
		res[2] = GetResolutionAttrValue("ResolutionZ");

		auto GetSizeAttrValue = [group](std::string_view name) -> int64_t {
			int64_t size;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_INT64, &size);
			sizeAttr.close();

			return size;
		};

		int size[3];
		size[0] = GetSizeAttrValue("SizeX");
		size[1] = GetSizeAttrValue("SizeY");
		size[2] = GetSizeAttrValue("SizeZ");

		std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]], std::default_delete<uint16_t[]>());
		dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
		dataSet.close();
		group.close();
		file.close();

		TCDataConverter converter;
        const auto image = converter.ArrToImage(data.get(), size, res);

		// get upper and lower threshold
		const auto autoThresholdModuleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.segmentation.threshold");
		const auto autoThresholdModule = std::dynamic_pointer_cast<IPluginAlgorithm>(autoThresholdModuleInstance);

		const auto autoThresholdParam = autoThresholdModuleInstance->Parameter();
		autoThresholdParam->SetValue("Method", IParameter::ValueType(true));

		autoThresholdModule->SetInput(0, image);
		autoThresholdModule->Execute();

		const auto autoThresholdResult = std::dynamic_pointer_cast<TC::Framework::DataList>(autoThresholdModule->GetOutput(0));
		const auto ulThreshold = std::dynamic_pointer_cast<DataSet>(autoThresholdResult->GetData(0));

		const auto lower = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("LThreshold"))->ValueAsDouble();
		const auto upper = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("UThreshold"))->ValueAsDouble();

		// get binary mask volume
		const auto thresholdingModuleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.segmentation.manualthreshold");
		const auto thresholdingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(thresholdingModuleInstance);

		const auto thresholdingParam = thresholdingModuleInstance->Parameter();
		thresholdingParam->SetValue("LThreshold", IParameter::ValueType(lower));
		thresholdingParam->SetValue("UThreshold", IParameter::ValueType(upper));

		thresholdingModule->SetInput(0, image);
		thresholdingModule->Execute();

		instanceMask = std::dynamic_pointer_cast<TCMask>(thresholdingModule->GetOutput(0));

	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	REQUIRE(instanceMask != nullptr);

	SECTION("ErosionDisk 3D processing") {
		const auto moduleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.morphop.erosiondisk.3d");
		const auto algorithmModule = std::dynamic_pointer_cast<IPluginAlgorithm>(moduleInstance);

		const auto param = moduleInstance->Parameter();
		param->SetValue("Theta", 1);
		param->SetValue("Phi", 2);
		param->SetValue("KernelRadius", 3);

		algorithmModule->SetInput(0, instanceMask);
		REQUIRE(algorithmModule->Execute() == true);

		const auto output = std::dynamic_pointer_cast<TCMask>(algorithmModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(instanceMask, "input");
		Save(output, "output");
	}

	imagedev::finish();
}
