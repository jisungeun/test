#include <ParameterRegistry.h>
#include "Dilation2DParameter.h"

namespace TC::Algorithm::MorphOp::Dilation2D {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("KernelRadius", "Kernel radius", "A square structuring element", "ScalarValue.int", 3, 1, 100);

		RegisterNode("Neighborhood", "Select neighborhood", "Select neighborhood configuration", "SelectOption", 1, 0, 1);
		RegisterNode("NeighborhoodIndex", "Select neighborhood", "Select neighborhood configuration", "ScalarValue.int", 1, 0, 1);
    }

}
