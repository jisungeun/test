#define LOGGER_TAG "[Dilation2D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataConverter.h>

#include "Dilation2DUiParameter.h"
#include "Dilation2DParameter.h"
#include "Dilation2D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::MorphOp::Dilation2D {
	struct Algorithm::Impl {
		TCMask::Pointer input{ nullptr };
		TCMask::Pointer result{ nullptr };

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.morphop.dilation.2d") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.morphop.dilation.2d") };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

		d->input = std::dynamic_pointer_cast<TCMask>(data);
		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->result;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return {d->result};
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
			if (d->params.contains(key)) {
				return d->params[key];
			} else {
				return nullptr;
			}
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {

	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START Dilation2D";

		bool succeed = false;

		try {
			if (d->input == nullptr) throw std::runtime_error("Invalid input data");

			const auto kernelRadius = d->param->GetValue("KernelRadius").toInt(3);

			const auto neighborhood = static_cast<Dilation2d::Neighborhood>(d->param->GetValue("Neighborhood").toInt(1));

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			const auto refMask = converter.MaskToImageView(d->input);
			const auto dilatedMask = dilation2d(refMask, kernelRadius, neighborhood);
			auto shape = dilatedMask->shape();
			int dim[3] = { shape[0], shape[1], 1 };

			auto spacing = getCalibrationSpacing(dilatedMask);
			double res[3] = { spacing[0], spacing[1], spacing[2] };

			d->result = converter.ArrToLabelMask((unsigned short*)dilatedMask->buffer(), dim, res);

			QLOG_INFO() << QString("FINISH Dilation2D - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
	}
}
