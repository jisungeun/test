#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataList.h>
#include <ScalarData.h>
#include <TCDataConverter.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;


auto Extract2DMask(const TCMask::Pointer mask) -> TCMask::Pointer {
	// mask 볼륨에서 z-center slice만 TCMask로 추출
	const auto [resX, resY, resZ] = mask->GetResolution();
	const auto [dimX, dimY, dimZ] = mask->GetSize();
	const auto sliceIndex = dimZ / 2;

	const auto originBuffer = mask->GetMaskVolume().get();
	std::shared_ptr<unsigned short> buffer(new unsigned short[dimX * dimY](), std::default_delete<unsigned short>());
	std::copy_n(originBuffer + (dimX * dimY * sliceIndex), dimX * dimY, buffer.get());

	int resultDims[3] = {dimX, dimY, 1};
	double resultRes[3] = {resX, resY, resZ};

	TCDataConverter converter;
	return converter.ArrToLabelMask(buffer.get(), resultDims, resultRes);
}

auto Save(const TCImage::Pointer image, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.ImageToImageView(image);
	ioformat::writeView(imageview, savePath.toStdString());
}

auto Save(const TCMask::Pointer mask, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.MaskToImageView(mask);
	ioformat::writeView(imageview, savePath.toStdString());
}

TEST_CASE("Dilation") {
	REQUIRE(QFile::exists(_TEST_DATA));

	try {
		imagedev::init();
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	// load plugins
	QStringList plugins{
		"/segmentation/TC.Algorithm.Segmentation.ManualThreshold.dll",
		"/segmentation/TC.Algorithm.Segmentation.Threshold.dll",
		"/morphop/TC.Algorithm.MorphOp.Dilation.2D.dll"
	};

	for (auto& plugin : plugins) {
		REQUIRE(PluginRegistry::LoadPlugin(QString("%1%2").arg(_PLUGIN_DIR).arg(plugin)) > 0);
	}

	TCMask::Pointer sliceMask{nullptr};

	// get a center slice from mask volume
	try {
		// get HT 3D
		H5::H5File file(_TEST_DATA, H5F_ACC_RDONLY);
		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		auto GetResolutionAttrValue = [group](std::string_view name) -> float {
			float res;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_FLOAT, &res);
			sizeAttr.close();

			return res;
		};

		double res[3];
		res[0] = GetResolutionAttrValue("ResolutionX");
		res[1] = GetResolutionAttrValue("ResolutionY");
		res[2] = GetResolutionAttrValue("ResolutionZ");

		auto GetSizeAttrValue = [group](std::string_view name) -> int64_t {
			int64_t size;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_INT64, &size);
			sizeAttr.close();

			return size;
		};

		int size[3];
		size[0] = GetSizeAttrValue("SizeX");
		size[1] = GetSizeAttrValue("SizeY");
		size[2] = GetSizeAttrValue("SizeZ");

		std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]], std::default_delete<uint16_t[]>());
		dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
		dataSet.close();
		group.close();
		file.close();

		// get a center slice image
		const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty( {0, 0, 0}, {1, 1, 1} );

		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

		const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]), static_cast<uint64_t>(size[2]) };
		const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
		setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

		const RegionXu64 imageRegion{{0, 0, 0}, imageShape};
		imageView->writeRegion(imageRegion, data.get());

		// get upper and lower threshold
		TCDataConverter converter;
		const auto image = converter.ImageViewToImage(imageView);

		const auto autoThresholdModuleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.segmentation.threshold");
		const auto autoThresholdModule = std::dynamic_pointer_cast<IPluginAlgorithm>(autoThresholdModuleInstance);

		const auto autoThresholdParam = autoThresholdModuleInstance->Parameter();
		autoThresholdParam->SetValue("Method", IParameter::ValueType(true));

		autoThresholdModule->SetInput(0, image);
		autoThresholdModule->Execute();

		const auto autoThresholdResult = std::dynamic_pointer_cast<TC::Framework::DataList>(autoThresholdModule->GetOutput(0));
		const auto ulThreshold = std::dynamic_pointer_cast<DataSet>(autoThresholdResult->GetData(0));

		const auto lower = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("LThreshold"))->ValueAsDouble();
		const auto upper = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("UThreshold"))->ValueAsDouble();

		// get binary mask volume
		const auto thresholdingModuleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.segmentation.manualthreshold");
		const auto thresholdingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(thresholdingModuleInstance);

		const auto thresholdingParam = thresholdingModuleInstance->Parameter();
		thresholdingParam->SetValue("LThreshold", IParameter::ValueType(lower));
		thresholdingParam->SetValue("UThreshold", IParameter::ValueType(upper));

		thresholdingModule->SetInput(0, image);
		thresholdingModule->Execute();

		const auto instanceMask = std::dynamic_pointer_cast<TCMask>(thresholdingModule->GetOutput(0));

		sliceMask = Extract2DMask(instanceMask);

	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	REQUIRE(sliceMask != nullptr);

	SECTION("Dilation 2D processing") {
		const auto moduleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.morphop.dilation.2d");
		const auto algorithmModule = std::dynamic_pointer_cast<IPluginAlgorithm>(moduleInstance);

		const auto param = moduleInstance->Parameter();
		param->SetValue("KernelRadius", 5);
		param->SetValue("Neighborhood", 1);

		algorithmModule->SetInput(0, sliceMask);
		algorithmModule->Execute();

		const auto output = std::dynamic_pointer_cast<TCMask>(algorithmModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(sliceMask, "input");
		Save(output, "output");
	}

	imagedev::finish();
}
