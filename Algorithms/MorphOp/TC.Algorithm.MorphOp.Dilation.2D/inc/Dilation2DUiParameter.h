#pragma once

#include <IUiParameter.h>

namespace TC::Algorithm::MorphOp::Dilation2D {
    class UiParameter: public IUiParameter {
        Q_OBJECT
    public:
        static auto Register()->void;

        static auto CreateMethod()->IUiParameter::Pointer {
            return std::make_shared<UiParameter>();
        }

        static auto GetName()->std::string { return "org.tomocube.algorithm.morphop.dilation.2d"; }

        UiParameter();
    };
}