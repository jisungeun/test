project(Erosion2DTest)

ADD_DEFINITIONS(-D_TEST_DATA=\"${CMAKE_CURRENT_SOURCE_DIR}/data/source.tcf\" )
ADD_DEFINITIONS(-D_PLUGIN_DIR=\"${CMAKE_BINARY_DIR}/bin/$<CONFIG>/algorithms\" )

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
	src/TestMain.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_compile_features(${PROJECT_NAME} 
	PRIVATE
		cxx_std_17
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		Catch2::Catch2
		Qt5::Core
		TC::Components::EngineFramework
		TC::Components::DataTypes
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Algorithms/TestCases/MorphOp")
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_DEFINITIONS H5_BUILT_AS_DYNAMIC_LIB=1)