#include <ParameterRegistry.h>
#include "Erosion2DUiParameter.h"

namespace TC::Algorithm::MorphOp::Erosion2D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}