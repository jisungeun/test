#include <ParameterRegistry.h>
#include "ErosionDisk2DParameter.h"

namespace TC::Algorithm::MorphOp::ErosionDisk2D {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("KernelRadius", "Kernel radius", "A square structuring element", "ScalarValue.int", 3, 1, 100);

		RegisterNode("Precision", "Select precision", "Select the precision of the computation configuration", "SelectOption", 1, 0, 1);
		RegisterNode("PrecisionIndex", "Select precision", "Select the precision of the computation configuration", "ScalarValue.int", 1, 0, 1);
    }

}
