#pragma once

#include <IParameter.h>

namespace TC::Algorithm::MorphOp::ErosionDisk2D {
	class Parameter : public IParameter {
		Q_OBJECT

	public:
		static auto Register()->void;

		static IParameter::Pointer CreateMethod() {
			return std::make_shared<Parameter>();
		}

		static std::string GetName() { return "org.tomocube.algorithm.morphop.erosiondisk.2d"; }

		Parameter();
	};
}