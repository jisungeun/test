#include <ParameterRegistry.h>
#include "Closing25DUiParameter.h"

namespace TC::Algorithm::MorphOp::Closing25D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}