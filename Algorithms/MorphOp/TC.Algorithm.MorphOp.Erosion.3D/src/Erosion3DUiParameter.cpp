#include <ParameterRegistry.h>
#include "Erosion3DUiParameter.h"

namespace TC::Algorithm::MorphOp::Erosion3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}