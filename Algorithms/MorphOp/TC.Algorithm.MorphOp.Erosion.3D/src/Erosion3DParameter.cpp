#include <ParameterRegistry.h>
#include "Erosion3DParameter.h"

namespace TC::Algorithm::MorphOp::Erosion3D {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("KernelRadius", "Kernel radius", "A square structuring element", "ScalarValue.int", 3, 1, 100);

		RegisterNode("Neighborhood", "Select neighborhood", "Select neighborhood configuration", "SelectOption", 1, 0, 2);
		RegisterNode("NeighborhoodIndex", "Select neighborhood", "Select neighborhood configuration", "ScalarValue.int", 1, 0, 2);
    }

}
