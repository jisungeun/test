#include <ParameterRegistry.h>
#include "Erosion25DUiParameter.h"

namespace TC::Algorithm::MorphOp::Erosion25D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}