#define LOGGER_TAG "[Erosion2D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataConverter.h>

#include "Erosion25DUiParameter.h"
#include "Erosion25DParameter.h"
#include "Erosion25D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::MorphOp::Erosion25D {
	struct Algorithm::Impl {
		TCMask::Pointer input{ nullptr };
		TCMask::Pointer result{ nullptr };

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.morphop.erosion.2.5d") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.morphop.erosion.2.5d") };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

		d->input = std::dynamic_pointer_cast<TCMask>(data);
		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->result;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return {d->result};
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
			if (d->params.contains(key)) {
				return d->params[key];
			} else {
				return nullptr;
			}
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {

	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START Erosion2D";

		if (d->input == nullptr) {
			QLOG_INFO() << "Invalid input data";
			return false;
		}

		const auto kernelRadius = d->param->GetValue("KernelRadius").toInt(3);

		const auto neighborhood = static_cast<Erosion2d::Neighborhood>(d->param->GetValue("Neighborhood").toInt(1));

		try {
			QElapsedTimer etimer;
			etimer.start();

			const auto [sizeX, sizeY, sizeZ] = d->input->GetSize();

			TCDataConverter converter;
			const auto refMask = converter.MaskToImageView(d->input);
			auto destMask = resetImage(refMask, 0);
			for (auto i = 0; i < sizeZ; i++) {
				const auto sliced = getSliceFromVolume3d(refMask, GetSliceFromVolume3d::Z_AXIS, i);
				const auto slice_erode = erosion2d(sliced, kernelRadius, neighborhood);
				destMask = setSliceToVolume3d(destMask, slice_erode, SetSliceToVolume3d::Z_AXIS, i);
			}
			int dim[3] = { sizeX, sizeY, sizeZ };

			auto spacing = getCalibrationSpacing(destMask);
			double res[3] = { spacing[0], spacing[1], spacing[2] };

			d->result = converter.ArrToLabelMask((unsigned short*)destMask->buffer(), dim, res);

			QLOG_INFO() << QString("FINISH MedianFilter - total: %1 ms").arg(etimer.elapsed());
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return true;
	}
}
