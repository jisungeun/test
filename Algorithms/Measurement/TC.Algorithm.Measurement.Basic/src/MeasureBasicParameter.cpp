#include <ParameterRegistry.h>
#include "MeasureBasicParameter.h"

namespace TC::Algorithm::Measurement::Basic {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() {
        SetVersion("1.0.0");
        //RegisterNode("CustomValue", "Customize", "Use preset coefficient", "TFCheck", false, "", "");
        RegisterNode("CustomRI", "Custom Medium RI", "Use File RI", "TFCheck", false, "", "");
        RegisterNode("RI", "Medium RI", "Set Medium RI", "ScalarValue.double", 1.337, 1.0, 3.0);
        RegisterNode("Preset", "Preset", "Set Measure Coefficient Preset", "SelectOption", 0, 0, 2);                
        RegisterNode("RII", "RII", "Set RII", "ScalarValue.double", 0.19, 0.0, 1.0);
        RegisterNode("RIMin", "Refractive Index Min", "", "ScalarValue.double", 13300.0, 13300.0, 14000.0);
        RegisterNode("RIMax", "Refractive Index Max", "", "ScalarValue.double", 13300.0, 13300.0, 14000.0);        
    }
}