#include <ParameterRegistry.h>
#include "MeasureBasicUiParameter.h"

namespace TC::Algorithm::Measurement::Basic {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetType("Table");
        RegisterTableItem("TableItem", QStringList{ "Preset","RII" });

        SetFullName(QString(GetName().c_str()));

        //RegisterEnabler("CustomValue", "Customize", QStringList{ "RI","RII" }, QStringList{ "Preset" });
        //RegisterSetter("Preset", "Preset", QStringList{ "RI","RII" }, QStringList{ "Protein","Hemoglobin","Lipid" });
        RegisterEnabler("CustomRI", "Custom Medium RI", QStringList{ "RI" }, QStringList{});
        RegisterHider("Hider1", QStringList{ "RI" });
        RegisterSetter("Preset", "Preset", QStringList{ "RII" }, QStringList{ "Protein","Hemoglobin","Lipid" });
        //AppendSetter("Preset", "RI", "Protein", 1.337);
        //AppendSetter("Preset", "RI", "Hemoglobin", 1.337);
        //AppendSetter("Preset", "RI", "Lipid", 1.337);
        AppendSetter("Preset", "RII", "Protein", 0.19);
        AppendSetter("Preset", "RII", "Hemoglobin", 0.15);
        AppendSetter("Preset", "RII", "Lipid", 0.135);
        RegisterSorter("Order", QStringList{/*"CustomValue",*/"CustomRI","RI","Preset","RII" /*,IMin","RIMax" */ });
    }
}
