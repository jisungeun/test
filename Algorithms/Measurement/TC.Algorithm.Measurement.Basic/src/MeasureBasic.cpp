#define LOGGER_TAG "[MeasureBasic]"
#include <TCLogger.h>

#include <iostream>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#include <ImageViz/SoImageViz.h>
#include <ImageViz/Engines/SoImageVizEngineHeaders.h>
#include <ImageViz/Nodes/Measures/SoDataMeasurePredefined.h>
#include <Medical/helpers/MedicalHelper.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Measures/SoDataMeasureCustom.h>
#pragma warning(pop)

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCMask.h>
#include <TCDataConverter.h>
#include <TCFMetaReader.h>
#include <OIVMutex.h>

#include <TCPython.h>

#include "MeasureBasicUiParameter.h"
#include "MeasureBasicParameter.h"
#include "MeasureBasic.h"

#include <QCoreApplication>
#include <QElapsedTimer>


using namespace TC::Framework;


namespace TC::Algorithm::Measurement::Basic {
	struct Algorithm::Impl {		
		TCImage::Pointer refImage;		
		TCMask::Pointer inst_input;
		TCMask::Pointer organ_input;		
		DataList::Pointer result;

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.measurement.basic") };
		QMap<QString, IParameter::Pointer> params;

		//measurement properties				
		QMap<QString,double> RefractiveIndex;
		QMap<QString, double> RII;	    
		double RIMin;
		double RIMax;
				
		QList<QList<int>> exist_label_idx;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}
	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 2) return false;

		if (index == 0) {
		    d->refImage = std::dynamic_pointer_cast<TCImage>(data);
		} else if (index == 1) {
		    d->inst_input = std::dynamic_pointer_cast<TCMask>(data);
		} else if (index == 2) {
		    d->organ_input = std::dynamic_pointer_cast<TCMask>(data);
		}
		
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index)
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		if (key == "Method!Setter") {
			return std::make_tuple("Preset", param->GetValue("Method!Setter"));
		}	
		if(key == "RI") {
			return std::make_tuple("RI", param->GetValue("RI"));
		}
		if(key == "RII") {
			return std::make_tuple("RII", param->GetValue("RII"));
		}
		if(key=="RIMin") {
			return std::make_tuple("RIMin", param->GetValue("RIMin"));
		}
		if(key == "RIMax") {
			return std::make_tuple("RIMax", param->GetValue("RIMax"));
		}
		return std::make_tuple(QString(), QJsonValue());
    }

	auto Algorithm::Execute() -> bool {
		//executing basic analysis
		std::cout << "START Measurement" << std::endl;
		QElapsedTimer etimer;
		etimer.start();
	    d->exist_label_idx.clear();		
		d->result = nullptr;
		d->result = DataList::New();
		std::cout << "Create New Result" << std::endl;

		auto inst_valid = d->inst_input->IsValid();
		auto organ_valid = d->organ_input->IsValid();

		if(!inst_valid || !organ_valid) {		
			QLOG_INFO() << "Input mask invalid, create dummy(-1) result";
			CreateDummyResult();
			std::cout << "Create Dummy Result" << std::endl;
			return true;
		}
		//set parameter for each mask
		//get Mask List				
		try {
			auto mediumRI = 1.337;

			auto isCustomRI = false;

			if (d->params.size() > 0) {
				auto firstNode = d->params[d->params.keys()[0]];
				if (firstNode->ExistNode("CustomRI")) {
					isCustomRI = firstNode->GetValue("CustomRI").toBool();
				}
				else {
					isCustomRI = true;
				}
			}
			else {
				if (d->param->ExistNode("CustomRI")) {
					isCustomRI = d->param->GetValue("CustomRI").toBool();
				}
				else {
					isCustomRI = true;
				}
			}
			if (false == isCustomRI) {
				auto filePath = d->refImage->GetFilePath();
				if (false == filePath.isEmpty()) {
					auto metaReader = new TC::IO::TCFMetaReader;
					auto tmpRI = metaReader->ReadMediumRI(d->refImage->GetFilePath());

					if (0.0 != tmpRI) {
						mediumRI = tmpRI;
					}
					delete metaReader;
				}
			}

			QLOG_INFO() << "Set Parameter";
			if (d->params.size() > 0) {
				auto names = d->params.keys();
				for (auto n : names) {
					d->RII[n] = d->params[n]->GetValue("RII").toDouble();
					if (isCustomRI) {
						d->RefractiveIndex[n] = d->params[n]->GetValue("RI").toDouble();
					}
					else {
						d->RefractiveIndex[n] = mediumRI;
					}
				}
			}
			else {
				d->RII["membrane"] = d->param->GetValue("RII").toDouble();
				if (isCustomRI) {
					d->RefractiveIndex["membrane"] = d->param->GetValue("RI").toDouble();
				}
				else {
					d->RefractiveIndex["membrane"] = mediumRI;
				}
			}

			d->RIMin = d->param->GetValue("RIMin").toDouble();
			d->RIMax = d->param->GetValue("RIMax").toDouble();

			std::cout << "Set parameter" << std::endl;

			//Separate organ input & inst input here
			auto conv = std::shared_ptr<TCDataConverter>(new TCDataConverter, std::default_delete<TCDataConverter>());

			auto numLayer = d->organ_input->GetLayerNames().size();
			auto names = d->organ_input->GetLayerNames();

			std::cout << "Start Measure" << std::endl;
			QLOG_INFO() << "Start Measure";
			double score = -1;
			if(d->organ_input->GetLayerNames().count()>3)
			{

				PythonModule::GetInstance()->ExecuteString(QString("file_path = '%1'").arg(d->organ_input->GetPath()));
				//copy array and information to python code
				auto sizeX = std::get<0>(d->organ_input->GetSize());
				auto sizeY = std::get<1>(d->organ_input->GetSize());
				auto sizeZ = std::get<2>(d->organ_input->GetSize());

				PythonModule::GetInstance()->ExecuteString(QString("size_x = %1").arg(sizeX));
				PythonModule::GetInstance()->ExecuteString(QString("size_y = %1").arg(sizeY));
				PythonModule::GetInstance()->ExecuteString(QString("size_z = %1").arg(sizeZ));
												
				//nucleus copy
				auto nucBlob = d->organ_input->GetBlob(1);
				auto nucBB = nucBlob.GetBoundingBox();
				unsigned nucDim[3]{ std::get<3>(nucBB) - std::get<0>(nucBB) + 1,std::get<4>(nucBB) - std::get<1>(nucBB) + 1,std::get<5>(nucBB) - std::get<2>(nucBB) + 1 };
				PythonModule::GetInstance()->ExecuteString(QString("nuc_blob_info = [-1,%1,%2,%3,%4,%5,%6]").arg(std::get<0>(nucBB)).arg(std::get<1>(nucBB)).arg(std::get<2>(nucBB)).arg(nucDim[0]).arg(nucDim[1]).arg(nucDim[2]));
				auto nucData = nucBlob.GetMaskVolume();				
				unsigned nndim[3]{ nucDim[2],nucDim[1],nucDim[0] };
				PythonModule::GetInstance()->CopyArrToPy(nucData.get(), 3, nndim, "nuc_mask",ArrType::arrUBYTE,ArrType::arrUBYTE);
				//nucleoli copy
				auto nucliBlob = d->organ_input->GetBlob(2);				
				auto nucliBB = nucliBlob.GetBoundingBox();
				unsigned nucliDim[3]{ std::get<3>(nucliBB) - std::get<0>(nucliBB) + 1,std::get<4>(nucliBB) - std::get<1>(nucliBB) + 1,std::get<5>(nucliBB) - std::get<2>(nucliBB) + 1 };
				PythonModule::GetInstance()->ExecuteString(QString("nco_blob_info = [-1,%1,%2,%3,%4,%5,%6]").arg(std::get<0>(nucliBB)).arg(std::get<1>(nucliBB)).arg(std::get<2>(nucliBB)).arg(nucliDim[0]).arg(nucliDim[1]).arg(nucliDim[2]));
				auto nucliData = nucliBlob.GetMaskVolume();				
				unsigned nidim[3]{ nucliDim[2],nucliDim[1],nucliDim[0] }; 
				PythonModule::GetInstance()->CopyArrToPy(nucliData.get(), 3, nidim, "nucleoli_mask", ArrType::arrUBYTE, ArrType::arrUBYTE);
				 
				const auto functionPath = QString("%1/py/ScoreFunction.py").arg(qApp->applicationDirPath());
				const auto executionPath = QString("%1/py/ScoreExecution.py").arg(qApp->applicationDirPath());  

				PythonModule::GetInstance()->ExecutePyFile(functionPath);
				PythonModule::GetInstance()->ExecutePyFile(executionPath); 

				score = PythonModule::GetInstance()->GetDoubleValue("score_total");

				std::cout << "score total: " << score << std::endl;
			}

			SoRef<SoVolumeData> labeledImage = conv->MaskToSoVolumeData(d->inst_input);

			std::cout << "Conv inst input" << std::endl;

			SoRef<SoVolumeData> refData = conv->ImageToSoVolumeData(d->refImage);

			std::cout << "Conv refImage" << std::endl;

			SoRef<SoMemoryDataAdapter> orilabelAdapter = MedicalHelper::getImageDataAdapter(labeledImage.ptr());
			orilabelAdapter->interpretation = SoMemoryDataAdapter::Interpretation::LABEL;

			std::cout << "get labelImage adapter" << std::endl;

			SoRef<SoMemoryDataAdapter> refAdapter = MedicalHelper::getImageDataAdapter(refData.ptr());

			std::cout << "get refData adapter" << std::endl;

			for (int i = 0; i < numLayer; i++) {
				//IO::OIVMutexLocker local_lock(IO::OIVMutex::GetInstance());
				SoRef<SoLabelAnalysisQuantification> analysis = new SoLabelAnalysisQuantification;
				analysis->measureList.set1Value(0, new SoDataMeasurePredefined(SoDataMeasurePredefined::VOLUME_3D));
				//1: Surface area (um^2)
				analysis->measureList.set1Value(1, new SoDataMeasurePredefined(SoDataMeasurePredefined::AREA_3D));
				//2: Mean RI
				analysis->measureList.set1Value(2, new SoDataMeasurePredefined(SoDataMeasurePredefined::INTENSITY_MEAN));
				analysis->inIntensityImage = refAdapter.ptr();

				SoRef<SoReorderLabelsProcessing> reorder = new SoReorderLabelsProcessing;

				//auto layer_volume = conv->MaskToSoVolumeData(d->organ_input, names[i]);
				SoRef<SoVolumeData> layer_volume = conv->MaskToSoVolumeData(d->organ_input, names[i]);
				//layer_volume->ref();

				std::cout << "Conv organ_input " << names[i].toStdString() << std::endl;

				SoRef<SoMemoryDataAdapter> maskAdapter = MedicalHelper::getImageDataAdapter(layer_volume.ptr());
				maskAdapter->interpretation = SoMemoryDataAdapter::Interpretation::BINARY;

				std::cout << "get layer volume adapter" << std::endl;

				SoRef<SoMaskImageProcessing> maskFilter = new SoMaskImageProcessing;
				maskFilter->inBinaryImage = maskAdapter.ptr();
				maskFilter->inImage = orilabelAdapter.ptr();

				SoRef<SoVRImageDataReader> inputImageReader = new SoVRImageDataReader;
				inputImageReader->imageData.connectFrom(&maskFilter->outImage);

				SoRef<SoVolumeData> labeled_layer = new SoVolumeData;
				labeled_layer->setReader(*inputImageReader.ptr(), TRUE);

				auto dim = labeled_layer->getDimension();
				SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
				SoLDMDataAccess::DataInfoBox dataInfoBox =
					labeled_layer->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
				SoRef<SoCpuBufferObject> ssdataBufferObj = new SoCpuBufferObject;
				ssdataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

				labeled_layer->getLdmDataAccess().getData(0, bBox, ssdataBufferObj.ptr());

				unsigned short* data = static_cast<unsigned short*>(ssdataBufferObj->map(SoBufferObject::READ_ONLY));
				QMap<int, bool> tmp;
				for (auto i = 0; i < dim[0] * dim[1] * dim[2]; i++) {
					auto val = static_cast<int>(*(data + i));
					tmp[val] = true;
				}
				ssdataBufferObj->unmap();

				QList<int> tExist = QList<int>();
				for (auto k : tmp.keys()) {
					if (k != 0) {
						tExist.push_back(k);
					}
				}
				d->exist_label_idx.push_back(tExist);

				auto labelName = d->organ_input->GetLayerNames()[i];

				auto localRII = 0.19;
				auto localRI = 1.337;
				if (d->RII.contains(labelName)) {
					localRII = d->RII[labelName];
					localRI = d->RefractiveIndex[labelName];
				}

				reorder->inLabelImage.connectFrom(&maskFilter->outImage);
				analysis->inLabelImage.connectFrom(&reorder->outLabelImage);

				SoRef<SoLabelAnalysisResult> analysisOutput = analysis->outAnalysis.getValue();

				//get measruement result
				DataSet::Pointer dset = DataSet::New();
				auto layerName = ScalarData::New(labelName);
				dset->AppendData(layerName, "Name");
				auto numLabels = d->exist_label_idx[i].size();
				auto labelNum = ScalarData::New(numLabels);
				dset->AppendData(labelNum, "CellCount");

				QLOG_INFO() << "Organ Name: " << labelName;

				std::cout << "Organ Name: " << labelName.toStdString() << std::endl;

				for (int j = 0; j < numLabels; j++) {
					auto real_idx = d->exist_label_idx[i][j];
					QLOG_INFO() << "Cell number: " << real_idx;
					double volume = -1;
					if (nullptr != analysisOutput.ptr()) {
						volume = analysisOutput->getValueAsDouble(j, 0, 0);
					}
					if (volume > 0) {
						auto realScalar = ScalarData::New(real_idx);
						dset->AppendData(realScalar, "real_idx" + QString::number(j));

						auto volumeScalar = ScalarData::New(volume);
						volumeScalar->setLabelIdx(real_idx);
						dset->AppendData(volumeScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Volume");

						auto surface_area = analysisOutput->getValueAsDouble(j, 1, 0);

						auto saScalar = ScalarData::New(surface_area);
						saScalar->setLabelIdx(real_idx);
						dset->AppendData(saScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".SurfaceArea");

						auto mean_ri = analysisOutput->getValueAsDouble(j, 2, 0) / 10000.0;
						auto mrScalar = ScalarData::New(mean_ri);
						mrScalar->setLabelIdx(real_idx);
						dset->AppendData(mrScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".MeanRI");

						auto phi = pow(3.14, 1.0 / 3.0);
						auto factor = 36.0 * volume * volume;
						auto sa = pow(factor, 1.0 / 3.0);
						auto sphericity = phi * sa / surface_area;
						auto spScalar = ScalarData::New(sphericity);
						spScalar->setLabelIdx(real_idx);
						dset->AppendData(spScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Sphericity");

						auto actual_label = real_idx;

						SoRef<SoThresholdingProcessing> threshold = new SoThresholdingProcessing;
						threshold->inImage.connectFrom(&maskFilter->outImage);
						threshold->thresholdLevel.setValue((float)actual_label, (float)actual_label + 0.1);

						SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
						reader->imageData.connectFrom(&threshold->outBinaryImage);

						SoRef<SoVolumeData> vol = new SoVolumeData;
						vol->setReader(*reader, TRUE);

						auto dim = vol->getDimension();

						SbBox3i32 bBox(SbVec3i32(0, 0, 0),
							SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
						SoLDMDataAccess::DataInfoBox dataInfoBox =
							vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

						SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
						dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);
						vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
						uint8_t* buffer = static_cast<uint8_t*>(dataBufferObj->map(SoBufferObject::READ_WRITE));

						uint8_t* result = new uint8_t[dim[0] * dim[1]]();
						for (auto i = 0; i < dim[0]; i++) {
							for (auto j = 0; j < dim[1]; j++) {
								for (auto k = 0; k < dim[2]; k++) {
									auto res_idx = i * dim[1] + j;
									auto target_idx = i * dim[1] * dim[2] + j * dim[2] + k;
									auto val = *(buffer + target_idx);
									if (val != 0) {
										*(result + res_idx) = 1;
									}
								}
							}
						}

						dataBufferObj->unmap();

						auto cnt = 0;
						auto res = vol->getVoxelSize();
						for (auto i = 0; i < dim[0]; i++) {
							for (auto j = 0; j < dim[1]; j++) {
								auto res_idx = i * dim[1] + j;
								if (*(result + res_idx) > 0) {
									cnt++;
								}
							}
						}
						delete[] result;
						auto projected_area = res[0] * res[1] * cnt;

						auto paScalar = ScalarData::New(projected_area);
						paScalar->setLabelIdx(real_idx);
						dset->AppendData(paScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".ProjectedArea");

						auto drymass = (mean_ri - localRI) * volume / localRII;
						auto dmScalar = ScalarData::New(drymass);
						if (drymass < 0) {
							dmScalar = ScalarData::New(-1);
						}
						dmScalar->setLabelIdx(real_idx);
						dset->AppendData(dmScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Drymass");

						auto concentration = drymass / volume;
						auto ccScalar = ScalarData::New(concentration);
						if (concentration < 0) {
							ccScalar = ScalarData::New(-1);
						}
						ccScalar->setLabelIdx(real_idx);
						dset->AppendData(ccScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Concentration");						
					}
					else {//volume이 없으면 모두 -1
						auto realScalar = ScalarData::New(real_idx);//cell indexing만 제외하고
						dset->AppendData(realScalar, "real_idx" + QString::number(j));

						auto volumeScalar = ScalarData::New(-1);
						volumeScalar->setLabelIdx(real_idx);
						dset->AppendData(volumeScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Volume");

						auto saScalar = ScalarData::New(-1);
						saScalar->setLabelIdx(real_idx);
						dset->AppendData(saScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".SurfaceArea");

						auto mrScalar = ScalarData::New(-1);
						mrScalar->setLabelIdx(real_idx);
						dset->AppendData(mrScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".MeanRI");

						auto spScalar = ScalarData::New(-1);
						spScalar->setLabelIdx(real_idx);
						dset->AppendData(spScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Sphericity");

						auto paScalar = ScalarData::New(-1);
						paScalar->setLabelIdx(real_idx);
						dset->AppendData(paScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".ProjectedArea");

						auto dmScalar = ScalarData::New(-1);
						dmScalar->setLabelIdx(real_idx);
						dset->AppendData(dmScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Drymass");

						auto ccScalar = ScalarData::New(-1);
						ccScalar->setLabelIdx(real_idx);
						dset->AppendData(ccScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Concentration");
					}
					auto scScalar = ScalarData::New(score);
					dset->AppendData(scScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Score");
				}
				d->result->Append(dset);				
			}
			
			QLOG_INFO() << "elapsed time for measurement: ";
			QLOG_INFO() << etimer.elapsed();
			conv = nullptr;

			std::cout << "FINISH Measurement" << std::endl;
			QLOG_INFO() << "Measurement finished";
			return TRUE;
		}catch(std::exception& e) {
			QLOG_INFO() << e.what();
		}catch(...) {
			QLOG_INFO() << "UNKNOWN ERROR";
		}

		QLOG_INFO() << "Error occured, create dummy(-1) result";
		CreateDummyResult();
		std::cout << "Create Dummy Result" << std::endl;
		return true;
	}
	auto Algorithm::CreateDummyResult() -> void {
		auto layerNum = d->organ_input->GetLayerNames().size();
		for (int i = 0; i < layerNum; i++) {
			auto labelName = d->organ_input->GetLayerNames()[i];
			DataSet::Pointer dset = DataSet::New();
			auto layerName = ScalarData::New(labelName);
			dset->AppendData(layerName, "Name");

			auto numLabels = 1;//무조건 하나
			auto real_idx = 1;//무조건 1
			auto labelNum = ScalarData::New(numLabels);
			dset->AppendData(labelNum, "CellCount");

			auto realScalar = ScalarData::New(real_idx);
			dset->AppendData(realScalar, "real_idx" + QString::number(0));

			auto volumeScalar = ScalarData::New(-1);
			volumeScalar->setLabelIdx(1);
			dset->AppendData(volumeScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Volume");

			auto saScalar = ScalarData::New(-1);
			saScalar->setLabelIdx(real_idx);
			dset->AppendData(saScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".SurfaceArea");
						
			auto mrScalar = ScalarData::New(-1);
			mrScalar->setLabelIdx(real_idx);
			dset->AppendData(mrScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".MeanRI");
						
			auto spScalar = ScalarData::New(-1);
			spScalar->setLabelIdx(real_idx);
			dset->AppendData(spScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Sphericity");
						
			auto paScalar = ScalarData::New(-1);
			paScalar->setLabelIdx(real_idx);
			dset->AppendData(paScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".ProjectedArea");
						
			auto dmScalar = ScalarData::New(-1);
			dmScalar->setLabelIdx(real_idx);
			dset->AppendData(dmScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Drymass");
						
			auto ccScalar = ScalarData::New(-1);
			ccScalar->setLabelIdx(real_idx);
			dset->AppendData(ccScalar, "cell" + QString(std::to_string(real_idx).c_str()) + ".Concentration");

			d->result->Append(dset);
		}
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		}else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    }else {
				return nullptr;
		    }
		}
		
	}
	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }
	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        for(const auto& key : keys) {
			d->params[key] = ParameterRegistry::Create("org.tomocube.algorithm.measurement.basic");
			if(key.contains("lipid droplet")) {
			    d->params[key]->SetValue("Preset",2);
			}
        }
    }	
}
