#pragma once

#include <QObject>

#include "enum.h"

#include <IPluginAlgorithm.h>
#include <DataList.h>
#include <ScalarData.h>

#include "TC.Algorithm.Measurement.BasicExport.h"

class SoMaskImageProcessing;
class SoMemoryDataAdapter;
class SoLabelAnalysisQuantification;
class SoVolumeData;

namespace TC::Algorithm::Measurement::Basic {
	BETTER_ENUM(MeasureName, int,
		VOLUME = 100,
		SURFACE_AREA = 101,
		PROJ_AREA = 102,
		MEAN_RI = 103,
		SPHERICITY = 104,
		DRYMASS = 105,
		CONCENTRATAION = 106,
		NONE = -1
	)

	class TC_Algorithm_Measurement_Basic_API Algorithm
		:public QObject
		, public IPluginAlgorithm
	{
		Q_OBJECT
			Q_PLUGIN_METADATA(IID "org.tomocube.algorithm.measurement.basic" FILE "AlgorithmMetadata.json")
			Q_INTERFACES(IPluginAlgorithm)

	public:
		Algorithm();
		virtual ~Algorithm();

		auto clone() const->IPluginModule* override;

		auto Parameter(const QString& key)->IParameter::Pointer override;
		auto DuplicateParameter(const QStringList& keys) -> void override;

	    auto UiParameter() -> IUiParameter::Pointer override;
	    auto ParamConverter(IParameter::Pointer, QString key) -> std::tuple<QString, QJsonValue> override;

        auto SetInput(int index, IBaseData::Pointer data) -> bool override;
		auto GetOutput(int index) const -> IBaseData::Pointer override;
		auto GetOutputs() const -> QList<IBaseData::Pointer> override;

	    auto Execute() -> bool override;

	private:
		auto CreateDummyResult()->void;
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}