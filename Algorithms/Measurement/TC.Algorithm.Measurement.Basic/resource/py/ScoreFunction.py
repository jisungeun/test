import os
import sys
import numpy as np
import cv2
#import h5py
import cc3d
from glob import glob

def isconvex(contours):
    """whether contours are covex or not"""
    cnt = contours[0][0]
    area = cv2.contourArea(cnt)
    hull = cv2.convexHull(cnt)
    hull_area = cv2.contourArea(hull)
    score = float(area) / (hull_area + 1)
    score = round(score * 100, 2)
    if score > 95:
        return True
    else:
        return False

def isfilled(contours, nuc_mip):
    """whether contours are filled or not"""
    drawing = np.zeros_like(nuc_mip, np.uint8)  # create a black image
    img_contour = cv2.drawContours(drawing, contours[0], 0, 1, -1)
    if img_contour.sum() == nuc_mip.sum():
        return True
    else:
        return False

def isvalid(nuc):
    """whether size is valid"""
    if nuc.sum() < 5000:
        return False
    else:
        return True
        
def scoring(seed_label, nucleoli_mask):
    """get scores"""
    scores = {"total": 0, "convex": 0, "filled": 0, "size": 0}
    cnt = 0    
    for i in range(int(seed_label.max())):        
        nuc_idx = np.zeros(seed_label.shape)        
        nuc_idx[seed_label == (i + 1)] = 1
        if nuc_idx.sum() < 200:
            continue
        # skip seed if it does not include nucleoli        
        nucleoli_inst = nucleoli_mask.copy()        
        nucleoli_inst[nuc_idx != 1] = 0
        if nucleoli_inst.sum() < 10:
            continue        
        cnt += 1        
        nuc_mip = nuc_idx.max(0)                        
        contours = cv2.findContours(nuc_mip.astype(np.uint8), 1, 2)        
        convexity = isconvex(contours)        
        filled = isfilled(contours, nuc_mip)        
        size = isvalid(nuc_idx)
        scores["total"] += (convexity + filled + size) / 3
        scores["convex"] += convexity
        scores["filled"] += filled
        scores["size"] += size
    if cnt > 0:
        for key, value in scores.items():
            scores[key] = round(value / cnt, 4)
    return scores