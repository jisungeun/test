#pass mask file path from TomoAnalysis

#with h5py.File(file_path, "r") as hdf:
#    size_x = int(hdf.attrs["SizeX"])
#    size_y = int(hdf.attrs["SizeY"])
#    size_z = int(hdf.attrs["SizeZ"])

#blob_path = os.path.join(file_path.replace(".msk", ""), "Blobs", "HT", "000000")

#blob_path = blob_path.replace("\\","/")

#with h5py.File(blob_path, "r") as hdf:
    #nuc_blob_info2 = (hdf["BlobData"])[1:2][0]
    #nco_blob_info2 = (hdf["BlobData"])[2:3][0]
    
#nuc_path = os.path.join(file_path.replace(".msk", ""), "Masks", "HT", "000000", "000001")

#nuc_path = nuc_path.replace("\\","/")

#with h5py.File(nuc_path, "r") as hdf:
#    nuc_mask = hdf["MaskData"][...]
seed_label = cc3d.connected_components(nuc_mask.astype(np.uint8))
#print("step4")
    
#nucleoli_path = os.path.join(
#    file_path.replace(".msk", ""), "Masks", "HT", "000000", "000002"
#)

#nucleoli_path = nucleoli_path.replace("\\","/")

#print(nucleoli_path)

#with h5py.File(nucleoli_path, "r") as hdf:
#    nucleoli_mask = hdf["MaskData"][...]
    
nucleoli_mask_pad = np.zeros((size_z, size_y, size_x))
nucleoli_mask_pad[
    nco_blob_info[3] : nco_blob_info[3] + nco_blob_info[6],
    nco_blob_info[2] : nco_blob_info[2] + nco_blob_info[5],
    nco_blob_info[1] : nco_blob_info[1] + nco_blob_info[4],
] = nucleoli_mask

nucleoli_mask = nucleoli_mask_pad[
    nuc_blob_info[3] : nuc_blob_info[3] + nuc_blob_info[6],
    nuc_blob_info[2] : nuc_blob_info[2] + nuc_blob_info[5],
    nuc_blob_info[1] : nuc_blob_info[1] + nuc_blob_info[4],
] 
scores = scoring(seed_label, nucleoli_mask)

score_total = scores["total"]