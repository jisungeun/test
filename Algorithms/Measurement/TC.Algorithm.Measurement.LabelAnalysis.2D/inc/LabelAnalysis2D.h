#pragma once

#include <QObject>

#include "enum.h"

#include <IPluginAlgorithm.h>

#include "TC.Algorithm.Measurement.LabelAnalysis.2DExport.h"

namespace TC::Algorithm::Measurement::LabelAnalysis2D {
	BETTER_ENUM(MeasureName, int,
		MEAN_RI = 100,		
		SURFACE_AREA,
		CIRCULARITY,
		FERET,
		CENTROID_X,
		CENTROID_Y,
		NONE = -1
	)

	class TC_Algorithm_Measurement_LabelAnalysis_2D_API Algorithm
		:public QObject
		, public IPluginAlgorithm
	{
		Q_OBJECT
			Q_PLUGIN_METADATA(IID "org.tomocube.algorithm.measurement.labelanalysis.2d" FILE "AlgorithmMetadata.json")
			Q_INTERFACES(IPluginAlgorithm)

	public:
		Algorithm();
		virtual ~Algorithm();

		auto clone() const->IPluginModule* override;

        auto Parameter(const QString& key)->IParameter::Pointer override;
		auto DuplicateParameter(const QStringList& keys) -> void override;	// deprecated

		auto UiParameter() -> IUiParameter::Pointer override;
		auto ParamConverter(IParameter::Pointer, QString key) -> std::tuple<QString, QJsonValue> override;

		auto SetInput(int index, IBaseData::Pointer data) -> bool override;
		auto GetOutput(int index) const -> IBaseData::Pointer override;
		auto GetOutputs() const -> QList<IBaseData::Pointer> override;

		auto Execute() -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}