#include <ParameterRegistry.h>
#include "LabelAnalysis2DUiParameter.h"

namespace TC::Algorithm::Measurement::LabelAnalysis2D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
        RegisterHider("Hider1", QStringList{ "IntensityDiv","ImageType" });
    }
}