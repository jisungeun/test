#define LOGGER_TAG "[LabelAnalysis2D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Data/MeasurementAttributes.h>

#include <ParameterRegistry.h>
#include <ScalarData.h>
#include <DataList.h>
#include <TCImage.h>
#include <TCMask.h>
#include <TCFMetaReader.h>
#include <TCDataConverter.h>

#include "LabelAnalysis2DUiParameter.h"
#include "LabelAnalysis2DParameter.h"
#include "LabelAnalysis2D.h"


using namespace TC::Framework;
using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Measurement::LabelAnalysis2D {
	struct Algorithm::Impl {
		TCImage::Pointer inputImage { nullptr };
		TCMask::Pointer inputMask { nullptr };
		DataList::Pointer result { nullptr };
		double intensityDiv { 10000 };
		IParameter::Pointer param { ParameterRegistry::Create("org.tomocube.algorithm.measurement.labelanalysis.2d") };
		IUiParameter::Pointer uiParam { UiParameterRegistry::Create("org.tomocube.algorithm.measurement.labelanalysis.2d") };

		QString imageType { "HT" };
		QMap<QString, QMap<MeasureName, QString>> measureKeyMap;

		auto ExistLabelIndexes(std::shared_ptr<ImageView> image) -> QList<int>;

		auto CreateDummyResult() -> DataList::Pointer;
		auto FillDummyMeasurementData(int index, DataSet::Pointer dataSet) -> void;
		auto BuildMeasureKeyMap() -> void;
	};

	auto Algorithm::Impl::BuildMeasureKeyMap() -> void {
		//measure keys for HT
		const QMap<MeasureName, QString> htMeasureKeys {
			{ MeasureName::MEAN_RI, "MeanRI" },
			{ MeasureName::SURFACE_AREA, "SurfaceArea" },
			{ MeasureName::CIRCULARITY, "Circularity" },
			{ MeasureName::FERET, "FeretDiameter" },
			{ MeasureName::CENTROID_X, "CentroidX" },
			{ MeasureName::CENTROID_Y, "CentroidY" }
		};
		measureKeyMap["HT"] = htMeasureKeys;
		const QMap<MeasureName, QString> flMeasureKeys {
			{ MeasureName::MEAN_RI, "MeanIntensity" },
			{ MeasureName::SURFACE_AREA, "SurfaceArea" },
			{ MeasureName::FERET, "FeretDiameter" },
			{ MeasureName::CIRCULARITY, "Circularity" },
			{ MeasureName::CENTROID_X, "CentroidX" },
			{ MeasureName::CENTROID_Y, "CentroidY" }
		};
		measureKeyMap["FL"] = flMeasureKeys;
	}

	auto Algorithm::Impl::ExistLabelIndexes(std::shared_ptr<ImageView> image) -> QList<int> {
		const auto buffer = static_cast<unsigned short*>(image->buffer());
		const auto dim = image->shape();

		QList<int> existLabels;
		for (uint64_t i = 0; i < dim[0] * dim[1]; i++) {
			auto val = static_cast<int>(*(buffer + i));
			if (val != 0 && !existLabels.contains(val))
				existLabels << val;
		}

		std::sort(existLabels.begin(), existLabels.end());

		return existLabels;
	}

	auto Algorithm::Impl::CreateDummyResult() -> DataList::Pointer {
		auto dummyResult = DataList::New();

		DataSet::Pointer dset = DataSet::New();

		auto realScalar = ScalarData::New(0);
		dset->AppendData(realScalar, "real_idx" + QString::number(0));

		FillDummyMeasurementData(0, dset);

		dummyResult->Append(dset);

		return dummyResult;
	}

	auto Algorithm::Impl::FillDummyMeasurementData(int index, DataSet::Pointer dataSet) -> void {
		for (const auto& key : measureKeyMap[imageType]) {
			const auto scalar = ScalarData::New(-1);
			scalar->setLabelIdx(index);
			dataSet->AppendData(scalar, key);
		}
	}

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
		d->BuildMeasureKeyMap();
	}

	Algorithm::~Algorithm() { }

	auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 1)
			return false;

		if (index == 0) {
			d->inputImage = std::dynamic_pointer_cast<TCImage>(data);
		} else if (index == 1) {
			d->inputMask = std::dynamic_pointer_cast<TCMask>(data);
		}

		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->result;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START LabelAnalysis2D";

		d->result = DataList::New();

		QElapsedTimer etimer;
		etimer.start();

		bool succeed = false;

		try {
			d->intensityDiv = d->param->GetValue("IntensityDiv").toDouble();
			d->imageType = d->param->GetValue("ImageType").toString();
			const auto feretCnt = d->param->GetValue("FeretCount").toInt();

			if (d->inputImage == nullptr)
				throw std::runtime_error("Invalid input image\n");

			if (d->inputMask == nullptr || !d->inputMask->IsValid())
				throw std::runtime_error("Invalid input mask\n");

			TCDataConverter converter;
			const auto refImage = converter.ImageToImageView(d->inputImage);
			auto labeledImage = converter.MaskToImageView(d->inputMask);

			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto imageOrigin = refImage->properties()->calibration().origin();
			const auto maskOrigin = labeledImage->properties()->calibration().origin();
			const auto imageSpacing = refImage->properties()->calibration().spacing();
			const auto maskSpacing = labeledImage->properties()->calibration().spacing();

			auto originDiff = false;
			auto spacingDiff = false;

			for (auto i = 0; i < 2; i++) {
				if (false == AreSame(imageOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(imageSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (refImage->shape() != labeledImage->shape() || originDiff || spacingDiff) {
				labeledImage = TCDataConverter::MapMaskGeometry(refImage, labeledImage);
			}

			const auto reorderLabelImage = reorderLabels(labeledImage);
			const auto labels = d->ExistLabelIndexes(reorderLabelImage);

			const auto analysis = std::make_shared<AnalysisMsr>();
			const auto areaMsr = analysis->select(NativeMeasurements::area2d);
			const auto intensityMeanMsr = analysis->select(NativeMeasurements::intensityMean);
			const auto inverseCircularityMsr = analysis->select(NativeMeasurements::inverseCircularity2d);
			Feret2d::Ptr feret2d = MeasurementAttributes::feret2d();
			feret2d->setOrientationCount(feretCnt);
			feret2d->resample();

			const auto feretMsr = analysis->select(NativeMeasurements::feretMax2d);
			const auto barycenterXMsr = analysis->select(NativeMeasurements::barycenterX);
			const auto barycenterYMsr = analysis->select(NativeMeasurements::barycenterY);

			LabelAnalysis labelAnalysisAlgo;
			labelAnalysisAlgo.setInputIntensityImage(refImage);
			labelAnalysisAlgo.setOutputAnalysis(analysis);
			labelAnalysisAlgo.setInputLabelImage(reorderLabelImage);
			labelAnalysisAlgo.execute();

			for (auto labelIndex = 0; labelIndex < labels.size(); labelIndex++) {
				const auto dset = DataSet::New();

				const auto realIndex = labels[labelIndex];
				QLOG_INFO() << "Label index: " << realIndex;

				auto realScalar = ScalarData::New(realIndex);
				dset->AppendData(realScalar, "LabelIndex");

				// mean RI
				auto meanRi = intensityMeanMsr->value(labelIndex);
				if (meanRi < 0 || qIsInf(meanRi)) {
					meanRi = -1;
				} else {
					meanRi /= d->intensityDiv;
				}

				const auto meanRiScalar = ScalarData::New(meanRi);
				meanRiScalar->setLabelIdx(realIndex);
				dset->AppendData(meanRiScalar, d->measureKeyMap[d->imageType][MeasureName::MEAN_RI]);

				// surface area
				auto surfaceArea = areaMsr->value(labelIndex);
				if (surfaceArea < 0 || qIsInf(surfaceArea)) {
					surfaceArea = -1;
				}

				const auto surfaceAreaScalar = ScalarData::New(surfaceArea);
				surfaceAreaScalar->setLabelIdx(realIndex);
				dset->AppendData(surfaceAreaScalar, d->measureKeyMap[d->imageType][MeasureName::SURFACE_AREA]);

				// circularity
				const auto inverseCircularity = inverseCircularityMsr->value(labelIndex);
				double circularity = 0.;
				if (inverseCircularity < 0 || qIsInf(inverseCircularity)) {
					circularity = -1;
				} else {
					circularity = 1. / inverseCircularity;
				}

				const auto circularityScalar = ScalarData::New(circularity);
				circularityScalar->setLabelIdx(realIndex);
				dset->AppendData(circularityScalar, d->measureKeyMap[d->imageType][MeasureName::CIRCULARITY]);

				// feret diameter
				const auto feret_value = feretMsr->value(labelIndex);
				const auto feretScalar = ScalarData::New(feret_value);
				dset->AppendData(feretScalar, d->measureKeyMap[d->imageType][MeasureName::FERET]);

				// centroid-x
				const auto centroidX = barycenterXMsr->value(labelIndex);
				const auto centroidXScalar = ScalarData::New(centroidX);
				centroidXScalar->setLabelIdx(realIndex);
				dset->AppendData(centroidXScalar, d->measureKeyMap[d->imageType][MeasureName::CENTROID_X]);

				// centroid-y
				const auto centroidY = barycenterYMsr->value(labelIndex);
				const auto centroidYScalar = ScalarData::New(centroidY);
				centroidYScalar->setLabelIdx(realIndex);
				dset->AppendData(centroidYScalar, d->measureKeyMap[d->imageType][MeasureName::CENTROID_Y]);

				d->result->Append(dset);
			}

			QLOG_INFO() << "elapsed time for measurement: ";
			QLOG_INFO() << etimer.elapsed();

			std::cout << "FINISH Measurement" << std::endl;
			QLOG_INFO() << "Measurement finished";

			succeed = true;

		} catch (std::exception& e) {
			QLOG_INFO() << e.what();
		}
		catch (Exception& e) {
			QLOG_INFO() << e.what().c_str();
		}
		catch (...) {
			QLOG_INFO() << "UNKNOWN ERROR";
		}

		if (!succeed) {
			d->result = d->CreateDummyResult();
		}

		return succeed;
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		Q_UNUSED(key)
		return d->param;
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
		Q_UNUSED(keys)
	}
}
