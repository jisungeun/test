#include <ParameterRegistry.h>
#include "LabelAnalysis2DParameter.h"

namespace TC::Algorithm::Measurement::LabelAnalysis2D {
	auto Parameter::Register() -> void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");
		RegisterNode("IntensityDiv", "Divider", "Set intensity modifier", "ScalarValue.double", 10000, 0, 10000);
		RegisterNode("ImageType", "Input image type", "Set input image modality", "String", "HT", {}, {});
		RegisterNode("FeretCount", "Feret angle count", "Set sampling number of angles in calculation of feret diameter", "ScalarValue.int", 31, 1, 1000);
	}
}
