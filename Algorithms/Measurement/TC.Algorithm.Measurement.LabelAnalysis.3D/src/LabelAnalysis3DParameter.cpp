#include <ParameterRegistry.h>
#include "LabelAnalysis3DParameter.h"

namespace TC::Algorithm::Measurement::LabelAnalysis3D {
	auto Parameter::Register() -> void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");
		RegisterNode("CustomRI", "Custom Medium RI", "Use File RI", "TFCheck", false, "", "");
		RegisterNode("RI", "Medium RI", "Set Medium RI", "ScalarValue.double", 1.337, 1.0, 3.0);
		RegisterNode("RII", "RII", "Set RII", "ScalarValue.double", 0.19, 0.0, 1.0);
		RegisterNode("IntensityDiv", "Divider", "Set intensity modifier", "ScalarValue.double", 10000, 0, 10000);
		RegisterNode("ImageType", "Input image type", "Set input image modality", "String", "HT", {}, {});
		RegisterNode("UseFeret", "Use Feret Measure", "Use Feret Measure", "TFCheck", false, "", "");
		RegisterNode("FeretCount", "Feret angle count", "Set sampling number of angles in calculation of feret diameter", "ScalarValue.int", 31, 1, 1000);
		RegisterNode("LengthType", "length measure type", "set length measure type", "String", "None", {}, {});
		RegisterNode("BranchSensibility", "Centerline parameter", "Set branching out sensibility", "ScalarValue.double", 3 , 1, 10);
	}

}
