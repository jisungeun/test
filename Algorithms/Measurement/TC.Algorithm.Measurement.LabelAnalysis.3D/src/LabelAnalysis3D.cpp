#define LOGGER_TAG "[LabelAnalysis3D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Data/MeasurementAttributes.h>
#include <iolink/view/ImageViewFactory.h>
#include <Inventor/SbVec.h>

#include <ParameterRegistry.h>
#include <ScalarData.h>
#include <DataList.h>
#include <TCImage.h>
#include <TCMask.h>
#include <TCFMetaReader.h>
#include <TCDataConverter.h>

#include "LabelAnalysis3DUiParameter.h"
#include "LabelAnalysis3DParameter.h"
#include "LabelAnalysis3D.h"

using namespace TC::Framework;
using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Measurement::LabelAnalysis3D {
	struct Algorithm::Impl {
		TCImage::Pointer refImage;
		TCMask::Pointer inst_input;
		DataList::Pointer result { nullptr };

		IParameter::Pointer param { ParameterRegistry::Create("org.tomocube.algorithm.measurement.labelanalysis.3d") };
		IUiParameter::Pointer uiParam { UiParameterRegistry::Create("org.tomocube.algorithm.measurement.labelanalysis.3d") };

		//measurement properties				
		double RefractiveIndex { 1.337 };
		double RII { 0.19 };
		double intensityDiv { 10000 };

		QList<QList<int>> exist_label_idx;

		QString imageType { "HT" };
		QMap<QString, QMap<MeasureName, QString>> measureKeyMap;

		auto ExistLabelIndexes(std::shared_ptr<ImageView> image) -> QList<int>;

		auto CalcSphericity(double volume, double surfaceArea) -> double;
		auto CalcProjectedArea(std::shared_ptr<ImageView> inputImage) -> double;

		double skelSum { 0 };
		int skelDim[3] { 1, 1, 1 };
		double skelRes[3] { 1, 1, 1 };
		double skelAvgLength{ 0 };
		double branchSensibility{ 3 };
		std::shared_ptr<uint8_t[]> skelRefArr{ nullptr };
		auto CalcCenterlineLength(std::shared_ptr<ImageView> inputImage)->double;
		auto CalcSkeletonLength(std::shared_ptr<ImageView> inputImage) -> double;
		auto FindStartSeed(uint8_t* refArr, int dimX, int dimY, int dimZ) -> QList<std::tuple<int, int, int>>;
		auto NeighborDFS(int idxX,int idxY,int idxZ) -> void;
		auto GotoNeighbor(int idxX, int idxY, int idxZ, int prevIdxX,int prevIdxY,int prevIdxZ) -> void;
		auto CalcDistanceBtwPoint(int idxX1,int idxY1,int idxZ1, int idxX2,int idxY2,int idxZ2) -> double;

		auto CreateDummyResult() -> DataList::Pointer;
		auto FillDummyMeasurementData(int index, DataSet::Pointer dataSet) -> void;
		auto AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double;
		auto BuildMeasureKeyMap() -> void;
	};

	auto Algorithm::Impl::BuildMeasureKeyMap() -> void {
		const QMap<MeasureName, QString> htMeasureKeys {
			{ MeasureName::VOLUME, "Volume" },
			{ MeasureName::VOLUME_FILLED, "Volume-Filled" },
			{ MeasureName::SURFACE_AREA, "SurfaceArea" },
			{ MeasureName::PROJ_AREA, "ProjectedArea" },
			{ MeasureName::MEAN_RI, "MeanRI" },
			{ MeasureName::SPHERICITY, "Sphericity" },
			{ MeasureName::FERET, "FeretDiameter" },
			{ MeasureName::LENGTH, "Length" },
			{ MeasureName::DRYMASS, "Drymass" },
			{ MeasureName::CONCENTRATAION, "Concentration" },
			{ MeasureName::CENTROID_X, "CentroidX" },
			{ MeasureName::CENTROID_Y, "CentroidY" },
			{ MeasureName::CENTROID_Z, "CentroidZ" }
		};
		measureKeyMap["HT"] = htMeasureKeys;

		const QMap<MeasureName, QString> flMeasureKeys {
			{ MeasureName::VOLUME, "Volume" },
			{ MeasureName::VOLUME_FILLED, "Volume-Filled" },
			{ MeasureName::SURFACE_AREA, "SurfaceArea" },
			{ MeasureName::PROJ_AREA, "ProjectedArea" },
			{ MeasureName::MEAN_RI, "MeanIntensity" },
			{ MeasureName::SPHERICITY, "Sphericity" },
			{ MeasureName::FERET, "FeretDiameter" },
			{ MeasureName::LENGTH, "Length" },
			{ MeasureName::DRYMASS, "SumIntensity" },
			{ MeasureName::CENTROID_X, "CentroidX" },
			{ MeasureName::CENTROID_Y, "CentroidY" },
			{ MeasureName::CENTROID_Z, "CentroidZ" }
		};
		measureKeyMap["FL"] = flMeasureKeys;
	}


	auto Algorithm::Impl::AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double {
		return offset - static_cast<double>(refDimZ) * refResZ / 2.0;
	}


	auto Algorithm::Impl::ExistLabelIndexes(std::shared_ptr<ImageView> image) -> QList<int> {
		const auto buffer = static_cast<unsigned short*>(image->buffer());
		const auto dim = image->shape();

		QList<int> existLabels;
		for (uint64_t i = 0; i < dim[0] * dim[1] * dim[2]; i++) {
			auto val = static_cast<int>(*(buffer + i));
			if (val != 0 && !existLabels.contains(val))
				existLabels << val;
		}

		std::sort(existLabels.begin(), existLabels.end());

		return existLabels;
	}

	auto Algorithm::Impl::CalcSphericity(double volume, double surfaceArea) -> double {
		if (volume < 0 || surfaceArea < 0)
			return -1;

		auto phi = pow(3.14, 1.0 / 3.0);
		auto factor = 36.0 * volume * volume;
		auto sa = pow(factor, 1.0 / 3.0);

		return (phi * sa / surfaceArea);
	}

	auto Algorithm::Impl::CalcProjectedArea(std::shared_ptr<ImageView> inputImage) -> double {
		const auto converted = convertImage(inputImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
		const auto projected = volumeProjection3d(converted, VolumeProjection3d::Z_AXIS, VolumeProjection3d::INTENSITY_MAXIMA, VolumeProjection3d::SPACE_2D, false);
		const auto convrtedBinary = convertImage(projected.outputImage, ConvertImage::BINARY);
		const auto integral = intensityIntegral2d(convrtedBinary);

		return integral->intensityIntegral();
	}

	auto Algorithm::Impl::FindStartSeed(uint8_t* refArr, int dimX, int dimY, int dimZ) -> QList<std::tuple<int, int, int>> {
		QList < std::tuple<int, int, int>> pts;
		for (auto k = 0; k < dimZ; k++) {
			for (auto i = 0; i < dimX; i++) {
				for (auto j = 0; j < dimY; j++) {
					const auto idx = k * dimX * dimY + i * dimY + j;
					const auto val = *(refArr + idx);
					if (val > 0) {
						//std::cout << "ST PT: " << i << " " << j << " " << k << std::endl;
						pts.append(std::make_tuple(i, j, k));
					}
				}
			}
		}
		return pts;
	}

	auto Algorithm::Impl::GotoNeighbor(int idxX, int idxY, int idxZ, int prevIdxX, int prevIdxY, int prevIdxZ) -> void {
		const auto arrIdx = idxZ * skelDim[0] * skelDim[1] + idxX * skelDim[1] + idxY;
		const auto val = *(skelRefArr.get() + arrIdx);
		if (val > 0) {			
			const auto dist = CalcDistanceBtwPoint(idxX,idxY,idxZ, prevIdxX,prevIdxY,prevIdxZ);
			std::cout << "Add " << dist << std::endl;
			skelSum += dist;			
			NeighborDFS(idxX,idxY,idxZ);
		}				
	}		

	auto Algorithm::Impl::CalcDistanceBtwPoint(int idxX1, int idxY1, int idxZ1, int idxX2, int idxY2, int idxZ2) -> double {
		int idx1[3]{ idxX1,idxY1,idxZ1 };
		int idx2[3]{ idxX2,idxY2,idxZ2 };
		double p1[3];
		double p2[3];
		for (auto i = 0; i < 3; i++) {
			p1[i] = idx1[i] * skelRes[i];
			p2[i] = idx2[i] * skelRes[i];
		}
		return sqrt(pow(p1[0] - p2[0], 2) + pow(p1[1] - p2[1], 2) + pow(p1[2] - p2[2], 2));
	}

	auto Algorithm::Impl::NeighborDFS(int idxX, int idxY, int idxZ) -> void {
		//Fill Visit element as 0
		const auto curIdx = idxZ * skelDim[0] * skelDim[1] + idxX * skelDim[1] + idxY;		
		*(skelRefArr.get() + curIdx) = 0;
		//Check 26-neibhgor		
		if (idxX + 1 < skelDim[0]) {
			//+X
			GotoNeighbor(idxX + 1, idxY, idxZ, idxX,idxY,idxZ);
			if (idxY + 1 < skelDim[1]) {
				//+X+Y
				GotoNeighbor(idxX + 1, idxY + 1, idxZ, idxX, idxY, idxZ);
				if (idxZ + 1 < skelDim[2]) {
					//+X+Y+Z
					GotoNeighbor(idxX + 1, idxY + 1, idxZ + 1, idxX, idxY, idxZ);
				}
				if (idxZ - 1 > -1) {
					//+X+Y-Z
					GotoNeighbor(idxX + 1, idxY + 1, idxZ - 1, idxX, idxY, idxZ);
				}
			}
			if (idxY - 1 > -1) {
				//+X-Y
				GotoNeighbor(idxX + 1, idxY - 1, idxZ, idxX, idxY, idxZ);
				if (idxZ + 1 < skelDim[2]) {
					//+X-Y+Z
					GotoNeighbor(idxX + 1, idxY - 1, idxZ + 1, idxX, idxY, idxZ);
				}
				if (idxZ - 1 > -1) {
					//+X-Y-Z
					GotoNeighbor(idxX + 1, idxY - 1, idxZ - 1, idxX, idxY, idxZ);
				}
			}
			if (idxZ + 1 < skelDim[2]) {
				//+X+Z
				GotoNeighbor(idxX + 1, idxY, idxZ + 1, idxX, idxY, idxZ);
			}
			if (idxZ - 1 > -1) {
				//+X-Z
				GotoNeighbor(idxX + 1, idxY, idxZ - 1, idxX, idxY, idxZ);
			}
		}
		if (idxX - 1 > -1) {
			//-X
			GotoNeighbor(idxX - 1, idxY, idxZ, idxX, idxY, idxZ);
			if (idxY + 1 < skelDim[1]) {
				//-X+Y
				GotoNeighbor(idxX - 1, idxY + 1, idxZ, idxX, idxY, idxZ);
				if (idxZ + 1 < skelDim[2]) {
					//-X+Y+Z
					GotoNeighbor(idxX - 1, idxY + 1, idxZ + 1, idxX, idxY, idxZ);
				}
				if (idxZ - 1 > -1) {
					//-X+Y-Z
					GotoNeighbor(idxX - 1, idxY + 1, idxZ - 1, idxX, idxY, idxZ);
				}
			}
			if (idxY - 1 > -1) {
				//-X-Y
				GotoNeighbor(idxX - 1, idxY - 1, idxZ, idxX, idxY, idxZ);
				if (idxZ + 1 < skelDim[2]) {
					//-X-Y+Z
					GotoNeighbor(idxX - 1, idxY - 1, idxZ + 1, idxX, idxY, idxZ);
				}
				if (idxZ - 1 > -1) {
					//-X-Y-Z
					GotoNeighbor(idxX - 1, idxY - 1, idxZ - 1, idxX, idxY, idxZ);
				}
			}
			if (idxZ + 1 < skelDim[2]) {
				//-X+Z
				GotoNeighbor(idxX - 1, idxY, idxZ + 1, idxX, idxY, idxZ);
			}
			if (idxZ - 1 > -1) {
				//-X-Z
				GotoNeighbor(idxX - 1, idxY, idxZ - 1, idxX, idxY, idxZ);
			}
		}
		if (idxY + 1 < skelDim[1]) {
			//+Y
			GotoNeighbor(idxX, idxY + 1, idxZ, idxX, idxY, idxZ);
			if (idxZ + 1 < skelDim[2]) {
				//+Y+Z
				GotoNeighbor(idxX, idxY + 1, idxZ + 1, idxX, idxY, idxZ);
			}
			if (idxZ - 1 > -1) {
				//+Y-Z
				GotoNeighbor(idxX, idxY + 1, idxZ - 1, idxX, idxY, idxZ);
			}
		}
		if (idxY - 1 > -1) {
			//-Y
			GotoNeighbor(idxX, idxY - 1, idxZ, idxX, idxY, idxZ);
			if (idxZ + 1 < skelDim[2]) {
				//-Y+Z
				GotoNeighbor(idxX, idxY - 1, idxZ + 1, idxX, idxY, idxZ);
			}
			if (idxZ - 1 > -1) {
				//-Y-Z
				GotoNeighbor(idxX, idxY - 1, idxZ - 1, idxX, idxY, idxZ);
			}
		}
		//+Z
		if (idxZ + 1 < skelDim[2]) {
			GotoNeighbor(idxX, idxY, idxZ + 1, idxX, idxY, idxZ);
		}
		//-Z
		if (idxZ - 1 > -1) {
			GotoNeighbor(idxX, idxY, idxZ - 1, idxX, idxY, idxZ);
		}
	}

	auto Algorithm::Impl::CalcCenterlineLength(std::shared_ptr<ImageView> inputImage) -> double {
		const auto bin = convertImage(inputImage, ConvertImage::BINARY);
		const auto shape = bin->shape();
		const auto spacing = bin->properties()->calibration().spacing();
		setCalibrationOrigin(bin, { 0, 0, 0 });

		const auto centered = centerline3d(bin, true, 3, branchSensibility, 0.3, 0);

		const auto skel = centered.outputObjectImage;
		setCalibrationSpacing(skel, { 1,1,1 });
		const auto stat = intensityIntegral3d(skel);

		const auto length = stat->intensityIntegral() * skelAvgLength;

		return length;
	}

	auto Algorithm::Impl::CalcSkeletonLength(std::shared_ptr<ImageView> inputImage) -> double {
		const auto bin = convertImage(inputImage, ConvertImage::BINARY);
		const auto shape = bin->shape();
		const auto spacing = bin->properties()->calibration().spacing();
		setCalibrationOrigin(bin, { 0, 0, 0 });

		const auto skel = skeleton(bin);
				
		setCalibrationSpacing(skel, { 1,1,1 });
		const auto stat = intensityIntegral3d(skel);
		
		const auto length = stat->intensityIntegral() * skelAvgLength;

		return length;

		/*const auto endP = endPoints3d(skel, EndPoints3d::ZERO, EndPoints3d::CONNECTIVITY_26);				

		//find start point
		const auto startPoints = FindStartSeed(static_cast<uint8_t*>(endP->buffer()), shape[0], shape[1], shape[2]);
		//const auto startPoints = FindStartSeed(static_cast<uint8_t*>(skel->buffer()), shape[0], shape[1], shape[2]);
		if (startPoints.count() < 0) {
			return -1;
		}

		std::cout << "#of point in skel: " << startPoints.count() << std::endl;
				
		//DFS
		skelRefArr = std::shared_ptr<uint8_t[]>(new uint8_t[shape[0] * shape[1] * shape[2]],std::default_delete<uint8_t[]>());		
		memcpy(skelRefArr.get(), skel->buffer(), shape[0] * shape[1] * shape[2]);
		

		for(auto i=0;i<3;i++) {
			skelDim[i] = shape[i];
			skelRes[i] = spacing[i];
		}

		std::cout << "shape: " << shape[0] << " " << shape[1] << " " << shape[2] << std::endl;

		double finalResult = 0;
		for (auto st : startPoints) {
			const auto stX = std::get<0>(st);
			const auto stY = std::get<1>(st);
			const auto stZ = std::get<2>(st);
			skelSum = 0;
			std::cout << "----" << std::endl;
			NeighborDFS(stX,stY,stZ);
			std::cout << "----" << std::endl;

			finalResult += skelSum;
		}
		return finalResult;*/

		//NeighborDFS(refArr.get(), shape[1], shape[2], shape[0]);

		/*const auto skeled = centerline3d(bin, false, 1, 4.0, 2, 2);
		bool isAdding = false;
		double sum = 0;
		double branch_sum = 0;
		double prev_x = 0;
		double prev_y = 0;
		double prev_z = 0;
		for (auto i = 0; i < skeled.outputIndices->shape()[0]; i++) {
			const auto idx = skeled.outputIndices->at({ static_cast<unsigned long long>(i) });			
			if (false == isAdding) {
				branch_sum = 0;
				isAdding = true;				
				prev_x = skeled.outputVertices->at({ 0, static_cast<unsigned long long>(idx) });
				prev_y = skeled.outputVertices->at({ 1, static_cast<unsigned long long>(idx) });
				prev_z = skeled.outputVertices->at({ 2, static_cast<unsigned long long>(idx) });
			} else {
				const auto cur_x = skeled.outputVertices->at({ 0, static_cast<unsigned long long>(idx) });
				const auto cur_y = skeled.outputVertices->at({ 1, static_cast<unsigned long long>(idx) });
				const auto cur_z = skeled.outputVertices->at({ 2, static_cast<unsigned long long>(idx) });

				const auto dist = std::sqrt(pow(cur_x - prev_x, 2) + pow(cur_y - prev_y, 2) + pow(cur_z - prev_z, 2));
				branch_sum += dist;
				prev_x = cur_x;
				prev_y = cur_y;
				prev_z = cur_z;
			}
			if (idx == -1) {
				sum += branch_sum;
				isAdding = false;
			}
		}

		return sum;*/		
	}

	auto Algorithm::Impl::CreateDummyResult() -> DataList::Pointer {
		auto dummyResult = DataList::New();

		DataSet::Pointer dset = DataSet::New();

		auto numLabels = 1;
		auto realIndex = 1;
		auto labelNum = ScalarData::New(numLabels);
		dset->AppendData(labelNum, "LabelCount");

		auto realScalar = ScalarData::New(realIndex);
		dset->AppendData(realScalar, "real_idx" + QString::number(0));

		FillDummyMeasurementData(0, dset);

		dummyResult->Append(dset);

		return dummyResult;
	}

	auto Algorithm::Impl::FillDummyMeasurementData(int index, DataSet::Pointer dataSet) -> void {
		for (const auto& key : measureKeyMap[imageType]) {
			const auto scalar = ScalarData::New(-1);
			scalar->setLabelIdx(index);
			dataSet->AppendData(scalar, key);
		}
	}

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
		d->BuildMeasureKeyMap();
	}

	Algorithm::~Algorithm() { }

	auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 1)
			return false;

		if (index == 0) {
			d->refImage = std::dynamic_pointer_cast<TCImage>(data);
		} else if (index == 1) {
			d->inst_input = std::dynamic_pointer_cast<TCMask>(data);
		}

		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->result;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		if (key == "RI") {
			return std::make_tuple("RI", param->GetValue("RI"));
		}
		if (key == "RII") {
			return std::make_tuple("RII", param->GetValue("RII"));
		}
		return std::make_tuple(QString(), QJsonValue());
	}

	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START LabelAnalysis3D";

		d->result = DataList::New();

		QElapsedTimer etimer;
		etimer.start();

		d->exist_label_idx.clear();

		bool succeed = false;
		try {
			d->intensityDiv = d->param->GetValue("IntensityDiv").toDouble();
			d->imageType = d->param->GetValue("ImageType").toString();
			if (!d->inst_input->IsValid())
				throw std::runtime_error("Input mask invalid, create dummy(-1) result");
			
			// set RI value
			auto mediumRI = 1.337;

			const auto [sizeX, sizeY, sizeZ] = d->refImage->GetSize();
			if (sizeZ < 2)
				throw std::runtime_error("Input mask invalid, create dummy(-1) result");

			double res[3];
			d->refImage->GetResolution(res);
			const auto resX = res[0];
			const auto resY = res[1];
			const auto resZ = res[2];

			//approximate length
			d->skelAvgLength = 0.0;
			//X direction +X, -X ; 2
			d->skelAvgLength += res[0] * 2;
			//Y direction +Y, -Y ; 2 
			d->skelAvgLength += res[1] * 2;
			//Z direction +Z, -Z ; 2 
			d->skelAvgLength += res[2] * 2;
			//XY direction +X+Y, +X-Y, -X+Y, -X+Y ; 4
			d->skelAvgLength += sqrt(pow(res[0], 2) + pow(res[1], 2)) * 4;
			//XZ direction +X+Z, +X-Z, -X+Z, -X-Z ; 4
			d->skelAvgLength += sqrt(pow(res[0], 2) + pow(res[2], 2)) * 4;
			//YZ direction +Y+Z, +Y-Z, -Y+Z, -Y-Z ; 4 
			d->skelAvgLength += sqrt(pow(res[1], 2) + pow(res[2], 2)) * 4;
			//XYZ direction +X+Y+Z, +X+Y-Z, +X-Y+Z, +X-Y-Z, -X+Y+Z, -X+Y-Z, -X-Y+Z, -X-Y-Z; 8
			d->skelAvgLength += sqrt(pow(res[0], 2) + pow(res[1], 2) + pow(res[2], 2)) * 8;

			d->skelAvgLength /= 26;

			const auto customRIName = QString("CustomRI");
			const auto isCustomRI = d->param->ExistNode(customRIName) ? d->param->GetValue(customRIName).toBool() : true;
			if (!isCustomRI) {
				const auto refImageFilePath = d->refImage->GetFilePath();
				if (!refImageFilePath.isEmpty()) {
					const auto metaReader = std::make_unique<TC::IO::TCFMetaReader>();
					const auto readMediumRI = metaReader->ReadMediumRI(refImageFilePath);

					if (readMediumRI != 0.0) {
						mediumRI = readMediumRI;
					}
				}
			}

			QLOG_INFO() << "Set Parameter";
			d->RII = d->param->GetValue("RII").toDouble();
			d->RefractiveIndex = isCustomRI ? d->param->GetValue("RI").toDouble() : mediumRI;
			const auto useFeret = d->param->ExistNode("UseFeret") ? d->param->GetValue("UseFeret").toBool() : false;
			const auto feretCnt = d->param->GetValue("FeretCount").toInt();
			const auto lengthType = d->param->GetValue("LengthType").toString();
			d->branchSensibility = d->param->GetValue("BranchSensibility").toDouble();

			int lengthTypeIdx = -1;
			if(lengthType == "None") {
				lengthTypeIdx = 0;
			}else if(lengthType == "Skeleton") {
				lengthTypeIdx = 1;
			}else if(lengthType == "Centerline") {
				lengthTypeIdx = 2;
			}

			// analyze label images
			TCDataConverter converter;

			const auto refImage = converter.ImageToImageView(d->refImage);
			auto labeledImage = converter.MaskToImageView(d->inst_input);

			const auto imageOrigin = refImage->properties()->calibration().origin();
			const auto maskOrigin = labeledImage->properties()->calibration().origin();
			const auto imageSpacing = refImage->properties()->calibration().spacing();
			const auto maskSpacing = labeledImage->properties()->calibration().spacing();

			auto originDiff = false;
			auto spacingDiff = false;

			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			for (auto i = 0; i < 3; i++) {
				if (false == AreSame(imageOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(imageSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}

			double refOffset = d->refImage->GetOffset();
			double targetOffset = d->inst_input->GetOffset();

			if (AreSame(refOffset, 0) && false == AreSame(targetOffset, 0)) {
				targetOffset = d->AdjustOffsetZ(targetOffset, refImage->shape()[2], imageSpacing[2]);
			} else if (false == AreSame(refOffset, 0) && AreSame(targetOffset, 0)) {
				refOffset = d->AdjustOffsetZ(refOffset, labeledImage->shape()[2], maskSpacing[2]);
			}

			if (refImage->shape() != labeledImage->shape() || originDiff || spacingDiff) {
				labeledImage = TCDataConverter::MapMaskGeometry(refImage, labeledImage, refOffset, targetOffset);
			}

			ReorderLabels reorderLabelsAlgo;
			reorderLabelsAlgo.setInputLabelImage(labeledImage);
			reorderLabelsAlgo.execute();

			const auto reorderLabelImage = reorderLabelsAlgo.outputLabelImage();
			const auto labels = d->ExistLabelIndexes(reorderLabelImage);

			const auto analysis = std::make_shared<AnalysisMsr>();
			const auto volumeMsr = analysis->select(NativeMeasurements::volume3d);
			const auto intensityMeanMsr = analysis->select(NativeMeasurements::intensityMean);
			const auto intensitySumMsr = analysis->select(NativeMeasurements::intensityIntegral);
			Feret3d::Ptr feret3d = MeasurementAttributes::feret3d();
			feret3d->setOrientationCount(feretCnt);
			feret3d->resample();
			std::shared_ptr<AnalysisMsrType<FeretMax3dInfo>::type> feretMsr{ nullptr };
			if (useFeret) {
				feretMsr = analysis->select(NativeMeasurements::feretMax3d);
			}
			//const auto feretMsr = analysis->select(NativeMeasurements::feretMax3d);
			const auto barycenterXMsr = analysis->select(NativeMeasurements::barycenterX);
			const auto barycenterYMsr = analysis->select(NativeMeasurements::barycenterY);
			const auto barycenterZMsr = analysis->select(NativeMeasurements::barycenterZ);
			const auto bboxOXMsr = analysis->select(NativeMeasurements::boundingBoxOX);
			const auto bboxOYMsr = analysis->select(NativeMeasurements::boundingBoxOY);
			const auto bboxOZMsr = analysis->select(NativeMeasurements::boundingBoxOZ);
			const auto bboxDXMsr = analysis->select(NativeMeasurements::boundingBoxDX);
			const auto bboxDYMsr = analysis->select(NativeMeasurements::boundingBoxDY);
			const auto bboxDZMsr = analysis->select(NativeMeasurements::boundingBoxDZ);

			imagedev::LabelAnalysis labelAnalysisAlgo;
			labelAnalysisAlgo.setInputIntensityImage(refImage);
			labelAnalysisAlgo.setOutputAnalysis(analysis);
			labelAnalysisAlgo.setInputLabelImage(reorderLabelImage);
			labelAnalysisAlgo.execute();

			// get measurement result
			for (auto labelIndex = 0; labelIndex < labels.size(); labelIndex++) {
				const auto dset = DataSet::New();

				const auto realIndex = labels[labelIndex];
				QLOG_INFO() << "Label index: " << realIndex;

				const auto realScalar = ScalarData::New(realIndex);
				dset->AppendData(realScalar, "LabelIndex");

				double volume = -1;
				if (volumeMsr->size() > labelIndex) {
					volume = volumeMsr->value(labelIndex);
				}

				if (volume > 0 && !qIsInf(volume)) {
					const auto ox = static_cast<int>(round((bboxOXMsr->value(labelIndex) + resX * sizeX / 2) / resX));
					const auto oy = static_cast<int>(round((bboxOYMsr->value(labelIndex) + resY * sizeY / 2) / resY));
					const auto oz = static_cast<int>(round((bboxOZMsr->value(labelIndex) + resZ * sizeZ / 2) / resZ));
					auto dx = static_cast<int>(bboxDXMsr->value(labelIndex) / resX) + 1;
					auto dy = static_cast<int>(bboxDYMsr->value(labelIndex) / resY) + 1;
					auto dz = static_cast<int>(bboxDZMsr->value(labelIndex) / resZ) + 1;
					if (dx < 1)
						dx = 1;
					if (dy < 1)
						dy = 1;
					if (dz < 1)
						dz = 1;

					auto croppedIntensityImage = cropImage3d(refImage, { ox, oy, oz }, { dx, dy, dz });
					croppedIntensityImage = imageFormula(croppedIntensityImage, nullptr, nullptr,
														QString("if(I1 < %1, %1, I1)").arg(round(mediumRI * d->intensityDiv)).toStdString(),
														ImageFormula::OutputType::UNSIGNED_INTEGER_16_BIT
														);

					auto croppedLabelImage = cropImage3d(reorderLabelImage, { ox, oy, oz }, { dx, dy, dz });
					auto croppedBinaryImage = thresholdingByCriterion(croppedLabelImage, ThresholdingByCriterion::EQUAL_TO, realIndex);
					if (croppedBinaryImage->dimensionCount() == 2) {
						croppedBinaryImage = fillHoles2d(croppedBinaryImage, FillHoles2d::CONNECTIVITY_4);
					} else {
						croppedBinaryImage = fillHoles3d(croppedBinaryImage, FillHoles3d::CONNECTIVITY_6);
					}
					croppedLabelImage = convertImage(croppedBinaryImage, ConvertImage::LABEL_16_BIT);

					auto surfaceArea = -1.0;
					auto currentLabelVolume = -1.0;
					auto croppedImageMeanRi = -1.0;
					const auto croppedLabelAnalysis = std::make_shared<AnalysisMsr>();
					if (dx == 1 || dy == 1 || dz == 1) {
						const auto area2d = croppedLabelAnalysis->select(NativeMeasurements::area2d);
						const auto voxelCount = croppedLabelAnalysis->select(NativeMeasurements::pixelCount);
						const auto inMean = croppedLabelAnalysis->select(NativeMeasurements::intensityMean);
						labelAnalysis(croppedLabelImage, croppedIntensityImage, croppedLabelAnalysis);
						surfaceArea = area2d->value();
						currentLabelVolume = voxelCount->value() * resX * resY * resZ;
						croppedImageMeanRi = inMean->value();
					} else {
						const auto croppedVoxelFaceAreaMsr = croppedLabelAnalysis->select(NativeMeasurements::area3d);
						const auto croppedVolumeMsr = croppedLabelAnalysis->select(NativeMeasurements::volume3d);
						const auto inMean = croppedLabelAnalysis->select(NativeMeasurements::intensityMean);
						labelAnalysis(croppedLabelImage, croppedIntensityImage, croppedLabelAnalysis);
						surfaceArea = croppedVoxelFaceAreaMsr->value();
						currentLabelVolume = croppedVolumeMsr->value();
						croppedImageMeanRi = inMean->value();
					}

					// volume
					auto volumeScalar = ScalarData::New(volume);
					volumeScalar->setLabelIdx(realIndex);
					dset->AppendData(volumeScalar, d->measureKeyMap[d->imageType][MeasureName::VOLUME]);

					// surface area					
					if (surfaceArea < 0 || qIsInf(surfaceArea)) {
						surfaceArea = -1;
					}

					const auto surfaceAreaScalar = ScalarData::New(surfaceArea);
					surfaceAreaScalar->setLabelIdx(realIndex);
					dset->AppendData(surfaceAreaScalar, d->measureKeyMap[d->imageType][MeasureName::SURFACE_AREA]);

					// mean RI
					auto meanRi = intensityMeanMsr->value(labelIndex);
					if (meanRi < 0 || qIsInf(meanRi)) {
						meanRi = -1;
					} else {
						meanRi /= d->intensityDiv;
					}

					const auto meanRiScalar = ScalarData::New(meanRi);
					meanRiScalar->setLabelIdx(realIndex);
					dset->AppendData(meanRiScalar, d->measureKeyMap[d->imageType][MeasureName::MEAN_RI]);

					// filled volume					
					auto volumeFilledScalar = ScalarData::New(currentLabelVolume);
					volumeFilledScalar->setLabelIdx(realIndex);
					dset->AppendData(volumeFilledScalar, d->measureKeyMap[d->imageType][MeasureName::VOLUME_FILLED]);

					// sphericity
					auto sphericity = d->CalcSphericity(currentLabelVolume, surfaceArea);
					if (sphericity < 0 || qIsInf(sphericity)) {
						sphericity = -1;
					}
					if (sphericity > 1) {
						sphericity = 1;
					}
					const auto sphericityScalar = ScalarData::New(sphericity);
					sphericityScalar->setLabelIdx(realIndex);
					dset->AppendData(sphericityScalar, d->measureKeyMap[d->imageType][MeasureName::SPHERICITY]);

					// feret diameter
					if (useFeret) {
						const auto feret_max = feretMsr->value(labelIndex);
						auto feretScalar = ScalarData::New(feret_max);
						dset->AppendData(feretScalar, d->measureKeyMap[d->imageType][MeasureName::FERET]);
					}

					// projected area
					auto projectedArea = d->CalcProjectedArea(croppedLabelImage);
					if (projectedArea < 0 || qIsInf(projectedArea)) {
						projectedArea = -1;
					}

					const auto projectedAreaScalar = ScalarData::New(projectedArea);
					projectedAreaScalar->setLabelIdx(realIndex);
					dset->AppendData(projectedAreaScalar, d->measureKeyMap[d->imageType][MeasureName::PROJ_AREA]);

					//skeleton length
					if (lengthTypeIdx == 1) {
						const auto skeletonLength = d->CalcSkeletonLength(croppedLabelImage);
						//const auto skeletonLength = 1;
						const auto skeletonLengthScalar = ScalarData::New(skeletonLength);
						dset->AppendData(skeletonLengthScalar, d->measureKeyMap[d->imageType][MeasureName::LENGTH]);
					}else if(lengthTypeIdx == 2) {
						const auto centerlineLength = d->CalcCenterlineLength(croppedLabelImage);
						const auto centerlineLengthScalar = ScalarData::New(centerlineLength);
						dset->AppendData(centerlineLengthScalar, d->measureKeyMap[d->imageType][MeasureName::LENGTH]);
					}

					// dry mass
					auto drymass = -1.0;
					if (d->imageType == "HT") {
						if (croppedImageMeanRi < 0 || qIsInf(croppedImageMeanRi)) {
							croppedImageMeanRi = -1;
						} else {
							croppedImageMeanRi /= d->intensityDiv;
						}

						drymass = (croppedImageMeanRi - d->RefractiveIndex) * volume / d->RII;
					} else {
						drymass = intensitySumMsr->value(labelIndex) / (resX * resY * resZ);
					}

					if (drymass < 0 || qIsInf(drymass)) {
						drymass = -1;
					}

					const auto drymassScalar = ScalarData::New(drymass);
					drymassScalar->setLabelIdx(realIndex);
					dset->AppendData(drymassScalar, d->measureKeyMap[d->imageType][MeasureName::DRYMASS]);

					// concentration
					auto concentration = drymass / volume;
					if (concentration < 0 || qIsInf(concentration)) {
						concentration = -1;
					}

					if (d->imageType == "HT") {
						auto concentrationScalar = ScalarData::New(concentration);
						concentrationScalar->setLabelIdx(realIndex);
						dset->AppendData(concentrationScalar, d->measureKeyMap[d->imageType][MeasureName::CONCENTRATAION]);
					}

					// centroid-x
					const auto centroidX = barycenterXMsr->value(labelIndex);
					const auto centroidXScalar = ScalarData::New(centroidX);
					centroidXScalar->setLabelIdx(realIndex);
					dset->AppendData(centroidXScalar, d->measureKeyMap[d->imageType][MeasureName::CENTROID_X]);

					// centroid-y
					const auto centroidY = barycenterYMsr->value(labelIndex);
					const auto centroidYScalar = ScalarData::New(centroidY);
					centroidYScalar->setLabelIdx(realIndex);
					dset->AppendData(centroidYScalar, d->measureKeyMap[d->imageType][MeasureName::CENTROID_Y]);

					// centroid-z
					const auto centroidZ = barycenterZMsr->value(labelIndex);
					const auto centroidZScalar = ScalarData::New(centroidZ);
					centroidZScalar->setLabelIdx(realIndex);
					dset->AppendData(centroidZScalar, d->measureKeyMap[d->imageType][MeasureName::CENTROID_Z]);
				} else {
					d->FillDummyMeasurementData(realIndex, dset);
				}

				d->result->Append(dset);
			}

			QLOG_INFO() << "elapsed time for measurement: ";
			QLOG_INFO() << etimer.elapsed();

			QLOG_INFO() << "Measurement finished";

			succeed = true;
		} catch (std::exception& e) {
			QLOG_INFO() << e.what();
			std::cout << e.what() << std::endl;
			d->result = d->CreateDummyResult();
		}
		catch (Exception& e) {
			QLOG_INFO() << e.what().c_str();
			std::cout << e.what().c_str() << std::endl;
			d->result = d->CreateDummyResult();
		}
		catch (...) {
			QLOG_INFO() << "UNKNOWN ERROR";
			d->result = d->CreateDummyResult();
		}

		return succeed;
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		Q_UNUSED(key)
		return d->param;
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
		Q_UNUSED(keys)
	}
}
