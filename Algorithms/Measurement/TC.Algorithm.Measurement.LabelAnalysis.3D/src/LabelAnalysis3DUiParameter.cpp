#include <ParameterRegistry.h>
#include "LabelAnalysis3DUiParameter.h"

namespace TC::Algorithm::Measurement::LabelAnalysis3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetType("Table");
        RegisterTableItem("TableItem", QStringList{ "Preset","RII" });

        SetFullName(QString(GetName().c_str()));

        RegisterEnabler("CustomRI", "Custom Medium RI", QStringList{ "RI" }, QStringList{});
        RegisterHider("Hider1", QStringList{ "RI","IntensityDiv","ImageType"});
        RegisterSetter("Preset", "Preset", QStringList{ "RII" }, QStringList{ "Protein","Hemoglobin","Lipid" });
        AppendSetter("Preset", "RII", "Protein", 0.19);
        AppendSetter("Preset", "RII", "Hemoglobin", 0.15);
        AppendSetter("Preset", "RII", "Lipid", 0.135);
        RegisterSorter("Order", QStringList{"CustomRI","RI","Preset","RII"});
    }
}