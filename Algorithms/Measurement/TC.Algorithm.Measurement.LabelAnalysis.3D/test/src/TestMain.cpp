#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <TCFMetaReader.h>
#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataList.h>
#include <ScalarData.h>
#include <TCDataConverter.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;

auto GenerateInputData(std::string_view path) -> std::tuple<TCImage::Pointer, TCMask::Pointer> {
	QStringList plugins{
		QString("%1/segmentation/TC.Algorithm.Segmentation.Threshold.dll").arg(_PLUGIN_DIR),
		QString("%1/segmentation/TC.Algorithm.Segmentation.ManualThreshold.dll").arg(_PLUGIN_DIR),
		QString("%1/labeling/TC.Algorithm.Labeling.ConnectedComponent.3D.dll").arg(_PLUGIN_DIR),
	};

	for (auto& plugin : plugins) {
		REQUIRE(PluginRegistry::LoadPlugin(plugin) > 0);
	}

	TCImage::Pointer image{nullptr};
	TCMask::Pointer label{nullptr};

	try {
		auto GetBinaryMask = [plugins](TCImage::Pointer image) -> TCMask::Pointer {
			const auto autoThresholdModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[0], true));

			const auto autoThresholdParam = autoThresholdModule->Parameter();

			autoThresholdModule->SetInput(0, image);
			autoThresholdModule->Execute();

			const auto autoThresholdResult = std::dynamic_pointer_cast<TC::Framework::DataList>(autoThresholdModule->GetOutput(0));
			const auto ulThreshold = std::dynamic_pointer_cast<DataSet>(autoThresholdResult->GetData(0));

			const auto lower = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("LThreshold"))->ValueAsDouble();
			const auto upper = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("UThreshold"))->ValueAsDouble();

			const auto thresholdingModuleInstance = PluginRegistry::GetPlugin(plugins[1], true);
			const auto thresholdingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(thresholdingModuleInstance);

			const auto thresholdingParam = thresholdingModuleInstance->Parameter();
			thresholdingParam->SetValue("LThreshold", IParameter::ValueType(lower));
			thresholdingParam->SetValue("UThreshold", IParameter::ValueType(upper));

			thresholdingModule->SetInput(0, image);
			thresholdingModule->Execute();

			return std::dynamic_pointer_cast<TCMask>(thresholdingModule->GetOutput(0));
		};

		const auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
		const auto metaInfo = metaReader->Read(path.data());

		H5::H5File file(path.data(), H5F_ACC_RDONLY);

		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		int size[3] = {metaInfo->data.data3D.sizeX, metaInfo->data.data3D.sizeY, metaInfo->data.data3D.sizeZ};
		double res[3] = {metaInfo->data.data3D.resolutionX, metaInfo->data.data3D.resolutionY, metaInfo->data.data3D.resolutionZ};

		std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]](), std::default_delete<uint16_t[]>());
		dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
		dataSet.close();
		group.close();
		file.close();

		// get a center slice image
		Vector3d spacing{ res[0], res[1], res[2] };
		Vector3d origin{ -size[0] * res[0] / 2, -size[1] * res[1] / 2, -size[2] * res[2] / 2 };

		const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty(origin, spacing);

		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

		const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]), static_cast<uint64_t>(size[2]) };
		const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		imagedev::setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
		imagedev::setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

		const RegionXu64 imageRegion{{0, 0, 0}, imageShape};
		imageView->writeRegion(imageRegion, data.get());

		TCDataConverter converter;
		image = converter.ImageViewToImage(imageView);
		image->SetMinMax(metaInfo->data.data3D.riMin * 10000, metaInfo->data.data3D.riMax * 10000);

		const auto binaryMask = GetBinaryMask(image);

		const auto labelingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[2], true));

		const auto param = labelingModule->Parameter();
		param->SetValue("LabelType", 1);
		param->SetValue("Neighborhood", 2);

		labelingModule->SetInput(0, binaryMask);
		labelingModule->Execute();

		label = std::dynamic_pointer_cast<TCMask>(labelingModule->GetOutput(0));

		file.close();
	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	} catch (...) {
		FAIL("Unknown error");
	}

	return {image, label};
}

auto PrintMeasurementResult(const TC::Framework::DataList::Pointer data) {
	QFile csvFile(QFileInfo(_TEST_DATA).absolutePath() + "/result.csv");
	csvFile.open(QIODevice::WriteOnly);

	auto WriteCSVRow = [f = &csvFile](QString row) {
		f->write(row.toStdString().data());
		f->write("\n");
	};

	QStringList measurements{
		"Volume",
        "SurfaceArea",
		"ProjectedArea",
		"MeanRI",
		"Sphericity",
		"Drymass",
		"Concentration"
    };

	QString rowString;
	rowString = ",";	// for label index column
	for (auto &header : measurements) {
		rowString += header + ",";
	}

	WriteCSVRow(rowString);

    const auto dataSetList = data->GetList();
    for (auto dataIndex = 0; dataIndex < dataSetList.size(); dataIndex++) {
        const auto dataSet = dataSetList.at(dataIndex);
        const auto realIndex = std::static_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData("LabelIndex"))->ValueAsInt();
		rowString = QString::number(realIndex) + ",";

        for (auto measureIndex = 0; measureIndex < measurements.size(); measureIndex++) {
            const auto measureName = measurements.at(measureIndex);
            const auto value = std::static_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData(measureName))->ValueAsDouble();

			rowString += QString::number(value) + ",";
        }

		WriteCSVRow(rowString);
    }

	csvFile.close();
}

TEST_CASE("LabelAnalysis") {
	REQUIRE(QFile::exists(_TEST_DATA));

	try {
		imagedev::init();
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	// load plugins
	const QString plugin = QString("%1/measurement/TC.Algorithm.Measurement.LabelAnalysis.3D.dll").arg(_PLUGIN_DIR);
	REQUIRE(PluginRegistry::LoadPlugin(plugin) > 0);

	const auto [image, labeledMask] = GenerateInputData(_TEST_DATA);

	SECTION("LabelAnalysis 3D") {
		const auto moduleInstance = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugin, true));
		moduleInstance->SetInput(0, image);
		moduleInstance->SetInput(1, labeledMask);
		moduleInstance->Execute();

		const auto output = std::dynamic_pointer_cast<TC::Framework::DataList>(moduleInstance->GetOutput(0));
		REQUIRE(output != nullptr);

		PrintMeasurementResult(output);
	}

	imagedev::finish();
}
