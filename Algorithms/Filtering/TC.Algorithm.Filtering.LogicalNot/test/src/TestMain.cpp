#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataList.h>
#include <ScalarData.h>
#include <TCDataConverter.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;

auto LoadHT(std::string_view path) -> TCImage::Pointer {
	TCImage::Pointer image{nullptr};
	try {
		// get HT 3D
		H5::H5File file(path.data(), H5F_ACC_RDONLY);
		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		auto GetResolutionAttrValue = [group](std::string_view name) -> float {
			float res;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_FLOAT, &res);
			sizeAttr.close();

			return res;
		};

		double res[3];
		res[0] = GetResolutionAttrValue("ResolutionX");
		res[1] = GetResolutionAttrValue("ResolutionY");
		res[2] = GetResolutionAttrValue("ResolutionZ");

		auto GetSizeAttrValue = [group](std::string_view name) -> int64_t {
			int64_t size;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_INT64, &size);
			sizeAttr.close();

			return size;
		};

		int size[3];
		size[0] = GetSizeAttrValue("SizeX");
		size[1] = GetSizeAttrValue("SizeY");
		size[2] = GetSizeAttrValue("SizeZ");

		std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]], std::default_delete<uint16_t[]>());
		dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
		dataSet.close();
		group.close();
		file.close();

		// get a center slice image
		Vector3d spacing{ res[0], res[1], res[2] };
		Vector3d origin{ -size[0] * res[0] / 2, -size[1] * res[1] / 2, -size[2] * res[2] / 2 };

		const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty(origin, spacing);

		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

		const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]), static_cast<uint64_t>(size[2]) };
		const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		imagedev::setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
		imagedev::setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

		const RegionXu64 imageRegion{{0, 0, 0}, imageShape};
		imageView->writeRegion(imageRegion, data.get());

		TCDataConverter converter;
		image = converter.ImageViewToImage(imageView);

	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	} catch (...) {
		FAIL("Unknown error");
	}

	return image;
}

auto Extract2DMask(const TCMask::Pointer mask) -> TCMask::Pointer {
	// mask 볼륨에서 z-center slice만 TCMask로 추출
	const auto [resX, resY, resZ] = mask->GetResolution();
	const auto [dimX, dimY, dimZ] = mask->GetSize();
	const auto sliceIndex = dimZ / 2;

	const auto originBuffer = mask->GetMaskVolume().get();
	std::shared_ptr<unsigned short> buffer(new unsigned short[dimX * dimY](), std::default_delete<unsigned short>());
	std::copy_n(originBuffer + (dimX * dimY * sliceIndex), dimX * dimY, buffer.get());

	int resultDims[3] = {dimX, dimY, 1};
	double resultRes[3] = {resX, resY, resZ};

	TCDataConverter converter;
	const auto sliceMask = converter.ArrToLabelMask(buffer.get(), resultDims, resultRes);
	sliceMask->SetResolution(resultRes);
	sliceMask->SetSize(resultDims[0], resultDims[1], resultDims[2]);
	
	return sliceMask;
}

auto Save(const TCImage::Pointer image, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.ImageToImageView(image);
	ioformat::writeView(imageview, savePath.toStdString());
}

auto Save(const TCMask::Pointer mask, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.MaskToImageView(mask);
	ioformat::writeView(imageview, savePath.toStdString());
}

TEST_CASE("LogicalOperationWithImage") {
	REQUIRE(QFile::exists(_TEST_DATA));

	try {
		imagedev::init();
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	// load plugins
	QStringList plugins{
		QString("%1/segmentation/TC.Algorithm.Segmentation.Threshold.dll").arg(_PLUGIN_DIR),
		QString("%1/segmentation/TC.Algorithm.Segmentation.ManualThreshold.dll").arg(_PLUGIN_DIR),
		QString("%1/filtering/TC.Algorithm.Filtering.LogicalNot.dll").arg(_PLUGIN_DIR),
	};

	for (auto& plugin : plugins) {
		REQUIRE(PluginRegistry::LoadPlugin(plugin) > 0);
	}

	TCMask::Pointer mask{nullptr};

	// get a center slice from mask volume
	try {
		const auto ht = LoadHT(_TEST_DATA);

		auto Threshold = [plugins](TCImage::Pointer image) -> TCMask::Pointer {
			// get upper and lower threshold
			const auto autoThresholdModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[0], true));

			const auto autoThresholdParam = autoThresholdModule->Parameter();
			autoThresholdParam->SetValue("Method", IParameter::ValueType(true));

			autoThresholdModule->SetInput(0, image);
			autoThresholdModule->Execute();

			const auto autoThresholdResult = std::dynamic_pointer_cast<TC::Framework::DataList>(autoThresholdModule->GetOutput(0));
			const auto ulThreshold = std::dynamic_pointer_cast<DataSet>(autoThresholdResult->GetData(0));

			const auto lower = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("LThreshold"))->ValueAsDouble();
			const auto upper = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("UThreshold"))->ValueAsDouble();

			// get binary mask volume
			const auto thresholdingModuleInstance = PluginRegistry::GetPlugin(plugins[1], true);
			const auto thresholdingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(thresholdingModuleInstance);

			const auto thresholdingParam = thresholdingModuleInstance->Parameter();
			thresholdingParam->SetValue("LThreshold", IParameter::ValueType(lower));
			thresholdingParam->SetValue("UThreshold", IParameter::ValueType(upper));

			thresholdingModule->SetInput(0, image);
			thresholdingModule->Execute();

			return std::dynamic_pointer_cast<TCMask>(thresholdingModule->GetOutput(0));
		};
		
		mask = Threshold(ht);

	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}	

	const auto algoModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[2], true));

	const auto param = algoModule->Parameter();
	param->SetValue("LogicalOperator", 0);

	SECTION("2D") {
		const auto sliceMask = Extract2DMask(mask);
		REQUIRE(sliceMask != nullptr);

		algoModule->SetInput(0, sliceMask);
		REQUIRE(algoModule->Execute());

		const auto output = std::dynamic_pointer_cast<TCMask>(algoModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(sliceMask, "input_2d");
		Save(output, "output_2d");
	}

	SECTION("3D") {
		algoModule->SetInput(0, mask);
		REQUIRE(algoModule->Execute());

		const auto output = std::dynamic_pointer_cast<TCMask>(algoModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(mask, "input_3d");
		Save(output, "output_3d");
	}

	imagedev::finish();
}
