#include <ParameterRegistry.h>
#include "LogicalNotUiParameter.h"

namespace TC::Algorithm::Filtering::LogicalNot {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}