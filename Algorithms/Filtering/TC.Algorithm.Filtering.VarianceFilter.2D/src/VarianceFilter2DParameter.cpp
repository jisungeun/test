#include <ParameterRegistry.h>
#include "VarianceFilter2DParameter.h"

namespace TC::Algorithm::Filtering::VarianceFilter2D {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");

        RegisterNode(
			"KernelRadius",
			"Kernel radius",
			"The kernel half side length or radius.",
			"ScalarValue.int",
			3, 1, 100
		);

        RegisterNode(
			"KernelShape",
			"Kernel shape",
			"The shape of the window defining the neighborhood.\n0: Square\n1: Disk",
			"ScalarValue.int",
			1, 0, 1
		);
    }
}