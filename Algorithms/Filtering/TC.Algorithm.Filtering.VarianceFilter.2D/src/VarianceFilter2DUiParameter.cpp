#include <ParameterRegistry.h>
#include "VarianceFilter2DUiParameter.h"

namespace TC::Algorithm::Filtering::VarianceFilter2D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}