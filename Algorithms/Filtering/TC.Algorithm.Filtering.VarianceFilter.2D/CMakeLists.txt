project(TC.Algorithm.Filtering.VarianceFilter.2D)

#Header files for external use
set(INTERFACE_HEADERS
	inc/VarianceFilter2D.h
	inc/VarianceFilter2DParameter.h
	inc/VarianceFilter2DUiParameter.h
)


#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
	src/VarianceFilter2D.cpp
	src/VarianceFilter2DParameter.cpp
	src/VarianceFilter2DUiParameter.cpp
)

set(RESOURCE
	resource/AlgorithmMetadata.json
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
	${RESOURCE}
)

add_library(TC::Algorithm::Filtering::VarianceFilter2D ALIAS ${PROJECT_NAME})


target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
		${CMAKE_CURRENT_SOURCE_DIR}/resource
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
		Qt5::Core
		TC::Components::EngineFramework
		TC::Components::DataTypes		
	PRIVATE		
		ImageDev
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

set_target_properties(${PROJECT_NAME}
	PROPERTIES
		ARCHIVE_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/lib/Release/algorithms/filtering"
		ARCHIVE_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/lib/Debug/algorithms/filtering"
		LIBRARY_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/lib/Release/algorithms/filtering"
		LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/lib/Debug/algorithms/filtering"
		RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/bin/Release/algorithms/filtering"
		RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/bin/Debug/algorithms/filtering"
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Algorithms/Filtering") 	

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR}/algorithms/filtering COMPONENT algorithm)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

add_subdirectory(test)
