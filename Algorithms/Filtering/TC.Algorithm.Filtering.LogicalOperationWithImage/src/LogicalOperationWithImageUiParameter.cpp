#include <ParameterRegistry.h>
#include "LogicalOperationWithImageUiParameter.h"

namespace TC::Algorithm::Filtering::LogicalOperationWithImage {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}