#include <ParameterRegistry.h>

#include "LogicalOperationWithImageParameter.h"

namespace TC::Algorithm::Filtering::LogicalOperationWithImage {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");

        RegisterNode(
            "LogicalOperator",
            "Logical operator",
            "The operator to apply.\n0: AND, 1: OR, 2: XOR, 3:NAND, 4: NOR, 5: NXOR, 6: SUBTRACT",
            "ScalarValue.double",
            0, 0, 6
        );
    }
}