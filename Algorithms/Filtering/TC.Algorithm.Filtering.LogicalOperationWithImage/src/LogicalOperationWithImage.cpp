#define LOGGER_TAG "[LogicalOperationWithImage]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataConverter.h>

#include "LogicalOperationWithImageUiParameter.h"
#include "LogicalOperationWithImageParameter.h"
#include "LogicalOperationWithImage.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Filtering::LogicalOperationWithImage {
	struct Algorithm::Impl {
		TCMask::Pointer input1 { nullptr };
		TCMask::Pointer input2 { nullptr };
		TCMask::Pointer result { nullptr };

		IParameter::Pointer param { ParameterRegistry::Create("org.tomocube.algorithm.filtering.logicaloperationwithimage") };
		IUiParameter::Pointer uiParam { UiParameterRegistry::Create("org.tomocube.algorithm.filtering.logicaloperationwithimage") };
		QMap<QString, IParameter::Pointer> params;
		auto AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double;
	};

	auto Algorithm::Impl::AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double {
		return offset - static_cast<double>(refDimZ) * refResZ / 2.0;
	}

	Algorithm::Algorithm() : d { new Impl } {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() { }

	auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index == 0)
			d->input1 = std::dynamic_pointer_cast<TCMask>(data);
		else if (index == 1)
			d->input2 = std::dynamic_pointer_cast<TCMask>(data);
		else
			return false;

		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index)
		return d->result;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
			if (d->params.contains(key))
				return d->params[key];
			else
				return nullptr;
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void { }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START LogicalOperationWithImage";

		bool succeed = false;

		try {
			if (d->input1 == nullptr || d->input2 == nullptr)
				throw std::runtime_error("Invalid input data\n");

			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto logicalOperator = d->param->GetValue("LogicalOperator").toInt(0);

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			auto input1 = converter.MaskToImageView(d->input1, TCDataConverter::MaskType::Label);
			auto input2 = converter.MaskToImageView(d->input2, TCDataConverter::MaskType::Label);

			const auto refOrigin = input1->properties()->calibration().origin();
			const auto maskOrigin = input2->properties()->calibration().origin();
			const auto refSpacing = input1->properties()->calibration().spacing();
			const auto maskSpacing = input2->properties()->calibration().spacing();			

			auto dims = 3;
			if (input1->shape()[2] == 1) {
				dims = 2;
			}

			auto originDiff = false;
			auto spacingDiff = false;
			for (auto i = 0; i < dims; i++) {
				if (false == AreSame(refOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(refSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}

			double refOffset = d->input1->GetOffset();
			double targetOffset = d->input2->GetOffset();

			if (AreSame(refOffset, 0) && false == AreSame(targetOffset, 0)) {
				targetOffset = d->AdjustOffsetZ(targetOffset, input1->shape()[2], refSpacing[2]);
			} else if (false == AreSame(refOffset, 0) && AreSame(targetOffset, 0)) {
				refOffset = d->AdjustOffsetZ(refOffset, input2->shape()[2], maskSpacing[2]);
			}

			if (input1->shape() != input2->shape() || originDiff || spacingDiff) {
				if(refSpacing[2] > maskSpacing[2]) {
					input1 = TCDataConverter::MapMaskGeometry(input2, input1, targetOffset, refOffset);
				}else {
					input2 = TCDataConverter::MapMaskGeometry(input1, input2, refOffset, targetOffset);
				}
			}

			auto processedMask = logicalOperationWithImage(
															input1,
															input2,
															static_cast<imagedev::LogicalOperationWithImage::LogicalOperator>(logicalOperator)
														);

			processedMask = convertImage(processedMask, ConvertImage::OutputType::LABEL_16_BIT);

			auto shape = processedMask->shape();
			int dim[3] = { static_cast<int>(shape[0]), static_cast<int>(shape[1]), static_cast<int>(shape[2]) };

			auto spacing = getCalibrationSpacing(processedMask);
			double res[3] = { spacing[0], spacing[1], spacing[2] };

			d->result = converter.ArrToLabelMask(static_cast<unsigned short*>(processedMask->buffer()), dim, res);

			QLOG_INFO() << QString("FINISH LogicalOperationWithImage - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
			std::cout << e.what() << std::endl;
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
	}

}
