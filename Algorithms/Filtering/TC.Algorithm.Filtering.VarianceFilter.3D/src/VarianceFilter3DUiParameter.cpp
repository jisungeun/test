#include <ParameterRegistry.h>
#include "VarianceFilter3DUiParameter.h"

namespace TC::Algorithm::Filtering::VarianceFilter3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}