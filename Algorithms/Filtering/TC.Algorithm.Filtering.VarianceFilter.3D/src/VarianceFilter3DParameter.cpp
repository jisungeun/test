#include <ParameterRegistry.h>
#include "VarianceFilter3DParameter.h"

namespace TC::Algorithm::Filtering::VarianceFilter3D {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");

        RegisterNode(
			"KernelRadius",
			"Kernel radius",
			"The kernel half side length or radius.",
			"ScalarValue.int",
			3, 1, 100
		);

        RegisterNode(
			"KernelShape",
			"Kernel shape",
			"The shape of the window defining the neighborhood.\n0: Cube\n1: Ball",
			"ScalarValue.int",
			1, 0, 1
		);
    }
}