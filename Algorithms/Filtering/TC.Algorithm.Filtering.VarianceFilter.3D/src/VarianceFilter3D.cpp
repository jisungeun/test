#define LOGGER_TAG "[VarianceFilter3D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataConverter.h>

#include "VarianceFilter3DUiParameter.h"
#include "VarianceFilter3DParameter.h"
#include "VarianceFilter3D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Filtering::VarianceFilter3D {
    struct Algorithm::Impl {
		TCImage::Pointer input{ nullptr };
		TCImage::Pointer output{ nullptr };

        IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.filtering.variancefilter.3d") };
        IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.filtering.variancefilter.3d") };
        QMap<QString, IParameter::Pointer> params;
    };

    Algorithm::Algorithm() : d{ new Impl } {
        Parameter::Register();
        UiParameter::Register();
    }

    Algorithm::~Algorithm() {
        
    }

    auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

		if (index == 0)
			d->input = std::dynamic_pointer_cast<TCImage>(data);
				
		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->output;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return {d->output};
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
			if (d->params.contains(key)) {
				return d->params[key];
			} else {
				return nullptr;
			}
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {

	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

    auto Algorithm::Execute() -> bool {
        QLOG_INFO() << "START VarianceFilter3D";

		bool succeed = false;		

		try {
			if (d->input == nullptr) throw std::runtime_error("Invalid input data\n");

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			auto inputImage = converter.ImageToImageView(d->input);

			const auto [min, max] = d->input->GetMinMax();
			const auto imageStat = intensityStatistics(inputImage, IntensityStatistics::MIN_MAX, { 0,1 });
			if(imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(inputImage, { -INT_MAX,static_cast<double>(min) });
				const auto reseted = resetImage(inputImage, min);
				inputImage = combineByMask(reseted, inputImage, thersholded);
			}

			const auto rescaledImage = rescaleIntensity(inputImage, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, {0, 0} /*ignored*/, {0, 0} /*ignored*/, {0., 100.});

			const auto kernelRadius = d->param->GetValue("KernelRadius").toInt(3);
			const auto kernelShape = static_cast<VarianceFilter3d::KernelShape>(d->param->GetValue("KernelShape").toInt(1));

			auto filteredImage = varianceFilter3d(rescaledImage, kernelShape, kernelRadius);
			const auto originStat = intensityStatistics(filteredImage, IntensityStatistics::MIN_MAX, { 0,1 });

			filteredImage = rescaleIntensity(filteredImage, RescaleIntensity::UNSIGNED_INTEGER_16_BIT, RescaleIntensity::MIN_MAX, { 1,1 }, { 1,1 }, { 13000, 15000 });
			filteredImage->setProperties(inputImage->properties());

			d->output = converter.ImageViewToImage(filteredImage);
			d->output->SetOriginalMinMax(static_cast<float>(originStat->minimum()), static_cast<float>(originStat->maximum()));
			d->output->SetMinMax(13000,15000);
			d->output->SetOffset(d->input->GetOffset());
			d->output->setChannel(d->input->getChannel());

			QLOG_INFO() << QString("FINISH VarianceFilter3D - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
    }

}