#include <ParameterRegistry.h>
#include "SizeFilterParameter.h"

namespace TC::Algorithm::Filtering::SizeFilter {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");

        RegisterNode("ObjectSize", "Object size", "The maximum pixel size of objects to remove", "ScalarValue.double", 10, 0.001, 1000);
    }
}