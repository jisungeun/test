#define LOGGER_TAG "[SizeFilter]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataConverter.h>

#include "SizeFilterUiParameter.h"
#include "SizeFilterParameter.h"
#include "SizeFilter.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Filtering::SizeFilter {
    struct Algorithm::Impl {
        TCMask::Pointer input{ nullptr };
		TCMask::Pointer result{ nullptr };

        IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.filtering.sizefilter") };
        IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.filtering.sizefilter") };
        QMap<QString, IParameter::Pointer> params;
    };

    Algorithm::Algorithm() : d{ new Impl } {
        Parameter::Register();
        UiParameter::Register();
    }

    Algorithm::~Algorithm() {
        
    }

    auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

		d->input = std::dynamic_pointer_cast<TCMask>(data);
		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->result;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return {d->result};
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
			if (d->params.contains(key)) {
				return d->params[key];
			} else {
				return nullptr;
			}
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {

	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

    auto Algorithm::Execute() -> bool {
        QLOG_INFO() << "START SizeFilter";

		bool succeed = false;

		try {
			if (d->input == nullptr) throw std::runtime_error("Invalid input data\n");

			const auto objectSize = d->param->GetValue("ObjectSize").toDouble(10);

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			const auto refMask = converter.MaskToImageView(d->input);
			const auto processedBinaryMask = removeSmallSpots(refMask, objectSize);
			const auto processedMask = convertImage(processedBinaryMask, ConvertImage::OutputType::LABEL_16_BIT);

			auto shape = processedMask->shape();
			int dim[3] = { static_cast<int>(shape[0]), static_cast<int>(shape[1]), static_cast<int>(shape[2]) };

			auto spacing = getCalibrationSpacing(processedMask);
			double res[3] = { spacing[0], spacing[1], spacing[2] };

			d->result = converter.ArrToLabelMask(static_cast<unsigned short*>(processedMask->buffer()), dim, res);
			d->result->SetOffset(d->input->GetOffset());

			QLOG_INFO() << QString("FINISH SizeFilter - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
    }

}