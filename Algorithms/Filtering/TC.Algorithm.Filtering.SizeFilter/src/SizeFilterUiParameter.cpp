#include <ParameterRegistry.h>
#include "SizeFilterUiParameter.h"

namespace TC::Algorithm::Filtering::SizeFilter {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}