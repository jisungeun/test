#include <ParameterRegistry.h>
#include "VarianceFilter25DUiParameter.h"

namespace TC::Algorithm::Filtering::VarianceFilter25D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}