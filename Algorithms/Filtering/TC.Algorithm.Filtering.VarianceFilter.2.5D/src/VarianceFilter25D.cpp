#define LOGGER_TAG "[VarianceFilter25D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataConverter.h>

#include "VarianceFilter25DUiParameter.h"
#include "VarianceFilter25DParameter.h"
#include "VarianceFilter25D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Filtering::VarianceFilter25D {
    struct Algorithm::Impl {
		TCImage::Pointer input{ nullptr };
		TCImage::Pointer output{ nullptr };

        IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.filtering.variancefilter.2.5d") };
        IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.filtering.variancefilter.2.5d") };
        QMap<QString, IParameter::Pointer> params;
    };

    Algorithm::Algorithm() : d{ new Impl } {
        Parameter::Register();
        UiParameter::Register();
    }

    Algorithm::~Algorithm() {
        
    }

    auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

		if (index == 0)
			d->input = std::dynamic_pointer_cast<TCImage>(data);
		
		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->output;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return {d->output};
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
			if (d->params.contains(key)) {
				return d->params[key];
			} else {
				return nullptr;
			}
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {

	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

    auto Algorithm::Execute() -> bool {
        QLOG_INFO() << "START VarianceFilter25D";

		bool succeed = false;		

		try {
			if (d->input == nullptr) throw std::runtime_error("Invalid input data\n");

			const auto kernelRadius = d->param->GetValue("KernelRadius").toInt(3);
			const auto kernelShape = static_cast<VarianceFilter2d::KernelShape>(d->param->GetValue("KernelShape").toInt(1));

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			const auto inputImage = converter.ImageToImageView(d->input);

			const auto [min, max] = d->input->GetMinMax();
			const auto imageStat = intensityStatistics(inputImage, IntensityStatistics::MIN_MAX, { 0,1 });
			const auto isZeroPaded = imageStat->minimum() < min;

			auto destImage = convertImage(inputImage, ConvertImage::FLOAT_32_BIT);

			const auto [sizeX, sizeY, sizeZ] = d->input->GetSize();
			for(auto i=0;i<sizeZ;i++) {
				auto sliced = getSliceFromVolume3d(inputImage, GetSliceFromVolume3d::Z_AXIS, i);
				if (isZeroPaded) {
					const auto thersholded = thresholding(sliced, { -INT_MAX,static_cast<double>(min) });
					const auto reseted = resetImage(sliced, min);
					sliced = combineByMask(reseted, sliced, thersholded);
				}
				const auto rescaledSlice = rescaleIntensity(sliced, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, {0, 0} /*ignored*/, {0, 0} /*ignored*/, {0., 100.});
				const auto filteredSlice = varianceFilter2d(rescaledSlice, kernelShape, kernelRadius);
				destImage = setSliceToVolume3d(destImage, filteredSlice, SetSliceToVolume3d::Z_AXIS, i);
			}

			const auto originStat = intensityStatistics(destImage, IntensityStatistics::MIN_MAX, { 0,1 });

			const auto rescaled = rescaleIntensity(destImage, RescaleIntensity::UNSIGNED_INTEGER_16_BIT, RescaleIntensity::MIN_MAX, { 1,1 }, { 1,1 }, { 13000, 15000 });
			rescaled->setProperties(inputImage->properties());
			
			d->output = converter.ImageViewToImage(rescaled);
			d->output->SetOriginalMinMax(static_cast<float>(originStat->minimum()), static_cast<float>(originStat->maximum()));
			d->output->SetMinMax(13000,15000);
			d->output->SetOffset(d->input->GetOffset());
			d->output->setChannel(d->input->getChannel());

			QLOG_INFO() << QString("FINISH VarianceFilter25D - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
    }

}