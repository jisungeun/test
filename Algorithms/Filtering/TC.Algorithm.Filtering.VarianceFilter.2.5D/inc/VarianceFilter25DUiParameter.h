#pragma once

#include <IUiParameter.h>

namespace TC::Algorithm::Filtering::VarianceFilter25D {
    class UiParameter : public IUiParameter {
        Q_OBJECT
    public:
        static auto Register()->void;

        static IUiParameter::Pointer CreateMethod() {
            return std::make_shared<UiParameter>();
        }

        static std::string GetName() {
            return "org.tomocube.algorithm.filtering.variancefilter.2.5d";
        }
        UiParameter();
    };
}