#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataList.h>
#include <ScalarData.h>
#include <TCDataConverter.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;


auto Generate2DMask(int dim[2], double res[2]) -> TCMask::Pointer {
	const uint64_t rowCount = dim[0];
    const uint64_t colCount = dim[1];
    std::vector<unsigned short> imageData( rowCount * colCount );

    // Define a synthetic square in this array
    const uint64_t sideRow = rowCount / 2;
    const uint64_t sideCol = colCount / 2;

    // Loop on image rows
    for ( uint64_t i = 0; i < rowCount; ++i )
    {
        // Loop on image columns
        for ( uint64_t j = 0; j < colCount; ++j )
        {
            if ( ( i >= ( rowCount - sideRow ) / 2 ) && ( i <= ( rowCount + sideRow ) / 2 ) &&
                 ( j >= ( colCount - sideCol ) / 2 ) && ( j <= ( colCount + sideCol ) / 2 ) )
                imageData[i * colCount + j] = 1; // Value inside the square
            else
                imageData[i * colCount + j] = 0; // Background value
        }
    }

	// Create an image view of same dimensions
    VectorXu64 imageShape{ colCount, rowCount };
    auto image = ImageViewFactory::allocate( imageShape, DataTypeId::UINT16 );
    setDimensionalInterpretation( image, ImageTypeId::IMAGE );

    // Define the region where to write the data
    VectorXu64 imageOrig{ 0, 0 };
    RegionXu64 imageRegion( imageOrig, imageShape );

    // Copy the data in the image view
    image->writeRegion( imageRegion, imageData.data() );

	TCDataConverter converter;
	int tempDim[3] = { dim[0], dim[1], 1 };
	double tempRes[3] = { res[0], res[1], 1};
	return converter.ArrToLabelMask((unsigned short*)image->buffer(), tempDim, tempRes);
}

auto Generate3DMask(int dim[3], double res[3]) -> TCMask::Pointer {
	const uint64_t rowCount = dim[0];
    const uint64_t colCount = dim[1];
    const uint64_t sliCount = dim[2];
    std::vector<unsigned short> imageData( rowCount * colCount * sliCount );

    // Define a synthetic cube in this array
    const uint64_t sideRow = rowCount / 2;
    const uint64_t sideCol = colCount / 2;
    const uint64_t sideSli = sliCount / 2;

    // Loop on image slices
    for ( uint64_t k = 0; k < sliCount; ++k )
    {
        // Loop on image rows
        for ( uint64_t i = 0; i < rowCount; ++i )
        {
            // Loop on image columns
            for ( uint64_t j = 0; j < colCount; ++j )
            {
                if ( ( i >= ( rowCount - sideRow ) / 2 ) && ( i <= ( rowCount + sideRow ) / 2 ) &&
                     ( j >= ( colCount - sideCol ) / 2 ) && ( j <= ( colCount + sideCol ) / 2 ) &&
                     ( k >= ( sliCount - sideSli ) / 2 ) && ( k <= ( sliCount + sideSli ) / 2 ) )
                    imageData[k * rowCount * colCount + i * colCount + j] = 1; // Value inside the cube
                else
                    imageData[k * rowCount * colCount + i * colCount + j] = 0; // Background value
            }
        }
    }

    // Create an image view of same dimensions
    VectorXu64 imageShape{ colCount, rowCount, sliCount };
    auto image = ImageViewFactory::allocate( imageShape, DataTypeId::UINT16 );
    setDimensionalInterpretation( image, ImageTypeId::VOLUME );

    // Define the region where to write the data
    VectorXu64 imageOrig{ 0, 0, 0 };
    RegionXu64 imageRegion{ imageOrig, imageShape };

    // Copy the data in the image view
    image->writeRegion( imageRegion, imageData.data() );

	TCDataConverter converter;
	return converter.ArrToLabelMask((unsigned short*)image->buffer(), dim, res);
}

auto Save(const TCImage::Pointer image, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.ImageToImageView(image);
	ioformat::writeView(imageview, savePath.toStdString());
}

auto Save(const TCMask::Pointer mask, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.MaskToImageView(mask);
	ioformat::writeView(imageview, savePath.toStdString());
}

TEST_CASE("Stencil") {
	REQUIRE(QFile::exists(_TEST_DATA));

	try {
		imagedev::init();
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	// load plugins
	QStringList plugins{
		"/segmentation/TC.Algorithm.Segmentation.ManualThreshold.dll",
		"/segmentation/TC.Algorithm.Segmentation.Threshold.dll",
		"/filtering/TC.Algorithm.Filtering.Stencil.dll"
	};

	for (auto& plugin : plugins) {
		REQUIRE(PluginRegistry::LoadPlugin(QString("%1%2").arg(_PLUGIN_DIR).arg(plugin)) > 0);
	}

	TCImage::Pointer image{ nullptr };
	TCImage::Pointer image2d{ nullptr };

	try {
		// get HT 3D
		H5::H5File file(_TEST_DATA, H5F_ACC_RDONLY);
		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		auto GetResolutionAttrValue = [group](std::string_view name) -> float {
			float res;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_FLOAT, &res);
			sizeAttr.close();

			return res;
		};

		double res[3];
		res[0] = GetResolutionAttrValue("ResolutionX");
		res[1] = GetResolutionAttrValue("ResolutionY");
		res[2] = GetResolutionAttrValue("ResolutionZ");

		auto GetSizeAttrValue = [group](std::string_view name) -> int64_t {
			int64_t size;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_INT64, &size);
			sizeAttr.close();

			return size;
		};

		int size[3];
		size[0] = GetSizeAttrValue("SizeX");
		size[1] = GetSizeAttrValue("SizeY");
		size[2] = GetSizeAttrValue("SizeZ");

		std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]], std::default_delete<uint16_t[]>());
		dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
		dataSet.close();
		group.close();
		file.close();

		const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty( {0, 0, 0}, {1, 1, 1} );

		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

		const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]), static_cast<uint64_t>(size[2]) };
		const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
		setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

		const RegionXu64 imageRegion{{0, 0, 0}, imageShape};
		imageView->writeRegion(imageRegion, data.get());

		TCDataConverter converter;
		image = converter.ImageViewToImage(imageView);

		auto centerSliceImageView = getSliceFromVolume3d(imageView, GetSliceFromVolume3d::Axis::Z_AXIS, size[2] / 2);
		image2d = converter.ImageViewToImage(centerSliceImageView);

	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}	

	const auto moduleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.filtering.stencil");
	const auto algorithmModule = std::dynamic_pointer_cast<IPluginAlgorithm>(moduleInstance);

	SECTION("Stencil 2D processing") {
		auto [x, y, z] = image->GetSize();
		double imageRes[3];
		image->GetResolution(imageRes);

		int dim[2] = { x, y };
		double res[2] = { imageRes[0], imageRes[1] };
		auto mask = Generate2DMask(dim, res);
		REQUIRE(mask != nullptr);

		algorithmModule->SetInput(0, image2d);
		algorithmModule->SetInput(1, mask);
		algorithmModule->Execute();

		const auto output = std::dynamic_pointer_cast<TCImage>(algorithmModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(image2d, "input1_2d");
		Save(mask, "input2_2d");
		Save(output, "output_2d");
	}

	SECTION("Stencil 3D processing") {
		auto [x, y, z] = image->GetSize();
		double res[3];
		image->GetResolution(res);

		int dim[3] = { x, y, z };
		auto mask = Generate3DMask(dim, res);
		REQUIRE(mask != nullptr);

		algorithmModule->SetInput(0, image);
		algorithmModule->SetInput(1, mask);
		algorithmModule->Execute();

		const auto output = std::dynamic_pointer_cast<TCImage>(algorithmModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(image, "input1_3d");
		Save(mask, "input2_3d");
		Save(output, "output_3d");
	}

	imagedev::finish();
}
