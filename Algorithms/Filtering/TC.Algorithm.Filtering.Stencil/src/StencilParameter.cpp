#include <ParameterRegistry.h>
#include "StencilParameter.h"

namespace TC::Algorithm::Filtering::Stencil {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");
    }
}