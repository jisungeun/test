#define LOGGER_TAG "[Stencil]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataConverter.h>

#include "StencilUiParameter.h"
#include "StencilParameter.h"
#include "Stencil.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Filtering::Stencil {
	struct Algorithm::Impl {
		TCImage::Pointer inputImage { nullptr };
		TCMask::Pointer inputMask { nullptr };
		TCImage::Pointer result { nullptr };

		IParameter::Pointer param { ParameterRegistry::Create("org.tomocube.algorithm.filtering.stencil") };
		IUiParameter::Pointer uiParam { UiParameterRegistry::Create("org.tomocube.algorithm.filtering.stencil") };
		QMap<QString, IParameter::Pointer> params;
		auto AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double;
	};

	auto Algorithm::Impl::AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double {
		return offset - static_cast<double>(refDimZ) * refResZ / 2.0;
	}

	Algorithm::Algorithm() : d { new Impl } {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() { }

	auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 1)
			return false;

		if (index == 0) {
			d->inputImage = std::dynamic_pointer_cast<TCImage>(data);
		} else if (index == 1) {
			d->inputMask = std::dynamic_pointer_cast<TCMask>(data);
		}

		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->result;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
			if (d->params.contains(key)) {
				return d->params[key];
			} else {
				return nullptr;
			}
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void { }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START Stencil";

		bool succeed = false;

		try {
			if (d->inputImage == nullptr || d->inputMask == nullptr)
				throw std::runtime_error("Invalid input data\n");

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			const auto inputImage = converter.ImageToImageView(d->inputImage);
			auto inputMask = converter.MaskToImageView(d->inputMask, TCDataConverter::MaskType::Binary);

			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto imageOrigin = inputImage->properties()->calibration().origin();
			const auto maskOrigin = inputMask->properties()->calibration().origin();
			const auto imageSpacing = inputImage->properties()->calibration().spacing();
			const auto maskSpacing = inputMask->properties()->calibration().spacing();
			auto originDiff = false;
			auto spacingDiff = false;
			auto dims = 3;
			const auto [sizeX, sizeY, sizeZ] = d->inputImage->GetSize();
			if (sizeZ == 1) {
				dims = 2;
			}
			for (auto i = 0; i < dims; i++) {
				if (false == AreSame(imageOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(imageSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}

			double refOffset = d->inputImage->GetOffset();
			double targetOffset = d->inputMask->GetOffset();

			if (AreSame(refOffset, 0) && false == AreSame(targetOffset, 0)) {
				targetOffset = d->AdjustOffsetZ(targetOffset, inputImage->shape()[2], imageSpacing[2]);
			} else if (false == AreSame(refOffset, 0) && AreSame(targetOffset, 0)) {
				refOffset = d->AdjustOffsetZ(refOffset, inputMask->shape()[2], maskSpacing[2]);
			}

			if (inputImage->shape() != inputMask->shape() || originDiff || spacingDiff) {
				inputMask = TCDataConverter::MapMaskGeometry(inputImage, inputMask, refOffset, targetOffset);
			}

			const auto [inputMin, inputMax] = d->inputImage->GetMinMax();

			const auto srcImage = maskImage(inputImage, inputMask);

			const auto minImage = resetImage(inputImage, inputMin);

			const auto invertMask = logicalNot(inputMask);

			const auto destImage = maskImage(minImage, invertMask);

			const auto resultImage = arithmeticOperationWithImage(srcImage, destImage, ArithmeticOperationWithImage::ADD);

			const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			d->result = converter.ImageViewToImage(processedImage);

			const auto stat = intensityStatistics(processedImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			d->result->SetMinMax(stat->minimum(), stat->maximum());
			d->result->SetOffset(d->inputImage->GetOffset());
			d->result->setChannel(d->inputImage->getChannel());

			QLOG_INFO() << QString("FINISH Stencil - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
	}

}
