#include <ParameterRegistry.h>
#include "BorderKillUiParameter.h"

namespace TC::Algorithm::Filtering::BorderKill {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}