#include <ParameterRegistry.h>
#include "BorderKillParameter.h"

namespace TC::Algorithm::Filtering::BorderKill {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");

        RegisterNode("Neighborhood", "Neighborhood", "The 3D neighborhood configuration", "ScalarValue.int", 2, 0, 2);
    }
}