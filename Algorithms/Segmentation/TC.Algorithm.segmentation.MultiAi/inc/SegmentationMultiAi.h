#pragma once

#include <QObject>
#include <ISegmentationAlgorithm.h>

#include "ImageRaw.h"
#include "TC_Algorithm_segmentation_MultiAiExport.h"

namespace TC::Algorithm::Segmentation::MultiAi {
    class TC_Algorithm_segmentation_MultiAi_API Algorithm
        :public QObject
        ,public ISegmentationAlgorithm
    {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.algorithm.segmentation.ai.multi")
        Q_INTERFACES(ISegmentationAlgorithm)
    public:
        Algorithm();
        virtual ~Algorithm();

        auto GetName() const->QString override { return "Ai Multi Organ Segmentation"; }
        auto GetFullName() const->QString override { return "org.tomocube.algorithm.segmentation.ai.multi"; }
        auto GetDescription() const -> QString override { return "Multi organ segmentation algorithm with Ai"; }
        auto GetOutputFormat() const -> QString override { return "image.organ.binary.multi_layer";}
        auto clone() const->IPluginModule* override { return new Algorithm(); }

        auto Execute()->bool override;
        auto SetInput(IBaseImage::Pointer image)->bool override;
        auto SetInput(IBaseMask::Pointer image)->bool override;
        auto GetOutput(void)->IBaseMask::Pointer override;
        auto Parameter(void)->IParameter::Pointer override;
                
        //TODO: move these stuffs to Parameter partition
        auto SetDimension(uint32_t size[3])->void; 
        auto SetModel(const std::string path)->void;
        auto ConvertVolumeData(void* buf)->SoVolumeData*;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}