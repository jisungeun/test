#pragma once

#include "ISegmentationAlgorithmParameter.h"

namespace TC::Algorithm::Segmentation::MultiAi {
	class Parameter : public ISegmentationAlgorithmParameter {
		Q_OBJECT    

	public:
		static auto Register()->void;

		static IParameter::Pointer CreateMethod() {
			return std::make_shared<Parameter>();
		}

		static std::string GetName() { return "org.tomocube.algorithm.segmentation.ai.multi"; }

		Parameter();
	};
}