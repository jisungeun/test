#include <iostream>

#include "SegmentationMultiAiParameter.h"
#include "SegmentationMultiAi.h"

#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Images/SoVolumeReaderAdapter.h>

#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>

#include "MaskRaw.h"
#include "ParameterRegistry.h"
#include "TCAiMultiOrganSeg.h"

namespace TC::Algorithm::Segmentation::MultiAi {
	struct Algorithm::Impl {
		//IBaseImage::Pointer image;
		ImageRaw::Pointer image;
		IParameter::Pointer param{ParameterRegistry::Create("org.tomocube.algorithm.segmentation.ai.multi")};
		MaskRaw::Pointer result;
		
		//Ai based cell segmentation
		TC::AI::MultiOrganSeg* multisegmentation;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		d->multisegmentation = new TC::AI::MultiOrganSeg;
	}

	Algorithm::~Algorithm() {
		delete d->multisegmentation;
	}

	auto Algorithm::SetInput(IBaseImage::Pointer image)->bool{
		d->image = std::dynamic_pointer_cast<ImageRaw>(image);
		auto volData = d->image->GetVolume(false);		
		if (volData) {
			double min, max;
			volData->getMinMax(min, max);
			auto dim = volData->getDimension();
			auto res = volData->getVoxelSize();

			auto metaInfo = std::make_shared<TCIO::VolumeMetaInfo>();
			metaInfo->SetDimension(dim[0], dim[1], dim[2]);
			metaInfo->SetResolution(res[0], res[1], res[2]);
			metaInfo->SetValid(true);
			metaInfo->SetFileType(TCIO::VolumeMetaInfo::FType::LDMTCFType);

			d->image->SetMetaInfo(metaInfo);						

			d->result = nullptr;

			d->multisegmentation->setInputImage(static_cast<unsigned short*>(image->GetBuffer()));

			return true;
		}else {
			return false;
		}
	}

	auto Algorithm::SetInput(IBaseMask::Pointer image) -> bool {
		//input should be grayscale image
		return false;
    }


	auto Algorithm::GetOutput() -> IBaseMask::Pointer {
		return d->result;
	}

	auto Algorithm::Parameter(void) -> IParameter::Pointer
	{
		return d->param;
	}

	auto Algorithm::Execute()->bool {
		auto dims = d->image->GetMetaInfo()->GetDimension();
		auto x = std::get<0>(dims); 
	    auto y = std::get<1>(dims);
		auto z = std::get<2>(dims);
		auto size = x*y*z;

		int sizes[3] = { x,y,z };

		auto model = d->param->GetValue("Model").toString();

		d->multisegmentation->setBufferSize(sizes);
		d->multisegmentation->setModelPath(model.toStdString());

		float* outBuf1 = new float[size];
		float* outBuf2 = new float[size];
		float* outBuf3 = new float[size];
		float* outBuf4 = new float[size];

		d->multisegmentation->processMultiSeg(outBuf1, outBuf2, outBuf3, outBuf4);		

		//combination of 4 buffer required
		//how to handle overlapped region?
		//OpenInventor labeling - overlapped label?
		//-> multi layered mask type added

		QVector<SoVolumeData*> result;		
		auto nucleus = ConvertVolumeData(outBuf1);
		nucleus->setName("nucleus_mask");
		auto nucleoli = ConvertVolumeData(outBuf2);
		nucleoli->setName("nucleoli_mask");
		auto membrane = ConvertVolumeData(outBuf3);
		membrane->setName("memebrane_mask");
		auto lipid = ConvertVolumeData(outBuf4);
		lipid->setName("lipid_mask");

		result.push_back(membrane);
		result.push_back(nucleus);
		result.push_back(nucleoli);		
		result.push_back(lipid);

		d->result = std::dynamic_pointer_cast<MaskRaw>(MaskRaw::New(result));
		d->result->setType(MASK_VALUE::ORGAN_BINARY);
		d->result->setLayerName(0, "membrane");
		d->result->setLayerName(1, "nucleus");
		d->result->setLayerName(2, "nucleoli");
		d->result->setLayerName(3, "lipid");

		return true;
	}

	auto Algorithm::ConvertVolumeData(void* buf) -> SoVolumeData* {
		auto dims = d->image->GetMetaInfo()->GetDimension();
		auto x = std::get<0>(dims);
		auto y = std::get<1>(dims);
		auto z = std::get<2>(dims);

		SoRef<SoCpuBufferObject> imgBuf1 = new SoCpuBufferObject;
		imgBuf1->setBuffer(buf, x*y*z * sizeof(unsigned short));

		SbVec4i32 imageSize(x, y, z, 1);

		SoRef<SoMemoryDataAdapter> adap1 = SoMemoryDataAdapter::createMemoryDataAdapter(
			imageSize, SbImageDataType(1, SbDataType::FLOAT),
			SoMemoryDataAdapter::CONTIGUOUS_PER_PLANE, imgBuf1.ptr());

		auto ref_volume = d->image->GetVolume(false);
		auto refAdap = new SoVolumeReaderAdapter;
		refAdap->volumeReader.setValue(ref_volume->getReader());		
		auto origin = refAdap->getOrigin();
		auto resolution = refAdap->getVoxelSize();
		auto row_direction = refAdap->getRowDirection();
		auto col_direction = refAdap->getColumnDirection();

		adap1->setOrigin(origin);
		adap1->setDirection(row_direction, col_direction);
		adap1->setInterpretation(SoImageDataAdapter::Interpretation::VALUE);
		adap1->setVoxelSize(resolution);

		SoRef<SoVRImageDataReader> imageReader = new SoVRImageDataReader;
		imageReader->imageData = adap1.ptr();
		
		//threshold �߰�
		auto threshFilter = new SoThresholdingProcessing;
		threshFilter->inImage = adap1.ptr();
		threshFilter->thresholdLevel.setValue(0.5, INT_MAX);

		auto resultReader = new SoVRImageDataReader;
		resultReader->imageData.connectFrom(&threshFilter->outBinaryImage);

		SoVolumeData* img1 = new SoVolumeData;
		//img1->setReader(*imageReader);
		img1->setReader(*resultReader);

		return img1;
    }

	auto Algorithm::SetModel(const std::string path) -> void {
		d->multisegmentation->setModelPath(path);
    }

	auto Algorithm::SetDimension(uint32_t size[3]) -> void {
		int isize[3];
		isize[0] = size[0];
		isize[1] = size[1];
		isize[2] = size[2];
		d->multisegmentation->setBufferSize(isize);
    }
}