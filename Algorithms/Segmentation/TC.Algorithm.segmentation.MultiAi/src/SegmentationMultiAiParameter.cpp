#include <ParameterRegistry.h>
#include "SegmentationMultiAiParameter.h"

namespace TC::Algorithm::Segmentation::MultiAi {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}
	Parameter::Parameter() {
		RegisterNode("Model", "AiModel", "Set Ai Model Path", "FilePath.pth", "pth/multi/multi_organ.pth", "", "");
		RegisterNode("Gaussian", "GaussianKernel", "Set Gaussian Kernel Path", "FilePath.pth", "pth/multi/gaussian_kernel.pth", "", "");
    }
}