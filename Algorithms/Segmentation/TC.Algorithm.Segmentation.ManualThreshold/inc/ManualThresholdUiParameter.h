#pragma once

#include <IUiParameter.h>

namespace TC::Algorithm::Masking::ManualThreshold {
    class UiParameter:public IUiParameter {
        Q_OBJECT
    public:
        static auto Register()->void;
        static IUiParameter::Pointer CreateMethod() {
            return std::make_shared<UiParameter>();
        }
        static std::string GetName() { return "org.tomocube.algorithm.segmentation.manualthreshold"; }

        UiParameter();
    };
}