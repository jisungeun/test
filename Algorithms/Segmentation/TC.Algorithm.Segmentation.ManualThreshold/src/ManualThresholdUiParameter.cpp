#include <ParameterRegistry.h>
#include "ManualThresholdUiParameter.h"

namespace TC::Algorithm::Masking::ManualThreshold {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterBounder("Bounder1", "UThreshold", "LThreshold", "Upper");
        RegisterBounder("Bounder2", "LThreshold", "UThreshold", "Lower");
        RegisterSorter("Order", QStringList{ "LThreshold","UThreshold" });
    }
}