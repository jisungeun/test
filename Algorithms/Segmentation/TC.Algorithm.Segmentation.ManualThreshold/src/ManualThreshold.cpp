#define LOGGER_TAG "[ManualThreshold]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ParameterRegistry.h>
#include <TCDataConverter.h>

#include "ManualThresholdUiParameter.h"
#include "ManualThresholdParameter.h"
#include "ManualThreshold.h"


using namespace imagedev;


namespace TC::Algorithm::Masking::ManualThreshold {
	struct Algorithm::Impl {		
		TCImage::Pointer image{ nullptr };
		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.segmentation.manualthreshold") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.segmentation.manualthreshold") };
		TCMask::Pointer result{ nullptr };

		bool clean{ false };
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {
		
	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->image = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index)
		return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		if(key== "LThreshold") {
			return std::make_tuple("LThreshold", param->GetValue("LThreshold"));
		}
		if(key=="UThreshold") {
			return std::make_tuple("UThreshold", param->GetValue("UThreshold"));
		}
		return std::make_tuple(QString(), QJsonValue());
	}

    auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
		Q_UNUSED(keys)
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		Q_UNUSED(key)
		return d->param;
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }

	auto Algorithm::Execute()->bool {
		bool succeed = true;

		QLOG_INFO() << "START ManualThreshold";

		const auto lower = d->param->GetValue("LThreshold").toDouble();
		const auto upper = d->param->GetValue("UThreshold").toDouble();

		try {
			if (d->image == nullptr) throw std::runtime_error("Invalid input data");

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			const auto inputImage = converter.ImageToImageView(d->image);

			const auto thresholdImage = thresholding( inputImage, {lower * 10000., upper * 10000.} );
			const auto binaryMask = convertImage(thresholdImage, ConvertImage::OutputType::UNSIGNED_INTEGER_16_BIT);

			auto shape = binaryMask->shape();
			int dim[3] = { shape[0], shape[1], shape[2] > 1 ? shape[2] : 1 };

			auto spacing = getCalibrationSpacing(binaryMask);
			double res[3] = { spacing[0], spacing[1], spacing[2] };

			d->result = converter.ArrToLabelMask((unsigned short*)binaryMask->buffer(), dim, res);

			QLOG_INFO() << QString("FINISH ManualThreshold - total: %1 ms").arg(etimer.elapsed());
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
			succeed = false;
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			QLOG_ERROR() << QString::fromStdString(e.what());
			succeed = false;
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
			succeed = false;
		}			

		return succeed;
	}
}