#include <ParameterRegistry.h>
#include "ManualThresholdParameter.h"

namespace TC::Algorithm::Masking::ManualThreshold {
    auto Parameter::Register()->void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }

    Parameter::Parameter() :IParameter()
    {
        SetVersion("1.0.0");
        RegisterNode("LThreshold", "Assign lower threshold", "Select lower threshold parameter",
                     "ScalarValue.double", 1.35, 0.0, 3.0);
        RegisterNode("UThreshold", "Assign upper threshold", "Select upper threshold parameter",
                     "ScalarValue.double", 1.44, 0.0, 3.0);
    }
}