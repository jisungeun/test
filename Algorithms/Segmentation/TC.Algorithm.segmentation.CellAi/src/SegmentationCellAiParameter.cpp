#include <ParameterRegistry.h>
#include "SegmentationCellAiParameter.h"

namespace TC::Algorithm::Masking::CellAi {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}
	Parameter::Parameter() {
		RegisterNode("BackBone", "BackBoneModel", "Set BackBone Module Path","FilePath.pth", "pth/smooth/backbone_model.pth", "", "");
		RegisterNode("Inst", "InstModel", "Set InstModel Path", "FilePath.pth", "pth/smooth/inst_branch.pth", "", "");
		RegisterNode("Semantic", "SemanticModel", "Set Semantic Model Path", "FilePath.pth", "pth/smooth/semantic_branch.pth", "", "");
		RegisterNode("Gaussian", "GaussianKernel", "Set Gaussian Kernel Path", "FilePath.pth", "pth/smooth/gaussian_kernel.pth", "", "");		
    }
}