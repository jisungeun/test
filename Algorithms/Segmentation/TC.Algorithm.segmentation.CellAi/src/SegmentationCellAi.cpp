#include <iostream>

#include "SegmentationCellAiParameter.h"
#include "SegmentationCellAi.h"

#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Images/SoVolumeReaderAdapter.h>
#include "ParameterRegistry.h"
#include "TCAiCellSegmentation.h"

namespace TC::Algorithm::Masking::CellAi {
	struct Algorithm::Impl {
		//IBaseImage::Pointer image;
		ImageRaw::Pointer image;
		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.masking.ai.cell")};
		MaskRaw::Pointer result;
		
		//Ai based cell segmentation
		AI::CellSegmentation* cellSegmentation;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		d->cellSegmentation = new AI::CellSegmentation;
	}

	Algorithm::~Algorithm() {
		delete d->cellSegmentation;
	}

	auto Algorithm::SetInput(IBaseImage::Pointer image)->bool {
		d->image = std::dynamic_pointer_cast<ImageRaw>(image);
		auto volData = d->image->GetVolume(false);		
		if (volData) {
			double min, max;
			volData->getMinMax(min, max);
			auto dim = volData->getDimension();

			auto res = volData->getVoxelSize();

			auto metaInfo = std::make_shared<TCIO::VolumeMetaInfo>();
			metaInfo->SetDimension(dim[0], dim[1], dim[2]);
			metaInfo->SetResolution(res[0], res[1], res[2]);
			metaInfo->SetValid(true);
			metaInfo->SetFileType(TCIO::VolumeMetaInfo::FType::LDMTCFType);

			d->image->SetMetaInfo(metaInfo);

			d->result = nullptr;			
			d->cellSegmentation->SetInputImage(static_cast<unsigned short*>(d->image->GetBuffer()));
			return true;
		} else {
			return false;
		}
	}

	auto Algorithm::SetInput(IBaseMask::Pointer image) -> bool {
		//input should be grayscale image
		return false;
    }


	auto Algorithm::GetOutput() -> IBaseMask::Pointer {
		return d->result;
	}

	auto Algorithm::Parameter(void) -> IParameter::Pointer
	{
		return d->param;
	}

	auto Algorithm::Execute()->bool {
		auto dims = d->image->GetMetaInfo()->GetDimension();

		//set parameter
		auto backbone = d->param->GetValue("BackBone").toString();
		auto inst = d->param->GetValue("Inst").toString();
		auto semantic = d->param->GetValue("Semantic").toString();
		auto gaussian = d->param->GetValue("Gaussian").toString();		
				
		auto x = std::get<0>(dims);
		auto y = std::get<1>(dims);
		auto z = std::get<2>(dims);

		d->cellSegmentation->SetBackBoneModel(backbone.toStdString());
		d->cellSegmentation->SetSemanticModel(semantic.toStdString());
		d->cellSegmentation->SetInstModel(inst.toStdString());
		d->cellSegmentation->SetGaussianKernel(gaussian.toStdString());
		uint32_t size[3] = { x,y,z };
		d->cellSegmentation->SetDimension(size);

		long long* outBuf = new long long[std::get<0>(dims) * std::get<1>(dims)* std::get<2>(dims)];
		d->cellSegmentation->Execute(outBuf);

		auto volData = ConvertVolumeData(outBuf);

		d->result = std::dynamic_pointer_cast<MaskRaw>(MaskRaw::New(volData));
		d->result->setType(MASK_VALUE::CELL_LABEL);
		d->result->setLayerName(0, "CellInst");

		return true;
	}

	auto Algorithm::SetBackBoneModel(const std::string path) -> void {
		d->cellSegmentation->SetBackBoneModel(path);
    }

	auto Algorithm::SetInstModel(const std::string path) -> void {		
		d->cellSegmentation->SetInstModel(path);
    }

	auto Algorithm::SetSemanticModel(const std::string path) -> void {
		d->cellSegmentation->SetSemanticModel(path);
    }

	auto Algorithm::SetDimension(uint32_t size[3]) -> void {		
		d->cellSegmentation->SetDimension(size);
    }

	auto Algorithm::ConvertVolumeData(void* buf) -> SoVolumeData* {
		auto dims = d->image->GetMetaInfo()->GetDimension();
		auto x = std::get<0>(dims);
		auto y = std::get<1>(dims);
		auto z = std::get<2>(dims);				
		
		//unsigned short* data = reinterpret_cast<unsigned short*>((long long*)buf);

		int cnt = x * y * z;
		unsigned short* data = new unsigned short[cnt];		

		for (int i = 0; i < cnt; i++) {
			*(data+i) = *((long long*)buf + i);			
		}
		
		SoRef<SoCpuBufferObject> imgBuf1 = new SoCpuBufferObject;
		imgBuf1->setBuffer(data, x * y * z * sizeof(unsigned short));

		SbVec4i32 imageSize(x, y, z, 1);

		SoRef<SoMemoryDataAdapter> adap1 = SoMemoryDataAdapter::createMemoryDataAdapter(
			imageSize, SbImageDataType(1, SbDataType::DataType::UNSIGNED_SHORT),
			SoMemoryDataAdapter::CONTIGUOUS_PER_PLANE, imgBuf1.ptr());

		auto volume = d->image->GetVolume(false);

		auto volAdap = new SoVolumeReaderAdapter;
		volAdap->volumeReader.setValue(volume->getReader());

		auto origin = volAdap->getOrigin();
		auto resolution = volAdap->getVoxelSize();
		auto row_direction = volAdap->getRowDirection();
		auto col_direction = volAdap->getColumnDirection();

	    adap1->setOrigin(origin);
		adap1->setDirection(row_direction, col_direction);		
		adap1->setInterpretation(SoImageDataAdapter::Interpretation::LABEL);
		adap1->setVoxelSize(resolution);

		SoRef<SoVRImageDataReader> imageReader = new SoVRImageDataReader;
		imageReader->imageData = adap1.ptr();

		SoVolumeData* img1 = new SoVolumeData;
		img1->setReader(*imageReader);

		return img1;
	}
}
