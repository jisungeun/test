#pragma once

#include <QObject>
#include <ISegmentationAlgorithm.h>

#include "ImageRaw.h"
#include "MaskRaw.h"
#include "TC_Algorithm_segmentation_CellAiExport.h"

namespace TC::Algorithm::Masking::CellAi {
    class TC_Algorithm_segmentation_CellAi_API Algorithm
        :public QObject
        ,public ISegmentationAlgorithm
    {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.algorithm.masking.ai.cell")
        Q_INTERFACES(ISegmentationAlgorithm)
    public:
        Algorithm();
        virtual ~Algorithm();

        auto GetName() const->QString override { return "Ai Cell Instance Segmentation"; }
        auto GetFullName() const->QString override { return "org.tomocube.algorithm.masking.ai.cell"; }
        auto GetDescription() const -> QString override { return "Cell Segmentation algorithm with Ai"; }
        auto GetOutputFormat() const -> QString override { return "image.cell.label.single_layer"; }
        auto clone() const->IPluginModule* override { return new Algorithm(); }

        auto Execute()->bool override;
        auto SetInput(IBaseImage::Pointer image)->bool override;
        auto SetInput(IBaseMask::Pointer image)->bool override;
        auto GetOutput(void)->IBaseMask::Pointer override;
        auto Parameter(void)->IParameter::Pointer override;

        //custom setting for Ai-based cell segmentation
        //TODO: move these stuffs to Parameter partition
        auto SetDimension(uint32_t size[3])->void; 
        auto SetBackBoneModel(const std::string path)->void;
        auto SetInstModel(const std::string path)->void;
        auto SetSemanticModel(const std::string path)->void;
        auto ConvertVolumeData(void* buf)->SoVolumeData*;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}