#define LOGGER_TAG "[AI inference]"
#include <TCLogger.h>
#include <iostream>

#include <QElapsedTimer>
#include <QCoreApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Images/SoVolumeReaderAdapter.h>
#include <Medical/helpers/MedicalHelper.h>

#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/Labeling/SoReorderLabelsProcessing.h>
#pragma warning(pop)

#include <ParameterRegistry.h>
//#include <TCAiUnifiedSeg.h>
#include <TCAiStitchedSeg.h>
#include <TCImage.h>
#include <TCMask.h>
#include <TCDataConverter.h>
#include <OIVMutex.h>

#include "SegmentationUnifiedAiUiParameter.h"
#include "SegmentationUnifiedAiParameter.h"
#include "SegmentationUnifiedAi.h"

namespace TC::Algorithm::Segmentation::UnifiedAi {
    struct Algorithm::Impl {
        TCImage::Pointer image;
        IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.masking.ai.unified") };
        IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.masking.ai.unified") };
        TCMask::Pointer result[2]{ nullptr,nullptr };

        //AI::TCAiUnifiedSeg* cellSegmentation{nullptr};
        AI::TCAiStitchedSeg* cellSegmentation{ nullptr };

        QMap<QString, QStringList> layer_names;
    };
    Algorithm::Algorithm() :d{ new Impl } {
        Parameter::Register();
        UiParameter::Register();
        //d->cellSegmentation = new AI::TCAiUnifiedSeg;
        d->layer_names["CellInst"] = QStringList();
        d->layer_names["CellInst"].push_back("cellInst");
        d->layer_names["CellOrgan"] = QStringList();
        d->layer_names["CellOrgan"].push_back("membrane");
        d->layer_names["CellOrgan"].push_back("nucleus");
        d->layer_names["CellOrgan"].push_back("nucleoli");
        d->layer_names["CellOrgan"].push_back("lipid droplet");
    }
    Algorithm::~Algorithm() {
        /*if (d->cellSegmentation) {
            delete d->cellSegmentation;
        }*/
    }
    auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
        return std::make_tuple(key, param->GetValue(key));
    }
    auto Algorithm::GetLayerNames(QString key) -> QStringList {
        if (d->layer_names.contains(key)) {
            return d->layer_names[key];
        }
        return QStringList();
    }

    auto Algorithm::SetInput(IBaseImage::Pointer image) -> bool {
        d->image = std::dynamic_pointer_cast<TCImage>(image);

        if (nullptr != d->image) {
            d->result[0] = nullptr;
            d->result[1] = nullptr;

            auto img = static_cast<unsigned short*>(d->image->GetBuffer());
            auto size_x = std::get<0>(d->image->GetSize());
            auto size_y = std::get<1>(d->image->GetSize());

            auto base_size = (size_x > size_y) ? size_x : size_y;

            int tile_size = ceil(base_size / 540.0);
            //int tile_size = base_size / 540.0;

            QLOG_INFO() << "TILE SIZE: " << tile_size;
            if (tile_size < 1) {
                tile_size = 1;
            }
            //d->cellSegmentation = new AI::TCAiUnifiedSeg;
            d->cellSegmentation = new AI::TCAiStitchedSeg;
            d->cellSegmentation->setInputImage(img);
            d->cellSegmentation->setTileSize(tile_size);
            return true;
        }
        else {
            return false;
        }
    }
    auto Algorithm::saveImage(unsigned short* buf)->void {
        Q_UNUSED(buf)
    }
    auto Algorithm::saveImage2(long long* buf)->void {
        Q_UNUSED(buf)
    }
    auto Algorithm::saveImage3(float* buf, std::string amne)->void {
        Q_UNUSED(buf)
            Q_UNUSED(amne)
    }
    auto Algorithm::saveImage4(uint8_t* buf, std::string anme) -> void {
        Q_UNUSED(buf)
            Q_UNUSED(anme)
    }

    auto Algorithm::SetInput(IBaseMask::Pointer input) -> bool {
        return false;
    }
    auto Algorithm::GetOutput(int idx) -> IBaseMask::Pointer {
        return d->result[idx];
    }
    auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
        Q_UNUSED(key)
            return d->param;
    }
    auto Algorithm::UiParameter() -> IUiParameter::Pointer {
        return d->uiParam;
    }
    auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        Q_UNUSED(keys)
    }

    auto Algorithm::FreeMemory() -> void {
        std::cout << "Memory freed" << std::endl;
        d->image = nullptr;
        d->result[0] = nullptr;
        d->result[1] = nullptr;
    }
    auto Algorithm::Execute() -> bool {
        std::cout << "START SEGMENTATION" << std::endl;
        //IO::OIVMutexLocker lock(IO::OIVMutex::GetInstance());
        QElapsedTimer etimer;
        etimer.start();
        auto dims = d->image->GetSize();

        /*
        auto inst = d->param->GetValue("Inst").toString();
        auto sub_organ = d->param->GetValue("SubOrgan").toString();
        auto floor = d->param->GetValue("Floor").toString();
        auto gaussian = d->param->GetValue("Gaussian").toString();

        if(inst.left(3).compare("pth")==0) {
            inst = qApp->applicationDirPath() + "/" + inst;
        }
        if (sub_organ.left(3).compare("pth") == 0) {
            sub_organ = qApp->applicationDirPath() + "/" + sub_organ;
        }
        if (floor.left(3).compare("pth") == 0) {
            floor = qApp->applicationDirPath() + "/" + floor;
        }
        if (gaussian.left(3).compare("pth") == 0) {
            gaussian = qApp->applicationDirPath() + "/" + gaussian;
        }

        d->cellSegmentation->setInstName(inst.toStdString());
        d->cellSegmentation->setOrganName(sub_organ.toStdString());
        d->cellSegmentation->setFloorName(floor.toStdString());
        d->cellSegmentation->setGausName(gaussian.toStdString());*/

        auto modelPath = d->param->GetValue("Model").toString();
        if (modelPath.left(3).compare("pth") == 0) {
            modelPath = qApp->applicationDirPath() + "/" + modelPath;
        }
        std::cout << "set model name : " << modelPath.toStdString() << std::endl;
        d->cellSegmentation->setModelName(modelPath.toStdString());

        auto x = std::get<0>(dims);
        auto y = std::get<1>(dims);
        auto z = std::get<2>(dims);
        int sizes[3] = { x,y,z };
        unsigned long long size = static_cast<unsigned long long>(x) * static_cast<unsigned long long>(y) * static_cast<unsigned long long>(z);

        d->cellSegmentation->setBufferSize(sizes);
        d->cellSegmentation->setTimStep(0);

        //std::shared_ptr<long long> instBuf(new long long[size](),std::default_delete<long long[]>());
        //std::shared_ptr<float> memBuf(new float[size](),std::default_delete<float[]>());
        //std::shared_ptr<float> lipBuf(new float[size](),std::default_delete<float[]>());
        //std::shared_ptr<float> nucleBuf(new float[size](),std::default_delete<float[]>());
        //std::shared_ptr<float> nucliBuf(new float[size](),std::default_delete<float[]>());        

        //Ai module
        //d->cellSegmentation->produceUnifiedSegmentation(instBuf.get(), memBuf.get(), nucleBuf.get(), nucliBuf.get(), lipBuf.get());
        d->cellSegmentation->produceAiSegmentation();

        QLOG_INFO() << "elapsed time for ai inference: ";
        QLOG_INFO() << etimer.elapsed();
        etimer.restart();

        auto success = d->cellSegmentation->getSuccess();

        if (!success) {//nucleus missing case
            d->image = nullptr;
            d->result[0] = nullptr;
            d->result[1] = nullptr;
            //instBuf = nullptr;
            //memBuf = nullptr;
            //nucleBuf = nullptr;
            //nucliBuf = nullptr;
            //lipBuf = nullptr;

            delete d->cellSegmentation;

            return false;
        }
        auto isInst = d->cellSegmentation->getInstSuccess();
        //delete d->cellSegmentation;

        //std::shared_ptr<unsigned short> instReal(new unsigned short[size](), std::default_delete<unsigned short>());
        auto instReal = new unsigned short[size]();
        //std::shared_ptr<uint8_t> nucleReal(new uint8_t[size](), std::default_delete<uint8_t[]>());
        auto nucleReal = new uint8_t[size]();
        //std::shared_ptr<uint8_t> nucliReal(new uint8_t[size](), std::default_delete<uint8_t[]>());
        auto nucliReal = new uint8_t[size]();
        //std::shared_ptr<uint8_t> memReal(new uint8_t[size](), std::default_delete<uint8_t[]>());
        auto memReal = new uint8_t[size]();
        //std::shared_ptr<uint8_t> lipReal(new uint8_t[size](), std::default_delete<uint8_t[]>());
        auto lipReal = new uint8_t[size]();

        QLOG_INFO() << "elapsed time for memory allocation: ";
        QLOG_INFO() << etimer.elapsed();
        etimer.restart();

        //No memory leak - Jose T. Kim 210215
        //TCMask RollBack - 21.07.16
        QStringList org_name;
        QList<SoVolumeData*> org_result;
        QList<SoVolumeData*> inst_result;

        QLOG_INFO() << "Convert membrane";
        //SoVolumeData* membrane = ConvertBinaryData2(memBuf.get(),memReal.get());
        //SoVolumeData* membrane = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getMembrane()), memReal.get());
        //SoRef<SoVolumeData> membrane = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getMembrane()), memReal.get());
        SoRef<SoVolumeData> membrane = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getMembrane()), memReal);
        if (nullptr != membrane.ptr()) {
            //membrane->ref();
            membrane->setName("memebrane_mask");
            org_result.push_back(membrane.ptr());
            org_name.push_back("membrane");
        }

        QLOG_INFO() << "elapsed time for membrane copy: ";
        QLOG_INFO() << etimer.elapsed();
        etimer.restart();

        SoRef<SoVolumeData>instance{ nullptr };

        QLOG_INFO() << "Convert instance";
        if (isInst) {
            //instance = ConvertVolumeData(instBuf.get(),instReal.get());
            //instance = ConvertVolumeData(d->cellSegmentation->getInstance(), instReal.get());
            instance = ConvertVolumeData(d->cellSegmentation->getInstance(), instReal);
            //instance->ref();
            inst_result.push_back(instance.ptr());
        }
        else if (nullptr != membrane.ptr()) {
            //instance = ConvertBinaryData3(memBuf.get(),instReal.get());
            //instance = ConvertBinaryData3(static_cast<float*>(d->cellSegmentation->getMembrane()), instReal.get());
            instance = ConvertBinaryData3(static_cast<float*>(d->cellSegmentation->getMembrane()), instReal);
            //instance->ref();
            inst_result.push_back(instance.ptr());
        }
        instance->setName("cell_inst");

        QLOG_INFO() << "elapsed time for instance copy: ";
        QLOG_INFO() << etimer.elapsed();
        etimer.restart();

        //instBuf = nullptr;
        d->cellSegmentation->delInstance();
        //instReal = nullptr;
        //memBuf = nullptr;
        d->cellSegmentation->delMembrane();
        //memReal = nullptr;

        QLOG_INFO() << "Convert nucleus";
        //SoVolumeData* nucleus = ConvertBinaryData2(nucleBuf.get(),nucleReal.get());
        //SoRef<SoVolumeData> nucleus = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getNucleus()), nucleReal.get());
        SoRef<SoVolumeData> nucleus = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getNucleus()), nucleReal);
        if (nullptr != nucleus.ptr()) {
            //nucleus->ref();
            nucleus->setName("nucleus_mask");
            org_result.push_back(nucleus.ptr());
            org_name.push_back("nucleus");
        }
        //nucleBuf = nullptr;
        d->cellSegmentation->delNucleus();
        //nucleReal = nullptr;

        QLOG_INFO() << "elapsed time for nucleus copy: ";
        QLOG_INFO() << etimer.elapsed();
        etimer.restart();

        QLOG_INFO() << "Convert nucleoli";
        //SoVolumeData* nucleoli = ConvertBinaryData2(nucliBuf.get(),nucliReal.get());
        //SoRef<SoVolumeData> nucleoli = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getNucleoli()), nucliReal.get());
        SoRef<SoVolumeData> nucleoli = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getNucleoli()), nucliReal);
        if (nullptr != nucleoli.ptr()) {
            //nucleoli->ref();
            nucleoli->setName("nucleoli_mask");
            org_result.push_back(nucleoli.ptr());
            org_name.push_back("nucleoli");
        }
        //nucliBuf = nullptr;
        d->cellSegmentation->delNucleoli();
        //nucliReal = nullptr;

        QLOG_INFO() << "elapsed time for nucleoli copy: ";
        QLOG_INFO() << etimer.elapsed();
        etimer.restart();

        QLOG_INFO() << "Convert lipid";
        //SoVolumeData* lipid = ConvertBinaryData2(lipBuf.get(),lipReal.get());
        //SoRef<SoVolumeData> lipid = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getLipid()), lipReal.get());
        SoRef<SoVolumeData> lipid = ConvertBinaryData2(static_cast<float*>(d->cellSegmentation->getLipid()), lipReal);
        if (nullptr != lipid.ptr()) {
            //lipid->ref();
            lipid->setName("lipid_mask");
            org_result.push_back(lipid.ptr());
            org_name.push_back("lipid droplet");
        }
        //lipBuf = nullptr;
        d->cellSegmentation->delLipid();
        //lipReal = nullptr;

        QLOG_INFO() << "elapsed time for lipid droplet copy: ";
        QLOG_INFO() << etimer.elapsed();
        etimer.restart();

        delete d->cellSegmentation;

        auto converter = std::shared_ptr<TCDataConverter>(new TCDataConverter, std::default_delete<TCDataConverter>());

        QStringList inst_name;
        inst_name.push_back("cellInst");


        QLOG_INFO() << "Convert SoVolume To TCMask";
        if (inst_result.size() > 0) {
            d->result[0] = converter->SoVolumeDataToMask(inst_result, MaskTypeEnum::MultiLabel, inst_name);
            d->result[0]->SetName("CellInst");
        }
        if (org_result.size() > 0) {
            d->result[1] = converter->SoVolumeDataToMask(org_result, MaskTypeEnum::MultiLayer, org_name);
            d->result[1]->SetName("CellOrgan");
        }

        QLOG_INFO() << "elapsed time for data conversion: ";
        QLOG_INFO() << etimer.elapsed();

        d->cellSegmentation = nullptr;

        /*
        QLOG_INFO() << "Free membrane";
        if (nullptr != membrane) {
            while (membrane->getRefCount() > 0) {
                membrane->unref();
            }
        }
        membrane = nullptr;

        QLOG_INFO() << "Free lipid";
        if (nullptr != lipid) {
            while(lipid->getRefCount()>0) {
                lipid->unref();
            }
        }
        lipid = nullptr;

        QLOG_INFO() << "Free nucleus";
        if (nullptr != nucleus) {
            while (nucleus->getRefCount() > 0) {
                nucleus->unref();
            }
        }
        nucleus = nullptr;

        QLOG_INFO() << "Free nucleoli";
        if (nullptr != nucleoli) {
            while (nucleoli->getRefCount() > 0) {
                nucleoli->unref();
            }
        }
        nucleoli = nullptr;

        QLOG_INFO() << "Free instance";
        if (nullptr != instance) {
            while (instance->getRefCount() > 0) {
                instance->unref();
            }
        }
        instance = nullptr;

        org_result.clear();
        inst_result.clear();
        */
        converter = nullptr;

        std::cout << "Ai Inference finished" << std::endl;

        return true;
    }
    auto Algorithm::SetParameter(IParameter::Pointer param) -> void {

    }

    auto Algorithm::ConvertVolumeData(void* buf, unsigned short* real_buf) -> SoVolumeData* {
        auto dims = d->image->GetSize();
        auto x = std::get<0>(dims);
        auto y = std::get<1>(dims);
        auto z = std::get<2>(dims);

        int cnt = x * y * z;

        for (int i = 0; i < cnt; i++) {
            //*(real_buf + i) = *((long long*)buf + i);
            *(real_buf + i) = static_cast<unsigned short>(*(static_cast<float*>(buf) + i));
        }

        //auto converter = std::shared_ptr<TCDataConverter>(new TCDataConverter, std::default_delete<TCDataConverter>());

        //SoRef<SoVolumeData> volume = converter->ImageToSoVolumeData(d->image);
        double resolution[3];
        d->image->GetResolution(resolution);
        //auto resolution = volume->getVoxelSize().getValue();

        SoRef<SoVolumeData> tempImg = new SoVolumeData;
        //tempImg->data.setValue(SbVec3i32(x, y, z), SbDataType::UNSIGNED_SHORT, 16, (void*)real_buf, SoSFArray::NO_COPY);
        tempImg->data.setValue(SbVec3i32(x, y, z), SbDataType::UNSIGNED_SHORT, 16, (void*)real_buf, SoSFArray::NO_COPY_AND_DELETE);
        tempImg->extent.setValue(SbBox3f(-x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2,
            x * resolution[0] / 2, y * resolution[1] / 2, z * resolution[2] / 2));

        //MedicalHelper::dicomAdjustVolume(tempImg.ptr());
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(tempImg.ptr());
        adap->interpretation = SoMemoryDataAdapter::LABEL;

        SoRef<SoReorderLabelsProcessing> reorder = new SoReorderLabelsProcessing;
        reorder->inLabelImage = adap.ptr();

        SoRef<SoVRImageDataReader> imageReader = new SoVRImageDataReader;
        imageReader->imageData.connectFrom(&reorder->outLabelImage);

        SoVolumeData* img1 = new SoVolumeData;
        img1->ref();
        img1->setReader(*imageReader, TRUE);

        /*
        while(tempImg->getRefCount()>1) {
            tempImg->unref();
        }
        tempImg = nullptr;

        while (volume->getRefCount() > 1) {
            volume->unref();
        }
        volume = nullptr;
        */
        //converter = nullptr;

        img1->unrefNoDelete();

        return img1;
    }
    auto Algorithm::ConvertBinaryData3(float* buf, unsigned short* real_buf) -> SoVolumeData* {
        //for instance missing case
        auto dims = d->image->GetSize();
        auto x = std::get<0>(dims);
        auto y = std::get<1>(dims);
        auto z = std::get<2>(dims);

        int cnt = x * y * z;

        for (int i = 0; i < cnt; i++) {
            if (*(buf + i) > 0.5) {
                *(real_buf + i) = 1;
            }
        }

        //auto converter = std::shared_ptr<TCDataConverter>(new TCDataConverter, std::default_delete<TCDataConverter>());

        //SoRef<SoVolumeData> volume = converter->ImageToSoVolumeData(d->image);
        double resolution[3];
        d->image->GetResolution(resolution);
        ///auto resolution = volume->getVoxelSize().getValue();

        SoVolumeData* img1 = new SoVolumeData;
        img1->ref();
        //img1->data.setValue(SbVec3i32(x,y,z),SbDataType::UNSIGNED_SHORT,16,(void*)real_buf,SoSFArray::COPY);
        img1->data.setValue(SbVec3i32(x, y, z), SbDataType::UNSIGNED_SHORT, 16, (void*)real_buf, SoSFArray::NO_COPY_AND_DELETE);
        img1->extent.setValue(SbBox3f(-x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2,
            x * resolution[0] / 2, y * resolution[1] / 2, z * resolution[2] / 2));
        /*
        while(volume->getRefCount()>1) {
            volume->unref();
        }
        volume = nullptr;*/

        //converter = nullptr;

        img1->unrefNoDelete();

        return img1;
    }

    auto Algorithm::ConvertBinaryData2(float* buf, uint8_t* real_buf) -> SoVolumeData* {
        auto dims = d->image->GetSize();
        auto x = std::get<0>(dims);
        auto y = std::get<1>(dims);
        auto z = std::get<2>(dims);

        int cnt = x * y * z;

        auto isEmpty = true;

        for (int i = 0; i < cnt; i++) {
            if (*(buf + i) > 0.5) {
                *(real_buf + i) = static_cast<uint8_t>(1);
                isEmpty = false;
            }
        }

        if (isEmpty) {
            return nullptr;
        }

        //auto converter = std::shared_ptr<TCDataConverter>(new TCDataConverter, std::default_delete<TCDataConverter>());

        //SoRef<SoVolumeData> volume = converter->ImageToSoVolumeData(d->image);
        double resolution[3];
        d->image->GetResolution(resolution);
        //auto resolution = volume->getVoxelSize();

        SoVolumeData* img1 = new SoVolumeData;
        img1->ref();
        //img1->data.setValue(SbVec3i32(x, y, z), SbDataType::UNSIGNED_BYTE, 8, (void*)real_buf, SoSFArray::COPY);
        img1->data.setValue(SbVec3i32(x, y, z), SbDataType::UNSIGNED_BYTE, 8, (void*)real_buf, SoSFArray::NO_COPY_AND_DELETE);
        img1->extent.setValue(SbBox3f(-x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2,
            x * resolution[0] / 2, y * resolution[1] / 2, z * resolution[2] / 2));

        /*
        while (volume->getRefCount() > 1) {
            volume->unref();
        }
        volume = nullptr;*/

        //converter = nullptr;

        img1->unrefNoDelete();

        return img1;
    }

    auto Algorithm::ConvertBinaryData(float* buf) -> SoVolumeData* {
        auto dims = d->image->GetSize();
        int x = std::get<0>(dims);
        int y = std::get<1>(dims);
        int z = std::get<2>(dims);

        double rs[3];
        d->image->GetResolution(rs);

        SoRef<SoVolumeData> newVol = new SoVolumeData;
        //newVol->ref();
        newVol->data.setValue(SbVec3i32(x, y, z), SbDataType::FLOAT, 32, (void*)buf, SoSFArray::NO_COPY);
        float xmin = -x * rs[0] / 2.0;
        float ymin = -y * rs[1] / 2.0;
        float zmin = -z * rs[2] / 2.0;
        float xmax = x * rs[0] / 2.0;
        float ymax = y * rs[1] / 2.0;
        float zmax = z * rs[2] / 2.0;
        newVol->extent.setValue(xmin, ymin, zmin, xmax, ymax, zmax);

        //MedicalHelper::dicomAdjustVolume(newVol.ptr());
        SoRef<SoMemoryDataAdapter> adap = MedicalHelper::getImageDataAdapter(newVol.ptr());

        SoRef<SoThresholdingProcessing> threshFilter = new SoThresholdingProcessing;
        threshFilter->inImage = adap.ptr();
        threshFilter->thresholdLevel.setValue(0.5, static_cast<float>(INT_MAX));

        SoRef<SoVRImageDataReader> resultReader = new SoVRImageDataReader;
        resultReader->imageData.connectFrom(&threshFilter->outBinaryImage);

        SoVolumeData* img1 = new SoVolumeData;
        img1->ref();
        img1->setReader(*resultReader, TRUE);

        img1->unrefNoDelete();

        return img1;
    }
}