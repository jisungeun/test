#include <ParameterRegistry.h>
#include "SegmentationUnifiedAiParameter.h"

namespace TC::Algorithm::Segmentation::UnifiedAi {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() {
        SetVersion("1.0.0");
        //RegisterNode("Model", "AIModel", "Set Model Path", "FilePath.pth", "pth/stitched/cell_segmentation_v2.2.jit", "", "");
        RegisterNode("Model", "AIModel", "Set Model Path", "FilePath.pth", "pth/stitched/cell_segmentation_v2.1.jit", "", "");
    }
}