#include <ParameterRegistry.h>
#include "SegmentationUnifiedAiUiParameter.h"

namespace TC::Algorithm::Segmentation::UnifiedAi {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterHider("Hider1", QStringList{ "Model"});
        RegisterSorter("Order", QStringList{ "Model"});
    }
}