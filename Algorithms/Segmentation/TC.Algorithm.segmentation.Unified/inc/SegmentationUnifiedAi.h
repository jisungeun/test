#pragma once

#include <QObject>
#include <ISegmentationAlgorithm.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include "TC_Algorithm_segmentation_UnifiedExport.h"

namespace TC::Algorithm::Segmentation::UnifiedAi {
    class TC_Algorithm_segmentation_Unified_API Algorithm
        :public QObject
        ,public ISegmentationAlgorithm {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.algorithm.masking.ai.unified")
        Q_INTERFACES(ISegmentationAlgorithm)
    public:
        Algorithm();
        virtual ~Algorithm();

        //auto GetName() const -> QString override { return "Ai Segmentation"; }
        auto GetName() const -> QString override { return "AI Inference"; }
        auto GetFullName() const -> QString override { return "org.tomocube.algorithm.masking.ai.unified"; }
        auto GetDescription() const -> QString override { return "Unified Segmentation algorithm with Ai"; }
        //TODO: need to generalize output format of modules 20.08.10 Jose T. Kim     
        auto GetOutputFormat() const -> QString override { return "Mask!MultiLabel!MultiLayer"; }
        auto clone() const -> IPluginModule* override { return new Algorithm(); }
        auto ParamConverter(IParameter::Pointer, QString key) -> std::tuple<QString, QJsonValue> override;
        auto FreeMemory() -> void override;

        auto Execute() -> bool override;
        auto SetInput(IBaseMask::Pointer input) -> bool override;
        auto SetInput(IBaseImage::Pointer image) -> bool override;
        auto GetOutput(int idx) -> IBaseMask::Pointer override;            
        auto Parameter(const QString& key) -> IParameter::Pointer override;
        auto UiParameter()->IUiParameter::Pointer override;
        auto DuplicateParameter(const QStringList& keys) -> void override;
        auto GetLayerNames(QString key) -> QStringList override;
        auto SetParameter(IParameter::Pointer param) -> void override;

        auto SetDimension(uint32_t size[3])->void;
        auto SetInstModel(const std::string path)->void;
        auto SetSubOrgModel(const std::string path)->void;
        auto SetGausKernel(const std::string path)->void;
        auto SetFloorModel(const std::string path)->void;
        auto ConvertVolumeData(void* buf,unsigned short* real_buf)->SoVolumeData*;
        auto ConvertBinaryData(float* buf)->SoVolumeData*;
        auto ConvertBinaryData2(float* buf, uint8_t* real_buf)->SoVolumeData*;
        auto ConvertBinaryData3(float* buf,unsigned short* real_buf)->SoVolumeData*;

    private:
        auto saveImage(unsigned short* buf)->void;
        auto saveImage2(long long* buf)->void;
        auto saveImage3(float* buf,std::string anme)->void;
        auto saveImage4(uint8_t* buf, std::string anme)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}