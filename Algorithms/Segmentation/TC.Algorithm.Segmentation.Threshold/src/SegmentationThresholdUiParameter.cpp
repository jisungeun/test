#include <ParameterRegistry.h>
#include "SegmentationThresholdUiParameter.h"

namespace TC::Algorithm::Segmentation::Threshold {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterEnabler("Method", "Auto lower threshold", QStringList{ "AlgoSelection" }, QStringList());
        RegisterHider("Hide1", QStringList{ "Index" });
        RegisterSetter("AlgoSelection","Select Automatic Threshold Algorithm", QStringList{"Index"}, QStringList{"Otsu","Entropy","Moment Preserving", "Picture"});        
        AppendSetter("AlgoSelection", "Index", "Ostu", 0);
        AppendSetter("AlgoSelection", "Index", "Entropy", 1);
        AppendSetter("AlgoSelection", "Index", "Moment Preserving", 2);
        AppendSetter("AlgoSelection", "Index", "Picture", 3);
                
        RegisterSorter("Order", QStringList{ "Method","AlgoSelection","Index"});
    }
}