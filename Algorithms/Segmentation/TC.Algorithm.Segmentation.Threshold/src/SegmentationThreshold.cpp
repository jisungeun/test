#define LOGGER_TAG "[OtsuThresh]"
#include <TCLogger.h>

#include <iostream>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <ScalarData.h>
#include <TCImage.h>
#include <DataList.h>
#include <TCDataConverter.h>

#include "SegmentationThresholdUiParameter.h"
#include "SegmentationThresholdParameter.h"
#include "SegmentationThreshold.h"


using namespace TC::Framework;
using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Segmentation::Threshold {
	struct Algorithm::Impl {		
		TCImage::Pointer image;
		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.segmentation.threshold") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.segmentation.threshold") };
		DataList::Pointer result;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {
	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->image = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        Q_UNUSED(keys)
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		Q_UNUSED(key)
		return d->param;
    }

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }
	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		if(key == "Method!Enabler") {
			return std::make_tuple("Method", param->GetValue("Method!Enabler"));
		}
		if(key == "Method!Setter") {
			return std::make_tuple("AlgoSelection", param->GetValue("Method!Setter"));
		}
		if(key == "Index") {
			return std::make_tuple("Index", param->GetValue("Index"));
		}
		return std::make_tuple(QString(), QJsonValue());
    }
	auto Algorithm::Execute()->bool {
		AutoThresholdingBright::ThresholdCriterion criteria = AutoThresholdingBright::ThresholdCriterion::FACTORISATION;
		const auto method = d->param->GetValue("Index").toInt();
		switch (method) {
		case 1:	// entropy based
			criteria = AutoThresholdingBright::ThresholdCriterion::ENTROPY;
			break;
		case 2:	// moment preserving
			criteria = AutoThresholdingBright::ThresholdCriterion::MOMENTS;
			break;
		case 3:	// picture
			criteria = AutoThresholdingBright::ThresholdCriterion::ISODATA;
			break;
		case 0:	// otsu
		default:
		    criteria = AutoThresholdingBright::ThresholdCriterion::FACTORISATION;
			break;
		}

	    auto [intensityMin, intensityMax] = d->image->GetMinMax();

		TCDataConverter converter;
		const auto inputImage = converter.ImageToImageView(d->image);

		// compute auto-threshold on gray level image to detect bright particles on a dark background
		AutoThresholdingBright autoThresholdingBright;
	    autoThresholdingBright.setInputGrayImage(inputImage);
		autoThresholdingBright.setRangeMode(AutoThresholdingBright::RangeMode::MIN_MAX);
		autoThresholdingBright.setIntensityInputRange( {static_cast<double>(intensityMin), static_cast<double>(intensityMax)} );
		autoThresholdingBright.setThresholdCriterion(criteria);
		autoThresholdingBright.execute();

		// save result
		const auto threshold = autoThresholdingBright.outputMeasurement()->threshold();

		auto dset = DataSet::New();
		dset->AppendData(ScalarData::New(threshold / 10000.0), "LThreshold");
		dset->AppendData(ScalarData::New(2.0), "UThreshold");

		d->result = DataList::New();
		d->result->Append(dset);

		return true;
	}
}