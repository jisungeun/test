#include <ParameterRegistry.h>
#include "SegmentationThresholdParameter.h"

namespace TC::Algorithm::Segmentation::Threshold {/*TC::Algorithm::Segmentation::Threshold*/
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() :IParameter()
	{
		SetVersion("1.0.0");
		RegisterNode("Method", "Auto lower threshold", "Use otsu threshold", "TFCheck", false, "", "");
		RegisterNode("AlgoSelection", "Select Algorithm", "Select automatic threshold algorithm", "SelectOption", 0, 0, 2);
		RegisterNode("Index", "Select Algorithm", "Select automatic threshold algorithm", "ScalarValue.int", 0, 0, 2);
	}
}