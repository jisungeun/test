#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <TCFMetaReader.h>
#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <TCDataConverter.h>
#include <DataList.h>
#include <ScalarData.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;

// return <TCImage-2D, TCImage-3D>
auto LoadHT(std::string_view path) -> std::tuple<TCImage::Pointer, TCImage::Pointer> {
	TCImage::Pointer image2d{nullptr};
	TCImage::Pointer image3d{nullptr};

	try {
		const auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
		const auto metaInfo = metaReader->Read(path.data());

		H5::H5File file(path.data(), H5F_ACC_RDONLY);

		// get 2D MIP
		{
			auto group = file.openGroup("/Data/2DMIP");
			auto dataSet = group.openDataSet("000000");

			int size[2] = {metaInfo->data.data2DMIP.sizeX, metaInfo->data.data2DMIP.sizeY};
			double res[2] = {metaInfo->data.data2DMIP.resolutionX, metaInfo->data.data2DMIP.resolutionY};

			std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1]](), std::default_delete<uint16_t[]>());
			dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
			dataSet.close();
			group.close();

			// get a center slice image
			Vector3d spacing{ res[0], res[1] };
			Vector3d origin{ -size[0] * res[0] / 2, -size[1] * res[1] / 2 };

			const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty(origin, spacing);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

			const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]) };
			const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			imagedev::setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);
			imagedev::setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

			const RegionXu64 imageRegion{{0, 0}, imageShape};
			imageView->writeRegion(imageRegion, data.get());

			TCDataConverter converter;
			image2d = converter.ImageViewToImage(imageView);
			image2d->SetMinMax(metaInfo->data.data2DMIP.riMin * 10000, metaInfo->data.data2DMIP.riMax * 10000);
		}
		// get 3D
		{
			auto group = file.openGroup("/Data/3D");
			auto dataSet = group.openDataSet("000000");

			int size[3] = {metaInfo->data.data3D.sizeX, metaInfo->data.data3D.sizeY, metaInfo->data.data3D.sizeZ};
			double res[3] = {metaInfo->data.data3D.resolutionX, metaInfo->data.data3D.resolutionY, metaInfo->data.data3D.resolutionZ};

			std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]](), std::default_delete<uint16_t[]>());
			dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
			dataSet.close();
			group.close();
			file.close();

			// get a center slice image
			Vector3d spacing{ res[0], res[1], res[2] };
			Vector3d origin{ -size[0] * res[0] / 2, -size[1] * res[1] / 2, -size[2] * res[2] / 2 };

			const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty(origin, spacing);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

			const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]), static_cast<uint64_t>(size[2]) };
			const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			imagedev::setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
			imagedev::setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

			const RegionXu64 imageRegion{{0, 0, 0}, imageShape};
			imageView->writeRegion(imageRegion, data.get());

			TCDataConverter converter;
			image3d = converter.ImageViewToImage(imageView);
			image3d->SetMinMax(metaInfo->data.data3D.riMin * 10000, metaInfo->data.data3D.riMax * 10000);
		}

		file.close();
	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	} catch (...) {
		FAIL("Unknown error");
	}

	return {image2d, image3d};
}

TEST_CASE("Threshold") {
    REQUIRE(QFile::exists(_TEST_DATA));

    try {
        imagedev::init();
    } catch(imagedev::Exception& e) {
        FAIL(e.what().c_str());
    }
    
	const auto plugin = QString("%1/segmentation/TC.Algorithm.Segmentation.Threshold.dll").arg(_PLUGIN_DIR);
	REQUIRE(PluginRegistry::LoadPlugin(plugin) > 0);

    const auto [image2d, image3d] = LoadHT(_TEST_DATA);

	SECTION("2D") {
		const auto algoModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugin, true));

		const auto param = algoModule->Parameter();
		param->SetValue("Index", 0);

		algoModule->SetInput(0, image2d);
		REQUIRE(algoModule->Execute());

		const auto output = std::dynamic_pointer_cast<TC::Framework::DataList>(algoModule->GetOutput(0));
		REQUIRE(output != nullptr);

	    const auto data = std::dynamic_pointer_cast<DataSet>(output->GetData(0));
	    const auto lowThresold = std::dynamic_pointer_cast<TC::Framework::ScalarData>(data->GetData("LThreshold"));
	    const auto highThresold = std::dynamic_pointer_cast<TC::Framework::ScalarData>(data->GetData("UThreshold"));

		std::cout << "2D - LThreshold: " << lowThresold->ValueAsDouble() << ", UThreshold: " << highThresold->ValueAsDouble() << std::endl;
	}

	SECTION("3D") {
		const auto algoModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugin, true));

		const auto param = algoModule->Parameter();
		param->SetValue("Index", 0);

		algoModule->SetInput(0, image3d);
		REQUIRE(algoModule->Execute());

		const auto output = std::dynamic_pointer_cast<TC::Framework::DataList>(algoModule->GetOutput(0));
		REQUIRE(output != nullptr);

		const auto data = std::dynamic_pointer_cast<DataSet>(output->GetData(0));
	    const auto lowThresold = std::dynamic_pointer_cast<TC::Framework::ScalarData>(data->GetData("LThreshold"));
	    const auto highThresold = std::dynamic_pointer_cast<TC::Framework::ScalarData>(data->GetData("UThreshold"));

		std::cout << "3D - LThreshold: " << lowThresold->ValueAsDouble() << ", UThreshold: " << highThresold->ValueAsDouble() << std::endl;
	}

    imagedev::finish();
}