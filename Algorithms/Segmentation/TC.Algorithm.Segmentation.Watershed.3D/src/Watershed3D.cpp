#define LOGGER_TAG "[Watershed3D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ParameterRegistry.h>
#include <TCDataConverter.h>

#include "Watershed3DUiParameter.h"
#include "Watershed3DParameter.h"
#include "Watershed3D.h"


using namespace imagedev;


namespace TC::Algorithm::Segmentation::Watershed3D {
	struct Algorithm::Impl {
		TCMask::Pointer inputLabel { nullptr };
		TCMask::Pointer inputBinary { nullptr };
		TCMask::Pointer output { nullptr };

		IParameter::Pointer param { ParameterRegistry::Create("org.tomocube.algorithm.segmentation.watershed.3d") };
		IUiParameter::Pointer uiParam { UiParameterRegistry::Create("org.tomocube.algorithm.segmentation.watershed.3d") };

		bool clean { false };
		auto AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double;
	};

	auto Algorithm::Impl::AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double {
		return offset - static_cast<double>(refDimZ) * refResZ / 2.0;
	}

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() { }

	auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index == 0)
			d->inputLabel = std::dynamic_pointer_cast<TCMask>(data);
		else if (index == 1)
			d->inputBinary = std::dynamic_pointer_cast<TCMask>(data);
		else
			return false;

		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index)
		return d->output;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->output };
	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		// unused
		return std::make_tuple(QString(), QJsonValue());
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
		Q_UNUSED(keys)
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		Q_UNUSED(key)
		return d->param;
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::Execute() -> bool {
		bool succeed = true;

		QLOG_INFO() << "START Watershed3D";

		const auto objectSize = d->param->GetValue("ObjectSize").toDouble();

		try {
			if (d->inputLabel == nullptr || d->inputBinary == nullptr)
				throw std::runtime_error("Invalid input data.");

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;

			auto inputLabel = converter.MaskToImageView(d->inputLabel);
			auto inputBinary = converter.MaskToImageView(d->inputBinary, TCDataConverter::MaskType::Binary);

			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			double refOffset = d->inputBinary->GetOffset();
			double targetOffset = d->inputLabel->GetOffset();

			const auto imageOrigin = inputBinary->properties()->calibration().origin();
			const auto maskOrigin = inputLabel->properties()->calibration().origin();
			const auto imageSpacing = inputBinary->properties()->calibration().spacing();
			const auto maskSpacing = inputLabel->properties()->calibration().spacing();

			auto originDiff = false;
			auto spacingDiff = false;
			auto dims = 3;
			const auto [sizeX, sizeY, sizeZ] = d->inputBinary->GetSize();
			if (sizeZ == 1) {
				dims = 2;
			}
			for (auto i = 0; i < dims; i++) {
				if (false == AreSame(imageOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(imageSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (AreSame(refOffset, 0) && false == AreSame(targetOffset, 0)) {
				targetOffset = d->AdjustOffsetZ(targetOffset, inputBinary->shape()[2], imageSpacing[2]);
			} else if (false == AreSame(refOffset, 0) && AreSame(targetOffset, 0)) {
				refOffset = d->AdjustOffsetZ(refOffset, inputLabel->shape()[2], maskSpacing[2]);
			}

			if (inputLabel->shape() != inputBinary->shape() || originDiff || spacingDiff) {
				if (imageSpacing[2] > maskSpacing[2]) {
					inputBinary = TCDataConverter::MapMaskGeometry(inputLabel, inputBinary, targetOffset, refOffset);
					inputBinary = convertImage(inputBinary, ConvertImage::BINARY);
				}else {
					inputLabel = TCDataConverter::MapMaskGeometry(inputBinary, inputLabel, refOffset, targetOffset);
				}
			}

			// Step1: Apply size Filter to input binary image
			auto filteredBinary = removeSmallSpots(inputBinary, objectSize);
			// Step2: Create seed label from filtered input binary image
			const auto filteredLabel = labeling3d(filteredBinary, Labeling3d::LABEL_16_BIT, Labeling3d::CONNECTIVITY_26);
			const auto labelStat = objectCount(filteredLabel);
			const auto labelCnt = labelStat.outputMeasurement->count();
			if (labelCnt < 1)
				throw std::runtime_error("Failed to get label image from filtered binary mask.");

			// Step3: if label region in Step2 does not contains label from InputLabel, append it.
			auto newLabel = d->inputLabel->GetBlobIndexes().count();
			for (auto i = 1; i < static_cast<int>(labelCnt) + 1; i++) {
				auto labelBinary = thresholding(filteredLabel, { static_cast<double>(i), i + 0.5 });

				const auto maskingBySingleLabel = maskImage(inputLabel, labelBinary);
				const auto maskingSum = intensityIntegral3d(maskingBySingleLabel);
				if (maskingSum->intensityIntegral() < 1) {
					newLabel++;
					auto valueImage = resetImage(inputLabel, newLabel);
					auto newlabelSrc = maskImage(valueImage, labelBinary);
					inputLabel = arithmeticOperationWithImage(inputLabel, newlabelSrc, ArithmeticOperationWithImage::ADD);
				}
			}
			inputLabel = maskImage(inputLabel, filteredBinary);
			inputLabel = convertImage(inputLabel, ConvertImage::LABEL_16_BIT);
			inputLabel = reorderLabels(inputLabel);
			// Step4: perform watershed
			const auto binInput = convertImage(filteredBinary, ConvertImage::BINARY);
			auto distanceMap = euclideanDistanceMap3d(binInput, EuclideanDistanceMap3d::MappingMode::INSIDE, EuclideanDistanceMap3d::BorderCondition::ZERO);
			distanceMap = arithmeticOperationWithValue(distanceMap, -1, ArithmeticOperationWithValue::MULTIPLY);
			const auto watersheded = markerBasedWatershed3d(distanceMap, inputLabel, MarkerBasedWatershed3d::FAST, MarkerBasedWatershed3d::OutputType::CONTIGUOUS_REGIONS, MarkerBasedWatershed3d::CONNECTIVITY_26);

			//Step5: make region growed label based on wateshed region separation
			const auto binaryInput = convertImage(filteredBinary, ConvertImage::BINARY);
			const auto result = maskImage(watersheded, binaryInput);

			const auto shape = result->shape();
			const auto spacing = getCalibrationSpacing(result);

			int dim[3] { shape[0], shape[1], shape[2] };
			double res[3] { spacing[0], spacing[1], spacing[2] };

			d->output = converter.ArrToLabelMask(static_cast<unsigned short*>(result->buffer()), dim, res);

			QLOG_INFO() << QString("FINISH Watershed3D - total: %1 ms").arg(etimer.elapsed());
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
			succeed = false;
		}
		catch (Exception& e) {
			std::cout << e.what() << std::endl;
			QLOG_ERROR() << QString::fromStdString(e.what());
			succeed = false;
		}
		catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
			succeed = false;
		}

		return succeed;
	}
}
