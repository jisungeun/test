#include <ParameterRegistry.h>
#include "Watershed3DUiParameter.h"

namespace TC::Algorithm::Segmentation::Watershed3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}