#include <ParameterRegistry.h>
#include "Translate3DUiParameter.h"

namespace TC::Algorithm::Geometry::Translate3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}