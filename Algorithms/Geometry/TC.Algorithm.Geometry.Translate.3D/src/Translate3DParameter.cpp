#include <ParameterRegistry.h>

#include "Translate3DParameter.h"

namespace TC::Algorithm::Geometry::Translate3D {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");

        RegisterNode(
            "TranslationX",
            "X",
            "The translation X vector in voxels.",
            "ScalarValue.int",
            0, -9999, 9999
        );

        RegisterNode(
            "TranslationY",
            "Y",
            "The translation Y vector in voxels.",
            "ScalarValue.int",
            0, -9999, 9999
        );

        RegisterNode(
            "TranslationZ",
            "Z",
            "The translation Z vector in voxels.",
            "ScalarValue.int",
            0, -9999, 9999
        );
    }
}