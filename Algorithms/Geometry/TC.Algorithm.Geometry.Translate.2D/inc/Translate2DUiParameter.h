#pragma once

#include <IUiParameter.h>

namespace TC::Algorithm::Geometry::Translate2D {
    class UiParameter : public IUiParameter {
        Q_OBJECT
    public:
        static auto Register()->void;

        static IUiParameter::Pointer CreateMethod() {
            return std::make_shared<UiParameter>();
        }

        static std::string GetName() {
            return "org.tomocube.algorithm.geometry.translate.2d";
        }
        UiParameter();
    };
}