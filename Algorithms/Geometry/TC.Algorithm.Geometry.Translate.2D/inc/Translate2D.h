#pragma once

#include <QObject>

#include <IPluginAlgorithm.h>

#include "TC.Algorithm.Geometry.Translate.2DExport.h"

namespace TC::Algorithm::Geometry::Translate2D {
    class TC_Algorithm_Geometry_Translate_2D_API Algorithm : public QObject, public IPluginAlgorithm {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.algorithm.geometry.translate.2d" FILE "AlgorithmMetadata.json")
        Q_INTERFACES(IPluginAlgorithm)

    public:
        Algorithm();
        virtual ~Algorithm();

        auto clone() const->IPluginModule* override;

        auto Parameter(const QString& key)->IParameter::Pointer override;
		auto DuplicateParameter(const QStringList& keys) -> void override;	// deprecated

		auto UiParameter() -> IUiParameter::Pointer override;
		auto ParamConverter(IParameter::Pointer, QString key) -> std::tuple<QString, QJsonValue> override;

		auto SetInput(int index, IBaseData::Pointer data) -> bool override;
		auto GetOutput(int index) const -> IBaseData::Pointer override;
		auto GetOutputs() const -> QList<IBaseData::Pointer> override;

		auto Execute() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}