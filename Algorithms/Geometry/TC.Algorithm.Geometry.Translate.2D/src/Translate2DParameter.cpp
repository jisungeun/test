#include <ParameterRegistry.h>

#include "Translate2DParameter.h"

namespace TC::Algorithm::Geometry::Translate2D {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");

        RegisterNode(
            "TranslationX",
            "X",
            "The translation X vector in pixels.",
            "ScalarValue.int",
            0, -9999, 9999
        );

        RegisterNode(
            "TranslationY",
            "Y",
            "The translation Y vector in pixels.",
            "ScalarValue.int",
            0, -9999, 9999
        );
    }
}