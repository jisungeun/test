#include <ParameterRegistry.h>
#include "Translate2DUiParameter.h"

namespace TC::Algorithm::Geometry::Translate2D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(UiParameter::GetName(), UiParameter::CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}