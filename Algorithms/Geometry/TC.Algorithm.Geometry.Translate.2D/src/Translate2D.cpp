#define LOGGER_TAG "[Translate2D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCDataConverter.h>

#include "Translate2DUiParameter.h"
#include "Translate2DParameter.h"
#include "Translate2D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Geometry::Translate2D {
    struct Algorithm::Impl {
        TCImage::Pointer input{ nullptr };
		TCImage::Pointer output{ nullptr };

        IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.geometry.translate.2d") };
        IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.geometry.translate.2d") };
    };

    Algorithm::Algorithm() : d{ new Impl } {
        Parameter::Register();
        UiParameter::Register();
    }

    Algorithm::~Algorithm() {
        
    }

    auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

    	d->input = std::dynamic_pointer_cast<TCImage>(data);

    	return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index)
		return d->output;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return {d->output};
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		}

    	return nullptr;
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {

	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

    auto Algorithm::Execute() -> bool {
        QLOG_INFO() << "START Translate2D";

		bool succeed = false;

		try {
			if (d->input == nullptr) throw std::runtime_error("Invalid input data\n");

			const auto x = d->param->GetValue("TranslationX").toInt();
			const auto y = d->param->GetValue("TranslationY").toInt();

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			const auto input = converter.ImageToImageView(d->input);

			const auto [min, max] = d->input->GetMinMax();
			const auto result = translateImage2d(input, {x, y}, TranslateImage2d::BackgroundMode::FIXED, min);

			const auto stat = intensityStatistics(result, IntensityStatistics::MIN_MAX, { 0,1 } /* if the range mode is MIN_MAX, this is ignored*/);

			d->output = converter.ImageViewToImage(result);

			d->output->SetMinMax(stat->minimum(), stat->maximum());
			d->output->SetOffset(d->input->GetOffset());
			d->output->setChannel(d->input->getChannel());

			QLOG_INFO() << QString("FINISH Translate2D - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
			std::cout << e.what() << std::endl;
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
    }

}