#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataList.h>
#include <ScalarData.h>
#include <TCDataConverter.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;

auto LoadHT() -> TCImage::Pointer {
	TCImage::Pointer image{nullptr};
	try {
		// get HT 3D
		H5::H5File file(_TEST_DATA, H5F_ACC_RDONLY);
		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		auto GetResolutionAttrValue = [group](std::string_view name) -> float {
			float res;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_FLOAT, &res);
			sizeAttr.close();

			return res;
		};

		double res[3];
		res[0] = GetResolutionAttrValue("ResolutionX");
		res[1] = GetResolutionAttrValue("ResolutionY");
		res[2] = GetResolutionAttrValue("ResolutionZ");

		auto GetSizeAttrValue = [group](std::string_view name) -> int64_t {
			int64_t size;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_INT64, &size);
			sizeAttr.close();

			return size;
		};

		int size[3];
		size[0] = GetSizeAttrValue("SizeX");
		size[1] = GetSizeAttrValue("SizeY");
		size[2] = GetSizeAttrValue("SizeZ");

		auto [min, max] = [group]() -> std::tuple<double, double> {
			float min, max;

			auto minAttr = group.openAttribute("RIMin");
			minAttr.read(H5::PredType::NATIVE_FLOAT, &min);
			minAttr.close();

			auto maxAttr = group.openAttribute("RIMax");
			maxAttr.read(H5::PredType::NATIVE_FLOAT, &max);
			maxAttr.close();

			return {min, max};
		}();

		std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]], std::default_delete<uint16_t[]>());
		dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
		dataSet.close();
		group.close();
		file.close();

		// get a center slice image
		Vector3d spacing{ res[0], res[1], res[2] };
		Vector3d origin{ -size[0] * res[0] / 2, -size[1] * res[1] / 2, -size[2] * res[2] / 2 };

		const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty(origin, spacing);

		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

		const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]), static_cast<uint64_t>(size[2]) };
		const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		imagedev::setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
		imagedev::setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

		const RegionXu64 imageRegion{{0, 0, 0}, imageShape};
		imageView->writeRegion(imageRegion, data.get());

		TCDataConverter converter;
		image = converter.ImageViewToImage(imageView);
		image->SetMinMax(min, max);

	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	} catch (...) {
		FAIL("Unknown error");
	}

	return image;
}

auto Extract2DMask(const TCMask::Pointer mask) -> TCMask::Pointer {
	// mask 볼륨에서 z-center slice만 TCMask로 추출
	const auto [resX, resY, resZ] = mask->GetResolution();
	const auto [dimX, dimY, dimZ] = mask->GetSize();
	const auto sliceIndex = dimZ / 2;

	const auto originBuffer = mask->GetMaskVolume().get();
	std::shared_ptr<unsigned short> buffer(new unsigned short[dimX * dimY](), std::default_delete<unsigned short>());
	std::copy_n(originBuffer + (dimX * dimY * sliceIndex), dimX * dimY, buffer.get());

	int resultDims[3] = {dimX, dimY, 1};
	double resultRes[3] = {resX, resY, resZ};

	TCDataConverter converter;
	const auto sliceMask = converter.ArrToLabelMask(buffer.get(), resultDims, resultRes);
	sliceMask->SetResolution(resultRes);
	sliceMask->SetSize(resultDims[0], resultDims[1], resultDims[2]);
	
	return sliceMask;
}

auto Save(const TCMask::Pointer mask, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.MaskToImageView(mask);
	ioformat::writeView(imageview, savePath.toStdString());
}

TEST_CASE("ReLabeling") {
	REQUIRE(QFile::exists(_TEST_DATA));

	try {
		imagedev::init();
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	// load plugins
	QStringList plugins{
		QString("%1/segmentation/TC.Algorithm.Segmentation.Threshold.dll").arg(_PLUGIN_DIR),
		QString("%1/segmentation/TC.Algorithm.Segmentation.ManualThreshold.dll").arg(_PLUGIN_DIR),
		QString("%1/labeling/TC.Algorithm.Labeling.ConnectedComponent.2D.dll").arg(_PLUGIN_DIR),
		QString("%1/labeling/TC.Algorithm.Labeling.ConnectedComponent.3D.dll").arg(_PLUGIN_DIR),
		QString("%1/labeling/TC.Algorithm.Labeling.Relabeling.dll").arg(_PLUGIN_DIR),
	};

	for (auto& plugin : plugins) {
		REQUIRE(PluginRegistry::LoadPlugin(plugin) > 0);
	}

	TCMask::Pointer mask{nullptr};

	// get a center slice from mask volume
	try {
		const auto ht = LoadHT();

		// get upper and lower threshold
		const auto autoThresholdModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[0], true));

		const auto autoThresholdParam = autoThresholdModule->Parameter();
		autoThresholdParam->SetValue("Method", IParameter::ValueType(true));

		autoThresholdModule->SetInput(0, ht);
		autoThresholdModule->Execute();

		const auto autoThresholdResult = std::dynamic_pointer_cast<TC::Framework::DataList>(autoThresholdModule->GetOutput(0));
		const auto ulThreshold = std::dynamic_pointer_cast<DataSet>(autoThresholdResult->GetData(0));

		const auto lower = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("LThreshold"))->ValueAsDouble();
		const auto upper = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("UThreshold"))->ValueAsDouble();

		// get binary mask volume
		const auto thresholdingModuleInstance = PluginRegistry::GetPlugin(plugins[1], true);
		const auto thresholdingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(thresholdingModuleInstance);

		const auto thresholdingParam = thresholdingModuleInstance->Parameter();
		thresholdingParam->SetValue("LThreshold", IParameter::ValueType(lower));
		thresholdingParam->SetValue("UThreshold", IParameter::ValueType(upper));

		thresholdingModule->SetInput(0, ht);
		thresholdingModule->Execute();

		mask = std::dynamic_pointer_cast<TCMask>(thresholdingModule->GetOutput(0));

	} catch (std::exception& e) {
		std::cout << e.what() << std::endl;
		FAIL(e.what());
	} catch (H5::Exception& e) {
		std::cout << e.getDetailMsg() << std::endl;
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		std::cout << e.what() << std::endl;
		FAIL(e.what().c_str());
	} catch (...) {
		std::cout << "Unknown error" << std::endl;
		FAIL("Unknown error");
	}

	const auto relabelingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[4], true));

	auto PrintLabelInfo = [](TCMask::Pointer label) {
		auto blobIndexes = label->GetBlobIndexes();
		auto [min, max] = std::minmax_element(blobIndexes.begin(), blobIndexes.end());

		std::cout << "labels: " << label->GetBlobIndexes().size() << ", min: " << *min << ", max: " << *max << std::endl;
	};

	SECTION("2D") {
		const auto sliceMask = Extract2DMask(mask);
		REQUIRE(sliceMask != nullptr);

		const auto labelingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[2], true));
		labelingModule->SetInput(0, sliceMask);
		REQUIRE(labelingModule->Execute());

		const auto label = std::dynamic_pointer_cast<TCMask>(labelingModule->GetOutput(0));

		relabelingModule->SetInput(0, label);
		REQUIRE(relabelingModule->Execute());

		const auto output = std::dynamic_pointer_cast<TCMask>(relabelingModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(label, "input_2d");
		std::cout << "2D input - ";
		PrintLabelInfo(label);

		Save(output, "output_2d");
		std::cout << "2D output - ";
		PrintLabelInfo(output);
	}

	SECTION("3D") {
		const auto labelingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugins[3], true));
		labelingModule->SetInput(0, mask);
		REQUIRE(labelingModule->Execute());

		const auto label = std::dynamic_pointer_cast<TCMask>(labelingModule->GetOutput(0));

		relabelingModule->SetInput(0, label);
		REQUIRE(relabelingModule->Execute());

		const auto output = std::dynamic_pointer_cast<TCMask>(relabelingModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(label, "input_3d");
		std::cout << "3D input - ";
		PrintLabelInfo(label);

		Save(output, "output_3d");
		std::cout << "3D output - ";
		PrintLabelInfo(output);
	}

	imagedev::finish();
}
