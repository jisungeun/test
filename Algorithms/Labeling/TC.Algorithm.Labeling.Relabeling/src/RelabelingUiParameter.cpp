#include <ParameterRegistry.h>
#include "RelabelingUiParameter.h"

namespace TC::Algorithm::Labeling::Relabeling {
    auto UiParameter::Register() -> void {
        static bool s_registred = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}