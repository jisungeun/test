#define LOGGER_TAG "[Relabeling]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCDataConverter.h>

#include "RelabelingUiParameter.h"
#include "RelabelingParameter.h"
#include "Relabeling.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Labeling::Relabeling {
    struct Algorithm::Impl {
        TCMask::Pointer input{ nullptr };
		TCMask::Pointer output{ nullptr };

        IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.filtering.relabeling") };
        IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.filtering.relabeling") };
        QMap<QString, IParameter::Pointer> params;
    };

    Algorithm::Algorithm() : d{ new Impl } {
        Parameter::Register();
        UiParameter::Register();
    }

    Algorithm::~Algorithm() {
        
    }

    auto Algorithm::clone() const -> IPluginModule* {
		return new Algorithm();
	}

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

		d->input = std::dynamic_pointer_cast<TCMask>(data);
		
		return true;
	}

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
		return d->output;
	}

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return {d->output};
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
			if (d->params.contains(key)) {
				return d->params[key];
			} else {
				return nullptr;
			}
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {

	}

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
	}

    auto Algorithm::Execute() -> bool {
        QLOG_INFO() << "START Relabeling";

		bool succeed = false;		

		try {
			if (d->input == nullptr) throw std::runtime_error("Invalid input data\n");

			QElapsedTimer etimer;
			etimer.start();

			TCDataConverter converter;
			const auto inputLabel = converter.MaskToImageView(d->input);

			const auto result = reorderLabels(inputLabel);

			auto shape = result->shape();
			int dim[3] = {shape[0], shape[1], shape[2]};

			auto spacing = getCalibrationSpacing(result);
			double res[3] = {spacing[0], spacing[1], spacing[2]};

			d->output = converter.ArrToLabelMask((unsigned short*)result->buffer(), dim, res);
			d->output->SetOffset(d->input->GetOffset());

			QLOG_INFO() << QString("FINISH Relabeling - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
			QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
    }

}