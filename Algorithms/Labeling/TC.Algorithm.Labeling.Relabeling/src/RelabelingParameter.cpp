#include <QJsonArray>

#include <ParameterRegistry.h>

#include "RelabelingParameter.h"

namespace TC::Algorithm::Labeling::Relabeling {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(GetName(), CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");
    }
}