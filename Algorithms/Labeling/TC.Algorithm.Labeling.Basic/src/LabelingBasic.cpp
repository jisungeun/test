#define LOGGER_TAG "[LabelingBasic]"
#include <TCLogger.h>

#include <iostream>

#pragma warning(push)
#pragma warning(disable:4819)
//inventor engine for automatic otsu threshold
#include <ImageViz/SoImageViz.h>
#include <ImageViz/Engines/SoImageVizEngineHeaders.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Measures/SoDataMeasurePredefined.h>
#include <Medical/helpers/MedicalHelper.h>
#include <ImageViz/Engines/ImageSegmentation/SeparatingAndFilling/SoSeparateObjectsProcessing.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#include <Inventor/image/SoTIFFImageRW.h>
#pragma warning(pop)

#include <ParameterRegistry.h>


#include <TCDataConverter.h>
#include <TCImage.h>
#include <TCMask.h>
#include <OIVMutex.h>

#include "LabelingBasicUiParameter.h"
#include "LabelingBasicParameter.h"
#include "LabelingBasic.h"


namespace TC::Algorithm::Labeling::Basic {
	struct Algorithm::Impl {		
		TCMask::Pointer input;
		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.labeling.basic") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.labeling.basic") };
		TCMask::Pointer result;		
		QMap<QString,QStringList> layer_names;		
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
		d->layer_names["CellInst"] = QStringList();
		d->layer_names["CellInst"].push_back("cellInst");
	}

	Algorithm::~Algorithm() {
	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->input = std::dynamic_pointer_cast<TCMask>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		if(key == "Method!Enabler") {
			return std::make_tuple("Method", param->GetValue("Method!Enabler"));
		}
		if(key == "Method!Setter") {
			return std::make_tuple("AlgoSelection", param->GetValue("Method!Setter"));
		}
		if(key == "Index") {
			return std::make_tuple("Index", param->GetValue("Index"));
		}
		if(key =="Particle Filter Size") {
			return std::make_tuple("Particle Filter Size", param->GetValue("Particle Filter Size"));
		}
		return std::make_tuple(QString(), QJsonValue());
    }

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
		Q_UNUSED(keys)
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		Q_UNUSED(key)
		return d->param;
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }

	auto Algorithm::Execute()->bool {		
		auto method = d->param->GetValue("Index").toInt();		
		//no parameter required for now		

		auto size = d->param->GetValue("Particle Filter Size").toDouble();		

		if(!d->input->IsValid()) {
			d->result = d->input;
		    return true;
		}

		//IO::OIVMutexLocker lock(IO::OIVMutex::GetInstance());

		auto converter = std::shared_ptr<TCDataConverter>(new TCDataConverter, std::default_delete<TCDataConverter>());
		SoRef<SoVolumeData> binaryMask = converter->MaskToSoVolumeData(d->input);		
		if (nullptr == binaryMask.ptr()) {
			QLOG_ERROR() << "binary mask null";
			return false;
		}
		//binaryMask->ref();
		////MedicalHelper::dicomAdjustVolume(binaryMask.ptr());
		SoRef<SoMemoryDataAdapter> imageAdapter = MedicalHelper::getImageDataAdapter(binaryMask.ptr());
		//imageAdapter->ref();		
		imageAdapter->interpretation = SoMemoryDataAdapter::BINARY;

		SoRef<SoLabelingProcessing> labeling = new SoLabelingProcessing;
		//labeling->ref();
		labeling->inObjectImage = imageAdapter.ptr();
		labeling->computeMode = SoLabelingProcessing::ComputeMode::MODE_3D;
		labeling->neighborhood3d = SoSeparateObjectsProcessing::Neighborhood3d::CONNECTIVITY_6;

		SoRef<SoMeasureImageProcessing> imageToSize = new SoMeasureImageProcessing;
		//imageToSize->ref();
		imageToSize->inIntensityImage = imageAdapter.ptr();
		imageToSize->inLabelImage.connectFrom(&labeling->outLabelImage);
		imageToSize->measure.setValue(new SoDataMeasurePredefined(SoDataMeasurePredefined::VOLUME_3D));
		
		QStringList inst_name{ "cellInst" };

		if (method == 1) { //single label			
			SoRef<SoVRImageDataReader> inputImageReader = new SoVRImageDataReader;
			//inputImageReader->ref();
			inputImageReader->imageData.connectFrom(&imageToSize->outMeasureImage);

			SoRef<SoVolumeData> labeled_data = new SoVolumeData;
			//labeled_data->ref();
			labeled_data->setReader(*inputImageReader,TRUE);

			double label_min, label_max;

			labeled_data->getMinMax(label_min, label_max);

			SoRef<SoThresholdingProcessing> thresholding = new SoThresholdingProcessing;
			//thresholding->ref();
			thresholding->inImage.connectFrom(&imageToSize->outMeasureImage);
			thresholding->thresholdLevel.setValue(label_max - 0.5, label_max + 0.5);

			SoRef<SoConvertImageProcessing> convert = new SoConvertImageProcessing;
			//convert->ref();
			convert->inImage.connectFrom(&thresholding->outBinaryImage);
			convert->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

			SoRef<SoVRImageDataReader> outSingleReader = new SoVRImageDataReader;
			//outSingleReader->ref();
			outSingleReader->imageData.connectFrom(&convert->outImage);

			SoRef<SoVolumeData> thresh_data = new SoVolumeData;
			//thresh_data->ref();
			thresh_data->setReader(*outSingleReader,TRUE);
			double min, max;

			thresh_data->getMinMax(min, max);
			auto valid = (max > 0);

			QList<SoVolumeData*> mask_result;
			mask_result.push_back(thresh_data.ptr());

			d->result = converter->SoVolumeDataToMask(mask_result, MaskTypeEnum::MultiLabel, inst_name);			
			d->result->SetName("CellInst");
			d->result->SetValid(valid);

			/*
			while (thresh_data->getRefCount()>1) {
				thresh_data->unref();
			}
			thresh_data = nullptr;
			while (outSingleReader->getRefCount()>1) {
				outSingleReader->unref();
			}
			outSingleReader = nullptr;
			while (convert->getRefCount()>1) {
				convert->unref();
			}
			convert = nullptr;
			while (thresholding->getRefCount()>1) {
				thresholding->unref();
			}			
			thresholding = nullptr;
			while (labeled_data->getRefCount()>1) {
				labeled_data->unref();
			}
			labeled_data = nullptr;
			while (inputImageReader->getRefCount()>1) {
				inputImageReader->unref();
			}
			inputImageReader = nullptr;
			for(auto m: mask_result) {
				m = nullptr;
			}			
			mask_result.clear();
			*/
		}
		else if (method == 0) {//multi label			
			SoRef<SoThresholdingProcessing> thresholding = new SoThresholdingProcessing;
			//thresholding->ref();
			thresholding->inImage.connectFrom(&imageToSize->outMeasureImage);
			thresholding->thresholdLevel.setValue(size, static_cast<float>(INT_MAX));
			
			SoRef<SoLabelingProcessing> relabeling = new SoLabelingProcessing;
			//relabeling->ref();
			relabeling->inObjectImage.connectFrom(&thresholding->outBinaryImage);
			relabeling->computeMode.setValue(SoLabelingProcessing::ComputeMode::MODE_3D);
			relabeling->neighborhood3d.setValue(SoSeparateObjectsProcessing::Neighborhood3d::CONNECTIVITY_6);

			SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
			//reader->ref();
			reader->imageData.connectFrom(&relabeling->outLabelImage);			

			SoRef<SoVolumeData> labeled_data = new SoVolumeData;
			//labeled_data->ref();
			labeled_data->setReader(*reader,TRUE);											

			double min, max;
			labeled_data->getMinMax(min, max);			
			auto valid = (max > 0);

			QList<SoVolumeData*> mask_result;
			mask_result.push_back(labeled_data.ptr());
						
			d->result = converter->SoVolumeDataToMask(mask_result, MaskTypeEnum::MultiLabel, inst_name);
			d->result->SetName("CellInst");
			d->result->SetValid(valid);
			/*
			while (labeled_data->getRefCount() > 0) {
				labeled_data->unref();
			}			
			labeled_data = nullptr;
			while (reader->getRefCount()>0) {
				reader->unref();
			}			
			reader = nullptr;
			while (relabeling->getRefCount()>0) {
				relabeling->unref();
			}			
			relabeling = nullptr;
			while (thresholding->getRefCount()>0) {
				thresholding->unref();
			}			
			thresholding = nullptr;
			for(auto m : mask_result) {
				m = nullptr;
			}
			mask_result.clear();*/
		}
		/*
		while(imageToSize->getRefCount()>0) {
			imageToSize->unref();			
		}		
		imageToSize = nullptr;
		while (labeling->getRefCount()>0) {
			labeling->unref();
		}		
		labeling = nullptr;
		while (imageAdapter->getRefCount()>0) {
			imageAdapter->unref();
		}		
		imageAdapter = nullptr;
		while (binaryMask->getRefCount()>0) {
			binaryMask->unref();
		}		
		binaryMask = nullptr;
		*/
		//delete converter;
		converter = nullptr;

		return true;
	}
}