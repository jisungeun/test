#include <ParameterRegistry.h>
#include "LabelingBasicParameter.h"

namespace TC::Algorithm::Labeling::Basic {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");
		RegisterNode("Method", "Use Labeling", "Use connected component labeling", "TFCheck", false, "", "");
		RegisterNode("AlgoSelection", "Select Option\n0: Multi Labels 1: Largest Label", "Select automatic labeling algorithm", "SelectOption", 0, 0, 1);
		RegisterNode("Index", "Select Option\n0: Multi Labels 1: Largest Label", "Select automatic labeling algorithm", "ScalarValue.int", 0, 0, 1);
		RegisterNode("Particle Filter Size", "Assign Size of Neglectable Particle", "Size of Neglectable Particle will be ignored",
			"ScalarValue.double", 25.0, 0.0, 100.0);
	}

}
