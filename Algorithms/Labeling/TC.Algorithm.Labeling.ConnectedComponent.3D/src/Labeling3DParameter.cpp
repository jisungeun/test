#include <ParameterRegistry.h>
#include "Labeling3DParameter.h"

namespace TC::Algorithm::Labeling::ConnectedComponent {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");
		
		RegisterNode(
			"LabelType",
			"Label data type",
			"The minimum output data type.\n0: 8bit label\n1: 16bit label\n2: 32bit label",
			"ScalarValue.int",
			0, 2, 1
		);

		RegisterNode(
			"Neighborhood",
			"Neighborhood configuration",
			"The 3D neighborhood configuration defining the connected components.\n0: 6-neighborhood connectivity\n1: 18-neighborhood connectivity\n2: 26-neighborhood connectivity",
			"ScalarValue.int",
			0, 2, 2
		);
	}

}
