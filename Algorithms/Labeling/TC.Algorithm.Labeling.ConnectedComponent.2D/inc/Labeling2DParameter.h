#pragma once

#include <IParameter.h>

namespace TC::Algorithm::Labeling::ConnectedComponent {
	class Parameter : public IParameter {
		Q_OBJECT

	public:
		static auto Register()->void;

		static IParameter::Pointer CreateMethod() {
			return std::make_shared<Parameter>();
		}

		static std::string GetName() { return "org.tomocube.algorithm.labeling.connectedcomponent.2d"; }

		Parameter();
	};
}