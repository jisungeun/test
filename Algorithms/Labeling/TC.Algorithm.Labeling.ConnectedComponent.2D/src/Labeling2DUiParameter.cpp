#include <ParameterRegistry.h>
#include "Labeling2DUiParameter.h"

namespace TC::Algorithm::Labeling::ConnectedComponent {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterEnabler("Method", "Use Labeling2D", QStringList{ "AlgoSelection", "Particle Filter Size" }, QStringList());
        RegisterHider("Hide1", QStringList{ "Index" });
        RegisterSetter("AlgoSelection","Select Labeling Option", QStringList{"Index"}, QStringList{"Multi Labels", "Largest Label"});
        AppendSetter("AlgoSelection", "Index", "Multi Labels", 0);
        AppendSetter("AlgoSelection", "Index", "Largest Label", 1);
        RegisterSorter("Order", QStringList{ "Method","AlgoSelection","Particle Filter Size","Index"});
    }
}