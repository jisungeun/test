#define LOGGER_TAG "[Labeling2D]"
#include <TCLogger.h>

#include <iostream>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCMask.h>
#include <TCDataConverter.h>

#include "Labeling2DUiParameter.h"
#include "Labeling2DParameter.h"
#include "Labeling2D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Labeling::ConnectedComponent {
	struct Algorithm::Impl {		
		TCMask::Pointer input;
		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.labeling.connectedcomponent.2d") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.labeling.connectedcomponent.2d") };
		TCMask::Pointer result;		
		QMap<QString,QStringList> layer_names;		
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
		d->layer_names["CellInst"] = QStringList();
		d->layer_names["CellInst"].push_back("cellInst");
	}

	Algorithm::~Algorithm() {
	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->input = std::dynamic_pointer_cast<TCMask>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		if(key == "Method!Enabler") {
			return std::make_tuple("Method", param->GetValue("Method!Enabler"));
		}
		if(key == "Method!Setter") {
			return std::make_tuple("AlgoSelection", param->GetValue("Method!Setter"));
		}
		if(key == "Index") {
			return std::make_tuple("Index", param->GetValue("Index"));
		}
		if(key =="Particle Filter Size") {
			return std::make_tuple("Particle Filter Size", param->GetValue("Particle Filter Size"));
		}
		return std::make_tuple(QString(), QJsonValue());
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		Q_UNUSED(key)
		return d->param;
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        Q_UNUSED(keys)
    }

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }

	auto Algorithm::Execute()->bool {
		if(!d->input->IsValid()) {
			d->result = d->input;
		    return true;
		}

		const auto labelType = static_cast<Labeling2d::LabelType>(d->param->GetValue("LabelType").toInt(1));
		const auto neighborhood = static_cast<Labeling2d::Neighborhood>(d->param->GetValue("Neighborhood").toInt(1));

		try {
	        TCDataConverter converter;
			const auto inputMask = converter.MaskToImageView(d->input);

			Labeling2d labeling2d;
	        labeling2d.setInputObjectImage(inputMask);
	        labeling2d.setLabelType(labelType);
	        labeling2d.setNeighborhood(neighborhood);

	        labeling2d.execute();

	        // convert label image to TCMask
	        const auto labelImage = labeling2d.outputLabelImage();

			auto [x, y, z] = d->input->GetSize();
			auto [resolutionX, resolutionY, resolutionZ] = d->input->GetResolution();

		    int dim[3] = {x, y, 1};
	        double res[3] = {resolutionX, resolutionY, resolutionZ};

	        d->result = converter.ArrToLabelMask(static_cast<unsigned short*>(labelImage->buffer()), dim, res);

		} catch(imagedev::Exception& e) {
		    std::cout << e.what() << std::endl;
		}

		return true;
	}
}