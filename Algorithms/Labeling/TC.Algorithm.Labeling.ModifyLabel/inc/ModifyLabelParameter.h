#pragma once

#include <IParameter.h>

namespace TC::Algorithm::Labeling::ModifyLabel {
    class Parameter : public IParameter {
        Q_OBJECT
    public:
        static auto Register()->void;

        static Pointer CreateMethod() {
            return std::make_shared<Parameter>();
        }

        static std::string GetName() { return "org.tomocube.algorithm.labeling.modification"; }

        Parameter();
    };
}