#pragma once

#include <IUiParameter.h>

namespace TC::Algorithm::Labeling::ModifyLabel {
    class UiParameter : public IUiParameter {
        Q_OBJECT
    public:
        static auto Register()->void;
        static auto CreateMethod()->Pointer {
            return std::make_shared<UiParameter>();
        }
        static auto GetName()->std::string { return "org.tomocube.algorithm.labeling.modification"; }
        UiParameter();
    };
}