#include <ParameterRegistry.h>
#include "ModifyLabelUiParameter.h"

namespace TC::Algorithm::Labeling::ModifyLabel {
    auto UiParameter::Register() -> void {
        static bool s_registred = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
        //Ui Parameter nodes
        RegisterHider("Hide1", QStringList{ "Translation","Scale"});
        RegisterSorter("Order", QStringList{ "Translation","Scale" });
    }
}