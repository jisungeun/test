#include <QJsonArray>

#include <ParameterRegistry.h>

#include "ModifyLabelParameter.h"

namespace TC::Algorithm::Labeling::ModifyLabel {
    auto Parameter::Register() -> void {
        static bool s_registered = ParameterRegistry::Register(GetName(), CreateMethod);
    }
    Parameter::Parameter() : IParameter() {
        SetVersion("1.0.0");
        //Parameter nodes
        RegisterNode("Translation", "Label translation", "Manual translation of each label", "ScalarValue.Coordinate",QJsonArray(), 0, 0);
        RegisterNode("Scale", "Label scale", "Morphology based scale of each label", "ScalarValue.int", QJsonArray(), 0, 0);
    }
}