#define LOGGER_TAG "[ModifyLabel]"
#include <TCLogger.h>

#include <iostream>
#include <QJsonArray>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoCombineByMaskProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalNotProcessing.h>
#include <ImageViz/Engines/GeometryAndMatching/GeometricTransforms/SoTranslateProcessing.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoDilationBallProcessing3d.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoErosionBallProcessing3d.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoResetImageProcessing.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include <ParameterRegistry.h>

#include <TCDataConverter.h>
#include <TCMask.h>

#include "ModifyLabelUiParameter.h"
#include "ModifyLabelParameter.h"
#include "ModifyLabel.h"

namespace TC::Algorithm::Labeling::ModifyLabel {
    struct Algorithm::Impl {
        TCMask::Pointer input;
        IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.labeling.modification") };
        IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.labeling.modification") };
        TCMask::Pointer result;
        QMap<QString, QStringList> layer_names;

        QMap<int, std::tuple<double, double, double>> transFactor;
        QMap<int, int> scaleFactor;
        int maxLabel{ -1 };
    };

    Algorithm::Algorithm() : d{ new Impl } {
        Parameter::Register();
        UiParameter::Register();
        d->layer_names["CellInst"] = QStringList();
        d->layer_names["CellInst"].push_back("flLabel");
    }
    Algorithm::~Algorithm() {
        
    }

    auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->input = std::dynamic_pointer_cast<TCMask>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

    auto Algorithm::ParamConverter(IParameter::Pointer, QString key) -> std::tuple<QString, QJsonValue> {
        return std::make_tuple(QString(), QJsonValue());
    }
    
    auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        Q_UNUSED(keys)
    }

    auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
        Q_UNUSED(key)
        return d->param;
    }

    auto Algorithm::UiParameter() -> IUiParameter::Pointer {
        return d->uiParam;
    }

    auto Algorithm::Execute() -> bool {
        //perform translation
        auto transParam = d->param->GetValue("Translation").toArray();
        auto paramCnt = transParam.count();
        if(d->input->CountBlobs() < paramCnt) {
            QLOG_DEBUG() << "# of translation parameter exceeds # of labels";
            return false;
        }
        auto scaleParam = d->param->GetValue("Scale").toArray();
        paramCnt = scaleParam.count();
        if(d->input->CountBlobs() < paramCnt) {
            QLOG_DEBUG() << "# of scale parameter exceeds # of labels";
            return false;
        }

        d->transFactor.clear();
        d->scaleFactor.clear();
        d->maxLabel = -1;

        auto converter = std::make_shared<TCDataConverter>();
        SoRef<SoVolumeData> volData = converter->MaskToSoVolumeData(d->input);                

        try {            
            for (auto trans : transParam) {
                auto labelValue = trans.toArray()[0].toInt();
                auto coordX = trans.toArray()[1].toDouble();
                auto coordY = trans.toArray()[2].toDouble();
                auto coordZ = trans.toArray()[3].toDouble();
                //append translation factor
                d->transFactor[labelValue] = std::make_tuple(coordX, coordY,coordZ);
                if(labelValue > d->maxLabel) {
                    d->maxLabel = labelValue;
                }
            }
            for (auto scale : scaleParam) {
                //Force Erosion & Dilation start from original state
                auto labelValue = scale.toArray()[0].toInt();
                auto scaleFactor = scale.toArray()[1].toInt();
                d->scaleFactor[labelValue] = scaleFactor;
                if (labelValue > d->maxLabel) {
                    d->maxLabel = labelValue;
                }
            }
            for(auto i=1;i<d->maxLabel+1;i++) {
                //skip no changes
                if(!d->scaleFactor.contains(i) && !d->transFactor.contains(i)) {
                    continue;
                }
                //MedicalHelper::dicomAdjustVolume(volData.ptr());
                SoRef<SoMemoryDataAdapter> imageAdapter = MedicalHelper::getImageDataAdapter(volData.ptr());
                //imageAdapter->ref();
                //make binary mask with given label                                
                SoRef<SoThresholdingProcessing> threshold = new SoThresholdingProcessing;
                //threshold->ref();
                threshold->inImage = imageAdapter.ptr();
                threshold->thresholdLevel.setValue(static_cast<float>(i), static_cast<float>(i) + 0.5f);

                //mask outer image
                SoRef<SoLogicalNotProcessing> not = new SoLogicalNotProcessing;
                //not->ref();
                not->inImage.connectFrom(&threshold->outBinaryImage);                

                SoRef<SoMaskImageProcessing> masking = new SoMaskImageProcessing;
                //masking->ref();                
                masking->inImage = imageAdapter.ptr();
                masking->inBinaryImage.connectFrom(&not->outImage);                

                SoRef<SoTranslateProcessing> translate{nullptr};
                SoRef<SoDilationBallProcessing3d> dilate{ nullptr };
                SoRef<SoErosionBallProcessing3d> erode{ nullptr };

                if(d->transFactor.contains(i)) {
                    //perform translation
                    auto transX = static_cast<float>(std::get<0>(d->transFactor[i]));
                    auto transY = static_cast<float>(std::get<1>(d->transFactor[i]));
                    auto transZ = static_cast<float>(std::get<2>(d->transFactor[i]));

                    translate = new SoTranslateProcessing;
                    //translate->ref();
                    translate->inImage.connectFrom(&threshold->outBinaryImage);
                    translate->backgroundMode = SoTranslateProcessing::FIXED_BACKGROUND;
                    translate->backgroundValue = 0.f;
                    translate->translationVector.setValue(SbVec3f(transX, transY, transZ)); 
                }                                

                if(d->scaleFactor.contains(i)) {
                    //perform scale
                    auto scale = d->scaleFactor[i];
                    if(scale > 0) {//dilation
                        dilate = new SoDilationBallProcessing3d;
                        dilate->elementSize.setValue(scale);
                        if (nullptr != translate.ptr()) {
                            dilate->inImage.connectFrom(&translate->outImage);
                        }else {
                            dilate->inImage.connectFrom(&threshold->outBinaryImage);
                        }
                    }else {//erosion
                        erode = new SoErosionBallProcessing3d;
                        erode->elementSize.setValue(-scale);
                        if(nullptr != translate.ptr()) {
                            erode->inImage.connectFrom(&translate->outImage);
                        }else {
                            erode->inImage.connectFrom(&threshold->outBinaryImage);
                        }
                    }
                }

                //merge modified label with original label value
                SoRef<SoResetImageProcessing> reset = new SoResetImageProcessing;
                //reset->ref();
                reset->inImage = imageAdapter.ptr();
                reset->intensityValue.setValue(i);

                SoRef<SoCombineByMaskProcessing> combine = new SoCombineByMaskProcessing;
                //combine->ref();
                combine->inImage1.connectFrom(&reset->outImage);
                combine->inImage2.connectFrom(&masking->outImage);
                if(nullptr != erode.ptr()) {
                    combine->inBinaryImage.connectFrom(&erode->outImage);
                }else if(nullptr != dilate.ptr()) {
                    combine->inBinaryImage.connectFrom(&dilate->outImage);
                }else {
                    combine->inBinaryImage.connectFrom(&translate->outImage);
                }

                SoRef<SoConvertImageProcessing> convert = new SoConvertImageProcessing;
                //convert->ref();
                convert->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;
                convert->inImage.connectFrom(&combine->outImage);

                SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
                //reader->ref();
                reader->imageData.connectFrom(&convert->outImage);

                SoRef<SoVolumeData> tempVolume = new SoVolumeData;
                //tempVolume->ref();
                tempVolume->setReader(*reader, TRUE);

                //copy value to original volume
                volData->data.copyFrom(tempVolume->data);

                //free temporal memory
                /*
                while(tempVolume->getRefCount()>0) {
                    tempVolume->unref();
                }
                tempVolume = nullptr;
                while (reader->getRefCount() > 0) {
                    reader->unref();
                }
                reader = nullptr;
                while(convert->getRefCount()>0) {
                    convert->unref();
                }
                convert = nullptr;
                while(combine->getRefCount()>0) {
                    combine->unref();
                }
                combine = nullptr;
                while(reset->getRefCount()>0) {
                    reset->unref();
                }
                reset = nullptr;
                if(erode.ptr() != nullptr) {
                    while(erode->getRefCount()>0) {
                        erode->unref();
                    }
                    erode = nullptr;
                }
                if(dilate.ptr() != nullptr) {
                    while(dilate->getRefCount()>0) {
                        dilate->unref();
                    }
                    dilate = nullptr;
                }
                if(translate.ptr()!=nullptr) {
                    while(translate->getRefCount()>0) {
                        translate->unref();
                    }
                    translate = nullptr;
                }
                while(masking->getRefCount()>0) {
                    masking->unref();
                }
                masking = nullptr;
                while(not->getRefCount()>0) {
                    not->unref();
                }
                not = nullptr;
                while(threshold->getRefCount()>0) {
                    threshold->unref();
                }
                threshold = nullptr;                
                while (imageAdapter->getRefCount() > 0) {
                    imageAdapter->unref();
                }
                imageAdapter = nullptr;
                */
            }
            //save using volumeData

            QList<SoVolumeData*> fl_result;
            fl_result.push_back(volData.ptr());

            QStringList inst_name = { "flmask" };
            d->result = converter->SoVolumeDataToMask(fl_result, MaskTypeEnum::MultiLabel, inst_name);
            d->result->SetName("FLMask");
            d->result->SetValid(true);

            //free global memory
            /*
            while (volData->getRefCount() > 0) {
                volData->unref();
            }
            volData = nullptr;

            fl_result.clear();
            */
        }catch(...) {
            QLOG_DEBUG() << "Exception during perform label modification";            
            return false;
        }                

        converter = nullptr;                

        return true;
    }
}
