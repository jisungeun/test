#include <ParameterRegistry.h>
#include "LaplacianOperator25UiParameter.h"

namespace TC::Algorithm::EdgeDetection::Laplacian25D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterHider("Hide1", QStringList{ "PrecisionIndex"});

        RegisterSetter("Precision","Select precision", QStringList{"PrecisionIndex"}, QStringList{"FASTER","PRECISE"});
        AppendSetter("Precision", "PrecisionIndex", "FASTER",      0);
        AppendSetter("Precision", "PrecisionIndex", "PRECISE", 1);                
    }
}