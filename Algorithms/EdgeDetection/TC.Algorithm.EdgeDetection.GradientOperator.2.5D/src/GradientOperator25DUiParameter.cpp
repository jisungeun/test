#include <ParameterRegistry.h>
#include "GradientOperator25DUiParameter.h"

namespace TC::Algorithm::EdgeDetection::GradientOperator25D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterHider("Hide1", QStringList{ "GradientOperatorIndex", "GradientModeIndex" });

        RegisterSetter("GradientOperator","Select operator", QStringList{"GradientOperatorIndex"}, QStringList{"Canny-Deriche", "Shen and Castan", "Sobel 3x3", "Canny", "Gaussian", "Sobel", "Prewitt"});
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Canny-Deriche",      0);
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Shen and Castan",    1);
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Sobel 3x3",          2);
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Canny",              3);
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Gaussian",           4);
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Sobel",              5);
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Prewitt",            6);

        RegisterSetter("GradientMode","Select mode", QStringList{"GradientModeIndex"}, QStringList{"Maximum amplitude", "Euclidean amplitude", "Euclidean amplitude and orientation", "X and Y directions"});
        AppendSetter("GradientMode", "GradientModeIndex", "Maximum amplitude",                      0);
        AppendSetter("GradientMode", "GradientModeIndex", "Euclidean amplitude",                    1);
        AppendSetter("GradientMode", "GradientModeIndex", "Euclidean amplitude and orientation",    2);
        AppendSetter("GradientMode", "GradientModeIndex", "X and Y directions",                     3);

        RegisterSorter("Order", QStringList{ "GradientOperator", "GradientOperatorIndex", "GradientMode", "GradientMode", "SearchMode"});
    }
}