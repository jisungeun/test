#define LOGGER_TAG "[GradientOperator25D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataconverter.h>

#include "GradientOperator25DUiParameter.h"
#include "GradientOperator25DParameter.h"
#include "GradientOperator25D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::EdgeDetection::GradientOperator25D {
	struct Algorithm::Impl {		
		TCImage::Pointer refImage{ nullptr };
		TCImage::Pointer result{ nullptr };

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.edgedetection.gradientoperator.2.5d") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.edgedetection.gradientoperator.2.5d") };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->refImage = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    } else {
				return nullptr;
		    }
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }
	
	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
    }
	
	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START GradientOperator25D";

		const auto gradientOperator = static_cast<GradientOperator2d::GradientOperator>(d->param->GetValue("GradientOperator").toInt());
		const auto gradientMode = static_cast<GradientOperator2d::GradientMode>(d->param->GetValue("GradientMode").toInt());
		const auto smoothingFactor = d->param->GetValue("SmoothingFactor").toDouble();

	    try {
			int64_t totalTime = 0;
			int64_t elapsedTime = 0;
	        QElapsedTimer etimer;

	        etimer.start();

			TCDataConverter converter;
		    const auto refImageView = converter.ImageToImageView(d->refImage);

			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for data conversion TCImage to ImageView: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			etimer.restart();

			const auto [sizeX, sizeY, sizeZ] = d->refImage->GetSize();
			const auto [min, max] = d->refImage->GetMinMax();

			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0,1 });

			const bool isZeroPaded = imageStat->minimum() < min;

			auto destImage = convertImage(refImageView, ConvertImage::SIGNED_INTEGER_32_BIT);

			for(auto i=0;i<sizeZ;i++) {
				auto sliced = getSliceFromVolume3d(refImageView, GetSliceFromVolume3d::Z_AXIS, i);
				if(isZeroPaded) {
					const auto thersholded = thresholding(sliced, { -INT_MAX,static_cast<double>(min) });
					const auto reseted = resetImage(sliced, min);
					sliced = combineByMask(reseted, sliced, thersholded);
				}
				const auto rescaledSlice =rescaleIntensity(sliced, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, {0, 0} /*ignored*/, {0, 0} /*ignored*/, {0., 100.});
				const auto slice_grad = gradientOperator2d(rescaledSlice, gradientOperator, gradientMode, smoothingFactor).outputAmplitudeImage;
				destImage = setSliceToVolume3d(destImage, slice_grad, SetSliceToVolume3d::Z_AXIS, i);
			}

			const auto originStat = intensityStatistics(destImage, IntensityStatistics::MIN_MAX, { 0,1 });

			const auto rescaled = rescaleIntensity(destImage, RescaleIntensity::UNSIGNED_INTEGER_16_BIT, RescaleIntensity::MIN_MAX, { 1,1 }, { 1,1 }, { 13000,15000 });
			rescaled->setProperties(refImageView->properties());
			
			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for gradient operator: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			etimer.restart();

			d->result = converter.ImageViewToImage(rescaled);
			d->result->SetOriginalMinMax(static_cast<float>(originStat->minimum()), static_cast<float>(originStat->maximum()));
			d->result->SetMinMax(13000,15000);
			d->result->SetOffset(d->refImage->GetOffset());
			d->result->setChannel(d->refImage->getChannel());

			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for data conversion ImageView to TCImage: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			QLOG_INFO() << QString("FINISH GradientOperator25D - total: %1 ms").arg(totalTime);
		} catch (std::exception& e) {
		    QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return true;
	}
}
