#include <ParameterRegistry.h>
#include "GradientOperator3DUiParameter.h"

namespace TC::Algorithm::EdgeDetection::GradientOperator3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}