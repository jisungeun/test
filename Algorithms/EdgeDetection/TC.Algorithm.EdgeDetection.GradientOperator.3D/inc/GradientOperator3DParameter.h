#pragma once

#include <IParameter.h>

namespace TC::Algorithm::EdgeDetection::GradientOperator3D {
	class Parameter : public IParameter {
		Q_OBJECT

	public:
		static auto Register()->void;

		static IParameter::Pointer CreateMethod() {
			return std::make_shared<Parameter>();
		}

		static std::string GetName() { return "org.tomocube.algorithm.edgedetection.gradientoperator.3d"; }

		Parameter();
	};
}