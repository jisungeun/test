#define LOGGER_TAG "[GradientOperator2D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataconverter.h>

#include "GradientOperator2DUiParameter.h"
#include "GradientOperator2DParameter.h"
#include "GradientOperator2D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::EdgeDetection::GradientOperator {
	struct Algorithm::Impl {		
		TCImage::Pointer refImage{ nullptr };
		TCImage::Pointer result{ nullptr };

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.edgedetection.gradientoperator.2d") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.edgedetection.gradientoperator.2d") };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->refImage = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    } else {
				return nullptr;
		    }
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }
	
	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
    }
	
	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START GradientOperator2D";

		const auto gradientOperator = static_cast<GradientOperator2d::GradientOperator>(d->param->GetValue("GradientOperator").toInt());
		const auto gradientMode = static_cast<GradientOperator2d::GradientMode>(d->param->GetValue("GradientMode").toInt());
		const auto smoothingFactor = d->param->GetValue("SmoothingFactor").toDouble();

		bool succeed = false;
	    try {
	    	QElapsedTimer etimer;
	        etimer.start();

			TCDataConverter converter;
		    auto refImageView = converter.ImageToImageView(d->refImage);

			const auto [min, max] = d->refImage->GetMinMax();
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0,1 });
			if (imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(refImageView, { -INT_MAX,static_cast<double>(min) });
				const auto reseted = resetImage(refImageView, min);
				refImageView = combineByMask(reseted, refImageView, thersholded);
			}

			const auto rescaledImage = rescaleIntensity(refImageView, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, {0, 0} /*ignored*/, {0, 0} /*ignored*/, {0., 100.});

	    	const auto gradientImage = gradientOperator2d(rescaledImage, gradientOperator, gradientMode, smoothingFactor).outputAmplitudeImage;
			const auto rescaledGradientImage = rescaleIntensity(gradientImage, RescaleIntensity::UNSIGNED_INTEGER_16_BIT, RescaleIntensity::MIN_MAX, { 1,1 }, { 1,1 }, { 13000, 15000 });
			rescaledGradientImage->setProperties(refImageView->properties());
			
			const auto originStat = intensityStatistics(gradientImage, IntensityStatistics::MIN_MAX, { 0,1 });

			d->result = converter.ImageViewToImage(rescaledGradientImage);
			d->result->SetOriginalMinMax(static_cast<float>(originStat->minimum()), static_cast<float>(originStat->maximum()));
			d->result->SetMinMax(13000,15000);
			d->result->SetOffset(d->refImage->GetOffset());
			d->result->setChannel(d->refImage->getChannel());

			succeed = true;

			QLOG_INFO() << QString("FINISH GradientOperator2D - %1 ms").arg(etimer.elapsed());
		} catch (std::exception& e) {
		    QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
	}
}
