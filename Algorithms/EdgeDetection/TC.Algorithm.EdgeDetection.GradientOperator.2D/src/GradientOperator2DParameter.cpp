#include <ParameterRegistry.h>
#include "GradientOperator2DParameter.h"

namespace TC::Algorithm::EdgeDetection::GradientOperator {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("GradientOperator", "Select operator", "Select the gradient operator", "SelectOption", 0, 0, 6);
		RegisterNode("GradientOperatorIndex", "Select operator", "Select the gradient operator", "ScalarValue.int", 0, 0, 6);

		RegisterNode("GradientMode", "Select mode", "Select the output image to compute", "SelectOption", 0, 0, 3);
		RegisterNode("GradientModeIndex", "Select mode", "Select the output image to compute", "ScalarValue.int", 0, 0, 3);

        RegisterNode("SmoothingFactor", "Smoothing factor", "The smoothing factor defines the gradient sharpness", "ScalarValue.double", 60, 0, 100);
    }

}
