#define LOGGER_TAG "[LaplacianOperator]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataconverter.h>

#include "LaplacianOperatorUiParameter.h"
#include "LaplacianOperatorParameter.h"
#include "LaplacianOperator.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::EdgeDetection::Laplacian {
	struct Algorithm::Impl {		
		TCImage::Pointer refImage{ nullptr };
		TCImage::Pointer result{ nullptr };

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.edgedetection.laplacian") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.edgedetection.laplacian") };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->refImage = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    } else {
				return nullptr;
		    }
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }
	
	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
    }
	
	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START LaplacianOperator";

		const auto precision = static_cast<MorphologicalLaplacian::Precision>(d->param->GetValue("Precision").toInt());
		const auto kernelSize = d->param->GetValue("KernelRadius").toInt();

	    try {
			int64_t totalTime = 0;
			int64_t elapsedTime = 0;
	        QElapsedTimer etimer;

	        etimer.start();

			TCDataConverter converter;
		    auto refImageView = converter.ImageToImageView(d->refImage);

			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for data conversion TCImage to ImageView: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			etimer.restart();

			const auto [min, max] = d->refImage->GetMinMax();
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0,1 });
			if (imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(refImageView, { -INT_MAX,static_cast<double>(min) });
				const auto reseted = resetImage(refImageView, min);
				refImageView = combineByMask(reseted, refImageView, thersholded);
			}

			auto floatingInput = rescaleIntensity(refImageView, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, { 1,1 }, { 1,1 }, { 0.0,1.0 });
			
			MorphologicalLaplacian laplacianAlgo;
			laplacianAlgo.setInputImage(floatingInput);			
			laplacianAlgo.setPrecision(precision);
			laplacianAlgo.setKernelRadius(kernelSize);			
			laplacianAlgo.execute();
			
            const auto laplacianImage = laplacianAlgo.outputImage();
			const auto original_stat = intensityStatistics(laplacianImage, IntensityStatistics::MIN_MAX, { 0,1 });			
			auto rescaled = rescaleIntensity(laplacianImage, RescaleIntensity::UNSIGNED_INTEGER_16_BIT, RescaleIntensity::MIN_MAX, { 1,1 }, { 1,1 }, { 13000,15000 });
			rescaled->setProperties(refImageView->properties());
			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for gradient operator: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			etimer.restart();
						
			d->result = converter.ImageViewToImage(rescaled);
			d->result->SetOriginalMinMax(static_cast<float>(original_stat->minimum()), static_cast<float>(original_stat->maximum()));			
			d->result->SetMinMax(13000,15000);
			d->result->SetOffset(d->refImage->GetOffset());
			d->result->setChannel(d->refImage->getChannel());

			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for data conversion ImageView to TCImage: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			QLOG_INFO() << QString("FINISH LaplacianOperator - total: %1 ms").arg(totalTime);
		} catch (std::exception& e) {
		    QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return true;
	}
}
