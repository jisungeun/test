#include <ParameterRegistry.h>
#include "LaplacianOperatorParameter.h"

namespace TC::Algorithm::EdgeDetection::Laplacian {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("Precision", "Select operator", "Select the precision", "SelectOption", 0, 0, 1);
		RegisterNode("PrecisionIndex", "Select operator", "Select the precision", "ScalarValue.int", 0, 0, 1);

		RegisterNode("KernelRadius", "Set Kernel Radius", "Set Kernel Radius Size", "ScalarValue.int", 3, 0,5);
	}

}
