#pragma once

#include <memory>

class SoSeparator;
class SoOrthographicCamera;
class SoVolumeData;

class SceneGraph {
public:
    explicit SceneGraph();
    ~SceneGraph();

    auto SetRefData(SoVolumeData* volumeData) -> void;
    auto SetMaskData(SoVolumeData* volumeData) -> void;

    auto GetRoot() -> SoSeparator*;
    auto GetRefVolumeData() -> SoVolumeData*;
    auto GetMaskVolumeData() -> SoVolumeData*;

    auto GetRefTransparency() -> float;
    auto SetRefTransparency(float transparency) -> void;

    auto GetMaskTransparency() -> float;
    auto SetMaskTransparency(float transparency) -> void;

    auto Clear() -> void;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};