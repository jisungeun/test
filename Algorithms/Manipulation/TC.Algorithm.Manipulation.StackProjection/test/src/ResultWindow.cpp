#include <algorithm>

#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/SoOffscreenRenderArea.h>
#include <Inventor/Qt/SoQt.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Medical/helpers/MedicalHelper.h>

#include <RenderWindowSlice.h>
#include <TCDataConverter.h>

#include "SceneGraph.h"

#include "ResultWindow.h"

#include "ui_ResultWindow.h"


struct ResultVolume {
    SoRef<SoVolumeData> image;
    SoRef<SoVolumeData> mask;
};

struct ResultWindow::Impl {
    Ui::ResultWindow ui;

    std::shared_ptr<SceneGraph> htSceneGraph{ nullptr };
    std::shared_ptr<SceneGraph> projectionSceneGraph{ nullptr };

    std::unique_ptr<RenderWindow2D> htRenderWindow{ nullptr };
    std::unique_ptr<RenderWindow2D> projectionRenderWindow{ nullptr };

    auto SetHTImage(SoVolumeData* image);
    auto SetGradientImage(SoVolumeData* image);
};

auto ResultWindow::Impl::SetHTImage(SoVolumeData* image) {
    if (image == nullptr) std::cout << "image data is nullptr" << std::endl;

    htSceneGraph->SetRefData(image);
    htRenderWindow->ResetView2D(Direction2D::XY);
}

auto ResultWindow::Impl::SetGradientImage(SoVolumeData* image) {
    if (image == nullptr) std::cout << "gradient data is nullptr" << std::endl;

    projectionSceneGraph->SetRefData(image);
    projectionRenderWindow->ResetView2D(Direction2D::XY);
}

ResultWindow::ResultWindow(QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>() } {
    d->ui.setupUi(this);

    SoVolumeRendering::init();

    d->htSceneGraph = std::make_shared<SceneGraph>();
    d->projectionSceneGraph = std::make_shared<SceneGraph>();

    delete d->ui.viewWidget->layout();

    d->htRenderWindow = std::make_unique<RenderWindow2D>(d->ui.viewWidget);
    d->htRenderWindow->setSceneGraph(d->htSceneGraph->GetRoot());

    d->projectionRenderWindow = std::make_unique<RenderWindow2D>(d->ui.viewWidget);
    d->projectionRenderWindow->setSceneGraph(d->projectionSceneGraph->GetRoot());

    const auto viewLayout = new QHBoxLayout;
    viewLayout->addWidget(d->htRenderWindow.get());
    viewLayout->addWidget(d->projectionRenderWindow.get());
    d->ui.viewWidget->setLayout(viewLayout);
}

ResultWindow::~ResultWindow() {
    SoVolumeRendering::finish();
}

auto ResultWindow::View() -> QWidget* {
    return d->ui.viewWidget;
}

void ResultWindow::onUpdateRef(TCImage::Pointer image) {
    // clear previous results
    d->htSceneGraph->Clear();
    d->projectionSceneGraph->Clear();

    TCDataConverter converter;
    const auto imageVolume = SoRef<SoVolumeData>(converter.ImageToSoVolumeData(image, true));

    d->SetHTImage(imageVolume.ptr());
}

void ResultWindow::onUpdateProjectionImage(TCProcImage::Pointer image) {
    // input image treated single image or volume

    // clear previous results
    d->projectionSceneGraph->Clear();

    const auto dimCount = image->GetDimensionCount();
    if (dimCount< 2) return;

    const auto imageType = image->GetImageType();
    if (imageType != +ProcImageType::IMAGE && imageType != +ProcImageType::VOLUME) return;

    TCDataConverter converter;
    const auto vol = SoRef<SoVolumeData>(converter.ProcImageToSoVolumData(image));

    d->SetGradientImage(vol.ptr());
}