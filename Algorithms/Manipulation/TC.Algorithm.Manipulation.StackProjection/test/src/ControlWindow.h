#pragma once

#include <memory>

#include <QMainWindow>

#include <TCImage.h>
#include <TCProcImage.h>

class ControlWindow : public QWidget {
    Q_OBJECT
public:
    explicit ControlWindow(QWidget *parent = nullptr);
    ~ControlWindow();

private slots:
    void onOpen();
    void onRun();

signals:
    void loadedRef(TCImage::Pointer image);
    void updateProjectionImage(TCProcImage::Pointer image);

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};