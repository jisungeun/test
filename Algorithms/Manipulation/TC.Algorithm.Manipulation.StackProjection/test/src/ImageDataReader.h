#pragma once

class SoVolumeData;

class ImageDataReader {
public:
    ImageDataReader();
    ~ImageDataReader();

    auto Read(const QString& path, const bool loadImage = false,int time_step=0) const -> TCImage::Pointer;
    auto GetTimeSteps(const QString& path) const -> int;
    auto ReadOiv(const QString& path)const->SoVolumeData*;

private:

};
