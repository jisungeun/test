#include "SceneGraph.h"

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoTransferFunction.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Medical/helpers/MedicalHelper.h>


constexpr std::string_view refSepName = "RefImageSeparator";
constexpr std::string_view refVolumeDataName = "RefVolumeData";
constexpr std::string_view refMaterialName = "RefMaterial";
constexpr std::string_view refOrthoSliceName = "RefOrthoSlice";

constexpr std::string_view maskSepName = "MaskImageSeparator";
constexpr std::string_view maskVolumeDataName = "MaskVolumeData";
constexpr std::string_view maskMaterialName= "MaskMaterial";
constexpr std::string_view maskOrthoSliceName = "MaskOrthoSlice";

constexpr MedicalHelper::Axis viewAxis{ MedicalHelper::AXIAL };


struct SceneGraph::Impl {
    SoRef<SoSeparator> root{ nullptr };

    auto initSceneGraph() -> void;
};

auto SceneGraph::Impl::initSceneGraph() -> void {
    root = new SoSeparator;
}

SceneGraph::SceneGraph() : d{ std::make_unique<Impl>() } {
	d->initSceneGraph();
}

SceneGraph::~SceneGraph() {
	d->root = NULL;
}

auto SceneGraph::SetRefData(SoVolumeData* volumeData) -> void {
	if (auto oldRefSep = d->root->getByName(refSepName.data())) d->root->removeChild(oldRefSep);

    SoRef<SoSeparator> volumeSep = new SoSeparator;
    volumeSep->setName(refSepName.data());

    volumeData->setName(refVolumeDataName.data());
	volumeSep->addChild(volumeData);

    SoRef<SoDataRange> dataRange = new SoDataRange;
	MedicalHelper::dicomAdjustDataRange(dataRange.ptr(), volumeData);
	volumeSep->addChild(dataRange.ptr());

    SoRef<SoMaterial> material = new SoMaterial;
	material->setName(refMaterialName.data());
	material->diffuseColor.setValue(1, 1, 1);
	volumeSep->addChild(material.ptr());

    SoRef<SoTransferFunction> transFunc = new SoTransferFunction;
	transFunc->predefColorMap = SoTransferFunction::INTENSITY;
    MedicalHelper::dicomCheckMonochrome1(transFunc.ptr(), volumeData);
	volumeSep->addChild(transFunc.ptr());

    SoRef<SoOrthoSlice> orthoSlice = new SoOrthoSlice;
	orthoSlice->setName(refOrthoSliceName.data());
	orthoSlice->axis = viewAxis;
	orthoSlice->sliceNumber = volumeData->data.getSize()[viewAxis] / 2;
	orthoSlice->interpolation = SoSlice::MULTISAMPLE_12;
	volumeSep->addChild(orthoSlice.ptr());

    d->root->addChild(volumeSep.ptr());
}

auto SceneGraph::SetMaskData(SoVolumeData* volumeData) -> void {
	auto transparency = GetMaskTransparency();
	if (auto oldMaskSep = d->root->getByName(maskSepName.data())) d->root->removeChild(oldMaskSep);

    SoRef<SoSeparator> volumeSep = new SoSeparator;
	volumeSep->setName(maskSepName.data());

    volumeData->setName(maskVolumeDataName.data());
	volumeSep->addChild(volumeData);

	double min, max;
	volumeData->getMinMax(min, max);

	SoRef<SoDataRange> dataRange = new SoDataRange;
	dataRange->min = min;
	dataRange->max = max;
	volumeSep->addChild(dataRange.ptr());

	SoRef<SoMaterial> material = new SoMaterial;
	material->setName(maskMaterialName.data());
	material->diffuseColor.setValue(1., 1., 1.);
	material->transparency.setValue(transparency);
	volumeSep->addChild(material.ptr());

	SoRef<SoTransferFunction> transFunc = new SoTransferFunction;
    transFunc->predefColorMap = SoTransferFunction::PredefColorMap::LABEL_256;
    volumeSep->addChild(transFunc.ptr());

	SoRef<SoOrthoSlice> orthoSlice = new SoOrthoSlice;
	orthoSlice->setName(maskOrthoSliceName.data());
	orthoSlice->axis = viewAxis;
	orthoSlice->sliceNumber = volumeData->data.getSize()[viewAxis] / 2;
	orthoSlice->interpolation = SoSlice::NEAREST;
	volumeSep->addChild(orthoSlice.ptr());

	d->root->addChild(volumeSep.ptr());
}

auto SceneGraph::GetRoot() -> SoSeparator* {
    return d->root.ptr();
}

auto SceneGraph::GetRefVolumeData() -> SoVolumeData* {
	return dynamic_cast<SoVolumeData*>(d->root->getByName(refVolumeDataName.data()));
}

auto SceneGraph::GetMaskVolumeData() -> SoVolumeData* {
    return dynamic_cast<SoVolumeData*>(d->root->getByName(maskVolumeDataName.data()));
}

auto SceneGraph::GetRefTransparency() -> float {
    const auto material = dynamic_cast<SoMaterial*>(d->root->getByName(refMaterialName.data()));
	if (material == nullptr) return 0.5;	// default value

	return material->transparency[0];
}

auto SceneGraph::SetRefTransparency(float transparency) -> void {
    const auto material = dynamic_cast<SoMaterial*>(d->root->getByName(refMaterialName.data()));
	material->transparency = transparency;
}

auto SceneGraph::GetMaskTransparency() -> float {
    const auto material = dynamic_cast<SoMaterial*>(d->root->getByName(maskMaterialName.data()));
	if (material == nullptr) return 0.5;	// default value

	return material->transparency[0];
}

auto SceneGraph::SetMaskTransparency(float transparency) -> void {
    const auto material = dynamic_cast<SoMaterial*>(d->root->getByName(maskMaterialName.data()));
	material->transparency = transparency;
}

auto SceneGraph::Clear() -> void {
    if (auto oldRefSep = d->root->getByName(refSepName.data())) d->root->removeChild(oldRefSep);
	if (auto oldMaskSep = d->root->getByName(maskSepName.data())) d->root->removeChild(oldMaskSep);
}
