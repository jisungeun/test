#pragma once

#include <memory>

#include <QWidget>

#include <TCImage.h>
#include <TCProcImage.h>


class ResultWindow : public QWidget {
    Q_OBJECT
public:
    explicit ResultWindow(QWidget *parent = nullptr);
    ~ResultWindow();

    auto View() -> QWidget*;

public slots:
    void onUpdateRef(TCImage::Pointer image);
    void onUpdateProjectionImage(TCProcImage::Pointer image);

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};