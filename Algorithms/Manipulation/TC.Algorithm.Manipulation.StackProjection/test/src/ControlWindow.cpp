#include <iostream>

#include <QFileDialog>
#include <QElapsedTimer>
#include <QColor>
#include <Qt>
#include <QFile>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <TCDataConverter.h>
#include <ScalarData.h>
#include <TCProcImage.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <iolink/view/MultiImageViewFactory.h>
#include <iolink/Error.h>
#include <ioformat/IOFormat.h>
#include <ImageViz/SoImageViz.h>

#include "ImageDataReader.h"

#include "ControlWindow.h"

#include <QJsonArray>

#include "ui_ControlWindow.h"


using namespace imagedev;
using namespace iolink;
using namespace ioformat;


struct ControlWindow::Impl {
    Ui::ControlWindow ui;

    TCImage::Pointer refImage{ nullptr };

    auto LoadModule() -> void;

    auto Projection(TCImage::Pointer image) -> TCProcImage::Pointer;

    auto LogImageViewInfo(std::shared_ptr<ImageView> image) -> void;
    auto LogImageInfo(const TCImage::Pointer image) -> void;
    auto LogMaskInfo(const TCMask::Pointer mask) -> void;
};

auto ControlWindow::Impl::LoadModule() -> void {
    auto Load = [](QDir dir) {
        const auto appPath = dir.absolutePath();

        QStringList fileList = dir.entryList(QStringList() << "*.dll", QDir::Files);
        for (int i = 0; i < fileList.size(); i++) {
            auto fullPath = appPath + "/" + fileList[i];                
            if (PluginRegistry::LoadPlugin(fullPath) == 0) {
                std::cout << "Failed to load plugins in " << fullPath.toLocal8Bit().toStdString() << std::endl;
            }
        }    
    };

    QDir dir(qApp->applicationDirPath());
    dir.cd("algorithms");
    dir.cd("manipulation");
    Load(dir);
}

auto ControlWindow::Impl::Projection(TCImage::Pointer image) -> TCProcImage::Pointer {
    auto moduleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.manipulation.stackprojection");
    auto module = std::dynamic_pointer_cast<IPluginAlgorithm>(moduleInstance);

    auto param = moduleInstance->Parameter();
    param->SetValue("ProjectionMode", 2);   // PROJECTION
    param->SetValue("AutoScale", false);

    QElapsedTimer timer;
    std::cout << "Start projection ========>";
    timer.start();

    TCDataConverter converter;
    const auto procImage = converter.ImageToProcImage(image);

    module->SetInput(0, procImage);
    module->Execute();

    auto result = std::dynamic_pointer_cast<TCProcImage>(module->GetOutput(0));

    std::cout << "Done - " << timer.elapsed() << " ms" << std::endl;

    return result;
}

auto ControlWindow::Impl::LogImageViewInfo(std::shared_ptr<ImageView> image) -> void{
    std::string imageType;

    switch (getImageInterpretation(image)) {
        case ImageInterpretation::GRAYSCALE:            imageType = "GRAYSCALE"; break;
        case ImageInterpretation::LABEL:                imageType = "LABEL"; break;
        case ImageInterpretation::BINARY:               imageType = "BINARY "; break;
        case ImageInterpretation::CIE_XYZ:              imageType = "CIE_XYZ"; break;
        case ImageInterpretation::CIE_XYZ_CHROMATICITY: imageType = "CIE_XYZ_CHROMATICITY"; break;
        case ImageInterpretation::CIE_UVW:              imageType = "CIE_UVW"; break;
        case ImageInterpretation::CIE_LUV:              imageType = "CIE_LUV"; break;
        case ImageInterpretation::CIE_LAB:              imageType = "CIE_LAB"; break;
        case ImageInterpretation::RGB:                  imageType = "RGB"; break;
        case ImageInterpretation::RGB_CHROMATICITY:     imageType = "RGB_CHROMATICITY"; break;
        case ImageInterpretation::SRGB:                 imageType = "SRGB"; break;
        case ImageInterpretation::YUV:                  imageType = "YUV"; break;
        case ImageInterpretation::YCC:                  imageType = "YCC"; break;
        case ImageInterpretation::YIQ:                  imageType = "YIQ"; break;
        case ImageInterpretation::YDD:                  imageType = "YDD "; break;
        case ImageInterpretation::HSL:                  imageType = "HSL"; break;
        case ImageInterpretation::HSV:                  imageType = "HSV"; break;
        case ImageInterpretation::CMYK:                 imageType = "CMYK"; break;
        case ImageInterpretation::MULTISPECTRAL:        imageType = "MULTISPECTRAL"; break;
        default:                                        imageType = "UNKNOWN"; break;
    }

    std::cout << image->toString() << " / " << imageType << " / " << getDimensionalInterpretation(image).toString() << std::endl;
}

auto ControlWindow::Impl::LogImageInfo(const TCImage::Pointer image) -> void {
    auto [centerX, centerY, centerZ] = image->GetCenter();
    std::cout << " - center: " << centerX << ", " << centerY << ", " << centerZ << std::endl;

    auto [sizeX, sizeY, sizeZ] = image->GetSize();
    std::cout << " - size: " << sizeX << " x " << sizeY << " x " << sizeZ << std::endl;

    auto [x0, y0, z0, x1, y1, z1] = image->GetBoundingBox();
    std::cout << " - bounding box: (" << x0 << ", " << y0 << ", " << z0 << "), (" << x1 << ", " << y1 << ", " << z1 << ")" <<  std::endl;

    auto [min, max] = image->GetMinMax();
    std::cout << " - data range: " << min << " ~ " << max << std::endl;    
}

auto ControlWindow::Impl::LogMaskInfo(const TCMask::Pointer mask) -> void {
    auto [sizeX, sizeY, sizeZ] = mask->GetSize();
    std::cout << " - size: " << sizeX << " x " << sizeY << " x " << sizeZ << std::endl;

    auto [resX, resY, resZ] = mask->GetResolution();
    std::cout << " - res.: " << resX << " x " << resY << " x " << resZ << std::endl;

    auto blobIndexes = mask->GetBlobIndexes();
    std::cout << " - blobs: " << blobIndexes.size() << std::endl;
    //for (auto blobIndex : blobIndexes) {
    //    auto blob = mask->GetBlob(blobIndex);
    //    auto [x0, y0, z0, x1, y1, z1] = blob.GetBoundingBox();
    //    std::cout << "    - blob[" << blobIndex << "]";
    //    std::cout << " - code: " << blob.GetCode();
    //    std::cout << ", bounding box: ((" << x0 << ", " << y0 << ", " << z0 << "), (" << x1 << ", " << y1 << ", " << z1 << "))" << std::endl;
    //}    
}

ControlWindow::ControlWindow(QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>() } {
    SoImageViz::init();
    try {
        imagedev::init();
    } catch(imagedev::Exception& e) {
        std::cout << e.what() << std::endl;
    }

    d->ui.setupUi(this);

    connect(d->ui.openButton, &QPushButton::clicked, this, &ControlWindow::onOpen);
    connect(d->ui.runButton, &QPushButton::clicked, this, &ControlWindow::onRun);

    d->LoadModule();
}

ControlWindow::~ControlWindow() {
    imagedev::finish();
    SoImageViz::finish();
}

void ControlWindow::onOpen() {
    auto dir = QString("/home");
    const auto currentPath = d->ui.pathEdit->text();
    if (!currentPath.isEmpty()) {
        const auto currentDir = QFileInfo(currentPath).path();
        if (QDir(currentDir).exists()) dir = currentDir;
    }

    const auto path = QFileDialog::getOpenFileName(this, "Open TCF", dir, "TCF (*.tcf)");
    d->ui.pathEdit->setText(path);

    QApplication::processEvents();

    if (path.isEmpty()) return;

    std::cout << "Open file - " << path.toLocal8Bit().toStdString() << std::endl;

    // load TCF
	const ImageDataReader reader;
	d->refImage = reader.Read(path, true, 0);
    {
        std::cout << "Ref. Image information" << std::endl;
        d->LogImageInfo(d->refImage);
    }

    emit loadedRef(d->refImage);
}

void ControlWindow::onRun() {
    if (d->refImage == nullptr) return;

    const auto projectionImage = d->Projection(d->refImage);
    emit updateProjectionImage(projectionImage);
}