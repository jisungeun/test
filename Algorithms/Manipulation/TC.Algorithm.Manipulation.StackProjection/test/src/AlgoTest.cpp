#include <QApplication>

#include <Inventor/Qt/SoQt.h>

#include "ControlWindow.h"
#include "ResultWindow.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    ResultWindow resultWindow;
    auto view = resultWindow.View();
    SoQt::init(view);
    
    ControlWindow controlWindow;

    QObject::connect(&controlWindow, &ControlWindow::loadedRef, &resultWindow, &ResultWindow::onUpdateRef);
    QObject::connect(&controlWindow, &ControlWindow::updateProjectionImage, &resultWindow, &ResultWindow::onUpdateProjectionImage);

    controlWindow.show();
    SoQt::show(&resultWindow);
    SoQt::mainLoop();

    SoQt::finish();

    return 0;
}