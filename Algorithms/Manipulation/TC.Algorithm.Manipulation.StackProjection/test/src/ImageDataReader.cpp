#include <QStringList>
#include <QDebug>

#include <HDF5Mutex.h>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include <TCImage.h>
#include <OivHdf5Reader.h>
#include <TCFMetaReader.h>
#include <TCFSimpleReader.h>
#include <OivLdmReader.h>

#include "ImageDataReader.h"


using namespace TC::IO;

ImageDataReader::ImageDataReader() {

}
ImageDataReader::~ImageDataReader() {

}
auto ImageDataReader::ReadOiv(const QString& path)const -> SoVolumeData* {        
    H5::H5File file(path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
    auto group = file.openGroup("/Data/3D");
    std::string attrName = "DataCount";
    auto attr = group.openAttribute(attrName);
    int64_t time_steps;
    attr.read(H5::PredType::NATIVE_INT64, &time_steps);
    auto img = std::make_shared<TCImage>();
    //for (int i = 0; i < time_steps; i++) {
    auto tcfToOivVol = new OivHdf5Reader;
    QString tileName = QString("%1").arg(0, 6, 10, QLatin1Char('0'));        
    tcfToOivVol->setDataGroupPath("/Data/3D");
    tcfToOivVol->setTileDimension(256);
    tcfToOivVol->setTileName(tileName.toStdString());
    tcfToOivVol->setFilename(path.toStdString());

    auto OivVol = new SoVolumeData;
    OivVol->setReader(*tcfToOivVol, TRUE);

    return OivVol;
}
auto ImageDataReader::GetTimeSteps(const QString& path) const -> int {
    HDF5MutexLocker locker(HDF5Mutex::GetInstance());
    try {
        H5::H5File file(path.toLocal8Bit().constData(), H5F_ACC_RDONLY);
        auto group = file.openGroup("/Data/3D");
        std::string attrName = "DataCount";
        auto attr = group.openAttribute(attrName);
        int64_t time_steps;
        attr.read(H5::PredType::NATIVE_INT64, &time_steps);
        file.close();
        return time_steps;
    }catch(H5::Exception & e ) {
        qDebug() << e.getDetailMsg().c_str();
        return -1;
    }
}
auto ImageDataReader::Read(const QString& path, const bool loadImage,int time_step) const -> TCImage::Pointer {
    if (loadImage) {
        auto metaReader = new TCFMetaReader;
        auto meta = metaReader->Read(path);
        auto vers = meta->common.formatVersion.split(".");
        auto minor_ver = vers[1].toInt();            
        auto isFloat = (minor_ver < 3);
        auto isLDM = meta->data.isLDM;            
        std::shared_ptr<TCImage> img(new TCImage, std::default_delete<TCImage>());
        img->SetFilePath(path);
        if (isLDM) {
            OivLdmReader* reader = new OivLdmReader;
            reader->ref();
            reader->setDataGroupPath("/Data/3D");
            reader->setTileDimension(meta->data.data3D.tileSizeX, meta->data.data3D.tileSizeY, meta->data.data3D.tileSizeZ);
            QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
            reader->setFilename(path.toStdString());
            reader->setTileName(tileName.toStdString());
            SoVolumeData* vol = new SoVolumeData;
            vol->ref();
            vol->texturePrecision = 16;
            vol->ldmResourceParameters.getValue()->tileDimension.setValue(meta->data.data3D.tileSizeX, meta->data.data3D.tileSizeY, meta->data.data3D.tileSizeZ);
            vol->setReader(*reader,TRUE);
            reader->touch();

            auto dim = vol->getDimension();
            auto res = vol->getVoxelSize();                

            auto cnt = dim[0] * dim[1] * dim[2];
            
            std::shared_ptr<unsigned short[]> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());

            SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
            SoLDMDataAccess::DataInfoBox dataInfoBox =
                vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

            SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
            dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

            vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

            unsigned short* ptr = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

            memcpy(arr.get(), ptr, cnt * sizeof(unsigned short));

            dataBufferObj->unmap();

            img->SetImageVolume(arr);

            double rres[3];
            for(auto i=0;i<3;i++) {
                rres[i] = res[i];
            }

            img->SetBoundingBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
            img->SetResolution(rres);

            while (vol->getRefCount() > 0) {
                vol->unref();
            }
            vol = nullptr;
            while (reader->getRefCount() > 0) {
                reader->unref();
            }
            reader = nullptr;
        }else {
            auto reader = new TCFSimpleReader;
            reader->SetFilePath(path);
            auto result = reader->ReadHT3D(time_step, isFloat);
            auto dim = result.GetDimension();
            auto res = result.GetResolution();

            auto cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

            std::shared_ptr<unsigned short[]> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());
            if (isFloat) {
                auto base = result.GetDataAsDouble();
                for (auto i = 0; i < static_cast<int>(cnt); i++) {
                    arr.get()[i] = base.get()[i] * 10000.0;
                }
            }
            else {
                auto base = result.GetDataAsUInt16();
                memcpy(arr.get(), base.get(), cnt * sizeof(unsigned short));
            }

            img->SetImageVolume(arr);

            double rres[3];
            rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
            img->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
            img->SetResolution(rres);
            delete reader;
        }
        img->SetMinMax(static_cast<int>(meta->data.data3D.riMin * 10000.0), static_cast<int>(meta->data.data3D.riMax * 10000.0));
        delete metaReader;                        
        return img;
    }
    return nullptr;
}
