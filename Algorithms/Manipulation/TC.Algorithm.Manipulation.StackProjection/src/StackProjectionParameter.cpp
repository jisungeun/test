#include <ParameterRegistry.h>
#include "StackProjectionParameter.h"

namespace TC::Algorithm::Manipulation::StackProjection {
	auto StackProjection::Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

		RegisterNode("ProjectionMode", "Select mode", "Select the projection mode", "SelectOption", 1, 0, 8);
		RegisterNode("ProjectionModeIndex", "Select mode", "Select the projection mode", "ScalarValue.int", 1, 0, 8);

		RegisterNode("AutoScale", "Use auto scale", "Automatic intensity scaling mode", "TFCheck", true, "", "");

        RegisterNode("SmoothingSize", "Smoothing size", "The kernel size for smoothing the gradient", "ScalarValue.int", 5, 0, 100);

		RegisterNode("GradientOperator", "Select operator", "The gradient operator to apply", "SelectOption", 1, 0, 2);
    }

}
