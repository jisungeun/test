#define LOGGER_TAG "[StackProjection]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ioformat/IOFormat.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCProcImage.h>
#include <TCDataConverter.h>

#include "StackProjectionUiParameter.h"
#include "StackProjectionParameter.h"
#include "StackProjection.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Manipulation::StackProjection {
	struct Algorithm::Impl {		
		TCProcImage::Pointer refImage{ nullptr };
		TCProcImage::Pointer result{ nullptr };

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.manipulation.stackprojection") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.manipulation.stackprojection") };
		QMap<QString, IParameter::Pointer> params;
	};
	
	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->refImage = std::dynamic_pointer_cast<TCProcImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index)
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    } else {
				return nullptr;
		    }
		}
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }
	
	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
    }
	
	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START StackProjection";

		const auto projectionMode = static_cast<ImageStackProjection3d::ProjectionMode>(d->param->GetValue("ProjectionMode").toInt());
		const auto useAutoScale = d->param->GetValue("AutoScale").toBool() ? ImageStackProjection3d::AutoScale::YES : ImageStackProjection3d::AutoScale::NO;
		const auto smoothingSize = d->param->GetValue("SmoothingSize").toInt();
		const auto gradientOperator = static_cast<ImageStackProjection3d::GradientOperator>(d->param->GetValue("GradientOperator").toInt());

	    try {
			int64_t totalTime = 0;
			int64_t elapsedTime = 0;
	        QElapsedTimer etimer;

	        etimer.start();

			TCDataConverter converter;
		    const auto refImageView = converter.ProcImageToImageView(d->refImage);

			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for data conversion TCProcImage to ImageView: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			etimer.restart();

			// convert to image sequence
            VolumeToSequence volumeToSequenceAlgo;
            volumeToSequenceAlgo.setInputVolume(refImageView);
            volumeToSequenceAlgo.execute();

            const auto seqImage = volumeToSequenceAlgo.outputSequence();

			// projection
            ImageStackProjection3d projection;
            projection.setInputImage(seqImage);
            projection.setProjectionMode(projectionMode);

	        if (projectionMode == ImageStackProjection3d::PROJECTION) {
                projection.setAutoScale(useAutoScale);
			}

			if (projectionMode == ImageStackProjection3d::GRADIENT_MAXIMA) {
			    projection.setSmoothingSize(smoothingSize);
				projection.setGradientOperator(gradientOperator);
			}

            projection.execute();

            const auto projectionImage = projection.outputImage();

			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for gradient operator: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			etimer.restart();

            d->result = converter.ImageViewToProImage(projectionImage);

			elapsedTime = etimer.elapsed();
			QLOG_INFO() << QString("elapsed time for data conversion ImageView to TCProcImage: %1 ms").arg(elapsedTime);
			totalTime += elapsedTime;

			QLOG_INFO() << QString("FINISH StackProjection - total: %1 ms").arg(totalTime);
		} catch (std::exception& e) {
		    QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return true;
	}
}
