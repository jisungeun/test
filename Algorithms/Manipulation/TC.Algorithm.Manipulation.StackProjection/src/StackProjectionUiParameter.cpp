#include <ParameterRegistry.h>
#include "StackProjectionUiParameter.h"

namespace TC::Algorithm::Manipulation::StackProjection {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterHider("Hide1", QStringList{ "ProjectionModeIndex", "GradientOperatorIndex" });

        RegisterSetter("ProjectionMode","Select mode", QStringList{"ProjectionModeIndex"}, QStringList{"Highest intensity", "Highest gradient", "Projection", "Lowest intensity", "Median intensity", "Mean", "Entropy", "Energy"});
        AppendSetter("ProjectionMode", "ProjectionModeIndex", "Highest intensity",  0);
        AppendSetter("ProjectionMode", "ProjectionModeIndex", "Highest gradient",   1);
        AppendSetter("ProjectionMode", "ProjectionModeIndex", "Projection",         2);
        AppendSetter("ProjectionMode", "ProjectionModeIndex", "Lowest intensity",   3);
        AppendSetter("ProjectionMode", "ProjectionModeIndex", "Median intensity",   4);
        AppendSetter("ProjectionMode", "ProjectionModeIndex", "Mean",               5);
        AppendSetter("ProjectionMode", "ProjectionModeIndex", "Entropy",            6);
        AppendSetter("ProjectionMode", "ProjectionModeIndex", "Energy",             7);

        RegisterSetter("GradientOperator","Select operator", QStringList{"GradientOperatorIndex"}, QStringList{"Gaussian", "Morphological", "Recursive"});
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Gaussian",       0);
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Morphological",  1);
        AppendSetter("GradientOperator", "GradientOperatorIndex", "Recursive",      2);

        RegisterSorter("Order", QStringList{ "ProjectionMode", "ProjectionModeIndex", "GradientOperator", "GradientOperatorIndex"});
    }
}