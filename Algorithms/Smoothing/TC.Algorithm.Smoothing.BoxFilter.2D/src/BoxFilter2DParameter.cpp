#include <ParameterRegistry.h>
#include "BoxFilter2DParameter.h"

namespace TC::Algorithm::Smoothing::BoxFilter2D {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

	    RegisterNode("KernelSizeX", "Kernel size X", "The horizontal kernel size", "ScalarValue.int", 3, 1, 100);
        RegisterNode("KernelSizeY", "Kernel size Y", "The vertical kernel size", "ScalarValue.int", 3, 1, 100);

		RegisterNode("AutoScale", "Auto scale", "Automatic intensity scaling mode", "ScalarValue.bool", true, "", "");
    }

}
