#include <ParameterRegistry.h>
#include "MedianFilter25DUiParameter.h"

namespace TC::Algorithm::Smoothing::MedianFilter25D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}