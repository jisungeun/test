#define LOGGER_TAG "[MedianFilter2D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCDataConverter.h>

#include "MedianFilter25DUiParameter.h"
#include "MedianFilter25DParameter.h"
#include "MedianFilter25D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Smoothing::MedianFilter25D {
	struct Algorithm::Impl {		
		TCImage::Pointer refImage{ nullptr };
		TCImage::Pointer result{ nullptr };

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.smoothing.medianfilter.2.5d") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.smoothing.medianfilter.2.5d") };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->refImage = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
    }
	
	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START MedianFilter";

		if (d->refImage == nullptr) {
			QLOG_INFO() << "Invalid input data";
		    return false;
		}

		bool succeed = false;

	    try {
			const auto kernelRadius = d->param->GetValue("KernelRadius").toInt(3);
			const auto kernelMode = static_cast<MedianFilter2d::KernelMode>(d->param->GetValue("KernelMode").toInt(0));
			const auto searchMode = static_cast<MedianFilter2d::SearchMode>(d->param->GetValue("SearchMode").toInt(0));

			QElapsedTimer etimer;
            etimer.start();

			// convert TCImage to ImageView
			TCDataConverter converter;
			const auto image = converter.ImageToImageView(d->refImage);
		    if (image == nullptr) throw std::runtime_error("Failed to convert an input to an ImageView");

			const auto [sizeX, sizeY, sizeZ] = d->refImage->GetSize();

	    	auto destImage = convertImage(image, ConvertImage::FLOAT_32_BIT);
			for(auto i=0;i<sizeZ;i++) {
				const auto slice = getSliceFromVolume3d(image, GetSliceFromVolume3d::Z_AXIS, i);
				const auto floatSlice = convertImage(slice, ConvertImage::FLOAT_32_BIT);				
				const auto slice_median = medianFilter2d(floatSlice, kernelRadius, kernelMode, searchMode);
				destImage = setSliceToVolume3d(destImage, slice_median, SetSliceToVolume3d::Z_AXIS, i);
			}

			const auto ushortImage = convertImage(destImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto stat = intensityStatistics(ushortImage, IntensityStatistics::MIN_MAX, { 0,1 });

	        d->result = converter.ImageViewToImage(ushortImage);
			d->result->SetMinMax(stat->minimum(), stat->maximum());
			d->result->SetOffset(d->refImage->GetOffset());
			d->result->setChannel(d->refImage->getChannel());

			QLOG_INFO() << QString("FINISH MedianFilter - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
		    QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    } else {
				return nullptr;
		    }
		}
		
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        
    }	
}
