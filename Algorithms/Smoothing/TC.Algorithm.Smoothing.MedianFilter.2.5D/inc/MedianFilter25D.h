#pragma once

#include <QObject>

#include <IPluginAlgorithm.h>

#include "TC.Algorithm.Smoothing.MedianFilter.2.5DExport.h"

namespace TC::Algorithm::Smoothing::MedianFilter25D {
	class TC_Algorithm_Smoothing_MedianFilter_2_5D_API Algorithm
		:public QObject
		, public IPluginAlgorithm
	{
		Q_OBJECT
			Q_PLUGIN_METADATA(IID "org.tomocube.algorithm.smoothing.medianfilter.2.5d" FILE "AlgorithmMetadata.json")
			Q_INTERFACES(IPluginAlgorithm)

	public:
		Algorithm();
		virtual ~Algorithm();

		auto clone() const->IPluginModule* override;

        auto Parameter(const QString& key)->IParameter::Pointer override;
		auto DuplicateParameter(const QStringList& keys) -> void override;	// deprecated

		auto UiParameter() -> IUiParameter::Pointer override;
		auto ParamConverter(IParameter::Pointer, QString key) -> std::tuple<QString, QJsonValue> override;

		auto SetInput(int index, IBaseData::Pointer data) -> bool override;
		auto GetOutput(int index) const -> IBaseData::Pointer override;
		auto GetOutputs() const -> QList<IBaseData::Pointer> override;

		auto Execute() -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}