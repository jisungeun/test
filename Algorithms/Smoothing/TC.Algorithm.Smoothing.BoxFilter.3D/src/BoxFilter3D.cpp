#define LOGGER_TAG "[BoxFilter]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCDataConverter.h>

#include "BoxFilter3DUiParameter.h"
#include "BoxFilter3DParameter.h"
#include "BoxFilter3D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Smoothing::BoxFilter3D {
	constexpr std::string_view AlgorithmName{"org.tomocube.algorithm.smoothing.boxfilter.3d"};

	struct Algorithm::Impl {
		TCImage::Pointer input;
		TCImage::Pointer output;

		IParameter::Pointer param{ ParameterRegistry::Create(AlgorithmName.data()) };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create(AlgorithmName.data()) };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    } else {
				return nullptr;
		    }
		}
		
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        
    }

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

	    d->input = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index)

		return d->output;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->output };
    }

	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START BoxFilter";

		QElapsedTimer etimer;
		etimer.start();

		if (d->input == nullptr) {
			QLOG_INFO() << "Invalid input data";
		    return false;
		}

		bool succeed = false;

	    try {
			const auto kernelX = d->param->GetValue("KernelSizeX").toInt();
		    const auto kernelY = d->param->GetValue("KernelSizeY").toInt();
		    const auto kernelZ = d->param->GetValue("KernelSizeZ").toInt();
		    const auto autoScale = d->param->GetValue("AutoScale").toBool() ? BoxFilter3d::AutoScale::YES : BoxFilter3d::AutoScale::NO;

			TCDataConverter converter;

	        const auto image = converter.ImageToImageView(d->input);
			if (image == nullptr) throw std::runtime_error("Failed to convert an input to an ImageView");

			auto rescaledImage = rescaleIntensity(image, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, {0, 0} /*ignored*/, {0, 0} /*ignored*/, {0., 100.});

	    	const auto filteredImage = boxFilter3d(rescaledImage, kernelX, kernelY, kernelZ, autoScale);
			rescaledImage = rescaleIntensity(filteredImage, RescaleIntensity::UNSIGNED_INTEGER_16_BIT, RescaleIntensity::MIN_MAX, { 1,1 }, { 1,1 }, { 13000, 15000 });
			rescaledImage->setProperties(image->properties());

			const auto stat = intensityStatistics(rescaledImage, IntensityStatistics::MIN_MAX, { 0,1 });

			d->output = converter.ImageViewToImage(rescaledImage);
			d->output->SetMinMax(stat->minimum(), stat->maximum());
			d->output->SetOffset(d->input->GetOffset());
			d->output->setChannel(d->input->getChannel());

			succeed = true;
		} catch (std::exception& e) {
		    QLOG_INFO() << e.what();
		} catch (...) {
			QLOG_INFO() << "UNKNOWN ERROR";
		}

		return succeed;
	}
	
}
