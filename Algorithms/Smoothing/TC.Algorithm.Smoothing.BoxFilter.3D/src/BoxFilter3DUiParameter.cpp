#include <ParameterRegistry.h>
#include "BoxFilter3DUiParameter.h"

namespace TC::Algorithm::Smoothing::BoxFilter3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}