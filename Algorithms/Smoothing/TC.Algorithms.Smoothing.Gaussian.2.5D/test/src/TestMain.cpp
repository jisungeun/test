#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <QDir>
#include <QFile>

#include <catch2/catch.hpp>
#include <H5Cpp.h>

#include <TCFMetaReader.h>
#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <TCDataConverter.h>


using namespace imagedev;
using namespace iolink;
using namespace ioformat;

auto LoadHT(std::string_view path) -> TCImage::Pointer {
	TCImage::Pointer image{nullptr};
	try {
		// get HT 3D
		const auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
		const auto metaInfo = metaReader->Read(path.data());

		H5::H5File file(path.data(), H5F_ACC_RDONLY);
		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		int size[3] = {metaInfo->data.data3D.sizeX, metaInfo->data.data3D.sizeY, metaInfo->data.data3D.sizeZ};
		double res[3] = {metaInfo->data.data3D.resolutionX, metaInfo->data.data3D.resolutionY, metaInfo->data.data3D.resolutionZ};

		std::shared_ptr<uint16_t[]> data(new uint16_t[size[0] * size[1] * size[2]](), std::default_delete<uint16_t[]>());
		dataSet.read(data.get(), H5::PredType::NATIVE_UINT16);
		dataSet.close();
		group.close();
		file.close();

		// get a center slice image
		Vector3d spacing{ res[0], res[1], res[2] };
		Vector3d origin{ -size[0] * res[0] / 2, -size[1] * res[1] / 2, -size[2] * res[2] / 2 };

		const auto spatialCalibrationProperty = iolink::SpatialCalibrationProperty(origin, spacing);

		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));

		const VectorXu64 imageShape{ static_cast<uint64_t>(size[0]), static_cast<uint64_t>(size[1]), static_cast<uint64_t>(size[2]) };
		const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		imagedev::setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);
		imagedev::setImageInterpretation(imageView, ImageInterpretation::GRAYSCALE);

		const RegionXu64 imageRegion{{0, 0, 0}, imageShape};
		imageView->writeRegion(imageRegion, data.get());

		TCDataConverter converter;
		image = converter.ImageViewToImage(imageView);
		image->SetMinMax(metaInfo->data.data3D.riMin * 10000, metaInfo->data.data3D.riMax * 10000);
		
	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	} catch (...) {
		FAIL("Unknown error");
	}

	return image;
}

auto Save(const TCImage::Pointer image, std::string_view name) {
	const auto dir = QFileInfo(_TEST_DATA).absolutePath();
	const auto savePath = QString("%1/%2.tiff").arg(dir).arg(name.data());

	TCDataConverter converter;
	const auto imageview = converter.ImageToImageView(image);
	ioformat::writeView(imageview, savePath.toStdString());
}

TEST_CASE("Gaussian") {
    REQUIRE(QFile::exists(_TEST_DATA));

    try {
        imagedev::init();
    } catch(imagedev::Exception& e) {
        FAIL(e.what().c_str());
    }
    
	const auto plugin = QString("%1/smoothing/TC.Algorithm.Smoothing.Gaussian.2.5D.dll").arg(_PLUGIN_DIR);
	REQUIRE(PluginRegistry::LoadPlugin(plugin) > 0);

    const auto ht = LoadHT(_TEST_DATA);

	SECTION("2.5D") {
		const auto algoModule = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(plugin, true));

		const auto param = algoModule->Parameter();
		param->SetValue("StandardDeviationX", 1.);
		param->SetValue("StandardDeviationY", 1.);

		algoModule->SetInput(0, ht);
		REQUIRE(algoModule->Execute());

		const auto output = std::dynamic_pointer_cast<TCImage>(algoModule->GetOutput(0));
		REQUIRE(output != nullptr);

		Save(ht, "input");
		Save(output, "output");
	}

    imagedev::finish();
}