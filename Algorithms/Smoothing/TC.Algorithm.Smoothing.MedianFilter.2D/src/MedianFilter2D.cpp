#define LOGGER_TAG "[MedianFilter2D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCDataConverter.h>

#include "MedianFilter2DUiParameter.h"
#include "MedianFilter2DParameter.h"
#include "MedianFilter2D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Smoothing::MedianFilter2D {
	struct Algorithm::Impl {		
		TCImage::Pointer refImage{ nullptr };
		TCImage::Pointer result{ nullptr };

		IParameter::Pointer param{ ParameterRegistry::Create("org.tomocube.algorithm.smoothing.medianfilter.2d") };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create("org.tomocube.algorithm.smoothing.medianfilter.2d") };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
        if (index > 0) return false;

		d->refImage = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index);
        return d->result;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->result };
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
    }
	
	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START MedianFilter2D";

		if (d->refImage == nullptr) {
			QLOG_INFO() << "Invalid input data";
		    return false;
		}

		bool succeed = false;

	    try {
			const auto kernelRadius = d->param->GetValue("KernelRadius").toInt(3);
			const auto kernelMode = static_cast<MedianFilter2d::KernelMode>(d->param->GetValue("KernelMode").toInt(0));
			const auto searchMode = static_cast<MedianFilter2d::SearchMode>(d->param->GetValue("SearchMode").toInt(0));

			QElapsedTimer etimer;
            etimer.start();

			// convert TCImage to ImageView
			TCDataConverter converter;
			const auto image = converter.ImageToImageView(d->refImage);
		    if (image == nullptr) throw std::runtime_error("Failed to convert an input to an ImageView");
						
			const auto floatImage = convertImage(image, ConvertImage::FLOAT_32_BIT);

	    	const auto filteredImage = medianFilter2d(floatImage, kernelRadius, kernelMode, searchMode);			
			const auto ushortImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto stat = intensityStatistics(ushortImage, IntensityStatistics::MIN_MAX, { 0,1 });

			d->result = converter.ImageViewToImage(ushortImage);
			d->result->SetMinMax(stat->minimum(), stat->maximum());
			d->result->SetOffset(d->refImage->GetOffset());
			d->result->setChannel(d->refImage->getChannel());

			QLOG_INFO() << QString("FINISH MedianFilter2D - total: %1 ms").arg(etimer.elapsed());

			succeed = true;
		} catch (std::exception& e) {
		    QLOG_ERROR() << e.what();
		} catch (Exception& e) {
			QLOG_ERROR() << QString::fromStdString(e.what());
		} catch (...) {
			QLOG_ERROR() << "UNKNOWN ERROR";
		}

		return succeed;
	}

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    } else {
				return nullptr;
		    }
		}
		
	}

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        
    }	
}
