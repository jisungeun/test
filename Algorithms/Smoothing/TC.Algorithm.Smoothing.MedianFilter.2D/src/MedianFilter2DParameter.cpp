#include <ParameterRegistry.h>
#include "MedianFilter2DParameter.h"

namespace TC::Algorithm::Smoothing::MedianFilter2D {
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

        RegisterNode(
			"KernelRadius",
			"Kernel radius",
			"The half size of the structuring element.",
			"ScalarValue.int",
			3, 1, 100
		);

		RegisterNode(
			"KernelMode",
			"Kernel mode",
			"The structuring element shape and application mode.\n0: Square\n1: Disk",
			"ScalarValue.int",
			0, 0, 1
		);

		RegisterNode(
			"SearchMode",
			"Search mode",
			"Handles all types of method for finding the median.\n0: Automatic\n1: Histogram\n2: Selection",
			"ScalarValue.int",
			0, 0, 2
		);
    }

}
