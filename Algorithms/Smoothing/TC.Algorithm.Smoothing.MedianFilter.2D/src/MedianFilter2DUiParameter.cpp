#include <ParameterRegistry.h>
#include "MedianFilter2DUiParameter.h"

namespace TC::Algorithm::Smoothing::MedianFilter2D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));
    }
}