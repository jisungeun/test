#pragma once

#include <IParameter.h>

namespace TC::Algorithm::Smoothing::Gaussian2D {
	class Parameter : public IParameter {
		Q_OBJECT

	public:
		static auto Register()->void;

		static IParameter::Pointer CreateMethod() {
			return std::make_shared<Parameter>();
		}

		static std::string GetName() { return "org.tomocube.algorithm.smoothing.gaussian.2d"; }

		Parameter();
	};
}