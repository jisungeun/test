#define LOGGER_TAG "[BoxFilter2D]"
#include <TCLogger.h>

#include <iostream>

#include <QElapsedTimer>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>

#include <ParameterRegistry.h>
#include <TCImage.h>
#include <TCDataConverter.h>

#include "Gaussian3DUiParameter.h"
#include "Gaussian3DParameter.h"
#include "Gaussian3D.h"


using namespace imagedev;
using namespace iolink;


namespace TC::Algorithm::Smoothing::Gaussian3D {
	constexpr std::string_view AlgorithmName{"org.tomocube.algorithm.smoothing.gaussian.3d"};

	struct Algorithm::Impl {
		TCImage::Pointer input;
		TCImage::Pointer output;

		IParameter::Pointer param{ ParameterRegistry::Create(AlgorithmName.data()) };
		IUiParameter::Pointer uiParam{ UiParameterRegistry::Create(AlgorithmName.data()) };
		QMap<QString, IParameter::Pointer> params;
	};

	Algorithm::Algorithm() : d(new Impl) {
		Parameter::Register();
		UiParameter::Register();
	}

	Algorithm::~Algorithm() {

	}

	auto Algorithm::clone() const -> IPluginModule* {
        return new Algorithm();
    }

	auto Algorithm::Parameter(const QString& key) -> IParameter::Pointer {
		if (key.isEmpty()) {
			return d->param;
		} else {
		    if(d->params.contains(key)) {
				return d->params[key];
		    } else {
				return nullptr;
		    }
		}
		
	}

	auto Algorithm::DuplicateParameter(const QStringList& keys) -> void {
        
    }

	auto Algorithm::UiParameter() -> IUiParameter::Pointer {
		return d->uiParam;
    }

	auto Algorithm::ParamConverter(IParameter::Pointer param, QString key) -> std::tuple<QString, QJsonValue> {
		return std::make_tuple(QString(), QJsonValue());
    }

	auto Algorithm::SetInput(int index, IBaseData::Pointer data) -> bool {
		if (index > 0) return false;

	    d->input = std::dynamic_pointer_cast<TCImage>(data);
		return true;
    }

	auto Algorithm::GetOutput(int index) const -> IBaseData::Pointer {
		Q_UNUSED(index)

		return d->output;
    }

	auto Algorithm::GetOutputs() const -> QList<IBaseData::Pointer> {
		return { d->output };
    }

	auto Algorithm::Execute() -> bool {
		QLOG_INFO() << "START BoxFilter2D";

		QElapsedTimer etimer;
		etimer.start();

		if (d->input == nullptr) {
			QLOG_INFO() << "Invalid input data";
		    return false;
		}

		bool succeed = false;

	    try {
			const auto kernelX = d->param->GetValue("StandardDeviationX").toDouble();
	        const auto kernelY = d->param->GetValue("StandardDeviationY").toDouble();
			const auto kernelZ = d->param->GetValue("StandardDeviationZ").toDouble();

			TCDataConverter converter;

	        auto image = converter.ImageToImageView(d->input);
			if (image == nullptr) throw std::runtime_error("Failed to convert an input to an ImageView");

			const auto [min, max] = d->input->GetMinMax();
			const auto imageStat = intensityStatistics(image, IntensityStatistics::MIN_MAX, { 0,1 });
			if (imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(image, { -INT_MAX,static_cast<double>(min) });
				const auto reseted = resetImage(image, min);
				image = combineByMask(reseted, image, thersholded);
			}
			const auto floatImage = convertImage(image, ConvertImage::FLOAT_32_BIT);

			const auto filteredImage = gaussianFilter3d(floatImage, GaussianFilter3d::SEPARABLE, { kernelX, kernelY, kernelZ }, 2.0, GaussianFilter3d::OutputType::SAME_AS_INPUT, false);

			const auto ushortImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto resultStat = intensityStatistics(ushortImage, IntensityStatistics::MIN_MAX, { 0,1 });
						
			d->output = converter.ImageViewToImage(ushortImage);
			d->output->SetOffset(d->input->GetOffset());
			d->output->SetMinMax(resultStat->minimum(), resultStat->maximum());
			d->output->setChannel(d->input->getChannel());

			succeed = true;
		} catch (std::exception& e) {
		    QLOG_INFO() << e.what();
		} catch(Exception& e) {
			QLOG_INFO() << e.what().c_str();
		} catch (...) {
			QLOG_INFO() << "UNKNOWN ERROR";
		}

		return succeed;
	}
	
}
