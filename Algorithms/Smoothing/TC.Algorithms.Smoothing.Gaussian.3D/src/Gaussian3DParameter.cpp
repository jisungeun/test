#include <ParameterRegistry.h>
#include "Gaussian3DParameter.h"

namespace TC::Algorithm::Smoothing::Gaussian3D{
	auto Parameter::Register()->void {
		static bool s_registered = ParameterRegistry::Register(Parameter::GetName(), Parameter::CreateMethod);
	}

	Parameter::Parameter() : IParameter() {
		SetVersion("1.0.0");

	    RegisterNode("StandardDeviationX", "Standard Deviation X", "The horizontal kernel size", "ScalarValue.double", 1, 0, 100);
		RegisterNode("StandardDeviationY", "Standard Deviation Y", "The horizontal kernel size", "ScalarValue.double", 1, 0, 100);
		RegisterNode("StandardDeviationZ", "Standard Deviation Z", "The horizontal kernel size", "ScalarValue.double", 1, 0, 100);
    }

}
