#include <ParameterRegistry.h>
#include "MedianFilter3DUiParameter.h"

namespace TC::Algorithm::Smoothing::MedianFilter3D {
    auto UiParameter::Register() -> void {
        static bool s_registered = UiParameterRegistry::Register(GetName(), CreateMethod);
    }
    UiParameter::UiParameter() : IUiParameter() {
        SetFullName(QString(GetName().c_str()));

        RegisterEnabler("Method", "Median filter", QStringList{ "KernelMode", "SearchMode" }, QStringList());
        RegisterHider("Hide1", QStringList{ "KernelModeIndex", "SearchModeIndex" });

        RegisterSetter("KernelMode","Select structuring element", QStringList{"KernelModeIndex"}, QStringList{"Cube", "Ball"});
        AppendSetter("KernelMode", "KernelModeIndex", "Cube", 0);
        AppendSetter("KernelMode", "KernelModeIndex", "Ball", 1);

        RegisterSetter("SearchMode","Select structuring element", QStringList{"SearchModeIndex"}, QStringList{"Automatic", "Histogram", "Selection"});
        AppendSetter("SearchMode", "SearchModeIndex", "Automatic", 0);
        AppendSetter("SearchMode", "SearchModeIndex", "Histogram", 1);
        AppendSetter("SearchMode", "SearchModeIndex", "Selection", 2);

        RegisterSorter("Order", QStringList{ "Method", "KernelRadius", "KernelMode", "KernelModeIndex", "SearchMode", "SearchModeIndex"});
    }
}